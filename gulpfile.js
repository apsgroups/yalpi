var elixir = require('laravel-elixir');
var spritesmith = require('gulp.spritesmith');

var gulp = require('gulp');

gulp.task('sprite', function () {
    var spriteData = gulp.src('resources/assets/images/layout/sprite/**/*.png')
        .pipe(spritesmith({
            /* this whole image path is used in css background declarations */
            imgName: '../images/sprite_'+(new Date().getTime())+'.png',
            cssName: 'sprite.scss',
            cssVarMap: function (sprite) {
                sprite.name = 'icon-' + sprite.name;
            }
        }));
    spriteData.img.pipe(gulp.dest('./public/build/images'));
    spriteData.css.pipe(gulp.dest('./resources/assets/sass/frontend/layout'));
});

gulp.task('sprite_mob', function () {
    var spriteData = gulp.src('resources/assets/images/mobile/**/*.png')
        .pipe(spritesmith({
            /* this whole image path is used in css background declarations */
            retinaSrcFilter: 'resources/assets/images/mobile/*@2x.png',
            imgName: '../../build/images/sprite_mob_'+(new Date().getTime())+'.png',
            cssName: 'sprite.scss',
            cssVarMap: function (sprite) {
                sprite.name = 'icon-' + sprite.name;
            },
            retinaImgName: '../../build/images/sprite_mob_2x-'+(new Date().getTime())+'.png'
        }));
    spriteData.img.pipe(gulp.dest('./public/build/images'));
    spriteData.css.pipe(gulp.dest('./resources/assets/sass/mobile'));
});

elixir(function(mix) {
 mix
     //  .phpUnit()

    /**
     * Copy needed files from /node directories
     * to /public directory.
     */

     .copy(
       'node_modules/font-awesome/fonts',
       'public/build/fonts/font-awesome'
     )
     .copy(
       'node_modules/bootstrap-sass/assets/fonts/bootstrap',
       'public/build/fonts/bootstrap'
     )
     .copy(
       'node_modules/bootstrap-sass/assets/javascripts/bootstrap.min.js',
       'public/js/vendor/bootstrap'
     )
     .copy(
         'resources/assets/images',
         'public/images'
     )
     .copy(
         'resources/assets/js/plugin/ckeditor',
         'public/js/plugin/ckeditor'
     )
     .copy(
         'resources/assets/js/plugin/fine-uploader',
         'public/js/plugin/fine-uploader'
     ) 
     .copy(
         'resources/assets/js/backend/gallery.js',
         'public/js/backend/gallery.js'
     )
     .copy(
         'resources/assets/js/backend/menu.js',
         'public/js/backend/menu.js'
     )
     .copy(
         'resources/assets/js/backend/widget.js',
         'public/js/backend/widget.js'
     )
     .copy(
        'resources/assets/js/backend/plugin/nestable/jquery.nestable.js',
        'public/js/backend/plugin/nestable/jquery.nestable.js'
     )
     .copy(
         'resources/assets/js/frontend/coupon.js',
         'public/js/coupon.js'
     )
     .copy(
         'resources/assets/fonts',
         'public/build/fonts'
     )
     .copy(
         'resources/assets/css/frontend/fonts',
         'public/build/fonts'
     )
     .copy(
        'resources/assets/js/plugin/owl',
        'public/js/plugin/owl'
     )
     .copy(
        'resources/assets/js/plugin/sly',
        'public/js/plugin/sly'
     )
     .copy(
         'resources/assets/js/frontend/promo.js',
         'public/js/frontend/promo.js'
     )

     /**
      * Process frontend SCSS stylesheets
      */
     .sass([
        'frontend/app.scss'
     ], 'resources/assets/css/frontend/tmp/app.css')

     .sass([
        'frontend/layout/404.scss'
     ], 'public/css/404.css')

     .sass([
        'frontend/layout/user.scss'
     ], 'public/css/user.css')

     .sass([
        'mobile/user.scss'
     ], 'public/css/mobile/user.css')


     /**
      * Combine pre-processed frontend CSS files
      */
     .styles([
        'frontend/tmp/app.css',
        'plugin/fancybox/jquery.fancybox.css'
     ], 'public/css/frontend.css')

     .styles([
        'plugin/mmenu-light/mmenu-light.css'
     ], 'public/css/mobile/menu.css')

     .sass([
        'mobile/app.scss'
     ], 'public/css/mobile.css')

     /**
      * Combine frontend scripts
      */
     .browserify("frontend/app.js")
     .browserify("frontend/mobile.js")

     /**
      * Process backend SCSS stylesheets
      */
     
     .sass([
         'backend/app.scss',
         'backend/plugin/toastr/toastr.scss',
         'plugin/sweetalert/sweetalert.scss',
     ], 'resources/assets/css/backend/app.css') 
     .sass(['backend/property-filter.scss'], 'public/css/backend/property-filter.css')

     .browserify("backend/custom.js")
     .browserify("backend/import.js")
     .browserify("backend/virtual_discounts.js")
     .browserify("backend/property.js")
     .browserify("backend/property-filter.js", "public/js/backend/property-filter.js")

     /**
      * Combine pre-processed backend CSS files
      */
    
     .styles([
         'backend/plugin/nestable/jquery.nestable.css',
         'backend/plugin/icheck/blue.css',
         'plugin/chosen/chosen.css',
         'backend/app.css',
         'plugin/fancybox/jquery.fancybox.css',
         'plugin/tokenize/jquery.tokenize.css',
         'plugin/treeselectmenu/treeselectmenu.css',
         'plugin/bootstrap-datepicker/bootstrap-datepicker.css',
         'backend/custom.css'
     ], 'public/css/backend.css') 

     /**
      * Combine backend scripts
      */
     
     .scripts([
         'plugin/treeselectmenu/treeselectmenu.jquery.js',
         'plugin/sweetalert/sweetalert.min.js',
         'plugin/chosen/chosen.jquery.min.js',
         'plugins.js',
         'backend/app.js',
         'backend/plugin/toastr/toastr.min.js',
         'backend/plugin/typeahead/bootstrap-typeahead.js',
         'backend/module.js',
         'backend/product_set.js',
         'backend/search_content.js',
         'backend/toggle.js',
         'plugin/bootstrap-datepicker/bootstrap-datepicker.js',
         '../../../public/js/custom.js',
         '../../../public/js/property.js'
     ], 'public/js/backend.js')
     
     .scripts([
         //'plugin/mmenu-light/mmenu-light.js',
         //'plugin/mmenu-light/mmenu-light.polyfills.js',
         'mobile/menu.js'
     ], 'public/js/mobile/menu.js')
     
    /**
      * Apply version control
      */
     .version([
        "public/css/frontend.css", 
        "public/js/app.js", 
        "public/js/mobile.js", 
        "public/css/mobile.css", 
        "public/js/mobile.js", 
        "public/css/404.css", 
        'public/css/user.css',
        'public/css/mobile/user.css',
        "public/css/backend.css", 
        "public/js/backend.js", 
        "public/js/mobile/menu.js"
        ]);
});
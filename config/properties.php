<?php

return [
    'color' => 'fasad-color',
    'desk-color' => 'desk-color',
    'dimensions' => [
        'name' => 'dimensions',
        'templates' => [
            'default' => [
                ['height', 'height-2'], ['width', 'width-2'], ['depth', 'depth-2'],
            ],
            'width_meters' => [
                ['width', 'width-2'],
            ],
            'width_sm' => [
                ['width', 'width-2'],
            ],
            'sm' => [
                ['width', 'width-2'], ['height', 'height-2'], ['depth', 'depth-2'],
            ],
        ],
        'separators' => ['dimension' => ' x ', 'size' => '/'],
        'template_sm_types' => [57, 34, 36, 37],
        'properties' => [
            'dimension-1' => [
                'width' => 179,
                'elements' => [
                    'width' => ['left' => 0, 'top' => 15],
                    'width-2' => ['left' => 115, 'top' => -2],
                    'height' => ['left' => -58, 'top' => 125],
                    'depth' => ['left' => -30, 'top' => 209],
                    'depth-2' => ['left' => 146, 'top' => 110],
                ],
            ],
            'dimension-2' => [
                'width' => 179,
                'elements' => [
                    'width' => ['left' => 0, 'top' => 15],
                    'width-2' => ['left' => 80, 'top' => 210],
                    'height' => ['left' => -35, 'top' => 125],
                    'depth' => ['left' => 130, 'top' => 107],
                    'depth-2' => ['left' => 135, 'top' => 135],
                ],
            ],
        ],
        'preview_separators' => [1, 57, 34, 36, 37],
        'kitchen' => [
            'product_type_id' => 1,
            'placement' => [
                'property_id' => 10,
                'corner' => 39,
                'straight' => 33,
            ],
            'dimension' => [
                'corner' => 61,
                'straight' => 396,
            ]
        ]
    ],
];
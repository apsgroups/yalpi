<?php

return [
    'method' => 'widen',
    'size' => 'preview',
    'image_thumbnail' => 'images',
    'quality' => 100,
    'sizes' => [
        'categories' => [
            'preview' => [100, 100, 'method' => 'fit'],
            'large' => [400, null],
        ],
        'users' => [
            'preview' => [100, 100, 'method' => 'fit'],
            'comment' => [60, 60, 'method' => 'fit'],
            'avatar' => [60, 60, 'method' => 'fit'],
        ],
        'products' => [
            'preview2' => [270, null, 'method' => 'widen'],
            'preview3' => [270, 152, 'method' => 'fit'],
            'preview' => [100, 100, 'method' => 'fit'],
            'preview_mobile' => [372, 210, 'method' => 'fit'],
            'preview_mobile_listing2' => [420, 224, 'method' => 'fit'],
            'news' => [100, 85, 'method' => 'fit'],
            'small' => [150, null],
            'card' => [400, 245],
            'promo' => [266, 180],
            'large' => [605, null,
                'watermark' => [
                    'src' => 'small.png',
                    'position' => 'bottom-center',
                    'x' => 0,
                    'y' => 50,
                ],
            ],
            'xlarge' => [1200, null,
                'watermark' => [
                    'src' => 'big.png',
                    'position' => 'bottom-center',
                    'x' => 0,
                    'y' => 50,
                ],
            ],
            'xlarge_mobile' => [630, null,
                'watermark' => [
                    'src' => 'big.png',
                    'position' => 'bottom-center',
                    'x' => 0,
                    'y' => 50,
                ],
            ],
        ],
        'productattributes' => [
            'preview' => [100, 100, 'method' => 'fit'],
            'news' => [100, 85, 'method' => 'fit'],
            'small' => [150, null],
            'middle' => [250, null],
            'cart-added' => [270, 200, 'method' => 'fit'],
        ],
        'contents' => [
            'preview' => [100, 100, 'method' => 'fit'],
            'large' => [400, null],
            'news_widget_lead' => [370, 276, 'method' => 'fit'],
            'news_widget' => [170, 170, 'method' => 'fit'],
            'blog' => [1170, 454, 'method' => 'fit'],
            'blog-related' => [570, 236, 'method' => 'fit'],
        ],
        'propertyvalues' => [
            'color' => [17, 17, 'method' => 'fit'],
            'preview' => [100, 100, 'method' => 'fit'],
            'large' => [400, null],
        ],
        'multiuploads' => [
            'preview' => [100, 100, 'method' => 'fit'],
        ],
        'menuitems' => [
            'preview' => [100, 100, 'method' => 'fit'],
            'large' => [400, null],
        ],
        'productreviews' => [
            'preview' => [130, 80, 'method' => 'fit'],
            'large' => [800, null],
        ],
        'custom' => [
            'small' => [100, 100],
            'p_set_2' => [476, 315, 'method' => 'fitByHeight', 'quality' => 85],
        ],
        'cources' => [
            'preview' => [177, 118, 'method' => 'fit'],
        ],
        'authors' => [
            'preview' => [177, 118, 'method' => 'fit'],
            'avatar' => [170, 170, 'method' => 'fit'],
        ],
    ],
];
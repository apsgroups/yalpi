<?php

return [
    'types' => [
        \App\Models\MenuItem::class => 'Пункт меню',
        \App\Models\Category::class => 'Категория',
        \App\Models\Product::class => 'Товар',
        \App\Models\Content::class => 'Контент',
        \App\Models\Redirect::class => 'URL',
    ],
];
<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Custom Elasticsearch Client Configuration
    |--------------------------------------------------------------------------
    |
    | This array will be passed to the Elasticsearch client.
    | See configuration options here:
    |
    | http://www.elasticsearch.org/guide/en/elasticsearch/client/php-api/current/_configuration.html
    */

    'config' => [
        'hosts'     => ['localhost:9200'],
        'retries'   => 1,
    ],

    /*
    |--------------------------------------------------------------------------
    | Default Index Name
    |--------------------------------------------------------------------------
    |
    | This is the index name that Elastiquent will use for all
    | Elastiquent models.
    */

    'default_index' => env('ELASTIC_INDEX', 'myapp'),

    'analysis' => [
        'analyzer' => [
            'my_analyzer' => [
                'type' => 'custom',
                'tokenizer' => 'standard',
                'filter' => [
                    'lowercase',
                    'russian_morphology',
                    'english_morphology',
                    'my_stopwords',
                ],
            ],
        ],
        'filter' => [
            'my_stopwords' => [
                'type' => 'stop',
                'stopwords' => 'а,без,более,бы,был,была,были,было,быть,в,вам,вас,весь,во,вот,все,всего,всех,вы,где,да,даже,для,до,его,ее,если,есть,еще,же,за,здесь,и,из,или,им,их,к,как,ко,когда,кто,ли,либо,мне,может,мы,на,надо,наш,не,него,нее,нет,ни,них,но,ну,о,об,однако,он,она,они,оно,от,очень,по,под,при,с,со,так,также,такой,там,те,тем,то,того,тоже,той,только,том,ты,у,уже,хотя,чего,чей,чем,что,чтобы,чье,чья,эта,эти,это,я,a,an,and,are,as,at,be,but,by,for,if,in,into,is,it,no,not,of,on,or,such,that,the,their,then,there,these,they,this,to,was,will,with'
            ]
        ]
    ],

    'mapping' => [
        'title' => [
            'type' => 'string',
            'analyzer' => 'my_analyzer',
        ],
        'search_keys' => [
            'type' => 'string',
            'analyzer' => 'my_analyzer',
        ],
        'created_at' => [
            'type' => 'date',
        ],
        'published' => [
            'type' => 'boolean',
        ],
        'is_module' => [
            'type' => 'boolean',
        ],
        'novelty' => [
            'type' => 'boolean',
        ],
        'sale' => [
            'type' => 'boolean',
        ],
        'preorder' => [
            'type' => 'boolean',
        ],
        'in_stock' => [
            'type' => 'boolean',
        ],
        'price' => [
            'type' => 'double',
        ],
        'price_wholesale' => [
            'type' => 'double',
        ],
        'product_categories' => [
            'type' => 'integer',
        ],
        'module_parents' => [
            'type' => 'integer',
        ],
        'hits' => [
            'type' => 'integer',
        ],
        'category_order' => [
            'type' => 'nested',
        ],
        'product_composition' => [
            'type' => 'integer',
        ],
    ]
);
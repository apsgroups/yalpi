<?php
return [
    'settings' => [
        'default' => [
            'auto_activate'    => true,
            'activate_parents' => true,
            'active_class'     => 'active',
            'restful'          => false,
            'cascade_data'     => true,
            'rest_base'        => '',      // string|array
            'active_element'   => 'item',  // item|link
        ],
        'top' => [
            'auto_activate'    => true,
            'activate_parents' => true,
            'active_class'     => 'uk-active',
            'restful'          => false,
            'cascade_data'     => true,
            'rest_base'        => '',      // string|array
            'active_element'   => 'item',  // item|link
        ],
    ]
];
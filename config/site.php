<?php

return [
    'perPage' => [
        'limits' => [
            3 => [
                24 => 24,
                48 => 48,
            ],
            4 => [
                20 => 20,
                40 => 40,
            ],
            5 => [
                25 => 25,
                50 => 50,
            ],
            'content' => [
                25 => 25,
                50 => 50,
            ],
            'category_584' => [
                40 => 40,
                80 => 80,
            ],
        ],
        'default' => [
            0 => 24,
            3 => 24,
            4 => 20,
            5 => 25,
            2 => 42,
            'content' => 25,
        ]
    ],
    'sorting' => [
        'aliases' => [
            'products' => [
                'popular' => 'category',
                'higher-price' => 'price',
                'lower-price' => 'price',
            ],
        ],
        'directions' => [
            'popular' => 'asc',
            'higher-price' => 'desc',
            'lower-price' => 'asc',
        ],
        'reverse' => [
            'higher-price' => false,
            'lower-price' => false,
            'popular' => false,
        ],
        'default' => 'popular',
        'keys' => [
            'attribute' => 'sort_by',
            'direction' => 'sort_dir',
        ],
    ],
    'layouts' => [
        'all' => [
            'list',
            'grid',
        ],
        'default' => 'grid',
        'key' => 'layout',
    ],
    'price' => [
        'currency' => 'руб.',
    ],
    'last-seen' => 8,
    'product' => [
        'slider' => [
            'limit' => 5,
            'in_row' => 5,
        ],
        'promo_widget' => [
            'limit' => 15,
            'in_row' => 4,
        ],
        'reviews' => [
            'limit' => 5,
        ]
    ],
    'checkout' => [
        'default' => [
            'shipping_id' => 1,
            'payment_id' => 1,
        ]
    ],
    'news' => [
        'slug' => 'blog',
        'homepage' => 2,
    ],
    'articles' => [
        'slug' => 'articles',
        'homepage' => 4,
    ],
    'popular' => [
        'count' => 10,
        'in_row' => 5,
        'title' => 'С этим товаром покупают',
    ],
    'feedbacks' => [
        'callback',
        'oneclick',
        'complain',
        'thank',
        'contact',
        'consult',
        'question',
        'director',
        'proposal'
    ],
    'virtual-discounts' => [
        1 => [
            'to' => 5000,
            'list' => [15, 20, 30],
        ],
        2 => [
            'from' => 5000,
            'to' => 10000,
            'list' => [15, 20, 30],
        ],
        3 => [
            'from' => 10000,
            'list' => [15, 20, 30],
        ],
    ],
    'feature_icons' => [
        'camera' => 'Видео',
        'discuss' => 'Обсуждения',
        'certificate' => 'Сертификат',
        'mobile' => 'Мобильный',
        'desktop' => 'Десктоп'
    ],
    'price_type' => [
        'Цена',
        'Бесплатно',
        'По договору'
    ],
    'comments' => [
        'pre_moderation' => true
    ],
];
<?php

return [
    'types' => [
        'catalog' => 'Каталог',
        'category' => 'Категория каталога',
        'contentType' => 'Список контента',
        'content' => 'Элемент контента',
        'reviews' => 'Отзывы',
        'horizontal_divider' => 'Горизонтальный разделитель',
        'vertical_divider' => 'Вертикальный разделитель',
        'menu_text' => 'Текстовый блок',
        'menu_item' => 'Ссылка на элемент меню',
        'link' => 'Ссылка',
        'header' => 'Заголовок',
        'space' => 'Пространство',
        'route' => 'Маршрут',
    ],
    'search_redirects' => [
        'category',
        'contentType',
        'content',
    ],
    'assign_enabled' => [
        'catalog',
        'category',
        'contentType',
        'content',
        'reviews',
        'route',
    ],
    'breadcrumbs' => [
        'catalog',
        'category',
        'contentType',
        'content',
        'reviews',
    ],
    'target' => [
        '_self',
        '_blank',
        '_parent',
        '_top',
    ],
    'routes' => [
        'frontend.index' => 'Главная',
        'frontend.category.catalog' => 'Каталог',
    ],
];
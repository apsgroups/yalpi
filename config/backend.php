<?php

return [

    /**
     * Skin for Admin LTE backend theme
     *
     * Available options:
     *
     * black
     * black-light
     * blue
     * blue-light
     * green
     * green-light
     * purple
     * purple-light
     * red
     * red-light
     * yellow
     * yellow-light
     */
    'theme' => 'blue',

    'date' => [
        'format' => 'd.m.y H:i',
    ],

    'actions' => [
        'product' => [
            'copy' => 'Копировать',
            'unpublish' => 'Снять с публикации',
            'publish' => 'Опубликовать',
            'destroy' => 'Удалить',
            'primary-category' => 'Назначить главную категорию',
            'attach-categories' => 'Добавить в категории',
            'detach-categories' => 'Убрать из категорий',
            'sync-categories' => 'Установить категории (дополнительные)',
        ],
        'product-review' => [
            'unpublish' => 'Снять с публикации',
            'publish' => 'Опубликовать',
            'destroy' => 'Удалить',
        ],
        'comment' => [
            'unpublish' => 'Снять с публикации',
            'publish' => 'Опубликовать',
            'destroy' => 'Удалить',
        ],
        'feedback' => [
            'destroy' => 'Удалить',
        ],
        'import' => [
            'trash' => [
                'destroy' => 'Удалить',
            ]
        ],
        'redirect' => [
            'destroy' => 'Удалить',
        ],
        'tag' => [
            'destroy' => 'Удалить',
        ],
    ],

    'category' => [
        'positions' => [
            1, 2, 3, 4,
        ],
        'types' => [
            'other' => 'Другое',
            'catalog' => 'Каталог мебели',
            'furniture' => 'Предметы мебели',
        ],
        'child_templates' => [
            'default' => 'По-умолчанию',
            'readmore' => 'Изображение и кнопка подробнее',
            'overlay' => 'Изображение с overlay-заголовком',
            'icons' => 'Навигация с иконками',
            'boxes' => 'Боксы с картинками',
            'boxes-sofa' => 'Боксы для диванов',
            'boxes-small' => 'Боксы с картинками (маленькие превью)',
            'boxes-center' => 'Боксы с картинками (по центру)',
            'none' => 'Не выводить',
        ]
    ],
    'properties' => [
        'position' => [
            'primary' => 'Что изучаем? - 1 уровень',
            'secondary' => 'Что изучаем? - 2 уровень',
            'ext' => 'Что изучаем? 3 уровень',
            'teaching' => 'Преподавание',
            'language' => 'Языки',
            'rate' => 'Рейтинг',
        ],
        'display_tag' => [
            'checkbox' => 'Чекбоксами',
            'select' => 'Выпадающий список',
        ],
    ]
];
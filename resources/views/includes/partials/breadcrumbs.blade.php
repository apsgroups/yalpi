@if ($breadcrumbs)
    <div class="uk-container uk-container-center uk-breadcrumb-container">
        <ul class="uk-breadcrumb breadcrumb">
            @foreach ($breadcrumbs as $k=>$breadcrumb)
                @if(!$breadcrumb->last)
                    <li {!! (count($breadcrumbs) - 2 === $k ? ' class="goback"' : '') !!}>
                        <a href="{{{ $breadcrumb->url }}}"><span>{{{ $breadcrumb->title }}}</span></a>
                    </li>
                @else
                    <li class="active">
                        <span>{{{ $breadcrumb->title }}}</span>
                    </li>
                @endif
            @endforeach
        </ul>
    </div>

    <?php

    $items = [];

    foreach ($breadcrumbs as $key => $value) {
        $items[] = [
            "@type" => "ListItem",
            "position" => $key + 1,
            "item" => [
                "@id" => url($value->url),
                "name" => $value->title
            ]
        ];
    }

    $scheme = [
        "@context" => "https://schema.org",
        "@type" => "BreadcrumbList",
        "itemListElement" => $items
    ];
    ?>

    <script type="application/ld+json">{!! json_encode($scheme) !!}</script>

    @push('header-css', 'shadow') 
@endif
@extends('mobile.layouts.master')

@include('frontend.includes.seometa', ['model' => $product])

@section('breadcrumbs', Breadcrumbs::render('frontend.product', $product))

@include('frontend.product.partials.opengraph')

@section('content')
    <div itemscope itemtype="https://schema.org/Product">
        <div class="main-container" id="product-container">
            <div class="uk-container uk-container-center">
                <div class="product-body">
                	<div class="main-screen uk-clearfix">
                		<div class="main-screen-inner">
		                    @if($product->product_type_id === 8 && (int)$product->price === 0)
		                        <div class="video-wrap">
		                            <iframe width="770" height="433" src="https://www.youtube.com/embed/{{ $product->video }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
		                        </div>
		                    @endif

		                    <div class="meta-wrap">
		                        <div class="meta-block shadow-block">
									<div class="inner">
			                            <div class="name">
			                                <h1 itemprop="name">{{ $product->title }}</h1>
			                            </div>              
			                            
			                            <div class="meta-row">
			                                <div class="rate-stars">
			                                    <span class="title">Рейтинг:</span>
			                                    <span class="rate">
			                                        {!! str_repeat('<span class="star"></span>', $product->rate) !!}
			                                    </span>
			                                </div>
			                                <div class="rate-counter">
			                                    <span class="title">Отзывы:</span>
			                                    <span>{{ $commentsCount }}</span>
			                                </div>
			                            </div>

			                            <meta itemprop="description" content="{{ $product->title }}" />

			                            <div class="preview-desc">{{ $product->preview_title }}</div>

			                            <div class="meta-row info">
				                            @if($authors->count())
				                            <?php
				                                $authors = $authors->map(function ($item, $key) {
				                                    $item->link = link_to_route('author.items', $item->name, [$item->slug]);

				                                    return $item;
				                                });
				                            ?>
				                            
				                            	<div class="authors"><span class="title">Авторы:</span> {!! $authors->implode('link', ', ') !!}</div>
				                            @endif

			                                <div><span class="title">Создание:</span> {{ $product->created_at->format('d.m.Y') }}</div>
			                                <div><span class="title">Обновление:</span> {{ $product->updated_at->format('d.m.Y') }}</div>

			                                @if($product->language)
			                                <div class="language"><span class="title">Субтитры:</span> {{ $product->language->title }}</div>
			                                @endif
			                            </div>
									</div>
		                        </div>
		                    </div>
		                </div>

		                <div class="card" id="product-card">
		                	<div class="card-inner">
		                        @if(!Auth::check())
			                    <div class="inner">
			                        @include('frontend.product.partials.price') 

		                            <div class="uk-text-center"><a href="{{ route('auth.login') }}" class="btn-popup login action"><span>Войти</span></a></div>
		                            
			                        @include('frontend.product.partials.features')
			                    </div>
			                    @else
			                    	@include('frontend.product.partials.soon')
			                    @endif
		                	</div>

		                	@include('frontend.product.partials.favorite')
		                </div>
                	</div>

                    @include('mobile.product.partials.tabs')

                    @include('frontend.product.partials.start-free')

                    @include('frontend.product.partials.program')

                    @if($product->product_type_id === 8 && (int)$product->price === 0)
	                    <?php
	                    	$items = [
						            ['skill', 'Прокачай скиллы', 'Посмотри видео, пройди тест (бета), получи бейдж (диплом)'],
						            ['free', 'Это бесплатно', 'Мы собрали тысячи качественных, бесплатных мастер-классов и уроков'],
						            ['share', 'Делись опытом', 'Делись опытом и мнением о пройденом материале и зарабатывай баллы']
						        ];
						?>
                    	@include('mobile.includes.advantages', ['title' => 'Преимущества он-лайн обучения в Yalpi', 'items' => $items])
                    @else
                        @include('mobile.includes.advantages', ['title' => 'Преимущества он-лайн обучения в Yalpi'])
                    @endif
                    
                </div>
            </div>
        </div>
    
        <div class="uk-container uk-container-center">
            {{-- @include('mobile.product.partials.authors') --}}

            @include('frontend.comments.index', ['item' => $product, 'comments' => $comments])

            @include('mobile.product.partials.related')
        </div>
    </div>
@endsection

@section('remarketing-data')
    <script type="text/javascript">
        var google_tag_params = {
            ecomm_prodid: '{{ $product->id }}',
            ecomm_pagetype: 'product',
            ecomm_totalvalue: {{ $product->price }},
        };
    </script>
@endsection

@section('body-attr', 'class="product-page"')

@push('js-footer')
{!! Html::script('/js/plugin/owl/owl.carousel.min.js') !!}

{!! Html::style('/js/plugin/owl/assets/owl.carousel.min.css') !!}
{!! Html::style('/js/plugin/owl/assets/owl.theme.default.min.css') !!}
@endpush
@if($relateds->count())
<div class="block-related product-items">
	<div class="block-title">Рекомендованные уроки:</div>

	<div class="products-slider owl-carousel owl-theme">
	    @foreach($relateds as $p)
	        <div class="item">
	            @include('frontend.product.preview', ['product' => $p])
	        </div>
	    @endforeach
	</div>
</div>
@endif
@if($product->experience != '' || $product->description)
    <div id="product-tabs">
        <div>
            <ul class="uk-tab" data-uk-tab="{connect:'#product-tabs-switcher'}">
                @if($product->experience != '')
                <li><a href="#" data-tab-btn="experience">Что ты получишь</a></li>
                @endif
                @if($product->description)
                <li><a href="#" data-tab-btn="description">Описание</a></li>
                @endif
            </ul>
        </div>

        <ul id="product-tabs-switcher" class="uk-switcher accordeon">
            @if($product->experience != '')
            <li class="item">
                <div class="toggle">Что ты получишь</div>
                <div class="content">
                    <div>
                        @include('frontend.product.partials.experience')
                        @if(!$product->video_format)
                            {{-- @include('frontend.product.partials.you-get') --}}
                        @endif
                    </div>
                </div>
            </li>
            @endif
            @if($product->description)
            <li class="item">
                <div class="toggle">Описание</div>
                <div class="content">
                    <div>
                        <div class="block-desc">
                            {!! $product->description !!}
                        </div>
                    </div>
                </div>
            </li>
            @endif
        </ul>
    </div>

    @if($product->start_learning)
        <a href="{{ $product->start_learning }}" class="action start-free-btn">Начать учиться</a>
    @endif
@endif
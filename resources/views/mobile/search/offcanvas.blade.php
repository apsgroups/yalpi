<div id="search-modal" class="uk-offcanvas">
    <div class="uk-offcanvas-bar">
    	<div class="search-header">
	    	<div id="search-header-bar">
	    		Поиск
	    		<a href="#" class="uk-offcanvas-close"></a>
	    	</div>

    		{!! Form::open(['route' => 'search.index', 'class' => 'uk-form search-form', 'method' => 'get', 'id' => 'search-form']) !!}
			    {!! Form::text('term', null, ['placeholder' => 'Более 3000 уроков', 'autocomplete' => 'off', 'id' => 'search-box', 'class' => 'search-input']) !!}
			    <button type="submit"></button>
			    <div class="search-holder"></div>
			{!! Form::close() !!}
    	</div>

		<div id="search-results">
			
		</div>
    </div>
</div>
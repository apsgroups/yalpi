@extends('mobile.layouts.master')

@section('title', 'Результаты поиска по запросу '.$term.($pagination->currentPage() > 1 ? ' | Страница '.$pagination->currentPage() : ''))

@section('body-attr', 'class="grey-bg search-page"')

@section('content')
    <div class="heading-block">
        <div class="uk-container uk-container-center">
            <div class="category-heading-inner">
                <h1 class="page-heading category-heading">Поиск &laquo;{!! $term !!}&raquo;</h1>
                
                @if($pagination)
                    <div class="category-total">{{ $pagination->total() }} {{ total_products_morph($pagination->total()) }}</div>
                @endif
            </div>
        </div>
    </div>

    <div class="uk-container uk-container-center">
        @include('mobile.search.partials.form')

        <div id="filter-results" class="block-area margin">
            @include('mobile.category.partials.products', ['itemPropUrl' => '/'])
        </div>
    </div>

    <div class="white-bg">
        <div class="uk-container uk-container-center category-advantages">
            @include('mobile.includes.advantages', ['title' => 'Преимущества он-лайн обучения в Yalpi'])
        </div>
    </div>
@stop

@push('js-footer')
{!! Html::script('/js/plugin/sly/sly.min.js') !!}
{!! Html::script('/js/plugin/owl/owl.carousel.min.js') !!}

{!! Html::style('/js/plugin/owl/assets/owl.carousel.min.css') !!}
{!! Html::style('/js/plugin/owl/assets/owl.theme.default.min.css') !!}
@endpush
<div class="container homeInfo hItxt hItxt1">
    <span class="title center">По вашему запросу ничего не найдено</span>
    <span class="title center low">Попробуйте уточнить запрос, либо воспользуйтесь навигацией по каталогу:</span>
    <div class="wrap">
        <button class="bigBut ico yellow navbar-toggle" type="button" data-toggle="offcanvas" data-target=".navmenu" data-canvas="body">
            <i class="fa fa-bars" aria-hidden="true"></i>
            <span>Каталог</span>
        </button>
    </div>
</div>
{!! Form::open(['route' => 'search.index', 'class' => 'search-page-form uk-form search-form', 'method' => 'get', 'id' => 'search-holder']) !!}
    {!! Form::text('term', $term, ['placeholder' => 'Что ищем?', 'autocomplete' => 'off', 'class' => 'search-input search-input-menu']) !!}
    <button type="submit"></button>

    <div class="search-holder"></div>
{!! Form::close() !!}
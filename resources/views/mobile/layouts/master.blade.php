<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="_token" content="{{ csrf_token() }}" />

        <title>@yield('title', \Settings::get('page_title'))</title>

        <meta name="description" content="@yield('metadesc', \Settings::get('page_title'))">

        @hasSection('metakeys-off')
        @else
        <meta name="keywords" content="@yield('metakeys', \Settings::get('metakeys'))">
        @endif

        @include('includes.partials.yandex-verification')

        @include('includes.partials.favicon')

        <!-- Styles -->
        @yield('before-styles-end')

        <style>
            html {
                min-height: 101%;
                overflow-y: visible;
            }
        </style>

        {!! Html::style(elixir('css/mobile.css')) !!}

        @yield('after-styles-end')

        @stack('head')

        <meta name="viewport" content="width=device-width">

        @include('includes.partials.google-site-verification')

        <!--[if lt IE 12]>
        <link rel="stylesheet" href="https://rawgit.com/codefucker/finalReject/master/reject/reject.css" media="all" />
        <script type="text/javascript" src="/js/reject.min.js" data-text="К сожалению, браузер, которым вы пользуйтесь, устарел, и не может нормально отображать сайт. Пожалуйста, скачайте любой из следующих браузеров:"></script>
        <![endif]-->

        <script>(window.Image ? (new Image()) : document.createElement('img')).src = 'https://vk.com/rtrg?p=VK-RTRG-222565-5i6oC';</script>

        @include('includes.vk')
        
        @include('includes.fb')

        @include('includes.gtm.head')
    </head>
    <body @yield('body-attr')>
        @include('includes.partials.metrika')

        @include('includes.gtm.body')
        
        @include('includes.partials.ga')

        <header class="@stack('header-css')">
            <div class="uk-container uk-container-center">
                @include('mobile.includes.header.index')
            </div>
        </header>

        @yield('breadcrumbs')
            
        <div id="content-container">
            @yield('content')
        </div>

        @yield('after-content')

        @include('frontend.includes.info')

        @include('mobile.search.offcanvas')

        @widget('menuWidget', ['menu' => 'catalog-homepage', 'layout' => 'offcanvas', 'menu_html_id' => 'offcanvas-catalog-menu', 'product_count' => 1])

        <footer>
            @include('mobile.includes.footer.index')
        </footer>

        <!-- JavaScripts -->
        {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>--}}
        {{--<script>window.jQuery || document.write('<script src="{{asset('js/vendor/jquery/jquery-2.1.4.min.js')}}"><\/script>')</script>--}}
        {{--{!! Html::script('js/vendor/bootstrap/bootstrap.min.js') !!}--}}

        @yield('before-scripts-end')
        {!! Html::script(elixir('js/mobile.js')) !!}
        {!! Html::script(elixir('js/mobile/menu.js')) !!}

        @stack('js-footer')

        @if(App::environment('production'))
        @include('includes.partials.livechat')
        @endif

        @yield('after-scripts-end')
    </body>
</html>
@if(count($products))
    <div class="products-grid-block" itemprop="hasOfferCatalog" itemscope itemtype="https://schema.org/OfferCatalog">
        @include('mobile.category.partials.layouts.'.$layoutService->getLayout())
    </div>

    <div class="uk-position-relative">
        @if($pagination && $pagination->hasMorePages() && $pagination->currentPage() < $pagination->lastPage())
        <div class="loadmore-block">
            @include('frontend.category.partials.loadmore')
        </div>
        @endif
    </div>

    <div class="uk-clearfix"></div>
@else
    <p class="uk-text-center">У данного автора еще нет уроков</p>
@endif
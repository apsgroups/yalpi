@extends('mobile.layouts.master')

@section('title', $meta->title().($pagination->currentPage() > 1 ? ' | Страница '.$pagination->currentPage() : ''))

@section('body-attr', 'class="grey-bg"')

@section('content')
<div class="heading-block">
    <div class="category-heading-inner">
        <h1 class="page-heading category-heading">{!! $meta->heading() !!}</h1>
        
        @if($total = $pagination->total())
            <div class="category-total">{{ $total }} {{ total_products_morph($total) }}</div>
        @endif
    </div>
</div>

    <div class="uk-container uk-container-center">
        <div id="filter-results" class="block-area margin">
            @include('mobile.author.partials.products', ['itemPropUrl' => route('author.items', [$author->slug])])
        </div>
    </div>
@stop

@if($pagination->currentPage() > 1)
@push('head')
<link rel="canonical" href="{{ route('author.items', [$author->slug]) }}" />
@endpush
@endif

@push('js-footer')
{!! Html::script('/js/plugin/sly/sly.min.js') !!}
{!! Html::script('/js/plugin/owl/owl.carousel.min.js') !!}

{!! Html::style('/js/plugin/owl/assets/owl.carousel.min.css') !!}
{!! Html::style('/js/plugin/owl/assets/owl.theme.default.min.css') !!}
@endpush
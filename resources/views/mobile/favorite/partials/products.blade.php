@php($compositions = isset($compositions) ? $compositions : collect([]))

@if(count($products))
    <div class="container-fuid catProds favor">
        <div class="container" id="product-grid">
            @include('mobile.category.partials.layouts.grid', ['addToCartBtn' => true, 'favorite' => true])
        </div>
    </div>

    @if($pagination->lastPage() > 1)
    <!--pagination-->
    <div class="container-fluid pagi">
        <div class="wrap">
            @include('mobile.category.partials.total')

            <div class="loadmore-block">
                @include('mobile.category.partials.loadmore')
            </div>
        </div>

        <div class="pagination-box">
            @include('mobile.category.partials.pagination')
        </div>
    </div>
    <!--/pagination-->
    @endif
@else
    <div class="container-fuid catProds favor text-center">
        У Вас нет избранных товаров
    </div>
@endif
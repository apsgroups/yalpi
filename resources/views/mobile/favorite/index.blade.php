@extends('mobile.layouts.master')

@include('frontend.includes.seometa_group', ['group' => 'favorite'])

@section('content')
    <div class="container breadcrumbs">
        @include('mobile.category.partials.breadcrumbs')
        @include('mobile.includes.breadcrumbs_menu')

        <h1>Избранное</h1>
    </div>

    <div id="filter-results">
        @include('mobile.favorite.partials.products')
    </div>
@endsection

@push('after-styles')
<link href="/mobile/css/mob_category_filter.css" rel="stylesheet">
<link href="/mobile/css/mob_category_products.css" rel="stylesheet">
<link href="/mobile/css/mob_filter.css" rel="stylesheet">
@endpush

@push('js-libs')
<script src="/mobile/js/ajax-setup.js"></script>
<script src="/mobile/js/ajax-pagination.js"></script>
<script src="/mobile/js/product-list.js"></script>
@endpush

@push('js-scripts')
<script src="/mobile/js/loadmore.js"></script>
<script src="/mobile/js/favorite.js"></script>
<script src="/mobile/js/minicart.js"></script>
<script src="/mobile/js/product.js"></script>
@endpush
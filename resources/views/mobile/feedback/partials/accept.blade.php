<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 custom-checkbox accept-lbl form-group">
    {!! Form::checkbox('accept', 1, 1, ['id' => 'accept', 'class' => 'required']) !!}
    <label for="accept">Нажимая кнопку «{{ (isset($acceptTxt) ? $acceptTxt : 'Отправить') }}» вы даете <a href="/uploads/media/polozhenie-o-konfidencialnosti-personalnyh-dannyh.pdf">согласие на обработку персональных данных</a>.</label>
</div>
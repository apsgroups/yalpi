<div class="text-center">
    <p>
        <strong>Ваш заказ оформлен!</strong>
    </p>
    <p>
        Спасибо за заказ, менеджер свяжется с Вами в {!! worktime_msg() !!}.
    </p>
</div>
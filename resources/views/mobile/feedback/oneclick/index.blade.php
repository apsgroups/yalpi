<!--breadcrumbs-->
<div class="container breadcrumbs">
    @include('mobile.includes.breadcrumbs_default', ['parent' => $product])
    @include('mobile.includes.breadcrumbs_menu')
</div>
<!--/breadcrumbs-->

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 content-title">
    <span>Купить в 1 клик</span>
</div>

<p class="text-center">{{ $product->title }}</p>

<div class="container-fuid">
    <div class="container">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 servBlock">
            {!! Form::open(['route' => ['feedback.oneclick', $product->id], 'method' => 'post', 'class' => 'form validate-form ajax-form']) !!}
                <div class="form-group {!! $errors->first('name', 'has-error') !!}">
                    <label for="name" class="control-label">Имя*</label>
                    {!! Form::text('name', null, ['id' => 'name', 'class' => 'form-control required']) !!}
                </div>

                <div class="form-group {!! $errors->first('phone', 'has-error') !!}">
                    <label for="phone" class="control-label">Телефон*</label>
                    {!! Form::tel('phone', null, ['id' => 'phone', 'class' => 'form-control required phone-mask']) !!}
                </div>

                @include('mobile.feedback.partials.accept')

                <button type="submit" class="bigBut marg" data-goal="kupklik2" data-loading-text="<i class='fa fa-spinner fa-spin'></i>">Отправить</button>
            {!! Form::close() !!}
        </div>
    </div>
</div>
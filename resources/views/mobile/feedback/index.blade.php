@extends('mobile.layouts.master')

@section('content')
    @include('mobile.feedback.'.$feedbackName.'.index')
@endsection

@push('after-styles')
<link href="/mobile/css/mob_info_service.css" rel="stylesheet">
@endpush
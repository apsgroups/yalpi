@extends('mobile.layouts.master')

@include('frontend.includes.seometa_group', ['group' => 'thank'])

@section('content')
    <!--breadcrumbs-->
    <div class="container breadcrumbs">
        @include('mobile.includes.breadcrumbs_default')
        @include('mobile.includes.breadcrumbs_menu')
    </div>
    <!--/breadcrumbs-->

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 content-title">
        <span>Оставить благодарность</span>
    </div>

    <div class="container-fuid">
        <div class="container">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 servBlock">
                {!! Form::open(['route' => ['feedback.thank'], 'method' => 'post', 'class' => 'form validate-form ajax-form']) !!}
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 servBlock">
                    <p class="align small">
                        Всем нравится, когда их благодарят, и наша компания не исключение.<br />
                        Лучшим показателем качества нашей работы служат ваши благодарности и положительные<br />
                        эмоции в адрес нашей компании. Для нас важно каждое мнение!<br />
                    </p>
                </div>

                <div class="form-group {!! $errors->first('name', 'has-error') !!}">
                    <label class="control-label" for="name">Имя*</label>
                    {!! Form::text('name', null, ['id' => 'name', 'class' => 'form-control required']) !!}
                </div>

                <div class="form-group {!! $errors->first('email', 'has-error') !!}">
                    <label class="control-label" for="email">Электронная почта*</label>
                    {!! Form::email('email', null, ['id' => 'email', 'class' => 'form-control required email']) !!}
                </div>

                <div class="form-group {!! $errors->first('order_number', 'has-error') !!}">
                    <label class="control-label" for="order_number">Номер заказа*</label>
                    {!! Form::text('order_number', null, ['id' => 'order_number', 'class' => 'form-control required']) !!}
                </div>

                <div class="form-group {!! $errors->first('message', 'has-error') !!}">
                    <label class="control-label" for="message">Сообщение*</label>
                    {!! Form::textarea('message', null, ['id' => 'message', 'class' => 'form-control required']) !!}
                </div>

                <button type="submit" class="bigBut marg" data-loading-text="<i class='fa fa-spinner fa-spin'></i>">Поблагодарить :)</button>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
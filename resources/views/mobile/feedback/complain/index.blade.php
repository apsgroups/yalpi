@extends('mobile.layouts.master')

@include('frontend.includes.seometa_group', ['group' => 'complain'])

@section('content')
    <!--breadcrumbs-->
    <div class="container breadcrumbs">
        @include('mobile.includes.breadcrumbs_default')
        @include('mobile.includes.breadcrumbs_menu')
    </div>
    <!--/breadcrumbs-->

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 content-title">
        <span>Оставить жалобу</span>
    </div>

    <div class="container-fuid">
        <div class="container">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 servBlock">
                {!! Form::open(['route' => ['feedback.complain'], 'method' => 'post', 'class' => 'form validate-form ajax-form']) !!}
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 servBlock">
                        <p class="align small">
                            В нашей компании работают живые люди, поэтому мы не застрахованы от ошибок или форсмажорных обстоятельств.<br />
                            Но вы всегда можете уведомить нас об любом прошествии, связанным с некачественной работой наших сотрудников.<br />
                            По каждому сообщению будет проведена проверка и ни один факт нарушений не останется без внимания.
                        </p>
                    </div>

                    <div class="form-group {!! $errors->first('name', 'has-error') !!}">
                        <label class="control-label" for="name">Имя*</label>
                        {!! Form::text('name', null, ['id' => 'name', 'class' => 'form-control required']) !!}
                    </div>

                    <div class="form-group {!! $errors->first('email', 'has-error') !!}">
                        <label class="control-label" for="email">Электронная почта*</label>
                        {!! Form::email('email', null, ['id' => 'email', 'class' => 'form-control required email']) !!}
                    </div>

                    <div class="form-group {!! $errors->first('order_number', 'has-error') !!}">
                        <label class="control-label" for="order_number">Номер заказа*</label>
                        {!! Form::text('order_number', null, ['id' => 'order_number', 'class' => 'form-control required']) !!}
                    </div>

                    <div class="form-group {!! $errors->first('message', 'has-error') !!}">
                        <label class="control-label" for="message">Суть проблемы*</label>
                        {!! Form::textarea('message', null, ['id' => 'message', 'class' => 'form-control required']) !!}
                    </div>

                    <button type="submit" class="bigBut marg" data-loading-text="<i class='fa fa-spinner fa-spin'></i>">Отправить</button>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
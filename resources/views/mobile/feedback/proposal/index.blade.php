@extends('mobile.layouts.master')

@include('frontend.includes.seometa_group', ['group' => 'proposal'])

@section('content')
    <!--breadcrumbs-->
    <div class="container breadcrumbs">
        @include('mobile.includes.breadcrumbs_default')
        @include('mobile.includes.breadcrumbs_menu')
    </div>
    <!--/breadcrumbs-->

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 content-title">
        <span>Что можно улучшить на сайте?</span>
    </div>

    <div class="container-fuid">
        <div class="container">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 servBlock">
                {!! Form::open(['files' => true, 'route' => ['feedback.proposal'], 'method' => 'post', 'class' => 'form validate-form ajax-form']) !!}
                    <div class="form-group {!! $errors->first('name', 'has-error') !!}">
                        <label class="control-label" for="name">Ваше имя*</label>
                        {!! Form::text('name', null, ['id' => 'name', 'class' => 'form-control required']) !!}
                    </div>

                    <div class="form-group {!! $errors->first('phone', 'has-error') !!}">
                        <label class="control-label" for="phone">Контактный телефон*</label>
                        {!! Form::tel('phone', null, ['id' => 'phone', 'class' => 'form-control required phone-mask']) !!}
                    </div>

                    <div class="form-group {!! $errors->first('email', 'has-error') !!}">
                        <label class="control-label" for="email">E-mail*</label>
                        {!! Form::text('email', null, ['id' => 'email', 'class' => 'form-control required email']) !!}
                    </div>

                    <div class="form-group {!! $errors->first('message', 'has-error') !!}">
                        <label class="control-label" for="message">Сообщение*</label>
                        {!! Form::textarea('message', null, ['id' => 'message', 'class' => 'form-control required']) !!}
                    </div>

                    <div class="upload-field form-group">
                        <input type="file" name="files[]" id="files" class="inputfile maxfiles-validate" accept="image/*" multiple />
                        <label for="files"><i class="fa fa-attach"></i> Прикрепить скриншот</label>
                        <div class="inputfile-list"></div>
                    </div>

                    <button type="submit" class="bigBut marg" data-loading-text="<i class='fa fa-spinner fa-spin'></i>">Отправить</button>

                    {!! Form::hidden('upload_token', str_random(20), ['id' => 'upload_token']) !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@push('js-scripts')
{!! Html::script('js/filepicker.js') !!}
@endpush
@include('frontend.includes.seometa_group', ['group' => 'contact'])

<!--breadcrumbs-->
<div class="container breadcrumbs">
    @include('mobile.includes.breadcrumbs_default')
    @include('mobile.includes.breadcrumbs_menu')
</div>
<!--/breadcrumbs-->

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 content-title">
    <span>Обратная связь</span>
</div>

<div class="container-fuid">
    <div class="container">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 servBlock">
            {!! Form::open(['route' => ['feedback.contact'], 'method' => 'post', 'class' => 'form validate-form ajax-form']) !!}
                <div class="form-group {!! $errors->first('name', 'has-error') !!}">
                    <label class="control-label" for="name">Ваше имя*</label>
                    {!! Form::text('name', null, ['id' => 'name', 'class' => 'form-control required']) !!}
                </div>

                <div class="form-group {!! $errors->first('phone', 'has-error') !!}">
                    <label class="control-label" for="phone">Контактный телефон*</label>
                    {!! Form::tel('phone', null, ['id' => 'phone', 'class' => 'form-control required phone-mask']) !!}
                </div>

                <div class="form-group {!! $errors->first('email', 'has-error') !!}">
                    <label class="control-label" for="email">E-mail*</label>
                    {!! Form::email('email', null, ['id' => 'email', 'class' => 'form-control required email']) !!}
                </div>

                <div class="form-group {!! $errors->first('message', 'has-error') !!}">
                    <label class="control-label" for="message">Сообщение*</label>
                    {!! Form::textarea('message', null, ['id' => 'message', 'class' => 'form-control required']) !!}
                </div>

                @include('mobile.feedback.partials.accept')

                <button type="submit" class="bigBut marg" data-loading-text="<i class='fa fa-spinner fa-spin'></i>">Отправить</button>

                {!! Form::hidden('is_opt', request('is_opt')) !!}
            {!! Form::close() !!}
        </div>
    </div>
</div>
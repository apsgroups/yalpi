<div class="text-center">
    <p>
        <strong>Спасибо, Ваша заявка №{{ $feedback->id }} принята!</strong>
    </p>
    <p>
        В {!! worktime_msg() !!} наш менеджер свяжется с Вами.
    </p>
</div>
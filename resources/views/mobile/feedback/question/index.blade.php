@include('frontend.includes.seometa_group', ['group' => 'question'])

{!! Form::open(['route' => ['feedback.question'], 'method' => 'post', 'class' => 'validate-form ajax-form uk-form']) !!}
    <h3 class="uk-text-center uk-margin-top-remove uppercase">Задать вопрос</h3>

    <div class="uk-form-row">
        <label class="uk-form-label" for="name">Ваше имя*</label>
        <div class="uk-form-controls">
            {!! Form::text('name', null, ['id' => 'name', 'class' => 'uk-form-width-large required']) !!}
        </div>
    </div>

    <div class="uk-form-row">
        <label class="uk-form-label" for="email">E-mail*</label>
        <div class="uk-form-controls">
            {!! Form::text('email', null, ['id' => 'email', 'class' => 'uk-form-width-large email required']) !!}
        </div>
    </div>

    <div class="uk-form-row">
        <label class="uk-form-label" for="message">Вопрос*</label>
        <div class="uk-form-controls">
            {!! Form::textarea('message', null, ['id' => 'message', 'class' => 'uk-form-width-large required', 'rows' => 4]) !!}
        </div>
    </div>

    <div class="uk-form-row uk-text-center">
        <button type="submit" id="send-question" class="uk-margin-top uk-button uk-button-large uk-button-primary">Отправить</button>
    </div>
{!! Form::close() !!}
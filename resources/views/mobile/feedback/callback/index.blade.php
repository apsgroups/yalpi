@include('frontend.includes.seometa_group', ['group' => 'callback'])

<!--breadcrumbs-->
<div class="container breadcrumbs">
    @include('mobile.includes.breadcrumbs_default')
    @include('mobile.includes.breadcrumbs_menu')
</div>
<!--/breadcrumbs-->

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 content-title">
    <span>Заказать звонок</span>
</div>

<div class="container-fuid">
    <div class="container">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 servBlock">
            {!! Form::open(['route' => ['feedback.callback'], 'method' => 'post', 'class' => 'form validate-form ajax-form']) !!}
                @if($product)
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 servBlock">
                        <p class="align small">
                            {{ $product->title }}
                        </p>
                    </div>
                @endif

                <div class="form-group {!! $errors->first('name', 'has-error') !!}">
                    <label class="control-label" for="name">Имя*</label>
                    {!! Form::text('name', null, ['id' => 'name', 'class' => 'form-control required']) !!}
                </div>

                <div class="form-group {!! $errors->first('phone', 'has-error') !!}">
                    <label class="control-label" for="phone">Телефон*</label>
                    {!! Form::tel('phone', null, ['id' => 'phone', 'class' => 'form-control required phone-mask']) !!}
                </div>

                @if($product)
                    {!! Form::hidden('product_id', $product->id) !!}
                @endif

                <button type="submit" class="bigBut marg" data-loading-text="<i class='fa fa-spinner fa-spin'></i>">Отправить</button>
            {!! Form::close() !!}
        </div>
    </div>
</div>
<div class="text-center">
    <p>
        <strong>Спасибо, ваша заявка №{{ $feedback->id }} принята!</strong>
    </p>
    <p>В {!! worktime_msg() !!} наш менеджер свяжется с Вами по телефону: {{ $feedback->phone }}</p>
    <p>Убедитесь, что номер телефона введён правильно!</p>
</div>
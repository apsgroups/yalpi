@extends('mobile.layouts.master')

@section('title', $user->name)

@section('body-attr', 'class="user-page"')

@section('content')
    @include('frontend.user.partials.user', ['mobile' => true])
    
    <div class="uk-container uk-container-center favorite-products">
        @if($favorites->count())
            <div class="products-grid-block uk-grid products-grid product-items" data-uk-grid-match="{target:'.product-desc'}" itemprop="hasOfferCatalog" itemscope itemtype="https://schema.org/OfferCatalog">
                @foreach($favorites->chunk(4) as $items)
                    @foreach($items as $k => $product)
                        <div class="uk-width-1-4" itemprop="itemListElement" itemscope itemtype="https://schema.org/Offer">
                            @include('frontend.product.preview', ['inRow' => 4, 'product' => $product->getPreviewData()])
                        </div>
                    @endforeach
                @endforeach
            </div>

            <div class="pagination-box">
                @include('frontend.category.partials.pagination', ['pagination' => $favorites])
            </div>
        @else      
            <div class="empty-results">
                У Вас нет избранных мастер-классов. Выберите нужный мастер-класс <a href="/">тут</a>.
            </div>  
        @endif
    </div>
@endsection

@push('head')
    {!! Html::style(elixir('css/mobile/user.css')) !!}
@endpush
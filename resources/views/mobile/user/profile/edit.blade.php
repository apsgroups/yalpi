@extends('mobile.layouts.master')

@section('title', 'Изменение профиля '.$user->name)

@section('body-attr', 'class="user-page"')

@section('content')
    @include('frontend.user.partials.user', ['mobile' => true])
    
    <div class="uk-container uk-container-center user-edit-form">
        <div class="shadow-block">
            {!! Form::model($user, ['route' => 'frontend.user.profile.update', 'class' => 'validate-form uk-form uk-form-horizontal', 'method' => 'PATCH', 'files' => 'true']) !!}
                <div class="form-title">Основная информация</div>

                <div class="uk-form-row">
                    <div>
                        <label class="uk-form-label" for="name">Имя*</label>
                    </div>
                    <div class="uk-form-controls">
                        {!! Form::text('name', null, ['id' => 'name', 'class' => 'uk-form-width-large required']) !!}
                    </div>
                </div>

                @if($user->canChangeEmail())
                    <div class="uk-form-row">
                        <div>
                            <label class="uk-form-label" for="email">E-mail*</label>
                        </div>
                        <div class="uk-form-controls">
                            {!! Form::email('email', null, ['id' => 'email', 'class' => 'uk-form-width-large required email']) !!}
                        </div>
                    </div>
                @endif
                
                <div class="uk-form-row">
                    <a href="{{ route('auth.password.change') }}"><i class="uk-icon-lock"></i> Изменить пароль</a>
                </div>

                <div class="separator"></div>

                <div class="uk-margin-top uk-form-row">
                    <button type="submit" class="action-btn">Сохранить</button>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
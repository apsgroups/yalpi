@extends('mobile.layouts.master')

@section('title', 'Skills '.$user->name)

@section('body-attr', 'class="user-page"')

@section('content')
    @include('frontend.user.partials.user', ['mobile' => true])
    
    <div class="uk-container uk-container-center">
        <div class="empty-results">
            Страница на доработке. <span>Здесь скоро будет новый функционал :)</span>
        </div>
    </div>
@endsection
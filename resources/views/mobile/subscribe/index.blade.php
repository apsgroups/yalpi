<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 content-title noMarg noBord">
    <span>Будьте в курсе акций и скидок!</span>
</div>

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 subscribe">
    <div class="inner">
        {!! Form::open(['route' => 'subscribe.add', 'class' => 'validate-form ajax-form', 'role' => 'form', 'method' => 'post']) !!}

        <div class="form-group">
            <div class="input-group">
                {!! Form::text('email', null, ['class' => 'email required form-control', 'placeholder' => 'Введите ваш E-mail']) !!}

                <span class="input-group-btn">
                <button class="btn btn-default" type="submit" data-loading-text="<i class='fa fa-spinner fa-spin'></i>">Подписаться</button>
            </span>
            </div>

            <div class="error-msg">Добавьте свой e-mail</div>
        </div>
        {!! Form::close() !!}
    </div>
</div>
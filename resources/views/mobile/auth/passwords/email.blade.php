<div class="auth password-reset-form">
    <div>
        {!! Form::open(['url' => 'password/email', 'class' => 'uk-form ajax-form validate-form']) !!}
            <div class="form-title">Восстановление пароля</div>

            <div class="desc">Введите вашу почту, чтобы воссатновить пароль. Вы получите письмо с дальнейшими инструкциями</div>

            <div class="uk-form-row">
                <div class="uk-form-controls">
                    {!! Form::input('email', 'email', null, ['id' => 'email', 'class' => 'uk-form-width-large email email-bg required', 'placeholder' => trans('validation.attributes.frontend.email')]) !!}
                </div><!--col-md-6-->
            </div><!--form-group-->

            <div>
                <button type="submit" class="uk-button uk-button-hover">
                    Продолжить
                </button>
            </div>

            <div class="uk-clearfix">
                <div class="uk-float-left">{!! link_to_route('auth.login', 'Войти', null, ['class' => 'btn-popup']) !!}</div>
                <div class="uk-float-right">Еще нет аккаунта? {!! link_to('register', trans('labels.frontend.auth.register_button'), ['class' => 'btn-popup']) !!}</div>
            </div>
        {!! Form::close() !!}
    </div>
</div>
@extends('mobile.layouts.master')

@section('title', 'Изменение профиля '.$user->name)

@section('body-attr', 'class="user-page"')

@section('content')
    @include('frontend.user.partials.user', ['mobile' => true])
    
    <div class="uk-container uk-container-center user-edit-form">
        <div class="shadow-block">
            {!! Form::model($user, ['route' => 'auth.password.update', 'class' => 'validate-form uk-form uk-form-horizontal', 'method' => 'post', 'files' => 'true']) !!}
                <div class="form-title">Изменение пароля</div>

                <div class="uk-form-row">
                    <div>
                        <label class="uk-form-label" for="old_password">{{ trans('validation.attributes.frontend.old_password') }}*</label>
                    </div>
                    <div class="uk-form-controls">
                        {!! Form::input('password', 'old_password', null, ['id' => 'old_password', 'class' => 'uk-form-width-large required', 'placeholder' => trans('validation.attributes.frontend.old_password'), 'autocomplete' => 'current-password']) !!}
                    </div>
                </div>

                <div class="uk-form-row">
                    <div>
                        <label class="uk-form-label" for="password">{{ trans('validation.attributes.frontend.new_password') }}*</label>
                    </div>
                    <div class="uk-form-controls">
                        {!! Form::input('password', 'password', null, ['id' => 'password', 'class' => 'uk-form-width-large required', 'placeholder' => trans('validation.attributes.frontend.new_password'), 'autocomplete' => 'new-password']) !!}
                    </div>
                </div>

                <div class="uk-form-row">
                    <div>
                        <label class="uk-form-label" for="password_confirmation">Подтвердите*</label>
                    </div>
                    <div class="uk-form-controls">
                        {!! Form::input('password', 'password_confirmation', null, ['id' => 'password_confirmation', 'class' => 'uk-form-width-large required', 'placeholder' => trans('validation.attributes.frontend.password_confirmation')]) !!}
                    </div>
                </div>

                <div class="separator"></div>

                <div class="uk-margin-top uk-form-row">
                    <button type="submit" class="action-btn">Сохранить</button>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
<div class="auth reg-form">
    @include('frontend.includes.logo')

    <div>
        {!! Form::open(['url' => 'register', 'class' => 'uk-form ajax-form validate-form']) !!}
            <div class="uk-text-center">
                <span>Зарегистрироваться через соц.сети</span>
            </div>

            <div class="social-account uk-grid">
                @foreach($socialite_links as $link)
                    <div class="uk-width-1-3">{!! $link !!}</div>
                @endforeach
            </div>

            <div class="uk-text-center text-line">
                <span>или</span>
            </div>

            <div class="uk-form-row">
                <div class="uk-form-controls">
                    {!! Form::text('name', null, ['id' => 'name', 'class' => 'login-bg uk-form-width-large required', 'placeholder' => trans('validation.attributes.frontend.name')]) !!}
                    {!! $errors->first('name', '<p class="uk-text-danger">:message</p>') !!}
                </div>
            </div>

            <div class="uk-form-row">
                <div class="uk-form-controls">
                    {!! Form::text('email', null, ['id' => 'email', 'class' => 'email-bg uk-form-width-large email required', 'placeholder' => trans('validation.attributes.frontend.email'), 'autocomplete' => 'login']) !!}
                    {!! $errors->first('email', '<p class="uk-text-danger">:message</p>') !!}
                </div>
            </div>

            <div class="uk-form-row">
                <div class="uk-form-controls">
                    {!! Form::password('password', ['id' => 'password', 'class' => 'password-bg uk-form-width-large required', 'placeholder' => trans('validation.attributes.frontend.password'), 'autocomplete' => 'new-password']) !!}
                    {!! $errors->first('password', '<p class="uk-text-danger">:message</p>') !!}
                </div>
            </div>

            <div class="uk-form-row">
                <div class="uk-form-controls">
                    {!! Form::password('password_confirmation', ['id' => 'password_confirmation', 'class' => 'password-bg uk-form-width-large required', 'placeholder' => trans('validation.attributes.frontend.password_confirmation')]) !!}
                    {!! $errors->first('password_confirmation', '<p class="uk-text-danger">:message</p>') !!}
                </div>
            </div>

            <div class="uk-form-row uk-margin-top">
                <div class="uk-form-controls">
                    <label>{!! Form::checkbox('accept', 1, true, ['id' => 'accept', 'class' => 'required']) !!} Я принимаю политику <a href="#" class="muted-link">конфеденциальности</a>.</label>
                </div>
            </div>

            <div>
                <button type="submit" class="uk-button uk-button-hover">Создать аккаунт #Yalpi</button>
            </div>

            <div class="uk-text-center">
                Уже есть аккаунт? {!! link_to_route('auth.login', 'Войти', null, ['class' => 'btn-popup']) !!}
            </div>

        {!! Form::close() !!}
    </div>
</div> 
@extends('mobile.layouts.master')

@section('content')
    <div class="uk-container uk-container-center" style="max-width: 460px">
    	@include('frontend.auth.login_form')
    </div>
@endsection
@extends('mobile.layouts.master')

@include('frontend.includes.seometa_group', ['group' => 'homepage'])

@section('body-attr', 'class="simple-theme"')

@section('content')
	<div class="about-block">
	    <div class="uk-container">
			<div class="title">
				Мастер-классы на любые темы!
			</div>
			<div class="desc">Более 10 000 занятий от лучших специалистов на разных языках</div>
	        @include('mobile.includes.header.search')
	    </div>
	</div>
    
    @widget('contentTypeWidget', ['id' => 5, 'layout' => 'mobile.slider'])

	<div class="uk-container uk-container-center">
        @widget('menuWidget', ['menu' => 'catalog-homepage', 'layout' => 'catalog-homepage-mob'])

        @include('mobile.includes.advantages')

        @widget('newsWidget', ['limit' => 4, 'layout' => 'mobile'])

        @include('mobile.includes.about')
    </div>
@endsection

@push('js-footer')
{!! Html::script('/js/plugin/owl/owl.carousel.min.js') !!}

{!! Html::style('/js/plugin/owl/assets/owl.carousel.min.css') !!}
{!! Html::style('/js/plugin/owl/assets/owl.theme.default.min.css') !!}
@endpush
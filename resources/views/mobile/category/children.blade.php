@if(view()->exists('mobile.category.partials.children.'.$category->child_template))
    @include('mobile.category.partials.children.'.$category->child_template)
@else
    @include('mobile.category.partials.children.default')
@endif
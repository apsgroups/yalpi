@extends('mobile.layouts.master')

@section('title', $meta->title().($pagination->currentPage() > 1 ? ' | Страница '.$pagination->currentPage() : ''))

@include('frontend.includes.seometa', ['model' => $category])

@include('frontend.category.partials.opengraph')

@section('breadcrumbs', Breadcrumbs::render('frontend.category', $category))

@section('body-attr', 'class="grey-bg"')

@section('content')
<div class="heading-block">
    <div class="category-heading-inner">
        <h1 class="page-heading category-heading">
            {!! $meta->heading() !!}
        </h1>
    </div>
</div>

<?php
$deepLvl = isset($category) && $category->parent && $category->parent->parent_id > 0;
?>

    <div class="uk-container uk-container-center">
        {!! Widgets::position('category-text') !!}

        @if($pagination->currentPage() === 1)
            @widget('menuWidget', ['menu' => 'catalog-homepage', 'layout' => 'catalog-children-mob', 'product_count' => true])
        @endif

        @include('frontend.category.partials.moneyback')

        @include('mobile.category.partials.filters.modal.index')

        @if(!$category->hide_products)
        <div id="filter-results" class="block-area margin">
            @include('mobile.category.partials.products', ['itemPropUrl' => $category->link()])
        </div>
        @endif
    </div>

    <div class="white-bg">
        <div class="uk-container uk-container-center category-advantages">
            @include('mobile.includes.advantages', ['title' => 'Преимущества он-лайн обучения в Yalpi'])
        </div>
    </div>

    <div class="uk-container uk-container-center">
        @if(isset($meta) && !empty($pagination) && $pagination->currentPage() === 1 && !empty($category))
        <div class="loadmore-hide">
            @if(!empty($category) && $category->info_enabled)
                @if($videos = $category->getInfoVids())
                    @include('frontend.category.partials.info.video')
                @endif
                
                @if($articles = $category->getInfoContent())
                    @include('frontend.category.partials.info.articles')
                @endif

                @if($faq = $category->getInfoContent('faq'))
                    @include('frontend.category.partials.info.faq')
                @endif

                @if(!empty($category->info_reviews))
                    @include('mobile.category.partials.info.reviews')
                @endif

                <div class="block-desc">{!! resize_text_img($meta->text()) !!}</div>
            @else
                <div class="block-desc">
                    {!! resize_text_img($meta->text()) !!}
                </div>
            @endif
        </div>
        @endif
    </div>
@stop

@if($pagination->currentPage() > 1)
@push('head')
<link rel="canonical" href="{{ $category->link(true) }}" />
@endpush
@endif

@push('js-footer')
{!! Html::script('/js/plugin/sly/sly.min.js') !!}
{!! Html::script('/js/plugin/owl/owl.carousel.min.js') !!}

{!! Html::style('/js/plugin/owl/assets/owl.carousel.min.css') !!}
{!! Html::style('/js/plugin/owl/assets/owl.theme.default.min.css') !!}
@endpush
@extends('mobile.layouts.master')

@include('frontend.includes.seometa_group', ['group' => 'catalog'])

@section('content')
	<div class="content-title noMarg margTop">
	    <span>Каталог товаров</span>
	</div>

	<div class="catalog-menu homeCategories bigMarg">
	    <ul class="nav nav-pills nav-stacked menu-uppercase catalog-menu" id="catalog-menu">
	        @widget('menuWidget', ['menu' => 'mobile-catalog', 'layout' => 'offcanvas', 'menu_html_id' => 'catalog-menu'])
	    </ul>
	</div>

    @widget('Product.PromoWidget', ['layout' => 'mobile.catalog', 'limit' => 4])
@stop
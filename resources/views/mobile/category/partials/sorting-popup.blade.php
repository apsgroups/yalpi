@if($sortUrls)
<?php
    $names = [
        'popular' => 'Сортировать по',
        'lower-price' => 'Дешевле',
        'higher-price' => 'Дороже',
    ];

    $slug = $sortingService->getAttribute();

    $name = isset($names[$slug]) ? $names[$slug] : '';
?>
<!--noindex-->
<div>
    <a href="#" id="select-sorting" class="sort-btn sort-by-{{ $slug }}">
        <span>{{ $name }}</span>
    </a>

    <div id="sort-links-modal">
        <div class="inner">
            <div class="header">
                <span>Сортировка</span>
                <a href="#" id="sort-links-close"></a>
            </div>
            <ul id="sort-links">
                @foreach($names as $k => $v)
                <li>
                    <a href="{{ $sortUrls[$k] }}" class="sort-by-{{ $k }} {{ ($sortingService->isActive($k) ? 'active' : '') }}">
                        <span>{{ $v }}</span>
                    </a>
                </li>
                @endforeach
            </ul>
        </div>
    </div>
</div>
<!--/noindex-->
@endif
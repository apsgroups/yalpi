<div class="category-helper">
	<div class="text sm">Не уверен, какой урок выбрать?<br>Помогу подобрать.</div>
	
	@if(request('filter') || count($products))
	<div class="uk-clearfix filter" id="filter-box">
		{!! Form::open(['url' => $filterUrl, 'class' => 'uk-form-horizontal', 'id' => 'filter-form', 'method' => 'get']) !!}
			@foreach($options as $name => $option)
				@include('frontend.category.partials.filters.helper._list')
	        @endforeach
		{!! Form::close() !!}

    	<div class="filter-loader"></div>
	</div>
	@endif
</div>
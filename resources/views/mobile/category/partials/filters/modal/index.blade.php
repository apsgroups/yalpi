@if((request('filter') || count($products)))
<div id="filter-modal" class="uk-offcanvas">
    {!! Form::open(['url' => $filterUrl, 'id' => 'filter-form-modal', 'method' => 'get']) !!}
        <div class="uk-offcanvas-bar">
            <div class="offcanvas-header">
                Фильтры 
                <a href="#" class="uk-offcanvas-close"></a>
            </div>

            <div id="filter-reset-row">
                <span id="filter-reset-lg" class="filter-reset-btn">Очистить всё</span>
            </div>
            
            <div id="filter-popup">
                <div class="filter-body">
                    @if(request('filter') || count($products))
                        <?php $buttons = ['ext' => ($category->filter_button ? $category->filter_button : 'Что изучаем?'), 'teaching' => 'Профессия', 'rate' => 'Рейтинг']; ?>
                        
                        @foreach($buttons as $btnId => $btnText)
                        <div class="option-row option-group" data-id="{{ $btnId }}">
                            <div class="option-title">{{ $btnText }}</div>
                            <div class="option-body">
                                @if($btnId == 'ext')
                                    <div id="primary-property"></div>
                                    <div id="secondary-property"></div>
                                    <div id="ext-property"></div>
                                @elseif($btnId === 'teaching')
                                    <div id="teaching-property"></div>
                                    <div id="language-property"></div>
                                @elseif($btnId === 'rate')
                                    <div id="rate-property"></div>
                                @endif
                            </div>
                        </div>
                        @endforeach
                    @endif

                    {!! Form::hidden('category_id', $category->id, ['id' => 'filter-category-id']) !!}
                </div>
            </div>
        </div>

        <div id="filter-show-results-holder">
            <button type="submit" id="filter-total-text" class="uk-button-action uk-button uk-width-1-1"></button>
        </div>
    {!! Form::close() !!}
</div>
@endif
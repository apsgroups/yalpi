@inject('listProperty', '\App\Services\Filter\Html\ListProperty')

@if((empty($options[$name]['buckets']) || count($options[$name]['buckets']) <= 1) && empty($showSingle))
<?php return ?>
@endif

@extends('mobile.category.partials.filters.modal.option')

@section('option-title-'.$name, $title)

@section('option-form-'.$name)
    <?php
    $propertyTitle = isset($property_title) ? $property_title : 'title';
    ?>

    @foreach($options[$name]['buckets'] as $k => $option)
        @php($id = $name.'_'.$k)
        @php($attr = ['class' => 'checkbox', 'id' => 'f_'.$id])
        @php($image = $listProperty->image($option['key']))

        @if($image)
            <div class="checkbox-lbl">
                {!! Form::checkbox('filter['.$name.'][]', $option['key'], (isset($attributes[$name]) && in_array($option['key'], $attributes[$name])), $attr) !!}

                <label for="f_{{ $id }}">
                    <img src="{{ img_src($image, 'color') }}" alt="{{ $listProperty->title($option['key']) }}">
                    {{ $listProperty->title($option['key'], $propertyTitle) }}
                </label>
            </div>
        @else
            <div class="checkbox-lbl">
                {!! Form::checkbox('filter['.$name.'][]', $option['key'], (isset($attributes[$name]) && in_array($option['key'], $attributes[$name])), $attr) !!}

                <label for="f_{{ $id }}">
                    {{ $listProperty->title($option['key'], $propertyTitle) }}
                </label>
            </div>
        @endif
    @endforeach
@stop
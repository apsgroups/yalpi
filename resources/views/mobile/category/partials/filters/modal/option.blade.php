<div class="option-row" data-id="{{ $name }}">
    <div class="option-title">
        @yield('option-title-'.$name)
    </div>

    <div class="option-body">
    	@yield('option-form-'.$name)
    </div>
</div>
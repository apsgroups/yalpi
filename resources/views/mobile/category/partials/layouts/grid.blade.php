﻿<div class="uk-grid products-grid product-items" data-uk-grid-match="{target:'.product-desc'}">
	@foreach($products->chunk($inRow) as $items)
      @include('mobile.category.partials.layouts.grid_products')
	@endforeach
</div>
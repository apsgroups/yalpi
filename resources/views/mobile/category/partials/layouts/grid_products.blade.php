@foreach($items as $k => $product)
    <div class="uk-width-1-4" itemprop="itemListElement" itemscope itemtype="https://schema.org/Offer">
        @include('frontend.product.preview', ['linkToModule' => isset($moduleParent), 'inRow' => $inRow, 'imgSize' => 'preview_mobile_listing2', 'favBtn' => true])
    </div>
@endforeach
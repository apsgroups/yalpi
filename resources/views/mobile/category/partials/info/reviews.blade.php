<div class="category-reviews">
    <div class="block-title">Отзывы учащихся</div>

    <div id="category-reviews" class="frame">
    	<ul class="slidee">
    	@foreach($category->info_reviews as $review)
    		@continue($review['text'] === '')

        	<li>
        		@include('mobile.category.partials.info.review')
        	</li>
    	@endforeach
    	</ul>
    </div>
</div>
@if($children->count() > 0)
    <div class="category-readmore-boxes">
        @foreach($children->chunk(3) as $k => $childs) 
            <div class="item{{ ($k === 0 ? ' active' : '') }}">
                <div class="row">
                @foreach($childs as $child) 
                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                    <div class="block-area">
                        <a href="{{ category_url($child) }}">
                            <img src="{{ img_src($child->image, 'children-readmore_mobile1') }}" alt="{{ $child->getChildTitle($category) }}">
                        </a>
                        <p class="title">
                            <a href="{{ category_url($child) }}">{{ $child->getChildTitle($category) }} <i class="fa fa-arrow-circle-right"></i></a>
                        </p>
                    </div>
                </div>
                @endforeach
                </div>
            </div>
        @endforeach
        </div>
    </div>
@endif

@push('js-libs')
<script src="/mobile/js/jquery.bcSwipe.min.js"></script>
@endpush
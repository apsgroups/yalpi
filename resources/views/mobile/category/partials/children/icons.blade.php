@if($children->count() > 0)
    <div class="container icon-categories">
        @foreach($children->chunk(2) as $group)
        <div class="row">
            @foreach($group as $child)
                <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
                    <a href="{{ category_url($child) }}" class="{!! ($child->id === $category->id ? ' active' : '') !!}">
                        <img src="/images/layout/sprite/catalog/{{ str_replace('s-icon icon-', '', $child->icon) }}{!! ($child->id === $category->id ? '-active' : '') !!}.png" alt="{{ $child->getChildTitle($category) }}" class="center-block" />

                        <span class="title">
                            {{ $child->getChildTitle($category) }}

                            @if(strpos($child->icon, 'zoom-') !== false)
                            <sup>{{ $child->products()->published(1)->count() }}</sup>
                            @endif
                        </span>
                    </a>
                </div>
            @endforeach
        </div>
        @endforeach
    </div>
@endif
@if($children->count() > 0)
    <div class="category-overlay-boxes">
        <div id="category-overlay-boxes" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner" role="listbox">
                    @foreach($children->chunk(2) as $k => $childs) 
                        <div class="item{{ ($k === 0 ? ' active' : '') }} container">
                            
                            <div class="row">
                            @foreach($childs as $child) 
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <div class="block-area">
                                    <a href="{{ category_url($child) }}">
                                        <img src="{{ img_src($child->image, 'children-overlay_mobile3') }}" alt="{{ $child->getChildTitle($category) }}">
                                    </a>
                                    <p class="title">
                                        <a href="{{ category_url($child) }}">{{ $child->getChildTitle($category) }}</a>
                                    </p>
                                </div>
                            </div>
                            @endforeach
                            </div>
                        </div>
                    @endforeach
                </div>

              <!-- Controls -->
              <a class="left carousel-control" href="#category-overlay-boxes" role="button" data-slide="prev">
                <span class="fa fa-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Назад</span>
              </a>
              <a class="right carousel-control" href="#category-overlay-boxes" role="button" data-slide="next">
                <span class="fa fa-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Вперед</span>
              </a>
        </div>
    </div>
@endif

@push('js-libs')
<script src="/mobile/js/jquery.bcSwipe.min.js"></script>
@endpush
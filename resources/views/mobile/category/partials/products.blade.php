@php($compositions = isset($compositions) ? $compositions : collect([]))

@php($filterLayout = isset($category) && strpos($category->link(), '/kukhni/') !== 0)

@if(count($products))
    <div class="sorting-nav-sm-wrapper">
        <div class="sorting-nav-sm">
            <div>
                @include('mobile.category.partials.sorting-popup')
            </div>
            <div>
                @if(isset($category) && $category->show_filter && (empty($context) || !in_array($context, ['search'])))
                <a href="#" class="open-filter filter-btn">
                    <span class="txt">Все фильтры</span>
                </a>
                @endif
            </div>
        </div>
    </div>

    <div class="uk-clearfix" id="selected-options"></div>
    
    <div class="products-grid-block" itemprop="hasOfferCatalog" itemscope itemtype="https://schema.org/OfferCatalog">
        @include('mobile.category.partials.layouts.'.$layoutService->getLayout())
    </div>

    <div class="uk-position-relative">
        @if($pagination && $pagination->hasMorePages() && $pagination->currentPage() < $pagination->lastPage())
        <div class="loadmore-block">
            @include('frontend.category.partials.loadmore')
        </div>
        @endif
    </div>

    <div class="uk-clearfix"></div>
@else
    @if(!request('filter') && request()->is('rasprodazhi*'))
    <p class="uk-text-center">
        Уважаемые клиенты! На данный момент товаров данной категории нет в распродаже. Приносим свои извинения.<br />
        Вы можете посмотреть все товары со скидкой в общем разделе <a href="/rasprodazhi/"><strong>"Распродажа"</strong></a>
    </p>
    @else
    <p class="uk-text-center">Товаров по заданным критериям нет</p>
    @endif
@endif
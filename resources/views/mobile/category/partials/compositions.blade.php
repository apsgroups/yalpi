@if($compositions->count() > 0)
    <div class="container-fuid catProds compose">
        <div class="container">
            @foreach($compositions->chunk(2) as $group)
            <div class="row">
                @foreach($group as $composition)
                <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
                    <div class="product">
                        <a href="{{ category_url($composition) }}">
                            <div class="img">
                                <img src="{{ img_src($composition->image, 'composition') }}" class="img-responsive">
                            </div>
                            <div class="title">
                                <span>{{ $composition->title }}</span>
                            </div>

                            @if(trim(strip_tags($composition->description)))
                            <div class="subtitle">
                                <span>{!! str_limit($composition->description, 150) !!}</span>
                            </div>
                            @endif
                        </a>
                        <div class="buy">
                            <a href="{{ category_url($composition) }}" class="bigBut">Все варианты</a>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            @endforeach
        </div>
    </div>
@endif
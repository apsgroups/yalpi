@extends('mobile.includes.breadcrumbs')

<?php
if(!empty($category)) {
    $categoryContract = app()->make('\App\Repositories\Frontend\Category\CategoryContract');

    $path = $categoryContract->getBreadcrumbsPath($category->id);

    $parent = count($path) > 1 ? $path[count($path) - 2] : null;
}

?>

@if(!empty($parent))
    @section('breadcrumb-title', $parent['title'])
    @section('breadcrumb-url', $parent['url'])
@endif

@if(empty($full))
    @push('breadcrumb-wrap-start')
    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 back">
    @endpush

    @push('breadcrumb-wrap-end')
    </div>
    @endpush
@endif
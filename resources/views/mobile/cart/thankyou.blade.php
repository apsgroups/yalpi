@extends('mobile.layouts.master')

@section('content')
    <!--breadcrumbs-->
    <div class="container breadcrumbs">
        @include('mobile.includes.breadcrumbs_default', ['title' => 'Вернуться к выбору товаров', 'full' => true])

        <h1>Ура! Заказ успешно оформлен.</h1>
    </div>
    <!--/breadcrumbs-->

    <div class="container-fuid">
        <div class="container">
            <p class="text-center">
                <strong>Спасибо за доверие к нашей компании</strong>
            </p>

            <p>
                Ваш заказ принят. В {!! worktime_msg() !!} мы свяжемся с Вами для уточнения деталей заказа.
            </p>
        </div>
    </div>
@endsection
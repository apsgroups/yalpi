{!! Form::open(['route' => 'cart.recalc', 'id' => 'cart-form']) !!}
    @php($num = 0)
    @php($countProd = count($products))

    <!--Buy list-->
    <div class="container buyList">
        @foreach($products as $key => $product)
            @include('mobile.cart.partials.item', ['border' => ($countProd > 1 && $num < $countProd-1)])
            @php($num++)
        @endforeach
    </div>
    <!--/Buy list-->

    <!--basket price block-->
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 prodPrice noMarg bord">
        <span class="price">
            <span class="price-act">
                <span>Итого:</span>
                <span class="sum cart-cost">{{ format_price($cart::cost()) }}</span>
                <span class="curr">р.</span>
            </span>
        </span>
    </div>
    <!--/basket price block-->
{!! Form::close() !!}
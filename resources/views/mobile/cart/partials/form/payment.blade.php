@php($paymentId = config('site.checkout.default.payment_id'))

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 content-title noMarg noBord">
    <span>Способы оплаты</span>
</div>

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 basketBoolButs">
    <button class="col-xs-6 col-sm-6 col-md-6 col-lg-6 l-Col btn {{ ($paymentId === 1 ? 'active' : '') }}" data-input="#payment_id" data-value="1">
        <span>Наличными</span>
    </button>
    <button class="col-xs-6 col-sm-6 col-md-6 col-lg-6 r-Col btn {{ ($paymentId === 2 ? 'active' : '') }}" data-input="#payment_id" data-value="2">
        <span>Картой</span>
    </button>

    {!! Form::hidden('payment_id', $paymentId, ['id' => 'payment_id']) !!}
</div>
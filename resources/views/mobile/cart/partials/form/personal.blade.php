<!--basket form-->
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 container-fuid basketForm basketForm1">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 inputContainer form-group">
        {!! Form::text('name', null, ['id' => 'name', 'class' => 'form-control required', 'placeholder' => 'Ваше имя']) !!}
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 inputContainer form-group">
        {!! Form::tel('phone', null, ['id' => 'phone', 'class' => 'form-control required phone-mask', 'placeholder' => 'Телефон']) !!}
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 inputContainer form-group">
        {!! Form::email('email', null, ['id' => 'email', 'class' => 'form-control email required', 'placeholder' => 'E-mail']) !!}
    </div>
</div>
<!--/basket form-->
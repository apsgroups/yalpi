@php($shippingId = config('site.checkout.default.shipping_id'))

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 content-title noMarg noBord">
    <span>Доставка</span>
</div>

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 basketBoolButs">
    <a class="col-xs-6 col-sm-6 col-md-6 col-lg-6 l-Col btn deliver {{ ($shippingId === 1 ? 'active' : '') }}" data-input="#shipping_id" data-value="1">
        <span>Нужна доставка</span>
    </a>
    <a class="col-xs-6 col-sm-6 col-md-6 col-lg-6 r-Col btn {{ ($shippingId === 2 ? 'active' : '') }}" data-input="#shipping_id" data-value="2">
        <span>Заберу сам</span>
    </a>

    {!! Form::hidden('shipping_id', $shippingId, ['id' => 'shipping_id']) !!}

    <div id="deliv" style="{{ ($shippingId === 1 ? '' : 'display: none') }}">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 container-fuid basketForm basketFormToggle">
            {!! Form::text('city', null, ['id' => 'city', 'placeholder' => 'Город', 'class' => 'form-control']) !!}
            {!! Form::text('street', null, ['id' => 'street', 'placeholder' => 'Улица', 'class' => 'form-control']) !!}
            {!! Form::text('home', null, ['id' => 'home', 'placeholder' => 'Дом', 'class' => 'form-control']) !!}
            {!! Form::text('apartment', null, ['id' => 'apartment', 'placeholder' => 'Квартира', 'class' => 'form-control']) !!}
            {!! Form::textarea('comment', null, ['id' => 'comment', 'rows' => 3, 'placeholder' => 'Комментарий', 'class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 custom-checkbox">
        {!! Form::checkbox('mounting', 1, false, ['id' => 'mounting']) !!}
        <label for="mounting">Нужна сборка</label>
    </div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 item {{ ($border ? 'bord' : '') }}" data-cart-product="{{ $key }}">
    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 img">
        @if($product['model']->image)
            <a href="{{ $product['model']->link() }}" target="_blank">
                <img src="{{ img_src($product['image'], 'small') }}" class="img-responsive">
            </a>
        @endif
    </div>
    <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 ttl">
        <a href="{{ $product['model']->link() }}">{{ $product['title'] }}</a>
        <div class="subTtl-group">
            <div class="subTtl">{{ $product['model']->getDimension()->getText() }}</div>

            @if(!empty($product['properties']))
                @foreach($product['properties'] as $property)
                    <div class="subTtl">{{ $property['title'] }}: {{ $property['plaintext'] }}</div>
                @endforeach
            @endif

            @if(!empty($product['attribute']))
                <div class="subTtl">{{ $product['attribute']['title'] }}: {{ $product['attribute']['value'] }}</div>
            @endif

            @if(!empty($product['desk']))
                <div class="subTtl">Столешница: <a href="{{ $product['desk']->link() }}" target="_blank">{{ $product['desk']->title }}</a></div>
            @endif
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 ttl1">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 sum">
            <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 price">{{ format_price($product['price']) }} {!! config('site.price.currency') !!}</div>
            <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 quantity-control">
                <div class="input-group number-spinner">
                    <span class="input-group-btn">
                        <button class="btn btn-default" data-dir="dwn" type="button"><span class="fa fa-minus"></span></button>
                    </span>

                    {!! Form::text('products['.$key.']', $product['amount'], ['class' => 'form-control text-center amount']) !!}

                    <span class="input-group-btn">
                        <button class="btn btn-default" data-dir="up" type="button"><span class="fa fa-plus"></span></button>
                    </span>
                </div>
            </div>
            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                <button class="btn btn-default cartDelete remove-cart" type="button" data-product="{{ $key }}"><i class="fa fa-times-circle" aria-hidden="true"></i></button>
            </div>
        </div>
    </div>
</div>
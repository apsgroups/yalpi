<tr class="product-module-row" data-cart-product="{{ $key }}" data-cart-module="{{ $product['model']->id }}">
    <td class="module-image">
        @if($product['model']->image)
            <a href="{{ $product['model']->link() }}" target="_blank">
                <img src="{{ img_src($product['image'], 'small') }}" />
            </a>
        @endif
    </td>
    <td class="module-title">
        <div>
            <a href="{{ $product['model']->link() }}" class="link-as-text hover title" target="_blank">{{ $product['title'] }}</a>
        </div>


        <div class="uk-text-muted">
            {{ $product['model']->getDimension()->getText() }}

            @if(!empty($product['properties']))
                @foreach($product['properties'] as $property)
                    <div>{{ $property['title'] }}: {{ $property['plaintext'] }}</div>
                @endforeach
            @endif
        </div>
    </td>
    <td class="center module-qtt">
        <div class="quantity-control">
            <span data-module-quantity="{{ $product['model']->id }}">{{ $product['amount'] }}</span> шт.
        </div>
    </td>
    <td class="center">
        @if($product['price'] > 0)
            <div class="big price {{ ($product['price_old'] > 0 ? 'new' : '') }} uk-margin-small">
                <span>{{ format_price($product['price']) }}</span>
                {!! config('site.price.currency') !!}
            </div>
        @endif

        @if($product['price_old'] > 0)
            <div class="uk-text-muted price old">{{ format_price($product['price_old']) }} {!! config('site.price.currency') !!}</div>
        @endif
    </td>
    <td class="center">
        <div class="big price uk-margin-small">
            <span data-module-price="{{ $product['model']->id }}">{{ format_price($product['cost']) }}</span>
            {!! config('site.price.currency') !!}
        </div>
    </td>
</tr>
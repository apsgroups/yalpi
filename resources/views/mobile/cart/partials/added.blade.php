<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="product-added-notify" id="product-added-notify">

<div class="modal-dialog" role="document">
    <div class="notify-content">
        <div class="title">Товар добавлен в корзину</div>

{{--        @if($product['model']->image)--}}
        {{--<div class="product-img">--}}
            {{--<img @src($product['model']->image, 'preview_mobile') class="img-responsive" alt="{{ $product['model']->title }}">--}}
        {{--</div>--}}
        {{--@endif--}}

        <div class="product-title">{{ $product['title'] }}</div>

        @if($product['price'] > 0)
            <div class="product-price">{{ format_price($product['price']) }} р.</div>
        @endif

        <a href="{{ route('cart.index') }}" class="bigBut yellow">Оформить заказ</a>
    </div>
</div>
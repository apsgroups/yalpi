{!! Form::open(['method' => 'post', 'route' => 'cart.checkout', 'id' => 'checkout-form']) !!}
    @include('mobile.cart.partials.form.personal')
    @include('mobile.cart.partials.form.payment')
    @include('mobile.cart.partials.form.shipping')

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 basketBoolButs" style="margin-bottom: 0">
    	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 custom-checkbox accept-lbl form-group" style="margin-top: 0">
		    {!! Form::checkbox('accept', 1, 1, ['id' => 'accept', 'class' => 'required']) !!}
		    <label for="accept">Нажимая кнопку «Оформить заказ» вы даете <a href="/uploads/media/polozhenie-o-konfidencialnosti-personalnyh-dannyh.pdf">согласие на обработку персональных данных</a>.</label>
		</div>
    </div>

    @include('mobile.cart.partials.form.submit')
    @include('mobile.cart.partials.form.timer')
{!! Form::close() !!}
@extends('mobile.layouts.master')

@include('frontend.includes.seometa_group', ['group' => 'cart'])

@section('content')
    <!--breadcrumbs-->
    <div class="container breadcrumbs">
        @include('mobile.includes.breadcrumbs_default', ['title' => 'Вернуться к выбору товаров', 'full' => true])

        <h1>Корзина</h1>
    </div>
    <!--/breadcrumbs-->

    <div class="cart-page">
        @if($cart::amount() > 0)
            @include('mobile.cart.partials.items')

            @include('mobile.cart.partials.checkout')
        @else
            <div class="text-center homeInfo">
                <p>
                    В Вашей корзине <b>нет товаров!</b>
                </p>
                <p class="wrap">
                    <a href="/" class="bigBut ico yellow marg">В каталог</a>
                </p>
            </div>
        @endif
    </div>
@endsection

@section('feedback-off', 1)
@section('subscribe-off', 1)

@push('after-styles')
<link href="/mobile/css/mob_filter.css" rel="stylesheet">
<link href="/mobile/css/mob_card.css" rel="stylesheet">
<link href="/mobile/css/mob_basket.css" rel="stylesheet">
@endpush

@push('js-scripts')
<script src="/mobile/js/minicart.js"></script>
<script src="/mobile/js/cart.js?v1"></script>
@endpush

@if($cart::amount() > 0)
@section('remarketing-data')
    <script type="text/javascript">
        var google_tag_params = {
            ecomm_prodid: ["{!! collect($products)->implode('product_id', '", "') !!}"],
            ecomm_pagetype: 'cart',
            ecomm_totalvalue: {{ $cart::cost() }},
        };
    </script>
@endsection
@endif
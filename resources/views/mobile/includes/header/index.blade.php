<div class="logo-box">
    @if(!request()->is('/'))
        <a href="{{ route('frontend.index') }}" class="logo-link">
    @endif
            <span class="logo"></span>
    @if(!request()->is('/'))
        </a>
    @endif

    <ul class="userbar">
        @if(Auth::check())
            <li>
                <div id="notifications" class="uk-button-dropdown" data-uk-dropdown="{pos:'bottom-center', mode:'click'}">
                    <button class="uk-button">
                        <img src="/images/mobile/userbar/comments.svg" alt="Комментарии" title="Комментарии">
                        @php($notifies = auth()->user()->notify_count)
                        <span class="count{{ ($notifies > 0 ? ' active' : '') }}">{{ $notifies }}</span>
                    </button>

                    <div class="uk-dropdown" id="notifications-content">
                    </div>
                </div>
            </li>
            <li id="search-nav-btn">
            	<a href="#search-modal" data-uk-offcanvas="{'mode': 'stay'}">
                    <img src="/images/mobile/userbar/search.svg" alt="Поиск" title="Поиск">
                </a>
            </li>
            <li>
                <a href="{{ route('frontend.user.dashboard') }}">
                    <img src="/images/mobile/userbar/user.svg" alt="Мой аккаунт" title="Мой аккаунт">
                </a>
            </li>
            <li class="menu-btn">
            	<a href="#menu-modal" data-uk-offcanvas="{'mode': 'stay'}">
            		<img src="/images/mobile/userbar/menu.svg" alt="Меню" title="Меню">
            	</a>
            </li>
        @else
            <li>
                @if(App::getLocale() === 'ru')
                <a href="{{ route('frontend.english') }}">English</a>
                @else
                <a href="{{ route('frontend.index') }}">Русский</a>
                @endif
            </li>
        @endif
    </ul>
</div>
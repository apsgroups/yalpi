{!! Form::open(['route' => 'search.index', 'class' => 'uk-form search-form', 'method' => 'get', 'id' => 'search-holder']) !!}
    {!! Form::text('term', null, ['placeholder' => 'Что ищем?', 'autocomplete' => 'off']) !!}
    <button type="submit"></button>
{!! Form::close() !!}
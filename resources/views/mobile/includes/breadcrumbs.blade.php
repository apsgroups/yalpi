@stack('breadcrumb-wrap-start')
    <a href="@yield('breadcrumb-url', '/')">
        <i class="fa fa-chevron-left" aria-hidden="true"></i>
        <span class="txt">@yield('breadcrumb-title', 'На главную')</span>
    </a>
@stack('breadcrumb-wrap-end')
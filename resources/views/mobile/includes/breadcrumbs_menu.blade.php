<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 fastCat dropdown">
    <?php
    $active = 'Категории';
    ?>

    <button class="btn btn-default" type="button" id="drop-catalog-menu-btn">
        <span class="txt">
            {{ $active }}
        </span>
        <span class="caret"></span>
    </button>
</div>
<div style="clear: both"></div>
<div class="catalog-menu">
	<ul class="nav nav-pills nav-stacked menu-uppercase catalog-menu" id="drop-catalog-menu">
	    @widget('menuWidget', ['menu' => 'mobile-catalog', 'layout' => 'offcanvas', 'menu_html_id' => 'drop-catalog-menu'])
	</ul>
</div>
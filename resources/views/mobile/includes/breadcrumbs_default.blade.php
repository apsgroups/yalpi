@extends('mobile.includes.breadcrumbs')

@if(isset($parent))
    @section('breadcrumb-title', str_limit($parent->title, 40))
    @section('breadcrumb-url', $parent->link())
@endif

@if(isset($title))
    @section('breadcrumb-title', $title)
@endif

@if(empty($full))
    @push('breadcrumb-wrap-start')
    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 back">
        @endpush

        @push('breadcrumb-wrap-end')
    </div>
    @endpush
@endif
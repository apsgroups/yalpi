<noindex>
    <div class="block">
        <div class="block-title">{{ (isset($title) ? $title : 'Преимущества обучения YaLpi') }}</div>

        <?php
        if(empty($items)) {
            $items = [
                ['1', 'Учитесь быстро!', 'Обучайтесь в формате небольших мастер-классов и интенсивов отпреподавателей со всего мира.'],
                ['2', 'Ищите практиков', 'Рейтинги, отзывы и подборки помогут определить тех преподавателей, у которых хочется учиться.'],
                ['3', 'Будьте в тренде!', 'Следите за актуальными темами в вашей профессии, добавляйте уроки в избранное и учитесь в своем темпе.']
            ];
        }
        ?>
        <div class="img-triggers">
            @foreach($items as $v)
                <div class="item item-{{ $v[0] }}">
                    <div class="img"><img src="/images/layout/advantages/{{ $v[0] }}.png" alt="{{ $v[1] }}"></div>
                    <div class="text">
                        <div class="title">{{ $v[1] }}</div>
                        <div class="desc">{{ $v[2] }}</div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</noindex>
@if($pagination && $pagination->hasMorePages() && $pagination->currentPage() < $pagination->lastPage())
    <button data-loading-text="<i class='fa fa-spinner fa-spin'></i>" data-href="{{ $pagination->nextPageUrl() }}" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 bigBut yellow ico noMarg loadmore">
        Показать еще<i class="fa fa-plus-circle" aria-hidden="true"></i>
    </button>
@endif
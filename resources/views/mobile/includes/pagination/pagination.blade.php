@if($pagination->lastPage() > 1)
<div class="wrap">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 pagiXS">
        @if($pagination->currentPage() > 1)
            <a href="{{ $pagination->previousPageUrl() }}" class="but back"><i class="fa fa-chevron-left" aria-hidden="true"></i>Назад</a>
        @endif

        @if($pagination->hasMorePages())
            <a href="{{ $pagination->nextPageUrl() }}" class="but forw">Вперед<i class="fa fa-chevron-right" aria-hidden="true"></i></a>
        @endif
    </div>
</div>

<nav>
    <?php
    $current = $pagination->currentPage();
    $total = $pagination->lastPage();

    $start = $current === 1 ? 1 : $current - 1;

    $to = $current + 1;

    $longEnd = $total > 4 && $current >= $total - 1;

    if($longEnd) {
        $start = $total - 1;
        $to = $total;
    } elseif($total <= 4) {
        $start = 1;
        $to = $total;
    }
    ?>
    <ul class="pagination">
        @if($current > 1)
        <li>
            <a href="{{ $pagination->previousPageUrl() }}">
                <span aria-hidden="true"><i class="fa fa-chevron-left" aria-hidden="true"></i></span>
            </a>
        </li>
        @else
            <li class="disabled">
                <span aria-hidden="true"><i class="fa fa-chevron-left" aria-hidden="true"></i></span>
            </li>
        @endif

        @if($longEnd)
            @include('mobile.includes.pagination.pagination_item', ['active' => false, 'page' => 1])

            <li class="disabled"><span>...</span></li>
        @endif

        @for($i = $start; $i <= $to; $i++)
            @include('mobile.includes.pagination.pagination_item', ['active' => ($current === $i), 'page' => $i])
        @endfor

        @if($total > 4 && $current < $total - 1)
            <li class="disabled"><span>...</span></li>
        @endif

        @if($total > 4 && $current == 1)
            @include('mobile.includes.pagination.pagination_item', ['active' => false, 'page' => $total])
        @endif

        @if($current < $total)
        <li>
            <a href="{{ $pagination->nextPageUrl() }}">
                <span aria-hidden="true"><i class="fa fa-chevron-right" aria-hidden="true"></i></span>
            </a>
        </li>
        @else
            <li class="disabled">
                <span aria-hidden="true"><i class="fa fa-chevron-right" aria-hidden="true"></i></span>
            </li>
        @endif
    </ul>
</nav>
@endif
<li {!! ($active ? ' class="active"' : '') !!}>
    @if($active)
        <span>{{ $page }}</span>
    @else
        <a href="{{ $pagination->url($page) }}">{{ $page }}</a>
    @endif
</li>
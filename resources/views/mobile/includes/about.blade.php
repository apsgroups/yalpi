<div class="block">
    <div class="block-title">
    	YaLpi сервис
    	<a href="#" class="readmore">Подробнее</a>
    </div>

    <?php 
        $items = [
            ['teaching', 'Обучение сотрудников', 'Мы составим единую подборку от лучших преподавателей в СНГ'],
            ['list', 'Подбор уникальных уроков', 'Команда YaLpi персонализирует программу обучения для Вас!'],
            ['top', 'Продвижения уроков', 'На правах рекламы Вы можете продвинуть свой топ выдачи YaLpi']
        ];
    ?>
    <div class="img-triggers">
        @foreach($items as $v)
        <div class="item">
            <div class="img"><img src="/uploads/about/{{ $v[0] }}.svg" alt="{{ $v[1] }}"></div>
            <div class="text">
                <div class="title">{{ $v[1] }}</div>
                <div class="desc">{{ $v[2] }}</div>
            </div>
        </div>
        @endforeach
    </div>
</div>
<div class="uk-container uk-container-center uk-clearfix">
    @include('frontend.includes.header.social')

    <div class="links">
        <div class="small-text">
            <span class="oferta">
                {!! link_to('/informatsiya-dlya-pokupatelya/', 'Сайт не является договором оферты') !!}
            </span>  
        
            <span class="pay">
                Оплата Viza, Mastercard
            </span>

            <span class="copyright">
                © {{ date('Y') }}, {{ Settings::get('title') }}.
            </span>
        </div>
    </div>
</div>
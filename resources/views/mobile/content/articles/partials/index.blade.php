<div class="container-fuid catProds newsBlock">
    <div class="container">
        @if(!empty($contents))
            <div id="product-grid">
                @include('mobile.content.articles.partials.items')
            </div>
        @else
            <p class="text-center">Статей нет</p>
        @endif
    </div>
</div>

@if(!empty($contents))
    <!--pagination-->
    <div class="container-fluid pagi">
        <div class="wrap">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 separate"></div>

            <div class="loadmore-block">
                @include('mobile.includes.pagination.loadmore')
            </div>
        </div>

        <div class="pagination-box">
            @include('mobile.includes.pagination.pagination')
        </div>
    </div>
    <!--/pagination-->
@endif
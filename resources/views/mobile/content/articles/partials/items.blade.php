@foreach($contents as $k => $content)
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 news {{ ($k >= $contents->count() - 2 ? 'noBordnoMarg' : '') }}">
        <div class="title">{{ $content->title }}</div>

        <div class="img">
            <a href="{{ content_url($content) }}">
                <img @src($content->image, 'article_mobile') alt="{{ $content->title }}" class="img-responsive" />
            </a>

            <div class="date">{{ $content->created_at->format('d.m.Y') }}</div>
        </div>

        <div class="desc">
            <p>{!! $content->getIntro() !!}</p>
        </div>

        <div class="link">
            <a href="{{ content_url($content) }}" class="bigBut">Подробнее</a>
        </div>
    </div>
@endforeach
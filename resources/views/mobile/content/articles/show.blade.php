@push('head')
	@if($prev)
	<link rel="prev" href="{{ $prev->link() }}">
	@endif

	@if($next)
	<link rel="next" href="{{ $next->link() }}">
	@endif
@endpush

@extends('mobile.layouts.master')

@include('frontend.includes.seometa', ['model' => $content])

@include('frontend.content.articles.opengraph_show')

@section('content')
	<div class="container breadcrumbs">
        @include('mobile.category.partials.breadcrumbs', ['parent' => $content->type])
        @include('mobile.includes.breadcrumbs_menu')

		<h1>{!! $content->title !!}</h1>
	</div>

    <div class="container catProds newsBlock marg infoPage">
        <div class="container container2 article-content homeInfo hItxt servBlock">
            @if($content->more_mobile)
                {!! $content->more_mobile !!}
            @else
                {!! category_mobile_description($content->more) !!}
            @endif
        </div>
    </div>
@endsection

@push('after-styles')
<link href="/mobile/css/mob_info_service.css" rel="stylesheet">
<link href="/mobile/css/mob_category_products.css" rel="stylesheet">
@endpush
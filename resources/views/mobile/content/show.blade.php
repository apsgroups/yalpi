@push('head')
	@if($prev)
	<link rel="prev" href="{{ $prev->link() }}">
	@endif

	@if($next)
	<link rel="next" href="{{ $next->link() }}">
	@endif
@endpush

@extends('mobile.layouts.master')

@include('frontend.includes.seometa', ['model' => $content])

@include('frontend.content.articles.opengraph_show')

@section('breadcrumbs', Breadcrumbs::render('frontend.content.show', $content))

@if(!Auth::check())
	@section('body-attr', 'class="simple-theme"')
@endif

@section('content')
<div class="blog blog-page uk-container uk-container-center" style="padding-top: 0">
	<article class="uk-article" itemscope itemtype="https://schema.org/Article">
	    <div class="content-inner">
	    	<h1 class="header">{{ $content->title }}</h1>
	    </div>

	    @if($content->image)
	        <a href="{{ content_url($content) }}">
	            <img src="{{ img_src($content->image, 'blog') }}" align="{{ $content->title }}" title="{{ $content->title }}" class="lead-img" />
	        </a>
	    @endif

	    <div itemprop="aggregateRating" itemscope="itemscope" itemtype="https://schema.org/AggregateRating">
	    	<meta itemprop="ratingValue" content="5" />
	    	<meta itemprop="bestRating" content="5" />
	    	<meta itemprop="ratingCount" content="100" />
	    </div>
	    
	    <meta itemprop="author" content="Yalpi.org" />
	    <meta itemprop="datePublished" content="{{ $content->created_at->toDateTimeString() }}" />
	    <meta itemprop="headline" content="{{ $content->title }}" />

	    @if($content->image)
	    <meta itemprop="image" content="{{ url(img_src($content->image)) }}" />
	    @endif

	    <div class="content-inner" itemprop="articleBody" itemscope itemtype="https://schema.org/articleBody">
		    <div class="item-text">
				@if($content->more_mobile)
					{!! $content->more_mobile !!}
				@else
					{!! $content->more !!}
				@endif
		    </div>

		    <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
		    	<meta itemprop="name" content="Yalpi.ru" />
		    	<a href="{{ url('/') }}" itemprop="url">&nbsp;</a>
		    	<meta itemprop="logo" content="{{ url('/images/layout/sprite/logo.png') }}">
		    	<meta itemprop="telephone" content="" />

		    	<div itemprop="address" itemscope itemtype="https://schema.org/PostalAddress">
				    <meta itemprop="streetAddress" content="">
				    <meta itemprop="addressLocality" content="">
				    <meta itemprop="addressRegion" content="">
				    <meta itemprop="postalCode" content="">
				 </div>
		    </div>

			<div class="uk-clearfix">
	            <div class="uk-float-right">
	                <a href="{{ content_url($content) }}#commento" data-custom-comments-text='comments_count' class="comments-count"></a>

	                <div class="share-btn">
	                    @include('frontend.content.blog.partials.social', ['content' => $content])
	                </div>                
	            </div>
			</div>
	    </div>
	</article>
</div>    
@endsection
@php($popup = 'onclick="javascript:window.open(this.href, \'\', \'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600\');return false;"')

<!-- noindex -->
<a href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode($content->link(true)) }}&t={{ urlencode($content->title) }}" class="uk-icon-facebook" rel="nofollow" {!! $popup !!} target="_blank"></a>
<a href="https://vk.com/share.php?url={{ urlencode($content->link(true)) }}" {!! $popup !!} target="_blank" class="uk-icon-vk" rel="nofollow" target="_blank"></a>
<a href="https://twitter.com/share?url={{ urlencode($content->link(true)) }}&text={{ urlencode($content->title) }}" {!! $popup !!} class="uk-icon-twitter" rel="nofollow" target="_blank"></a>
<!-- /noindex -->
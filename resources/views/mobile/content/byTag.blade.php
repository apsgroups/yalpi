@extends('frontend.layouts.master')

@section('content')
    <div class="blog uk-container uk-container-center">
    	<div class="content-inner">
    		<div class="header">Статьи по тегу: {!! $tag->name !!}</div>
    	</div>

		@if(!empty($contents))
		    @include('frontend.content.blog.partials.items')

            <div class="pagination-inner uk-clearfix">
            	<div class="uk-text-center">
            		<a href="{{ $contents->nextPageUrl() }}" id="loadmore">Показать больше</a>
            	</div>

		        {!! (new App\Pagination($contents))->render() !!}
		    </div>
		@else
		    <p class="uk-text-center">Статей нет</p>
		@endif
    </div>
@endsection
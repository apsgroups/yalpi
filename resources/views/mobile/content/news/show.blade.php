@extends('mobile.layouts.master')

@include('frontend.includes.seometa', ['model' => $content])

@section('content')
    <div class="container-fluid content">
        <!--breadcrumbs-->
        <div class="container breadcrumbs">
            @include('mobile.includes.breadcrumbs_default', ['parent' => $content->type, 'full' => true])

            <h1 id="page-heading">{{ $content->title }}</h1>
        </div>
        <!--/breadcrumbs-->

        <div class="container homeInfo noMarg hItxt">
            <div class="row noMarg">
                @if($content->image)
                    <img @src($content->image, 'news_mobile') class="img-responsive blogImg noMarg" alt="{{ $content->title }}"/>
                @endif

                @if($content->more_mobile)
                    {!! $content->more_mobile !!}
                @else
                    {!! $content->more !!}
                @endif
            </div>
        </div>

        <!--category products-->
        <div class="container-fuid catProds marg">
            <div class="container">
                @if($products->count())
                    @foreach($products->chunk(2) as $chunk)
                        @foreach($chunk as $product)
                            <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
                                @include('mobile.product.preview')
                            </div>
                        @endforeach
                    @endforeach
                @endif
            </div>
        </div>
        <!--/category products-->
    </div>
@stop

@push('after-styles')
<link href="/mobile/css/mob_category_filter.css" rel="stylesheet">
<link href="/mobile/css/mob_category_products.css" rel="stylesheet">
<link href="/mobile/css/mob_filter.css" rel="stylesheet">
@endpush
@foreach($contents->chunk(2) as $chank)
    <div class="row">
    @foreach($chank as $content)
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 news news2">
            <div class="inner">
                <div class="title">
                    {{ $content->title }}
                </div>

                <div class="img">
                    <a href="{{ content_url($content) }}">
                        <img @src($content->image, 'news_preview_mobile') alt="{{ $content->title }}" />
                    </a>
                    <div class="date">{{ $content->created_at->format('d.m.Y') }}</div>
                </div>

                <div class="desc">
                    <p>{!! $content->getIntro() !!}</p>
                </div>

                <div class="container">
                    <div class="row">
                        @if($products[$content->id])
                            @foreach($products[$content->id] as $product)
                                <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
                                    @include('mobile.product.preview')
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>

                <div class="link link2">
                    <a href="{{ content_url($content) }}" class="bigBut">Подробнее</a>
                </div>
            </div>
        </div>
    @endforeach
    </div>
@endforeach
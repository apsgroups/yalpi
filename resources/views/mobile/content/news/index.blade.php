@extends('mobile.layouts.master')

@include('frontend.includes.seometa', ['model' => $contentType])

@section('content')
	<!--breadcrumbs-->
	<div class="container breadcrumbs">
		@include('mobile.includes.breadcrumbs_default', ['full' => true])

		<h1 id="page-heading">{{ $contentType->title }}</h1>
	</div>
	<!--/breadcrumbs-->

    <div id="filter-results">
        @include('mobile.content.news.partials.index')
    </div>
@endsection

@push('after-styles')
<link href="/mobile/css/mob_category_products.css" rel="stylesheet">
<link href="/mobile/css/mob_news.css" rel="stylesheet">
@endpush

@push('js-libs')
<script src="/mobile/js/ajax-setup.js"></script>
<script src="/mobile/js/ajax-pagination.js"></script>
<script src="/mobile/js/product-list.js"></script>
@endpush

@push('js-scripts')
<script src="/mobile/js/loadmore.js"></script>
@endpush
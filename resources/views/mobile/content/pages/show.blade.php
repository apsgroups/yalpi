@extends('mobile.layouts.master')

@include('frontend.includes.seometa', ['model' => $content])

@section('content')
    <div class="container breadcrumbs">
        @include('mobile.includes.breadcrumbs_default')
		@include('mobile.includes.breadcrumbs_menu')

        <h1>{!! $content->title !!}</h1>
    </div>

	@if($content->more_mobile)
		{!! $content->more_mobile !!}
	@else
		{!! $content->more !!}
	@endif
@endsection

@push('after-styles')
<link href="/mobile/css/mob_info_service.css" rel="stylesheet">
@endpush
@push('head')
	@if($prev)
	<link rel="prev" href="{{ $prev->link() }}">
	@endif

	@if($next)
	<link rel="next" href="{{ $next->link() }}">
	@endif
@endpush

@extends('mobile.layouts.master')

@include('frontend.includes.seometa', ['model' => $content])

@include('frontend.content.articles.opengraph_show')

@section('content')
    <!--breadcrumbs-->
    <div class="container breadcrumbs">
        @include('mobile.category.partials.breadcrumbs', ['parent' => ['title' => $content->type->title, 'url' => $content->type->link()]])
        @include('mobile.includes.breadcrumbs_menu')

        <h1 id="page-heading">{!! $content->title !!}</h1>
    </div>
    <!--/breadcrumbs-->

    <p>
	    <a href="{{ content_url($content) }}">
	        <img src="{{ img_src($content->image, 'promo') }}" class="img-responsive" />
	    </a>
	</p>

    <div class="container catProds newsBlock marg infoPage">
        <div class="container container2 article-content homeInfo hItxt servBlock">
            @if($content->more_mobile)
                {!! $content->more_mobile !!}
            @else
                {!! category_mobile_description($content->more) !!}
            @endif
        </div>
    </div>
@endsection
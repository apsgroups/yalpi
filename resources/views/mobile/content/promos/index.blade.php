@extends('mobile.layouts.master')

@include('frontend.includes.seometa', ['model' => $contentType])

@include('frontend.content.articles.opengraph')

@section('content')
    <!--breadcrumbs-->
    <div class="container breadcrumbs">
        @include('mobile.category.partials.breadcrumbs')
        @include('mobile.includes.breadcrumbs_menu')

        <h1 id="page-heading">{!! $contentType->title !!}</h1>
    </div>
    <!--/breadcrumbs-->

    <div style="margin-bottom: 35px">
        @if(!empty($contents))
            @foreach($contents as $content)
	            <p>
		            <a href="{{ content_url($content) }}">
		                <img src="{{ img_src($content->image, 'promo') }}" class="img-responsive" />
		            </a>
		        </p>
            @endforeach
        @else
            <p class="uk-text-center">Статей нет</p>
        @endif
    </div>
@endsection
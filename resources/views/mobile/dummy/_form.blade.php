{!! Form::open(['route' => ['feedback.consult'], 'method' => 'post', 'class' => 'validate-form ajax-form uk-form']) !!}
    <div class="uk-text-center">{{ trans('frontend.dummy.form.desc') }}</div>

    <div class="inputs uk-margin-top">
        <div class="uk-form-row">
        	{!! Form::text('name', null, ['id' => 'name', 'class' => 'required', 'placeholder' => trans('frontend.dummy.form.name')]) !!}
        </div>

        <div class="uk-form-row">
        	{!! Form::text('phone', null, ['id' => 'phone', 'class' => 'phone-mask required', 'placeholder' => trans('frontend.dummy.form.phone')]) !!}
        </div>
        
        <div class="uk-form-row uk-text-center uk-margin-bottom">
	        <button type="submit" class="uk-button action-btn">{{ trans('frontend.dummy.form.send') }}</button>
	    </div>
    </div>
{!! Form::close() !!}
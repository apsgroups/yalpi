@extends('mobile.layouts.master')

@include('frontend.includes.seometa_group', ['group' => 'homepage'])

<?php

$og = [
    'url' => url('/'),
    'title' => \Settings::getOr('homepage_page_title', 'page_title'),
    'description' => \Settings::getOr('homepage_metadesc', 'metadesc'),
    'image' => '',
    'type' => 'product',
];

?>

@section('body-attr', 'class="simple-theme"')

@section('content')
    <div class="uk-container uk-container-center">
        <div class="cosmos">
            <div class="text-inner uk-text-center uk-width-1-1 uk-margin-large-top">
                <div class="date uk-text-bold">Запуск 12 января 2020 года</div>
                <div class="title">Все уроки в СНГ на одной площадке</div>
                <div class="desc">Учитесь из любой точки вселенной</div>
            </div>
            
            <div class="uk-margin-top">
                @include('mobile.dummy._form')
            </div>
        </div>

        <div class="uk-text-center">
            <a href="{{ \Settings::get('social_instagram') }}" class="big-btn" rel="nofollow" target="_blank"><i class="uk-icon-instagram"></i> Instagram</a>
        </div>
    </div>
@endsection
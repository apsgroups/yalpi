@extends('mobile.layouts.master')

<?php
$og = [
    'url' => url('/'),
    'title' => '#YaLpi - master class from all around the world',
    'description' => \Settings::getOr('homepage_metadesc', 'metadesc'),
    'image' => '',
    'type' => 'product',
];

?>

@section('title', '#YaLpi - master class from all around the world')

@section('body-attr', 'class="simple-theme"')

@section('content')
    <div class="uk-container uk-container-center">
        <div class="cosmos">
            <div class="text-inner uk-text-center uk-width-1-1 uk-margin-large-top">
                <div class="date uk-text-bold">1 March, 2020 – the launch in Europe and the USA</div>
                <div class="title">Master class from all around the world</div>
            </div>
            
            <div class="uk-margin-top">
                @include('mobile.dummy._form')
            </div>
        </div>

        <div class="uk-text-center">
            <a href="{{ \Settings::get('social_instagram') }}" class="big-btn" rel="nofollow" target="_blank"><i class="uk-icon-instagram"></i> Instagram</a>
        </div>
    </div>
@endsection
{!! Form::open(['route' => 'search.index', 'class' => 'search-secondary uk-form', 'method' => 'get']) !!}
    <div class="uk-clearfix">
        <button type="submit" class="uk-button uk-button-primary">
            Найти
        </button>

        <div class="text-box">
            {!! Form::text('term', null, ['placeholder' => 'Введите текст для поиска']) !!}
        </div>
    </div>
{!! Form::close() !!}
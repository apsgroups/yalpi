@extends('frontend.layouts.master')

@section('title', trans('http.404.title'))

@section('after-styles-end')
    {!! Html::style(elixir('css/404.css')) !!}
@endsection

@section('content')
    <div class="uk-container uk-container-center">
        <h2>{{ trans('http.404.title') }}</h2>
        
        <p>Такой страницы нет! Вероятно, ссылка по которой вы сюда попали, устарела, или вы ошиблись, когда набирали адрес.</p>
        
        <p>Вы можете зайти на {!! link_to_route('frontend.index', 'главную страницу') !!}, или выбрать из списка справа интересующую вас рубрику.</p>
        
        <div>
            @include('errors.partials.search')
        </div>
    </div>
@endsection
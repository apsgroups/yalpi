@extends('emails.layouts.master')

@section('title', 'Logs')

@section('content')
    <div>
        Msg: {!! $e->getMessage() !!}
    </div>
    <div>
        File: {!! $e->getFile() !!}
    </div>
    <div>
        Code: {!! $e->getCode() !!}
    </div>
    <div>
        On line: {!! $e->getLine() !!}
    </div>
    <div>
        Please, check it!
    </div>
@endsection
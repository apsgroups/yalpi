<style type="text/css">
body {
  margin: 0;
  padding: 0;
  -ms-text-size-adjust: 100%;
  -webkit-text-size-adjust: 100%;
}

table {
  border-spacing: 0;
}

table td {
  border-collapse: collapse;
}

.ExternalClass {
  width: 100%;
}

.ExternalClass,
.ExternalClass p,
.ExternalClass span,
.ExternalClass font,
.ExternalClass td,
.ExternalClass div {
  line-height: 100%;
}

.ReadMsgBody {
  width: 100%;
  background-color: #ebebeb;
}

table {
  mso-table-lspace: 0pt;
  mso-table-rspace: 0pt;
}

img {
  -ms-interpolation-mode: bicubic;
}

.yshortcuts a {
  border-bottom: none !important;
}

@media screen and (max-width: 599px) {
  .force-row,
  .container {
    width: 100% !important;
    max-width: 100% !important;
  }
}
@media screen and (max-width: 400px) {
  .container-padding {
    padding-left: 12px !important;
    padding-right: 12px !important;
  }
}
.ios-footer a {
  color: #aaaaaa !important;
  text-decoration: underline;
}

.value {
    padding-left: 10px;
    padding-bottom: 5px;
}

.fieldname {
    text-align: right;
    padding-bottom: 5px;
}

a {
    color: #01a9c1;
    text-decoration: none;
}

.btn {
    color: #fff;
    display: inline-block;
    padding: 10px 15px;
    text-decoration: none;
}

.btn.success {
    background: #54b600;
}

.btn.primary {
    background: #01a9c1;
}

.order,
.order th,
.order td {
border: 1px solid #eee;
border-collapse: collapse;
}

.order th,
.order td {
padding: 10px;
}
</style>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1"> <!-- So that mobile will display zoomed in -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- enable media queries for windows phone 8 -->
  <meta name="format-detection" content="telephone=no"> <!-- disable auto telephone linking in iOS -->
  <title>@yield('title')</title>

  @include('emails.layouts.partials.style')
</head>

<body style="margin:0; padding:0;" bgcolor="#F0F0F0" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

    <!-- 100% background wrapper (grey background) -->
    <table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" bgcolor="#F0F0F0">
      <tr>
        <td align="center" valign="top" bgcolor="#F0F0F0" style="background-color: #F0F0F0;">

          <br>

          <!-- 600px container (white background) -->
          <table border="0" width="600" cellpadding="0" cellspacing="0" class="container" style="width:600px;max-width:600px">
            <tr>
              <td class="container-padding header" align="left" style="font-family:Helvetica, Arial, sans-serif;padding-right:24px;padding-bottom:24px">
                <a href="{{ url('/') }}"><img src="<?php echo $message->embed(base_path('resources/assets/images/emails/logo.png')); ?>"></a>
              </td>
              <td class="container-padding header" valign="middle" align="right" style="font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-bottom:24px">
                @include('emails.layouts.partials.contacts')
              </td>
            </tr>
            <tr>
              <td class="container-padding content" align="left" style="padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff" colspan="2">
                <br>

                <div class="title" style="font-family:Helvetica, Arial, sans-serif;font-size:18px;font-weight:600;color:#374550">@yield('title')</div>

                <br>

                <div class="body-text" style="font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333">
                  @yield('content')
                  <br><br>
                </div>

              </td>
            </tr>
            <tr>
              <td class="container-padding footer-text" align="left" style="font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px" colspan="2">
                {{--@include('emails.layouts.partials.footer')--}}
              </td>
            </tr>
          </table>
          <!--/600px container -->

        </td>
      </tr>
    </table>
    <!--/100% background wrapper-->
</body>
</html>
@extends('emails.layouts.master')

@section('title', 'Новый отзыв №'.$review->id)

@section('content')
    <div>На сайте {!! link_to('/', \Settings::get('title')) !!} добавлен новый отзыв</div>
    <br />

    <table>
        <tbody>
            <tr>
                <td class="fieldname" style="width: 100px"><b>Имя:</b></td>
                <td class="value">{{ $review->name }}</td>
            </tr>
            <tr>
                <td class="fieldname" style="width: 100px"><b>Оценка</b></td>
                <td class="value">{{ $review->rating }}</td>
            </tr>
            <tr>
                <td class="fieldname"><b>Добавлен:</b></td>
                <td class="value">{{ $review->created_at->format('d.m.Y H:i') }}</td>
            </tr>
            {{--<tr>--}}
                {{--<td class="fieldname"><b>Достоинства:</b></td>--}}
                {{--<td class="value">{{ $review->positive }}</td>--}}
            {{--</tr>--}}
            {{--<tr>--}}
                {{--<td class="fieldname"><b>Недостатки:</b></td>--}}
                {{--<td class="value">{{ $review->negative }}</td>--}}
            {{--</tr>--}}
            <tr>
                <td class="fieldname"><b>Отзыв:</b></td>
                <td class="value">{{ $review->review }}</td>
            </tr>
        </tbody>
    </table>

    <p>
        {!! link_to_route('admin.review.publish', 'Опубликовать', [$review->id], ['style' => 'width: 85px;', 'class' => 'success btn']) !!}
        {!! link_to_route('admin.review-response.create', 'Ответить', [$review->id], ['style' => 'width: 85px; margin-left: 10px', 'class' => 'primary btn']) !!}
    </p>
@endsection
@extends('emails.layouts.master')

@section('title', 'Новый отзыв №'.$review->id)

@section('content')
    <div>На сайте {!! link_to('/', \Settings::get('title')) !!} добавлен новый отзыв к уроку {{ $product->title }}</div>
    <br />

    <table>
        <tbody>
        <tr>
            <td class="fieldname" style="width: 100px"><b>Урок</b></td>
            <td class="value">{!! link_to($product->link(true), $product->title) !!}</td>
        </tr>
        <tr>
            <td class="fieldname" style="width: 100px"><b>Имя:</b></td>
            <td class="value">{{ $review->name }}</td>
        </tr>
        <tr>
            <td class="fieldname" style="width: 100px"><b>Комментарий</b></td>
            <td class="value">{{ $review->comment }}</td>
        </tr>
        <tr>
            <td class="fieldname"><b>Добавлен:</b></td>
            <td class="value">{{ $review->created_at->format('d.m.Y H:i') }}</td>
        </tr>
        </tbody>
    </table>

    <p>
        {!! link_to_route('admin.product-review.publish', 'Опубликовать', [$review->id], ['style' => 'width: 85px;', 'class' => 'success btn']) !!}
    </p>
@endsection
<h3>{{ $title }}</h3>

<table>
    <thead>
        <tr>
            <th>Наименование</th>
            <th>Старая цена</th>
            <th>Цена</th>
            <th>Скидка</th>
        </tr>
    </thead>
    <tbody>
        @foreach($products as $product)
        <tr>
            <td><a href="{{ route('admin.product.edit', $product->id) }}">{{ $product->title }}</a></td>
            <td>{{ $product->price_old }}</td>
            <td>{{ $product->price }}</td>
            <td>{{ $product->virtual_discount }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
@extends('emails.layouts.master')

@section('title', 'Отчет о виртуальных скидках')

@section('content')
    @if($added->count())
        @include('emails.virtual-discounts._products', ['products' => $added, 'title' => 'Новые скидки'])
    @endif

    @if($updated->count())
        @include('emails.virtual-discounts._products', ['products' => $updated, 'title' => 'Обновленные скидки'])
    @endif
@endsection
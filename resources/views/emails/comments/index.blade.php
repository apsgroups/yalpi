@extends('emails.layouts.master')

@section('title', 'Новый отзыв №'.$comment->id)

@section('content')
    <div>На сайте {!! link_to('/', \Settings::get('title')) !!} добавлен новый отзыв к уроку {{ $comment->commentable->title }}</div>
    <br />

    <table>
        <tbody>
        <tr>
            <td class="fieldname" style="width: 100px"><b>Урок</b></td>
            <td class="value">{!! link_to($comment->commentable->link(true), $comment->commentable->title) !!}</td>
        </tr>
        <tr>
            <td class="fieldname" style="width: 100px"><b>Имя:</b></td>
            <td class="value">{{ $comment->name }}</td>
        </tr>
        <tr>
            <td class="fieldname" style="width: 100px"><b>Комментарий</b></td>
            <td class="value">{{ $comment->comment }}</td>
        </tr>
        <tr>
            <td class="fieldname"><b>Добавлен:</b></td>
            <td class="value">{{ $comment->created_at->format('d.m.Y H:i') }}</td>
        </tr>
        </tbody>
    </table>

    <p>
        {!! link_to_route('admin.comment.publish', 'Опубликовать', [$comment->id], ['style' => 'width: 85px;', 'class' => 'success btn']) !!}
    </p>
@endsection
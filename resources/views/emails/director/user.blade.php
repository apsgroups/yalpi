@extends('emails.layouts.master')

@section('title', 'Сообщение директору №'.$feedback->id)

@section('content')
    <div>Здравствуйте, {{ $feedback->name }}!<br /><br />
        Спасибо! Ваш запрос <b>№{{ $feedback->id }}</b> принят.<br />
        Заявки обрабатываются в течении 24 часов в рабочие дни.<br />
        <br />
        Благодарим Вас за доверие к нашей компании!
    </div>
@endsection
@extends('emails.layouts.master')

@section('title', 'Вопрос в обсуждениях')

@section('content')
    <div>На сайте {!! link_to('/', \Settings::get('title')) !!} добавлен новый отзыв</div>
    <br />

    <table>
        <tbody>
            <tr>
                <td class="fieldname" style="width: 100px"><b>Имя:</b></td>
                <td class="value">{{ $discussion->name }}</td>
            </tr>
            <tr>
                <td class="fieldname"><b>Добавлен:</b></td>
                <td class="value">{{ $discussion->created_at->format('d.m.Y H:i') }}</td>
            </tr>
            <tr>
                <td class="fieldname"><b>Текст:</b></td>
                <td class="value">{{ $discussion->messsage }}</td>
            </tr>
        </tbody>
    </table>

    <p>
{{--        {!! link_to_route('admin.discussion.publish', 'Опубликовать', [$discussion->id], ['style' => 'width: 85px;', 'class' => 'success btn']) !!}--}}
{{--        {!! link_to_route('admin.discussion.create', 'Ответить', [$discussion->id], ['style' => 'width: 85px; margin-left: 10px', 'class' => 'primary btn']) !!}--}}
    </p>
@endsection
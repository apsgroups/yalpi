@extends('emails.layouts.master')

@section('title', 'Сообщение с "Задать вопрос" №'.$feedback->id)

@section('content')
    <div>На сайте <b>№{{ $feedback->id }}</b> с сайта {!! link_to('/', \Settings::get('title')) !!} было оставлено сообщение с обатной связи</div>
    <br />

    <table>
        <tbody>
            <tr>
                <td class="fieldname" style="width: 100px"><b>Имя:</b></td>
                <td class="value">{{ $feedback->name }}</td>
            </tr>
            <tr>
                <td class="fieldname"><b>E-mail:</b></td>
                <td class="value"><a href="mailto:{{ $feedback->email }}">{{ $feedback->email }}</a></td>
            </tr>
            <tr>
                <td class="fieldname"><b>Телефон:</b></td>
                <td class="value">{{ $feedback->phone }}</td>
            </tr>
            <tr>
                <td class="fieldname"><b>Сообщение:</b></td>
                <td class="value">{{ $feedback->message }}</td>
            </tr>
        </tbody>
    </table>
@endsection
@extends('emails.layouts.master')

@section('title', 'Благодарность №'.$feedback->id)

@section('content')
    <div>Оставлена благодарность на сайте {!! link_to('/', \Settings::get('title')) !!}</div>
    <br />

    <table>
        <tbody>
            <tr>
                <td class="fieldname" style="width: 100px"><b>Имя:</b></td>
                <td class="value">{{ $feedback->name }}</td>
            </tr>
            <tr>
                <td class="fieldname"><b>E-mail:</b></td>
                <td class="value"><a href="mailto:{{ $feedback->email }}">{{ $feedback->email }}</a></td>
            </tr>
            <tr>
                <td class="fieldname"><b>Номер заказа:</b></td>
                <td class="value">{{ $feedback->order_number }}</td>
            </tr>
            <tr>
                <td class="fieldname"><b>Сообщение:</b></td>
                <td class="value">{{ $feedback->message }}</td>
            </tr>
        </tbody>
    </table>
@endsection
@extends('emails.layouts.master')

@section('title', 'Заяка на обратный звонок №'.$feedback->id)

@section('content')
    <table>
        <tbody>
            <tr>
                <td class="fieldname"><b>Имя:</b></td>
                <td class="value">{{ $feedback->name }}</td>
            </tr>
            <tr>
                <td class="fieldname"><b>Телефон:</b></td>
                <td class="value">{{ $feedback->phone }}</td>
            </tr>

            @if($feedback->product)
            <tr>
                <td class="fieldname"><b>Товар:</b></td>
                <td class="value"><a href="{{ $feedback->product->link(true) }}">{{ $feedback->product->title }}</a></td>
            </tr>
            @endif
        </tbody>
    </table>
@endsection
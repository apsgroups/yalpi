@extends('emails.layouts.master')

@section('title', 'Жалоба №'.$feedback->id)

@section('content')
    <div>Поступила жалоба с сайта {!! link_to('/', \Settings::get('title')) !!}</div>
    <br />

    <table>
        <tbody>
            <tr>
                <td class="fieldname" style="width: 100px"><b>Имя:</b></td>
                <td class="value">{{ $feedback->name }}</td>
            </tr>
            <tr>
                <td class="fieldname"><b>E-mail:</b></td>
                <td class="value"><a href="mailto:{{ $feedback->email }}">{{ $feedback->email }}</a></td>
            </tr>
            <tr>
                <td class="fieldname"><b>Номер заказа:</b></td>
                <td class="value">{{ $feedback->order_number }}</td>
            </tr>
            <tr>
                <td class="fieldname"><b>Суть проблемы:</b></td>
                <td class="value">{{ $feedback->message }}</td>
            </tr>
        </tbody>
    </table>
@endsection
@extends('emails.layouts.master')

@section('title', 'Жалоба №'.$feedback->id)

@section('content')
    <div>Здравствуйте, {{ $feedback->name }}!<br /><br />
        Спасибо! Ваш запрос <b>№{{ $feedback->id }}</b> принят.<br />
        В ближайшее время с Вами свяжется наш менеджер для уточнения деталей. Заявки обрабатываются в течении 24 часов в рабочие дни.<br />
    </div>
@endsection
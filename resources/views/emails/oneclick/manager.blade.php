@extends('emails.layouts.master')

@section('title', 'Быстрый заказ №'.$feedback->id)

@section('content')
    <div>Быстрый заказ <b>№{{ $feedback->id }}</b> с сайта {!! link_to('/', \Settings::get('title')) !!}</div>
    <br />

    <table>
        <tbody>
            <tr>
                <td class="fieldname" style="width: 100px"><b>Имя:</b></td>
                <td class="value">{{ $feedback->name }}</td>
            </tr>
            <tr>
                <td class="fieldname" style="width: 100px"><b>Телефон</b></td>
                <td class="value">{{ $feedback->phone }}</td>
            </tr>
            @foreach($feedback->order->products as $product)
            <tr>
                <td class="fieldname" valign="top" style="width: 100px"><b>Товар</b></td>
                <td class="value">
                    <a href="{{ $product->product->link(true) }}">{{ $product->title }}</a>

                    @if(!$product->orderProductProperty->isEmpty())
                        <div style="font-size: 11px;">
                            {!! $product->orderProductProperty->map(function ($item) {
                            $item->content .= ': '.$item->title;
                            return $item;
                            })->implode('content', '<br />') !!}
                        </div>
                    @endif
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
@endsection
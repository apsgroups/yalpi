@extends('emails.layouts.master')

@section('title', 'Что можно улучшить на сайте №'.$feedback->id)

@section('content')
    <table>
        <tbody>
            <tr>
                <td class="fieldname"><b>Имя:</b></td>
                <td class="value">{{ $feedback->name }}</td>
            </tr>

            <tr>
                <td class="fieldname"><b>E-mail:</b></td>
                <td class="value">{{ $feedback->email }}</td>
            </tr>

            <tr>
                <td class="fieldname"><b>Сообщение:</b></td>
                <td class="value">{{ $feedback->message }}</td>
            </tr>
        </tbody>
    </table>
@endsection
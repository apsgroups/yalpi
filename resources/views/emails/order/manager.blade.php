@extends('emails.layouts.master')

@section('title', 'Заказ №'.$order->id)

@section('content')
    <div>Поступил заказ №{{ $order->id }} с сайта {!! link_to('/', \Settings::get('title')) !!}</div>
    <br />
    <table>
        <tbody>
            <tr>
                <td class="fieldname" style="width: 160px"><b>Имя:</b></td>
                <td class="value">{{ $order->name }}</td>
            </tr>
            <tr>
                <td class="fieldname"><b>Телефон:</b></td>
                <td class="value">{{ $order->phone }}</td>
            </tr>
            <tr>
                <td class="fieldname"><b>E-mail:</b></td>
                <td class="value">{{ $order->email }}</td>
            </tr>
            @if($order->city)
            <tr>
                <td class="fieldname"><b>Город:</b></td>
                <td class="value">{{ $order->city }}</td>
            </tr>
            @endif
            @if($order->street)
            <tr>
                <td class="fieldname"><b>Улица:</b></td>
                <td class="value">{{ $order->street }}</td>
            </tr>
            @endif
            @if($order->home)
            <tr>
                <td class="fieldname"><b>Номер дома:</b></td>
                <td class="value">{{ $order->home }}</td>
            </tr>
            @endif
            @if($order->apartment)
            <tr>
                <td class="fieldname"><b>Номер квартиры:</b></td>
                <td class="value">{{ $order->apartment }}</td>
            </tr>
            @endif
            @if($order->comment)
            <tr>
                <td class="fieldname"><b>Комментарий:</b></td>
                <td class="value">{{ $order->comment }}</td>
            </tr>
            @endif
            <tr>
                <td class="fieldname"><b>Нужна сборка:</b></td>
                <td class="value">{{ ($order->mounting ? 'Да' : 'Нет') }}</td>
            </tr>

            @if($order->payment)
            <tr>
                <td class="fieldname"><b>Способ оплаты:</b></td>
                <td class="value">{{ $order->payment->title }}</td>
            </tr>
            @endif

            @if($order->shipping)
            <tr>
                <td class="fieldname"><b>Способ доставки:</b></td>
                <td class="value">{{ $order->shipping->title }}</td>
            </tr>
            @endif
        </tbody>
    </table>

    <h3>Товары</h3>
    <table class="order" style="width: 100%">
        <thead>
            <tr>
                <th>Наименование</th>
                <th style="text-align: center;">Цена</th>
                <th style="text-align: center;">Количество</th>
            </tr>
        </thead>

        <tbody>
            @foreach($order->products as $product)
            <tr>
                <td>
                    <a href="{{ $product->product->link(true) }}">{{ $product->title }}</a>

                    @if(!$product->orderProductProperty->isEmpty())
                        <div style="font-size: 11px;">
                            @foreach($product->orderProductProperty as $item)
                                {{ $item->content }}: {{ $item->title }}<br />
                            @endforeach
                        </div>
                    @endif

                    @if($product->productAttribute)
                        <div style="font-size: 11px;">
                            {{ $product->attribute_title }}: {{ $product->attribute_value }}
                        </div>
                    @endif
                </td>
                <td style="text-align: center; width: 100px">{{ format_price($product->price * $product->amount) }} руб.</td>
                <td style="text-align: center; width: 30px">{{ $product->amount }}</td>
            </tr>
            @endforeach

            @if($order->coupon)
                <tr>
                    <td></td>
                    <td style="text-align: center"><b>Купон</b></td>
                    <td style="text-align: center"><b>{{ $order->coupon }}</b></td>
                </tr>
                <tr>
                    <td></td>
                    <td style="text-align: center"><b>Скидка по купону</b></td>
                    <td style="text-align: center"><b>{{ format_price($order->coupon_discount) }} руб.</b></td>
                </tr>
            @endif

            <tr>
                <td></td>
                <td style="text-align: center"><b>{{ format_price($order->total) }} руб.</b></td>
                <td style="text-align: center"><b>{{ $order->amount }}</b> ед.</td>
            </tr>
        </tbody>
    </table>
@endsection
@extends ('backend.layouts.master')

@section ('title', 'Создание категории')

@section('page-header')
    <h1>
        Добавление категории
    </h1>
@endsection

@section('content')
    @include('backend.category.partials.form')
@stop
@extends('backend.layouts.master')

@section('title', 'Категории')

@section('page-header')
    <h1>
        Управление категориями
        <small>все</small>
    </h1>
@endsection

@section('after-styles-end')
@stop

@section('content')
    <div class="clearfix"></div>

    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Категории</h3>

            <div class="box-tools pull-right">
                @include('backend.category.partials.header-buttons')
            </div>
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="tree-handle categories dd permission-hierarchy">
                <ol class="dd-list">
                    @foreach($categories as $category)
                        @include('backend.category.partials.tree-item', ['category' => $category])
                    @endforeach
                </ol>
            </div><!--master-list-->

            <div class="clearfix"></div>
        </div><!-- /.box-body -->
    </div><!--box-->

    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Инструменты</h3>
        </div><!-- /.box-header -->

        <div class="box-body">
            <ul>
            <li>{!! link_to_route('admin.tool.autosale', 'Обновить привязку распродаж') !!} - полезно при изменении категории распродаж у Типа товара.</li>
            </ul>

            <div class="clearfix"></div>
        </div><!-- /.box-body -->
    </div><!--box-->
@stop

@section('after-scripts-end')
    {!! Html::script('js/backend/plugin/nestable/jquery.nestable.js') !!}

    <script>
        (function($) {
            var hierarchy = $('.permission-hierarchy');
            hierarchy.nestable({maxDepth:5});
            hierarchy.nestable('collapseAll');

            hierarchy.on('change', function() {
                $.ajax({
                    url : '{{ route('admin.category.sort') }}',
                    type: "post",
                    data : {data:hierarchy.nestable('serialize'), '_token': $('meta[name=_token]').attr('content')},
                    success: function(data) {
                        if (data.status == "OK")
                            toastr.success("{!! trans('strings.backend.access.permissions.groups.hierarchy_saved') !!}");
                        else
                            toastr.error("{!! trans('auth.unknown') !!}.");
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        toastr.error("{!! trans('auth.unknown') !!}: " + errorThrown);
                    }
                });
            });
        })(jQuery);
    </script>
@stop
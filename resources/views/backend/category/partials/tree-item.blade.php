<li class="dd-item dd3-item" data-id="{!! $category->id !!}">
    <div class="dd-handle dd3-handle">
        <span class="glyphicon glyphicon-option-vertical"></span>
    </div>

    <div class="dd3-content">
        <small>({{ $category->product_priority }})</small>
        {!! link_to_route('admin.category.edit', $category->title, [$category->id]) !!}
        <small>({{ $category->slug }})</small>
        <small>({{ $category->id }})</small>

        <small>{!! link_to_route('admin.product.index', 'Товары ('.$category->products()->count().')', [
            'filter' => ['category_id' => $category->id]]) !!}</small>

        <div class="item-actions">
            <?php
            $buttons = [
                'published' => 'Публикация',
                //'at_home' => 'На главной',
                //'composition' => 'Композиция',
                'model' => 'Модель',
                //'show_as_child' => 'Отображать в родительской',
                //'search_form' => 'Отображать в форме поиска',
                //'furniture_catalog' => 'Каталог мебели',
                //'furniture_objects' => 'Предметы мебели',
                //'sitemap' => 'Карта сайта',
                //'sitemap_items' => 'Карта сайта (товары)',
                //'promo_widget' => 'Виджет спецпредложений',
                //'market_available_apply' => 'Перекрывать В наличии для Я.Маркет',
                //'market_available' => 'В наличии для Я.Маркет',
                //'market' => 'Выгружать в Я.Маркет',
                //'modules' => 'Модули',
                //'virtual_discounts' => 'Виртуальные скидки',
                //'autosale' => 'Применять ярлык на основе скидки',
                //'dimension_filter' => 'Отображать в фильтре ШИРИНА\ВЫСОТА\ГЛУБИНА?',
                'assign_modules' => 'Привязывать модули',
                'showroom' => 'Эту модель можно посмотреть в магазине?',
            ]
            ?>
            @foreach($buttons as $name => $title)
                <div style="display: inline-block">
                    @include('backend.includes.partials.toggle', ['model' => $category, 'toggle' => $name, 'title' => $title])
                </div>
            @endforeach

            <div style="display: inline-block">
                @include('backend.category.partials.form.destroy')
            </div>
        </div>
    </div>

    @if($category->children->count())
    <ol class="dd-list">
        @foreach($category->children as $child)
            @include('backend.category.partials.tree-item', ['category' => $child])
        @endforeach
    </ol>
    @endif
</li>
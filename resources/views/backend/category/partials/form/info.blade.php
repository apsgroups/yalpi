    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Отзывы учащихся</h3>
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="form-group">
                <label class="col-lg-2 control-label">Включить полезную информацию</label>
                <div class="col-lg-6">
                    {!! Form::hidden('info_enabled', 0) !!}
                    {!! Form::checkbox('info_enabled', 1, null) !!}
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-2 control-label">Видео коды (YouTube)</label>
                <div class="col-lg-6">
                    {!! Form::textarea('info_vids', null, [
                            'id' => 'info_vids',
                            'class' => 'form-control',
                        ]) !!}
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-2 control-label">Вопросы и ответы</label>
                <div class="col-lg-5">
                    {!! Form::select('info_faq[]', $selectFaq, null, [
                            'data-placeholder' => 'Выберите вопросы',
                            'data-token-order' => json_encode($selectFaq),
                            'class' => 'select-items',
                            'multiple' => 'multiple',
                        ]) !!}
                    {!! $errors->first('info_faq', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-2 control-label">Статьи</label>
                <div class="col-lg-5">
                    {!! Form::select('info_articles[]', $selectArticles, null, [
                            'data-placeholder' => 'Выберите статьи',
                            'data-token-order' => json_encode($selectArticles),
                            'class' => 'select-items',
                            'multiple' => 'multiple',
                        ]) !!}
                    {!! $errors->first('info_articles', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>

            @for($i=0; $i <= 5; $i++)
                <div class="form-group">
                    <label class="col-lg-2 control-label">Отзыв {{ $i+1 }} Текст</label>
                    <div class="col-lg-6">
                        <textarea class="form-control" name="info_reviews[{{ $i }}][text]">{{ (isset($category->info_reviews[$i]['text']) ? $category->info_reviews[$i]['text'] : null) }}</textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-2 control-label">Отзыв {{ $i+1 }} Имя</label>
                    <div class="col-lg-6">
                        <textarea class="form-control" name="info_reviews[{{ $i }}][name]">{{ (isset($category->info_reviews[$i]['name']) ? $category->info_reviews[$i]['name'] : null) }}</textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-2 control-label">Отзыв {{ $i+1 }} Специальность</label>
                    <div class="col-lg-6">
                        <textarea class="form-control" name="info_reviews[{{ $i }}][spec]">{{ (isset($category->info_reviews[$i]['spec']) ? $category->info_reviews[$i]['spec'] : null) }}</textarea>
                    </div>
                </div>
            @endfor
        </div><!-- /.box-body -->
    </div><!--box-->
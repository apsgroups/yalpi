@include('backend.includes.partials.form.seo')

<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Теги</h3>
    </div><!-- /.box-header -->

    <div class="box-body">
        <div class="form-group">
            <label class="col-lg-2 control-label">Перелинковка</label>
            <div class="col-lg-4">
                {!! Form::select('tags[]', $tags, array_keys($tags->toArray()), [
                        'class' => 'search-tags',
                        'multiple' => 'multiple'
                    ]) !!}
                {!! $errors->first('tags', '<div class="text-danger">:message</div>') !!}
            </div>
        </div>
    </div>
</div>

<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Meta-шаблоны</h3>
    </div><!-- /.box-header -->

    <div class="box-body">
        <div class="form-group">
            <label class="col-lg-2 control-label">Meta-шаблон для категории</label>
            <div class="col-lg-5">
                {!! Form::select('meta_category_id', $meta, null, ['class' => 'form-control']) !!}
            </div>
        </div>

        <div class="form-group">
            <label class="col-lg-2 control-label">Meta-шаблон для товаров категории</label>
            <div class="col-lg-5">
                {!! Form::select('meta_product_id', $meta, null, ['class' => 'form-control']) !!}
            </div>
        </div>

        <div class="form-group">
            <label class="col-lg-2 control-label">Meta-шаблон для фильтра категории</label>
            <div class="col-lg-5">
                {!! Form::select('meta_filter_id', $meta, null, ['class' => 'form-control']) !!}
            </div>
        </div>
    </div>
</div>

<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Падежи</h3>
    </div><!-- /.box-header -->

    <div class="box-body">
        <div class="form-group">
            <label class="col-lg-2 control-label">Автозаполнение падежами</label>
            <div class="col-lg-1">
                {!! Form::hidden('automorph', 0) !!}
                {!! Form::checkbox('automorph', 1, (!isset($category) || $category->automorph === 1)) !!}
            </div>
        </div>

        <div class="form-group">
            <label class="col-lg-2 control-label">Родительный</label>
            <div class="col-lg-5">
                {!! Form::text('morph[R]', null, ['class' => 'form-control', 'placeholder' => 'КОГО? ЧЕГО?']) !!} 
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-2 control-label">Винительный</label>
            <div class="col-lg-5">
                {!! Form::text('morph[V]', null, ['class' => 'form-control', 'placeholder' => 'КОГО? ЧТО?']) !!} 
            </div>
        </div>
    </div>
</div>

<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">SEO текст</h3>
    </div><!-- /.box-header -->

    <div class="box-body">
        <div class="form-group">
            <label class="col-lg-2 control-label">Описание (низ)</label>
            <div class="col-lg-1">
                {!! Form::textarea('description_bottom', null, [
                        'id' => 'description',
                        'class' => 'wysiwyg form-control'
                    ]) !!}
            </div>
        </div>
    </div>
</div>

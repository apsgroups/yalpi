<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Фильтр</h3>
    </div><!-- /.box-header -->

    <div class="box-body">
        <div class="form-group">
            <label class="col-lg-2 control-label">Отображать фильтр</label>
            <div class="col-lg-1">
                {!! Form::hidden('show_filter', 0) !!}
                {!! Form::checkbox('show_filter', 1, (empty($category) || $category->show_filter === 1)) !!}
            </div>
        </div><!--form control-->

        <div class="form-group">
            {!! Form::label('filter_button', 'Текст кнопки фильтра', ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-10">
                {!! Form::text('filter_button', null, ['class' => 'form-control', 'placeholder' => 'текст']) !!}
                {!! $errors->first('filter_button', '<div class="text-danger">:message</div>') !!}
            </div>
        </div>

        <div class="form-group">
            <label class="col-lg-2 control-label">Свойство 1-го уровня</label>
            <div class="col-lg-5">
                {!! Form::select('primary_property_id', $properties, null, [
                        'data-placeholder' => 'Выберите опции',
                        'class' => 'wide-select chosen-select form-control',
                    ]) !!}
                {!! $errors->first('options', '<div class="text-danger">:message</div>') !!}
            </div>
        </div>

        <div class="form-group">
            <label class="col-lg-2 control-label">Свойство 2-го уровня</label>
            <div class="col-lg-5">
                {!! Form::select('secondary_property_id', $properties, null, [
                        'data-placeholder' => 'Выберите опции',
                        'class' => 'wide-select chosen-select form-control',
                    ]) !!}
                {!! $errors->first('options', '<div class="text-danger">:message</div>') !!}
            </div>
        </div>

        <div class="form-group">
            <label class="col-lg-2 control-label">Скрыть опции фильтра</label>
            <div class="col-lg-5">
                {!! Form::select('disabled_options[]', $propertyValues, $activeDisabledOptions, [
                        'data-placeholder' => 'Выберите опции',
                        'class' => 'select-items',
                        'multiple' => 'multiple',
                    ]) !!}
                {!! $errors->first('options', '<div class="text-danger">:message</div>') !!}
            </div>
        </div><!--form control-->

        <div class="form-group">
            <label class="col-lg-2 control-label">Выводить только указанные поля</label>
            <div class="col-lg-5">
                {!! Form::select('filters_activate[]', $properties, $filtersActivated, [
                        'data-placeholder' => 'Выберите опции',
                        'class' => 'select-items',
                        'multiple' => 'multiple',
                    ]) !!}
                {!! $errors->first('filters_activate', '<div class="text-danger">:message</div>') !!}
            </div>
        </div><!--form control-->

        <div class="form-group">
            <label class="col-lg-2 control-label">Скрыть поля</label>
            <div class="col-lg-5">
                {!! Form::select('filters_deactivate[]', $properties, $filtersDeactivated, [
                        'data-placeholder' => 'Выберите опции',
                        'class' => 'select-items',
                        'multiple' => 'multiple',
                    ]) !!}
                {!! $errors->first('filters_deactivate', '<div class="text-danger">:message</div>') !!}
            </div>
        </div><!--form control-->

    </div><!-- /.box-body -->
</div><!--box-->

<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Автонаполнение</h3>
    </div><!-- /.box-header -->

    <div class="box-body"> 
        <div class="form-group">
            <label class="col-lg-2 control-label">Включить автонаполнение из фильтра</label>
            <div class="col-lg-1">
                {!! Form::hidden('autofiltering', 0) !!}
                {!! Form::checkbox('autofiltering', 1, (isset($category) && $category->autofiltering === 1)) !!}
            </div>
        </div><!--form control-->

        <div class="form-group">
            <label class="col-lg-2 control-label">Категория для автонаполнения</label>
            <div class="col-lg-5">
                {!! Form::select('autofiltering_category_id', $parents, null, [
                        'data-placeholder' => 'Выберите категорию',
                        'class' => 'wide-select chosen-select form-control'
                    ]) !!}
            </div>
        </div><!--form control-->

        <div class="form-group">
            <label class="col-lg-2 control-label">Автонаполнение из фильтра</label>
            <div class="col-lg-5">
                {!! Form::select('filter_options[]', $propertyValues, $activeFilterOptions, [
                        'data-placeholder' => 'Выберите опции',
                        'class' => 'select-items',
                        'multiple' => 'multiple',
                    ]) !!}
                {!! $errors->first('options', '<div class="text-danger">:message</div>') !!}
            </div>
        </div><!--form control--> 

        @foreach($rangeProperties as $p)
            <div class="form-group">
                <label class="col-lg-2 control-label">{{ $p->title }}</label>
                <div class="col-lg-2">
                    {!! Form::text('filter_options[range_'.$p->id.'][min]', null, ['class' => 'form-control', 'placeholder' => 'ОТ:']) !!} 
                </div>
                <div class="col-lg-2">
                    {!! Form::text('filter_options[range_'.$p->id.'][max]', null, ['class' => 'form-control', 'placeholder' => 'ДО:']) !!} 
                </div>
            </div><!--form control--> 
        @endforeach

    </div><!-- /.box-body -->
</div><!--box-->

<style>
    .chosen-container {
        width: 100% !important;
    }
</style>
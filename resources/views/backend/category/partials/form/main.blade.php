    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Основное</h3>
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="form-group">
                {!! Form::label('title', 'Наименование', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-10">
                    {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'наименование']) !!}
		            {!! $errors->first('title', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>

            <div class="form-group">
                {!! Form::label('alter_title', 'Альтернативный заголовок', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-10">
                    {!! Form::text('alter_title', null, ['class' => 'form-control', 'placeholder' => 'Альтернативный заголовок']) !!}
                    {!! $errors->first('alter_title', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>

            <div class="form-group">
                {!! Form::label('breadcrumb_title', 'Заголовок в хлебных крошках', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-10">
                    {!! Form::text('breadcrumb_title', null, ['class' => 'form-control', 'placeholder' => 'Заголовок в хлебных крошках']) !!}
                    {!! $errors->first('breadcrumb_title', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>

            <div class="form-group">
                {!! Form::label('sub_category_title', 'Заголовок в родительской', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-10">
                    {!! Form::text('sub_category_title', null, ['class' => 'form-control', 'placeholder' => 'Заголовок в родительской']) !!}
                    {!! $errors->first('sub_category_title', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>

            <div class="form-group">
                {!! Form::label('sub_category_alt_title', 'Заголовок для перелинковки', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-10">
                    {!! Form::text('sub_category_alt_title', null, ['class' => 'form-control', 'placeholder' => 'Заголовок для перелинковки']) !!}
                    {!! $errors->first('sub_category_alt_title', '<div class="text-danger">:message</div>') !!}
                </div>
            </div><!--form control-->

            <div class="form-group">
                {!! Form::label('slug', 'Псевдоним', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-10">
                    {!! Form::text('slug', null, ['class' => 'form-control', 'placeholder' => 'введите псевдоним для URL']) !!}
		            {!! $errors->first('slug', '<div class="text-danger">:message</div>') !!}
                </div>
            </div><!--form control-->

            <div class="form-group">
                {!! Form::label('image', 'Изображение', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-5">
                    {!! Form::file('image', ['class' => 'form-control']) !!}
		            {!! $errors->first('image', '<div class="text-danger">:message</div>') !!}
                </div>

                @if(isset($category) && $category->image)
                <div class="col-lg-3">
                    <div class="image-manager">
                        <div class="img-item" data-img-id="{{ $category->image->id }}">
                            <a href="{{ img_src($category->image, 'large') }}" class="fancybox" rel="gallery">
                                <img src="{{ img_src($category->image, 'preview') }}" class="img-rounded" />
                            </a>
                            <button class="remove-img btn btn-danger btn-xs" data-id="{{ $category->image->id }}"><i class="glyphicon glyphicon-trash"></i></button>

                            {{ Form::hidden('image_id', null) }}
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>

                @endif
            </div><!--form control-->

            <div class="form-group">
                {!! Form::label('mobile_image', 'Изображение для мобильных', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-5">
                    {!! Form::file('mobile_image', ['class' => 'form-control']) !!}
                    {!! $errors->first('mobile_image', '<div class="text-danger">:message</div>') !!}
                </div>

                @if(isset($category) && $category->mobileImage)
                    <div class="col-lg-3">
                        <div class="image-manager">
                            <div class="img-item" data-img-id="{{ $category->mobileImage->id }}">
                                <a href="{{ img_src($category->mobileImage, 'large') }}" class="fancybox" rel="gallery">
                                    <img src="{{ img_src($category->mobileImage, 'preview') }}" class="img-rounded" />
                                </a>
                                <button class="remove-img btn btn-danger btn-xs" data-id="{{ $category->mobileImage->id }}"><i class="glyphicon glyphicon-trash"></i></button>

                                {{ Form::hidden('mobile_image_id', null) }}
                            </div>
                        </div>
                    </div>

                    <div class="clearfix"></div>

                @endif
            </div><!--form control-->

            <div class="form-group">
                {!! Form::label('alt_image', 'Альт. изображение', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-5">
                    {!! Form::file('alt_image', ['class' => 'form-control']) !!}
                    {!! $errors->first('alt_image', '<div class="text-danger">:message</div>') !!}
                </div>

                @if(isset($category) && $category->altImage)
                    <div class="col-lg-3">
                        <div class="image-manager">
                            <div class="img-item" data-img-id="{{ $category->altImage->id }}">
                                <a href="{{ img_src($category->altImage, 'large') }}" class="fancybox" rel="gallery">
                                    <img src="{{ img_src($category->altImage, 'preview') }}" class="img-rounded" />
                                </a>
                                <button class="remove-img btn btn-danger btn-xs" data-id="{{ $category->altImage->id }}"><i class="glyphicon glyphicon-trash"></i></button>

                                {{ Form::hidden('alt_image_id', null) }}
                            </div>
                        </div>
                    </div>

                    <div class="clearfix"></div>

                @endif
            </div>

            <div class="form-group">
                {!! Form::label('search_image', 'Изображение в поиске', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-5">
                    {!! Form::file('search_image', ['class' => 'form-control']) !!}
                    {!! $errors->first('search_image', '<div class="text-danger">:message</div>') !!}
                </div>

                @if(isset($category) && $category->searchImage)
                    <div class="col-lg-3">
                        <div class="image-manager">
                            <div class="img-item" data-img-id="{{ $category->searchImage->id }}">
                                <a href="{{ img_src($category->searchImage, 'large') }}" class="fancybox" rel="gallery">
                                    <img src="{{ img_src($category->searchImage, 'preview') }}" class="img-rounded" />
                                </a>
                                <button class="remove-img btn btn-danger btn-xs" data-id="{{ $category->searchImage->id }}"><i class="glyphicon glyphicon-trash"></i></button>

                                {{ Form::hidden('search_image_id', null) }}
                            </div>
                        </div>
                    </div>

                    <div class="clearfix"></div>
                @endif
            </div>

            <div class="form-group">
                <label class="col-lg-2 control-label">Фон карточек</label>
                @for($i=1; $i <= 10; $i++)
                <label class="col-lg-1 img-bg-{{ $i }}">
                    {!! Form::radio('img_bg', $i, (isset($category) && $category->img_bg === $i)) !!}
                </label>
                @endfor
            </div>

            <div class="form-group">
                <label class="col-lg-2 control-label">Опубликовать</label>
                <div class="col-lg-1">
                    {!! Form::hidden('published', 0) !!}
                    {!! Form::checkbox('published', 1, (isset($category) && $category->published === 1)) !!}
                </div>
            </div><!--form control-->

            <div class="form-group">
                <label class="col-lg-2 control-label">Привязывать модули</label>
                <div class="col-lg-1">
                    {!! Form::hidden('assign_modules', 0) !!}
                    {!! Form::checkbox('assign_modules', 1, (isset($category) && $category->assign_modules === 1)) !!}
                </div>
            </div><!--form control-->


            <div class="form-group">
                <label class="col-lg-2 control-label">Уровень сложности</label>
                <div class="col-lg-5">
                    {!! Form::select('level_score', [1 => 1, 2, 3, 4, 5], null, [
                            'class' => 'form-control'
                        ]) !!}
                    {!! $errors->first('level_score', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>

            <div class="form-group">
                {!! Form::label('meter_price', 'Цена за погонный метр', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-10">
                    {!! Form::hidden('meter_price', 0) !!}
                    {!! Form::checkbox('meter_price', 1, (!isset($category) || $category->meter_price > 0)) !!}
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-2 control-label">Применять виртуальные скидки</label>
                <div class="col-lg-1">
                    {!! Form::hidden('virtual_discounts', 0) !!}
                    {!! Form::checkbox('virtual_discounts', 1, (isset($category) && $category->virtual_discounts === 1)) !!}
                </div>
            </div><!--form control-->

            <div class="form-group">
                <label class="col-lg-2 control-label">Добавлять товары в категорию Распродаж на основе скидки</label>
                <div class="col-lg-1">
                    {!! Form::hidden('autosale', 0) !!}
                    {!! Form::checkbox('autosale', 1, (isset($category) && $category->autosale === 1)) !!}
                </div>
            </div><!--form control-->

            <div class="form-group">
                <label class="col-lg-2 control-label">Виджет спец.предложений</label>
                <div class="col-lg-1">
                    {!! Form::hidden('promo_widget', 0) !!}
                    {!! Form::checkbox('promo_widget', 1, (isset($category) && $category->promo_widget === 1)) !!}
                </div>
            </div><!--form control-->

            <div class="form-group">
                <label class="col-lg-2 control-label">Карта сайта</label>
                <div class="col-lg-1">
                    {!! Form::hidden('sitemap', 0) !!}
                    {!! Form::checkbox('sitemap', 1, (!isset($category) || $category->sitemap === 1)) !!}
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-2 control-label">Скрыть товары категории</label>
                <div class="col-lg-1">
                    {!! Form::hidden('hide_products', 0) !!}
                    {!! Form::checkbox('hide_products', 1, !empty($category->hide_products)) !!}
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-2 control-label">Включить товары категории в карте сайта</label>
                <div class="col-lg-1">
                    {!! Form::hidden('sitemap_items', 0) !!}
                    {!! Form::checkbox('sitemap_items', 1, (!isset($category) || $category->sitemap_items === 1)) !!}
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-2 control-label">Отображать в родительской</label>
                <div class="col-lg-1">
                    {!! Form::hidden('show_as_child', 0) !!}
                    {!! Form::checkbox('show_as_child', 1, (isset($category) && $category->show_as_child === 1)) !!}
                </div>
            </div><!--form control-->

            <div class="form-group">
                <label class="col-lg-2 control-label">Родительская категория</label>
                <div class="col-lg-5">
                    {!! Form::select('parent_id', $parents, null, [
                            'data-placeholder' => 'Выберите категорию',
                            'class' => 'wide-select chosen-select form-control'
                        ]) !!}
                </div>
            </div><!--form control-->

            <div class="form-group">
                <label class="col-lg-2 control-label">Позиция</label>
                <div class="col-lg-5">
                    {!! Form::select('position', $positions, null, ['class' => 'form-control']) !!}
                </div>
            </div><!--form control-->

            <div class="form-group">
                <label class="col-lg-2 control-label">Тип</label>
                <div class="col-lg-5">
                    {!! Form::select('type', $types, null, ['class' => 'form-control']) !!}
                </div>
            </div><!--form control-->

            <div class="form-group">
                <label class="col-lg-2 control-label">Шаблон дочерних</label>
                <div class="col-lg-5">
                    {!! Form::select('child_template', $childTemplates, null, ['class' => 'form-control']) !!}
                </div>
            </div> 

            <div class="form-group">
                {!! Form::label('icon', 'Иконка', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-5">
                    {!! Form::text('icon', null, ['class' => 'form-control', 'placeholder' => 'введите имя css иконки']) !!}
                    {!! $errors->first('icon', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-2 control-label">Выводить "Заголовок для перелинковки"</label>
                <div class="col-lg-1">
                    {!! Form::hidden('use_sub_category_alt_title', 0) !!}
                    {!! Form::checkbox('use_sub_category_alt_title', 1, (isset($category) && $category->use_sub_category_alt_title === 1)) !!}
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-2 control-label">Выводить указанные категории вместо дочерних</label>
                <div class="col-lg-5">
                    <?php 
                        $subCategories = [];

                        if(!empty($category->sub_categories)) {                                
                            foreach ($category->sub_categories as $key => $value) {
                                if($value !== '') {
                                    $subCategories[] = (int)$value;
                                }
                            }
                        }
                    ?>

                    {!! Form::select('sub_categories[]', $parents, null, [
                            'data-placeholder' => 'Выберите категорию',
                            'data-token-order' => json_encode($subCategories),
                            'class' => 'select-items',
                            'multiple' => 'multiple',
                        ]) !!}
                    {!! $errors->first('sub_categories', '<div class="text-danger">:message</div>') !!}
                </div>
            </div><!--form control-->

            <div class="form-group">
                <label class="col-lg-2 control-label">C этим покупают</label>
                <div class="col-lg-5">
                    {!! Form::select('populars[]', $parents, (isset($populars) ? $populars : null), [
                            'data-placeholder' => 'Выберите категорию',
                            'data-token-order' => json_encode($populars),
                            'class' => 'select-items',
                            'multiple' => 'multiple',
                        ]) !!}
                    {!! $errors->first('populars', '<div class="text-danger">:message</div>') !!}
                </div>
            </div><!--form control-->

            <div class="form-group">
                <label class="col-lg-2 control-label">Отображать в форме поиска</label>
                <div class="col-lg-1">
                    {!! Form::hidden('search_form', 0) !!}
                    {!! Form::checkbox('search_form', 1, (isset($category) && $category->search_form === 1)) !!}
                </div>
            </div><!--form control-->

            <div class="form-group">
                <label class="col-lg-2 control-label">Производитель</label>
                <div class="col-lg-1">
                    {!! Form::hidden('vendor', 0) !!}
                    {!! Form::checkbox('vendor', 1, (!isset($category) || $category->vendor === 1)) !!}
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-2 control-label">Модель (у товаров будут выводиться модули)</label>
                <div class="col-lg-1">
                    {!! Form::hidden('model', 0) !!}
                    {!! Form::checkbox('model', 1, (isset($category) && $category->model === 1)) !!}
                </div>
            </div><!--form control-->

            <div class="form-group">
                <label class="col-lg-2 control-label">Композиция</label>
                <div class="col-lg-1">
                    {!! Form::hidden('composition', 0) !!}
                    {!! Form::checkbox('composition', 1, (isset($category) && $category->composition === 1)) !!}
                </div>
            </div><!--form control-->

            <div class="form-group">
                <label class="col-lg-2 control-label">Модули</label>
                <div class="col-lg-1">
                    {!! Form::hidden('modules', 0) !!}
                    {!! Form::checkbox('modules', 1, (isset($category) && $category->modules === 1)) !!}
                </div>
            </div><!--form control-->

            <div class="form-group">
                {!! Form::label('in_row', 'Товаров в ряд', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-10">
                    {!! Form::text('in_row', null, ['class' => 'form-control', 'placeholder' => 'сейчас по умолчанию '.\Settings::get('in_row')]) !!}
		            {!! $errors->first('in_row', '<div class="text-danger">:message</div>') !!}
                </div>
            </div><!--form control-->

            <div class="form-group">
                <label class="col-lg-2 control-label">Эту модель можно посмотреть в магазине?</label>
                <div class="col-lg-5">
                    {!! Form::hidden('showroom', 0) !!}
                    {!! Form::checkbox('showroom', 1, (empty($category) || $category->showroom === 1)) !!}
                </div>
            </div> 

            <div class="form-group">
                <label class="col-lg-2 control-label">Описание</label>
                <div class="col-lg-1">
                    {!! Form::textarea('description', null, [
                            'id' => 'description',
                            'class' => 'wysiwyg tform-control'
                        ]) !!}
                </div>
            </div><!--form control-->

            <div class="form-group">
                <label class="col-lg-2 control-label">Описание для моб.</label>
                <div class="col-lg-1">
                    {!! Form::textarea('mobile_description_bottom', null, [
                            'id' => 'mobile_description_bottom',
                            'class' => 'wysiwyg tform-control'
                        ]) !!}
                </div>
            </div><!--form control-->

            <div class="form-group">
                <label class="col-lg-2 control-label">Примечание к комплектующим</label>
                <div class="col-lg-6">
                    {!! Form::textarea('complects_note', null, [
                            'id' => 'complects_note',
                            'class' => 'form-control',
                        ]) !!}
                </div>
            </div><!--form control-->

            <div class="form-group">
                {!! Form::label('tag', 'Тег в карточке', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-10">
                    {!! Form::text('tag', null, ['class' => 'form-control', 'placeholder' => 'Тег в карточке']) !!}
                    {!! $errors->first('tag', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>

            @php($docs = !empty($category->docs) ? $category->docs : [[]])

            <div class="repeatable property-list">
                @if(!empty($docs))
                    @foreach($docs as $i => $row)
                    <div class="form-group repeatable-row">
                        {!! Form::label('docs', 'Документация', ['class' => 'col-lg-2 control-label']) !!}

                        <div class="col-lg-2">
                            {!! Form::file('docs['.$i.'][file]', ['class' => 'form-control']) !!}
                        </div>

                        <div class="col-lg-2">
                            {{ Form::text('docs['.$i.'][url]', old('docs.'.$i.'.url', null), ['class' => 'form-control', 'placeholder' => 'Ссылка на документацию']) }}
                        </div>

                        <div class="col-lg-2">
                            {{ Form::text('docs['.$i.'][title]', old('docs.'.$i.'.title'), ['class' => 'form-control', 'placeholder' => 'Описание']) }}
                        </div>

                        <div class="col-lg-2">
                            {!! Form::button('<i class="glyphicon glyphicon-remove"></i>', ['class' => 'remove-row btn btn-danger btn-sm']) !!}
                            {!! Form::button('<i class="glyphicon glyphicon-plus"></i>', ['class' => 'add btn btn-success btn-sm']) !!}
                            {!! Form::hidden('docs['.$i.'][id]', $i) !!}
                        </div><!-- /.col-lg-6 -->
                    </div>
                    @endforeach
                @endif
            </div>

            <div class="form-group">
                {!! Form::label('product_priority', 'Приоритет товаров', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-10">
                    {!! Form::text('product_priority', null, ['class' => 'form-control', 'placeholder' => 'Приоритет товаров']) !!}
                    {!! $errors->first('product_priority', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>

            @php($namePriorities = !empty($category->name_priorities) ? $category->name_priorities : [[]])

            <div class="repeatable property-list">
                @if(!empty($namePriorities))
                    @foreach($namePriorities as $i => $row)
                    <div class="form-group repeatable-row">
                        {!! Form::label('name_priorities', 'Приоритет', ['class' => 'col-lg-2 control-label']) !!}

                        <div class="col-lg-2">
                            {{ Form::text('name_priorities['.$i.'][name]', old('name_priorities.'.$i.'.name', null), ['class' => 'form-control', 'placeholder' => 'Ключевое слово в наименовании товара']) }}
                        </div>

                        <div class="col-lg-2">
                            {{ Form::text('name_priorities['.$i.'][priority]', old('name_priorities.'.$i.'.priority', null), ['class' => 'form-control', 'placeholder' => 'Приоритет']) }}
                        </div>

                        <div class="col-lg-2">
                            {!! Form::button('<i class="glyphicon glyphicon-remove"></i>', ['class' => 'remove-row btn btn-danger btn-sm']) !!}
                            {!! Form::button('<i class="glyphicon glyphicon-plus"></i>', ['class' => 'add btn btn-success btn-sm']) !!}
                            {!! Form::hidden('name_priorities['.$i.'][id]', $i) !!}
                        </div><!-- /.col-lg-6 -->
                    </div>
                    @endforeach
                @endif
            </div>
        </div><!-- /.box-body -->
    </div><!--box-->
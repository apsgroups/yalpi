<a href="{{ route('admin.category.destroy', $category->id) }}"
    class="btn btn-xs btn-danger"
    data-method="delete">
        <i class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Удалить"></i>
</a>
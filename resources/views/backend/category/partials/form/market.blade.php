<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Яндекс.Маркет</h3>
    </div>

    <div class="box-body">
        <div class="form-group">
            <label class="col-lg-2 control-label">Выгружать в Маркет</label>
            <div class="col-lg-1">
                {!! Form::hidden('market', 1) !!}
                {!! Form::checkbox('market', 1, (isset($category) && $category->market === 1)) !!}
            </div>
        </div>

        <div class="form-group">
            <label class="col-lg-2 control-label">Наименование</label>
            <div class="col-lg-4">
                {!! Form::text('market_title', null, ['class' => 'form-control', 'placeholder' => 'Прямая кухня :preview_title (краткое имя) :title (полное имя)']) !!}
            </div>
        </div>

        <div class="form-group">
            <label class="col-lg-2 control-label">Sales notes</label>
            <div class="col-lg-4">
                {!! Form::text('sales_notes', null, ['class' => 'form-control']) !!}
            </div>
        </div>

        <div class="form-group">
            <label class="col-lg-2 control-label">Категория</label>
            <div class="col-lg-4">
                {!! Form::select('market_category_id', $marketCategories, null, ['class' => 'form-control']) !!}
            </div>
        </div>

        <div class="form-group">
            <label class="col-lg-2 control-label">"Товары в наличии" - применить настройки категории для товаров</label>
            <div class="col-lg-1">
                {!! Form::hidden('market_available_apply', 1) !!}
                {!! Form::checkbox('market_available_apply', 1, (isset($category) && $category->market_available_apply === 1)) !!}
            </div>
        </div>

        <div class="form-group">
            <label class="col-lg-2 control-label">Товары в наличии</label>
            <div class="col-lg-1">
                {!! Form::hidden('market_available', 1) !!}
                {!! Form::checkbox('market_available', 1, (isset($category) && $category->market_available === 1)) !!}
            </div>
        </div>
    </div>
</div>
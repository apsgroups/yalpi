@if(!empty($category))
{!! Form::model($category, ['files' => true, 'route' => ['admin.category.update', $category->id], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'put']) !!}
@else
{!! Form::open(['files' => true, 'route' => 'admin.category.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) !!}
@endif

<?php
$tabs = ['main', 'filter', 'seo', 'market', 'info'];
$active = 'main';
?>

    @include('backend.includes.partials.save-buttons', ['cancel' => route('admin.category.index')])

    <div>
      <!-- Nav tabs -->
      <ul class="nav nav-tabs" role="tablist">
        @foreach($tabs as $tab)
        <li role="presentation"<?php echo $tab === $active ? ' class="active"' : '' ?>>
            <a href="#{{ $tab }}" aria-controls="{{ $tab }}" role="tab" data-toggle="tab">{{ trans('category.tab.'.$tab) }}</a>
        </li>
        @endforeach
      </ul>

      <!-- Tab panes -->
      <div class="tab-content">
        @foreach($tabs as $tab)
        <div role="tabpanel" class="tab-pane<?php echo $tab === $active ? ' active' : '' ?>" id="{{ $tab }}">
            @include('backend.category.partials.form.'.$tab)
        </div>
        @endforeach
      </div>

    </div>

    @include('backend.includes.partials.save-buttons', ['cancel' => route('admin.category.index')])

    {!! Form::hidden('id') !!}
{!! Form::close() !!}

@section('after-scripts-end')
    {!! Html::script('js/plugin/ckeditor/ckeditor.js') !!}
    <script>
    CKEDITOR.replace('description', {
        filebrowserBrowseUrl: "{{ route('elfinder.ckeditor') }}",
        removeDialogTabs: 'link:upload;image:upload',
        extraAllowedContent: '*[*]{*}(*)'
    });

    CKEDITOR.replace('description_bottom', {
        filebrowserBrowseUrl: "{{ route('elfinder.ckeditor') }}",
        removeDialogTabs: 'link:upload;image:upload',
        extraAllowedContent: '*[*]{*}(*)'
    });

    CKEDITOR.replace('mobile_description_bottom', {
        filebrowserBrowseUrl: "{{ route('elfinder.ckeditor') }}",
        removeDialogTabs: 'link:upload;image:upload',
        extraAllowedContent: '*[*]{*}(*)'
    });
    </script>
@stop
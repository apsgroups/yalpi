@extends ('backend.layouts.master')

@section ('title', 'Изменение категории')

@section('page-header')
    <h1>
        Изменение категории <small>{{ $category->title }}</small>
    </h1>
@endsection

@section('content')
    @include('backend.category.partials.form')
@stop
@extends('backend.layouts.master')

@section('title', 'Экспорт товаров')

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Экспорт товаров</h3>
        </div>

        <div class="box-body">
            {!! Form::open(['route' => 'admin.export.product.export', 'method' => 'post', 'class' => 'form-horizontal']) !!}
                <div class="form-group">
                    {!! Form::label('category_id', 'ID категории', ['class' => 'col-lg-2 control-label']) !!}
                    <div class="col-lg-10">
                        {!! Form::text('category_id', null, ['class' => 'form-control', 'placeholder' => 'ID категории']) !!}
                        {!! $errors->first('category_id', '<div class="text-danger">:message</div>') !!}
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-lg-2"></div>
                    <div class="col-lg-10">
                        <button type="submit" class="btn btn-primary btn-sm">Экспорт</button>
                    </div>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
@stop
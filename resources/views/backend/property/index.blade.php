@extends('backend.layouts.master')

@section('title', 'Управление свойствами товаров')

@section('page-header')
    <h1>
        Управление свойствами товаров
    </h1>
@endsection

@section('content')
    @inject('sort', 'App\Services\Sort')

    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Фильтр</h3>
        </div><!-- /.box-header -->

        <div class="box-body">
            @include('backend.property.partials.filter')
        </div><!-- /.box-body -->
    </div>

    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Просмотр свойств товаров</h3>

            <div class="box-tools pull-right">
                @include('backend.property.partials.header-buttons')
            </div>
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover {{ ((request()->input('sort') === 'ordering' && request()->input('dir', 'desc') === 'asc') ? 'sortable-properties' : '') }} ">
                    <thead>
                    <tr>
                        <th>{!! $sort->route('admin.property.index', 'ordering', '<i class="fa fa-sort"></i>') !!}</th>
                        <th>ID</th>
                        <th>Наименование</th>
                        <th>Псевдоним</th>
                        <th>Тип</th>
                        <th>Значения</th>
                        <th class="text-center">Опубликован</th>
                        <th class="text-center">Фильтр</th>
                        <th class="text-center">Создано</th>
                        <th class="text-center">Обновлено</th>
                        <th class="text-center">Удалить</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach ($properties as $property)
                            <tr id="item-{{ $property->id }}">
                                <td>{!! $property->ordering !!}</td>
                                <td>{!! $property->id !!}</td>
                                <td>{!! link_to_route('admin.property.edit', $property->title, [$property->id]) !!}</td>
                                <td>{{ $property->slug }}</td>
                                <td>{{ $property->type }}</td>
                                <td>{!! link_to_route('admin.property-value.index', 'Список значений', [$property->id]) !!}</td>
                                <td class="text-center">
                                    @include('backend.includes.partials.toggle', ['model' => $property, 'toggle' => 'published'])
                                </td>
                                <td class="text-center">
                                    @include('backend.includes.partials.toggle', ['model' => $property, 'toggle' => 'filter'])
                                </td>
                                <td class="text-center">{!! $property->created_at->format(config('backend.date.format')) !!}</td>
                                <td class="text-center">{!! $property->updated_at->format(config('backend.date.format')) !!}</td>
                                <td class="text-center">
                                    @include('backend.includes.partials.destroy', ['link' => route('admin.property.destroy', $property->id)])
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

            @include('backend.includes.partials.pagination', ['items' => $properties])
        </div><!-- /.box-body -->
    </div><!--box-->
@stop
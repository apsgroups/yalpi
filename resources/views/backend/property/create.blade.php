@extends ('backend.layouts.master')

@section ('title', 'Добавление свойства товара')

@section('page-header')
    <h1>
        Добавление свойства товара
    </h1>
@endsection

@section('content')
    @include('backend.property.partials.form')
@stop
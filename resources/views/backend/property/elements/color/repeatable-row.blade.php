<div class="form-group repeatable-row">
      <div class="col-lg-1">
          <span class="handle"><i class="fa fa-bars"></i></span>
          {!! Form::hidden('values['.$i.'][ordering]', old('values.'.$i.'.ordering', (isset($row->ordering) ? $row->ordering : null)), ['class' => 'order-field']) !!}
      </div>

      <div class="col-lg-2">
        {!! Form::text('values['.$i.'][title]', old('values.'.$i.'.title', (isset($row->title) ? $row->title : null)), [
            'class' => 'form-control input-sm',
            'placeholder' => 'наименование',
            ]) !!}
      </div>

      <div class="col-lg-2">
        {!! Form::text('values['.$i.'][slug]', old('values.'.$i.'.slug', (isset($row->slug) ? $row->slug : null)), [
            'class' => 'form-control input-sm',
            'placeholder' => 'URL',
            ]) !!}
      </div>

      <div class="col-lg-2">
        {!! Form::text('values['.$i.'][plural]', old('values.'.$i.'.plural', (isset($row->plural) ? $row->plural : null)), [
            'class' => 'form-control input-sm',
            'placeholder' => 'мн. число',
            ]) !!}
      </div>

      <div class="col-lg-1">
        {!! Form::text('values['.$i.'][title_2]', old('values.'.$i.'.title_2', (isset($row->title_2) ? $row->title_2 : null)), [
            'class' => 'form-control input-sm',
            'placeholder' => 'В фильтре',
            ]) !!}
      </div>

      <div class="col-lg-1">
        {!! Form::file('values['.$i.'][image]', ['class' => 'form-control input-sm']) !!}
      </div>

      <div class="col-lg-1">
          {!! Form::button('<i class="glyphicon glyphicon-remove"></i>', ['class' => 'remove-row btn btn-danger btn-sm']) !!}
          {!! Form::button('<i class="glyphicon glyphicon-plus"></i>', ['class' => 'add btn btn-success btn-sm']) !!}
          {!! Form::hidden('values['.$i.'][id]', (isset($row->id) ? $row->id : null)) !!}
      </div>

      @if(isset($row) && $row->image)
      <div class="col-lg-2 no-clone">
        <div class="image-manager">
            {{ Form::hidden('values['.$i.'][image_id]', 0) }}
            <div class="img-item" data-img-id="{{ $row->image->id }}">
                <a href="{{ img_src($row->image, 'large') }}" class="fancybox" rel="gallery">
                    <img src="{{ img_src($row->image, 'preview') }}" class="img-rounded" />
                </a>
                <button class="remove-img btn btn-danger btn-xs" data-id="{{ $row->image->id }}"><i class="glyphicon glyphicon-trash"></i></button>
                {{ Form::hidden('values['.$i.'][image_id]', $row->image->id) }}
            </div>
        </div>
      </div>
      @endif

      {!! $errors->first('values.'.$i.'.title', '<div class="text-danger">:message</div>') !!}

</div>

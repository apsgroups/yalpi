<div class="repeatable property-list">
    <div class="form-group">
      <div class="col-lg-4">
        {!! Form::button('<i class="glyphicon glyphicon-plus"></i>', ['class' => 'add last btn btn-success input-sm']) !!}
        </div>
    </div>

    @if(!empty($property) && $property->values->count() > 0)
        @foreach($property->values()->with('image')->orderBy('ordering')->get() as $k => $value)
            @include('backend.property.elements.color.repeatable-row', ['i' => $k, 'row' => $value])
        @endforeach
    @else
        @include('backend.property.elements.color.repeatable-row', ['i' => 0])
    @endif

</div>
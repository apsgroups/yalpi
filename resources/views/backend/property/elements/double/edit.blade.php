<div class="form-group">
    <label class="col-lg-2 control-label">{{ $property->title }}</label>
    <div class="col-lg-4">
        {!! Form::text('property['.$property->id.']',
                (isset($product) ? $product->getProductProperty($property->id) : null),
                ['class' => 'form-control']
            ) !!}
    </div>
</div><!--form control-->
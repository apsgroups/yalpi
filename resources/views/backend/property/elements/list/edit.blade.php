<div class="form-group">
    <label class="col-lg-2 control-label">{{ $property->title }}</label>
    <div class="col-lg-4">
        @if($property->multiple)
        {!! Form::select('property['.$property->id.'][]', $property->values()->lists('title', 'id')->toArray(),
                (isset($product) ? $product->getProductProperty($property->id) : []),
                ['class' => 'select-items', 'multiple' => 'multiple']
            ) !!}
        @else
        {!! Form::select('property['.$property->id.']',
                (['' => 'Выбрать']+$property->values()->lists('title', 'id')->toArray()),
                (isset($product) ? $product->getProductProperty($property->id) : null),
                ['class' => 'form-control']
            ) !!}
        @endif
    </div>
</div><!--form control-->
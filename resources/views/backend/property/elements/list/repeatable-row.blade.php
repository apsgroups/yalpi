<div class="form-group repeatable-row">
      <div class="col-lg-1">
          <span class="handle"><i class="fa fa-bars"></i></span>
          {!! Form::hidden('values['.$i.'][ordering]', old('values.'.$i.'.ordering', (isset($row->ordering) ? $row->ordering : null)), ['class' => 'order-field']) !!}
      </div>

      <div class="col-lg-3">

        {!! Form::text('values['.$i.'][title]', old('values.'.$i.'.title', (isset($row->title) ? $row->title : null)), [
            'class' => 'form-control input-sm',
            'placeholder' => 'наименование',
            ]) !!}
      </div><!-- /.col-lg-6 -->

      <div class="col-lg-2">
        {!! Form::text('values['.$i.'][slug]', old('values.'.$i.'.slug', (isset($row->slug) ? $row->slug : null)), [
            'class' => 'form-control input-sm',
            'placeholder' => 'URL',
            ]) !!}
      </div><!-- /.col-lg-6 -->

      <div class="col-lg-2">
        {!! Form::text('values['.$i.'][plural]', old('values.'.$i.'.plural', (isset($row->plural) ? $row->plural : null)), [
            'class' => 'form-control input-sm',
            'placeholder' => 'мн. число',
            ]) !!}
      </div><!-- /.col-lg-6 -->

      <div class="col-lg-1">
        {!! Form::text('values['.$i.'][title_2]', old('values.'.$i.'.title_2', (isset($row->title_2) ? $row->title_2 : null)), [
            'class' => 'form-control input-sm',
            'placeholder' => 'Муж. род',
            ]) !!}
      </div>

      <div class="col-lg-1">
        {!! Form::text('values['.$i.'][title_1c]', old('values.'.$i.'.title_1c', (isset($row->title_1c) ? $row->title_1c : null)), [
            'class' => 'form-control input-sm',
            'placeholder' => 'Наименование в 1С',
            ]) !!}
      </div>

      <div class="col-lg-3">
          {!! Form::button('<i class="glyphicon glyphicon-remove"></i>', ['class' => 'remove-row btn btn-danger btn-sm']) !!}
          {!! Form::button('<i class="glyphicon glyphicon-plus"></i>', ['class' => 'add btn btn-success btn-sm']) !!}
          {!! Form::hidden('values['.$i.'][id]', (isset($row->id) ? $row->id : null)) !!}
      </div><!-- /.col-lg-6 -->

      {!! $errors->first('values.'.$i.'.title', '<div class="text-danger">:message</div>') !!}

</div>
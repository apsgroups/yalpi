@extends ('backend.layouts.master')

@section ('title', 'Изменение свойства товара: '.$property->title)

@section('page-header')
    <h1>
        Изменение свойства товара: {{ $property->title }}
    </h1>
@endsection

@section('content')
    @include('backend.property.partials.form')
@stop
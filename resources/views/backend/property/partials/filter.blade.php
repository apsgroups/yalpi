{!! Form::open(['route' => 'admin.property.index', 'class' => 'form-inline', 'role' => 'form ', 'method' => 'get']) !!}
<div class="input-group-lg">
    <div class="col-md-3">
        {!! Form::label('productTypes', 'Типы продуктов') !!}
        {!! Form::select('filter[productTypes][]', $productTypes, @$filter['productTypes'], [
                    'class' => 'form-control',
                    'multiple' => 'multiple',
                    'data-placeholder' => 'Выберите типы продуктов',
                ])!!}
    </div>
    <div class="col-md-3">
        {!! Form::label('title', 'Заголовок / алиас') !!}
        {!! Form::input('string', 'filter[title]', @$filter['title'], ['class' => 'form-control', 'style' => 'width: 100%', 'placeholder' => 'Введите свойство']) !!}
    </div>
    <div class="col-md-3">
        {!! Form::label('value', 'Значение / алиас') !!}
        {!! Form::input('string', 'filter[value]', @$filter['value'], ['class' => 'form-control', 'style' => 'width: 100%', 'placeholder' => 'Введите значение']) !!}
    </div>
    <div class="col-md-3">
        <div class="row">
            <div class="col-sm-6">
                {!! Form::label('published', 'Опубликован') !!}
                <div>
                    {!! Form::select('filter[published]', ['' => 'Все', 1 => 'Да', 0 => 'Нет'], @$filter['published'], ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="col-sm-6">
                {!! Form::label('filter', 'Фильтр') !!}
                <div>
                    {!! Form::select('filter[filter]', ['' => 'Все', 1 => 'Да', 0 => 'Нет'], @$filter['filter'], ['class' => 'form-control']) !!}
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <br>
        <button type="submit" class="btn btn-success"><i class="fa fa-search"></i> Применить</button>

        @if($filter)
            &nbsp;{!! link_to_route('admin.property.index', 'Сбросить', null, ['class' => 'btn btn-danger']) !!}
        @endif
    </div>
</div>
{!! Form::close() !!}

@section('after-styles-end')
    {{ Html::style('css/backend/jquery-ui/jquery-ui.css') }}
    {{ Html::style('css/backend/property-filter.css') }}
@stop
@section('after-scripts-end')
    {{ Html::script('js/backend/property-filter.js') }}
@stop

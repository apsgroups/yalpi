<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Опции</h3>
    </div><!-- /.box-header -->

    <div class="box-body">
        <div id="property-manager">
            @if(!empty($property) && $property->type && view()->exists('backend.property.elements.'.$property->type.'.manage'))
                @include('backend.property.elements.'.$property->type.'.manage')
            @endif
        </div>
    </div><!-- /.box-body -->
</div><!--box-->
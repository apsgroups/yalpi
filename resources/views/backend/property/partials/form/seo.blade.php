    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">SEO</h3>
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="form-group">
                <label class="col-lg-2 control-label">Meta Description</label>
                <div class="col-lg-4">
                    {!! Form::textarea('metadesc', null, ['class' => 'form-control', 'rows' => 3]) !!}
                </div>
            </div><!--form control-->

            <div class="form-group">
                <label class="col-lg-2 control-label">Meta Keywords</label>
                <div class="col-lg-4">
                    {!! Form::textarea('metakeys', null, ['class' => 'form-control', 'rows' => 3]) !!}
                </div>
            </div><!--form control-->

        </div><!-- /.box-body -->
    </div><!--box-->
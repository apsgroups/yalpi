    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Основное</h3>
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="form-group">
                {!! Form::label('productTypes[]', 'Типы продуктов', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-10">
                    {!! Form::select('productTypes[]', $productTypes, $productTypesSelected, [
                        'class' => 'form-control',
                        'multiple' => 'multiple',
                        'data-placeholder' => 'Выберите типы продуктов'
                    ]) !!}
                    {!! $errors->first('productTypes[]', '<div class="text-danger">:message</div>') !!}
                </div>
            </div><!--form control-->

            <div class="form-group">
                {!! Form::label('title', 'Наименование', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-10">
                    {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'наименование']) !!}
		            {!! $errors->first('title', '<div class="text-danger">:message</div>') !!}
                </div>
            </div><!--form control-->

            <div class="form-group">
                {!! Form::label('slug', 'Псевдоним', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-10">
                    {!! Form::text('slug', null, ['class' => 'form-control', 'placeholder' => 'введите псевдоним для URL латинскими буквами, - вместо пробела']) !!}
		            {!! $errors->first('slug', '<div class="text-danger">:message</div>') !!}
                </div>
            </div><!--form control-->

            <div class="form-group">
                {!! Form::label('single_title', 'Наименование опции', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-10">
                    {!! Form::text('single_title', null, ['class' => 'form-control', 'placeholder' => 'например, Цвет столешниц -> столешница']) !!}
		            {!! $errors->first('single_title', '<div class="text-danger">:message</div>') !!}
                </div>
            </div><!--form control-->

            <div class="form-group">
                {!! Form::label('type', 'Тип', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-3">
                    {!! Form::select('type', [
                            'color' => 'Цвет',
                            'list' => 'Список',
                            'input' => 'Ввод',
                            'double' => 'Число с точкой',
                            'integer' => 'Целое число',
                        ],  null, ['class' => 'form-control type-switcher', 'placeholder' => 'тип']) !!}
		            {!! $errors->first('type', '<div class="text-danger">:message</div>') !!}
                </div>
            </div><!--form control-->

            <div class="form-group">
                {!! Form::label('position', 'Позиция', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-10">
                    {!! Form::select('position', config('backend.properties.position'),  null, ['class' => 'form-control', 'placeholder' => 'Выберите позицию']) !!}
                    {!! $errors->first('type', '<div class="text-danger">:message</div>') !!}
		            {!! $errors->first('position', '<div class="text-danger">:message</div>') !!}
                </div>
            </div><!--form control-->

            <div class="form-group">
                {!! Form::label('display_tag', 'Тег для вывода', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-10">
                    {!! Form::select('display_tag', config('backend.properties.display_tag'),  null, ['class' => 'form-control', 'placeholder' => 'Выберите тег']) !!}
                    {!! $errors->first('type', '<div class="text-danger">:message</div>') !!}
                    {!! $errors->first('position', '<div class="text-danger">:message</div>') !!}
                </div>
            </div><!--form control-->

            <div class="form-group">
                <label class="col-lg-2 control-label">Опубликовать</label>
                <div class="col-lg-1">
                    {!! Form::hidden('published', 0) !!}
                    {!! Form::checkbox('published', 1, (isset($property) && $property->published === 1)) !!}
		            {!! $errors->first('published', '<div class="text-danger">:message</div>') !!}
                </div>
            </div><!--form control-->

            <div class="form-group">
                <label class="col-lg-2 control-label">Фильтр по полю</label>
                <div class="col-lg-1">
                    {!! Form::hidden('filter', 0) !!}
                    {!! Form::checkbox('filter', 1, (isset($property) && $property->filter === 1)) !!}
		            {!! $errors->first('filter', '<div class="text-danger">:message</div>') !!}
                </div>
            </div><!--form control-->

            <div class="form-group">
                <label class="col-lg-2 control-label">Множественный выбор</label>
                <div class="col-lg-1">
                    {!! Form::hidden('multiple', 0) !!}
                    {!! Form::checkbox('multiple', 1, (isset($property) && $property->multiple === 1)) !!}
		            {!! $errors->first('multiple', '<div class="text-danger">:message</div>') !!}
                </div>
            </div><!--form control-->
        </div><!-- /.box-body -->
    </div><!--box-->
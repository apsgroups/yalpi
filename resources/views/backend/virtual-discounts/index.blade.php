@extends('backend.layouts.master')

@section('title', 'Виртуальные скидки')

@section('content')
<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Виртуальные скидки</h3>
    </div>

    <div class="box-body">
        <div id="virtual-discounts-box">
            <div class="clearfix">
                <small class="pull-left action-status"></small>
            </div>

            <div class="progress">
                <div class="progress-bar progress-bar-primary progress-bar-striped" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                  <span class="sr-only"></span>
                </div>
            </div>

            <p>
                <button class="btn btn-success btn-sm run-virtual-discounts">Сформировать виртуальные скидки</button>
            </p>
        </div>

        <p>{!! link_to_route('admin.tool.autosale', 'Обновить привязку распродаж') !!} - запускать после генерации скидок.</p>

        <h3>Текущие скидки</h3>
        <ul>
        @foreach(config('site.virtual-discounts') as $v)
            <li>
                @if(isset($v['from']))
                от {{ number_format($v['from'], 0, '.', ' ') }} р.
                @endif

                @if(isset($v['to']))
                до {{ number_format($v['to'], 0, '.', ' ')  }} р.
                @endif

                &mdash;

                {{ implode('%, ', $v['list']) }}%
            </li>
        @endforeach
        </ul>
    </div>
</div>
@endsection

@section('after-scripts-end')
    {!! Html::script('js/virtual_discounts.js') !!}
@stop
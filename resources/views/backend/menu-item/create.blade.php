@extends ('backend.layouts.master')

@section ('title', 'Создание пункта меню')

@section('page-header')
    <h1>
        Добавление пункта меню
    </h1>
@endsection

@section('content')
    @include('backend.menu-item.partials.form')
@stop
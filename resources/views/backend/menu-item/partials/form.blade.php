@if(!empty($item))
{!! Form::model($item, ['route' => ['admin.menu-item.update', $menu->slug, $item->id], 'files' => true, 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'put']) !!}
@else
{!! Form::open(['route' => ['admin.menu-item.store', $menu->slug], 'files' => true, 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) !!}
@endif

<?php
$tabs = ['main'];
$active = 'main';
?>

    @include('backend.includes.partials.save-buttons', ['cancel' => route('admin.menu-item.index', [$menu->slug])])

    <div>
      <!-- Nav tabs -->
      <ul class="nav nav-tabs" role="tablist">
        @foreach($tabs as $tab)
        <li role="presentation"<?php echo $tab === $active ? ' class="active"' : '' ?>>
            <a href="#{{ $tab }}" aria-controls="{{ $tab }}" role="tab" data-toggle="tab">{{ trans('menu-item.tab.'.$tab) }}</a>
        </li>
        @endforeach
      </ul>

      <!-- Tab panes -->
      <div class="tab-content">
        @foreach($tabs as $tab)
        <div role="tabpanel" class="tab-pane<?php echo $tab === $active ? ' active' : '' ?>" id="{{ $tab }}">
            @include('backend.menu-item.partials.form.'.$tab)
        </div>
        @endforeach
      </div>s

    </div>

    @include('backend.includes.partials.save-buttons', ['cancel' => route('admin.menu-item.index', [$menu->slug])])

    {!! Form::hidden('id') !!}
{!! Form::close() !!}

@section('after-scripts-end')
    {!! Html::script('js/plugin/ckeditor/ckeditor.js') !!}
    {!! Html::script('js/backend/menu.js') !!}
    <script>
    CKEDITOR.replace('description', {
        filebrowserBrowseUrl: "{{ route('elfinder.ckeditor') }}",
        removeDialogTabs: 'link:upload;image:upload'
    });
    </script>
@stop
<li class="dd-item dd3-item" data-id="{{ $item->id }}">
    <div class="dd-handle dd3-handle">
        <span class="glyphicon glyphicon-option-vertical"></span>
    </div>

    <div class="dd3-content">
        {!! link_to_route('admin.menu-item.edit', $item->title, [$menu->slug, $item->id]) !!}
        <small>({{ $item->slug }})</small>

        <div class="item-actions">
            <div style="display: inline-block">
                @include('backend.includes.partials.toggle', [
                        'model' => $item,
                        'toggle' => 'published',
                        'route' => ['admin.menu-item.toggle', $item->id]
                    ])
            </div>
            <div style="display: inline-block">
                @include('backend.menu-item.partials.form.destroy')
            </div>
        </div>
    </div>

    @if($item->children->count())
    <ol class="dd-list">
        @foreach($item->children as $child)
            @include('backend.menu-item.partials.tree-item', ['item' => $child])
        @endforeach
    </ol>
    @endif
</li>
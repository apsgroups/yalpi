    <div class="pull-left" style="margin-bottom:10px">
        <div class="btn-group">
            <a href="{{ route('admin.menu-item.create', [$menu->slug]) }}" class="btn btn-success btn-sm"><i class="glyphicon glyphicon-plus"></i> Добавить</a>
            <a href="{{ route('admin.menu-item.category-create', [$menu->slug]) }}" class="btn btn-success btn-sm"><i class="glyphicon glyphicon-plus"></i> Пункты на категории</a>
            <a href="{{ route('admin.menu-item.pointer-create', [$menu->slug]) }}" class="btn btn-success btn-sm"><i class="glyphicon glyphicon-plus"></i> Ссылки на пункты</a>
        </div>
    </div><!--pull right-->

    <div class="clearfix"></div>
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Основное</h3>
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="form-group">
                {!! Form::label('title', 'Наименование', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-10">
                    {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'наименование']) !!}
		            {!! $errors->first('title', '<div class="text-danger">:message</div>') !!}
                </div>
            </div><!--form control-->

            <div class="form-group">
                {!! Form::label('slug', 'Псевдоним', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-10">
                    {!! Form::text('slug', null, ['class' => 'form-control', 'placeholder' => 'URL']) !!}
                    {!! $errors->first('slug', '<div class="text-danger">:message</div>') !!}
                </div>
            </div><!--form control-->

            <div class="form-group">
                <label class="col-lg-2 control-label">Родительские пункт</label>
                <div class="col-lg-5">
                    {!! Form::select('parent_id', $parents, (isset($item) ? $item->parent_id : null), [
                            'data-placeholder' => 'Выберите пункт',
                            'class' => 'wide-select chosen-select form-control'
                        ]) !!}
                </div>
            </div><!--form control-->

            <div class="form-group">
                <label class="col-lg-2 control-label">Тип</label>
                <div class="col-lg-5">
                    {!! Form::select('type', $types, (isset($item) ? $item->type : null), [
                            'id' => 'type-list',
                            'data-placeholder' => 'На что ссылаемся?',
                            'class' => 'wide-select form-control'
                        ]) !!}
                </div>
            </div><!--form control-->

            @foreach(array_keys(config('menu.types')) as $type)
                <div id="type-{{ $type }}" class="form-type">
                    @include('backend.menu-item.partials.form.types.'.$type)
                </div>
            @endforeach

            <div class="form-group">
                <label class="col-lg-2 control-label">Опубликовать</label>
                <div class="col-lg-1">
                    {!! Form::hidden('published', 0) !!}
                    {!! Form::checkbox('published', 1, (isset($item) && $item->published === 1)) !!}
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-2 control-label">Скрыть дочерние в Главном меню</label>
                <div class="col-lg-1">
                    {!! Form::hidden('hide_children_mainmenu', 0) !!}
                    {!! Form::checkbox('hide_children_mainmenu', 1, (isset($item) && $item->hide_children_mainmenu === 1)) !!}
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-2 control-label">Скрыть в Сайдбаре</label>
                <div class="col-lg-1">
                    {!! Form::hidden('hide_sidebar', 0) !!}
                    {!! Form::checkbox('hide_sidebar', 1, (isset($item) && $item->hide_sidebar === 1)) !!}
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-2 control-label">Количество уроков</label>
                <div class="col-lg-1">
                    {!! Form::hidden('show_product_count', 0) !!}
                    {!! Form::checkbox('show_product_count', 1, (isset($item) && $item->show_product_count === 1)) !!}
                </div>
            </div>

            <div class="form-group">
                {!! Form::label('label_text', 'Текст ярлыка', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-10">
                    {!! Form::text('label_text', null, ['class' => 'form-control', 'placeholder' => 'хит, распродажа и т.д.']) !!}
                    {!! $errors->first('label_text', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-2 control-label">Стиль ярлыка</label>
                <div class="col-lg-5">
                    {!! Form::select('label_style', ['blue' => 'Синий', 'yellow' => 'Желтый'], null, ['class' => 'form-control', 'placeholder' => 'Выберите стиль']) !!}
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-2 control-label">Жирным</label>
                <div class="col-lg-1">
                    {!! Form::hidden('bold', 0) !!}
                    {!! Form::checkbox('bold', 1, (isset($item) && $item->bold === 1)) !!}
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-2 control-label">Жирным в каталоге</label>
                <div class="col-lg-1">
                    {!! Form::hidden('bold_catalog', 0) !!}
                    {!! Form::checkbox('bold_catalog', 1, (isset($item) && $item->bold_catalog === 1)) !!}
                </div>
            </div>

            <div class="form-group">
                {!! Form::label('note', 'Примечание', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-10">
                    {!! Form::text('note', null, ['class' => 'form-control', 'placeholder' => 'выводится после заголовка']) !!}
		            {!! $errors->first('note', '<div class="text-danger">:message</div>') !!}
                </div>
            </div><!--form control-->

            <div class="form-group">
                <label class="col-lg-2 control-label">Noindex</label>
                <div class="col-lg-1">
                    {!! Form::hidden('noindex', 0) !!}
                    {!! Form::checkbox('noindex', 1, (isset($item) && $item->noindex === 1)) !!}
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-2 control-label">Ссылка в Каталоге</label>
                <div class="col-lg-1">
                    {!! Form::hidden('showcase', 0) !!}
                    {!! Form::checkbox('showcase', 1, (!empty($item) && $item->showcase === 1)) !!}
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-2 control-label">Только в ссылках Каталога</label>
                <div class="col-lg-1">
                    {!! Form::hidden('only_showcase', 0) !!}
                    {!! Form::checkbox('only_showcase', 1, (!empty($item) && $item->only_showcase === 1)) !!}
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-2 control-label">Target (html атрибут)</label>
                <div class="col-lg-5">
                    {!! Form::select('target', $target, (isset($item) ? $item->target : null), ['class' => 'form-control']) !!}
                </div>
            </div><!--form control-->

            <div class="form-group">
                {!! Form::label('icon', 'CSS Иконка', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-5">
                    {!! Form::text('icon', null, ['class' => 'form-control', 'placeholder' => 'введите css class иконки']) !!}
		            {!! $errors->first('icon', '<div class="text-danger">:message</div>') !!}
                </div>
            </div><!--form control-->

            <div class="form-group">
                <label class="col-lg-2 control-label">Изображение</label>
                <div class="col-lg-6">
                    {!! Form::file('image', ['class' => 'form-control']) !!}
		            {!! $errors->first('image', '<div class="text-danger">:message</div>') !!}
                </div>

                @if(isset($item) && $item->image)
                <div class="col-lg-3">
                    <div class="image-manager">
                        <div class="img-item" data-img-id="{{ $item->image->id }}">
                            <a href="{{ img_src($item->image, 'large') }}" class="fancybox" rel="gallery">
                                <img src="{{ img_src($item->image, 'preview') }}" class="img-rounded" />
                            </a>
                            <button class="remove-img btn btn-danger btn-xs" data-id="{{ $item->image->id }}"><i class="glyphicon glyphicon-trash"></i></button>
                            {{ Form::radio('image_id', $item->image->id) }}
                        </div>
                    </div>
                </div>
                @endif
            </div><!--form control-->

            <div class="form-group">
                <label class="col-lg-2 control-label">Описание</label>
                <div class="col-lg-1">
                    {!! Form::textarea('description', null, [
                            'id' => 'description',
                            'class' => 'wysiwyg tform-control'
                        ]) !!}
                </div>
            </div><!--form control-->

        </div><!-- /.box-body -->
    </div><!--box-->

    {{ Form::hidden('menu_id', $menu->id) }}
    {{ Form::hidden('item_id', $menu->item_id, ['id' => 'item-id']) }}
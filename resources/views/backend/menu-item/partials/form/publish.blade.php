{!! Form::open(['route' => ['admin.category.publish', $category->id], 'class' => 'inline form-inline', 'role' => 'form', 'method' => 'post']) !!}
    {!! Form::hidden('published', (int)(!$category->published)) !!}
    <button type="submit"
        data-toggle="tooltip"
        data-placement="top"
        title="{{ $category->published ? 'Опубликовано' : 'Скрыто' }}"
        class="btn btn-{{ $category->published ? 'success' : 'danger' }} btn-xs">
            <i class="glyphicon glyphicon-{{ $category->published ? 'ok' : 'remove' }}"></i>
    </button>
{!! Form::close() !!}
<div class="form-group">
    <label class="col-lg-2 control-label">Тип контента</label>
    <div class="col-lg-5">
        {!! Form::select('content_type_id', $contentType, (isset($item) ? $item->item_id : null), [
                'data-placeholder' => 'Выберите',
                'class' => 'set-item-id wide-select chosen-select1 form-control'
            ]) !!}
    </div>
</div>
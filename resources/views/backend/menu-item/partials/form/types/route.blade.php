<div class="form-group">
    <label class="col-lg-2 control-label">Route</label>
    <div class="col-lg-5">
        {!! Form::select('route', array_merge(['выберите'], config('menu.routes')), (isset($item) ? $item->route : null), [
                'data-placeholder' => 'Выберите маршрут',
                'class' => 'wide-select chosen-select form-control'
            ]) !!}
    </div>
</div><!--form control-->
<div class="form-group">
    <label class="col-lg-2 control-label">Ссылка</label>
    <div class="col-lg-5">
        {!! Form::text('link', null, [
            'class' => 'form-control',
            'placeholder' => 'введите URL'
            ]) !!}
    </div>
</div>
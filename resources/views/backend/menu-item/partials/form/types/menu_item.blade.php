<div class="form-group">
    <label class="col-lg-2 control-label">Пункт меню</label>
    <div class="col-lg-5">
        {!! Form::select('menu_item_id', $menuItems, (isset($item) ? $item->item_id : null), [
                'data-placeholder' => 'Выберите',
                'class' => 'set-item-id wide-select chosen-select form-control'
            ]) !!}
    </div>
</div>
<div class="form-group">
    <label class="col-lg-2 control-label">Страница</label>
    <div class="col-lg-5">
        {!! Form::text('content', null, [
            'id' => 'content-search',
            'class' => 'search form-control',
            'autocomplete' => 'off',
            'placeholder' => 'введите наименование/псевдоним/id'
            ]) !!}
        <span id="contentTitle">
            @if($content)
                {{ $content->title }} / {{ $content->slug }} / {{ $content->id }}
            @endif
        </span>
    </div>
</div>
<ul class="treeselect{{ ($root !== 1 ? '-sub' : '') }}">
    @foreach($items as $item)
        <li>
            <div class="treeselect-item pull-left">
                @if(empty($disabled))
                    {{ Form::checkbox('items[]', $item->id, false, ['class' => 'pull-left', 'id' => 'assign_'.$item->id]) }}
                @endif

                <label for="assign_{{ $item->id }}" class="pull-left">{{ $item->title }}</label>
            </div>

            <div class="clearfix"></div>

            @if($item->children->count())
                @include('backend.menu-item.partials.assign', ['items' => $item->children, 'root' => 0, 'disabled' => 0])
            @endif
        </li>
    @endforeach
</ul>
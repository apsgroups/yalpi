@extends ('backend.layouts.master')

@section ('title', 'Создание пункта меню')

@section('page-header')
    <h1>
        Добавление пунктов меню на категории
    </h1>
@endsection

@section('content') 
	{!! Form::open(['route' => ['admin.menu-item.category-store', $menu->slug], 'files' => true, 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) !!}

		@include('backend.includes.partials.save-buttons', ['cancel' => route('admin.menu-item.index', [$menu->slug])])

	    <div class="box">
	        <div class="box-header with-border">
	            <h3 class="box-title">Основное</h3>
	        </div><!-- /.box-header -->

	        <div class="box-body">
	            <div class="form-group">
	                <label class="col-lg-2 control-label">Родительские пункт</label>
	                <div class="col-lg-5">
	                    {!! Form::select('parent_id', $parents, null, [
	                            'data-placeholder' => 'Выберите пункт',
	                            'class' => 'wide-select chosen-select form-control'
	                        ]) !!}
	                </div>
	            </div><!--form control-->

	            <div class="form-group">
	                <label class="col-lg-2 control-label">Опубликовать</label>
	                <div class="col-lg-1">
	                    {!! Form::checkbox('published', 1) !!}
	                </div>
	            </div>

		        <div class="treeview">
		            <div id="assignment"> 
		            <div id="menuselect-group" class="control-group">
		            	<div id="form_menuselect" class="controls">
		            		<div class="well well-small">
		            	        @include('backend.widget.partials.assign.assign_controls')

		            			<div class="clearfix"></div>

		            			<hr class="hr-condensed" />

		            			@include('backend.menu-item.partials.assign', ['items' => $categories, 'root' => 1])

		            			@include('backend.widget.partials.assign.item_control')
		            		</div>
		            	</div>
		            </div>

		            </div>
		        </div>
	        </div>
	    </div>

	    @include('backend.includes.partials.save-buttons', ['cancel' => route('admin.menu-item.index', [$menu->slug])])

	    {!! Form::hidden('id') !!}
	{!! Form::close() !!}
@stop
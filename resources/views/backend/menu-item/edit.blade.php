@extends ('backend.layouts.master')

@section ('title', 'Редактирование пункта меню')

@section('page-header')
    <h1>
        Редактирование пункта меню <small>{{ $item->title }}</small>
    </h1>
@endsection

@section('content')
    @include('backend.menu-item.partials.form')
@stop
@extends('backend.layouts.master')

@section('title', 'Меню')

@section('page-header')
    <h1>
        Управление меню
        <small>все</small>
    </h1>
@endsection

@section('after-styles-end')
@stop

@section('content')
    <div class="clearfix"></div>

    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Меню</h3>

            <div class="box-tools pull-right">
                @include('backend.menu-item.partials.header-buttons')
            </div>
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="tree-handle dd permission-hierarchy">
                <ol class="dd-list">
                    @foreach($items as $item)
                        @include('backend.menu-item.partials.tree-item', ['item' => $item])
                    @endforeach
                </ol>
            </div><!--master-list-->

            <div class="clearfix"></div>
        </div><!-- /.box-body -->
    </div><!--box-->
@stop

@section('after-scripts-end')
    {!! Html::script('js/backend/plugin/nestable/jquery.nestable.js') !!}

    <script>
        (function($) {
            var hierarchy = $('.permission-hierarchy');
            hierarchy.nestable({maxDepth:5});
            hierarchy.nestable('collapseAll');

            hierarchy.on('change', function() {
                $.ajax({
                    url : '{{ route('admin.menu-item.sort') }}',
                    type: "post",
                    data : {data:hierarchy.nestable('serialize'), '_token': $('meta[name=_token]').attr('content')},
                    success: function(data) {
                        if (data.status == "OK")
                            toastr.success("{!! trans('strings.backend.access.permissions.groups.hierarchy_saved') !!}");
                        else
                            toastr.error("{!! trans('auth.unknown') !!}.");
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        toastr.error("{!! trans('auth.unknown') !!}: " + errorThrown);
                    }
                });
            });
        })(jQuery);
    </script>
@stop
@extends('backend.layouts.login')

@section('content')
    @include('backend.auth.login_form')
@endsection
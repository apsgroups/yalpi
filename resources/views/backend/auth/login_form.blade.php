<div class="login-box-body"> 
{!! Form::open(['route' => 'admin.auth.login.submit', 'method' => 'post']) !!}
  <div class="form-group has-feedback">
    {!! Form::input('email', 'email', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.email')]) !!} 
    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
  </div>
  <div class="form-group has-feedback">
    {!! Form::input('password', 'password', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.password')]) !!}
    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
  </div>
  <div class="row">
    <div class="col-xs-8">
    </div>
    <!-- /.col -->
    <div class="col-xs-4">
      <button type="submit" class="btn btn-primary btn-block btn-flat">{{ trans('labels.frontend.auth.login_button') }}</button>
    </div>
    <!-- /.col -->
  </div>
          {!! Form::hidden('remember', 1) !!}
{!! Form::close() !!}
</div>
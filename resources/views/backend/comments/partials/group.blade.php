<div class="form-group repeatable-row">
      <div class="col-lg-3">
        {!! Form::text('groups['.$i.'][title]', old('groups.'.$i.'.title', (isset($group->title) ? $group->title : null)), [
            'class' => 'form-control input-sm',
            'placeholder' => 'наименование',
            ]) !!}
      </div>

      <div class="col-lg-4">
        {!! Form::select('groups['.$i.'][properties][]', $properties, (isset($group) ? $group->properties()->get()->pluck('id')->toArray() : []), [
            'class' => 'property-tokenize search',
            'autocomplete' => 'off',
            'placeholder' => 'выберите свойство',
            'multiple' => 'multiple',
            'style' => 'width: 300px',
            ]) !!}
      </div>

      <div class="col-lg-3">
          {!! Form::button('<i class="glyphicon glyphicon-remove"></i>', ['class' => 'remove-row btn btn-danger btn-sm']) !!}
          {!! Form::button('<i class="glyphicon glyphicon-plus"></i>', ['class' => 'add btn btn-success btn-sm']) !!}
          {!! Form::hidden('groups['.$i.'][id]', (isset($group->id) ? $group->id : null)) !!}
      </div>

      {!! $errors->first('groups.'.$i.'.title', '<div class="text-danger">:message</div>') !!}
</div>
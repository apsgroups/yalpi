<div class="box">
    <div class="box-body">
        <div class="pull-left">
            <a href="{{ $cancel }}" class="btn btn-danger btn-sm">Отмена</a>
        </div>

        <div class="pull-right">
            <input type="submit" class="btn btn-success btn-sm" value="Сохранить" />
            <input type="submit" name="save-and-close" class="btn btn-success btn-sm" value="Сохранить и закрыть" />
        </div>
        <div class="clearfix"></div>
    </div><!-- /.box-body -->
</div><!--box-->
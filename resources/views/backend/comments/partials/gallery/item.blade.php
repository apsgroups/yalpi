<div class="img-item" data-img-id="{{ $image->id }}">
    <a href="{{ img_src($image, 'large') }}" class="fancybox" rel="gallery"><img src="{{ img_src($image, 'preview') }}" class="img-rounded" /></a>
    <button class="remove-img btn btn-danger btn-xs" data-id="{{ $image->id }}"><i class="glyphicon glyphicon-trash"></i></button>
</div>
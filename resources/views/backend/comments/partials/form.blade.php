@if(!empty($comment))
{!! Form::model($comment, ['route' => ['admin.comment.update', $comment->id], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'put']) !!}
@else
{!! Form::open(['route' => 'admin.comment.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) !!}
@endif

    @include('backend.comments.partials.save-buttons', ['cancel' => route('admin.comment.index')])

    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Основное</h3>
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="form-group">
                {!! Form::label('rate', 'Оценка', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-10">
                    {!! Form::select('rate', [0, 1, 2, 3, 4, 5], null, ['class' => 'form-control']) !!}
                    {!! $errors->first('rate', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-2 control-label">Опубликовать</label>
                <div class="col-lg-1">
                    {!! Form::hidden('published', 0) !!}
                    {!! Form::checkbox('published', 1, (isset($comment) && $comment->published)) !!}
                    {!! $errors->first('published', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>

            <div class="box-body">
                <div class="form-group">
                    {!! Form::label('name', 'Имя', ['class' => 'col-lg-2 control-label']) !!}
                    <div class="col-lg-10">
                        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'имя']) !!}
                        {!! $errors->first('name', '<div class="text-danger">:message</div>') !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('dignity', 'Достоинства', ['class' => 'col-lg-2 control-label']) !!}
                    <div class="col-lg-10">
                        {!! Form::textarea('dignity', null, ['rows' => 2, 'class' => 'form-control', 'placeholder' => 'Достоинства']) !!}
                        {!! $errors->first('dignity', '<div class="text-danger">:message</div>') !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('limitations', 'Недостатки', ['class' => 'col-lg-2 control-label']) !!}
                    <div class="col-lg-10">
                        {!! Form::textarea('limitations', null, ['rows' => 2, 'class' => 'form-control', 'placeholder' => 'Недостатки']) !!}
                        {!! $errors->first('limitations', '<div class="text-danger">:message</div>') !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('comment', 'Комментарий', ['class' => 'col-lg-2 control-label']) !!}
                    <div class="col-lg-10">
                        {!! Form::textarea('comment', null, ['class' => 'form-control', 'placeholder' => 'Комментарий']) !!}
                        {!! $errors->first('comment', '<div class="text-danger">:message</div>') !!}
                    </div>
                </div>
            </div>
        </div><!--box-->
    </div><!--box-->

    @include('backend.includes.partials.save-buttons', ['cancel' => route('admin.comment.index')])

    {!! Form::hidden('id') !!}
{!! Form::close() !!}
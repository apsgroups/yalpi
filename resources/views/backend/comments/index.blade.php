@extends('backend.layouts.master')

@section('title', 'Комментарии')

@section('content')
    @inject('sort', 'App\Services\Sort')

    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Управление комментариями</h3>

            @if(!empty($item))
                <h4>{{ $item->title }}</h4>
            @endif
        </div><!-- /.box-header -->


        <div class="box-body">
            @include('backend.includes.partials.check.action', ['scope' => 'comment'])

            <p>&nbsp;</p>

            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        @include('backend.includes.partials.check.all')
                        <th class="text-center">{!! $sort->route('admin.comment.index', 'id', 'ID') !!}</th>
                        <th class="text-center">Имя</th>
                        <th class="text-center">{!! $sort->route('admin.comment.index', 'rate', 'Оценка') !!}</th>
                        <th class="text-center">Элемент</th>
                        <th class="text-center">{!! $sort->route('admin.comment.index', 'published', 'Опубликовано') !!}</th>
                        <th class="text-center">{!! $sort->route('admin.comment.index', 'created_at', 'Создано') !!}</th>
                        <th class="text-center">{!! $sort->route('admin.comment.index', 'updated_at', 'Обновлено') !!}</th>
                        <th class="text-center">Удалить</th>
                        <th class="text-center">Фронт</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach ($comments as $comment)
                            <tr>
                                @include('backend.includes.partials.check.item', ['id' => $comment->id])
                                <td class="text-center">{{ $comment->id }}</td>
                                <td>{!! link_to_route('admin.comment.edit', $comment->name, [$comment->id]) !!}</td>
                                <td class="text-center">{{ $comment->rate }}</td>
                                <td>
                                @if($comment->commentable)
                                    <?php
                                    $params = [$comment->commentable->id];

                                    if(get_class($comment->commentable) === \App\Models\Content::class) {
                                        $params = [$comment->commentable->type->slug, $comment->commentable->id];
                                    }
                                    ?>

                                    {!! link_to_route('admin.'.strtolower(class_basename($comment->commentable)).'.edit', $comment->commentable->title, $params) !!}
                                @endif
                                </td>
                                <td class="text-center">
                                    @include('backend.includes.partials.toggle', ['model' => $comment, 'toggle' => 'published'])
                                </td>
                                <td class="text-center">{!! $comment->created_at->format(config('backend.date.format')) !!}</td>
                                <td class="text-center">{!! $comment->updated_at->format(config('backend.date.format')) !!}</td>
                                <td class="text-center">
                                    @include('backend.includes.partials.destroy', ['link' => route('admin.comment.destroy', $comment->id)])
                                </td>
                                <td class="text-center">
                                @if($comment->commentable)
                                    <a href="{{ $comment->commentable->link() }}" target="_blank" class="btn btn-xs btn-info"><i class="fa fa-search"></i></a>
                                @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

            @include('backend.includes.partials.pagination', ['items' => $comments])
        </div><!-- /.box-body -->
    </div><!--box-->
@stop
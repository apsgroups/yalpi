@extends ('backend.layouts.master')

@section ('title', 'Изменение комментария')

@section('page-header')
    <h1>
        Изменение комментария
    </h1>
@endsection

@section('content')
    @include('backend.comments.partials.form')
@stop
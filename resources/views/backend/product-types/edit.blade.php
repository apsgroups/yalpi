@extends ('backend.layouts.master')

@section ('title', 'Изменение типа товара: '.$item->title)

@section('page-header')
    <h1>
        Изменение типа {{ $item->title }}
    </h1>
@endsection

@section('content')
    @include('backend.product-types.partials.form')
@stop
@if(!empty($item))
{!! Form::model($item, ['route' => ['admin.product-type.update', $item->id], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'put']) !!}
@else
{!! Form::open(['route' => 'admin.product-type.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) !!}
@endif

    @include('backend.includes.partials.save-buttons', ['cancel' => route('admin.product-type.index')])

    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Основное</h3>
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="form-group">
                {!! Form::label('title', 'Наименование', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-10">
                    {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'наименование']) !!}
		            {!! $errors->first('title', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>

            <div class="form-group">
                {!! Form::label('module_title', 'Склонение для страницы модулей', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-10">
                    {!! Form::text('module_title', null, ['class' => 'form-control', 'placeholder' => 'Модули *кухонь* «Прага Венге»']) !!}
		            {!! $errors->first('module_title', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>

            <div class="form-group">
                {!! Form::label('search_url', 'Ссылка в тегах поиска', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-10">
                    {!! Form::text('search_url', null, ['class' => 'form-control', 'placeholder' => 'Ссылка в тегах поиска']) !!}
                    {!! $errors->first('search_url', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>

            <div class="form-group">
                {!! Form::label('search_priority', 'Приоритет в поиске', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-10">
                    {!! Form::text('search_priority', null, ['class' => 'form-control', 'placeholder' => '30']) !!}
		            {!! $errors->first('search_priority', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>

            <div class="form-group">
                {!! Form::label('search_keys', 'Ключи для поиска', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-10">
                    {!! Form::textarea('search_keys', null, ['class' => 'form-control', 'placeholder' => 'Дополнительные ключи для поиска']) !!}
		            {!! $errors->first('search_keys', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>

            <div class="alert alert-info">
              После изменения полей "Приоритет в поиске" и "Ключи для поиска" сохранитесь и проведите {!! link_to_route('admin.tool.products.reindex', 'реиндексацию') !!}</div>
            </div>

            <div class="form-group">
                <label class="col-lg-2 control-label">Распродажная категория</label>
                <div class="col-lg-5">
                    {!! Form::select('sale_category_id', $categories, null, [
                            'data-placeholder' => 'Выберите категорию распродаж',
                            'class' => 'wide-select chosen-select form-control'
                        ]) !!}
                </div>
            </div><!--form control-->

            <div class="form-group">
                <div class="col-lg-2"></div>
                <div class="col-lg-5">
                    <div class="checkbox">
                        <label>
                            {!! Form::hidden('display_name', 0) !!}
                            {!! Form::checkbox('display_name', 1, !empty($item->display_name)) !!}
                            {!! $errors->first('display_name', '<div class="text-danger">:message</div>') !!}
                            Добавлять к имени товара имя типа при выводе в категории?
                        </label>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-lg-2"></div>
                <div class="col-lg-5">
                    <div class="checkbox">
                        <label>
                            {!! Form::hidden('modules', 0) !!}
                            {!! Form::checkbox('modules', 1, !empty($item->modules)) !!}
                            {!! $errors->first('modules', '<div class="text-danger">:message</div>') !!}
                            Товары данного типа являются модулями?
                        </label>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-lg-2"></div>
                <div class="col-lg-5">
                    <div class="checkbox">
                        <label>
                            {!! Form::hidden('display_tag', 0) !!}
                            {!! Form::checkbox('display_tag', 1, !empty($item->display_tag)) !!}
                            {!! $errors->first('display_tag', '<div class="text-danger">:message</div>') !!}
                            Добавлять к имени товара тег из категории в превью товара?
                        </label>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-lg-2"></div>
                <div class="col-lg-5">
                    <div class="checkbox">
                        <label>
                            {!! Form::hidden('preview_dimensions', 0) !!}
                            {!! Form::checkbox('preview_dimensions', 1, !empty($item->preview_dimensions)) !!}
                            {!! $errors->first('preview_dimensions', '<div class="text-danger">:message</div>') !!}
                            Добавлять к имени товара габариты?
                        </label>
                    </div>
                </div>
            </div>
        </div><!-- /.box-body -->

    </div><!--box-->

    @include('backend.includes.partials.save-buttons', ['cancel' => route('admin.product-type.index')])

    {!! Form::hidden('id') !!}
{!! Form::close() !!}
@extends('backend.layouts.master')

@section('title', 'Добавление типа товара')

@section('page-header')
    <h1>
        Добавление типа товара
    </h1>
@endsection

@section('content')
    @include('backend.product-types.partials.form')
@stop
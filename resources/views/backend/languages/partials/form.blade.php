@if(!empty($item))
{!! Form::model($item, ['route' => ['admin.language.update', $item->id], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'put']) !!}
@else
{!! Form::open(['route' => 'admin.language.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) !!}
@endif

    @include('backend.includes.partials.save-buttons', ['cancel' => route('admin.language.index')])

    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Основное</h3>
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="form-group">
                {!! Form::label('title', 'Наименование', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-10">
                    {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'наименование']) !!}
		            {!! $errors->first('title', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>

            <div class="form-group">
                {!! Form::label('slug', 'Псевдоним', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-10">
                    {!! Form::text('slug', null, ['class' => 'form-control', 'placeholder' => 'псевдоним']) !!}
                    {!! $errors->first('slug', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>
        </div>
    </div>

    @include('backend.includes.partials.save-buttons', ['cancel' => route('admin.language.index')])

    {!! Form::hidden('id') !!}
{!! Form::close() !!}
@extends('backend.layouts.master')

@section('title', 'Добавление языка')

@section('page-header')
    <h1>
        Добавление языка
    </h1>
@endsection

@section('content')
    @include('backend.languages.partials.form')
@stop
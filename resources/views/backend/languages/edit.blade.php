@extends ('backend.layouts.master')

@section ('title', 'Изменение языка: '.$item->title)

@section('page-header')
    <h1>
        Изменение языка {{ $item->title }}
    </h1>
@endsection

@section('content')
    @include('backend.languages.partials.form')
@stop
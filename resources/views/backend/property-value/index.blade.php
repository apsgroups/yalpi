@extends('backend.layouts.master')

@section('title', 'Управление значениями свойства '.$property->title)

@section('page-header')
    <h1>Управление значениями свойства {{ $property->title }}</h1>
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">{{ $property->title }}</h3>

            <div class="box-tools pull-right">
                @include('backend.property-value.partials.header-buttons')
            </div>
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover sortable-property-values">
                    <thead>
                    <tr>
                        <th><i class="fa fa-sort"></i></th>
                        <th>ID</th>
                        <th>Наименование</th>
                        <th>Псевдоним</th>
                        <th class="text-center">Удалить</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach ($propertyValues as $value)
                            <tr id="item-{{ $value->id }}">
                                <td>{!! $value->ordering !!}</td>
                                <td>{!! $value->id !!}</td>
                                <td>{!! link_to_route('admin.property-value.edit', $value->title, [$value->id]) !!}</td>
                                <td>{{ $value->slug }}</td>
                                <td class="text-center">
                                    @include('backend.includes.partials.destroy', ['link' => route('admin.property-value.destroy', [$value->id])])
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div><!-- /.box-body -->
    </div><!--box-->
@stop
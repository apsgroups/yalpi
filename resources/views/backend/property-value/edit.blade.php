@extends ('backend.layouts.master')

@section ('title', 'Изменение свойства товара: '.$propertyValue->title)

@section('page-header')
    <h1>Изменение свойства товара: {{ $propertyValue->title }}</h1>
@endsection

@section('content')
    @include('backend.property-value.partials.form')
@stop
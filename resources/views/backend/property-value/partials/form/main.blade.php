<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Основное</h3>
    </div><!-- /.box-header -->

    <div class="box-body">
        <div class="form-group">
            {!! Form::label('title', 'Наименование', ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-10">
                {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'наименование']) !!}
	            {!! $errors->first('title', '<div class="text-danger">:message</div>') !!}
            </div>
        </div><!--form control-->

        <div class="form-group">
            {!! Form::label('slug', 'Псевдоним', ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-10">
                {!! Form::text('slug', null, ['class' => 'form-control', 'placeholder' => 'введите псевдоним для URL латинскими буквами, - вместо пробела']) !!}
	            {!! $errors->first('slug', '<div class="text-danger">:message</div>') !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('property_id', 'Свойство', ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-10">
                {!! Form::select('property_id', $properties, (isset($propertyValue) ? null : $property->id), [
                    'class' => 'chosen-select form-control',
                    'data-placeholder' => 'Выберите свойства'
                ]) !!}
                {!! $errors->first('property_id', '<div class="text-danger">:message</div>') !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('plural', 'Мн. число', ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-10">
                {!! Form::text('plural', null, ['class' => 'form-control', 'placeholder' => 'например, Прямой -> Прямые']) !!}
                {!! $errors->first('plural', '<div class="text-danger">:message</div>') !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('title_2', 'В фильтре', ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-10">
                {!! Form::text('title_2', null, ['class' => 'form-control', 'placeholder' => 'В фильтре']) !!}
                {!! $errors->first('title_2', '<div class="text-danger">:message</div>') !!}
            </div>
        </div>

        <div class="form-group">
            <label class="col-lg-2 control-label">Изображение</label>
            <div class="col-lg-6">
                {!! Form::file('image', ['class' => 'form-control']) !!}
                {!! $errors->first('image', '<div class="text-danger">:message</div>') !!}
            </div>

            @if(isset($propertyValue) && $propertyValue->image)
            <div class="col-lg-3">
                <div class="image-manager">
                    <div class="img-item" data-img-id="{{ $propertyValue->image->id }}">
                        <a href="{{ img_src($propertyValue->image, 'large') }}" class="fancybox" rel="gallery">
                            <img src="{{ img_src($propertyValue->image, 'preview') }}" class="img-rounded" />
                        </a>
                        <button class="remove-img btn btn-danger btn-xs" data-id="{{ $propertyValue->image->id }}"><i class="glyphicon glyphicon-trash"></i></button>
                    </div>
                </div>
            </div>
            @endif
        </div>

        <div class="form-group">
            {!! Form::label('parent_values', 'Родительские значения 1-го уровня', ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-10">
                {!! Form::select('parent_values[]', $parents, $selectedParents, [
                    'class' => 'search-property-values',
                    'data-placeholder' => 'Выберите значения',
                    'multiple' => 'multiple'
                ]) !!}
                {!! $errors->first('parent_values', '<div class="text-danger">:message</div>') !!}
            </div>
        </div>
    </div>
</div><!--box-->
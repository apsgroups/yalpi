@if(!empty($propertyValue))
{!! Form::model($propertyValue, ['files' => true, 'route' => ['admin.property-value.update', $propertyValue->id], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'put']) !!}
@else
{!! Form::open(['files' => true, 'route' => 'admin.property-value.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) !!}
@endif

<?php
$tabs = ['main'];
$active = 'main';
?>

    @include('backend.includes.partials.save-buttons', ['cancel' => route('admin.property-value.index', [$property->id])])

    <div>
      <!-- Nav tabs -->
      <ul class="nav nav-tabs" role="tablist">
        @foreach($tabs as $tab)
        <li role="presentation"<?php echo $tab === $active ? ' class="active"' : '' ?>>
            <a href="#{{ $tab }}" aria-controls="{{ $tab }}" role="tab" data-toggle="tab">{{ trans('property-value.tab.'.$tab) }}</a>
        </li>
        @endforeach
      </ul>

      <!-- Tab panes -->
      <div class="tab-content">
        @foreach($tabs as $tab)
        <div role="tabpanel" class="tab-pane<?php echo $tab === $active ? ' active' : '' ?>" id="{{ $tab }}">
            @include('backend.property-value.partials.form.'.$tab)
        </div>
        @endforeach
      </div>
    </div>

    @include('backend.includes.partials.save-buttons', ['cancel' => route('admin.property-value.index', [$property->id])])

    {!! Form::hidden('id') !!}
{!! Form::close() !!}

@section('after-scripts-end')
    {!! Html::script('js/plugin/ckeditor/ckeditor.js') !!}
@stop
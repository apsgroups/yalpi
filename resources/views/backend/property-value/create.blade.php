@extends ('backend.layouts.master')

@section ('title', 'Добавление свойства товара: '.$property->title)

@section('page-header')
    <h1>Добавление свойства товара: {{ $property->title }}</h1>
@endsection

@section('content')
    @include('backend.property-value.partials.form')
@stop
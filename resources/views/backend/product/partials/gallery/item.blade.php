<div class="img-item" data-img-id="{{ $image->id }}">
    <a href="{{ img_src($image, 'large') }}" class="fancybox" rel="gallery"><img src="{{ img_src($image, 'preview') }}" class="img-rounded" /></a>
    <button class="remove-img btn btn-danger btn-xs" data-id="{{ $image->id }}"><i class="glyphicon glyphicon-trash"></i></button>
    {{ Form::radio('image_id', $image->id) }}
    {{ Form::checkbox('ext_preview['.$image->id.']', 1, $image->ext_preview, ['title' => 'Показать в расширенном превью']) }}
</div>
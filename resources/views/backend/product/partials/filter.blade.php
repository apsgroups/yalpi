{!! Form::open(['route' => 'admin.product.index', 'class' => 'form-inline', 'role' => 'form ', 'method' => 'get']) !!}
    <div>
        <div class="input-group input-group-sm">
            {!! Form::label('search', 'Найти') !!}
            <div>
            {!! Form::text('filter[search]', @$filter['search'], ['class' => 'form-control', 'placeholder' => 'Найти']) !!}
            <span class="input-group-btn">
              <button type="submit" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
            </span>
            </div>
        </div>

        <div class="input-group input-group-sm">
            {!! Form::select('filter[category_id]', $categories, @$filter['category_id'], ['class' => 'chosen-select form-control']) !!}
        </div>

        <div class="input-group input-group-sm">
            {!! Form::label('published', 'Опубликовано') !!}
            {!! Form::select('filter[published]', ['' => 'Все', 1 => 'Да', 0 => 'Нет'], @$filter['published'], ['class' => 'form-control']) !!}
        </div>

        <div class="input-group input-group-sm">
            {!! Form::label('out_of_stock', 'Снят с продаж') !!}
            {!! Form::select('filter[out_of_stock]', ['' => 'Все', 1 => 'Да', 0 => 'Нет'], @$filter['out_of_stock'], ['class' => 'form-control']) !!}
        </div>

        <div class="input-group input-group-sm">
            <br />
            <button type="submit" class="btn btn-success"><i class="fa fa-search"></i></button>

            @if($filter)
            &nbsp;{!! link_to_route('admin.product.index', 'Сбросить', null, ['class' => 'btn btn-danger']) !!}
            @endif
        </div>

        <div class="input-group input-group-sm">
            <br />

            <button class="btn btn-xs btn-default" type="button" data-toggle="collapse" data-target="#filter-extended" aria-expanded="false" aria-controls="filter-extended">
                Дополнительно
            </button>
        </div>

        <p></p>

        <div class="collapse" id="filter-extended">
          <div class="well">
            <?php
            $fields = [
                'import' => 'Импорт',
                'market' => 'Маркет',
            ];
            ?>
            @foreach($fields as $name => $title)
            <div class="input-group input-group-sm">
                {!! Form::label($name, $title) !!}
                {!! Form::select('filter['.$name.']', ['' => 'Все', 1 => 'Да', 0 => 'Нет'], @$filter[$name], ['class' => 'form-control']) !!}
            </div>
            @endforeach

            <div class="input-group input-group-sm">
                {!! Form::label('ext_id', 'Код 1С') !!}
                {!! Form::select('filter[ext_id]', ['' => 'Все', 1 => 'Заполнены', 2 => 'Пустые'], @$filter['ext_id'], ['class' => 'form-control']) !!}
            </div>

            @if($properties->slug(config('properties.color')))
            <div class="input-group input-group-sm">
                {!! Form::label('color', 'Цвет') !!}
                {!! Form::select('filter[property]['.$properties->slug(config('properties.color'))->id.']', $properties->selectOptions(config('properties.color')), @$filter[property][$properties->slug(config('properties.color'))->id], ['class' => 'form-control']) !!}
            </div>
            @endif

            @if($properties->slug('obshchij-cvet'))
            <div class="input-group input-group-sm">
                {!! Form::label('color', 'Общий Цвет') !!}
                {!! Form::select('filter[property][17]', $properties->selectOptions('obshchij-cvet'), @$filter[property][$properties->slug('obshchij-cvet')->id], ['class' => 'form-control']) !!}
            </div>
            @endif
          </div>
        </div>
    </div>

    <div class="clearfix"></div>
{!! Form::close() !!}
@if(!empty($product))
{!! Form::model($product, ['files' => true, 'route' => ['admin.product.update', $product->id], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'put']) !!}
@else
{!! Form::open(['files' => true, 'route' => 'admin.product.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) !!}
@endif

<?php
$tabs = ['main', 'content', 'free_cources', 'cource_program', 'properties', 'gallery', 'seo'];
$active = 'main';
?>

    @include('backend.includes.partials.save-buttons', ['cancel' => route('admin.product.index'), 'preview' => (isset($product) ? product_url($product) : null)])

    <div>
      <!-- Nav tabs -->
      <ul class="nav nav-tabs" role="tablist">
        @foreach($tabs as $tab)
        <li role="presentation"<?php echo $tab === $active ? ' class="active"' : '' ?>>
            <a href="#{{ $tab }}" aria-controls="{{ $tab }}" role="tab" data-toggle="tab">{{ trans('product.tab.'.$tab) }}</a>
        </li>
        @endforeach
      </ul>

      <!-- Tab panes -->
      <div class="tab-content">
        @foreach($tabs as $tab)
        <div role="tabpanel" class="tab-pane<?php echo $tab === $active ? ' active' : '' ?>" id="{{ $tab }}">
            @include('backend.product.partials.form.'.$tab)
        </div>
        @endforeach
      </div>

    </div>

    @include('backend.includes.partials.save-buttons', ['cancel' => route('admin.product.index')])

    {!! Form::hidden('id', null, ['id' => 'product-id']) !!}
{!! Form::close() !!}

@section('after-scripts-end')
    @if(!empty($product))
        {!! Html::script('js/plugin/fine-uploader/jquery.fine-uploader.min.js') !!}

        <script>
            var imgMorph = {type: '{{ str_replace('\\', '\\\\', \App\Models\Product::class) }}', 'id': {{ $product->id }}}
        </script>

        {!! Html::script('js/backend/gallery.js') !!}
    @endif

    {!! Html::script('js/backend/menu.js') !!}

    {!! Html::script('js/plugin/ckeditor/ckeditor.js') !!}

    {!! Html::script('js/plugin/ckeditor/adapters/jquery.js') !!}

    <script>
    CKEDITOR.replace('description', {
        filebrowserBrowseUrl: "{{ route('elfinder.ckeditor') }}",
        removeDialogTabs: 'link:upload;image:upload',
        width: '100%'
    });
    </script>
@stop


@section('after-styles-end')
    {!! Html::style('js/plugin/fine-uploader/fine-uploader.min.css') !!}
    {!! Html::style('js/plugin/fine-uploader/fine-uploader-new.min.css') !!}
    {!! Html::style('js/plugin/fine-uploader/fine-uploader-gallery.min.css') !!}
@stop

<div class="form-group">
    <div class="col-lg-10">
        {!! Form::text('module', null, ['class' => 'search form-control', 'autocomplete' => 'off', 'placeholder' => 'введите наименование или артикул']) !!}
        <div id="module-status"></div>
    </div>

    <div class="col-lg-1">
        {!! Form::text('module_qtt', null, ['class' => 'form-control', 'id' => 'module_qtt', 'placeholder' => 'шт.']) !!}
    </div>

    <div class="col-lg-1">
        {!! Form::button('<i class="glyphicon glyphicon-plus"></i>', ['class' => 'add btn btn-success btn-sm']) !!}
    </div>
</div><!--form control-->
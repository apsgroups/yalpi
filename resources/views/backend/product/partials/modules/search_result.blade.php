{{ $product->title }} /

@if($product->sku)
{{ $product->sku }} /
@endif

@if($product->ext_id)
{{ $product->ext_id }} /
@endif

{{ $product->id }} /
{{ $product->slug }}
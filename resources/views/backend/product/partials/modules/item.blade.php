<tr>
    <td class="col-lg-1">{{ $module->id }}</td>
    <td class="col-lg-2">{!! link_to_route('admin.product.edit', $module->title, [$module->id], ['target' => '_blank']) !!}</td>
    <td class="col-lg-2">
        <span class="module-ext-id" data-attr-id="default">{{ $module->ext_id }}</span>
        @if($module->attributes)
            @foreach($module->attributes as $ma)
            <span class="hide module-ext-id" data-attr-id="{{ $ma->id }}">{{ $ma->ext_id }}</span>
            @endforeach
        @endif
    </td>
    <td class="col-lg-1">
        <span class="module-sku" data-attr-id="default">{{ $module->sku }}</span>
        @if($module->attributes)
            @foreach($module->attributes as $ma)
            <span class="hide module-sku" data-attr-id="{{ $ma->id }}">{{ $ma->getSku() }}</span>
            @endforeach
        @endif
    </td>
    @php($moduleAttributes = ['выбрать'] + $module->attributes->pluck('value', 'id')->toArray())
    <td class="col-lg-1">{!! Form::select('modules['.$module->id.'][product_attribute_id]', $moduleAttributes, $attribute, ['class' => 'module-attribute']) !!}</td>
    <td class="col-lg-1">{{ $module->propertyBySlug('width') .'*'. $module->propertyBySlug('height') .'*'. $module->propertyBySlug('depth')}}</td>
    <td class="col-lg-1">
        <span class="module-price-base" data-attr-id="default">{{ $module->price_base }}</span>
        @if($module->attributes)
            @foreach($module->attributes as $ma)
            <span class="hide module-price-base" data-attr-id="{{ $ma->id }}">{{ $ma->getPriceBase() }}</span>
            @endforeach
        @endif
    </td>
    <td class="col-lg-1">
        <span class="module-price" data-attr-id="default">{{ $module->price }}</span>
        @if($module->attributes)
            @foreach($module->attributes as $ma)
            <span class="hide module-price" data-attr-id="{{ $ma->id }}">{{ $ma->getPrice() }}</span>
            @endforeach
        @endif
    </td>
    <td class="col-lg-1">
        <span class="module-discount" data-attr-id="default">{{ $module->discount }}</span>
        @if($module->attributes)
            @foreach($module->attributes as $ma)
            <span class="hide module-discount" data-attr-id="{{ $ma->id }}">{{ $ma->discount }}</span>
            @endforeach
        @endif
    </td>
    <td class="col-lg-1">
        {!! Form::text('modules['.$module->id.'][quantity]', $quantity, [
            'class' => 'module-quantity form-control input-sm',
            'placeholder' => 'шт.',
            ]) !!}

        {!! Form::hidden('modules['.$module->id.'][module_id]', $module->id) !!}
    </td>
    <td class="col-lg-1">
        {!! $module->in_stock !!}
    </td>
    <td class="col-lg-1">
        {!! Form::button('<i class="glyphicon glyphicon-remove"></i>', ['class' => 'remove-row btn btn-danger btn-sm']) !!}
    </td>
</tr>
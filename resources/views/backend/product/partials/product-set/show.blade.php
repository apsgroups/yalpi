<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title" id="product-set-title">{{ $productSet->title }}</h3>

        <input type="hidden" name="product_set_id" id="product-set-id" value="{{ $productSet->id }}" />
    </div><!-- /.box-header -->

    <div class="box-body" id="set-products">

        <div class="form-group">
            <div class="col-lg-6">
                {!! Form::text('product_set[title]', $productSet->title, ['id' => 'set-edit-title', 'class' => 'form-control', 'placeholder' => 'введите наименование для набора']) !!}
            </div>
        </div>

        <div class="form-group">
            <div class="col-lg-6">
                {!! Form::select('product_set[property_id]', $properties, $productSet->property_id, ['id' => 'set-edit-property-id', 'class' => 'form-control']) !!}
            </div>
        </div>

        <div class="form-group">
            <div class="col-lg-6">
                {!! Form::button('<i class="glyphicon glyphicon-save"></i> Сохранить', ['class' => 'update-set btn btn-success btn-xs']) !!}
                {!! Form::button('<i class="glyphicon glyphicon-remove"></i> Удалить набор', ['class' => 'remove-set btn btn-danger btn-xs']) !!}
                {!! Form::button('<i class="glyphicon glyphicon-remove"></i> Закрыть набор', ['class' => 'close-set btn btn-warning btn-xs']) !!}
            </div>
        </div>
    </div><!-- /.box-body -->
</div><!--box-->

<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Добавьте товары в набор</h3>
    </div><!-- /.box-header -->

    <div class="box-body" id="set-products">
        <div class="form-group">
            <div class="col-lg-12">
                {!! Form::text('product-set-title', null, ['class' => 'search form-control', 'autocomplete' => 'off', 'placeholder' => 'введите наименование товара или артикул']) !!}
                <div id="product-set-status"></div>
            </div>
        </div>

        <table class="table table-striped table-bordered table-hover">
            <thead>
            <tr>
                <th>ID</th>
                <th>Наименование</th>
                <th>Удаление</th>
            </tr>
            </thead>

            <tbody id="set-table">
            @if($productSet->products->count())
                @foreach($productSet->products as $product)
                    @include('backend.product.partials.product-set.product')
                @endforeach
            @endif
            </tbody>
        </table>
    </div><!-- /.box-body -->
</div><!--box-->
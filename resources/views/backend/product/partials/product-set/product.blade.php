<tr>
    <td>{{ $product->id }}</td>
    <td>{!! link_to_route('admin.product.edit', $product->title, [$product->id], ['target' => '_blank']) !!}</td>
    <td>
        {!! Form::button('<i class="glyphicon glyphicon-remove"></i>', ['class' => 'remove-row btn btn-danger btn-sm', 'data-id' => $product->id]) !!}
    </td>
</tr>
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Описание</h3>
    </div><!-- /.box-header -->

    <div class="box-body">
        <div class="form-group">
            {!! Form::label('title', 'Наименование', ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-10">
                {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'наименование']) !!}
                {!! $errors->first('title', '<div class="text-danger">:message</div>') !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('title', 'Превью наименование', ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-10">
                {!! Form::text('preview_title', null, ['class' => 'form-control', 'placeholder' => 'Превью наименование']) !!}
                {!! $errors->first('preview_title', '<div class="text-danger">:message</div>') !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('slug', 'Псевдоним', ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-10">
                {!! Form::text('slug', null, ['class' => 'form-control', 'placeholder' => 'введите псевдоним для URL латинскими буквами, - вместо пробела']) !!}
                {!! $errors->first('slug', '<div class="text-danger">:message</div>') !!}
            </div>
        </div>

        <div class="form-group">
            <label class="col-lg-2 control-label">Связанные товары</label>
            <div class="col-lg-4">
                {!! Form::select('relateds[]', $relateds, array_keys($relateds->toArray()), [
                        'class' => 'search-products',
                        'multiple' => 'multiple'
                    ]) !!}
                {!! $errors->first('relateds', '<div class="text-danger">:message</div>') !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('video', 'Код YouTube', ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-10">
                {!! Form::text('video', null, ['class' => 'form-control', 'placeholder' => 'введите Youtube код видео']) !!}
                {!! $errors->first('video', '<div class="text-danger">:message</div>') !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('icon_pack', 'Пак иконок', ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-10">
                {!! Form::text('icon_pack', null, ['class' => 'form-control', 'placeholder' => 'Пак иконок']) !!}
                {!! $errors->first('icon_pack', '<div class="text-danger">:message</div>') !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('icon_name', 'Имя иконки', ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-10">
                {!! Form::text('icon_name', null, ['class' => 'form-control', 'placeholder' => 'Имя иконки']) !!}
                {!! $errors->first('icon_name', '<div class="text-danger">:message</div>') !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('image', 'Изображение', ['class' => 'col-lg-2 control-label']) !!}

            <div class="col-lg-3">
                {!! Form::file('image', ['class' => 'form-control']) !!}
                {!! $errors->first('image', '<div class="text-danger">:message</div>') !!}
            </div>

            @if(isset($product) && $product->image)
            <div class="col-lg-3">
                <div class="image-manager">
                    <div class="img-item" data-img-id="{{ $product->image->id }}">
                        <a href="{{ img_src($product->image, 'large') }}" class="fancybox" rel="gallery">
                            <img src="{{ img_src($product->image, 'preview') }}" class="img-rounded" />
                        </a>
                        <button class="remove-img btn btn-danger btn-xs" data-id="{{ $product->image->id }}"><i class="glyphicon glyphicon-trash"></i></button>
                        {{ Form::hidden('image_id', $product->image->id) }}
                    </div>
                </div>
            </div>
            @endif
        </div><!--form control-->

        <div class="form-group">
            <label class="col-lg-2 control-label">Родительская категория</label>
            <div class="col-lg-5">
                {!! Form::select('category_id', $categories, (isset($product) ? $product->category_id : null), [
                        'data-placeholder' => 'Выберите категорию',
                        'class' => 'wide-select chosen-select form-control'
                    ]) !!}
                {!! $errors->first('category_id', '<div class="text-danger">:message</div>') !!}
            </div>
        </div><!--form control-->

        <div class="form-group">
            <label class="col-lg-2 control-label"></label>
            <div class="col-lg-10">
                <a class="btn btn-primary" role="button" data-toggle="collapse" href="#additional-categories" aria-expanded="false" aria-controls="collapseExample">Категории <i class="glyphicon glyphicon-chevron-down"></i></a>
                <div class="collapse" id="additional-categories">
                  <div class="well">
                        @foreach($categories as $k => $v)
                            @continue(empty($k))
                            <label class="checkbox">
                                {!! Form::checkbox('categories[]', $k, (isset($product) ? in_array($k, $product->getCategoryIdList()) : null), ['class' => 'icheck']) !!}
                                {{ $v }}
                            </label>
                        @endforeach
                        {!! $errors->first('categories', '<div class="text-danger">:message</div>') !!}
                  </div>
                </div>
            </div>
        </div>
    </div><!-- /.box-body -->
</div><!--box-->
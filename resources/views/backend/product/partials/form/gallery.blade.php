<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Фотогалерея</h3>
    </div><!-- /.box-header -->

    <div class="box-body">

    @if(!empty($product))
        @include('backend.product.partials.gallery.uploader')

        <div class="image-manager">
        @foreach($product->images as $image)
            @include('backend.product.partials.gallery.item')
        @endforeach
        </div>
    @else
        <p>Сохраните товар, затем загрузите фото</p>
    @endif
    </div><!--box-->
</div>
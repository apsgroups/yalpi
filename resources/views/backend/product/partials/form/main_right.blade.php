<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Опции</h3>
    </div><!-- /.box-header -->

    <div class="box-body">
        <div class="checkbox">
            <label>
                {!! Form::hidden('published', 0) !!}
                {!! Form::checkbox('published', 1, (isset($product) && $product->published === 1)) !!}
                {!! $errors->first('published', '<div class="text-danger">:message</div>') !!}
                Опубликовать
            </label>
        </div><!--form control-->

        <div class="checkbox">
            <label>
                {!! Form::hidden('out_of_stock', 0) !!}
                {!! Form::checkbox('out_of_stock', 1, (isset($product) && $product->out_of_stock === 1)) !!}
                {!! $errors->first('out_of_stock', '<div class="text-danger">:message</div>') !!}
                Снят с продажи
            </label>
        </div><!--form control-->

        <div class="checkbox">
            <label>
                {!! Form::hidden('soon', 0) !!}
                {!! Form::checkbox('soon', 1, (isset($product) && $product->soon === 1)) !!}
                {!! $errors->first('soon', '<div class="text-danger">:message</div>') !!}
                Ярлык "Скоро"
            </label>
        </div><!--form control-->

        <div class="checkbox">
            <label>
                {!! Form::hidden('novelty', 0) !!}
                {!! Form::checkbox('novelty', 1, (isset($product) && $product->novelty === 1)) !!}
                {!! $errors->first('novelty', '<div class="text-danger">:message</div>') !!}
                Новинка
            </label>
        </div><!--form control-->

        <div class="checkbox">
            <label>
                {!! Form::hidden('sale', 0) !!}
                {!! Form::checkbox('sale', 1, (isset($product) && $product->sale === 1)) !!}
                {!! $errors->first('sale', '<div class="text-danger">:message</div>') !!}
                Распродажа
            </label>
        </div><!--form control-->

        <div class="checkbox">
            <label>
                {!! Form::hidden('bestseller', 0) !!}
                {!! Form::checkbox('bestseller', 1, (isset($product) && $product->bestseller === 1)) !!}
                {!! $errors->first('bestseller', '<div class="text-danger">:message</div>') !!}
                Лидер продаж
            </label>
        </div><!--form control-->

        <div class="checkbox">
            <label>
                {!! Form::hidden('online_learning', 0) !!}
                {!! Form::checkbox('online_learning', 1, (isset($product) && $product->online_learning === 1)) !!}
                {!! $errors->first('online_learning', '<div class="text-danger">:message</div>') !!}
                Онлайн обучение
            </label>
        </div>

        <div class="checkbox">
            <label>
                {!! Form::hidden('preorder', 0) !!}
                {!! Form::checkbox('preorder', 1, (isset($product) && $product->preorder === 1)) !!}
                {!! $errors->first('preorder', '<div class="text-danger">:message</div>') !!}
                На заказ
            </label>
        </div><!--form control-->

        <div class="checkbox">
            <label>
                {!! Form::hidden('sitemap', 0) !!}
                {!! Form::checkbox('sitemap', 1, (!isset($product) || $product->sitemap === 1)) !!}
                Карта сайта
            </label>
        </div>

        <div class="checkbox">
            <label>
                {!! Form::hidden('video_format', 0) !!}
                {!! Form::checkbox('video_format', 1, (!isset($product) || $product->video_format === 1)) !!}
                Видео шаблон
            </label>
        </div>
    </div><!-- /.box-body -->
</div><!--box-->
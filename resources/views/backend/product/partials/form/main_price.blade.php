<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Ценообразование</h3>
    </div><!-- /.box-header -->


    <div class="box-body">
        <div class="form-group">
            <label class="col-lg-3 control-label">Тип цены</label>
            <div class="col-lg-9">
                {!! Form::select('price_type', $priceTypes, null, ['class' => 'form-control']) !!}
                {!! $errors->first('price_type', '<div class="text-danger">:message</div>') !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('price_base', 'Цена', ['class' => 'col-lg-3 control-label']) !!}
            <div class="col-lg-9">
                {!! Form::text('price_base', null, ['class' => 'form-control', 'placeholder' => 'розничная цена, пример: 120.7']) !!}
                {!! $errors->first('price_base', '<div class="text-danger">:message</div>') !!}
            </div>
        </div><!--form control-->

        <div class="form-group">
            {!! Form::label('discount', 'Скидка', ['class' => 'col-lg-3 control-label']) !!}
            <div class="col-lg-9">
                {!! Form::text('discount', null, ['class' => 'form-control', 'placeholder' => 'в %, например: 15']) !!}
                {!! $errors->first('discount', '<div class="text-danger">:message</div>') !!}
            </div>
        </div><!--form control-->
    </div><!-- /.box-body -->
</div><!--box-->
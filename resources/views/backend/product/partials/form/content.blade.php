<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Содержимое</h3>
    </div><!-- /.box-header -->

    <div class="box-body">
        <div class="form-group">
            <label class="col-lg-2 control-label">Язык</label>
            <div class="col-lg-10">
                {!! Form::select('language_id', $languages, null, ['id' => 'language_id', 'class' => 'form-control']) !!}
            </div>
        </div>

        <div class="form-group">
            <label class="col-lg-2 control-label">Начать учиться (URL)</label>
            <div class="col-lg-10">
                {!! Form::text('start_learning', null, ['id' => 'start_learning', 'class' => 'form-control', 'placeholder' => 'URL']) !!}
            </div>
        </div>

        <div class="form-group">
            <label class="col-lg-2 control-label">Рейтинг</label>

            <div class="col-lg-2">
                {{ Form::select('rate_round', $rateSelect, null, ['class' => 'form-control', 'placeholder' => '- выбрать -']) }}
            </div>
        </div>

        <div class="form-group">
            <label class="col-lg-2 control-label">Авторы</label>
            <div class="col-lg-4">
                {!! Form::select('authors[]', (isset($authors) ? $authors : []), (isset($authors) ? array_keys($authors->toArray()) : []), [
                        'class' => 'search-author',
                        'data-placeholder' => 'Найти автора...',
                        'data-token-order' => json_encode($authors),
                        'multiple' => 'multiple'
                    ]) !!}
                {!! $errors->first('authors', '<div class="text-danger">:message</div>') !!}
            </div>
        </div>

        <?php

        if(!count($features)) {
            $feature = new stdClass();
            $feature->id = 0;
            $feature->text = '';
            $feature->icon = '';
            $feature->ordering = 0;

            $features[] = $feature;
        } else {
            $features = $product->features()->orderBy('ordering', 'asc')->get();
        }

        ?>

        <div class="repeatable property-list">
            @if(!empty($features))
                @foreach($features as $i => $row)
                <div class="form-group repeatable-row">
                    {!! Form::label('features', 'Триггеры', ['class' => 'col-lg-2 control-label']) !!}

                    <div class="col-lg-1">
                      <span class="handle"><i class="fa fa-bars"></i></span>
                    </div>

                    <div class="col-lg-1">
                      {!! Form::text('features['.$i.'][ordering]', old('features.'.$i.'.ordering', (isset($row->ordering) ? $row->ordering : null)), ['class' => 'form-control order-field', 'placeholder' => 'Порядок']) !!}
                    </div>

                    <div class="col-lg-2">
                        {{ Form::select('features['.$i.'][icon]', $featureIcons, old('features.'.$i.'.icon', $row->icon), ['class' => 'form-control', 'placeholder' => '- выбрать -']) }}
                    </div>

                    <div class="col-lg-3">
                        {{ Form::text('features['.$i.'][text]', old('features.'.$i.'.text', $row->text), ['class' => 'form-control', 'placeholder' => 'Текст']) }}
                    </div>

                    <div class="col-lg-2">
                        {!! Form::button('<i class="glyphicon glyphicon-remove"></i>', ['class' => 'remove-row btn btn-danger btn-sm']) !!}
                        {!! Form::button('<i class="glyphicon glyphicon-plus"></i>', ['class' => 'add btn btn-success btn-sm']) !!}
                        {!! Form::hidden('features['.$i.'][id]', $row->id) !!}
                    </div><!-- /.col-lg-6 -->
                </div>
                @endforeach
            @endif
        </div>

        <div class="form-group">
            <label class="col-lg-2 control-label">Что ты получишь?</label>
            <div class="col-lg-10">
                {!! Form::textarea('experience', null, ['id' => 'experience', 'class' => 'form-control', 'placeholder' => 'Разделяйте новой строкой']) !!}
            </div>
        </div>

        <div class="form-group">
            <label class="col-lg-2 control-label">Описание</label>
            <div class="col-lg-10">
                {!! Form::textarea('description', null, [
                        'id' => 'description',
                        'class' => 'wysiwyg form-control'
                    ]) !!}
            </div>
        </div>
    </div><!-- /.box-body -->
</div><!--box-->
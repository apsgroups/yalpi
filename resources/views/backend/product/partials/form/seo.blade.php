<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">SEO meta</h3>
    </div>

    <div class="box-body">
        <div class="checkbox">
            <label>
                {!! Form::hidden('override_meta', 0) !!}
                {!! Form::checkbox('override_meta', 1, (isset($product) && $product->override_meta === 1)) !!}
                {!! $errors->first('override_meta', '<div class="text-danger">:message</div>') !!}
                Перекрыть мета-шаблон категории, если задан
            </label>
        </div>
    </div>
</div>

@include('backend.includes.partials.form.seo')
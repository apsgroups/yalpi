<div class="row">
    <div class="col-lg-8">
        @include('backend.product.partials.form.main_left')
    </div>

    <div class="col-lg-4">
        @include('backend.product.partials.form.main_price')
        @include('backend.product.partials.form.main_right')
    </div>
</div>
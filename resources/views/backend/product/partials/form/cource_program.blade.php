<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Программа урока</h3>
    </div><!-- /.box-header -->

    <div class="box-body">
        @include('backend.product.partials.form._cources', ['name' => 'cource_program', 'items' => $courceProgram, 'title' => 'Программа урока'])
    </div><!-- /.box-body -->
</div><!--box-->
<div class="well" style="padding: 20px 40px;">
    @foreach($categories as $k => $v)
        @continue(empty($k))
        <label class="checkbox">
            {!! Form::checkbox('categories[]', $k, (isset($product) ? in_array($k, $product->getCategoryIdList()) : null), ['class' => 'icheck']) !!}
            {{ $v }}
        </label>
    @endforeach
    {!! $errors->first('categories', '<div class="text-danger">:message</div>') !!}
</div>
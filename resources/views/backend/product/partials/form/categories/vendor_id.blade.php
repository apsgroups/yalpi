{!! Form::select('vendor_id', $vendors, (isset($product) ? $product->vendor_id : null), [
        'data-placeholder' => 'Выберите производителя',
        'class' => 'wide-select chosen-select form-control'
    ]) !!}
{!! $errors->first('vendor_id', '<div class="text-danger">:message</div>') !!}
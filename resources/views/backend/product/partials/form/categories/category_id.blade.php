{!! Form::select('category_id', $categories, (isset($product) ? $product->category_id : null), [
        'data-placeholder' => 'Выберите категорию',
        'class' => 'wide-select chosen-select form-control'
    ]) !!}
{!! $errors->first('category_id', '<div class="text-danger">:message</div>') !!}
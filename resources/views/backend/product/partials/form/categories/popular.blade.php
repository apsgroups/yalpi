{!! Form::select('populars[]', $categories, (isset($populars) ? $populars : null), [
        'data-placeholder' => 'Выберите категорию',
        'class' => 'wide-select chosen-select form-control',
        'multiple' => 'multiple',
    ]) !!}
{!! $errors->first('populars', '<div class="text-danger">:message</div>') !!}
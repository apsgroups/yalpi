<?php

if(!$items || $items->isEmpty()) {
    $item = new stdClass();
    $item->id = 0;
    $item->title = '';
    $item->desc = '';
    $item->ordering = 0;
    $item->video = '';
    $item->image = null;

    $items[] = $item;
}

?>

<div class="repeatable property-list">
    @if(!empty($items))
        @foreach($items as $i => $row)
        <div class="form-group repeatable-row">
            <div class="col-lg-1">
              {!! Form::text($name.'['.$i.'][ordering]', old($name.'.'.$i.'.ordering', (isset($row->ordering) ? $row->ordering : null)), ['class' => 'form-control order-field', 'placeholder' => 'Порядок']) !!}
            </div>

            <div class="col-lg-5">
                <p>
                    {{ Form::text($name.'['.$i.'][title]', old($name.'.'.$i.'.title', $row->title), ['class' => 'form-control', 'placeholder' => 'Заголовок']) }}
                </p>

                <p>
                    {{ Form::text($name.'['.$i.'][video]', old($name.'.'.$i.'.title', $row->video), ['class' => 'form-control', 'placeholder' => 'Код YouTube']) }}
                </p>

                <p>
                    {{ Form::textarea($name.'['.$i.'][desc]', old($name.'.'.$i.'.desc', $row->desc), ['class' => 'form-control', 'placeholder' => 'Описание']) }}
                </p>
            </div>

            <div class="col-lg-2">
                {{ Form::file($name.'['.$i.'][img]', null) }}

                @if($row->image)
                    <div style="margin-top: 30px" class="no-clone"><img src="{{ img_src($row->image, 'preview') }}" alt=""></div>
                @endif
            </div>

            <div class="col-lg-1">
                {!! Form::button('<i class="glyphicon glyphicon-remove"></i>', ['class' => 'remove-row btn btn-danger btn-sm']) !!}
                {!! Form::button('<i class="glyphicon glyphicon-plus"></i>', ['class' => 'add btn btn-success btn-sm']) !!}
                <span class="handle"><i class="fa fa-bars"></i></span>
                {!! Form::hidden($name.'['.$i.'][id]', $row->id) !!}
            </div><!-- /.col-lg-6 -->
        </div>
        @endforeach
    @endif
</div>
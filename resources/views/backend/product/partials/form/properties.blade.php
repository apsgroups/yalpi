    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Свойства</h3>
        </div><!-- /.box-header -->

        <div class="box-body">

            <div class="form-group">
                {!! Form::label('product_type_id', 'Тип', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-4">
                    {!! Form::select('product_type_id', $types, (isset($product) ? $product->product_type_id : []), ['class' => 'form-control']) !!}
                    {!! $errors->first('product_type_id', '<div class="text-danger">:message</div>') !!}
                </div>
            </div><!--form control-->

            @foreach($properties as $property)
                @if (isset($product) && !$product->type->properties->contains($property->id))
                    @continue
                @endif
                
                @if(view()->exists('backend.property.elements.'.$property->type.'.edit'))
                    @include('backend.property.elements.'.$property->type.'.edit')
                @endif
            @endforeach
        </div><!-- /.box-body -->
    </div><!--box-->
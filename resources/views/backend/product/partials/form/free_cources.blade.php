<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Начни бесплатно</h3>
    </div><!-- /.box-header -->

    <div class="box-body">
        @include('backend.product.partials.form._cources', ['name' => 'free_cources', 'items' => $freeCources, 'title' => 'Начни бесплатно'])
    </div>
</div><!--box-->
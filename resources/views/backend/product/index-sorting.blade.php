@extends('backend.layouts.master')

@section('title', 'Товары')

@section('content')
    @inject('properties', 'App\Services\Properties')
    @inject('sort', 'App\Services\Sort')

    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Управление товарами</h3>

            <div class="box-tools pull-right">
                @include('backend.product.partials.header-buttons')
            </div>
        </div><!-- /.box-header -->

        <div class="box-body">
            @include('backend.product.partials.filter')

            <p>&nbsp;</p>

            @include('backend.includes.partials.check.action', ['scope' => 'product'])

            <p>&nbsp;</p>

            @if(request('sort') === 'sets_ordering')
                <div class="alert alert-warning">
                    <b>Внимание!</b> В этом режиме сортировки обязательно используйте правильную категорию в фильтре.<br />
                    Если настроить сортировку с фильтром Категория=Диваны, а затем с Категория=Прямые диваны, то значения у товаров в категории "Диваны" перезапишутся.
                </div>
            @endif
            
            {!! Form::open(['files' => true, 'route' => 'admin.product.'.(request('sort') === 'sets_ordering' ? 'sets_ordering' : 'sort'), 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) !!}
            <div class="table-responsive">
                <table class="table sortable-sets-ordering table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>{!! $sort->route('admin.product.index', 'id', 'ID') !!}</th>
                        <th>{!! $sort->route('admin.product.index', 'title', 'Наименование') !!}</th>
                        <th>{!! $sort->route('admin.product.index', 'price', 'Цена') !!}</th>
                        <th>Приоритет в карточке</th>
                        <th class="text-center">Опубликовано</th>
                        <th class="text-center">
                            @if(request('sort') === 'sets_ordering')
                                {!! $sort->route('admin.product.index', 'sets_ordering', 'Сорт.комп.') !!}
                            @else
                                {!! $sort->route('admin.product.index', 'category', 'Сорт. в категории') !!}
                            @endif
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach ($products as $product)
                            <tr id="item-{{ $product->id }}">
                                <td>{{ $product->id }}</td>
                                <td>{!! link_to_route('admin.product.edit', $product->title, [$product->id]) !!}</td>
                                <td>{{ $product->price }}</td>
                                <td>{{ $product->order_priority }}</td>
                                <td class="text-center">@include('backend.includes.partials.toggle', ['model' => $product, 'toggle' => 'published'])</td>
                                <td class="text-center">
                                    @if(request('sort') === 'sets_ordering')
                                        {{ Form::text('items['.$product->id.']', $product->sets_ordering, ['size' => 3, 'class' => 'form-control order-field']) }}</td>
                                    @else
                                        {{ Form::text('items['.$product->id.']', $product->category_ordering, ['size' => 3, 'class' => 'form-control order-field']) }}</td>
                                    @endif
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            
            <button type="submit" class="btn btn-success" style="position: fixed;border: 0;z-index: 1000;left: 50%;bottom: 15px;">
                <i class="fa fa-save"></i> Сохранить
            </button>

            @if(isset($filter['category_id']))
                {{ Form::hidden('category_id', $filter['category_id']) }}
            @endif

            {!! Form::close() !!}
        </div><!-- /.box-body -->
    </div><!--box-->
@stop
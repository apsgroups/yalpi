@extends ('backend.layouts.master')

@section ('title', 'Добавление товара')

@section('page-header')
    <h1>
        Добавление товара
    </h1>
@endsection

@section('content')
    @include('backend.product.partials.form')
@stop
@extends ('backend.layouts.master')

@section ('title', 'Изменение товара: '.$product->title)

@section('page-header')
    <h1>
        Изменение товара &mdash; {{ $product->title }}
    </h1>

    @if($product->comments->count() > 0)
        <i class="fa fa-star-half-o" aria-hidden="true"></i> {!! link_to_route('admin.comment.index', '(Отзывы о товаре &mdash; '.$product->comments->count().')', ['item_id' => $product->id, 'scope' => 'product'], ['target' => '_blank']) !!}
    @endif
@endsection

@section('content')
    @include('backend.product.partials.form')
@stop
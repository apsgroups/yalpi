@extends('backend.layouts.master')

@section('title', 'Товары')

@section('content')
    @inject('properties', 'App\Services\Properties')
    @inject('sort', 'App\Services\Sort')

    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Управление товарами</h3>

            <div class="box-tools pull-right">
                @include('backend.product.partials.header-buttons')
            </div>
        </div><!-- /.box-header -->


        <div class="box-body">
            @include('backend.product.partials.filter')

            <p>&nbsp;</p>

            @include('backend.includes.partials.check.action', ['scope' => 'product'])

            <p>&nbsp;</p>

            <div class="table-responsive">
                <table class="table {{ (((request()->input('sort') === 'category' || request()->input('sort') === 'sets_ordering') && request()->input('dir', 'desc') === 'asc') ? 'sortable' : '') }} {{ (request()->input('sort') === 'sets_ordering' && request()->input('dir', 'desc') === 'asc' ? 'sets-ordering' : '') }} table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>{!! $sort->route('admin.product.index', 'category', '<i class="fa fa-sort"></i>') !!}</th>
                        @include('backend.includes.partials.check.all')
                        <th>{!! $sort->route('admin.product.index', 'id', 'ID') !!}</th>
                        <th>{!! $sort->route('admin.product.index', 'title', 'Наименование') !!}</th>
                        <th>{!! $sort->route('admin.product.index', 'category_id', 'Категория') !!}</th>
                        <th>{!! $sort->route('admin.product.index', 'price', 'Цена') !!}</th>
{{--                        <th>{!! $sort->route('admin.product.index', 'property_id', 'Цвет', ['property_id' => $properties->slug(config('properties.color'))->id ]) !!}</th>--}}
                        {{--<th>Тип</th>--}}
                        <th class="text-center">{!! $sort->route('admin.product.index', 'published', 'Публ.') !!}</th>
                        <th class="text-center">Ярлык Скоро</th>
                        {{--<th class="text-center">{!! $sort->route('admin.product.index', 'sitemap', 'Sitemap') !!}</th>--}}
                        {{--<th class="text-center">{!! $sort->route('admin.product.index', 'market', 'Маркет') !!}</th>--}}
                        {{--<th class="text-center">{!! $sort->route('admin.product.index', 'import', 'Импорт') !!}</th>--}}
                        <th class="text-center">{!! $sort->route('admin.product.index', 'sets_ordering', 'Сорт.комп.') !!}</th>
                        <th class="text-center">{!! $sort->route('admin.product.index', 'created_at', 'Создано') !!}</th>
{{--                        <th class="text-center">{!! $sort->route('admin.product.index', 'updated_at', 'Обновлено') !!}</th>--}}
                        <th class="text-center">Удалить</th>
                        <th class="text-center">Фронт</th>
                        <th class="text-center">Скидка</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach ($products as $product)
                            <tr id="item-{{ $product->id }}" {!! ($product->took_import && !$product->updated_by_import ? 'style="background: #ffcfcf;"' : '') !!}>
                                <td>{{ $product->category_ordering }}</td>
                                @include('backend.includes.partials.check.item', ['id' => $product->id])
                                <td>{{ $product->id }}</td>
                                <td>{!! link_to_route('admin.product.edit', $product->title, [$product->id]) !!}</td>
                                <td>
                                    @if($product->category)
                                    {!! link_to_route('admin.category.edit', $product->category->title, [$product->category->id]) !!}
                                    @endif
                                </td>
                                <td>{{ $product->price }}</td>
{{--                                <td>{{ $properties->getValueProduct($product, config('properties.color')) }}</td>--}}
                                {{--<td>{{ @$product->type->title }}</td>--}}
                                <td class="text-center">
                                    @include('backend.includes.partials.toggle', ['model' => $product, 'toggle' => 'published'])
                                </td>
                                <td class="text-center">
                                    @include('backend.includes.partials.toggle', ['model' => $product, 'toggle' => 'soon'])
                                </td>
                                {{--<td class="text-center">--}}
                                    {{--@include('backend.includes.partials.toggle', ['model' => $product, 'toggle' => 'sitemap'])--}}
                                {{--</td>--}}
                                {{--<td class="text-center">--}}
{{--                                    @include('backend.includes.partials.toggle', ['model' => $product, 'toggle' => 'yandex_sitemap'])--}}
                                {{--</td>--}}
                                {{--<td class="text-center">--}}
                                    {{--@include('backend.includes.partials.toggle', ['model' => $product, 'toggle' => 'google_sitemap'])--}}
                                {{--</td>--}}
                                {{--<td class="text-center">--}}
                                    {{--@include('backend.includes.partials.toggle', ['model' => $product, 'toggle' => 'market'])--}}
                                {{--</td>--}}
                                {{--<td class="text-center">--}}
                                    {{--@include('backend.includes.partials.toggle', ['model' => $product, 'toggle' => 'import'])--}}
                                {{--</td>--}}
                                <td class="text-center">{{ $product->sets_ordering }}</td>
                                <td class="text-center">{!! $product->created_at->format(config('backend.date.format')) !!}</td>
{{--                                <td class="text-center">{!! $product->updated_at->format(config('backend.date.format')) !!}</td>--}}
                                <td class="text-center">
                                    @include('backend.includes.partials.destroy', ['link' => route('admin.product.destroy', $product->id)])
                                </td>
                                <td class="text-center">
                                    <a href="{{ product_url($product) }}" target="_blank" class="btn btn-xs btn-info"><i class="fa fa-search"></i></a>
                                </td>
                                <td class="text-center">
                                    {{ $product->virtual_discount }} / {{ $product->virtual_discount_id }}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

            @include('backend.includes.partials.pagination', ['items' => $products])
        </div><!-- /.box-body -->
    </div><!--box-->
@stop
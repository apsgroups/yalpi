@extends('backend.layouts.master')

@section('title', 'Добавление типа для контента')

@section('page-header')
    <h1>
        Добавление типа для контента
    </h1>
@endsection

@section('content')
    @include('backend.content-types.partials.form')
@stop
@if(!empty($item))
{!! Form::model($item, ['route' => ['admin.content-type.update', $item->id], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'put']) !!}
@else
{!! Form::open(['route' => 'admin.content-type.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) !!}
@endif

<?php
$tabs = ['main', 'seo'];
$active = 'main';
?>

    @include('backend.includes.partials.save-buttons', ['cancel' => route('admin.content-type.index')])

    <div>
      <!-- Nav tabs -->
      <ul class="nav nav-tabs" role="tablist">
        @foreach($tabs as $tab)
        <li role="presentation"<?php echo $tab === $active ? ' class="active"' : '' ?>>
            <a href="#{{ $tab }}" aria-controls="{{ $tab }}" role="tab" data-toggle="tab">{{ trans('content.tab.'.$tab) }}</a>
        </li>
        @endforeach
      </ul>

      <!-- Tab panes -->
      <div class="tab-content">
        @foreach($tabs as $tab)
        <div role="tabpanel" class="tab-pane<?php echo $tab === $active ? ' active' : '' ?>" id="{{ $tab }}">
            @include('backend.content-types.partials.form.'.$tab)
        </div>
        @endforeach
      </div>

    </div>

    @include('backend.includes.partials.save-buttons', ['cancel' => route('admin.content-type.index')])

    {!! Form::hidden('id') !!}
{!! Form::close() !!}
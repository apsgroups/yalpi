<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Основное</h3>
    </div><!-- /.box-header -->

    <div class="box-body">
        <div class="form-group">
            {!! Form::label('title', 'Наименование', ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-10">
                {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'наименование']) !!}
                {!! $errors->first('title', '<div class="text-danger">:message</div>') !!}
            </div>
        </div><!--form control-->

        <div class="form-group">
            {!! Form::label('slug', 'Псевдоним', ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-10">
                {!! Form::text('slug', null, ['class' => 'form-control', 'placeholder' => 'введите псевдоним для URL латинскими буквами, - вместо пробела']) !!}
                {!! $errors->first('slug', '<div class="text-danger">:message</div>') !!}
            </div>
        </div><!--form control-->

        <div class="form-group">
            {!! Form::label('per_page', 'Количество элементов на странице', ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-10">
                {!! Form::text('per_page', null, ['class' => 'form-control', 'placeholder' => 'Количество элементов на странице']) !!}
                {!! $errors->first('per_page', '<div class="text-danger">:message</div>') !!}
            </div>
        </div><!--form control-->

        <div class="form-group">
            <label class="col-lg-2 control-label">Опубликовано</label>
            <div class="col-lg-1">
                {!! Form::hidden('published', 0) !!}
                {!! Form::checkbox('published', 1, (isset($item) && $item->published === 1)) !!}
                {!! $errors->first('published', '<div class="text-danger">:message</div>') !!}
            </div>
        </div><!--form control-->

        <div class="form-group">
            <label class="col-lg-2 control-label">Карта сайта</label>
            <div class="col-lg-1">
                {!! Form::hidden('sitemap', 0) !!}
                {!! Form::checkbox('sitemap', 1, (!isset($item) || $item->sitemap === 1)) !!}
            </div>
        </div>

        <div class="form-group">
            <label class="col-lg-2 control-label">Включить товары категории</label>
            <div class="col-lg-1">
                {!! Form::hidden('sitemap_items', 0) !!}
                {!! Form::checkbox('sitemap_items', 1, (!isset($item) || $item->sitemap_items === 1)) !!}
            </div>
        </div>

        <div class="form-group">
            <label class="col-lg-2 control-label">В меню администратора</label>
            <div class="col-lg-1">
                {!! Form::hidden('admin_menu', 0) !!}
                {!! Form::checkbox('admin_menu', 1, (isset($item) && $item->admin_menu === 1)) !!}
                {!! $errors->first('admin_menu', '<div class="text-danger">:message</div>') !!}
            </div>
        </div><!--form control-->

        <div class="form-group">
            {!! Form::label('description', 'Описание', ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-10">
                {!! Form::textarea('description', null, ['class' => 'form-control', 'placeholder' => 'описание']) !!}
                {!! $errors->first('description', '<div class="text-danger">:message</div>') !!}
            </div>
        </div><!--form control-->

    </div><!-- /.box-body -->
</div><!--box-->
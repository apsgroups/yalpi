@extends('backend.layouts.master')

@section('title', 'Типы контента')

@section('page-header')
    <h1>
        Управление типами контента
    </h1>
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Список типов контента</h3>

            <div class="box-tools pull-right">
                @include('backend.content-types.partials.header-buttons')
            </div>
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Наименование</th>
                        <th>Псевдоним</th>
                        <th class="text-center">Создано</th>
                        <th class="text-center">Обновлено</th>
                        <th class="text-center">Опубликован</th>
                        <th class="text-center">Sitemap</th>
                        <th class="text-center">Элементы в sitemap</th>
                        <td class="text-center">Удалить</td>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach ($items as $item)
                            <tr>
                                <td>{{ $item->id }}</td>
                                <td>{!! link_to_route('admin.content-type.edit', $item->title, [$item->id]) !!}</td>
                                <td>{{ $item->slug }}</td>
                                <td class="text-center">{{ $item->created_at->format(config('backend.date.format')) }}</td>
                                <td class="text-center">{{ $item->updated_at->format(config('backend.date.format')) }}</td>
                                <td class="text-center">
                                    @include('backend.includes.partials.toggle', ['route' => ['admin.content-type.toggle', $item->id], 'model' => $item, 'toggle' => 'published'])
                                </td>
                                <td class="text-center">
                                    @include('backend.includes.partials.toggle', ['route' => ['admin.content-type.toggle', $item->id], 'model' => $item, 'toggle' => 'sitemap'])
                                </td>
                                <td class="text-center">
                                    @include('backend.includes.partials.toggle', ['route' => ['admin.content-type.toggle', $item->id], 'model' => $item, 'toggle' => 'sitemap_items'])
                                </td>
                                <td class="text-center">
                                    @include('backend.includes.partials.destroy', ['link' => route('admin.content-type.destroy', $item->id)])
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

            @include('backend.includes.partials.pagination', ['items' => $items])
        </div><!-- /.box-body -->
    </div><!--box-->
@stop
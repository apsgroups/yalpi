@extends ('backend.layouts.master')

@section ('title', 'Изменение типа контента: '.$item->title)

@section('page-header')
    <h1>
        Изменение типа {{ $item->title }}
    </h1>
@endsection

@section('content')
    @include('backend.content-types.partials.form')
@stop
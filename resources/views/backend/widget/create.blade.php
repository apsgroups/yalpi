@extends ('backend.layouts.master')

@section ('title', 'Добавление виджета')

@section('page-header')
    <h1>
        Добавление виджета
    </h1>
@endsection

@section('content')
    @include('backend.widget.partials.form')
@stop
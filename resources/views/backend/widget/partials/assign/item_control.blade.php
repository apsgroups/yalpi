<div style="display:none;" id="treeselectmenu">
    <div class="pull-left nav-hover treeselect-menu">
        <div class="btn-group">
            <a href="#" data-toggle="dropdown" class="btn-default dropdown-toggle btn btn-micro">
                <span class="caret"></span>
            </a>
            <ul class="dropdown-menu">
                <li class="nav-header">Вложенные элементы:</li>
                <li class="divider"></li>
                <li class="">
                    <a class="checkall" href="javascript://"><i class="fa fa-check-square-o"></i> Выбрать</a>
                </li>
                <li>
                    <a class="uncheckall" href="javascript://"><i class="fa fa-square-o"></i> Снять выделение</a>
                </li>

                <div class="treeselect-menu-expand">
                    <li class="divider"></li>
                    <li><a class="expandall" href="javascript://"><i class="fa fa-minus-square"></i> Развернуть</a></li>
                    <li><a class="collapseall" href="javascript://"><i class="fa fa-plus-square"></i> Свернуть</a></li>
                </div>
            </ul>
        </div>
    </div>
</div>
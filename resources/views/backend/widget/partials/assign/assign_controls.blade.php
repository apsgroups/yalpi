<div class="form-inline">
    <span class="small">Выбрать:
        <a id="treeCheckAll" href="javascript://">Все</a>,
        <a id="treeUncheckAll" href="javascript://">Нет</a>
    </span>

    <span class="width-20">|</span>

    <span class="small">Развернуть:
        <a id="treeExpandAll" href="javascript://">Все</a>,
        <a id="treeCollapseAll" href="javascript://">Нет</a>
    </span>

    <input type="text" id="treeselectfilter" name="treeselectfilter" class="input-medium search-query pull-right form-control" placeholder="Поиск" />
</div>
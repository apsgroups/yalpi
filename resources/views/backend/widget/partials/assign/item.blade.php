<ul class="treeselect-sub">
    @foreach($items as $item)
        <li>
            <div class="treeselect-item pull-left">
                @php($checked = null)

                @if(isset($widget) && $widget->assigns->contains('menu_item_id', $item['item']->id))
                    @php($checked = true)
                @endif

                @php($attr = ['class' => 'pull-left', 'id' => 'assign_'.$item['item']->id])

                @if(!in_array($item['item']->type, config('menu.assign_enabled')))
                    @php($attr['disabled'] = 'disabled')
                @endif

                {{ Form::checkbox('assign_items[]', $item['item']->id, $checked, $attr) }}
                <label for="assign_{{ $item['item']->id }}" class="pull-left">{{ $item['item']->title }} <span class="small">({{ $item['item']->slug }})</span></label>
            </div>

            <div class="clearfix"></div>

            @if(!empty($item['childs']))
                @include('backend.widget.partials.assign.item', ['items' => $item['childs']])
            @endif
        </li>
    @endforeach
</ul>
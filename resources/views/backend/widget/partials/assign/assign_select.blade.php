<div class="form-group">
    {!! Form::label('assign', 'Привязка модуля', ['class' => 'col-lg-2 control-label', 'id' => 'menus-lbl']) !!}
    <div class="col-lg-4">
        {!! Form::select('assign', [
            'На всех страницах',
            'Ни на одной странице',
            'Только на указанных страницах',
            'На всех страницах, кроме указанных'
        ], null, ['id' => 'form-assignment', 'class' => 'form-control']) !!}
        {!! $errors->first('assign', '<div class="text-danger">:message</div>') !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('on_product', 'В карточке товара', ['class' => 'col-lg-2 control-label']) !!}
    <div class="col-lg-4">
        {!! Form::hidden('on_product', 0) !!}
        {!! Form::checkbox('on_product', 1, (isset($widget) && $widget->on_product === 1)) !!}
        {!! $errors->first('on_product', '<div class="text-danger">:message</div>') !!}
    </div>
</div><!--form control-->
<div class="form-group">
    <label class="col-lg-2 control-label">Меню</label>
    <div class="col-lg-4">
        {!! Form::select('params[menu][menu_id]', $menus, (isset($selected) ? $selected : null), [
                'data-placeholder' => 'Выберите меню',
                'class' => 'wide-select chosen-select form-control'
            ]) !!}
    </div>
</div><!--form control-->

<div class="form-group">
    <label class="col-lg-2 control-label">Шаблон</label>
    <div class="col-lg-4">
        {!! Form::select('params[menu][layout]', $layouts, (isset($layout) ? $layout : null), [
                'data-placeholder' => 'Выберите шаблон',
                'class' => 'form-control'
            ]) !!}
    </div>
</div><!--form control-->
<div class="form-group">
    <label class="col-lg-2 control-label">Промо вкладки</label>

    <div class="col-lg-4">

        <div class="repeatable promo-tab">
            {!! Form::button('<i class="glyphicon glyphicon-plus"></i>', ['class' => 'add last btn btn-success input-sm']) !!}

            @if($tabs->count())
                @foreach($tabs as $tab)
                <div class="repeatable-row">

                    <div class="field-item">
                    {!! Form::text('params[promo]['.$tab->id.'][title]', $tab->title, [
                        'class' => 'form-control',
                        'placeholder' => 'введите наименование'
                        ]) !!}
                    </div>
                    <div class="field-item">
                    {!! Form::text('params[promo]['.$tab->id.'][ordering]', $tab->ordering, [
                            'class' => 'form-control order-field',
                            'placeholder' => 'порядковый номер'
                        ]) !!}
                    </div>
                    <div class="field-item">
                    {!! Form::select('params[promo]['.$tab->id.'][categories][]', $categories, $tab->categories->pluck('id')->toArray(), [
                            'data-placeholder' => 'Выберите категорию',
                            'class' => 'wide-select form-control',
                            'multiple' => 'multiple',
                        ]) !!}
                    </div>
                    <div class="field-item">
                      {!! Form::button('<i class="glyphicon glyphicon-remove"></i>', ['class' => 'remove-row btn btn-danger btn-sm']) !!}
                      {!! Form::button('<i class="glyphicon glyphicon-plus"></i>', ['class' => 'add btn btn-success btn-sm']) !!}
                      <div class="btn btn-info handle "><i class="fa fa-list"></i></div>
                      {!! Form::hidden('params[promo]['.$tab->id.'][id]', $tab->id) !!}
                    </div><!-- /.col-lg-6 -->
                </div>
                @endforeach
            @else
                <div class="repeatable-row">
                    <div class="field-item">
                    {!! Form::text('params[promo][0][title]', null, [
                        'class' => 'form-control',
                        'placeholder' => 'введите наименование'
                        ]) !!}
                    </div>
                    <div class="field-item">
                    {!! Form::select('params[promo][0][categories][]', $categories, [], [
                            'data-placeholder' => 'Выберите категорию',
                            'class' => 'wide-select form-control',
                            'multiple' => 'multiple',
                        ]) !!}
                    </div>
                    <div class="field-item">
                    {!! Form::hidden('params[promo][0][ordering]', 0, [
                            'class' => 'form-control order-field',
                            'placeholder' => 'порядковый номер'
                        ]) !!}
                    </div>
                    <div class="field-item">
                      {!! Form::button('<i class="glyphicon glyphicon-remove"></i>', ['class' => 'remove-row btn btn-danger btn-sm']) !!}
                      {!! Form::button('<i class="glyphicon glyphicon-plus"></i>', ['class' => 'add btn btn-success btn-sm']) !!}
                      {!! Form::hidden('params[promo][0][id]', 0) !!}
                    </div>
                </div>
            @endif
        </div>
    </div>
</div>
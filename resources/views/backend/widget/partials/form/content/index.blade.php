<div class="form-group">
    <label class="col-lg-2 control-label">Элемент контента</label>

    <div class="col-lg-3">
        {!! Form::text('content', null, [
            'class' => 'content-search search form-control',
            'autocomplete' => 'off',
            'placeholder' => 'введите наименование/псевдоним/id'
            ]) !!}

        <span class="content-title">
            @if(isset($content))
                {{ $content->title }} / {{ $content->slug }} / {{ $content->id }}
            @endif
        </span>
    </div>

    <div class="col-lg-1">
        {!! Form::text('params[content][content_id]', (isset($content) ? $content->id : null), ['readonly' => 'readonly', 'class' => 'content-id form-control']) !!}
    </div>
</div>
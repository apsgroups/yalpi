    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Основное</h3>
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="form-group">
                {!! Form::label('title', 'Наименование', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-4">
                    {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'наименование']) !!}
		            {!! $errors->first('title', '<div class="text-danger">:message</div>') !!}
                </div>
            </div><!--form control-->

            <div class="form-group">
                {!! Form::label('type', 'Тип', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-4">
                    {!! Form::select('type', $types, null, ['id' => 'type-list', 'class' => 'form-control']) !!}
		            {!! $errors->first('type', '<div class="text-danger">:message</div>') !!}
                </div>
            </div><!--form control-->

            @foreach($forms as $form)
                <div id="type-{{ $form->getName() }}" class="form-type">
                    {!! $form->getHtml() !!}
                </div>
            @endforeach

            <div class="form-group">
                {!! Form::label('position', 'Позиция', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-4">
                    {!! Form::select('position', $positions, null, ['class' => 'form-control']) !!}
		            {!! $errors->first('position', '<div class="text-danger">:message</div>') !!}
                </div>
            </div><!--form control-->

            <div class="form-group">
                <label class="col-lg-2 control-label">Опубликовать</label>
                <div class="col-lg-4">
                    {!! Form::hidden('published', 0) !!}
                    {!! Form::checkbox('published', 1, (isset($widget) && $widget->published === 1)) !!}
		            {!! $errors->first('published', '<div class="text-danger">:message</div>') !!}
                </div>
            </div><!--form control-->

            <div class="form-group">
                <label class="col-lg-2 control-label">Начало публикации</label>
                <div class="col-lg-4">
                    <div class="input-group datepicker">
                        {!! Form::text('publish_up', null, ['data-provide' => 'datepicker', 'data-date-format' => 'yyyy-mm-dd 00:00:00', 'class' => 'form-control']) !!}
                        <span class="input-group-addon"><span class="glyphicon-calendar glyphicon"></span></span>
                    </div>

		            {!! $errors->first('publish_up', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-2 control-label">Окончание публикации</label>
                <div class="col-lg-4">
                    <div class="input-group datepicker">
                        {!! Form::text('publish_down', null, ['data-provide' => 'datepicker', 'data-date-format' => 'yyyy-mm-dd 00:00:00', 'class' => 'form-control']) !!}
                        <span class="input-group-addon"><span class="glyphicon-calendar glyphicon"></span></span>
                    </div>

		            {!! $errors->first('publish_down', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>

            <div class="form-group">
                {!! Form::label('note', 'Примечание', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-4">
                    {!! Form::textarea('note', null, ['class' => 'form-control', 'rows' => 3]) !!}
		            {!! $errors->first('note', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>

        </div><!-- /.box-body -->
    </div><!--box-->
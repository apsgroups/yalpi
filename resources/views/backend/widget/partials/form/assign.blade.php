<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Привязка к страницам</h3>
    </div>

    <div class="box-body">
        <div class="treeview">
            <div id="assignment">
            @include('backend.widget.partials.assign.assign_select')

            <div id="menuselect-group" class="control-group">
            	<label id="form_menuselect-lbl" class="control-label" for="form_menuselect">Выбор меню:</label>

            	<div id="form_menuselect" class="controls">
            		<div class="well well-small">
            	        @include('backend.widget.partials.assign.assign_controls')

            			<div class="clearfix"></div>

            			<hr class="hr-condensed" />

            			<ul class="treeselect">
                            @foreach($assignItems as $menu)
                            <li>
                                <div class="treeselect-item pull-left">
                                    <label class="pull-left nav-header">{{ $menu['item']->title }}</label>
                                </div>

                                @include('backend.widget.partials.assign.item', ['items' => $menu['childs']])
                            </li>
                            @endforeach
                        </ul>

            			@include('backend.widget.partials.assign.item_control')
            		</div>
            	</div>
            </div>

            </div>
        </div>
    </div>
</div>

<div class="row">
    {!! Form::open(['files' => true, 'route' => 'admin.widget.index', 'class' => 'form-inline filter', 'role' => 'form', 'method' => 'get']) !!}
    <div class="col-lg-2">
        <div class="input-group">
            {!! Form::text('filter[search]', $filter['search'], ['placeholder' => 'Поиск', 'class' => 'form-control']) !!}

            <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
            </span>
        </div>
    </div>

    <div class="col-lg-8">
        Публикация {!! Form::select('filter[published]', ['' => 'выбрать', 'нет', 'да'], $filter['published'], ['class' => 'form-control']) !!}

        Тип {!! Form::select('filter[type]', $types, $filter['type'], ['class' => 'form-control']) !!}

        Позиция {!! Form::select('filter[position]', $positions, $filter['position'], ['class' => 'form-control']) !!}

        @if(request()->get('filter'))
        {!! link_to_route('admin.widget.index', 'Сбросить', [], ['class' => 'btn btn-danger']) !!}
        @endif
    </div>

    <div class="clearfix"></div>
    {!! Form::close() !!}
</div>
@if(!empty($widget))
{!! Form::model($widget, ['files' => true, 'route' => ['admin.widget.update', $widget->id], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'put']) !!}
@else
{!! Form::open(['files' => true, 'route' => 'admin.widget.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) !!}
@endif

<?php
$tabs = ['main', 'assign'];
$active = 'main';
?>

    @include('backend.includes.partials.save-buttons', ['cancel' => route('admin.widget.index')])

    <div>
      <!-- Nav tabs -->
      <ul class="nav nav-tabs" role="tablist">
        @foreach($tabs as $tab)
        <li role="presentation"<?php echo $tab === $active ? ' class="active"' : '' ?>>
            <a href="#tab-{{ $tab }}" aria-controls="tab-{{ $tab }}" role="tab" data-toggle="tab">{{ trans('widget.tab.'.$tab) }}</a>
        </li>
        @endforeach
      </ul>

      <!-- Tab panes -->
      <div class="tab-content">
        @foreach($tabs as $tab)
        <div role="tabpanel" class="tab-pane<?php echo $tab === $active ? ' active' : '' ?>" id="tab-{{ $tab }}">
            @include('backend.widget.partials.form.'.$tab)
        </div>
        @endforeach
      </div>

    </div>

    @include('backend.includes.partials.save-buttons', ['cancel' => route('admin.widget.index')])

    {!! Form::hidden('id') !!}
{!! Form::close() !!}

@section('after-scripts-end')
    {!! Html::script('js/backend/widget.js') !!}
@stop
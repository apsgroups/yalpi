@extends ('backend.layouts.master')

@section ('title', 'Изменение виджета '.$widget->title)

@section('page-header')
    <h1>
        Изменение: {{ $widget->title }}
    </h1>
@endsection

@section('content')
    @include('backend.widget.partials.form')
@stop
@extends('backend.layouts.master')

@section('title', 'Управление виджетами')

@section('page-header')
    <h1>
        Управление виджетами
    </h1>
@endsection

@section('content')
    @inject('sort', 'App\Services\Sort')

    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Список</h3>

            <div class="box-tools pull-right">
                @include('backend.widget.partials.header-buttons')
            </div>
        </div><!-- /.box-header -->

        <div class="box-body">
            @include('backend.widget.partials.filter')

            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover {{ ((request()->input('sort') === 'ordering' && request()->input('dir', 'desc') === 'asc') ? 'widget-sortable' : '') }}">
                    <thead>
                    <tr>
                        <th>{!! $sort->route('admin.widget.index', 'ordering', 'O') !!}</th>
                        <th>{!! $sort->route('admin.widget.index', 'id', 'ID') !!}</th>
                        <th>{!! $sort->route('admin.widget.index', 'title', 'Наименование') !!}</th>
                        <th>{!! $sort->route('admin.widget.index', 'type', 'Тип') !!}</th>
                        <th>{!! $sort->route('admin.widget.index', 'position', 'Позиция') !!}</th>
                        <th class="text-center">{!! $sort->route('admin.widget.index', 'published', 'Опубликовано') !!}</th>
                        <th class="text-center">{!! $sort->route('admin.widget.index', 'created_at', 'Создано') !!}</th>
                        <th class="text-center">{!! $sort->route('admin.widget.index', 'updated_at', 'Обновлено') !!}</th>
                        <th class="text-center">Удалить</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach ($widgets as $widget)
                            <tr id="items-{{ $widget->id }}">
                                <td></td>
                                <td>{{ $widget->id }}</td>
                                <td>{!! link_to_route('admin.widget.edit', $widget->title, [$widget->id]) !!}</td>
                                <td>{{ $widget->type }}</td>
                                <td>{{ $widget->position }}</td>
                                <td class="text-center">
                                    @include('backend.includes.partials.toggle', ['model' => $widget, 'toggle' => 'published'])
                                </td>
                                <td class="text-center">{{ $widget->created_at->format(config('backend.date.format')) }}</td>
                                <td class="text-center">{{ $widget->updated_at->format(config('backend.date.format')) }}</td>
                                <td class="text-center">
                                    @include('backend.includes.partials.destroy', ['link' => route('admin.widget.destroy', $widget->id)])
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

            @include('backend.includes.partials.pagination', ['items' => $widgets])
        </div><!-- /.box-body -->
    </div><!--box-->
@stop
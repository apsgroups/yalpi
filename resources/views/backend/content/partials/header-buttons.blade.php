<div class="pull-right" style="margin-bottom:10px">
    <div class="btn-group">
        <a href="{{ route('admin.content.create', [$content->slug]) }}" class="btn btn-success btn-sm"><i class="fa fa-plus"></i> Добавить</a>
    </div><!--btn group-->
</div>
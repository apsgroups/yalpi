@if(!empty($item))
{!! Form::model($item, ['files' => true, 'route' => ['admin.content.update', $item->type->slug, $item->id], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'put']) !!}
@else
{!! Form::open(['files' => true, 'route' => ['admin.content.store', $content->slug], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) !!}
@endif

<?php
$tabs = ['main', 'seo', 'gallery'];
$active = 'main';
?>

    @include('backend.includes.partials.save-buttons', ['cancel' => route('admin.content.index', [$content->slug])])

    <div>
      <!-- Nav tabs -->
      <ul class="nav nav-tabs" role="tablist">
        @foreach($tabs as $tab)
        <li role="presentation"<?php echo $tab === $active ? ' class="active"' : '' ?>>
            <a href="#{{ $tab }}" aria-controls="{{ $tab }}" role="tab" data-toggle="tab">{{ trans('content.tab.'.$tab) }}</a>
        </li>
        @endforeach
      </ul>

      <!-- Tab panes -->
      <div class="tab-content">
        @foreach($tabs as $tab)
        <div role="tabpanel" class="tab-pane<?php echo $tab === $active ? ' active' : '' ?>" id="{{ $tab }}">
            @include('backend.content.partials.form.'.$tab)
        </div>
        @endforeach
      </div>

    </div>

    @include('backend.includes.partials.save-buttons', ['cancel' => route('admin.content.index', [$content->slug])])

    {!! Form::hidden('id') !!}
{!! Form::close() !!}


@section('after-scripts-end')
    @if(!empty($item))
        {!! Html::script('js/plugin/fine-uploader/jquery.fine-uploader.min.js') !!}

        <script>
            var imgMorph = {type: '{{ str_replace('\\', '\\\\', \App\Models\Content::class) }}', 'id': {{ $item->id }}}
        </script>

        {!! Html::script('js/backend/gallery.js') !!}
    @endif

    @if(empty($item) || $item->visual_editor)
        {!! Html::script('js/plugin/ckeditor/ckeditor.js') !!}

        <script>
        CKEDITOR.replace('intro', {
           filebrowserBrowseUrl: "{{ route('elfinder.ckeditor') }}",
           removeDialogTabs: 'link:upload;image:upload'
        });

        CKEDITOR.replace('more', {
            filebrowserBrowseUrl: "{{ route('elfinder.ckeditor') }}",
            removeDialogTabs: 'link:upload;image:upload',
            allowedContent: true,
            extraAllowedContent:'p(*)[*]{*};div(*)[*]{*};li(*)[*]{*};ul(*)[*]{*};i(*)[*]{*}'
        });

        CKEDITOR.replace('more_mobile', {
            filebrowserBrowseUrl: "{{ route('elfinder.ckeditor') }}",
            removeDialogTabs: 'link:upload;image:upload',
            allowedContent: true,
            extraAllowedContent:'p(*)[*]{*};div(*)[*]{*};li(*)[*]{*};ul(*)[*]{*};i(*)[*]{*}'
        });
        </script>
    @endif
@stop


@section('after-styles-end')
    {!! Html::style('js/plugin/fine-uploader/fine-uploader.min.css') !!}
    {!! Html::style('js/plugin/fine-uploader/fine-uploader-new.min.css') !!}
    {!! Html::style('js/plugin/fine-uploader/fine-uploader-gallery.min.css') !!}
@stop
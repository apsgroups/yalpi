<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Фотогалерея</h3>
    </div><!-- /.box-header -->

    <div class="box-body">
        @if(!empty($item))
            @include('backend.content.partials.gallery.uploader')

            <div class="image-manager">
            @foreach($item->images as $image)
                @include('backend.content.partials.gallery.item')
            @endforeach
            </div>
        @else
            <p>Сохраните, затем загрузите фото</p>
        @endif
    </div><!--box-->
</div>
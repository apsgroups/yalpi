    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Основное</h3>
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="form-group">
                {!! Form::label('title', 'Наименование', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-6">
                    {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'наименование']) !!}
		            {!! $errors->first('title', '<div class="text-danger">:message</div>') !!}
                </div>
            </div><!--form control-->

            <div class="form-group">
                {!! Form::label('slug', 'Псевдоним', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-6">
                    {!! Form::text('slug', null, ['class' => 'form-control', 'placeholder' => 'введите псевдоним для URL латинскими буквами, - вместо пробела']) !!}
		            {!! $errors->first('slug', '<div class="text-danger">:message</div>') !!}
                </div>
            </div><!--form control-->

            <div class="form-group">
                <label class="col-lg-2 control-label">Опубликовать</label>
                <div class="col-lg-1">
                    {!! Form::hidden('published', 0) !!}
                    {!! Form::checkbox('published', 1, (isset($item) && $item->published === 1)) !!}
		            {!! $errors->first('published', '<div class="text-danger">:message</div>') !!}
                </div>
            </div><!--form control-->

            <div class="form-group">
                <label class="col-lg-2 control-label">Карта сайта</label>
                <div class="col-lg-1">
                    {!! Form::hidden('sitemap', 0) !!}
                    {!! Form::checkbox('sitemap', 1, (!isset($item) || $item->sitemap === 1)) !!}
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-2 control-label">Связанные товары</label>
                <div class="col-lg-4">
                    {!! Form::select('products[]', (isset($products) ? $products : []), (isset($products) ? array_keys($products->toArray()) : []), [
                            'class' => 'search-products',
                            'multiple' => 'multiple'
                        ]) !!}
		            {!! $errors->first('products', '<div class="text-danger">:message</div>') !!}
                </div>
            </div><!--form control-->

            <div class="form-group">
                <label class="col-lg-2 control-label">Связанные статьи</label>
                <div class="col-lg-4">
                    {!! Form::select('related_contents[]', (isset($relatedContents) ? $relatedContents : []), (isset($relatedContents) ? array_keys($relatedContents->toArray()) : []), [
                            'class' => 'search-related-content',
                            'multiple' => 'multiple'
                        ]) !!}
                    {!! $errors->first('products', '<div class="text-danger">:message</div>') !!}
                </div>
            </div><!--form control-->

            <div class="form-group">
                <label class="col-lg-2 control-label">Теги</label>
                <div class="col-lg-4">
                    {!! Form::select('tags[]', $selectedTags, $selectedTags, ['id' => 'tagss', 'class' => 'select-tags', 'multiple' => 'multiple']) !!}
                    {!! $errors->first('tags', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>

            @if($content->slug === 'news' || $content->slug === 'pages')
            <div class="form-group">
                <label class="col-lg-2 control-label">Для оптовиков</label>
                <div class="col-lg-1">
                    {!! Form::hidden('wholesaler', 0) !!}
                    {!! Form::checkbox('wholesaler', 1, (isset($item) && $item->wholesaler === 1)) !!}
		            {!! $errors->first('wholesaler', '<div class="text-danger">:message</div>') !!}
                </div>
            </div><!--form control-->
            @endif

            <div class="form-group">
                <label class="col-lg-2 control-label">Превью</label>
                <div class="col-lg-6">
                    {!! Form::file('image', ['class' => 'form-control']) !!}
		            {!! $errors->first('image', '<div class="text-danger">:message</div>') !!}
                </div>

                @if(isset($item) && $item->image)
                <div class="col-lg-3">
                    <div class="image-manager">
                        <div class="img-item" data-img-id="{{ $item->image->id }}">
                            <a href="{{ img_src($item->image, 'large') }}" class="fancybox" rel="gallery">
                                <img src="{{ img_src($item->image, 'preview') }}" class="img-rounded" />
                            </a>
                            <button class="remove-img btn btn-danger btn-xs" data-id="{{ $item->image->id }}"><i class="glyphicon glyphicon-trash"></i></button>
                            {{ Form::radio('image_id', $item->image->id) }}
                        </div>
                    </div>
                </div>
                @endif
            </div><!--form control-->

            <div class="form-group">
                <label class="col-lg-2 control-label">Визуальный редактор</label>
                <div class="col-lg-1">
                    {!! Form::hidden('visual_editor', 0) !!}
                    {!! Form::checkbox('visual_editor', 1, (!isset($item) || $item->visual_editor === 1)) !!}
                    {!! $errors->first('visual_editor', '<div class="text-danger">:message</div>') !!}
                </div>
            </div><!--form control-->

            <div class="form-group">
                {!! Form::label('intro', 'Текст-превью', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-7">
                    {!! Form::textarea('intro', null, ['id' => 'intro', 'class' => 'form-control', 'placeholder' => 'вступительный текст']) !!}
		            {!! $errors->first('intro', '<div class="text-danger">:message</div>') !!}
                </div>
            </div><!--form control-->

            <div class="form-group">
                {!! Form::label('more', 'Подробный текст', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-7">
                    {!! Form::textarea('more', null, ['id' => 'more', 'class' => 'form-control', 'placeholder' => 'подробный текст']) !!}
		            {!! $errors->first('more', '<div class="text-danger">:message</div>') !!}
                </div>
            </div><!--form control-->

            <div class="form-group">
                {!! Form::label('more_mobile', 'Подробный текст для моб. версии', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-7">
                    {!! Form::textarea('more_mobile', null, ['id' => 'more_mobile', 'class' => 'form-control', 'placeholder' => 'подробный текст в мобильной версии сайта']) !!}
                    {!! $errors->first('more_mobile', '<div class="text-danger">:message</div>') !!}
                </div>
            </div><!--form control-->

        </div><!-- /.box-body -->
    </div><!--box-->

    {!! Form::hidden('content_type_id', $content->id) !!}
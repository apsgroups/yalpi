@extends('backend.layouts.master')

@section('title', $content->title)

@section('page-header')
    <h1>
        Управление контентом: {{ $content->title }}
    </h1>
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Список</h3>

            <div class="box-tools pull-right">
                @include('backend.content.partials.header-buttons')
            </div>
        </div><!-- /.box-header -->

        <div class="box-body">
{{--            @include('backend.content.partials.filter')--}}

            <p>&nbsp;</p>

{{--            @include('backend.includes.partials.check.action', ['scope' => 'product'])--}}

            <p>&nbsp;</p>

            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        @include('backend.includes.partials.check.all')
                        <th>ID</th>
                        <th>Наименование</th>
                        <th>Псевдоним</th>
                        <th class="text-center">Опубликовано</th>
                        @if($content->slug === 'news' || $content->slug === 'pages')
                        <th class="text-center">Для оптовиков</th>
                        @endif
                        <th class="text-center">Sitemap</th>
                        <th class="text-center">Создано</th>
                        <th class="text-center">Обновлено</th>
                        <th class="text-center">Удалить</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach ($items as $item)
                            <tr>
                                @include('backend.includes.partials.check.item', ['id' => $item->id])
                                <td>{!! $item->id !!}</td>
                                <td>{!! link_to_route('admin.content.edit', $item->title, [$content->slug, $item->id]) !!}</td>
                                <td>{{ $item->slug }}</td>
                                <td class="text-center">
                                    @include('backend.includes.partials.toggle', ['model' => $item, 'toggle' => 'published'])
                                </td>
                                @if($content->slug === 'news' || $content->slug === 'pages')
                                <td class="text-center">
                                    @include('backend.includes.partials.toggle', ['model' => $item, 'toggle' => 'wholesaler'])
                                </td>
                                @endif
                                <td class="text-center">
                                    @include('backend.includes.partials.toggle', ['model' => $item, 'toggle' => 'sitemap'])
                                </td>
                                <td class="text-center">{{ $item->created_at->format(config('backend.date.format')) }}</td>
                                <td class="text-center">{{ $item->updated_at->format(config('backend.date.format')) }}</td>
                                <td class="text-center">
                                    @include('backend.includes.partials.destroy', ['link' => route('admin.content.destroy', $item->id)])
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

            @include('backend.includes.partials.pagination')
        </div><!-- /.box-body -->
    </div><!--box-->
@stop
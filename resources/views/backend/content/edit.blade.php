@extends ('backend.layouts.master')

@section ('title', 'Изменение контента '.$item->title)

@section('page-header')
    <h1>
        Изменение: {{ $item->title }}
    </h1>
@endsection

@section('content')
    @include('backend.content.partials.form')
@stop
@extends('backend.layouts.master')
@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">{{ trans('strings.backend.dashboard.welcome') }}, {!! access()->user()->name !!}!</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div><!-- /.box tools -->
        </div><!-- /.box-header -->
        <div class="box-body">
            <div>{!! link_to_route('admin.tool.cache.clear', 'Очистить кэш', [], ['class' => 'cache-clear']) !!}</div>
            <div>{!! link_to_route('admin.tool.products.reindex', 'Индексация товаров') !!}</div>
            <div>{!! link_to_route('admin.tool.categories.reindex', 'Индексация категорий') !!}</div>
        </div><!-- /.box-body -->
    </div><!--box box-success-->
@endsection
<?php

$blocks = [
    0 => 'По-умолчанию',
    'homepage' => 'Главная',
    'product_prefix' => 'Добавление к товару',
    'category_prefix' => 'Добавление к категории',
    'catalog' => 'Каталог',
    'measure' => 'Вызвать замерщика',
    'callback' => 'Обратный звонок',
    'cart' => 'Корзина',
    'favorite' => 'Избранные',
    'reviews' => 'Отзывы',
    'warranty' => 'Форма Гарантия',
    'contact' => 'Форма Контакты',
    'complain' => 'Пожаловаться',
    'thank' => 'Поблагодарить',
    'consult' => 'Консультация',
    'cost' => 'Расчёт стоимости',
    'director' => 'Директору',
    'diller' => 'Опт',
];
?>

@foreach($blocks as $key => $title)
@php($prefix = $key === 0 ? '' : $key.'_')
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">SEO &mdash; {{ $title }}</h3>
    </div><!-- /.box-header -->

    <div class="box-body">
        <div class="form-group">
            {!! Form::label($prefix.'page_title', 'SEO Title', ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-6">
                {!! Form::text('settings['.$prefix.'page_title]', Settings::get($prefix.'page_title'), ['class' => 'form-control', 'placeholder' => 'seo title-тег']) !!}
                {!! $errors->first('settings.'.$prefix.'page_title', '<div class="text-danger">:message</div>') !!}
            </div>
        </div>
    </div><!-- /.box-body -->

    <div class="box-body">
        <div class="form-group">
            {!! Form::label($prefix.'metakeys', 'SEO Keywords', ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-6">
                {!! Form::text('settings['.$prefix.'metakeys]', Settings::get($prefix.'metakeys'), ['class' => 'form-control', 'placeholder' => 'seo meta keywords-тег']) !!}
                {!! $errors->first('settings.'.$prefix.'metakeys', '<div class="text-danger">:message</div>') !!}
            </div>
        </div>
    </div><!-- /.box-body -->

    <div class="box-body">
        <div class="form-group">
            {!! Form::label($prefix.'metadesc', 'SEO Description', ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-6">
                {!! Form::text('settings['.$prefix.'metadesc]', Settings::get($prefix.'metadesc'), ['class' => 'form-control', 'placeholder' => 'seo meta description-тег']) !!}
                {!! $errors->first('settings.'.$prefix.'metadesc', '<div class="text-danger">:message</div>') !!}
            </div>
        </div>
    </div>
</div><!--box-->
@endforeach
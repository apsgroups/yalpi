    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Контакты</h3>
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="form-group">
                {!! Form::label('phone', 'Телефон', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-6">
                    {!! Form::text('settings[phone]', Settings::get('phone'), ['class' => 'form-control', 'placeholder' => 'телефон']) !!}
		            {!! $errors->first('settings.phone', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>
        </div><!-- /.box-body -->

        <div class="box-body">
            <div class="form-group">
                {!! Form::label('worktime', 'График работы', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-6">
                    {!! Form::textarea('settings[worktime]', Settings::get('worktime'), ['rows' => 2, 'class' => 'form-control', 'placeholder' => 'пн-пт 09:00-10:00']) !!}
		            {!! $errors->first('settings.worktime', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>
        </div><!-- /.box-body -->

        <div class="box-body">
            <div class="form-group">
                {!! Form::label('location', 'Адрес (шапка)', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-6">
                    {!! Form::textarea('settings[location]', Settings::get('location'), ['rows' => 2, 'class' => 'form-control', 'placeholder' => 'ул. ...']) !!}
		            {!! $errors->first('settings.location', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>
        </div><!-- /.box-body -->

        <div class="box-body">
            <div class="form-group">
                {!! Form::label('footer_address', 'Адрес (футер)', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-6">
                    {!! Form::textarea('settings[footer_address]', e(Settings::get('footer_address')), ['rows' => 2, 'class' => 'form-control', 'placeholder' => 'ул. ...']) !!}
		            {!! $errors->first('settings.footer_address', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>
        </div><!-- /.box-body -->

        <div class="box-body">
            <div class="form-group">
                {!! Form::label('rekvizity', 'Реквизиты', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-6">
                    {!! Form::text('settings[rekvizity]', Settings::get('rekvizity'), ['rows' => 2, 'class' => 'form-control', 'placeholder' => 'Реквизиты']) !!}
                    {!! $errors->first('settings.rekvizity', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>
        </div><!-- /.box-body -->

        <div class="box-body">
            <div class="form-group">
                {!! Form::label('social_fb', 'Facebook', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-6">
                    {!! Form::text('settings[social_fb]', Settings::get('social_fb'), ['rows' => 2, 'class' => 'form-control', 'placeholder' => 'ссылка на страницу Facebook']) !!}
		            {!! $errors->first('settings.social_fb', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>
        </div><!-- /.box-body -->

        <div class="box-body">
            <div class="form-group">
                {!! Form::label('social_odnoklassniki', 'Odnoklassniki', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-6">
                    {!! Form::text('settings[social_odnoklassniki]', Settings::get('social_odnoklassniki'), ['rows' => 2, 'class' => 'form-control', 'placeholder' => 'ссылка на страницу OK']) !!}
		            {!! $errors->first('settings.social_odnoklassniki', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>
        </div><!-- /.box-body -->

        <div class="box-body">
            <div class="form-group">
                {!! Form::label('social_vkontakte', 'VKontakte', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-6">
                    {!! Form::text('settings[social_vkontakte]', Settings::get('social_vkontakte'), ['rows' => 2, 'class' => 'form-control', 'placeholder' => 'ссылка на страницу VK']) !!}
		            {!! $errors->first('settings.social_vkontakte', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>
        </div><!-- /.box-body -->

        <div class="box-body">
            <div class="form-group">
                {!! Form::label('social_twitter', 'Twitter', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-6">
                    {!! Form::text('settings[social_twitter]', Settings::get('social_twitter'), ['rows' => 2, 'class' => 'form-control', 'placeholder' => 'ссылка на страницу Twitter']) !!}
		            {!! $errors->first('settings.social_twitter', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>
        </div><!-- /.box-body -->

        <div class="box-body">
            <div class="form-group">
                {!! Form::label('social_instagram', 'Instagram', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-6">
                    {!! Form::text('settings[social_instagram]', Settings::get('social_instagram'), ['rows' => 2, 'class' => 'form-control', 'placeholder' => 'ссылка на страницу Instagram']) !!}
                    {!! $errors->first('settings.social_instagram', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>
        </div><!-- /.box-body -->

        <div class="box-body">
            <div class="form-group">
                {!! Form::label('social_youtube', 'YouTube', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-6">
                    {!! Form::text('settings[social_youtube]', Settings::get('social_youtube'), ['rows' => 2, 'class' => 'form-control', 'placeholder' => 'ссылка на страницу YouTube']) !!}
                    {!! $errors->first('settings.social_youtube', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>
        </div><!-- /.box-body -->

    </div><!--box-->
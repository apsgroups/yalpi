    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Основное</h3>
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="form-group">
                {!! Form::label('title', 'Наименование сайта', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-6">
                    {!! Form::text('settings[title]', Settings::get('title'), ['class' => 'form-control', 'placeholder' => 'наименование']) !!}
		            {!! $errors->first('settings.title', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>
        </div><!-- /.box-body -->

        <div class="box-body">
            <div class="form-group">
                {!! Form::label('site_domain', 'Домен', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-6">
                    {!! Form::text('settings[site_domain]', Settings::get('site_domain'), ['class' => 'form-control', 'placeholder' => 'домен']) !!}
		            {!! $errors->first('settings.site_domain', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>
        </div><!-- /.box-body -->

        <div class="box-body">
            <div class="form-group">
                {!! Form::label('email', 'E-mail сайта', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-6">
                    {!! Form::text('settings[email]', Settings::get('email'), ['class' => 'form-control', 'placeholder' => 'example@site.com']) !!}
		            {!! $errors->first('settings.email', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>
        </div><!-- /.box-body -->

        <div class="box-body">
            <div class="form-group">
                {!! Form::label('email_dev', 'E-mail разработчика', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-6">
                    {!! Form::text('settings[email_dev]', Settings::get('email_dev'), ['class' => 'form-control', 'placeholder' => 'example@site.com']) !!}
		            {!! $errors->first('settings.email_dev', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>
        </div><!-- /.box-body -->

        <div class="box-body">
            <div class="form-group">
                {!! Form::label('contacts', 'ID страницы Контакты', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-6">
                    {!! Form::text('settings[contacts]', Settings::get('contacts'), ['rows' => 2, 'class' => 'form-control', 'placeholder' => 'id страницы']) !!}
                    {!! $errors->first('settings.contacts', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>
        </div><!-- /.box-body -->

        <div class="box-body">
            <div class="form-group">
                {!! Form::label('calc_page_id', 'ID страницы калькулятора', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-6">
                    {!! Form::text('settings[calc_page_id]', Settings::get('calc_page_id'), ['rows' => 2, 'class' => 'form-control', 'placeholder' => 'id страницы']) !!}
                    {!! $errors->first('settings.calc_page_id', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>
        </div><!-- /.box-body -->

        <div class="box-body">
            <div class="form-group">
                {!! Form::label('novelty_category_id', 'ID категории Новинок', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-6">
                    {!! Form::text('settings[novelty_category_id]', Settings::get('novelty_category_id'), ['rows' => 2, 'class' => 'form-control', 'placeholder' => 'id категории Новинок']) !!}
                    {!! $errors->first('settings.novelty_category_id', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>
        </div>

        <div class="box-body">
            <div class="form-group">
                {!! Form::label('sale_category_id', 'ID категории распродаж', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-6">
                    {!! Form::text('settings[sale_category_id]', Settings::get('sale_category_id'), ['rows' => 2, 'class' => 'form-control', 'placeholder' => 'ID категории распродаж']) !!}
                    {!! $errors->first('settings.sale_category_id', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>
        </div>

        <div class="box-body">
            <div class="form-group">
                {!! Form::label('search_empty_category_id', 'ID категории для поиска без результатов', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-6">
                    {!! Form::text('settings[search_empty_category_id]', Settings::get('search_empty_category_id'), ['rows' => 2, 'class' => 'form-control', 'placeholder' => 'ID категории для поиска без результатов']) !!}
                    {!! $errors->first('settings.search_empty_category_id', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>
        </div>

        <div class="box-body">
            <div class="form-group">
                {!! Form::label('sale_category_id', 'ID категории столешниц', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-6">
                    {!! Form::text('settings[desk_category_id]', Settings::get('desk_category_id'), ['rows' => 2, 'class' => 'form-control', 'placeholder' => 'ID категории столешниц']) !!}
                    {!! $errors->first('settings.desk_category_id', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>
        </div>

        <div class="box-body">
            <div class="form-group">
                {!! Form::label('order_emails', 'Email для заказов', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-6">
                    {!! Form::text('settings[order_emails]', Settings::get('order_emails'), ['class' => 'form-control', 'placeholder' => 'admin@mail.com,second@mail.com']) !!}
                    {!! $errors->first('settings.order_emails', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>
        </div>

        <div class="box-body">
            <div class="form-group">
                {!! Form::label('email_shop_discussion', 'Email для обсуждений', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-6">
                    {!! Form::text('settings[email_shop_discussion]', Settings::get('email_shop_discussion'), ['class' => 'form-control', 'placeholder' => 'admin@mail.com,second@mail.com']) !!}
                    {!! $errors->first('settings.email_shop_discussion', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>
        </div>

    </div><!--box-->
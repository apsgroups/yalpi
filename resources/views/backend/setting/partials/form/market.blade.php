    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Яндекс.Маркет</h3>
        </div>

        <div class="box-body">
            <div class="form-group">
                {!! Form::label('market_title', 'Тег name', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-6">
                    {!! Form::text('settings[market_title]', Settings::get('market_title'), ['class' => 'form-control', 'placeholder' => 'тег name']) !!}
                    {!! $errors->first('settings.market_title', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>

            <div class="form-group">
                {!! Form::label('market_company', 'Тег company', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-6">
                    {!! Form::text('settings[market_company]', Settings::get('market_company'), ['class' => 'form-control', 'placeholder' => 'тег company']) !!}
                    {!! $errors->first('settings.market_company', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>
        </div>

    </div>
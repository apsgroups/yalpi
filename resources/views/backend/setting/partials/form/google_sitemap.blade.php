@include('backend.setting.partials.form._sitemap_form', ['title' => 'По-умолчанию для всех', 'name' => 'default', 'sitemap' => 'google'])

@include('backend.setting.partials.form._sitemap_form', ['title' => 'По-умолчанию для категорий', 'name' => 'category', 'sitemap' => 'google'])

@include('backend.setting.partials.form._sitemap_form', ['title' => 'По-умолчанию для товаров', 'name' => 'product', 'sitemap' => 'google'])

@include('backend.setting.partials.form._sitemap_form', ['title' => 'По-умолчанию для типа контента', 'name' => 'content_type', 'sitemap' => 'google'])

@include('backend.setting.partials.form._sitemap_form', ['title' => 'По-умолчанию для элемента контента', 'name' => 'content', 'sitemap' => 'google'])

@include('backend.setting.partials.form._sitemap_form', ['title' => 'Форма гарантии', 'name' => 'warranty', 'sitemap' => 'google'])

@include('backend.setting.partials.form._sitemap_form', ['title' => 'Форма пожаловаться', 'name' => 'complain', 'sitemap' => 'google'])

@include('backend.setting.partials.form._sitemap_form', ['title' => 'Форма поблагодарить', 'name' => 'thank', 'sitemap' => 'google'])
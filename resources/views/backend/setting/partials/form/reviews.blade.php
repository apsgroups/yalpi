<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Отзывы о магазине</h3>
    </div><!-- /.box-header -->

    <div class="box-body">
        <div class="form-group">
            {!! Form::label('email', 'E-mail для уведомлений', ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-6">
                {!! Form::text('settings[email_shop_reviews]', Settings::get('email_shop_reviews'), ['class' => 'form-control', 'placeholder' => 'example@mail.com']) !!}
	            {!! $errors->first('settings.email_shop_reviews', '<div class="text-danger">:message</div>') !!}
            </div>
        </div>
    </div><!-- /.box-body -->
</div>

<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Отзывы о товаре</h3>
    </div><!-- /.box-header -->

    <div class="box-body">
        <div class="form-group">
            {!! Form::label('email_product_reviews', 'E-mail для уведомлений', ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-6">
                {!! Form::text('settings[email_product_reviews]', Settings::get('email_product_reviews'), ['class' => 'form-control', 'placeholder' => 'example@mail.com']) !!}
	            {!! $errors->first('settings.email_product_reviews', '<div class="text-danger">:message</div>') !!}
            </div>
        </div>
    </div><!-- /.box-body -->
</div>
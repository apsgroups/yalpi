<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Основное</h3>
    </div><!-- /.box-header -->

    <div class="box-body">
        <div class="form-group">
            {!! Form::label('info_enabled', 'Включить', ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-6">
                {!! Form::hidden('settings[info_enabled]', 0) !!}
                {!! Form::checkbox('settings[info_enabled]', 1, Settings::get('info_enabled') === '1') !!}
                {!! $errors->first('settings.info_enabled', '<div class="text-danger">:message</div>') !!}
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('info_title', 'Заголовок', ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-6">
                {!! Form::text('settings[info_title]', Settings::get('info_title'), ['class' => 'form-control', 'placeholder' => 'Заголовок']) !!}
	            {!! $errors->first('settings.info_title', '<div class="text-danger">:message</div>') !!}
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('info_url', 'Заголовок', ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-6">
                {!! Form::text('settings[info_url]', Settings::get('info_url'), ['class' => 'form-control', 'placeholder' => 'URL']) !!}
                {!! $errors->first('settings.info_url', '<div class="text-danger">:message</div>') !!}
            </div>
        </div>
    </div><!-- /.box-body -->
</div>
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Подписка</h3>
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="form-group">
                {!! Form::label('subscribe_api_key', 'API Key', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-6">
                    {!! Form::text('settings[subscribe_api_key]', Settings::get('subscribe_api_key'), ['class' => 'form-control', 'placeholder' => 'api key']) !!}
		            {!! $errors->first('settings.subscribe_api_key', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>
        </div><!-- /.box-body -->

        <div class="box-body">
            <div class="form-group">
                {!! Form::label('subscribe_list_ids', 'Списки контактов', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-6">
                    {!! Form::text('settings[subscribe_list_ids]', Settings::get('subscribe_list_ids'), ['class' => 'form-control', 'placeholder' => 'id списков контактов через запятую: 123,456']) !!}
		            {!! $errors->first('settings.subscribe_list_ids', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>
        </div><!-- /.box-body -->

        <div class="box-body">
            <div class="form-group">
                {!! Form::label('subscribe_double_optin', 'Подтверждение', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-6">
                    {!! Form::select('settings[subscribe_double_optin]', ['Да', 'Нет'], Settings::get('subscribe_double_optin'), ['class' => 'form-control']) !!}
		            {!! $errors->first('settings.subscribe_double_optin', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>
        </div>
    </div><!--box-->
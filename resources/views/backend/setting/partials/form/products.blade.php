    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Товары</h3>
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="form-group">
                {!! Form::label('calc_page_id', 'ID страницы калькулятора', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-6">
                    {!! Form::text('settings[calc_page_id]', Settings::get('calc_page_id'), ['rows' => 2, 'class' => 'form-control', 'placeholder' => 'id страницы']) !!}
                    {!! $errors->first('settings.calc_page_id', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>
        </div><!-- /.box-body -->

        <div class="box-body">
            <div class="form-group">
                {!! Form::label('novelty_category_id', 'ID категории Новинок', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-6">
                    {!! Form::text('settings[novelty_category_id]', Settings::get('novelty_category_id'), ['rows' => 2, 'class' => 'form-control', 'placeholder' => 'id категории Новинок']) !!}
                    {!! $errors->first('settings.novelty_category_id', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>
        </div>

        <div class="box-body">
            <div class="form-group">
                {!! Form::label('sale_category_id', 'ID категории распродаж', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-6">
                    {!! Form::text('settings[sale_category_id]', Settings::get('sale_category_id'), ['rows' => 2, 'class' => 'form-control', 'placeholder' => 'ID категории распродаж']) !!}
                    {!! $errors->first('settings.sale_category_id', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>
        </div>

        <div class="box-body">
            <div class="form-group">
                {!! Form::label('in_row', 'Количество товаров в ряду категории', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-6">
                    {!! Form::text('settings[in_row]', Settings::get('in_row'), ['rows' => 2, 'class' => 'form-control', 'placeholder' => 3]) !!}
                    {!! $errors->first('settings.in_row', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>
        </div>

        <div class="box-body">
            <div class="form-group">
                {!! Form::label('in_search_row', 'Количество товаров в ряду поиска', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-6">
                    {!! Form::text('settings[in_search_row]', Settings::get('in_search_row'), ['rows' => 2, 'class' => 'form-control', 'placeholder' => 3]) !!}
                    {!! $errors->first('settings.in_search_row', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>
        </div>

    </div><!--box-->
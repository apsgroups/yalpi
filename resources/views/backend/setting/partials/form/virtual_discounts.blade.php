<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Виртуальные скидки</h3>
    </div><!-- /.box-header -->


    <div class="box-body">

        <div class="form-group">
            <label></label>
            <div class="col-lg-6">
                <label>
                    {!! Form::hidden('settings[virtual_discount_stock]', 0) !!}
                    {!! Form::checkbox('settings[virtual_discount_stock]', 1, Settings::get('virtual_discount_stock')) !!}
                    {!! $errors->first('settings.virtual_discount_stock', '<div class="text-danger">:message</div>') !!}
                    Отключать наценку для отсутствующих в наличии
                </label>
            </div>            
        </div>

        <div class="form-group">
            {!! Form::label('virtual_discount_email', 'E-mail для отчета', ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-6">
                {!! Form::text('settings[virtual_discount_email]', Settings::get('virtual_discount_email'), ['class' => 'form-control', 'placeholder' => 'example@site.com']) !!}
                {!! $errors->first('settings.virtual_discount_email', '<div class="text-danger">:message</div>') !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('virtual_discount_subject', 'Тема e-mail для отчета', ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-6">
                {!! Form::text('settings[virtual_discount_subject]', Settings::get('virtual_discount_subject'), ['class' => 'form-control', 'placeholder' => 'Отчет о виртуальных скидках']) !!}
                {!! $errors->first('settings.virtual_discount_subject', '<div class="text-danger">:message</div>') !!}
            </div>
        </div>
    </div><!-- /.box-body -->
</div><!--box-->
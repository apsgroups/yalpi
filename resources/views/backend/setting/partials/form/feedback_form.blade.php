<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">{{ trans('feedback.'.$feedback->getName().'.title') }}</h3>
    </div>

    <div class="box-body">
        @if($feedback->getToUser())
        <div class="form-group">
            @php($userSubject = \App\Services\Feedback\Config\User::getConfigKey($feedback->getName(), 'subject'))

            {!! Form::label($userSubject, 'Тема письма (пользователю)', ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-6">
                {!! Form::text('settings['.$userSubject.']', Settings::get($userSubject), [
                    'rows' => 2,
                    'class' => 'form-control',
                    'placeholder' => 'Спасибо! Заявка :id с сайта :s.site_domain.']) !!}

                {!! $errors->first('settings.'.$userSubject, '<div class="text-danger">:message</div>') !!}
            </div>
        </div>
        @endif

        @if($feedback->getToManager())
        <div class="form-group">
            @php($email = \App\Services\Feedback\Config\Manager::getConfigKey($feedback->getName(), 'email'))

            {!! Form::label($email, 'E-mail менеджера', ['class' => 'col-lg-2 control-label']) !!}

            <div class="col-lg-6">
                {!! Form::text('settings['.$email.']', Settings::get($email), [
                    'rows' => 2,
                    'class' => 'form-control',
                    'placeholder' => 'manager@gmail.com, admin@gmail.com']) !!}

                {!! $errors->first('settings.'.$email, '<div class="text-danger">:message</div>') !!}
            </div>
        </div>

        <div class="form-group">
            @php($managerSubject = \App\Services\Feedback\Config\Manager::getConfigKey($feedback->getName(), 'subject'))

            {!! Form::label($managerSubject, 'Тема письма (менеджер)', ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-6">
                {!! Form::text('settings['.$managerSubject.']', Settings::get($managerSubject), [
                    'rows' => 2,
                    'class' => 'form-control',
                    'placeholder' => 'Заявка :id с сайта :s.site_domain.']) !!}

                {!! $errors->first('settings.'.$managerSubject, '<div class="text-danger">:message</div>') !!}
            </div>
        </div>
        @endif
    </div>
</div>
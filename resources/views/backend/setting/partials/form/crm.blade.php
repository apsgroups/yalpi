<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">CRM</h3>
    </div><!-- /.box-header -->

    <div class="box-body">
        <div class="form-group">
            {!! Form::label('gentleman_api_key', 'Gentleman API Key', ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-6">
                {!! Form::text('settings[gentleman_api_key]', Settings::get('gentleman_api_key'), ['class' => 'form-control', 'placeholder' => 'Gentleman API Key']) !!}
                {!! $errors->first('settings.gentleman_api_key', '<div class="text-danger">:message</div>') !!}
            </div>
        </div>
    </div>
</div><!--box-->
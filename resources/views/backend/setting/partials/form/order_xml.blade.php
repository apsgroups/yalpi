<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Выгрузка заказов в 1С</h3>
    </div><!-- /.box-header -->

    <div class="box-body">
        <div class="form-group">
            {!! Form::label('order_xml_wsdl', 'WSDL URL', ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-6">
                {!! Form::text('settings[order_xml_wsdl]', Settings::get('order_xml_wsdl'), ['class' => 'form-control', 'placeholder' => 'http://...']) !!}
                {!! $errors->first('settings.order_xml_wsdl', '<div class="text-danger">:message</div>') !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('order_xml_login', 'WSDL Login', ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-6">
                {!! Form::text('settings[order_xml_login]', Settings::get('order_xml_login'), ['class' => 'form-control', 'placeholder' => 'username']) !!}
                {!! $errors->first('settings.order_xml_login', '<div class="text-danger">:message</div>') !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('order_xml_pass', 'WSDL Pass', ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-6">
                {!! Form::text('settings[order_xml_pass]', Settings::get('order_xml_pass'), ['class' => 'form-control', 'placeholder' => 'pass']) !!}
                {!! $errors->first('settings.order_xml_pass', '<div class="text-danger">:message</div>') !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('order_xml_site', 'WSDL Site param', ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-6">
                {!! Form::text('settings[order_xml_site]', Settings::get('order_xml_site'), ['class' => 'form-control']) !!}
                {!! $errors->first('settings.order_xml_site', '<div class="text-danger">:message</div>') !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('order_xml_region', 'WSDL Region', ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-6">
                {!! Form::text('settings[order_xml_region]', Settings::get('order_xml_region'), ['class' => 'form-control']) !!}
                {!! $errors->first('settings.order_xml_region', '<div class="text-danger">:message</div>') !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('order_xml_user_id', 'WSDL User ID', ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-6">
                {!! Form::text('settings[order_xml_user_id]', Settings::get('order_xml_user_id'), ['class' => 'form-control']) !!}
                {!! $errors->first('settings.order_xml_user_id', '<div class="text-danger">:message</div>') !!}
            </div>
        </div>
    </div><!-- /.box-body -->
</div><!--box-->
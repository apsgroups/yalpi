    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Свойства</h3>
        </div>

        <div class="box-body">
            <div class="form-group">
                {!! Form::label('desk_color_id', 'ID свойства цвета столешницы', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-6">
                    {!! Form::text('settings[desk_color_id]', Settings::get('desk_color_id'), ['rows' => 2, 'class' => 'form-control', 'placeholder' => 'ID свойства цвета столешницы']) !!}
                    {!! $errors->first('settings.desk_color_id', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>
        </div>

        <div class="box-body">
            <div class="form-group">
                {!! Form::label('body_color_id', 'ID свойства цвета корпуса', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-6">
                    {!! Form::text('settings[body_color_id]', Settings::get('body_color_id'), ['rows' => 2, 'class' => 'form-control', 'placeholder' => '']) !!}
                    {!! $errors->first('settings.body_color_id', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>
        </div>

        <div class="box-body">
            <div class="form-group">
                {!! Form::label('color_id', 'ID свойства цвета', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-6">
                    {!! Form::text('settings[color_id]', Settings::get('color_id'), ['rows' => 2, 'class' => 'form-control', 'placeholder' => '']) !!}
                    {!! $errors->first('settings.color_id', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>
        </div>

    </div><!--box-->
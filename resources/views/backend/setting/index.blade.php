@extends('backend.layouts.master')

@section('title', 'Настройки')

@section('page-header')
    <h1>
        Настройки
    </h1>
@endsection

@section('content')

{!! Form::open(['route' => 'admin.setting.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) !!}

<?php
$tabs = [
  'main', 
  'seo', 
  'contacts', 
  'subscribe',
  'property', 
  'products', 
  'feedback',
  'reviews',
  'market',
  'virtual_discounts',
  'crm',
  'order_xml',
  'info',
];

$active = 'main';
?>

    @include('backend.setting.partials.buttons')

    <div>
      <!-- Nav tabs -->
      <ul class="nav nav-tabs" role="tablist">
        @foreach($tabs as $tab)
        <li role="presentation"<?php echo $tab === $active ? ' class="active"' : '' ?>>
            <a href="#{{ $tab }}" aria-controls="{{ $tab }}" role="tab" data-toggle="tab">{{ trans('setting.tab.'.$tab) }}</a>
        </li>
        @endforeach
      </ul>

      <!-- Tab panes -->
      <div class="tab-content">
        @foreach($tabs as $tab)
        <div role="tabpanel" class="tab-pane<?php echo $tab === $active ? ' active' : '' ?>" id="{{ $tab }}">
            @include('backend.setting.partials.form.'.$tab)
        </div>
        @endforeach
      </div>

    </div>

    @include('backend.setting.partials.buttons')
{!! Form::close() !!}
@stop

@section('after-scripts-end')
    {!! Html::script('js/plugin/ckeditor/ckeditor.js') !!}

    <script>
    CKEDITOR.replace('intro');
    CKEDITOR.replace('more');
    </script>
@stop
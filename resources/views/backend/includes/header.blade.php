<!-- Main Header -->
<header class="main-header">

    <!-- Logo -->
    <a href="{!! route('frontend.index') !!}" class="logo">{!! app_name() !!}</a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">{{ trans('labels.general.toggle_navigation') }}</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                @if(!request()->is('admin/product/*'))
                <li>
                    <a href="{{ route('admin.product.create') }}" class="btn btn-sm btn-info btn-flat pull-left">
                    <i class="fa fa-plus"></i> Товар
                    </a>
                </li>
                @endif

                @if(!request()->is('admin/product/*') && !request()->is('admin/news/*'))
                <li>
                    <a href="{{ route('admin.content.create', ['news']) }}" class="btn btn-sm btn-info btn-flat pull-left">
                    <i class="fa fa-plus"></i> Новость
                    </a>
                </li>
                @endif

                <li>
                    <a href="{{ route('admin.tool.cache.clear') }}" class="btn btn-sm btn-info btn-flat pull-left cache-clear">
                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                        Очистить кэш
                    </a>
                </li>

                <!-- Notifications Menu -->
                @widget('Backend\Notifier')

                <!-- User Account Menu -->
                <li class="dropdown user user-menu">
                    <!-- Menu Toggle Button -->
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <!-- The user image in the navbar-->
                        <!-- hidden-xs hides the username on small devices so only the image appears. -->
                        <span class="hidden-xs">{{ access()->user()->name }}</span>
                    </a>

                    <ul class="dropdown-menu">
                        <!-- The user image in the menu -->
                        <li class="user-header">
                            <p>
                                {!! access()->user()->name !!} - {{ trans('roles.web_developer') }}
                            </p>
                        </li>

                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="#" class="btn btn-default btn-flat">{{ trans('navs.backend.button') }}</a>
                            </div>
                            <div class="pull-right">
                                <a href="{!! route('auth.logout') !!}" class="btn btn-default btn-flat">{{ trans('navs.general.logout') }}</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>

<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- search form (Optional) -->
        <form action="{{ route('admin.product.index') }}" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="filter[search]" class="form-control" placeholder="Найти товар"/>
                  <span class="input-group-btn">
                    <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                  </span>
            </div>
        </form>
        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <!-- Optionally, you can add icons to the links -->
            <li class="{{ Active::pattern('admin/dashboard') }}">
                <a href="{!! route('admin.dashboard') !!}">
                    <i class="fa fa-tachometer" aria-hidden="true"></i>
                    <span>{{ trans('menus.backend.sidebar.dashboard') }}</span>
                </a>
            </li>

            <li class="header">Каталог</li>

            <li class="{{ Active::route('admin.product.index') }}">
                <a href="{!! route('admin.product.index') !!}">
                    <i class="fa fa-list" aria-hidden="true"></i>
                    <span>{{ trans('menus.backend.sidebar.products') }}</span>
                </a>
            </li>

            <li class="{{ Active::route('admin.category.index') }}">
                <a href="{!! route('admin.category.index') !!}">
                    <i class="fa fa-th-large" aria-hidden="true"></i>
                    <span>{{ trans('menus.backend.sidebar.categories') }}</span>
                </a>
            </li>

            <li class="{{ Active::route('admin.author.index') }}">
                <a href="{!! route('admin.author.index') !!}">
                    <i class="fa fa-user" aria-hidden="true"></i>
                    <span>Авторы</span>
                </a>
            </li>

            <li class="{{ Active::route('admin.language.index') }}">
                <a href="{!! route('admin.language.index') !!}">
                    <i class="fa fa-globe" aria-hidden="true"></i>
                    <span>Языки</span>
                </a>
            </li>

            <li class="{{ Active::route('admin.property.index') }}">
                <a href="{!! route('admin.property.index') !!}">
                    <i class="fa fa-puzzle-piece" aria-hidden="true"></i>
                    <span>{{ trans('menus.backend.sidebar.properties') }}</span>
                </a>
            </li>

            <li class="{{ Active::route('admin.product-type.index') }}">
                <a href="{!! route('admin.product-type.index') !!}">
                    <i class="fa fa-cubes" aria-hidden="true"></i>
                    <span>Типы товаров</span>
                </a>
            </li>

            <li class="{{ Active::route('admin.sort-template.index') }}">
                <a href="{!! route('admin.sort-template.index') !!}">
                    <i class="fa fa-bars" aria-hidden="true"></i>
                    <span>Шаблоны сортировки</span>
                </a>
            </li>

            <li class="{{ Active::route('admin.order.index') }}">
                <a href="{!! route('admin.order.index') !!}">
                    <i class="fa fa-area-chart" aria-hidden="true"></i>
                    <span>Заказы</span>
                </a>
            </li>

            <li class="{{ Active::route('admin.comment.index') }}">
                <a href="{!! route('admin.comment.index') !!}">
                    <i class="fa fa-star-half-o" aria-hidden="true"></i>
                    <span>Комментарии</span>
                </a>
            </li>

            <li class="header">{{ trans('menus.backend.sidebar.general') }}</li>

            <li class="{{ Active::pattern('admin/menu*') }} treeview">
                <a href="#">
                    <i class="fa fa-bars" aria-hidden="true"></i>
                    <span>Меню</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu {{ Active::pattern('admin/menu*', 'menu-open') }}" style="display: none; {{ Active::pattern('admin/menu*', 'display: block;') }}">
                    @foreach(\App\Models\Menu::all() as $menu)
                    <li class="{{ Active::pattern('admin/menu/items/'.$menu->slug) }}">
                        {!! link_to_route('admin.menu-item.index', $menu->title, [$menu->slug]) !!}
                    </li>
                    @endforeach

                    <li class="{{ Active::pattern('admin/menu') }}">
                        <a href="{!! url('admin/menu') !!}">Управление меню</a>
                    </li>
                </ul>
            </li>

            <li class="{{ Active::pattern('admin/content*') }} treeview">
                <a href="#">
                    <i class="fa fa-file-text" aria-hidden="true"></i>
                    <span>Контент</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu {{ Active::pattern('admin/content*', 'menu-open') }}" style="display: none; {{ Active::pattern('admin/content*', 'display: block;') }}">
                    @foreach(\App\Models\ContentType::all() as $type)
                    <li class="{{ Active::pattern('admin/content/'.$type->slug) }}">
                        {!! link_to_route('admin.content.index', $type->title, [$type->slug]) !!}
                    </li>
                    @endforeach

                    <li class="{{ Active::pattern('admin/content-types') }}">
                        <a href="{!! url('admin/content-types') !!}">Типы контента</a>
                    </li>
                </ul>
            </li>

            <li class="{{ Active::route('admin.feedback.index') }}">
                <a href="{!! route('admin.feedback.index') !!}"><i class="fa fa-envelope" aria-hidden="true"></i> <span>Формы сайта</span></a>
            </li>

            <li class="header">Служебные</li>

            <li class="{{ Active::route('admin.widget.index') }}">
                <a href="{!! route('admin.widget.index') !!}">
                    <i class="fa fa-cube" aria-hidden="true"></i>
                    <span>Виджеты</span>
                </a>
            </li>

            <li class="{{ Active::route('admin.meta.index') }}">
                <a href="{!! route('admin.meta.index') !!}"><i class="fa fa-tags" aria-hidden="true"></i> <span>Meta-шаблоны</span></a>
            </li>

            <li class="{{ Active::route('admin.pretty-url.index') }}">
                <a href="{!! route('admin.pretty-url.index') !!}"><i class="fa fa-tags" aria-hidden="true"></i> <span>URL фильтров</span></a>
            </li>

            <li class="{{ Active::route('admin.redirect.index') }}">
                <a href="{!! route('admin.redirect.index') !!}">
                    <i class="fa fa-share" aria-hidden="true"></i>
                    <span>Редиректы</span>
                </a>
            </li>

            <li class="{{ Active::route('admin.tag.index') }}">
                <a href="{!! route('admin.tag.index') !!}">
                    <i class="fa fa-share" aria-hidden="true"></i>
                    <span>Теги (перелинковка)</span>
                </a>
            </li>

            <li class="{{ Active::pattern('admin/import*') }} treeview">
                <a href="#">
                    <i class="fa fa-cloud" aria-hidden="true"></i>
                    <span>Импорт</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu {{ Active::pattern('admin/import*', 'menu-open') }}" style="display: none; {{ Active::pattern('admin/import*', 'display: block;') }}">
                    <li class="{{ Active::pattern('admin/import/products') }}">
                        <a href="{!! route('admin.import.product.index') !!}">Импорт товаров</a>
                    </li>
                    <li class="{{ Active::pattern('admin/import') }}">
                        <a href="{!! url('admin/import') !!}">
                            <span>Импорт цен</span>
                        </a>
                    </li>
                    <li class="{{ Active::pattern('admin/import/articuls') }}">
                        <a href="{!! route('admin.import.articul.index') !!}">Импорт/экспорт артикулов</a>
                    </li>
                    <li class="{{ Active::pattern('admin/import/missing') }}">
                        <a href="{!! route('admin.import.missing.index') !!}">Отсутствующие</a>
                    </li>
                    <li class="{{ Active::pattern('admin/import/meta') }}">
                        <a href="{!! route('admin.import.meta.index') !!}">META теги</a>
                    </li>
                    <li class="{{ Active::pattern('admin/import/tags') }}">
                        <a href="{!! route('admin.import.tag.index') !!}">Теги (перелинковка)</a>
                    </li>
                    <li class="{{ Active::pattern('admin/import/categories') }}">
                        <a href="{!! route('admin.import.category.index') !!}">Категории</a>
                    </li>
                    <li class="{{ Active::pattern('admin/import/category-tags') }}">
                        <a href="{!! route('admin.import.category-tag.index') !!}">Теги категорий</a>
                    </li>
                    <li class="{{ Active::pattern('admin/import/category-links') }}">
                        <a href="{!! route('admin.import.category-link.index') !!}">Перелинковка категорий</a>
                    </li>
                    <li class="{{ Active::pattern('admin/import/category-images') }}">
                        <a href="{!! route('admin.import.category-image.index') !!}">Изображения для категорий</a>
                    </li>
                    <li class="{{ Active::pattern('admin/import/category-priority') }}">
                        <a href="{!! route('admin.import.category-priority.index') !!}">Приоритеты по категориям</a>
                    </li>
                    <li class="{{ Active::pattern('admin/import/color-priority') }}">
                        <a href="{!! route('admin.import.color-priority.index') !!}">Приоритеты по цветам</a>
                    </li>
                    <li class="{{ Active::pattern('admin/import/category-breadcrumbs') }}">
                        <a href="{!! route('admin.import.category-breadcrumbs.index') !!}">Хл. крошки для категорий</a>
                    </li>
                </ul>
            </li>

            <li class="{{ Active::pattern('admin/export*') }} treeview">
                <a href="#">
                    <i class="fa fa-cloud" aria-hidden="true"></i>
                    <span>Экспорт</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu {{ Active::pattern('admin/export*', 'menu-open') }}" style="display: none; {{ Active::pattern('admin/export*', 'display: block;') }}">
                    <li class="{{ Active::pattern('admin/export/products') }}">
                        <a href="{!! url('admin/export/products') !!}">
                            <span>Экспорт товаров</span>
                        </a>
                    </li>
                </ul>
            </li>

            <li class="{{ Active::route('admin.market-category.index') }}">
                <a href="{!! route('admin.market-category.index') !!}">
                    <i class="fa fa-tags" aria-hidden="true"></i>
                    <span>Категории яндекс.маркета</span>
                </a>
            </li>

            <li class="{{ Active::route('admin.virtual-discounts.index') }}">
                <a href="{!! route('admin.virtual-discounts.index') !!}">
                    <i class="fa fa-percent" aria-hidden="true"></i>
                    <span>Виртуальные скидки</span>
                </a>
            </li>

            @permission('view-access-management')
                <li class="{{ Active::pattern('admin/access/*') }}">
                    <a href="{!!url('admin/access/users')!!}">
                        <i class="fa fa-users" aria-hidden="true"></i>
                        <span>{{ trans('menus.backend.access.title') }}</span>
                    </a>
                </li>
            @endauth

            <li class="{{ Active::route('admin.setting') }}">
                <a href="{!! route('admin.setting') !!}">
                    <i class="fa fa-cogs" aria-hidden="true"></i>
                    <span>Настройки</span>
                </a>
            </li>

            <li class="{{ Active::pattern('admin/log-viewer*') }} treeview">
                <a href="#">
                    <i class="fa fa-database" aria-hidden="true"></i>
                    <span>{{ trans('menus.backend.log-viewer.main') }}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu {{ Active::pattern('admin/log-viewer*', 'menu-open') }}" style="display: none; {{ Active::pattern('admin/log-viewer*', 'display: block;') }}">
                    <li class="{{ Active::pattern('admin/log-viewer') }}">
                        <a href="{!! url('admin/log-viewer') !!}">{{ trans('menus.backend.log-viewer.dashboard') }}</a>
                    </li>
                    <li class="{{ Active::pattern('admin/log-viewer/logs') }}">
                        <a href="{!! url('admin/log-viewer/logs') !!}">{{ trans('menus.backend.log-viewer.logs') }}</a>
                    </li>
                </ul>
            </li>

        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
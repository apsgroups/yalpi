    <div class="box">
        <div class="box-body">
            <div class="pull-left">
                <a href="{{ $cancel }}" class="btn btn-danger btn-sm">Отмена</a>
            </div>

            <div class="pull-right">
                <input type="submit" class="btn btn-success btn-sm" value="Сохранить" />
                <input type="submit" name="save-and-new" class="btn btn-success btn-sm" value="Сохранить и создать" />
                <input type="submit" name="save-and-close" class="btn btn-success btn-sm" value="Сохранить и закрыть" />

                @if(!empty($preview))
                    <a href="{{ $preview }}" target="_blank" class="btn btn-primary btn-sm">Просмотр</a>
                @endif
            </div>
            <div class="clearfix"></div>
        </div><!-- /.box-body -->
    </div><!--box-->
<div class="box">
    @php($prefix = $name == 'default' ? '' : $name.'_')

    <div class="box-header with-border">
        <h3 class="box-title">{{ $name }}</h3>
    </div>

    <div class="box-body">
        <div class="form-group">
            <label class="col-lg-2 control-label">Включить</label>
            <div class="col-lg-5">
                {!! Form::select($prefix.'sitemap', config('sitemap.sitemap'), null, ['class' => 'form-control']) !!}
            </div>
        </div>

        <div class="form-group">
            <label class="col-lg-2 control-label">Priority</label>
            <div class="col-lg-5">
                {!! Form::select($prefix.'priority', config('sitemap.priority'), null, ['class' => 'form-control']) !!}
            </div>
        </div>

        <div class="form-group">
            <label class="col-lg-2 control-label">Changefreq</label>
            <div class="col-lg-5">
                {!! Form::select($prefix.'changefreq', config('sitemap.changefreq'), null, ['class' => 'form-control']) !!}
            </div>
        </div>

        @if(isset($model) && in_array($model->getTable(), ['categories', 'content_types']))
        <div class="form-group">
            <label class="col-lg-2 control-label">Включать элементы (товары/контент)</label>
            <div class="col-lg-5">
                {!! Form::select($prefix.'sitemap_items', config('sitemap.items'), null, ['class' => 'form-control']) !!}
            </div>
        </div>
        @endif
    </div>
</div>
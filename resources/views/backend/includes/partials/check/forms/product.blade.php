<div class="action-block action-primary-category form-group">
    {!! Form::select('category_id', $categories, null, ['class' => 'form-control']) !!}
</div>

<div class="action-block action-attach-categories action-detach-categories action-sync-categories form-group">
    {!! Form::select('categories[]', $categories, null, ['multiple' => true, 'class' => 'wide-select chosen-select form-control']) !!}
</div>
<div>
    {!! Form::open(['class' => 'action-form form-inline', 'method' => 'post']) !!}
        <select class="route form-control">
            <option>Выберите действие</option>
            @foreach(config('backend.actions.'.$scope, []) as $route => $title)
            <option value="{{ route('admin.'.$scope.'.batch.'.$route) }}" data-action="{{ $route }}">{{ $title }}</option>
            @endforeach
        </select>

        @php($form = 'backend.includes.partials.check.forms.'.$scope)

        @if(!empty(view()->exists($form)))
            @include($form)
        @endif

        <button type="submit" class="btn btn-primary btn-sm">Применить</button>
    {!! Form::close() !!}
</div>
<div>
<b>Всего:</b> {!! $items->total() !!}
</div>

<div>
<b>Результатов на страницу:</b> 

@foreach([10, 20, 50, 100, 500, 1000] as $num)
{!! link_to(request()->url().'?'.http_build_query(array_merge(request()->except('page'), ['limit' => $num])), $num) !!}
@endforeach
</div>

{!! $items->render() !!}



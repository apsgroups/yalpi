{!! Form::model($model, [
                    'route' => (isset($route) ? $route : ['admin.'.strtolower(class_basename($model)).'.toggle', $model->id]),
                    'method' => 'POST',
                    'class' => 'toggle-form'
                ]) !!}
    {!! Form::hidden('value', (int)(!$model->getAttribute($toggle))) !!}
    {!! Form::hidden('field', $toggle) !!}
    {!! Form::button(
        '<i class="glyphicon glyphicon-'.($model->getAttribute($toggle) ? 'ok' : 'remove').'"></i>',
        [
            'class' => 'btn btn-'.($model->getAttribute($toggle) ? 'success' : 'danger').' btn-xs',
            'type' => 'submit',
            'data-loading-text' => '...',
            'title' => @$title,
        ]) !!}
{!! Form::close() !!}
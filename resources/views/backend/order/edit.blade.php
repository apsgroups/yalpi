@extends ('backend.layouts.master')

@section ('title', 'Заказ '.$order->id)

@section('content')
    @include('backend.order.partials.form')
@stop
@extends ('backend.layouts.master')

@section ('title', $content->title.' - добавление')

@section('page-header')
    <h1>
        {{ $content->title }} - добавление
    </h1>
@endsection

@section('content')
    @include('backend.content.partials.form')
@stop
<table class="table table-striped">
<tbody>
    <tr>
      <td class="text-right" style="width: 100px">ФИО</td>
      <td>
        {{ $order->name }}
      </td>
    </tr>

    <tr>
      <td class="text-right">Город</td>
      <td>
        {{ $order->city }}
      </td>
    </tr>

    <tr>
      <td class="text-right">Улица</td>
      <td>
        {{ $order->street }}
      </td>
    </tr>

    <tr>
      <td class="text-right">Дом</td>
      <td>
        {{ $order->home }}
      </td>
    </tr>

    <tr>
      <td class="text-right">Квартира</td>
      <td>
        {{ $order->apartment }}
      </td>
    </tr>

    <tr>
      <td class="text-right">Телефон</td>
      <td>
        {{ $order->phone }}
      </td>
    </tr>

    <tr>
      <td class="text-right">E-mail</td>
      <td>
        {{ $order->email }}
      </td>
    </tr>

    <tr>
      <td class="text-right">Комментарий</td>
      <td>
        {{ $order->comment }}
      </td>
    </tr>

    <tr>
        <td class="text-right">1С код</td>
        <td>
            {{ $order->hash }}
        </td>
    </tr>

    <tr>
        <td class="text-right">GA Client ID</td>
        <td>
            {{ $order->client_id }}
        </td>
    </tr>
</tbody>
</table>
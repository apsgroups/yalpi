<table class="table table-striped">
<thead>
    <tr>
      <th class="text-center">ID</th>
      <th>Наименование</th>
      <th>Артикул</th>
      <th class="text-center">Количество</th>
      <th class="text-center">Цена за ед.</th>
      <th class="text-center">Сумма</th>
    </tr>
</thead>
<tbody>
    @if(!$order->products->isEmpty())
    @foreach($order->products as $product)
    <tr>
      <td class="text-center">{{ $product->product_id }}</td>
      <td>
          <a href="{{ route('admin.product.edit', [$product->product_id]) }}">{{ $product->title }}</a>

          @if(!$product->orderProductProperty->isEmpty())
              <div class="small">
                  {!! $product->orderProductProperty->map(function ($item) {
                      $item->content .= ': '.$item->title;
                      return $item;
                     })->implode('content', '<br />') !!}
              </div>
          @endif

          @if($product->product_attribute_id)
              <div style="font-size: 11px;">
                  {{ $product->attribute_title }}: {{ $product->attribute_value }}
              </div>
          @endif

          @if(!empty($product->desk_id))
              <div class="small">
                  Столешница:
                  @if(!$product->desk)
                      {{ $product->desk_title }}
                  @else
                      {!! link_to_route('admin.product.edit', $product->desk->title, [$product->desk->id]) !!}
                  @endif
              </div>
          @endif

          @if(!empty($product->modules))
              <p> </p>

              <div class="small"><b>Модули</b></div>

              @foreach($product->modules as $m)
              <div class="small">
                  @if(!$m->product)
                      {{ $m->title }}
                  @else
                      {!! link_to_route('admin.product.edit', $m->title, [$product->product_id]) !!}
                  @endif

                  {{ $m->price }} р., {{ $m->amount }} шт.
              </div>
              @endforeach
          @endif
      </td>
      <td>{{ $product->sku }}</td>
      <td class="text-center">{{ $product->amount }}</td>
      <td class="text-center">{{ format_price($product->price) }}</td>
      <td class="text-center">{{ format_price($product->amount * $product->price) }}</td>
    </tr>
    @endforeach
    @endif

    <tr>
        <td colspan="4"></td>
        <td class="text-right">Сумма заказа</td>
        <td class="text-center">{{ format_price($order->subtotal) }}</td>
    </tr>

    <tr>
        <td colspan="4"></td>
        <td class="text-right">Купон</td>
        <td class="text-center">{{ $order->coupon }}</td>
    </tr>

    <tr>
        <td colspan="3"></td>
        <td colspan="2" class="text-right">Скидка по купону</td>
        <td class="text-center">{{ format_price($order->coupon_discount) }}</td>
    </tr>

    <tr>
        <td colspan="4"></td>
        <td class="text-right">{{ $order->amount }} шт.</td>
        <td class="text-center">{{ format_price($order->total) }}</td>
    </tr>

    @if($order->shipping)
    <tr>
        <td colspan="5" class="text-right">Способ доставки</td>
        <td colspan="3">{{ $order->shipping->title }}</td>
    </tr>
    @endif

    @if($order->payment)
    <tr>
        <td colspan="5" class="text-right">Способ оплаты</td>
        <td colspan="3">{{ $order->payment->title }}</td>
    </tr>
    @endif

    <tr>
        <td colspan="5" class="text-right">Нужна сборка</td>
        <td colspan="3">{{ ($order->mounting ? 'Да' : 'Нет') }}</td>
    </tr>

</tbody>
</table>
    <div class="box">
        <div class="box-body">
            <div class="pull-left">
                {!! link_to_route('admin.order.index', 'Закрыть', [], ['class' => 'btn btn-danger btn-sm']) !!}
            </div>
            <div class="clearfix"></div>
        </div><!-- /.box-body -->
    </div><!--box-->

    <section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="fa fa-globe"></i> Заказ №{{ $order->id }} {!! ($order->oneclick ? '<b>(оформлен в 1 клик)</b>' : '') !!}
            <small class="pull-right">Дата: {{ $order->created_at->format('d.m.Y H:i') }}</small>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
      <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
            @include('backend.order.partials.client')
        </div>
        <div class="col-sm-8 invoice-col table-responsive">
            @include('backend.order.partials.products')
        </div>
      </div>
      <!-- /.row -->

      <!-- Table row -->
      <div class="row">
        <!-- /.col -->
      </div>
    </section>
@extends('backend.layouts.master')

@section('title', 'Управление заказами')

@section('page-header')
    <h1>
        Управление заказами
    </h1>
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Список</h3>

            <div class="box-tools pull-right">
                @include('backend.order.partials.header-buttons')
            </div>
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Номер</th>
                        <th>1 клик</th>
                        <th>Сумма</th>
                        <th>Кол-во</th>
                        <th>Имя</th>
                        <th>Телефон</th>
                        <th>E-mail</th>
                        <th>Город</th>
                        <th class="text-center">Дата</th>
                        <th class="text-center">Удалить</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach ($orders as $order)
                            <tr>
                                <td>{!! link_to_route('admin.order.edit', $order->id, [$order->id]) !!}</td>
                                <td>{{ ($order->oneclick ? 'Да' : 'Нет') }}</td>
                                <td>{{ $order->total }}</td>
                                <td>{{ $order->amount }}</td>
                                <td>{{ $order->name }}</td>
                                <td>{{ $order->phone }}</td>
                                <td>{{ $order->email }}</td>
                                <td>{{ $order->city }}</td>
                                <td class="text-center">{{ $order->created_at->format(config('backend.date.format')) }}</td>
                                <td class="text-center">
                                    @include('backend.includes.partials.destroy', ['link' => route('admin.order.destroy', $order->id)])
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

            @include('backend.includes.partials.pagination', ['items' => $orders])
        </div><!-- /.box-body -->
    </div><!--box-->
@stop
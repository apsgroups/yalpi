@extends('backend.layouts.master')

@section('title', 'Категории яндекс.маркета')

@section('page-header')
    <h1>
        Категории яндекс.маркета
    </h1>
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Список категорий</h3>

            <div class="box-tools pull-right">
                @include('backend.market-categories.partials.header-buttons')
            </div>
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Наименование</th>
                        <th class="text-center">Создано</th>
                        <th class="text-center">Обновлено</th>
                        <td class="text-center">Удалить</td>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach ($categories as $category)
                            <tr>
                                <td>{{ $category->id }}</td>
                                <td>{!! link_to_route('admin.market-category.edit', $category->title, [$category->id]) !!}</td>
                                <td class="text-center">{{ $category->created_at->format(config('backend.date.format')) }}</td>
                                <td class="text-center">{{ $category->updated_at->format(config('backend.date.format')) }}</td>
                                <td class="text-center">
                                    @include('backend.includes.partials.destroy', ['link' => route('admin.market-category.destroy', $category->id)])
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

            @include('backend.includes.partials.pagination', ['items' => $categories])
        </div><!-- /.box-body -->
    </div><!--box-->
@stop
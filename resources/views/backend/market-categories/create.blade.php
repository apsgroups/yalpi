@extends('backend.layouts.master')

@section('title', 'Добавление категории')

@section('page-header')
    <h1>
        Добавление категории яндекс.маркет
    </h1>
@endsection

@section('content')
    @include('backend.market-categories.partials.form')
@stop
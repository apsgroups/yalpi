<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Основное</h3>
    </div><!-- /.box-header -->

    <div class="box-body">
        <div class="form-group">
            {!! Form::label('title', 'Наименование', ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-10">
                {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'наименование']) !!}
                {!! $errors->first('title', '<div class="text-danger">:message</div>') !!}
            </div>
        </div>
    </div>
</div>
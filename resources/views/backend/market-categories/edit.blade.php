@extends ('backend.layouts.master')

@section ('title', 'Изменение категории: '.$category->title)

@section('page-header')
    <h1>
        Изменение категории {{ $category->title }}
    </h1>
@endsection

@section('content')
    @include('backend.market-categories.partials.form')
@stop
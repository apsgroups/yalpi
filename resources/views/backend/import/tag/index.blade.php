@extends('backend.layouts.master')

@section('title', 'Теги (перелинковка)')

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Импорт тегов</h3>
        </div>

        <div class="box-body">
            <p>Загрузите csv файл, содержащий солбцы <b>Страница размещения</b>, <b>Ключ</b>, <b>Ссылка</b>, <b>Ключ</b>, <b>Ссылка</b>. Столбы должны быть разделены "<b>;</b>"</p>

            {!! Form::open(['route' => 'admin.import.tag.upload', 'method' => 'post', 'files' => true, 'class' => 'form-horizontal']) !!}

            <div class="form-group">
                <div class="col-lg-2">
                    <input type="file" name="file" />
                </div>
            </div>

            <div class="form-group">
                <div class="col-lg-5">
                    <button type="submit" class="btn btn-primary btn-sm">Импорт</button>
                </div>
            </div>

            {!! Form::close() !!}

            @include('backend.import.partials.reports', ['reports' => $reports, 'operation' => 'tag-import'])
        </div>
    </div>
@stop

@section('after-scripts-end')
    {!! Html::script('js/import.js') !!}
@stop
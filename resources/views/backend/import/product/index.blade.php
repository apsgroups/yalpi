@extends('backend.layouts.master')

@section('title', 'Товары')

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Импорт товаров</h3>
        </div>

        <?php
            $clmns = ['URL', 'Категория 0-ого LVL', 'Категория 1-ого LVL', 'Категория 2-ого LVL', 'Категория 3-ого LVL', 'Наименование урока', 'Цена', 'Тезисное описание', 'URL-курса', 'Платформа', 'Преподаватель', 'О преподавателе', 'Формат', 'Тип обучения', 'Язык', 'Рейтинг', 'Оценка', 'Вес', 'Фильтр что изучаем', 'Фильтр что разрабатываем', 'Фильтр какой тип веб-сайт', 'Фильтр тип разработки сайта', 'Фильтр какой фреймворк', 'Фильтр какая CMS', 'Фильтр какая технология', 'Фильтр какой язык программирования', 'Фильтр разработки', 'Фильтр компания разработчик', 'Фильтр какая программа', 'Фильтр', 'Специалист', 'Пак иконок']
        ?>

        <div class="box-body">
            <div>Загрузите csv файл, содержащий солбцы:</div>
            <div class="alert alert-info"> {{ implode(';', $clmns) }}
            <div>Столбы должны быть разделены "<b>;</b>"</div></div>

            {!! Form::open(['route' => 'admin.import.product.upload', 'method' => 'post', 'files' => true, 'class' => 'form-horizontal']) !!}

            <div class="form-group">
                <div class="col-lg-2">
                    <input type="file" name="file" />
                </div>
            </div>

            <div class="form-group">
                <div class="col-lg-5">
                    <button type="submit" class="btn btn-primary btn-sm">Импорт</button>
                </div>
            </div>

            {!! Form::close() !!}

            @include('backend.import.partials.reports', ['reports' => $reports, 'operation' => 'product-import'])
        </div>
    </div>
@stop

@section('after-scripts-end')
    {!! Html::script('js/import.js') !!}
@stop
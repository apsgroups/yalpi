@extends('backend.layouts.master')

@section('title', 'Импорт изображений для категорий')

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Импорт изображений для категорий</h3>
        </div>

        <?php
            $clmns = ['URL', 'Путь к изображению'];
        ?>

        <div class="box-body">
            <div>Загрузите csv файл, содержащий солбцы:</div>
            <div class="alert alert-info"> {{ implode(';', $clmns) }}
            <div>Столбы должны быть разделены "<b>;</b>"</div></div>
            <div>Изображение должны находиться в каталоге <b>uploads/media/category-images-import/</b></div>
            <div>Указывайте путь от каталога uploads/media/category-images-import/, например <b>kukhni/modulnye.jpg</b></div>
            <div>Загрузить изображения с помощью <a target="_blank" href="/elfinder/ckeditor/?CKEditor=description&CKEditorFuncNum=1&langCode=en#elf_l1_Y29udGVudC9jYXRlZ29yaWVzLWltYWdlcy9iaWcvY2F0ZWdvcnktaW1hZ2VzLWltcG9ydA">медиа менеджера</a></div>

            <p>&nbsp;</p>

            {!! Form::open(['route' => 'admin.import.category-image.upload', 'method' => 'post', 'files' => true, 'class' => 'form-horizontal']) !!}

            <div class="form-group">
                <div class="col-lg-2">
                    <input type="file" name="file" />
                </div>
            </div>

            <div class="form-group">
                <div class="col-lg-5">
                    <button type="submit" class="btn btn-primary btn-sm">Импорт</button>
                </div>
            </div>

            {!! Form::close() !!}

            @include('backend.import.partials.reports', ['reports' => $reports, 'operation' => 'category-image-import'])
        </div>
    </div>
@stop

@section('after-scripts-end')
    {!! Html::script('js/import.js') !!}
@stop
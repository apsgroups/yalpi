@extends('backend.layouts.master')

@section('title', 'Товары, пропавшие из импорта')

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Товары, пропавшие из импорта</h3>

            <div class="pull-right">
                {{ link_to_route('admin.import.lost.download', 'Экспорт в csv', [], ['class' => 'btn btn-success btn-sm']) }}
            </div>
        </div>

        <div class="box-body">

            <p class="box-tools">
                @include('backend.import.partials.search')
            </p>

            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th style="width: 25px">ID</th>
                        <th>Наименование</th>
                        <th>Артикул</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach ($products as $product)
                            <tr>
                                <td>{{ $product->id }}</td>
                                <td>{{ link_to_route('admin.product.edit', $product->title, [$product->id]) }}</td>
                                <td>{{ $product->ext_id }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

            @include('backend.includes.partials.pagination', ['items' => $products])
        </div>
    </div>
@stop
@extends('backend.layouts.master')

@section('title', 'Импорт приоритетов товаров по цветам')

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Импорт приоритетов товаров по цветам</h3>
        </div>

        <?php
            $clmns = ['Наименование цвета', 'Приоритет'];
        ?>

        <div class="box-body">
            <div>Загрузите csv файл, содержащий солбцы:</div>
            <div class="alert alert-info"> {{ implode(';', $clmns) }}
            <div>Столбы должны быть разделены "<b>;</b>"</div></div>

            {!! Form::open(['route' => 'admin.import.color-priority.upload', 'method' => 'post', 'files' => true, 'class' => 'form-horizontal']) !!}

            <div class="form-group">
                <div class="col-lg-2">
                    <input type="file" name="file" />
                </div>
            </div>

            <div class="form-group">
                <div class="col-lg-5">
                    <button type="submit" class="btn btn-primary btn-sm">Импорт</button>
                </div>
            </div>

            {!! Form::close() !!}

            @include('backend.import.partials.reports', ['reports' => $reports, 'operation' => 'color-priority-import'])
        </div>
    </div>

    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Таблица цветов</h3>
        </div>

        <div class="box-body">
            <table class="table table-striped table-bordered table-hover">
                <thead>                    
                    <tr>
                        <th>Наименование</th>
                        <th>Вес</th>
                        <th>Иконка</th>
                    </tr>
                </thead>

                @foreach($propertyValues as $v)
                    <tr>
                        <td>{{ $v->title }}</td>
                        <td>{{ $v->sort_priority }}</td>
                        <td>
                            @if($img = img_src($v->image, 'color'))
                                <img src="{{ $img }}" alt="{{ $v->title }}" />
                            @else
                                Нет
                            @endif
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
@stop

@section('after-scripts-end')
    {!! Html::script('js/import.js') !!}
@stop
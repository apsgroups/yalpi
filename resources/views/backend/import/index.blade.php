@extends('backend.layouts.master')

@section('title', 'Импорт')

@section('content')
    @include('backend.import.partials.box', ['title' => 'Импорт цен', 'type' => 'price', 'reports' => $priceReports, 'operation' => 'price-import'])
    @include('backend.import.partials.box', ['title' => 'Импорт характеристик', 'type' => 'characteristic', 'reports' => $characteristicReports, 'operation' => 'characteristic-import'])
@stop

@section('after-scripts-end')
    {!! Html::script('js/import.js') !!}
@stop
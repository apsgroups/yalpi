@extends('backend.layouts.master')

@section('title', 'META')

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Импорт META</h3>
        </div>

        <div class="box-body">
            <p>Загрузите csv файл, содержащий столбцы <b>URL</b>, <b>TITLE</b>, <b>DESCRIPTION</b>, <b>H1</b>. Столбы должны быть разделены "<b>;</b>"</p>

            {!! Form::open(['route' => 'admin.import.meta.upload', 'method' => 'post', 'files' => true, 'class' => 'form-horizontal']) !!}

            <div class="form-group">
                <div class="col-lg-2">
                    <input type="file" name="file" />
                </div>
            </div>

            <div class="form-group">
                <div class="col-lg-5">
                    <button type="submit" class="btn btn-primary btn-sm">Импорт</button>
                </div>
            </div>

            {!! Form::close() !!}

            @include('backend.import.partials.reports', ['reports' => $reports, 'operation' => 'meta-import'])
        </div>
    </div>
@stop

@section('after-scripts-end')
    {!! Html::script('js/import.js') !!}
@stop
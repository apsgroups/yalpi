@extends('backend.layouts.master')

@section('title', 'Артикулы')

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Экспорт артикулов</h3>
        </div>

        <div class="box-body">
            {{ link_to_route('admin.import.articul.export', 'Экспорт товаров', [], ['class' => 'btn btn-success btn-sm']) }}
        </div>
    </div>

    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Импорт артикулов</h3>
        </div>

        <div class="box-body">
            <p>Загрузите csv файл, содержащий солбцы <b>ID товара</b>, <b>Наименование</b>, <b>1С артикул</b>. Столбы должны быть разделены "<b>;</b>"</p>
            <p>Или получите его путем экспорта</p>

            {!! Form::open(['route' => 'admin.import.articul.upload', 'method' => 'post', 'files' => true, 'class' => 'form-horizontal']) !!}

            <div class="form-group">
                <div class="col-lg-2">
                    <input type="file" name="file" />
                </div>
            </div>

            <div class="form-group">
                <div class="col-lg-5">
                    <label>
                        <input type="hidden" name="save_empty" value="" />
                        <input type="checkbox" name="save_empty" value="1" />
                        Стереть артикул у товара, если в файле пустое значение
                    </label>
                </div>
            </div>

            <div class="form-group">
                <div class="col-lg-5">
                    <button type="submit" class="btn btn-primary btn-sm">Импорт</button>
                </div>
            </div>

            {!! Form::close() !!}

            @include('backend.import.partials.reports', ['reports' => $reports, 'operation' => 'articul-import'])
        </div>
    </div>
@stop

@section('after-scripts-end')
    {!! Html::script('js/import.js') !!}
@stop
@extends('backend.layouts.master')

@section('title', 'Перелинковка категорий')

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Импорт перелинковки для категорий</h3>
        </div>

        <?php
            $clmns = ['URL', 'Шаблон дочерних', 'Выводить заголовок', 'Выводить категорию-1', 'Выводить категорию-2'];
        ?>

        <div class="box-body">
            <div>Загрузите csv файл, содержащий солбцы:</div>
            <div class="alert alert-info"> {{ implode(';', $clmns) }}
            <div>Столбы должны быть разделены "<b>;</b>"</div></div>

            {!! Form::open(['route' => 'admin.import.category-link.upload', 'method' => 'post', 'files' => true, 'class' => 'form-horizontal']) !!}

            <div class="form-group">
                <div class="col-lg-2">
                    <input type="file" name="file" />
                </div>
            </div>

            <div class="form-group">
                <div class="col-lg-5">
                    <button type="submit" class="btn btn-primary btn-sm">Импорт</button>
                </div>
            </div>

            {!! Form::close() !!}

            @include('backend.import.partials.reports', ['reports' => $reports, 'operation' => 'category-link-import'])
        </div>
    </div>
@stop

@section('after-scripts-end')
    {!! Html::script('js/import.js') !!}
@stop
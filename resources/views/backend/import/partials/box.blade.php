<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">{{ $title }}</h3>
    </div>

    <div class="box-body">
        <div id="import-box-{{ $type }}">
            <div class="clearfix">
                <small class="pull-left import-status"></small>
            </div>

            <div class="progress">
                <div class="progress-bar progress-bar-primary progress-bar-striped" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                  <span class="sr-only"></span>
                </div>
            </div>

            <p>
                <button class="btn btn-success btn-sm run-import" data-type="{{ $type }}">Импорт</button>
            </p>
        </div>

        @include('backend.import.partials.reports', ['reports' => $reports, 'operation' => $operation])
    </div>
</div>
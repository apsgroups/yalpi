<h3>Отчеты</h3>

<div class="report-list" data-operation="{{ $operation }}">
    @if(count($reports))
        @foreach($reports as $report)
            <div id="report-{{ $report->id }}">
                <a href="{{ route('admin.import.report.download', [$report->id]) }}">
                    <i class="fa fa-cloud-download"></i>
                    {{ $report->created_at->format('d.m.Y H:i:s') }}
                </a>
                &nbsp;
                <a href="#" class="report-delete" data-id="{{ $report->id }}"><i class="fa fa-close"></i> удалить</a>
            </div>
        @endforeach

        <a href="#" class="report-clean" data-operation="{{ $operation }}"><i class="fa fa-close"></i> удалить все</a>
    @else
        <div>Отчетов нет</div>
    @endif
</div>
{!! Form::open(['route' => request()->route()->getName(), 'class' => 'form-inline', 'role' => 'form ', 'method' => 'get']) !!}
    <div>
        <div class="input-group input-group-sm">
            {!! Form::text('search', $search, ['class' => 'form-control', 'placeholder' => 'Найти']) !!}
            <span class="input-group-btn">
              <button type="submit" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
            </span>
        </div>

        <div class="input-group input-group-sm">
            @if(!empty($search))
            &nbsp;{!! link_to_route(request()->route()->getName(), 'Сбросить', null, ['class' => 'btn btn-danger btn-sm']) !!}
            @endif
        </div>
    </div>

    <div class="clearfix"></div>
{!! Form::close() !!}
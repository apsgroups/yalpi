@extends('backend.layouts.master')

@section('title', 'Товары, отсутствующие на сайте')

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Товары, отсутствующие на сайте</h3>

            <div class="pull-right">
                {{ link_to_route('admin.import.missing.download', 'Экспорт в csv', [], ['class' => 'btn btn-success btn-sm']) }}
            </div>
        </div>

        <div class="box-body">
            <p>Список обновляется после {!! link_to_route('admin.import.index', 'Импорта') !!}.</p>

            <p class="box-tools">
                @include('backend.import.partials.search')
            </p>

            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th style="width: 25px">ID</th>
                        <th>Наименование</th>
                        <th>1С Артикул</th>
                        <th>Артикул</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach ($articuls as $articul)
                            <tr>
                                <td>{{ $articul->id }}</td>
                                <td>{{ $articul->name }}</td>
                                <td>{{ $articul->articul }}</td>
                                <td>{{ $articul->sku }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

            @include('backend.includes.partials.pagination', ['items' => $articuls])
        </div>
    </div>
@stop
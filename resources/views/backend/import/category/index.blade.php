@extends('backend.layouts.master')

@section('title', 'Категории')

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Импорт категорий</h3>
        </div>

        <?php
            $clmns = ['URL', 'Синоним (ЧПУ)', 'Заголовок', 'Родительская', 'Путь-1', 'Путь-2', 'Путь-3', 
                'Title', 'Description', 'KeyWords', 'SEO-текст', 'Вопросы-ответы-1', 'Вопросы-ответы-2', 'Вопросы-ответы-3', 'Вопросы-ответы-4', 'Вопросы-ответы-5', 
            'Статья-1', 'Статья-2', 'Отзыв-1', 'Отзыв-2', 'Отзыв-3', 'Сайдбар', 'h1', 'Посадочная']
        ?>

        <div class="box-body">
            <div>Загрузите csv файл, содержащий солбцы:</div>
            <div class="alert alert-info"> {{ implode(';', $clmns) }}
            <div>Столбы должны быть разделены "<b>;</b>"</div></div>

            {!! Form::open(['route' => 'admin.import.category.upload', 'method' => 'post', 'files' => true, 'class' => 'form-horizontal']) !!}

            <div class="form-group">
                <div class="col-lg-2">
                    <input type="file" name="file" />
                </div>
            </div>

            <div class="form-group">
                <div class="col-lg-5">
                    <button type="submit" class="btn btn-primary btn-sm">Импорт</button>
                </div>
            </div>

            {!! Form::close() !!}

            @include('backend.import.partials.reports', ['reports' => $reports, 'operation' => 'category-import'])
        </div>
    </div>
@stop

@section('after-scripts-end')
    {!! Html::script('js/import.js') !!}
@stop
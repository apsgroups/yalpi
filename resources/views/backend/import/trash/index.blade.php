@extends('backend.layouts.master')

@section('title', 'Корзина артикулов')

@section('content')
    @include('backend.import.trash.partials.form')

    @include('backend.import.trash.partials.upload')

    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Корзина артикулов</h3>

            <div class="box-tools pull-right">
                @include('backend.import.trash.partials.header-buttons')
            </div>
        </div>

        <div class="box-body">

            <p class="box-tools">
                @include('backend.import.partials.search')
            </p>

            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        @include('backend.includes.partials.check.all')
                        <th style="width: 25px">ID</th>
                        <th>Артикул</th>
                        <th style="width: 50px">Удалить</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach ($articuls as $articul)
                            <tr>
                                @include('backend.includes.partials.check.item', ['id' => $articul->id])
                                <td>{{ $articul->id }}</td>
                                <td>{{ $articul->articul }}</td>
                                <td class="text-center">
                                    @include('backend.includes.partials.destroy', ['link' => route('admin.import.trash.destroy', $articul->id)])
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

            @include('backend.includes.partials.pagination', ['items' => $articuls])
        </div><!-- /.box-body -->
    </div><!--box-->
@stop
{!! Form::open(['files' => true, 'route' => 'admin.import.trash.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) !!}
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Добавить артикул</h3>
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="form-group">
                <div class="col-lg-3">
                    {!! Form::text('articul', null, ['class' => 'form-control', 'placeholder' => 'Артикул']) !!}
                    {!! $errors->first('articul', '<div class="text-danger">:message</div>') !!}
                </div>

                <div class="col-lg-2">
                    <input type="submit" class="btn btn-success" value="Добавить" />
                </div>
            </div>
        </div>
    </div>
{!! Form::close() !!}
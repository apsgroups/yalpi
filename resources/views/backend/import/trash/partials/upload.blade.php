{!! Form::open(['files' => true, 'route' => 'admin.import.trash.upload', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) !!}
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Импортировать артикулы</h3>
        </div>

        <div class="box-body">
            <p>Загрузите csv файл. Столбец Артикул. Разделения ";"</p>

            <div class="form-group">
                <div class="col-lg-3">
                    {!! Form::file('articuls') !!}
                    {!! $errors->first('articuls', '<div class="text-danger">:message</div>') !!}
                </div>

                <div class="col-lg-2">
                    <input type="submit" class="btn btn-success" value="Импортировать" />
                </div>
            </div>
        </div>
    </div>
{!! Form::close() !!}
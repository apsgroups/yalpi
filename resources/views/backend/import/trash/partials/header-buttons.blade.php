<div class="pull-right" style="margin-bottom:10px">
    <div class="btn-group">
        @include('backend.includes.partials.check.action', ['scope' => 'import.trash'])

        {!! Form::open(['route' => 'admin.import.trash.clean', 'method' => 'delete']) !!}
            <button type="submit" class="btn btn-danger" onclick="return confirm('Все артикулы, находящиеся в корзине будут удалены. Удалить?')">Удалить все</button>
        {!! Form::close() !!}
    </div><!--btn group-->
</div>
@extends ('backend.layouts.master')

@section ('title', 'Изменение тега: '.$tag->title)

@section('page-header')
    <h1>
        Изменение тега {{ $tag->title }}
    </h1>
@endsection

@section('content')
    @include('backend.tag.partials.form')
@stop
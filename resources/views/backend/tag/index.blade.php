@extends('backend.layouts.master')

@section('title', 'Теги (Перелинковка)')

@section('page-header')
    <h1>
        Управление тегами (Перелинковка)
    </h1>
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Список тегов</h3>

            <div class="box-tools pull-right">
                @include('backend.tag.partials.header-buttons')
            </div>
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Наименование</th>
                        <th>Ссылается на</th>
                        <th class="text-center">Создано</th>
                        <th class="text-center">Обновлено</th>
                        <th class="text-center">Опубликован</th>
                        <td class="text-center">Удалить</td>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach ($tags as $tag)
                            <tr>
                                <td>{{ $tag->id }}</td>
                                <td>{!! link_to_route('admin.tag.edit', $tag->title, [$tag->id]) !!}</td>
                                <td>{{ ($tag->tagable ? $tag->tagable->link() : '') }}</td>
                                <td class="text-center">{{ $tag->created_at->format(config('backend.date.format')) }}</td>
                                <td class="text-center">{{ $tag->updated_at->format(config('backend.date.format')) }}</td>
                                <td class="text-center">
                                    @include('backend.includes.partials.toggle', ['route' => ['admin.tag.toggle', $tag->id], 'model' => $tag, 'toggle' => 'published'])
                                </td>
                                <td class="text-center">
                                    @include('backend.includes.partials.destroy', ['link' => route('admin.tag.destroy', $tag->id)])
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

            @include('backend.includes.partials.pagination', ['items' => $tags])
        </div><!-- /.box-body -->
    </div><!--box-->
@stop
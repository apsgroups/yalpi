@extends('backend.layouts.master')

@section('title', 'Добавление тега')

@section('page-header')
    <h1>
        Добавление тега
    </h1>
@endsection

@section('content')
    @include('backend.tag.partials.form')
@stop
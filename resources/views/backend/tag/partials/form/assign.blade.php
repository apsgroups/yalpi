<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Основное</h3>
    </div><!-- /.box-header -->

    <div class="box-body">
        @if(isset($tag) && $tag->categories->count())
            <h3>Категории</h3>

            <ul>
                @foreach($tag->categories as $category)
                    <li>{!! link_to_route('admin.category.edit', $category->title, [$category->id]) !!}</li>
                @endforeach
            </ul>
        @endif

        @if(isset($tag) && $tag->prettyUrls->count())
            <h3>URL фильтров</h3>

            <ul>
                @foreach($tag->prettyUrls as $url)
                    <li>{!! link_to_route('admin.pretty-url.edit', $url->pretty_url, [$url->id]) !!}</li>
                @endforeach
            </ul>
        @endif
    </div>
</div>
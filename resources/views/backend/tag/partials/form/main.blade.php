<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Основное</h3>
    </div><!-- /.box-header -->

    <div class="box-body">
        <div class="form-group">
            {!! Form::label('title', 'Наименование', ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-10">
                {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'наименование']) !!}
                {!! $errors->first('title', '<div class="text-danger">:message</div>') !!}
            </div>
        </div><!--form control-->

        <div class="form-group">
            {!! Form::label('tagable_type', 'Тип', ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-10">
                {!! Form::select('tagable_type', [
                    \App\Models\Category::class => 'Категория',
                    \App\Models\PrettyUrl::class => 'URL фильтра',
                ], null, ['class' => 'form-control']) !!}
                {!! $errors->first('tagable_type', '<div class="text-danger">:message</div>') !!}
            </div>
        </div><!--form control-->

        <div class="form-group">
            {!! Form::label('tagable_id', 'ID', ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-10">
                {!! Form::text('tagable_id', null, ['class' => 'form-control']) !!}
                {!! $errors->first('tagable_id', '<div class="text-danger">:message</div>') !!}
            </div>
        </div><!--form control-->

        <div class="form-group">
            <label class="col-lg-2 control-label">Опубликовано</label>
            <div class="col-lg-1">
                {!! Form::hidden('published', 0) !!}
                {!! Form::checkbox('published', 1, (isset($tag) && $tag->published === 1)) !!}
                {!! $errors->first('published', '<div class="text-danger">:message</div>') !!}
            </div>
        </div>
    </div><!-- /.box-body -->
</div><!--box-->
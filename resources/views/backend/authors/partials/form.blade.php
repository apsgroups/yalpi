@if(!empty($item))
{!! Form::model($item, ['route' => ['admin.author.update', $item->id], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'put', 'files' => true]) !!}
@else
{!! Form::open(['route' => 'admin.author.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post', 'files' => true]) !!}
@endif

    @include('backend.includes.partials.save-buttons', ['cancel' => route('admin.author.index')])

    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Основное</h3>
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="form-group">
                {!! Form::label('name', 'Имя', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-10">
                    {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'наименование']) !!}
		            {!! $errors->first('name', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>

            <div class="form-group">
                {!! Form::label('slug', 'Псевдоним', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-10">
                    {!! Form::text('slug', null, ['class' => 'form-control', 'placeholder' => 'псевдоним']) !!}
                    {!! $errors->first('slug', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>

            <div class="form-group">
                {!! Form::label('image', 'Фото', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-10">
                    {!! Form::file('image', null) !!}
                    {!! $errors->first('image', '<div class="text-danger">:message</div>') !!}

                    @if(isset($item) && $item->image)
                        <div style="margin-top: 30px" class="no-clone"><img src="{{ img_src($item->image, 'preview') }}" alt=""></div>
                    @endif
                </div>
            </div>

            <div class="form-group">
                {!! Form::label('about', 'Об авторе', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-10">
                    {!! Form::textarea('about', null, ['class' => 'form-control', 'placeholder' => 'об авторе']) !!}
                    {!! $errors->first('about', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>

            <div class="form-group">
                {!! Form::label('slogan', 'Девиз', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-10">
                    {!! Form::textarea('slogan', null, ['class' => 'form-control', 'placeholder' => 'любимая цитата']) !!}
                    {!! $errors->first('slogan', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>

            <?php
            $socials = ['facebook', 'vk', 'instagram', 'twitter', 'youtube'];
            ?>

            @foreach($socials as $v)
            <div class="form-group">
                {!! Form::label($v, ucfirst($v), ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-10">
                    {!! Form::text($v, null, ['class' => 'form-control', 'placeholder' => 'URL']) !!}
                    {!! $errors->first($v, '<div class="text-danger">:message</div>') !!}
                </div>
            </div>
            @endforeach
        </div>
    </div>

    @include('backend.includes.partials.save-buttons', ['cancel' => route('admin.author.index')])

    {!! Form::hidden('id') !!}
{!! Form::close() !!}
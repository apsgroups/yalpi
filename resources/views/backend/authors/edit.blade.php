@extends ('backend.layouts.master')

@section ('title', 'Изменение автора: '.$item->name)

@section('page-header')
    <h1>
        Изменение Автора {{ $item->name }}
    </h1>
@endsection

@section('content')
    @include('backend.authors.partials.form')
@stop
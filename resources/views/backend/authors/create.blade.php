@extends('backend.layouts.master')

@section('title', 'Добавление автора')

@section('page-header')
    <h1>
        Добавление автора
    </h1>
@endsection

@section('content')
    @include('backend.authors.partials.form')
@stop
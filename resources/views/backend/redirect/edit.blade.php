@extends ('backend.layouts.master')

@section ('title', 'Изменение редиректа')

@section('page-header')
    <h1>
        Изменение: {{ $redirect->from_url }}
    </h1>
@endsection

@section('content')
    @include('backend.redirect.partials.form')
@stop
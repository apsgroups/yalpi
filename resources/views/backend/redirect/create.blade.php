@extends ('backend.layouts.master')

@section ('title', 'Добавление редиректа')

@section('page-header')
    <h1>
        Добавление редиректа
    </h1>
@endsection

@section('content')
    @include('backend.redirect.partials.form')
@stop
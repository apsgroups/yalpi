@extends('backend.layouts.master')

@section('title', 'Управление редиректами')

@section('page-header')
    <h1>
        Управление редиректами
    </h1>
@endsection

@section('content')
    @include('backend.redirect.partials.upload')

    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Список</h3>

            <div class="box-tools pull-right">
                @include('backend.redirect.partials.header-buttons')
            </div>
        </div><!-- /.box-header -->

        <div class="box-body">
            @include('backend.redirect.partials.filter')

            <p>&nbsp;</p>

            @include('backend.includes.partials.check.action', ['scope' => 'redirect'])

            <p>&nbsp;</p>

            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        @include('backend.includes.partials.check.all')
                        <th>ID</th>
                        <th>С адреса</th>
                        <th class="text-center">На адрес</th>
                        <th class="text-center">Создано</th>
                        <th class="text-center">Обновлено</th>
                        <th class="text-center">Удалить</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach ($redirects as $redirect)
                            <tr>
                                @include('backend.includes.partials.check.item', ['id' => $redirect->id])
                                <td>{!! $redirect->id !!}</td>
                                <td>{!! link_to_route('admin.redirect.edit', $redirect->from_url, [$redirect->id]) !!}</td>
                                <td>{{ $redirect->link() }}</td>
                                <td class="text-center">{{ $redirect->created_at->format(config('backend.date.format')) }}</td>
                                <td class="text-center">{{ $redirect->updated_at->format(config('backend.date.format')) }}</td>
                                <td class="text-center">
                                    @include('backend.includes.partials.destroy', ['link' => route('admin.redirect.destroy', $redirect->id)])
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

            @include('backend.includes.partials.pagination', ['items' => $redirects])
        </div><!-- /.box-body -->
    </div><!--box-->
@stop
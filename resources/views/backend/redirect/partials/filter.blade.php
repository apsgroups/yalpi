<div class="row">
    {!! Form::open(['files' => true, 'route' => 'admin.redirect.index', 'class' => 'form-inline filter', 'role' => 'form', 'method' => 'get']) !!}
    <div class="col-lg-2">
        <div class="input-group">
            {!! Form::text('filter[search]', $filter['search'], ['placeholder' => 'Поиск', 'class' => 'form-control']) !!}

            <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
            </span>
        </div>
    </div>

    <div class="col-lg-8">
        @if(request()->get('filter'))
        {!! link_to_route('admin.redirect.index', 'Сбросить', [], ['class' => 'btn btn-danger']) !!}
        @endif
    </div>

    <div class="clearfix"></div>
    {!! Form::close() !!}
</div>
{!! Form::open(['files' => true, 'route' => 'admin.redirect.upload', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) !!}
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Импортировать URL-редиректы</h3>
        </div>

        <div class="box-body">
            <p>Загрузите файл. По одному URL на строку для автоматического определения нового адреса или CSV файл с разделителем ";" в виде old/my/page/;new/my/page</p>

            <div class="form-group">
                <div class="col-lg-3">
                    {!! Form::file('urls') !!}
                    {!! $errors->first('urls', '<div class="text-danger">:message</div>') !!}
                </div>

                <div class="col-lg-2">
                    <input type="submit" class="btn btn-success" value="Импортировать" />
                </div>
            </div>
        </div>
    </div>
{!! Form::close() !!}
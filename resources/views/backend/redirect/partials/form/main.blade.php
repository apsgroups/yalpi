<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Основное</h3>
    </div><!-- /.box-header -->

    <div class="box-body">
        <div class="form-group">
            {!! Form::label('from_url', 'С адреса', ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-6">
                {!! Form::text('from_url', null, ['class' => 'form-control', 'placeholder' => 'some-url']) !!}
                {!! $errors->first('from_url', '<div class="text-danger">:message</div>') !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('item_type', 'Тип', ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-6">
                {!! Form::select('item_type', config('redirect.types'), null, ['class' => 'form-control']) !!}
                {!! $errors->first('item_type', '<div class="text-danger">:message</div>') !!}
            </div>
        </div>

        <div class="form-group search-block">
            {!! Form::label('item_search', 'На элемент', ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-6">
                {!! Form::text('item_search', null, ['class' => 'form-control', 'placeholder' => 'Введите имя элемента']) !!}
                {!! Form::text('item_id', null, ['class' => 'form-control', 'placeholder' => 'или укажите id']) !!}
                {!! $errors->first('item_search', '<div class="text-danger">:message</div>') !!}
            </div>
        </div>

        <div class="form-group url-block">
            {!! Form::label('to_url', 'Произвольный', ['class' => 'col-lg-2 control-label']) !!}
            <div class="col-lg-6">
                {!! Form::text('to_url', null, ['class' => 'form-control']) !!}
                {!! $errors->first('to_url', '<div class="text-danger">:message</div>') !!}
            </div>
        </div>
    </div>
</div>
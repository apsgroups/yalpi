@extends('backend.layouts.master')

@section('title', 'Сообщения с форм сайта')

@section('page-header')
    <h1>
        Сообщения с форм сайта
    </h1>
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Сообщения с форм сайта</h3>
        </div>

        <div class="box-body">
            @include('backend.feedback.partials.filter')

            <p>&nbsp;</p>

            @include('backend.includes.partials.check.action', ['scope' => 'feedback'])

            <p>&nbsp;</p>

            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        @include('backend.includes.partials.check.all')
                        <th>ID</th>
                        <th></th>
                        <th>Имя</th>
                        <th>Форма</th>
                        <th>Телефон</th>
                        <th>E-mail</th>
                        <th>Город</th>
                        <th>Сообщение</th>
                        <th class="text-center">Создано</th>
                        <td class="text-center">Удалить</td>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach ($feedbacks as $feedback)
                            <tr>
                                @include('backend.includes.partials.check.item', ['id' => $feedback->id])
                                <td>{{ $feedback->id }}</td>
                                <td>
                                    <a href="{{ route('admin.feedback.show', [$feedback->id]) }}" class="btn btn-xs btn-primary">
                                        <i class="fa fa-search"></i>
                                    </a>
                                </td>
                                <td>{{ $feedback->name }}</td>
                                <td>{{ trans('feedback.'.$feedback->feedback.'.title') }}</td>
                                <td>{{ $feedback->phone }}</td>
                                <td>{{ $feedback->email }}</td>
                                <td>{{ $feedback->city }}</td>
                                <td>{{ str_limit($feedback->message, 30) }}</td>
                                <td class="text-center">{{ $feedback->created_at->format(config('backend.date.format')) }}</td>
                                <td class="text-center">
                                    @include('backend.includes.partials.destroy', ['link' => route('admin.feedback.destroy', $feedback->id)])
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

            @include('backend.includes.partials.pagination', ['items' => $feedbacks])
        </div><!-- /.box-body -->
    </div><!--box-->
@stop
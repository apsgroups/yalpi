<div class="box-body">
    <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover">
            <tbody>
                <tr>
                    <th>ID</th>
                    <td>{{ $feedback->id }}</td>
                </tr>
                <tr>
                    <th>Форма</th>
                    <td>{{ trans('feedback.'.$feedback->feedback.'.title') }}</td>
                </tr>
                <tr>
                    <th>Имя</th>
                    <td>{{ $feedback->name }}</td>
                </tr>
                <tr>
                    <th>Телефон</th>
                    <td>{{ $feedback->phone }}</td>
                </tr>
                <tr>
                    <th>E-mail</th>
                    <td>{{ $feedback->email }}</td>
                </tr>
                <tr>
                    <th>Город</th>
                    <td>{{ $feedback->city }}</td>
                </tr>
                <tr>
                    <th>Сообщение</th>
                    <td>{{ $feedback->message }}</td>
                </tr>
                <tr>
                    <th>Создано</th>
                    <td>{{ $feedback->created_at->format(config('backend.date.format')) }}</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
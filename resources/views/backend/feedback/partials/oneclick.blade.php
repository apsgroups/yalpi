<table class="table table-striped table-bordered table-hover">
    <tbody>
        <tr>
            <th style="width: 130px">Имя</th>
            <td>{{ $feedback->name }}</td>
        </tr>
        <tr>
            <th>Телефон</th>
            <td>{{ $feedback->phone }}</td>
        </tr>
        @if($feedback->order_id)
        <tr>
            <th>Заказ</th>
            <td>{!! link_to_route('admin.order.edit', $feedback->order_id, $feedback->order_id) !!}</td>
        </tr>
        @endif
    </tbody>
</table>
<table class="table table-striped table-bordered table-hover">
    <tbody>
        <tr>
            <th style="width: 130px">Имя</th>
            <td>{{ $feedback->name }}</td>
        </tr>
        <tr>
            <th>Телефон</th>
            <td>{{ $feedback->phone }}</td>
        </tr>
        @if($feedback->product)
        <tr>
            <th>Товар</th>
            <td>{!! link_to_route('admin.product.edit', $feedback->product->title, $feedback->product->id) !!}</td>
        </tr>
        @endif
    </tbody>
</table>
{!! Form::open(['route' => 'admin.feedback.index', 'class' => 'form-inline', 'role' => 'form ', 'method' => 'get']) !!}
    <div>
        <div class="input-group input-group-sm">
            {!! Form::label('search', 'Найти') !!}
            <div>
            {!! Form::text('filter[search]', @$filter['search'], ['class' => 'form-control', 'placeholder' => 'Найти']) !!}
            <span class="input-group-btn">
              <button type="submit" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
            </span>
            </div>
        </div>

        <div class="input-group input-group-sm">
            {!! Form::label('feedback', 'Форма') !!}
            {!! Form::select('filter[feedback]', $feedbackList, @$filter['feedback'], ['class' => 'form-control']) !!}
        </div>

        <div class="input-group input-group-sm">
            <br />
            <button type="submit" class="btn btn-success"><i class="fa fa-search"></i></button>

            @if($filter)
            &nbsp;{!! link_to_route('admin.feedback.index', 'Сбросить', null, ['class' => 'btn btn-danger']) !!}
            @endif
        </div>
    </div>

    <div class="clearfix"></div>
{!! Form::close() !!}
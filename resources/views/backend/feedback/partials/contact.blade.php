<table class="table table-striped table-bordered table-hover">
    <tbody>
        <tr>
            <th style="width: 130px">Имя</th>
            <td>{{ $feedback->name }}</td>
        </tr>
        <tr>
            <th>Телефон</th>
            <td>{{ $feedback->phone }}</td>
        </tr>
        <tr>
            <th>E-mail</th>
            <td>{{ $feedback->email }}</td>
        </tr>
        <tr>
            <th>Сообщение</th>
            <td>{{ $feedback->message }}</td>
        </tr>
    </tbody>
</table>
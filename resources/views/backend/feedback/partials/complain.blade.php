<table class="table table-striped table-bordered table-hover">
    <tbody>
        <tr>
            <th style="width: 130px">Имя</th>
            <td>{{ $feedback->name }}</td>
        </tr>
        <tr>
            <th>E-mail</th>
            <td>{{ $feedback->email }}</td>
        </tr>
        <tr>
            <th>Номер заказа</th>
            <td>{{ $feedback->order_number }}</td>
        </tr>
        <tr>
            <th>Суть проблемы</th>
            <td>{{ $feedback->message }}</td>
        </tr>
    </tbody>
</table>
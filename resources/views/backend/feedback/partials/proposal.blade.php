<table class="table table-striped table-bordered table-hover">
    <tbody>
        <tr>
            <th style="width: 130px">Имя</th>
            <td>{{ $feedback->name }}</td>
        </tr>
        <tr>
            <th>Телефон</th>
            <td>{{ $feedback->phone }}</td>
        </tr>
        <tr>
            <th>E-mail</th>
            <td>{{ $feedback->email }}</td>
        </tr>
        <tr>
            <th>Сообщение</th>
            <td>{{ $feedback->message }}</td>
        </tr>
        
        @if($feedback->multiupload && $feedback->multiupload->images->count())
        <tr>
            <th>Прикреплено</th>
            <td>
                @foreach($feedback->multiupload->images as $image)
                    <p>
                        <a class="fancybox" rel="gallery" href="/{{ \App\Services\Image\ImageService::getFullpath($image) }}">
                        <img src="{{ img_src($image, 'preview') }}" />
                        {{ $image->original_name }}
                        </a>
                    </p>
                @endforeach
            </td>
        </tr>
        @endif
    </tbody>
</table>
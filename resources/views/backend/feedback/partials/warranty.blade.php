<table class="table table-striped table-bordered table-hover">
    <tbody>
        <tr>
            <th style="width: 130px">Номер заказа</th>
            <td>{{ $feedback->order_number }}</td>
        </tr>
        <tr>
            <th>Телефон</th>
            <td>{{ $feedback->phone }}</td>
        </tr>
        <tr>
            <th>Имя</th>
            <td>{{ $feedback->name }}</td>
        </tr>
        <tr>
            <th>E-mail</th>
            <td>{{ $feedback->email }}</td>
        </tr>
        <tr>
            <th>Описание проблемы</th>
            <td>{{ $feedback->message }}</td>
        </tr>
        @if($feedback->multiupload && $feedback->multiupload->images->count())
        <tr>
            <th>Прикреплено</th>
            <td>
                @foreach($feedback->multiupload->images as $image)
                    <p>
                        <a class="fancybox" rel="gallery" href="/{{ \App\Services\Image\ImageService::getFullpath($image) }}">
                        <img src="{{ img_src($image, 'preview') }}" />
                        {{ $image->original_name }}
                        </a>
                    </p>
                @endforeach
            </td>
        </tr>
        @endif
    </tbody>
</table>
<table class="table table-striped table-bordered table-hover">
    <tbody>
        <tr>
            <th style="width: 130px">Имя</th>
            <td>{{ $feedback->name }}</td>
        </tr>
        <tr>
            <th>Телефон</th>
            <td>{{ $feedback->phone }}</td>
        </tr>
        <tr>
            <th>Какая кухня Вам нужна</th>
            <td>{{ $feedback->message }}</td>
        </tr>
        <tr>
            <th>Площадь кухни, м<sup>2</sup></th>
            <td>{{ $feedback->additional_1 }}</td>
        </tr>
        <tr>
            <th>Расположение</th>
            <td>{{ $feedback->additional_2 }}</td>
        </tr>
        <tr>
            <th>Стиль</th>
            <td>{{ $feedback->additional_3 }}</td>
        </tr>
        @if($feedback->multiupload && $feedback->multiupload->images->count())
        <tr>
            <th>Прикреплено</th>
            <td>
                @foreach($feedback->multiupload->images as $image)
                    <p>
                        <a class="fancybox" rel="gallery" href="/{{ \App\Services\Image\ImageService::getFullpath($image) }}">
                        <img src="{{ img_src($image, 'preview') }}" />
                        {{ $image->original_name }}
                        </a>
                    </p>
                @endforeach
            </td>
        </tr>
        @endif
    </tbody>
</table>
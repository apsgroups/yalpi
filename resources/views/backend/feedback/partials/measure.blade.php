<table class="table table-striped table-bordered table-hover">
    <tbody>
        <tr>
            <th style="width: 130px">Имя</th>
            <td>{{ $feedback->name }}</td>
        </tr>
        <tr>
            <th>Город</th>
            <td>{{ $feedback->city }}</td>
        </tr>
        <tr>
            <th>Телефон</th>
            <td>{{ $feedback->phone }}</td>
        </tr>
        <tr>
            <th>E-mail</th>
            <td>{{ $feedback->email }}</td>
        </tr>
        <tr>
            <th>Подписка</th>
            <td>{{ ($feedback->subscribe ? 'Да' : 'Нет') }}</td>
        </tr>
    </tbody>
</table>
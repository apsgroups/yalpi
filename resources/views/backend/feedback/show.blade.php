@extends ('backend.layouts.master')

@section ('title', 'Просмотр сообщения #'.$feedback->id)

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">{{ trans('feedback.'.$feedback->feedback.'.title') }} #{{ $feedback->id }}</h3>
            <small>
                [{{ $feedback->created_at->format(config('backend.date.format')) }}]
            </small>
            <div class="pull-right">
                {!! link_to_route('admin.feedback.index', 'Закрыть', [], ['class' => 'btn btn-danger btn-sm']) !!}
            </div>
            <div class="clearfix"></div>
        </div>

        <div class="box-body">

            <div class="table-responsive">
                @php($layout = view()->exists('backend.feedback.partials.'.$feedback->feedback) ? $feedback->feedback : 'default')

                @include('backend.feedback.partials.'.$layout)
            </div>
        </div>
    </div>
@stop
@extends ('backend.layouts.master')

@section ('title', 'Изменение атрибута: '.$attribute->title)

@section('page-header')
    <h1>
        Изменение атрибута {{ $attribute->title }}
    </h1>
@endsection

@section('content')
    @include('backend.attributes.partials.form')
@stop
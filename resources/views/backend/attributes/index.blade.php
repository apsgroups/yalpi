@extends('backend.layouts.master')

@section('title', 'Атрибуты')

@section('page-header')
    <h1>
        Управление атрибутами товаров
    </h1>
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Список атрибутов</h3>

            <div class="box-tools pull-right">
                @include('backend.attributes.partials.header-buttons')
            </div>
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Наименование</th>
                        <th>Псевдоним</th>
                        <th class="text-center">Создано</th>
                        <th class="text-center">Обновлено</th>
                        <td class="text-center">Удалить</td>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach ($attributes as $attribute)
                            <tr>
                                <td>{{ $attribute->id }}</td>
                                <td>{!! link_to_route('admin.attribute.edit', $attribute->title, [$attribute->id]) !!}</td>
                                <td>{{ $attribute->slug }}</td>
                                <td class="text-center">{{ $attribute->created_at->format(config('backend.date.format')) }}</td>
                                <td class="text-center">{{ $attribute->updated_at->format(config('backend.date.format')) }}</td>
                                <td class="text-center">
                                    @include('backend.includes.partials.destroy', ['link' => route('admin.attribute.destroy', $attribute->id)])
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

            @include('backend.includes.partials.pagination', ['items' => $attributes])
        </div><!-- /.box-body -->
    </div><!--box-->
@stop
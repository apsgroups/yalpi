@extends('backend.layouts.master')

@section('title', 'Добавление атрибута')

@section('page-header')
    <h1>
        Добавление атрибута
    </h1>
@endsection

@section('content')
    @include('backend.attributes.partials.form')
@stop
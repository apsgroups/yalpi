@extends('backend.layouts.master')

@section('title', 'Добавление мета-шаблона')

@section('page-header')
    <h1>
        Добавление мета-шаблона
    </h1>
@endsection

@section('content')
    @include('backend.meta.partials.form')
@stop
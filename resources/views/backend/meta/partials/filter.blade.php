{!! Form::open(['route' => 'admin.meta.index', 'class' => 'form-inline', 'role' => 'form ', 'method' => 'get']) !!}
    <div>
        <div class="input-group input-group-sm">
            {!! Form::label('search', 'Найти') !!}
            <div>
            {!! Form::text('filter[search]', @$filter['search'], ['class' => 'form-control', 'placeholder' => 'Найти']) !!}
            <span class="input-group-btn">
              <button type="submit" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
            </span>
            </div>
        </div>

        <div class="input-group input-group-sm">
            {!! Form::select('filter[category_id]', $categories, @$filter['category_id'], ['class' => 'chosen-select form-control']) !!}
        </div>

        <div class="input-group input-group-sm">
            {!! Form::select('filter[type]', ['' => 'Все', 'meta_category_id' => 'Категория', 'meta_filter_id' => 'Фильтр', 'meta_product_id' => 'Товары'], @$filter['type'], ['class' => 'form-control']) !!}
        </div>

        <div class="input-group input-group-sm">
            <br />
            <button type="submit" class="btn btn-success"><i class="fa fa-search"></i></button>

            @if($filter)
            &nbsp;{!! link_to_route('admin.meta.index', 'Сбросить', null, ['class' => 'btn btn-danger']) !!}
            @endif
        </div>
    </div>

    <div class="clearfix"></div>
{!! Form::close() !!}
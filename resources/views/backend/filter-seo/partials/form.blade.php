@if(!empty($meta))
{!! Form::model($meta, ['route' => ['admin.meta.update', $meta->id], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'put']) !!}
@else
{!! Form::open(['route' => 'admin.meta.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) !!}
@endif

    @include('backend.includes.partials.save-buttons', ['cancel' => route('admin.meta.index')])

    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Основное</h3>
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="form-group">
                {!! Form::label('title', 'Наименование', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-10">
                    {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'наименование']) !!}
		            {!! $errors->first('title', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-2 control-label"></label>
                <div class="col-lg-5">
                    <a class="btn btn-primary"
                        role="button"
                        data-toggle="collapse"
                        href="#category-vars"
                        aria-expanded="false"
                        aria-controls="collapseExample">Переменные для категории <i class="glyphicon glyphicon-chevron-down"></i></a>
                    <div class="collapse" id="category-vars">
                      <div class="well">
                            <ul>
                                <li>*title* - наименование</li>
                                <li>*title|lower* - наименование с маленькой буквы</li>
                            </ul>
                      </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-2 control-label"></label>
                <div class="col-lg-5">
                    <a class="btn btn-primary"
                        role="button"
                        data-toggle="collapse"
                        href="#product-vars"
                        aria-expanded="false"
                        aria-controls="collapseExample">Переменные для товара <i class="glyphicon glyphicon-chevron-down"></i></a>
                    <div class="collapse" id="product-vars">
                      <div class="well">
                            <ul>
                                <li>*title* - наименование</li>
                                <li>*title|lower* - наименование с маленькой буквы</li>
                                <li>*price* - цена</li>
                                <li>*category* - наименование категории</li>
                                <li>*category|lower* - наименование категории с маленькой буквы</li>
                                <li>*type* - тип товара</li>
                                <li>*type|lower* - тип товара с маленькой буквы</li>

                                @foreach(\App\Models\Property::all() as $property)
                                <li>*{{ $property->slug }}* - {{ $property->title }}</li>
                                @continue($property->type == 'double')
                                <li>*{{ $property->slug }}|lower* - {{ $property->title }} с маленькой буквы</li>
                                @endforeach
                            </ul>
                      </div>
                    </div>
                </div>
            </div>


            <div class="form-group">
                {!! Form::label('meta_title', 'Meta Title', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-10">
                    {!! Form::textarea('meta_title', null, ['class' => 'form-control', 'placeholder' => 'meta title']) !!}
		            {!! $errors->first('meta_title', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>

            <div class="form-group">
                {!! Form::label('meta_description', 'Meta description', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-10">
                    {!! Form::textarea('meta_description', null, ['class' => 'form-control', 'placeholder' => 'meta description']) !!}
		            {!! $errors->first('meta_description', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>

            <div class="form-group">
                {!! Form::label('meta_keywords', 'Meta Keywords', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-10">
                    {!! Form::textarea('meta_keywords', null, ['class' => 'form-control', 'placeholder' => 'meta keywords']) !!}
		            {!! $errors->first('meta_keywords', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-2 control-label"></label>
                <div class="col-lg-5">
                    <a class="btn btn-primary"
                        role="button"
                        data-toggle="collapse"
                        href="#use-in-categories"
                        aria-expanded="false"
                        aria-controls="collapseExample">Применять к категориям <i class="glyphicon glyphicon-chevron-down"></i></a>
                    <div class="collapse" id="use-in-categories">
                      <div class="well">
                            @foreach($categories as $k => $v)
                                @continue(empty($k))

                                <label class="checkbox">
                                    {!! Form::checkbox('categories[]', $k, in_array($k, $useInCategories), ['class' => 'icheck']) !!}
                                    {{ $v }}
                                </label>
                            @endforeach
                            {!! $errors->first('categories', '<div class="text-danger">:message</div>') !!}
                      </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-2 control-label"></label>
                <div class="col-lg-5">
                    <a class="btn btn-primary"
                        role="button"
                        data-toggle="collapse"
                        href="#use-in-product"
                        aria-expanded="false"
                        aria-controls="collapseExample">Применять к товарам <i class="glyphicon glyphicon-chevron-down"></i></a>
                    <div class="collapse" id="use-in-product">
                      <div class="well">
                            @foreach($categories as $k => $v)
                                @continue(empty($k))

                                <label class="checkbox">
                                    {!! Form::checkbox('products[]', $k, in_array($k, $useInProducts), ['class' => 'icheck']) !!}
                                    {{ $v }}
                                </label>
                            @endforeach
                            {!! $errors->first('products', '<div class="text-danger">:message</div>') !!}
                      </div>
                    </div>
                </div>
            </div>
        </div>


    </div><!--box-->

    @include('backend.includes.partials.save-buttons', ['cancel' => route('admin.meta.index')])

    {!! Form::hidden('id') !!}
{!! Form::close() !!}
@extends ('backend.layouts.master')

@section ('title', 'Изменение мета-шаблона: '.$meta->title)

@section('page-header')
    <h1>
        Изменение мета-шаблона {{ $meta->title }}
    </h1>
@endsection

@section('content')
    @include('backend.meta.partials.form')
@stop
@extends('backend.layouts.master')

@section('title', 'Мета теги')

@section('page-header')
    <h1>
        filter-seo настройка фильтра
    </h1>
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Список</h3>

            <div class="box-tools pull-right">
                @include('backend.filter-seo.partials.header-buttons')
            </div>
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Наименование</th>
                        <th class="text-center">Создано</th>
                        <th class="text-center">Обновлено</th>
                        <th class="text-center">Удалить</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach ($items as $item)
                            <tr>
                                <td>{!! $item->id !!}</td>
                                <td>{!! link_to_route('admin.filter-seo.edit', $item->title, [$item->id]) !!}</td>
                                <td class="text-center">{!! $item->created_at !!}</td>
                                <td class="text-center">{!! $item->updated_at !!}</td>
                                <td class="text-center">
                                    @include('backend.includes.partials.destroy', ['link' => route('admin.filter-seo.destroy', $item->id)])
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

            <div class="pull-left">
                {!! $items->total() !!}
            </div>

            <div class="pull-right">
                {!! $items->render() !!}
            </div>

            <div class="clearfix"></div>
        </div><!-- /.box-body -->
    </div><!--box-->
@stop
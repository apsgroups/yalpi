@extends('backend.layouts.master')

@section('title', 'Меню')

@section('page-header')
    <h1>
        Управление меню
    </h1>
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Список меню</h3>

            <div class="box-tools pull-right">
                @include('backend.menu.partials.header-buttons')
            </div>
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Наименование</th>
                        <th>Псевдоним</th>
                        <th class="text-center">Опубликовано</th>
                        <th class="text-center">Создано</th>
                        <th class="text-center">Обновлено</th>
                        <th class="text-center">Удалить</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach ($menus as $menu)
                            <tr>
                                <td>{!! $menu->id !!}</td>
                                <td>{!! link_to_route('admin.menu.edit', $menu->title, [$menu->id]) !!}</td>
                                <td>{!! $menu->slug !!}</td>
                                <td class="text-center">
                                    @include('backend.includes.partials.toggle', ['model' => $menu, 'toggle' => 'published'])
                                </td>
                                <td class="text-center">{!! $menu->created_at->format(config('backend.date.format')) !!}</td>
                                <td class="text-center">{!! $menu->updated_at->format(config('backend.date.format')) !!}</td>
                                <td class="text-center">
                                    @include('backend.includes.partials.destroy', ['link' => route('admin.menu.destroy', $menu->id)])
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

            @include('backend.includes.partials.pagination', ['items' => $menus])
        </div><!-- /.box-body -->
    </div><!--box-->
@stop
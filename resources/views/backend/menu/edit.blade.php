@extends ('backend.layouts.master')

@section ('title', 'Меню '.$menu->title)

@section('page-header')
    <h1>
        Изменение меню {{ $menu->title }}
    </h1>
@endsection

@section('content')
    @include('backend.menu.partials.form')
@stop
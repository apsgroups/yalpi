@if(!empty($menu))
{!! Form::model($menu, ['route' => ['admin.menu.update', $menu->id], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'put']) !!}
@else
{!! Form::open(['route' => 'admin.menu.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) !!}
@endif

    @include('backend.menu.partials.buttons')

    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Основное</h3>
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="form-group">
                {!! Form::label('title', 'Наименование', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-10">
                    {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'наименование']) !!}
		            {!! $errors->first('title', '<div class="text-danger">:message</div>') !!}
                </div>
            </div><!--form control-->

            <div class="form-group">
                {!! Form::label('slug', 'Псевдоним', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-10">
                    {!! Form::text('slug', null, ['class' => 'form-control', 'placeholder' => 'введите псевдоним для URL латинскими буквами, - вместо пробела']) !!}
		            {!! $errors->first('slug', '<div class="text-danger">:message</div>') !!}
                </div>
            </div><!--form control-->

            <div class="form-group">
                <label class="col-lg-2 control-label">Опубликовано</label>
                <div class="col-lg-1">
                    {!! Form::hidden('published', 0) !!}
                    {!! Form::checkbox('published', 1, (isset($menu) && $menu->published === 1)) !!}
		            {!! $errors->first('published', '<div class="text-danger">:message</div>') !!}
                </div>
            </div><!--form control-->

            <div class="form-group">
                {!! Form::label('description', 'Описание', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-10">
                    {!! Form::textarea('description', null, ['class' => 'form-control', 'placeholder' => 'описание']) !!}
		            {!! $errors->first('description', '<div class="text-danger">:message</div>') !!}
                </div>
            </div><!--form control-->

        </div><!-- /.box-body -->
    </div><!--box-->

    @include('backend.menu.partials.buttons')

    {!! Form::hidden('id') !!}
{!! Form::close() !!}
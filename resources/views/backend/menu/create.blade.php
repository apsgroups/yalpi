@extends('backend.layouts.master')

@section('title', 'Добавление меню')

@section('page-header')
    <h1>
        Добавление vty.
    </h1>
@endsection

@section('content')
    @include('backend.menu.partials.form')
@stop
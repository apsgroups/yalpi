@extends ('backend.layouts.master')

@section ('title', 'Изменение шаблона сортировки: '.$item->title)

@section('page-header')
    <h1>
        Изменение шаблона {{ $item->title }}
    </h1>
@endsection

@section('content')
    @include('backend.sort-templates.partials.form')
@stop
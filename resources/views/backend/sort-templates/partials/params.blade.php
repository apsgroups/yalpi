<div class="form-group repeatable-row">
    <div class="col-lg-1">
        <span class="handle"><i class="fa fa-bars"></i></span>
    </div>

    <div class="col-lg-3">
            {!! Form::select('params['.$i.'][category_id]', $categories, (isset($param['category_id']) ? $param['category_id'] : null), [
                'class' => 'wide-select chosen-select form-control',
                'autocomplete' => 'off',
                'placeholder' => 'выберите категорию',
                'style' => 'width: 300px',
                'data-placeholder' => 'Выберите категорию',
                ]) !!}
        </div>

        <div class="col-lg-1">
            {!! Form::text('params['.$i.'][items]', old('params.'.$i.'.items', (isset($param['items']) ? $param['items'] : null)), [
                'class' => 'form-control input-sm',
                'placeholder' => 'Лимит товаров',
                ]) !!}
        </div>

        <div class="col-lg-1">
            {!! Form::text('params['.$i.'][ordering]', old('params.'.$i.'.ordering', (isset($param['ordering']) ? $param['ordering'] : null)), [
                'class' => 'form-control input-sm order-field',
                'placeholder' => 'Порядок',
                ]) !!}
        </div>

      <div class="col-lg-2">
          {!! Form::button('<i class="glyphicon glyphicon-remove"></i>', ['class' => 'remove-row btn btn-danger btn-sm']) !!}
          {!! Form::button('<i class="glyphicon glyphicon-plus"></i>', ['class' => 'add btn btn-success btn-sm']) !!}
      </div>
</div>
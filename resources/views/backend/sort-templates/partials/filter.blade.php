{!! Form::open(['route' => 'admin.sort-template.assign', 'class' => 'form-inline', 'role' => 'form ', 'method' => 'post']) !!}
    <div>
        <div class="input-group input-group-sm">
            {!! Form::select('sort_template_id', $sortTemplates, null, ['class' => 'chosen-select form-control']) !!}
        </div>

        <div class="input-group input-group-sm">
            {!! Form::select('category_id[]', $categories, null, ['class' => 'chosen-select form-control', 'multiple' => 'multiple']) !!}
        </div>

        <div class="input-group input-group-sm">
            <button type="submit" class="btn btn-success">Применить</button>
        </div>
    </div>

    <div class="clearfix"></div>
{!! Form::close() !!}
@if(!empty($item))
{!! Form::model($item, ['route' => ['admin.sort-template.update', $item->id], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'put']) !!}
@else
{!! Form::open(['route' => 'admin.sort-template.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) !!}
@endif

    @include('backend.includes.partials.save-buttons', ['cancel' => route('admin.sort-template.index')])

    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Основное</h3>
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="form-group">
                {!! Form::label('title', 'Наименование', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-10">
                    {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'наименование']) !!}
		            {!! $errors->first('title', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>

            <div class="repeatable property-list">
                <div class="form-group">
                  <div class="col-lg-4">
                    {!! Form::button('<i class="glyphicon glyphicon-plus"></i>', ['class' => 'add last btn btn-success input-sm']) !!}
                    </div>
                </div>

                @if(isset($params))
                    @foreach($params as $k => $param)
                        @include('backend.sort-templates.partials.params', ['i' => $k, 'param' => $param])
                    @endforeach
                @else
                    @include('backend.sort-templates.partials.params', ['i' => 0])
                @endif

            </div>
        </div><!-- /.box-body -->


    </div><!--box-->

    @include('backend.includes.partials.save-buttons', ['cancel' => route('admin.sort-template.index')])

    {!! Form::hidden('id') !!}
{!! Form::close() !!}
@extends('backend.layouts.master')

@section('title', 'Добавление шаблона сортировки')

@section('page-header')
    <h1>
        Добавление шаблона
    </h1>
@endsection

@section('content')
    @include('backend.sort-templates.partials.form')
@stop
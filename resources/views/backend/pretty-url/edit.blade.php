@extends ('backend.layouts.master')

@section ('title', 'Изменение URL фильтра: '.$item->pretty_url)

@section('page-header')
    <h1>
        Изменение URL фильтра {{ $item->pretty_url }}
    </h1>
@endsection

@section('content')
    @include('backend.pretty-url.partials.form')
@stop
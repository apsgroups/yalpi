@extends('backend.layouts.master')

@section('title', 'Добавление URL для фильтра')

@section('page-header')
    <h1>
        Добавление URL для фильтра
    </h1>
@endsection

@section('content')
    @include('backend.pretty-url.partials.form')
@stop
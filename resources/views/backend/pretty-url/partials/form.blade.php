@if(!empty($item))
{!! Form::model($item, ['route' => ['admin.pretty-url.update', $item->id], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'put']) !!}
@else
{!! Form::open(['route' => 'admin.pretty-url.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) !!}
@endif

    @include('backend.includes.partials.save-buttons', ['cancel' => route('admin.pretty-url.index')])

    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Основное</h3>
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="form-group">
                {!! Form::label('pretty_url', 'URL', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-10">
                    {!! Form::text('pretty_url', null, ['class' => 'form-control', 'placeholder' => '/some/foo/bar/']) !!}
		            {!! $errors->first('pretty_url', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-2 control-label">Категория</label>
                <div class="col-lg-5">
                    {!! Form::select('category_id', $categories, null, [
                            'data-placeholder' => 'Выберите категорию',
                            'class' => 'wide-select chosen-select form-control'
                        ]) !!}
                    {!! $errors->first('category_id', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-2 control-label">Опции фильтра</label>
                <div class="col-lg-5">
                    {!! Form::select('options[]', $options, $activeOptions, [
                            'data-placeholder' => 'Выберите опции',
                            'class' => 'select-items',
                            'multiple' => 'multiple',
                        ]) !!}
                    {!! $errors->first('options', '<div class="text-danger">:message</div>') !!}
                </div>
            </div><!--form control-->

            <div class="form-group">
                {!! Form::label('page_title', 'Title', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-10">
                    {!! Form::textarea('page_title', null, ['rows' => 2, 'class' => 'form-control', 'placeholder' => 'Title тег']) !!}
		            {!! $errors->first('page_title', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>

            <div class="form-group">
                {!! Form::label('metadesc', 'Meta Description', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-10">
                    {!! Form::textarea('metadesc', null, ['rows' => 2, 'class' => 'form-control', 'placeholder' => 'Meta Description']) !!}
		            {!! $errors->first('metadesc', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>

            <div class="form-group">
                {!! Form::label('metakeys', 'Meta Keywords', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-10">
                    {!! Form::textarea('metakeys', null, ['rows' => 2, 'class' => 'form-control', 'placeholder' => 'Meta Keywords']) !!}
		            {!! $errors->first('metakeys', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>

            <div class="form-group">
                {!! Form::label('heading', 'Заголовок H1', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-10">
                    {!! Form::text('heading', null, ['class' => 'form-control', 'placeholder' => 'H1']) !!}
		            {!! $errors->first('heading', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>

            <div class="form-group">
                {!! Form::label('description', 'Описание', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-10">
                    {!! Form::textarea('description', null, ['id' => 'description']) !!}
		            {!! $errors->first('description', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>
        </div>


    </div><!--box-->

    @include('backend.includes.partials.save-buttons', ['cancel' => route('admin.pretty-url.index')])

    {!! Form::hidden('id') !!}
{!! Form::close() !!}


@section('after-scripts-end')
    {!! Html::script('js/plugin/ckeditor/ckeditor.js') !!}

    <script>
    CKEDITOR.replace('description', {
        filebrowserBrowseUrl: "{{ route('elfinder.ckeditor') }}",
        removeDialogTabs: 'link:upload;image:upload',
        allowedContent: true,
        extraAllowedContent:'p(*)[*]{*};div(*)[*]{*};li(*)[*]{*};ul(*)[*]{*};i(*)[*]{*}'
    });
    </script>
@stop
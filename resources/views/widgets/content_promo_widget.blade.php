<div class="uk-accordion accordion-block uk-margin-large-bottom" data-uk-accordion>
    <div class="uk-accordion-title uppercase">
        {{ $content->title }}

        <span class="accordion-state accordion-open">Развернуть</span>
        <span class="accordion-state accordion-close">Свернуть</span>
    </div>

    <div class="uk-accordion-content uk-block-muted">
        <div>
            {!! $content->intro !!}
        </div>

        <div class="uk-text-center uk-margin-top">
            <a href="{{ content_url($content) }}" class="uk-button uk-button-large uk-button-primary big">
                <span>Подробнее об акции</span>
            </a>
        </div>
    </div>
</div>
<li class="dropdown notifications-menu">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
        <i class="fa fa-bell-o"></i>
        <span class="label label-danger">{{ count($items) }}</span>
    </a>

    <ul class="dropdown-menu">
        <li>
            <div class="slimScrollDiv">
                <ul class="menu">
                  @foreach($items as $item)
                  <li>
                    <a href="{{ $item['link'] }}">
                        <small class="label pull-right bg-red">{{ $item['count'] }}</small>
                        <small>{{ $item['title'] }}</small>
                    </a>
                  </li>
                  @endforeach
                </ul>
            </div>
        </li>
    </ul>
</li>
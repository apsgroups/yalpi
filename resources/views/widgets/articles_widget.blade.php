<div class="block-area-title">Полезные советы</div>

<div class="block-area margin news-widget">
    <div class="uk-clearfix uk-margin-bottom">
        <a href="{{ content_type_url($contentType) }}" class="uk-button accent more-section-btn uk-float-right">Посмотреть все <i class="uk-icon-arrow-circle-right"></i></a>
    </div>

    <div class="uk-grid uk-grid-match" data-uk-grid-match="{target:'.uk-panel'}">
        @foreach($articles as $item)
        <div class="uk-width-1-4">
            <div class="uk-clearfix uk-panel">
                <a href="{{ content_url($item) }}" class="preview">
                    <img src="{{ img_src($item->image, 'homepage') }}" alt="{{ $item->title }}" title="{{ $item->title }}" />
                </a>

                <div class="news-content">
                    <div class="title"><a href="{{ content_url($item) }}" class="link-as-text hover">{{ $item->title }}</a></div>

                    <div class="desc">
                        {!! $item->getIntro(105) !!}
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
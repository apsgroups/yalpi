<div class="news-widget">
    <div class="block-title">Блог и новости Yalpi:</div>

    <div class="uk-grid uk-grid-margin uk-margin-top-remove">
        <div class="uk-width-1-3">
            @php($lead = $news->shift())
            @include('widgets.news.default_item', ['item' => $lead, 'lead' => true, 'num' => 0])
        </div>

        <div class="uk-width-2-3">
            @foreach($news->chunk(2) as $chunk)
                <div class="uk-grid uk-grid-margin uk-margin-top-remove">
                    @foreach($chunk as $k => $item)
                        <div class="uk-width-1-2">
                            @include('widgets.news.default_item', ['item' => $item, 'lead' => false, 'num' => $k+1])
                        </div>
                    @endforeach
                </div>
            @endforeach
        </div>
    </div>

    <div class="see-more">
        <a href="/blog/">Посмотреть все новости</a>
    </div>
</div>
<div class="item {{ (!empty($lead) ? 'lead' : 'secondary num-'.$num) }}">
    <a href="{{ content_url($item) }}" class="preview">
        <img src="{{ img_src($item->image, 'news_widget'.(!empty($lead) ? '_lead' : '')) }}" alt="{{ $item->title }}" title="{{ $item->title }}" />
    </a>

    <div class="desc">
        @if(!empty($lead))
        <!-- <div class="date">Вт, 07 мая, 20:00 MSK</div> -->
        @endif

        <a href="{{ content_url($item) }}" class="title">
            {{ $item->title }}
        </a>

        <div class="author">{{ $item->user->name }}</div>

        <ul class="stat">
            <li class="views">{{ $item->hits }}</li>
            <li class="comments">{{ $item->rate_count }}</li>
            <li class="likes">{{ $item->rate }}</li>
        </ul>
    </div>
</div>
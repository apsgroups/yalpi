<div class="news-widget">
    <div class="block-title">
        Статьи и рекомендации
        <a href="/blog/" class="all">Все</a>
    </div>

    <div class="news-slider owl-carousel owl-theme">
        @foreach($news as $k => $item)
            <div class="item">
                @include('widgets.news.mobile_item', ['item' => $item, 'num' => ($k+1 % 2 === 0 ? 1 : 2)])
            </div>
        @endforeach
    </div>
</div>
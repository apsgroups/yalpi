<div class="categories-grid uk-margin-bottom-remove uk-margin uk-text-center uk-contrast uk-width-medium-1-1">

    <div class="uk-grid" data-uk-grid-margin="">
    @foreach($categories->take(2) as $k => $category)
        <div class="uk-width-1-2">
            @include('frontend.category.partials.category', ['imageName' => 'homepage_large'])
        </div> 
    @endforeach 

    @foreach($categories->slice(2) as $k => $category) 
        <div class="uk-width-1-3">
            @include('frontend.category.partials.category', ['imageName' => 'homepage_medium'])
        </div>
    @endforeach
    </div>
</div>
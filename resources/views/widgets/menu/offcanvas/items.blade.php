@foreach($menu as $menuItem)
	@continue(in_array($menuItem['type'], ['horizontal_divider', 'vertical_divider', 'menu_text']))
	@include('widgets.menu.offcanvas.item', ['item' => $menuItem])
@endforeach
<?php
    $activeChild = false;
 
    if(stripos(Request::path(), trim($item['link'], '/')) === 0) { 
        $activeChild = true;
    }

    ?>

    <li>
    @if($item['type'] === 'header')
        <span class="{!! (!empty($item['children']) ? ' parent' : ' ') !!}" @if(!empty($item['children'])) data-target="#{{ $config['menu_html_id'] }}item{{ $item['id'] }}" data-toggle="collapse" data-parent="#{{ $config['menu_html_id'] }}" aria-expanded="{{ ($activeChild ? 'true' : 'false') }}" @endif >
            {!! $item['title'] !!}
        </span>
    @else
        <a href="{!! $item['link'] !!}" {!! (!empty($item['children']) ? ' class="parent"' : ' ') !!}>
            @if(!$item['parent_id'] && !empty($item['icon']))
            <span class="icon">
              <img src="/uploads/catalog-homepage/{{ $item['icon'] }}.svg" />
            </span>
            @endif

            {!! $item['title'] !!}

            @if(!empty($item['category_product_count']))
                <span class="product-count">{{ $item['category_product_count'].' '.total_products_morph($item['category_product_count']) }}</span>
            @endif
        </a>
    @endif

    @if(!empty($item['children']))
        <div class="toggle"></div>
    @endif

    @if(!empty($item['children']))
        <ul>
            @include('widgets.menu.offcanvas.items', ['menu' => $item['children']])
        </ul>
    @endif
</li>
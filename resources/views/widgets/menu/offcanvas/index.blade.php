<div id="menu-modal" class="uk-offcanvas">
    <div class="uk-offcanvas-bar">
        <nav id="main-menu">
        	<div id="main-menu-header"></div>
        	<a href="#" class="uk-offcanvas-close" id="main-menu-close"></a>

			<ul class="index">
				@include('widgets.menu.offcanvas.items')
			</ul>
		</nav>
    </div>
</div>
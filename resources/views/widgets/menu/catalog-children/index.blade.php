@if($menu)
    <div class="category-children">
        @foreach($menu as $item)
          @continue(empty($item['category']))

          @if(!empty($item['children']))
              @include('widgets.menu.catalog-children.children', ['parent' => $item])
          @endif
        @endforeach
    </div>
@endif
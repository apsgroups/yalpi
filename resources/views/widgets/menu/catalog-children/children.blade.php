@if(!empty($parent['children']))
  @foreach($parent['children'] as $children)
    @if(request()->path() === trim($parent['link'], '/'))
      @continue(empty($children['category']))
      
      <a href="{{ $children['link'] }}">{{ $children['title'] }} @if(!empty($children['category_product_count']))<span class="count">{{ $children['category_product_count'] }}</span>@endif</a>
    @endif

	@if(!empty($children['children']))
		@if(request()->path() === trim($children['link'], '/'))
			@include('widgets.menu.catalog-children.children', ['parent' => $children])
		@endif
	@endif
  @endforeach
@endif
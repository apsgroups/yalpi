<div class="children-items">
	<div>
		<ul>
			@foreach($children['children'] as $item)
			<li>
				<div class="group-title">{{ $item['title'] }}</div>

				@if(!empty($item['children']))
				<ul>
					@foreach($item['children'] as $subitem)
						<li><a href="{{ $subitem['link'] }}">{{ $subitem['title'] }}</a></li>
					@endforeach
				</ul>
				@endif
			</li>
	    	@endforeach
		</ul>
	</div>
</div>

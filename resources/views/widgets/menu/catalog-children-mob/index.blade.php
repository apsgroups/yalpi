@if($menu)
	<?php
	$items = [];

	foreach ($menu as $item) {
		if(empty($item['category'])) {
			continue;
		}

		foreach($item['children'] as $child) {
		    if(!empty($child['children'])) {
    			foreach($child['children'] as $child2) {
    			    if(empty($child2['category']) || request()->path() !== trim($child['link'], '/')) {
    			    	continue;
    			    }
    				
    				$items[] = $child2;
    			}
		    }

		    if(empty($child['category']) || request()->path() !== trim($item['link'], '/')) {
		    	continue;
		    }
			
			$items[] = $child;
		}
	}

	if(!$items) {
		return;
	}
	?>
	<div class="category-children-inner">
		<div>
			@php($items = collect($items))

			@foreach($items->chunk(3) as $chunk)
			    <div class="category-children">
			        @foreach($chunk as $item)
			          <a href="{{ $item['link'] }}">{{ $item['title'] }} @if(!empty($item['category_product_count']))<span class="count">{{ $item['category_product_count'] }}</span>@endif</a>
			        @endforeach
			    </div>
			@endforeach
		</div>
	</div>
@endif
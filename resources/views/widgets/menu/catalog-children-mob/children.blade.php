@if(!empty($parent['children']))
  @foreach($parent['children'] as $children)
    @if(request()->path() === trim($parent['link'], '/'))
      @continue(empty($children['category']))
      
      <a href="{{ $children['link'] }}">{{ $children['title'] }} @if(!empty($children['category_product_count']))<span class="count">{{ $children['category_product_count'] }}</span>@endif</a>
    @endif
  @endforeach
@endif
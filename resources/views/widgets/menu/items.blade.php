@foreach($items as $item)
  <li @if(!empty($item['children'])) class="uk-parent" data-uk-dropdown @endif>
      <a href="{!! $item['link'] !!}">
        {!! $item['title'] !!}
      </a>
      @if(!empty($item['children']))
      <div class="uk-dropdown uk-dropdown-navbar">
        <ul class="uk-nav uk-nav-navbar">
              @include('widgets.menu.items', array('items' => $item['children']))
        </ul>
      </div>
      @endif
  </li>
@endforeach
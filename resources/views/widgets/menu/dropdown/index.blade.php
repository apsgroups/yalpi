<nav id="dropdown-menu">
    <ul class="index">
        @foreach($menu as $item)
        	@continue(in_array($item['type'], ['menu_text']))

            <?php

            $css = [];

            if($item['icon']) {
                $css[] = 'icon '.$item['icon'];
            }

            $css[] = $item['marker'];

            if(!empty($item['children'])) {
                $css[] = 'parent';
                $css[] = 'layout-regex-sidebar-main.parent-end';
            }

            if($item['title'] === 'Комнаты') {
                $css[] = 'menu-header';
            }

            $cssClass = $css ? 'class="'.implode(' ', $css).'"' : '';

            $target = ($item['target'] ? 'target="'.$item['target'].'"' : '');
            $rel = ($item['rel'] ? 'rel="'.$item['rel'].'"' : '');

            $marker = $item['type'] === 'menu_item_active' ? 'active '.$item['item_id'] : $item['link'];

            $toggle = in_array($item['type'], ['header', 'menu_item_active']);
            ?>
            
            <li {!! $cssClass !!}>
                @if($toggle)
                    <span data-href="{!! $item['link'] !!}" {!! ($toggle ? 'class="toggle"' : '') !!} >
                    {!! $item['title'] !!}
                    </span>
                @else
                    @if($item['noindex'])
                    <!--noindex-->
                    @endif

                    <a href="{!! $item['link'] !!}" {!! $target !!} {!! $rel !!}>
	                    @if(!empty($item['icon']))
	                    <span class="icon">
	                      <img src="/uploads/catalog-homepage/{{ $item['icon'] }}.svg" />
	                    </span>
	                    @endif

                        {!! $item['title'] !!}
                    </a>

                    @if($item['noindex'])
                    <!--/noindex-->
                    @endif
                @endif

                @if(!empty($item['children']))
                    @include('widgets.menu.dropdown.children', ['items' => $item['children'], 'parent' => $item])
                @endif
            </li>
        @endforeach
    </ul>
</nav>
<ul>
    <?php 
    $activeIndex = [];
    $activeExist = false;
    ?>

    @foreach($items as $item)
        @if($item['type'] === 'horizontal_divider')
            <li class="divider">&nbsp;</li>
        @endif

        @continue(in_array($item['type'], ['vertical_divider', 'horizontal_divider', 'loadmore']))
            
        <?php
        $css = [];
        
        if(!empty($item['children'])) {
            $css[] = 'parent';
        }

        $target = ($item['target'] ? 'target="'.$item['target'].'"' : '');
        $rel = ($item['rel'] ? 'rel="'.$item['rel'].'"' : '');
        
        $dataParent = '';

        if(!empty($item['children'])) {
            $dataParent = 'data-id="'.$item['id'].'"';
        }

        ?>
        <li {!! 'class="'.implode(' ', $css).'"' !!} {!! $dataParent !!}>
            @if($item['type'] === 'header')
                <span {!! ($item['type'] === 'header' ? 'class="header-item"' : '') !!}>
                    {!! ($item['bold'] ? '<b>'.$item['title'].'</b>' : $item['title']) !!}
                </span>
            @else
                @if($item['noindex'])
                <!--noindex-->
                @endif
                <a href="{!! $item['link'] !!}" {!! $target !!} {!! $rel !!}>
                    {!! ($item['bold'] ? '<b>'.$item['title'].'</b>' : $item['title']) !!}

                    @if(!empty($item['category_product_count']))
                    	<span class="product-count">{{ $item['category_product_count'].' '.total_products_morph($item['category_product_count']) }}</span>
                    @endif
                </a>
                @if($item['noindex'])
                <!--/noindex-->
                @endif
            @endif
        </li>
    @endforeach
</ul>
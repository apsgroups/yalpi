<div class="menu uk-clearfix">
    @foreach(collect($menu)->chunk(4) as $items)
        <ul>
        @foreach($items as $item)
            @if($item['type'] === 'horizontal_divider')
                <li class="social-wrapper">
                    <ul class="social">
                        <li><a href="#" rel="nofollow" class="fb"></a></li>
                        <li><a href="#" rel="nofollow" class="yt"></a></li>
                        <li><a href="#" rel="nofollow" class="vk"></a></li>
                        <li><a href="#" rel="nofollow" class="instagram"></a></li>
                    </ul>
                </li>
            @else
                <li>
                    @if($item['link'] && $item['link'] !== '#')
                        <a href="{!! $item['link'] !!}">{!! $item['title'] !!}</a>
                    @else
                        {!! $item['title'] !!}
                    @endif
                </li>
            @endif
        @endforeach
        </ul>
    @endforeach
</div>
<nav class="{{ $model->slug }}-menu menu">
    <ul>
        @foreach($menu as $item)
          <li class="{!! $item['marker'] !!}">
          	@if($item['disabled'])
          		<span class="{{ $item['icon'] }}">{!! $item['title'] !!}</span>
          	@else
              <a href="{!! $item['link'] !!}" class="{{ $item['icon'] }}"><span>{!! $item['title'] !!}</span></a>
          	@endif
          </li>
        @endforeach
    </ul>
</nav>
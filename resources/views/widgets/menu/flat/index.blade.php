<nav class="{{ $model->slug }}-menu">
    <ul class="uk-navbar-nav">
        @foreach($menu as $item)
          <li class="{!! $item['marker'] !!}">
          	@if($item['disabled'])
          		<span>{!! $item['title'] !!}</span>
          	@else
              <a href="{!! $item['link'] !!}">{!! $item['title'] !!}</a>
          	@endif
          </li>
        @endforeach
    </ul>
</nav>
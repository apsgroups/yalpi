@foreach(array_chunk($menu, 5) as $items)
    <ul class="nav col-xs-6 col-sm-6 col-md-6 col-lg-6">
        @foreach($items as $item)
            <li><a href="{{ $item['link'] }}">{{ $item['title'] }}</a></li>
        @endforeach
    </ul>
@endforeach
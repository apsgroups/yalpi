<div class="header {{ $model['description'] }}">{{ $model['title'] }}</div>

@php($i = 0)

<ul>
@foreach($menu as $item)
    @continue($config['limit'] > 0 && $i >= $config['limit'])

    @php($i++)

    <li>
        <a href="{!! $item['link'] !!}">
            <span class="title {{ ($item['bold'] ? 'bold' : '') }}">{!! $item['title'] !!}</span>

            @if($item['show_product_count'] && !empty($item['category_product_count']))
                <span class="total">{!! $item['category_product_count'].' '.total_products_morph($item['category_product_count']) !!}</span>
            @endif

            @if(!empty($item['label_text']))
                <span class="menu-label label-{{ $item['label_style'] }}">{!! $item['label_text'] !!}</span>
            @endif
        </a>
    </li>
@endforeach
</ul>
<li class="uk-nav-header">
	{{ $item['title'] }}
	@if($item['note'])
	    <span class="note">{{ $item['note'] }}</span>
	@endif
</li>
<div class="accordeon catalog-homepage">
  @foreach($menu as $item)
    @continue($item['type'] === 'menu_text')

    <div class="item icon-{{ $item['icon'] }}">
      <div class="header">
        <a href="{!! $item['link'] !!}">
          @if(!empty($item['icon']))
          <span class="icon">
            <img src="/uploads/catalog-homepage/{{ $item['icon'] }}.svg" />
          </span>
          @endif
          
          <span class="title">
            {!! $item['title'] !!}
          </span>
        </a>

        <span class="toggle"></span>
      </div>
      
      <div class="content">
        @if(!empty($item['children']))
          @php($i = 0)
          <ul>
            @foreach($item['children'] as $child)
              @php($i++)
              <li>
                <a href="{!! $child['link'] !!}">{!! $i.' '.$child['title'] !!}</a>
              </li>
            @endforeach
          </ul>
        @endif

        @if($item['type'] !== 'menu_text')
          <div class="more">
            <a href="{{ $item['link'] }}">Посмотреть все</a>
          </div>
        @endif
      </div>
    </div>
  @endforeach
</div>
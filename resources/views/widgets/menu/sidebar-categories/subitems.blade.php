<div class="sidebar-menu">
	<div class="parent-link">
		<a href="{{ $item['link'] }}">{{ $item['title'] }}</a>
	</div>

	<ul>
		@foreach($item['children'] as $menuItem)
			<li>
			    @if(!empty($menuItem['children']))
			    	<a href="{{ $menuItem['link'] }}">{{ $menuItem['title'] }}</a>
					<span class="toggle"></span>
					
			    	<ul>
			    		@foreach($menuItem['children'] as $child)
			    			@php($active = strpos(request()->path(), trim($child['link'], '/')) !== false)
			    			<li {!! ($active ? 'class="selected"' : '') !!}>
			    				<a href="{{ $child['link'] }}">{{ $child['title'] }}</a>
			    				
			    				@if(!empty($child['category_product_count']))
			    					<span class="count">{{ $child['category_product_count'] }}</span>
			    				@endif
			    			</li>
			    		@endforeach
			    	</ul>
			    @else
			    	<a href="{{ $menuItem['link'] }}">{{ $menuItem['title'] }}</a>
			    @endif
			</li>
		@endforeach
	</ul>
</div>
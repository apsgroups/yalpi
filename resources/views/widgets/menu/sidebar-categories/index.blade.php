@foreach($menu as $menuItem)
	@if(!empty($menuItem['children']))
    	@include('widgets.menu.sidebar-categories.item', ['item' => $menuItem, 'currentLvl' => 1])
    @endif
@endforeach
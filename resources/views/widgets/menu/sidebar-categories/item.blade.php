<?php
$currentLvl++;
?>

@foreach($item['children'] as $menuItem)
    @if($currentLvl >= 3 && strpos(request()->path(), trim($menuItem['link'], '/')) !== false && !empty($menuItem['children']))
        @include('widgets.menu.sidebar-categories.subitems', ['item' => $menuItem])
    @elseif(!empty($menuItem['children']))
        @include('widgets.menu.sidebar-categories.item', ['item' => $menuItem, 'currentLvl' => $currentLvl])
    @endif
@endforeach
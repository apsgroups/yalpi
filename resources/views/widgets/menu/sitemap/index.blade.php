<div class="uk-grid">
    <ul class="uk-list">
    @foreach($menu as $item)
	    <li class="uk-margin-bottom">
	        <div class="uk-text-large uk-text-bold">
	            @if($item['type'] === 'header')
	                {!! $item['title'] !!}
	            @else
	                <a href="{!! $item['link'] !!}">
	                    {!! $item['title'] !!}
	                </a>
	            @endif
	        </div>

	        @if(!empty($item['children']))
	            @include('widgets.menu.sitemap.children', ['items' => $item['children'], 'parent' => $item])
	        @endif
	    </li>
    @endforeach
    </ul>
</div>
<ul>
    @foreach($items as $item)
        @continue(in_array($item['type'], ['horizontal_divider', 'vertical_divider']))

        <li>
            <a href="{!! $item['link'] !!}">
                {!! $item['title'] !!}
            </a>

            @if(!empty($item['children']))
                @include('widgets.menu.sitemap.children', ['items' => $item['children'], 'parent' => $item])
            @endif
        </li>
    @endforeach
</ul>
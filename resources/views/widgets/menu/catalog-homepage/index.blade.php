<div class="catalog-homepage">
  <div class="block-title">Каталог уроков:</div>

  <div class="uk-grid uk-grid-margin" data-uk-grid-match="{target: 'ul'}">
    @foreach($menu as $item)
      <div class="uk-width-1-4">
        <div class="item icon-{{ $item['icon'] }}">

          @if($item['type'] !== 'menu_text')
            @if(!empty($item['icon']))
            <div class="icon">
              <img src="/uploads/catalog-homepage/{{ $item['icon'] }}.svg" />
            </div>
            @endif
            
            <div class="title">
              <a href="{!! $item['link'] !!}">{!! $item['title'] !!}</a>
            </div>
          @else
            {!! $item['description'] !!}
          @endif
          
          @if(!empty($item['children']))
            @php($i = 0)
            <ul>
              @foreach($item['children'] as $child)
                @php($i++)
                <li>
                  <a href="{!! $child['link'] !!}">{!! $i.' '.$child['title'] !!}</a>
                </li>
              @endforeach
            </ul>
          @endif

          @if($item['type'] !== 'menu_text')
            <div class="more">
              <a href="{{ $item['link'] }}">Посмотреть все</a>
            </div>
          @endif
        </div>
      </div>
    @endforeach
  </div>
</div>
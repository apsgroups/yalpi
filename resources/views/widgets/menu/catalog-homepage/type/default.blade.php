<li @if(!empty($item['children'])) class="uk-nav-header" @endif>
	@if($item['noindex'])
	<!-- noindex -->
	@endif

	@if($item['disabled'])
		<span class="menu-item-title {{ ($item['bold'] ? ' uk-text-bold' : '') }}">{{ $item['title'] }}</span>

        @if($item['note'])
            <span class="note">{{ $item['note'] }}</span>
        @endif
	@else
		<a href="{{ $item['link'] }}" {!! ($item['target'] ? 'target="'.$item['target'].'"' : '') !!}
		{!! ($item['rel'] ? 'rel="'.$item['rel'].'"' : '') !!}>

		    @if($item['bold'])
		    <span class="uk-text-bold">{{ $item['title'] }}</span>
		    @else
		    {{ $item['title'] }}
		    @endif

	        @if($item['note'])
	            <span class="note">{{ $item['note'] }}</span>
	        @endif

		    @if(empty($item['children']) && isset($item['total_products']))
		    <span class="total-products">{{ $item['total_products'] }}</span>
		    @endif
		</a>
	@endif

	@if($item['noindex'])
	<!--/ noindex -->
	@endif

	@if($item['description'])
    <span class="description">{!! $item['description'] !!}</span>
    @endif
</li>
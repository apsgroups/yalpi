<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 fastCat dropdown">
    <?php

    $active = 'Категории';

    $html = '';

    foreach($menu as $item) {
        $html .= '<li><a href="'.$item['link'].'" class="'. ($item['active'] ? 'active' : '') .'">'.$item['title'].'</a></li>';

        if($item['active']) {
            $active = $item['title'];
        }
    }
    ?>

    <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
        <span class="txt">
            {{ $active }}
        </span>
        <span class="caret"></span>
    </button>
    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
        {!! $html !!}
    </ul>
</div>
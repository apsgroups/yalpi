<div class="slider owl-carousel owl-theme">
    @foreach($contents as $k => $content)
        <div class="item">
            <div class="text">
                <div class="title">{!! $content->title !!}</div>
                <div class="desc">{!! $content->intro !!}</div>
            </div>

            <div class="img">
            	{!! $content->more !!}
            </div>
        </div>
    @endforeach
</div>
<div class="about">
	<div class="block-title">Yalpi сервис:</div>

	<div class="uk-grid uk-grid-margin uk-text-center">
		@foreach($contents as $item)
			<div class="uk-width-1-3">
				<div class="img">
					<div><img src="{{ img_src($item->image) }}"></div>
				</div>

				<div class="title">{{ $item->title }}</div>
				
				<div class="desc">{{ $item->intro }}</div>

				<div class="more">
					<a href="{{ $item->link() }}">Узнать больше</a>
				</div>
			</div>
		@endforeach
	</div>
</div>
 <div class="uk-width-medium-1-1 slider-box">
     <h3 class="uk-heading-large uppercase">{{ $title }}</h3>

     <div data-uk-slideset="{default: {{ $inRow }} }" itemprop="isRelatedTo" itemscope itemtype="https://schema.org/Service">
         <meta itemprop="serviceType" content="" />
         <meta itemprop="areaServed" content="" />
         <div itemprop="provider" itemscope itemtype="https://schema.org/Organization">
            <meta itemprop="name" content="" />
            <meta itemprop="telephone" content="" />
            <meta itemprop="address" content="" />
        </div>	

        <div class="uk-slidenav-position uk-margin item-slideset">    
            <ul class="uk-grid uk-slideset uk-grid uk-flex-center uk-grid-width-1-{{ $inRow }}" data-uk-grid-match="{target:'.product-desc'}" itemprop="hasOfferCatalog" itemscope itemtype="https://schema.org/OfferCatalog">
                @foreach($products as $k=>$product)
                <li itemprop="itemListElement" itemscope itemtype="https://schema.org/Offer">
                    @include('frontend.product.preview')
                </li>
                @endforeach
            </ul>

            @if(count($products) > $inRow)
            <a href="" class="uk-slidenav uk-slidenav-contrast uk-slidenav-previous" data-uk-slideset-item="previous"></a>
            <a href="" class="uk-slidenav uk-slidenav-contrast uk-slidenav-next" data-uk-slideset-item="next"></a>
            @endif
        </div>
    </div>

    @if(!empty($button))
    <div class="uk-text-center text">
        <a href="{{ $more }}" class="uk-button uk-button-large uk-button-primary uk-margin big"><span>{{ $button }}</span></a>
    </div>
    @endif
 </div>

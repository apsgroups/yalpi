@if($products->count())
    <div class="uk-grid">
        @foreach($products as $k=>$product)
            <div class="uk-width-1-4">
                @include('frontend.product.preview', ['newWindow' => true])
            </div>
        @endforeach
    </div>
@endif
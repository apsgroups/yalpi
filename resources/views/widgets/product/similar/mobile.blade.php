@if($products->count())
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 content-title noMarg">
        <span>Вам может понравиться:</span>
    </div>

    <!--Extra goods-->
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 catProds extraGoods">
        <div class="container">
            <div class="row">
                @foreach($products as $product)
                    <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
                        @include('mobile.product.preview', ['lastRow' => true])
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <!--/Extra goods-->
@endif
<div class="uk-width-medium-1-1 slider-box category-slider popular-categories">
    <div class="block-title">С этим товаром покупают</div>

    <div data-uk-slideset="{default: {{ $inRow }} }">
        <div class="uk-slidenav-position uk-margin item-slideset">
            <ul class="uk-grid uk-slideset uk-grid uk-flex-center uk-grid-width-1-{{ $inRow }}" data-uk-grid-match="{target:'.product-desc'}">
                @foreach($categories as $category)
                    <li class="item">
                        <a href="{{ $category->link() }}" target="_blank">
                            <?php $img = ($category->altImage) ? $category->altImage : $category->image; ?>
                            <img src="{{ img_src($img, 'popular') }}" alt="{{ $category->title }}" />

                            <span>{{ $category->title }}</span>
                        </a>
                    </li>
                @endforeach
            </ul>

            @if(count($categories) > $inRow)
                <a href="" class="uk-slidenav uk-slidenav-contrast uk-slidenav-previous" data-uk-slideset-item="previous"></a>
                <a href="" class="uk-slidenav uk-slidenav-contrast uk-slidenav-next" data-uk-slideset-item="next"></a>
            @endif
        </div>
    </div>
</div>

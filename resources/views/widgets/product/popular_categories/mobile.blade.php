<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 content-title noMarg">
    <span>С этим товаром покупают:</span>
</div>

<!--Extra goods-->
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 catProds extraGoods extraGoods2">
    <div class="container">
        @foreach($categories as $category)
            <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
                <div class="product">
                    <a href="{{ $category->link() }}">
                        <div class="img">
                            <?php $img = ($category->altImage) ? $category->altImage : $category->image; ?>
                            <img src="{{ img_src($img, 'popular') }}" class="img-responsive" alt="{{ $category->title }}">
                        </div>
                        <div class="title">
                            <span>{{ $category->title }}</span>
                        </div>
                    </a>
                </div>
            </div>
        @endforeach

    </div>
</div>
<!--/Extra goods-->
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 content-title noMarg">
    <span>Специальные предложения</span>
</div>

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 specOffer">
    <div id="specOffer" class="carousel slide" data-ride="carousel" data-interval="false">
        <div class="carousel-inner" role="listbox">
            @php($slide = 0)

            @foreach($products as $categoryId => $items)
                @continue(! $items->count())

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 item {{ $slide === 0 ? 'active' : '' }}">
                    @foreach($items as $k=>$product)
                        <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3 product">
                            @include('widgets.product.promo.mobile._product')
                        </div>
                    @endforeach
                </div>

                @php($slide++)
            @endforeach

        </div>

        <a class="left carousel-control" href="#specOffer" role="button" data-slide="prev">
            <span class="left" aria-hidden="true"><i class="fa fa-chevron-left" aria-hidden="true"></i></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#specOffer" role="button" data-slide="next">
            <span class="right" aria-hidden="true"><i class="fa fa-chevron-right" aria-hidden="true"></i></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>

<a href="{{ $product->link() }}">
    <div class="img">
        <img @src($product->image, 'preview_mobile') title="{{ $product->shortTitle }}" alt="{{ $product->shortTitle }}" class="img-responsive">

        @if($product->novelty)
            <span class="tag">new</span>
        @endif

        @if($product->virtual_discount > 0)
            <span class="saleTag">-{{ $product->virtual_discount }}%</span>
        @elseif($product->discount > 0 && $product->discount_from <= 1)
            <span class="saleTag">-{{ $product->discount }}%</span>
        @endif
    </div>
    <div class="title">
        <span>{{ $product->shortTitle }}</span>
    </div>

    @if(!empty($product->body_color_title))
        <div class="subtitle">
            <span>{{ $product->body_color_title }}</span>
        </div>
    @endif

    @if(!empty($product->color_title))
        <div class="subtitle">
            <span>{{ $product->color_title }}</span>
        </div>
    @endif

    <div class="price">
        @if($product->price_old > 0)
            <span class="oldPrice">{{ format_price($product->price_old) }} р.</span>

            @if($product->price > 0)
                <span class="sale">{{ format_price($product->price) }} р.</span>
            @endif
        @endif
    </div>
</a>
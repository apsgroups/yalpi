<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 content-title noMarg">
    <span>Специальные предложения</span>
</div>

<!--special offer-->
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 specOffer">
    <div id="specOffer" class="carousel slide" data-ride="carousel" data-interval="false">
        <div class="carousel-inner" role="listbox">
            @php($slide = 0)

            @foreach($products as $categoryId => $items)
                @continue($slide > 0 || !$items->count())

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 item{{ $slide === 0 ? ' active' : '' }}">
                @foreach($items as $k=>$product)
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 product">
                        @include('widgets.product.promo.mobile._product')
                    </div>
                @endforeach
                </div>

                @php($slide++)
        @endforeach
        </div>

    </div>

    <div class="wrap">
        <a href="/rasprodazhi/" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 bigBut ico yellow">
            Посмотреть все <i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
        </a>
    </div>
</div>
<!--/special offer-->
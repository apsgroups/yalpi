<div class="block-area-title">
    Спец. предложения
</div>

<div class="block-area margin content-switcher" itemscope itemtype="https://schema.org/Service">
    <a href="{{ $saleCategory->link() }}" class="more-section-btn uk-button accent uk-float-right">Посмотреть все <i class="uk-icon-arrow-circle-right"></i></a>
        
    <meta itemprop="serviceType" content="" />
    <meta itemprop="areaServed" content="" />
    <div itemprop="provider" itemscope itemtype="https://schema.org/Organization">
        <meta itemprop="name" content="" />
        <meta itemprop="telephone" content="" />
        <meta itemprop="address" content="" />
    </div>

    <ul data-uk-switcher="{connect:'#promo-widget', animation: 'fade'}" class="uk-subnav uk-subnav-pill" id="promo-nav">
        @foreach($categories as $category)
            @continue(! $products[$category->id]->count())
            <li><a href="" data-category="{{ $category->id }}">{{ $category->title }}</a></li>
        @endforeach
    </ul>

    <ul id="promo-widget" class="uk-switcher" itemprop="hasOfferCatalog" itemscope itemtype="https://schema.org/OfferCatalog">
        @php($display = 1)

        @foreach($products as $categoryId => $items)
            @continue(! $items->count())
            <li id="promo-{{ $categoryId }}">
                @if($display)
                <div class="uk-grid" data-uk-grid-margin="">
                    @foreach($items as $k=>$product)
                        <div class="uk-width-1-{{ $inRow }}" itemprop="itemListElement" itemscope itemtype="https://schema.org/Offer">
                            @include('frontend.product.preview', ['signLine' => true])
                        </div>
                    @endforeach
                </div>
                @endif
            </li>

            @php($display = 0)
        @endforeach
    </ul>
</div>
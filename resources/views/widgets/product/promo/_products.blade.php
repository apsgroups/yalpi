<div class="uk-grid" data-uk-grid-margin="">
    @foreach($products as $k=>$product)
        <div class="uk-width-1-4" itemprop="itemListElement" itemscope itemtype="https://schema.org/Offer">
            @include('frontend.product.preview', ['signLine' => true])
        </div>
    @endforeach
</div>
<nav class="uk-navbar {{ $model->slug }}">
    <ul class="uk-navbar-nav">
        @include('widgets.menu.items', array('items' => $menu))
    </ul>
</nav>
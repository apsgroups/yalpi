<div class="uk-position-relative uk-nbfc">
	<figure class="uk-overlay category-link animation-scale">
	    <img src="{{ img_src($category->image, $size) }}" alt="{{ $category->title }}">

	    <div class="uk-overlay-panel">	    	
		    <span class="desc-box">
		        <span class="title">{{ $category->title }}</span>

		        @if(isset($price))
		            <span class="price-from">от {!! format_price($prices[$category->id], true) !!}</span>
		        @endif
		    </span>
	    </div>

	    <a class="uk-position-cover" href="{{ $category->link() }}"></a>
	</figure>
</div>
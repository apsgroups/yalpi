<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 content-title noMarg">
    <span>Каталог товаров</span>
</div>

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 homeCategories fullSet bigMarg">
    <div class="inner">
        @if($categories->get('1'))
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 cat cat1">
                @include('widgets.catalog.mobile._desc', ['size' => 'catalog-1', 'price' => true, 'category' => $categories->get('1')->first()])
            </div>
        @endif

        @if($categories->get('2'))
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 cat padLeft">
                @include('widgets.catalog.mobile._desc', ['size' => 'catalog-2', 'price' => true, 'category' => $categories->get('2')->shift()])
            </div>

                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 cat padRight">
                @include('widgets.catalog.mobile._desc', ['size' => 'catalog-2', 'price' => true, 'category' => $categories->get('2')->shift()])
            </div>
        @endif

        @if($categories->get('3'))
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 cat padLeft">
                @include('widgets.catalog.mobile._desc', ['size' => 'catalog-2', 'price' => true, 'category' => $categories->get('3')->shift()])
            </div>

                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 cat padRight">
                @include('widgets.catalog.mobile._desc', ['size' => 'catalog-2', 'price' => true, 'category' => $categories->get('3')->shift()])
            </div>
        @endif
    </div>
</div>
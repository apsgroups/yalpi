<a href="{{ $category->link() }}" class="catA {{ $category->id }}">
    <div class="img">
        @php($img = $category->mobileImage ? $category->mobileImage : $category->image)
        <img @src($img, $size.'_mobile') alt="{{ $category->title }}" class="img-responsive">
    </div>
    <div class="txtBlock">
        <div class="title">{{ $category->title }}</div>

        <div class="subtitle">
            @if($prices[$category->id])
            от {!! format_price($prices[$category->id], true) !!}
            @else
                &nbsp;
            @endif
        </div>
    </div>
    <div class="txtArrow">
        <i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
    </div>
</a>
<div class="category-plates">
    <div class="uk-grid uk-contrast" data-uk-grid-margin="">
        @if($categories->get('1'))
        <div class="uk-width-1-2">
            @include('widgets.catalog._desc', ['size' => 'catalog-1', 'price' => true, 'category' => $categories->get('1')->first()])
        </div>
        @endif

        @if($list = $categories->get('2'))
        <div class="uk-width-1-2">
            <div class="uk-grid">
                @foreach($list as $category)
                <div class="uk-width-1-2">
                    @include('widgets.catalog._desc', ['size' => 'catalog-2', 'price' => true])
                </div>
                @endforeach
            </div>
        </div>
        @endif

        @if($list = $categories->get('3'))
            @foreach($list as $category)
            <div class="uk-width-1-4">
                @include('widgets.catalog._desc', ['size' => 'catalog-3', 'price' => true])
            </div>
            @endforeach
        @endif

        @if($list = $categories->get('4'))
        <div class="uk-width-1-4">
            <div class="uk-grid" data-uk-grid-margin="">
                @foreach($list as $category)
                    <div class="uk-width-1-1">
                        @include('widgets.catalog._desc', ['size' => 'catalog-4'])
                    </div>
                @endforeach
            </div>
        </div>
        @endif
    </div>
</div>
<div id="user-bar" @if(Favorite::count() > 0 || $amount > 0) class="visible" @endif>
	<a href="{{ route('favorite.index') }}" class="{{ (Favorite::count() === 0 ? ' empty' : '') }}">
	    <span class="favorites-total text">{{ Favorite::count() }}</span>
	    <span class="icon icon-menu-favorite"></span>
	</a>
	<a href="{{ route('cart.index') }}" class="{{ ($amount > 0 ? '' : 'empty') }} minicart">
	    <span class="text minicart-amount">{{ $amount }}</span>
	    <span class="icon icon-cart"></span>
	</a>
</div>
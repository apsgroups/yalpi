<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 offcanvas-cart ofc1">
    <a href="{{ route('cart.index') }}">
        <i class="fa fa-shopping-cart" aria-hidden="true"></i>
        <span class="num"><span class="minicart-amount">{{ $amount }}</span></span>
          <span class="sum">
          	<span class="digit minicart-cost">{{ number_format($cost, 0, '', ' ') }}</span>
            <span class="curr">руб.</span>
          </span>
    </a>
</div>

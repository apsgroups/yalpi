<div id="user-bar-fixed">
	<a href="{{ route('favorite.index') }}" class="favorites empty">
	    <span class="favorites-total text"></span>
	    <span class="icon icon-menu-favorite-fixed"></span>
	</a>
	<a href="{{ route('cart.index') }}" class="minicart empty">
	    <span class="text minicart-amount"></span>
	    <span class="icon icon-cart-fixed"></span>
	</a>
</div>

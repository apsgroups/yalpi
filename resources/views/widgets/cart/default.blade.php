<span class="status">
  @if($amount > 0)
    <span>{{ $amount }}</span>
  @else
    0
  @endif
</span>

<i class="uk-icon-shopping-cart"></i>
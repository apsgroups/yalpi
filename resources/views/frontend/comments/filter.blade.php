@if($total)
    <div class="filter-body">
        <a href="#" data-href="{{ route('frontend.comments.index', [$scope, $item->id]) }}" class="show-all active">Все ({{ $total }})</a>

        @for($i = 5; $i > 0; $i--)
            @php($rateTotal = $item->comments()->published(1)->where('parent_id', null)->where('rate', $i)->count())

            @continue(!$rateTotal)

            <a href="#" data-href="{{ route('frontend.comments.index', [$scope, $item->id, 'rate' => $i]) }}" class="star-filter">
                <span>{!! str_repeat('<img src="/images/layout/reviews/star-filter.svg" alt="" />', $i) !!}</span> ({{ $rateTotal }})
            </a>
        @endfor
    </div>
@endif
<div class="comment" data-id="{{ $comment->id }}" id="comment{{ $comment->id }}" itemprop="review" itemscope itemtype="https://schema.org/Review">
    <div class="comment-body">
        @if($comment->user->image)
        <div class="img">
            <img src="{{ img_src($comment->user->image, 'comment') }}" alt="{{ $comment->name }}">
        </div>
        @endif

        <div class="comment-desc">
            @if($comment->rate > 0)
                <div class="rate">{!! str_repeat('<img src="/images/layout/reviews/star.svg" alt="" />', $comment->rate) !!}</div>
            @endif

            <div class="created-date">
                @php($date = new Date($comment->created_at))
                <meta itemprop="datePublished" content="{{ $date->format('Y-m-d') }}">
                {{ $date->format('d F Y H:i') }}
            </div>

            <div class="name" itemprop="author" itemscope itemtype="https://schema.org/Person">
                <span itemprop="name">{{ $comment->name }}</span>
            </div>

            <div class="property comment" itemprop="reviewBody">
                {{ $comment->comment }}
            </div>

            <div class="comment-actions" data-id="{{ $comment->id }}">
                <div class="actions">
                    <div class="status">
                        <span class="likes{{ (isset($userLikes[$comment->id]) && $userLikes[$comment->id]['like'] ? ' active' : '') }}"><span class="value">{{ (int)$comment->likes }}</span></span>
                        <span class="dislikes{{ (isset($userLikes[$comment->id]) && !$userLikes[$comment->id]['like'] ? ' active' : '') }}"><span class="value">{{ (int)$comment->dislikes }}</span></span>
                    </div>

                    <span class="reply-btn">Ответить</span>
                </div>
            </div>
        </div>
    </div>
    
    <?php
    $replies = $comment->replies()->published(1)->orderBy('popularity', 'desc')->orderBy('id', 'desc')->get();

    $userReplyLikes = [];

    if($user = auth()->user()) {            
        $userReplyLikes = \App\Models\CommentLike::ofUser($user->id)->whereIn('comment_id', $replies->pluck('id'))->get()->keyBy('comment_id')->toArray();
    }
    ?>

    @if($replies->count())
        <div class="replies">
            @foreach($replies as $comment)
                @include('frontend.comments.item', compact('comment') + ['userLikes' => $userReplyLikes])
            @endforeach
        </div>
    @endif
</div>
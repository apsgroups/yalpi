@php($scope = strtolower(class_basename($item)))

<div class="comments-box">
    <div class="block-title">Обсуждения:</div>

    @if(Auth::check())
    	@include('frontend.comments.create')
    @else
        <div class="uk-margin-bottom">
            <a href="{{ route('auth.login') }}" class="btn-popup login">Зарегистрируйтесь</a>, чтобы оставить отзыв.
        </div>
    @endif

    @php($total = $comments->total())

    @if($total > 0)
    <div id="comments" data-scope="{{ $scope }}" data-id="{{ $item->id }}" class="shadow-block">
        @if($total > 0 && $item->rate > 0)
            <div class="avg-rate">
                <div class="stars"><span style="width: {{ 27 * $item->rate }}px">{!! str_repeat('<img src="/images/layout/reviews/star.svg" alt="" />', ceil($item->rate)) !!}</span></div>
                <div class="avg">{{ number_format($item->rate, 1, ',', '') }}</div>
            </div>

            @include('frontend.comments.filter', compact('item', 'comments', 'total'))
        @endif

        <div id="comment-items">
            @include('frontend.comments.items')
        </div>
    </div>
    @endif
</div>
{!! Form::open(['route' => ['frontend.comments.store', $scope, $item->id], 'method' => 'post', 'class' => 'comment-form validate-form ajax-form uk-form']) !!}
    <div class="shadow-block">
        @if(!$item->comments()->where('rate', '>', 0)->where('parent_id', null)->where('user_id', auth()->user()->id)->count())
            <div class="uk-form-row">
                <div class="rate-inner">
                    {!! Form::radio('rate', '', false, ['id' => 'comment-rate', 'class' => 'required']) !!}
                    
                    <label for="comment-rate" id="comment-rate-label">Оцените урок по пятибальной шкале *</label>
                    
                    <div id="comment-rating">
                        {!! str_repeat('<span class="star"></span>', 5) !!}
                    </div>
                </div>
            </div>
        @endif
        <div class="uk-form-row">
            <div>
            {!! Form::textarea('comment', null, ['id' => 'comment', 'class' => 'required uk-form-width-large', 'rows' => 1, 'placeholder' => 'Оставьте свой комментарий...']) !!}
            </div>
        </div>
    </div>

    <div>
        <button type="submit" class="uk-button">Отправить</button>
    </div>
{!! Form::close() !!}
<div>
    {!! Form::open(['route' => ['frontend.comments.reply.store', $comment->id], 'method' => 'post', 'class' => 'comment-form reply-form validate-form ajax-form uk-form']) !!}    
        <div class="uk-form-row">
            @if($image = auth()->user()->image)
            <div class="img">
                <img src="{{ img_src($image, 'comment') }}" alt="{{ auth()->user()->name }}">
            </div>
            @endif

            <div>
            {!! Form::textarea('comment', null, ['id' => 'comment', 'class' => 'uk-form-width-large', 'rows' => 2, 'placeholder' => 'Оставить свой комментарий']) !!}
            </div>
        </div>

        <div>
            <button type="submit" class="uk-button">Отправить</button>
        </div>
    {!! Form::close() !!}
</div>
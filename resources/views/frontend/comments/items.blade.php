@foreach($comments as $comment)
    @include('frontend.comments.item', compact('comment'))
@endforeach

@if($comments->hasMorePages())
<div id="comments-pagination">
	<a href="#" data-href="{{ $comments->nextPageUrl() }}">Показать следующие отзывы</a>
</div>
@endif
<{{ '?' }}xml version="1.0" encoding="UTF-8"{{ '?' }}>
<yml_catalog date="{{ \Carbon\Carbon::now()->toDateTimeString() }}">
    <shop>
        <name>{!! \Settings::getOr('market_title', 'title') !!}</name>
        <company>{!! \Settings::getOr('market_company', 'title') !!}</company>
        <url>{{ url('/') }}</url>
        <currencies>
            <currency id="RUR" rate="1" plus="0"/>
        </currencies>
        @include('frontend.market.partials.categories') 
        <delivery-options>
        <option cost="1250" days="2-3"/>
        </delivery-options>
        @include('frontend.market.partials.offers'.$layout)
    </shop>
</yml_catalog>
@php($baseImgUrl = rtrim(url('/'), '/'))
<offers>
    <?php
    $products->chunk(10, function ($offers) use ($baseImgUrl) {
        foreach ($offers as $offer) {
            ?>            
            @continue(!$offer->category->published)

            <?php 
            $available = $offer->market_available;

            if($offer->category->market_available_apply && $offer->market_available_apply) {
                $available = $offer->category->market_available;
            }

            if(! $offer->in_stock) {
                $available = false;
            }
            ?>

            <offer id="{{ $offer->id }}"{!! ($available ? ' available="true"' : ' available="false"') !!}>
                <url>{{ $offer->link(true) }}</url>
                <price>{{ number_format($offer->price, 2, '.', '') }}</price>
                @if($offer->price_old > 0)
                <oldprice>{{ number_format($offer->price_old, 2, '.', '')  }}</oldprice>
                @endif
                <currencyId>RUB</currencyId>
                <categoryId>{{ $offer->category_id }}</categoryId>
                @if(!empty($offer->market_category))
                <market_category>{{ $offer->market_category->title }}</market_category>
                @elseif(!empty($offer->category->market_category))
                <market_category>{{ $offer->category->market_category->title }}</market_category>
                @else
                    @foreach($offer->categories as $productCategory)
                        @continue(empty($productCategory->market_category))
                        <market_category>{{ $productCategory->market_category->title }}</market_category>
                        <?php break; ?>
                    @endforeach
                @endif
                @if($img = img_src($offer->image, 'xlarge'))
                <picture>{{ $baseImgUrl.$img }}</picture>
                @endif
                <store>true</store>
                <pickup>true</pickup>
                <delivery>true</delivery>
                <vendor>Браво</vendor>
                <name>
                @if(!empty($offer->market_title))
                {{ $offer->market_title }}
                @elseif(!empty($offer->category->market_title))
                {{ str_replace([':preview_title', ':title'], [($offer->preview_title ?: $offer->title), $offer->title], $offer->category->market_title) }}
                @else
                    @php($showTitle = true)
                    @foreach($offer->categories as $productCategory)
                        @continue(empty($productCategory->market_title))
                        {{ str_replace([':preview_title', ':title'], [($offer->preview_title ?: $offer->title), $offer->title], $productCategory->market_title) }}
                        @php($showTitle = false)
                        <?php break; ?>
                    @endforeach
                    @if($showTitle)
                    {{ $offer->title }}
                    @endif
                @endif
                </name>
                @if($offer->sku)
                {{-- <vendorCode>{{ $offer->sku }}</vendorCode> --}}
                @endif
                <description>
                    <![CDATA[
                    {{ str_limit(strip_tags($offer->description), 175) }}
                    ]]>
                </description>
                @if(!empty($offer->sales_notes))
                <sales_notes>{{ $offer->sales_notes }}</sales_notes>
                @elseif(!empty($offer->category->sales_notes))
                <sales_notes>{{ $offer->category->sales_notes }}</sales_notes>
                @else
                    @foreach($offer->categories as $productCategory)
                        @continue(empty($productCategory->sales_notes))
                        <sales_notes>{{ $productCategory->sales_notes }}</sales_notes>
                        <?php break; ?>
                    @endforeach
                @endif
                @if($offer->properties)
                    @foreach($offer->properties as $property)
                        <param name="{{ $property->property->title }}">{{ $property->title }}</param>
                    @endforeach
                @endif
            </offer>
            <?php
        }
    });
    ?>
</offers>
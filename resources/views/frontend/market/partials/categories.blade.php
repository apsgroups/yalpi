<categories>
    @foreach($categories as $category)
        <category id="{{ $category->id }}"{!! ($category->parent_id ? ' parentId="'.$category->parent_id.'"' : '') !!}>{{ $category->title }}</category>
    @endforeach
</categories>

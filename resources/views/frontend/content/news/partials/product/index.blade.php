<a href="{{ isset($moduleParent) ? module_url($moduleParent, $product) : product_url($product) }}">
    <img src="{{ img_src($product->image, 'news') }}" data-product-id="{{ $product->id }}" />
</a>

<div class="product-desc">
    <div class="uk-margin-small">
        <a href="{{ product_url($product) }}" class="link-as-text">{{ str_limit($product->title, 35) }}</a>
    </div>

    @include('frontend.content.news.partials.product.price')
</div>
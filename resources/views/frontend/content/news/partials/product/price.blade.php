<div class="product-price uk-margin-small-top">
    @if($product->price_old > 0)
    <span class="price old">{!! format_price($product->price_old, true) !!}</span>
    @endif

    @if($product->price > 0)
    <span class="price {{ ($product->price_old > 0 ? 'new' : '') }}">
        <span>{!! format_price($product->price, true) !!}</span>
    </span>
    @endif

    @if(is_wholesale())
        <div class="uk-float-left uk-margin-left notice-message">Оптовая цена</div>
    @endif
</div>

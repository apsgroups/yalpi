<div class="uk-grid">
    @foreach($contents as $content)
   <div class="uk-width-1-1 uk-margin-large-bottom">
        <div class="uk-clearfix uk-block-muted">
            <div class="uk-float-left uk-margin-right uk-position-relative">
                <a href="{{ content_url($content) }}" class="uk-display-block">
                    <img src="{{ img_src($content->image, 'news') }}" />
                </a>

                <div class="date uk-position-bottom-right">
                    <div class="large">{{ $content->created_at->format('d.m') }}</div>
                    {{ $content->created_at->format('Y') }}
                </div>
            </div>

            <div class="uk-panel-space">
                <h3 class="size-3"><a href="{{ content_url($content) }}" class="link-as-text">{{ $content->title }}</a></h3>

                <div class="uk-text-justify">
                    {!! str_limit($content->intro, 100) !!}
                </div>
            </div>
        </div>
    </div>
    @endforeach
</div>
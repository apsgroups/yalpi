@foreach($contents as $content)
    <div class="uk-clearfix block-area uk-margin-large-bottom uk-padding-remove">
        <div class="uk-float-right uk-margin-left uk-position-relative">
            <a href="{{ content_url($content) }}" class="uk-display-block">
                <img src="{{ img_src($content->image, 'news') }}" />

                <div class="date uk-position-bottom-right">
                    {{ $content->created_at->format('d.m.Y') }}
                </div>
            </a>
        </div>

        <div class="news-item">
            <div class="uk-vertical-align" style="">
                <div class="uk-vertical-align-middle">
                    <h3 class="size-3"><a href="{{ content_url($content) }}" class="link-as-text hover">{{ $content->title }}</a></h3>

                    <div class="uk-text-justify">
                        {!! $content->getIntro() !!}
                    </div>
                </div>
            </div>

            @if($products[$content->id])
            <div class="uk-margin-top uk-margin-bottom">
                <div class="uk-grid" data-uk-grid-margin="">
                    @foreach($products[$content->id] as $product)
                        <div class="uk-width-1-{{ count($products[$content->id]) }} news-product">
                        @include('frontend.content.news.partials.product.index')
                        </div>
                    @endforeach
                </div>
            </div>
            @endif

            <div class="uk-clearfix"></div>
        </div>
    </div>
@endforeach
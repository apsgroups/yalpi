@extends('frontend.layouts.master')

@section('breadcrumbs', Breadcrumbs::render('frontend.content.type', $contentType))

@include('frontend.includes.seometa', ['model' => $contentType])

@section('content')
    <div class="uk-container uk-container-center">
    	<h1 class="uk-text-center uppercase">{{ $contentType->title }}</h1>

		@if(!empty($contents))
		    @include('frontend.content.news.partials.layouts.grid')

            <div class="uk-margin uk-margin-top uk-clearfix">
		        <div class="uk-float-left">
                	{!! (new App\Pagination($contents->appends(\Request::except('id'))))->render() !!}
		        </div>
		    </div>
		@else
		    <p class="uk-text-center">Новостей нет</p>
		@endif
    </div>
@endsection
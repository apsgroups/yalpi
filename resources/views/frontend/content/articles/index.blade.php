@extends('frontend.layouts.master')

@section('breadcrumbs', Breadcrumbs::render('frontend.content.type', $contentType))

@include('frontend.includes.seometa', ['model' => $contentType])

@include('frontend.content.articles.opengraph')

@section('content')
    <h1 class="uk-text-center uppercase">{{ $contentType->title }}</h1>

    <div class="uk-margin-large-top">
        @if(!empty($contents))
            <div class="uk-grid uk-grid-medium articles" data-uk-grid-margin data-uk-grid-match>
                @foreach($contents as $content)
                    <div class="uk-width-1-3"> 
                    	<div class="uk-panel block-area uk-padding-remove">
                    		<div class="uk-panel-teaser">
		                        <a href="{{ content_url($content) }}" class="uk-display-block">
		                            <img src="{{ img_src($content->image, 'article') }}" />
		                        </a>

		                        <div class="uk-panel-body">
			                        <h3 class="uk-panel-title">
			                        	<a href="{{ content_url($content) }}" class="link-as-text hover">{{ $content->title }}</a>
			                        </h3>

			                        <div class="uk-text-muted">{!! $content->getIntro() !!}</div>
			                    </div> 
	                        </div>
                        </div>
                    </div>
                @endforeach
            </div>


            <div class="uk-margin-large-top uk-clearfix">
                <div class="uk-float-left">
                    {!! (new App\Pagination($contents->appends(\Request::except('id', 'menu_item_id'))))->render() !!}
                </div>
            </div>
        @else
            <p class="uk-text-center">Статей нет</p>
        @endif
    </div>
@endsection
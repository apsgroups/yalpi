<?php

$meta = isset($meta) ? $meta : \App\Services\Meta\MetaFactory::getInstance($contentType);

$og = [
    'url' => $contentType->link(true),
    'title' => preg_replace('/"(.*?)"/', '«$1»', $meta->title()),
    'description' => preg_replace('/"(.*?)"/', '«$1»', $meta->description()),
    'type' => 'website',
];

$first = $contents->first();

if(!empty($first->image)) {
	$og['image'] = url(img_src($first->image, 'large'));
}
?>

@push('head')
    @include('frontend.includes.opengraph', compact('og'))
@endpush
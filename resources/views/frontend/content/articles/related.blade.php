<div class="uk-margin-large">
	<h4>Читайте также</h4>

	<div class="uk-grid uk-grid-large related-items uk-margin-top" data-uk-grid-margin>
	@foreach($relatedContent as $item)
		<div class="uk-width-1-4">
			@if($item->image)
			<a href="{{ $item->link() }}" class="uk-link-reset">
				<img src="{{ img_src($item->image, 'article_related') }}" alt="{{ $item->title }}" /> 
			</a>
			@endif

			<div class="uk-margin-top">
				<a href="{{ $item->link() }}" class="uk-link-reset uk-text-bold">
					{{ $item->title }}
				</a>
			</div>
		</div>
	@endforeach
	</div>
</div>
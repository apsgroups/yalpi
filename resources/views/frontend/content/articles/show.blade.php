@push('head')
	@if($prev)
	<link rel="prev" href="{{ $prev->link() }}">
	@endif

	@if($next)
	<link rel="next" href="{{ $next->link() }}">
	@endif
@endpush

@extends('frontend.layouts.master')

@include('frontend.includes.seometa', ['model' => $content])

@include('frontend.content.articles.opengraph_show')

@section('breadcrumbs')
<div>
{!! Breadcrumbs::render('frontend.content.show', $content) !!}
</div>
@endsection

@section('content')
<div class="block-area">
	<article class="uk-article" itemscope itemtype="https://schema.org/Article">
	    <h1 class="uk-article-title uk-text-center uppercase">{{ $content->title }}</h1>

	    <div itemprop="aggregateRating" itemscope="itemscope" itemtype="https://schema.org/AggregateRating">
	    	<meta itemprop="ratingValue" content="5" />
	    	<meta itemprop="bestRating" content="5" />
	    	<meta itemprop="ratingCount" content="100" />
	    </div>
	    
	    <meta itemprop="author" content="Mebel169.ru" />
	    <meta itemprop="datePublished" content="{{ $content->created_at->toDateTimeString() }}" />
	    <meta itemprop="headline" content="{{ $content->title }}" />

	    @if($content->image)
	    <meta itemprop="image" content="{{ url(img_src($content->image)) }}" />
	    @endif

	    <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
	    	<meta itemprop="name" content="Mebel169.ru" />
	    	<a href="{{ url('/') }}" itemprop="url">&nbsp;</a>
	    	<meta itemprop="logo" content="{{ url('/images/layout/sprite/logo.png') }}">
	    	<meta itemprop="telephone" content="+7 (495) 256-11-10" />

	    	<div itemprop="address" itemscope itemtype="https://schema.org/PostalAddress">
			    <meta itemprop="streetAddress" content="">
			    <meta itemprop="addressLocality" content="">
			    <meta itemprop="addressRegion" content="">
			    <meta itemprop="postalCode" content="">
			 </div>
	    </div>

	    <div class="uk-clearfix uk-margin-bottom" itemprop="articleBody" itemscope itemtype="https://schema.org/articleBody">
		    {!! $content->more !!}
	    </div>

	    @if($products->count())
	    <div class="line"></div>
	    <div class="uk-margin-large">
        	@include('frontend.category.partials.layouts.grid')
	    </div>
        @endif

        <p><time datetime="{{ $content->created_at->toDateTimeString() }}">{{ $content->created_at->format('d.m.Y') }}</time></p>
	</article>

    <div class="uk-margin-large-top">
        <a href="{{ content_type_url($content->type) }}" class="uppercase link-as-text hover"><i class="uk-icon-newspaper-o"></i> Все Статьи</a>
    </div>

    @if($prev || $next)
	<div class="uk-margin-large-top uk-pagination articles-nav">
		@if($prev)
		<li class="uk-pagination-previous"><a href="{{ $prev->link() }}"><i class="uk-icon-justify uk-icon-angle-left"></i> {{ $prev->title }}</a></li>
		@endif

		@if($next)
		<li class="uk-pagination-next"><a href="{{ $next->link() }}">{{ $next->title }} <i class="uk-icon-justify uk-icon-angle-right"></i></a></li>
		@endif
	</div>
	@endif

    @if($relatedContent->count())
    	@include('frontend.content.articles.related')
    @endif
</div>    
@endsection
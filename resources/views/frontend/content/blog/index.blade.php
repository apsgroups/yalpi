@extends('frontend.layouts.master')

@include('frontend.includes.seometa', ['model' => $contentType])

@section('breadcrumbs', Breadcrumbs::render('frontend.content.type', $contentType))

@if(!Auth::check())
    @section('body-attr', 'class="simple-theme"')
@endif

@section('content')
    <div class="blog uk-container uk-container-center">
    	<div class="content-inner">
    		{!! $contentType->description !!}
            
            @if($tags->count())
        		<ul class="tags as-buttons">
                    @foreach($tags as $tag)
        			<li><a href="{{ route('content.byTag', $tag->slug) }}">{{ $tag->name }}</a></li>
                    @endforeach
        		</ul>
            @endif
    	</div>

		@if(!empty($contents))
		    <div id="filter-results">
                @include('frontend.content.blog.partials.items')
            </div>

            <div class="pagination-inner uk-clearfix">
                <div class="loadmore-block uk-text-center">
                    @include('frontend.content.blog.pagination.loadmore')
                </div>

                @if($pagination)
                <div class="pagination-box">
                    @include('frontend.content.blog.pagination.index')
                </div>
                @endif
		    </div>
		@else
		    <p class="uk-text-center">Статей нет</p>
		@endif
    </div>
@endsection
@foreach($contents as $content)
<div class="blog-item">
    @if($content->image)
        <a href="{{ content_url($content) }}">
            <img src="{{ img_src($content->image, 'blog') }}" align="{{ $content->title }}" title="{{ $content->title }}" class="lead-img" />
        </a>
    @endif
    
    <div class="content-inner">
        <h3><a href="{{ content_url($content) }}">{{ $content->title }}</a></h3>

        <div class="item-meta">
            <div class="author"><img src="/images/layout/blog/author.svg" alt="{{ $content->user->name }}">{{ $content->user->name }}</div>
            <div class="date">
                @php($date = new Date($content->created_at))
                {{ $date->format('F d, Y') }}
            </div>
        </div>

        <div class="item-text">
            {!! $content->getIntro() !!}
        </div>

        <div class="item-links uk-clearfix">
            <a href="{{ content_url($content) }}" class="read-more">Подробнее...</a>

            <div class="uk-float-right">
                <div class="uk-float-left share-btn">
                    @include('frontend.content.blog.partials.social', ['content' => $content])
                </div>                

                <a href="{{ content_url($content) }}#comments" class="comments-count">{{ $content->rate_count }}</a>
            </div>
        </div>
    </div>
</div>
@endforeach

<script>
    var comments_count = function(count) {
        return count;
    }
</script>

<script src="https://cdn.commento.io/js/count.js"></script>
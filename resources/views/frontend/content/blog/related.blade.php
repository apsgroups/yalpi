<div class="related-items">
	@foreach($relatedContent as $item)
		<div class="related-item">
			<div class="uk-grid">
				<div class="uk-width-1-2">
					@if($item->image)
					<a href="{{ $item->link() }}">
						<img src="{{ img_src($item->image, 'blog-related') }}" alt="{{ $item->title }}" class="lead-img" /> 
					</a>
					@endif
				</div>
				<div class="uk-width-1-2">
				    <div class="content-inner">
				    	<a class="header" href="{{ $item->link() }}">{{ $item->title }}</a>

				        <div class="item-meta">
				            <div class="author">{{ $item->user->name }}</div>
				            <div class="date">
				                @php($date = new Date($item->created_at))
				                <time datetime="{{ $item->created_at->toDateTimeString() }}">{{ $date->format('F d, Y') }}</time>
				            </div>
				        </div>

				        <div class="item-text">
				            {!! $item->getIntro() !!}
				        </div>

				        <div class="item-links">
				            <a href="{{ content_url($item) }}" class="read-more">Подробнее...</a>
				        </div>
				    </div>
				</div>
			</div>
		</div>
	@endforeach
</div>
@if($pagination && $pagination->hasMorePages() && $pagination->currentPage() < $pagination->lastPage())
<button class="uk-button uk-width-1-1 uk-text-center loadmore" id="loadmore" data-url="{{ $pagination->nextPageUrl() }}">Показать больше</button>
@endif
@extends('frontend.layouts.master')

@include('frontend.includes.seometa', ['model' => $contentType])

@section('breadcrumbs', Breadcrumbs::render('frontend.content.type', $contentType))

@section('content')
    <h1>{{ $contentType->title }}</h1>

    @foreach($contents as $content)
        <div>
            <h3>{!! link_to(content_url($content), $content->title) !!}</h3>
            <p>{!! $content->intro !!}</p>
        </div>
    @endforeach

    {!! $contents->render() !!}
@endsection
@extends('frontend.layouts.master')

@section('breadcrumbs', Breadcrumbs::render('frontend.content.type', $contentType))

@include('frontend.includes.seometa', ['model' => $contentType])

@include('frontend.content.articles.opengraph')

@section('content')
    <article class="uk-article">
        <h1 class="uk-article-title uk-text-center uppercase">{{ $contentType->title }}</h1>

        @if(!empty($contents))
        <div class="uk-accordion bordered-accordion" data-uk-accordion="{'hideall': true}">
            @foreach($contents as $content)
                <div class="uk-accordion-title">{{ $content->title }}</div>
                <div class="uk-accordion-content">{!! $content->intro !!}</div>
            @endforeach
        </div>
        @endif

        <div class="uk-container-center uk-width-2-4 uk-margin-large-top">
            <div class="block-area">
                @include('frontend.feedback.question.index')
            </div>
        </div>
    </article>
@endsection
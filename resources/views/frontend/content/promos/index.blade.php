@extends('frontend.layouts.master')

@section('breadcrumbs', Breadcrumbs::render('frontend.content.type', $contentType))

@include('frontend.includes.seometa', ['model' => $contentType])

@include('frontend.content.articles.opengraph')

@section('content')
    <h1 class="uk-text-center uppercase">{{ $contentType->title }}</h1>

    <div class="uk-margin-large-top block-area uk-article">
        @if(!empty($contents))
            @foreach($contents as $content)
	            <div>
	            	<h2>{{ $content->title }}</h2>
		            <a href="{{ content_url($content) }}" class="uk-display-block">
		                <img src="{{ img_src($content->image, 'promo') }}" />
		            </a>

		            <div class="uk-text-right uk-margin-top"><a href="{{ $content->link() }}" class="uk-button uk-button-primary uk-button-large">Подробнее</a></div>

		            <div class="uk-margin-large-top uk-margin-large-bottom">
		            	<hr />
		            </div>
		        </div>
            @endforeach
        @else
            <p class="uk-text-center">Статей нет</p>
        @endif
    </div>
@endsection
@extends('frontend.layouts.master')

@include('frontend.includes.seometa', ['model' => $content])

@section('breadcrumbs', Breadcrumbs::render('frontend.content.show', $content))

@section('content')
	<article class="uk-article block-area">
	    <h1 class="uk-article-title uk-text-center uppercase">{{ $content->title }}</h1>

	    <div class="uk-clearfix">
		    @if($content->image)
		    <img src="{{ img_src($content->image, 'promo') }}" />
		    @endif
	    </div>

	    <div class="uk-margin-top">
		    {!! $content->more !!}
		</div>

        <div class="uk-margin-large-top">
            <a href="{{ content_type_url($content->type) }}" class="uppercase link-as-text hover"><i class="uk-icon-chevron-left"></i> Посмотреть все акции</a>
        </div>
	</article>
@endsection
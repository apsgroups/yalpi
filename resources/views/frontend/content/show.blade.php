@extends('frontend.layouts.master')

@include('frontend.includes.seometa', ['model' => $content])

@section('breadcrumbs', Breadcrumbs::render('frontend.content.show', $content))

@section('content')
	<article class="uk-article uk-container uk-container-center">
	    <h1 class="uk-article-title uk-text-center uppercase">{{ $content->title }}</h1>

	    <div class="uk-clearfix uk-margin-bottom">
		    @if($content->image)
		    <img src="{{ img_src($content->image, 'news') }}" class="uk-align-right uk-margin-top" />
		    @endif

		    {!! $content->more !!}
	    </div>

	    @if($products->count())
	    <div class="line"></div>
	    <div class="uk-margin-large">
        	@include('frontend.category.partials.layouts.grid')
	    </div>
        @endif

        @if($content->type->slug === 'news')
        <div class="uk-margin-large-top">
            <a href="{{ content_type_url($content->type) }}" class="uppercase link-as-text hover"><i class="uk-icon-newspaper-o"></i> Все новости</a>
        </div>
        @endif
	</article>
@endsection
<div class="tab-carousel">
    <div class="uk-clearfix">
        <div class="block-title">Бесплатные уроки:</div>

        <ul class="switcher">
            @foreach($tabs as $tab)
                @continue(! $products[$tab->id]->count())
                <li>{{ $tab->title }}</li>
            @endforeach
        </ul>
    </div>

    <div class="tab-content product-items">
        @foreach($products as $tabId => $items)
            @continue(! $items->count())
            
            <div class="tab-item">
                <div class="owl-carousel owl-theme">
                    @foreach($items as $product)
                        @include('frontend.product.preview', ['product' => $product->getPreviewData()])
                    @endforeach
                </div>

                <div class="see-more">
                    <a href="{{ $tabs[$tabId]->url }}">Посмотреть все уроки</a>
                </div>
            </div>
        @endforeach
    </div>
</div>

@push('js-footer')
{!! Html::script('/js/plugin/owl/owl.carousel.min.js') !!}
{!! Html::script('/js/frontend/promo.js') !!}

{!! Html::style('/js/plugin/owl/assets/owl.carousel.min.css') !!}
{!! Html::style('/js/plugin/owl/assets/owl.theme.default.min.css') !!}
@endpush
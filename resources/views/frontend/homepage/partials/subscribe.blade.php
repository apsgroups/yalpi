<div class="subscribe">
	<div class="title">Мастер-классы<br />на любые темы!</div>
	<div class="subtitle">Обучайся у лучших преподавателей<br>не выходя из дома!</div>

	<div class="desc">Регистрируйся, смотри видео и проходи тесты</div>

    {!! Form::open(['route' => 'subscribe.add', 'class' => 'validate-form ajax-form uk-form', 'method' => 'post']) !!}
        {!! Form::text('email', null, ['class' => 'email required', 'placeholder' => 'Введите ваш E-mail']) !!}
        {!! Form::hidden('shortbox', 1) !!}
        <button class="uk-button uk-button-primary">Я в деле!</button>

    	<div class="note">Мы не делимся данными, обещаем отправлять информацию портала по e-mail не<br>
чаще одного раза в месяц</div>
    {!! Form::close() !!}
</div>
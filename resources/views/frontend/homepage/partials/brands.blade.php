@php($brands = ['samsung'])

<div class="brand-slider">
	<div data-uk-slideset="{default: 5}">
	    <ul class="uk-grid uk-slideset">
	    	@foreach($brands as $brand)
	        <li><img src="/images/brands/{{ $brand }}.png" alt="{{ $brand }}" title="{{ $brand }}" /></li>
	        @endforeach
	    </ul>
	    <a href="#" data-uk-slideset-item="previous" class="nav-btn prev"><i class="uk-icon-chevron-left"></i></a>
	    <a href="#" data-uk-slideset-item="next" class="nav-btn next"><i class="uk-icon-chevron-right"></i></a>
	</div>
</div>
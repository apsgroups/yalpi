@php($content = Block::get('comfort-price'))

@if($content)
<div class="uk-block uk-block-muted uk-margin-large-top">
    <div class="uk-container">
        <h1 class="uk-heading-large uk-text-center uppercase">{{ $content->title }}</h1>

        <div class="uk-text-justify">
            {!! $content->more !!}
        </div>
    </div>
</div>
@endif
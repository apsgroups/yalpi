<div class="menu-block-title large-margin">Обратная связь</div>

<nav class="sidebar-box contact-nav no-title">
    <ul>
        <li>
            <a class="btn-popup" href="{{ route('feedback.director') }}"><i class="uk-icon-envelope"></i> <span>Письмо директору</span></a>
        </li>
        <li>
            <a href="{{ route('feedback.thank') }}"><i class="uk-icon-smile-o uk-text-success"></i> <span>Поблагодарить</span></a>
        </li>
        <li>
            <a href="{{ route('feedback.complain') }}"><i class="uk-icon-frown-o uk-text-danger"></i> <span>Пожаловаться</span></a>
        </li>
    </ul>
</nav>
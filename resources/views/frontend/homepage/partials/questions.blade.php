<div class="uk-block uk-block-muted uk-margin-large-top question">
    <div class="uk-container">
        <h3 class="uk-heading-large uk-text-center uppercase">Остались вопросы?</h3>

        <div class="uk-text-center icon">
            {!! link_to_route('feedback.callback', 'Заказать консультацию специалиста', [], ['class' => 'btn-popup uppercase question primary-link']) !!}
        </div>
    </div>
</div>
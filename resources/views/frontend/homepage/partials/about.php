<div class="about">
	<div class="block-title">Yalpi сервис:</div>

	<div class="uk-grid uk-grid-margin uk-text-center">
		<div class="uk-width-1-3">
			<div class="title">Обучение сотрудников</div>
			<div class="desc">Мы можем составить единую подборку от лучших преподавателей в СНГ для ваших сотрудников</div>
			<a href="" class="more">Узнать больше</a>
		</div>
		<div class="uk-width-1-3">
			<div class="title">Подбор уроков</div>
			<div class="desc">Команда Yalpi может персонализировать программу обучения для Вас. Это позволит Вам оптимизировать время и достичь результатов быстрее</div>
			<div class="more">
				<a href="">Узнать больше</a>
			</div>
		</div>
		<div class="uk-width-1-3">
			<div class="title">Продвижение уроков</div>
			<div class="desc">На правах рекламы вы можете продвинуть свой урок в топ выдачи Yalpi, это позволит Вам быстрей набрать группу учеников.</div>
			<a href="" class="more">Узнать больше</a>
		</div>
	</div>
</div>
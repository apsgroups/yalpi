@extends('frontend.layouts.master')

@php($slider = Block::get('slider'))

@include('frontend.includes.seometa_group', ['group' => 'homepage'])

<?php
$og = [
    'url' => url('/'),
    'title' => \Settings::getOr('homepage_page_title', 'page_title'),
    'description' => \Settings::getOr('homepage_metadesc', 'metadesc'),
    'image' => '',
    'type' => 'product',
];

?>

@push('head')
    @include('frontend.includes.opengraph', compact('og'))
@endpush

@section('content')
    <div class="uk-container uk-container-center">
        @include('frontend.homepage.partials.subscribe')

        {!! \Widgets::position('promo') !!}
    </div>

    <div class="bg-container">
        <div class="uk-container uk-container-center">
            @widget('menuWidget', ['menu' => 'catalog-homepage', 'layout' => 'catalog-homepage'])

            @widget('newsWidget', ['limit' => 5])
        </div>
    </div>

    <div class="uk-container uk-container-center">
        @widget('contentTypeWidget', ['id' => 3, 'layout' => 'about'])
    </div>
@endsection

@section('subscribe')
    @include('frontend.includes.partials.subscribe')
@endsection
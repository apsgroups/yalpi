@extends('frontend.layouts.master')

@section('content')
    <div class="uk-container uk-container-center">
        @include('frontend.includes.messages')
    </div>
    
    <div class="uk-container uk-container-center" style="max-width: 460px">
        <div class="auth">
            @include('frontend.includes.logo')

            <div>
                {!! Form::open(['url' => 'password/reset', 'class' => 'uk-form validate-form']) !!}
                    <div class="uk-form-row">
                        <div class="uk-form-controls">
                            {!! Form::input('email', 'email', null, ['id' => 'email', 'class' => 'uk-form-width-large email login-bg required', 'placeholder' => trans('validation.attributes.frontend.email')]) !!}
                        </div><!--col-md-6-->
                    </div><!--form-group-->

                    <div class="uk-form-row">
                        <div class="uk-form-controls">
                            {!! Form::input('password', 'password', null, ['id' => 'password', 'class' => 'password-bg uk-form-width-large required', 'placeholder' => trans('validation.attributes.frontend.password')]) !!}
                        </div><!--col-md-6-->
                    </div><!--form-group-->

                    <div class="uk-form-row">
                        <div class="uk-form-controls">
                            {!! Form::input('password', 'password_confirmation', null, ['id' => 'password_confirmation', 'class' => 'password-bg uk-form-width-large required', 'placeholder' => trans('validation.attributes.frontend.password_confirmation')]) !!}
                        </div><!--col-md-6-->
                    </div><!--form-group-->

                    <div>
                        <button type="submit" class="uk-button uk-button-hover">
                            {{ trans('labels.frontend.passwords.reset_password_button') }}
                        </button>
                    </div>

                    <input type="hidden" name="token" value="{{ $token }}">
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
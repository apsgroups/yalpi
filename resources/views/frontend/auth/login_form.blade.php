<div class="auth">
    @include('frontend.includes.logo')

    <div>
        {!! Form::open(['route' => 'auth.login.submit', 'class' => 'uk-form ajax-form validate-form']) !!}
            <div class="uk-form-row">
                <div class="uk-form-controls">
                    {!! Form::input('email', 'email', null, ['id' => 'email', 'class' => 'uk-form-width-large email login-bg required', 'placeholder' => trans('validation.attributes.frontend.email')]) !!}
                </div><!--col-md-6-->
            </div><!--form-group-->

            <div class="uk-form-row">
                <div class="uk-form-controls">
                    {!! Form::input('password', 'password', null, ['id' => 'password', 'class' => 'password-bg uk-form-width-large required', 'placeholder' => trans('validation.attributes.frontend.password')]) !!}
                </div><!--col-md-6-->
            </div><!--form-group-->

            <div class="uk-form-row links">
                <div class="uk-clearfix">
                    <div class="uk-float-right">
                        {!! link_to('password/reset', trans('labels.frontend.passwords.forgot_password'), ['class' => 'muted-link btn-popup']) !!}
                    </div>
                    <div class="uk-float-left">
                        <label>
                            {{ trans('labels.frontend.auth.remember_me') }} {!! Form::checkbox('remember') !!}
                        </label>
                    </div>
                </div>
            </div><!--form-group-->

            <div>
                <button type="submit" class="uk-button uk-button-hover">
                    {{ trans('labels.frontend.auth.login_button') }}
                </button>
            </div>
        {!! Form::close() !!}
    </div>

    <div class="uk-text-center text-line">
        <span>Войти как пользователь</span>
    </div>

    <div class="social-account uk-grid">
        @foreach($socialite_links as $link)
            <div class="uk-width-1-3">{!! $link !!}</div>
        @endforeach
    </div>

    <div class="uk-text-center">
        Еще нет аккаунта? {!! link_to('register', trans('labels.frontend.auth.register_button'), ['class' => 'btn-popup']) !!}
    </div>
</div>
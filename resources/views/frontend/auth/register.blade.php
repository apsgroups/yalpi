@extends('frontend.layouts.master')

@section('content')
    <div class="uk-container uk-container-center" style="max-width: 460px">
        @include('frontend.auth.register_form')
    </div>
@endsection
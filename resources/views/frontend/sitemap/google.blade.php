<?php echo '<?' ?>xml version="1.0" encoding="UTF-8"<?php echo '?>' ?>
<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd"
xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
xmlns:image="http://www.google.com/schemas/sitemap-image/1.1">
@foreach($items as $item)
<url>
<loc>{{ request()->root().$item['link'] }}</loc>
@if(!empty($item['image']))
<image:image>
<image:loc>
{{ request()->root().img_src($item['image']) }}
</image:loc>
</image:image>
@endif
<lastmod>{{ $item['lastmod'] }}</lastmod>
<changefreq>{{ $item['changefreq'] }}</changefreq>
<priority>{{ $item['priority'] }}</priority>
</url>
@endforeach
</urlset>
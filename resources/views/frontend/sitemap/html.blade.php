@extends('frontend.layouts.master')

@section('title', 'Карта сайта')
@section('metadesc', 'Карта сайта')

@section('content')
    <article class="uk-article uk-container uk-container-center">
        <h1 class="uk-article-title uppercase">Карта сайта</h1>

        <div style="margin-left: 35px">
            @widget('menuWidget', ['menu' => 'main', 'layout' => 'sitemap'])
        </div>
    </article>
@endsection
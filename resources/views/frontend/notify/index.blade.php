@if($notifications->count())
	<ul class="uk-nav">
		@foreach($notifications as $notify)
			@if($notify->notifable_type === \App\Models\Comment::class)
	    		<li>
	    			<a href="{{ $notify->notifable->commentable->link() }}#comment{{ $notify->notifable_id }}" data-id="{{ $notify->id }}">{{ $notify->notifable->user->name }} ответил(а) Вам <span>{{ $notify->notifable->commentable->title }}</span></a>
	    		</li>
	    	@endif
	    @endforeach
	</ul>
@else
	Уведомлений нет
@endif
@extends('frontend.layouts.master')

@section('title', $meta->title().($pagination->currentPage() > 1 ? ' | Страница '.$pagination->currentPage() : ''))

@include('frontend.includes.seometa', ['model' => $author])

@section('breadcrumbs', '<div class="white-bg">'.Breadcrumbs::render('frontend.author', $author).'</div>')

@section('body-attr', 'class="grey-bg"')

@section('content')
<div class="white-bg">
    <div class="uk-container uk-container-center">
        <div class="uk-clearfix category-heading-inner">
            <h1 class="category-heading page-heading uk-float-left">{!! $meta->heading() !!}</h1>

            @if($total = $pagination->total())
                <div class="category-total uk-float-left">{{ $total }} {{ total_products_morph($total) }}</div>
            @endif
        </div>
    </div>
</div>

<div class="category-bg">
    <div class="uk-container uk-container-center">
        <div id="filter-results" class="block-area margin">
            @include('frontend.author.partials.products', ['itemPropUrl' => route('author.items', [$author->slug])])
        </div>
    </div>
</div>
@stop

@if($pagination->currentPage() > 1)
@push('head')
<link rel="canonical" href="{{ route('author.items', [$author->slug]) }}" />
@endpush
@endif
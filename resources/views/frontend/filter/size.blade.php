@extends('frontend.filter.option')

@section('option-title-'.$name, $title)

@section('option-form-'.$name)
    <div class="uk-grid">
        <div class="uk-width-1-3">
            <div class="option-label">Ширина</div>
            @include('frontend.filter._range', ['name' => 'width', 'units' => 'мм'])
        </div>
        <div class="uk-width-1-3">
            <div class="option-label">Высота</div>
            @include('frontend.filter._range', ['name' => 'height', 'units' => 'мм'])
        </div>
        <div class="uk-width-1-3">
            <div class="option-label">Глубина</div>
            @include('frontend.filter._range', ['name' => 'depth', 'units' => 'мм'])
        </div>
    </div>
@stop


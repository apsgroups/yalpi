{{--@include('frontend.category.partials.sorting')--}}

@foreach($products->chunk(3) as $items)
    <div class="uk-grid" data-uk-grid-margin>
        @foreach($items as $k => $product)
            <div class="uk-width-medium-1-3 {{ $k === 0 ? 'uk-row-first' : '' }}">
                @include('frontend.product.preview')
            </div>
        @endforeach
    </div>
@endforeach

{{--{!! (new App\Pagination($es->paginate(9)))->render() !!}--}}

{!! Form::open(['method' => 'get']) !!}
{!! Form::select('per_page', [24 => 24, 48 => 48, 'all' => 'all'], $perPage, ['class' => 'per-page-box']) !!}
{!! Form::close() !!}
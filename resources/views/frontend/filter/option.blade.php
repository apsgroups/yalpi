<div class="option-title">
    @yield('option-title-'.$name)
</div>

<div class="filter-form">
    @yield('option-form-'.$name)
</div>
<div class="option-wrapper">
	<div class="option-btn">
	    @yield('option-title-'.$name)
	    <i class="uk-icon-"></i>
	</div>
	   
	   <div class="dropdown-connector"></div>

	<div class="option-form">
	    @yield('option-form-'.$name)
	</div>
</div>
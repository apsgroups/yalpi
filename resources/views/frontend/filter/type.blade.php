@if(empty($options[$name]['buckets']) || count($options[$name]['buckets']) <= 1)
<?php return ?>
@endif

@extends('frontend.filter.option')

@section('option-title-'.$name, $title)

@section('option-form-'.$name)
    @php($take = $name === 'model' ? 24 : 12)
    @php($items = collect($options[$name]['buckets'])->take($take))
    <?php
    $cols = $items->count() / 4 <= 2 ? 4 : 5;

    if(!empty($context) && $context === 'search' && !empty($options['obshchij-cvet']['buckets'])) {
        $cols = $items->count() >= 6 ? ceil($items->count() / 2) : $items->count();
    }
    ?>

    @php($items = $items->chunk($cols))

    @foreach($items as $group)
        <div class="list-group">
        @foreach($group as $k => $option)
            <div class="list-item">
                @php($id = $name.'_'.$k)

                @php($attr = ['class' => 'checkbox', 'id' => $id])

                {!! Form::checkbox('filter['.$name.'][]', $option['key'], (isset($attributes[$name]) && in_array($option['key'], $attributes[$name])), $attr) !!}

                <label for="{{ $id }}">
                    {{ \App\Models\ProductType::find($option['key'])->title }}
                </label>
            </div>
        @endforeach
        </div>
    @endforeach
@stop
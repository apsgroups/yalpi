@inject('listProperty', '\App\Services\Filter\Html\ListProperty')

@if(empty($options[$name]['buckets']))
<?php return ?>
@endif

@php($items = collect($options[$name]['buckets'])->take(12)->sort())
@php($chunkCount = 4)
@php($items = $items->chunk($chunkCount))

@foreach($items as $group)
    <div class="list-group">
    @foreach($group as $k => $option)
        <div class="list-item">
            @php($id = $name.'_'.$k)

            @php($attr = ['class' => 'checkbox', 'id' => $id])

            {!! Form::checkbox('filter['.$name.'][]', $option['key'], (isset($attributes[$name]) && in_array($option['key'], $attributes[$name])), $attr) !!}

            <label for="{{ $id }}">
                {{ $option['key'] }}
            </label>
        </div>
    @endforeach
    </div>
@endforeach
@if(isset($options[$name]))
    от
    {!! Form::text('filter['.$name.'][min]', $options[$name]['min_value']['value'], ['id' => $name.'_min', 'class' => 'range-input min']) !!}
    до
    {!! Form::text('filter['.$name.'][max]', $options[$name]['max_value']['value'], ['id' => $name.'_max', 'class' => 'range-input max']) !!}
    {{ $units }}

    <div class="nstSlider-holder">
        <div class="nstSlider"
             id="slider_{{ $name }}"
            data-name="{{ $name }}"
            data-range_min="{{ $options[$name]['min_value']['value'] }}"
            data-range_max="{{ $options[$name]['max_value']['value'] }}"
            data-cur_min="{{ $attributes[$name]['min'] or $options[$name]['min_value']['value'] }}"
            data-cur_max="{{ $attributes[$name]['max'] or $options[$name]['max_value']['value'] }}">

            <div class="bar"></div>
            <div class="leftGrip icon-range-grip"></div>
            <div class="rightGrip icon-range-grip"></div>
        </div>
    </div>
@endif
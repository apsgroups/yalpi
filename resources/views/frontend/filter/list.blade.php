@inject('listProperty', '\App\Services\Filter\Html\ListProperty')

@if(empty($options[$name]['buckets']) || (count($options[$name]['buckets']) <= 1 && empty($showSingle)))
<?php return ?>
@endif

@extends('frontend.filter.'.(!empty($layout) ? $layout : 'option'))

@section('option-title-'.$name, $title)

@section('option-form-'.$name)
    @php($take = $name === 'model' ? 24 : 12)
    @php($items = collect($options[$name]['buckets'])->take($take))
    <?php $cols = 2; ?>
    @php($items = $items->chunk($cols))

    @foreach($items as $group)
        <div class="list-group">
        @foreach($group as $k => $option)
            <div class="list-item">
                @php($id = $name.'_'.$k)

                @php($attr = ['class' => 'checkbox', 'id' => $id])
                
                {!! Form::checkbox('filter['.$name.'][]', $option['key'], (isset($attributes[$name]) && in_array($option['key'], $attributes[$name])), $attr) !!}

                <label for="{{ $id }}">
                    @php($image = $listProperty->image($option['key']))

                    @if($image)
                    <img src="{{ img_src($image, 'color') }}" alt="{{ $listProperty->title($option['key']) }}" />
                    @endif

                    {{ $listProperty->title($option['key']) }}
                </label>
            </div>
        @endforeach
        </div>
    @endforeach
@stop
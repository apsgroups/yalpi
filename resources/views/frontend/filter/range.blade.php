@extends('frontend.filter.option')

@section('option-title-'.$name, $title)

@section('option-form-'.$name)
    @include('frontend.filter._range')
@stop

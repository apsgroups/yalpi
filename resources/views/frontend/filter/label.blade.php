{!! Form::checkbox('filter['.$name.'][]', true, (isset($attributes[$name])), ['class' => 'checkbox', 'id' => $name.'_check']) !!}
<label for="{{ $name }}_check">
    {{ $title }}
</label>
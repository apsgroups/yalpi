<?php

$totalOthers = 0;

foreach($bucketNames as $bn) {
    if(!empty($options[$bn]['buckets'])) {
        $totalOthers += count($options[$bn]['buckets']);
    }
}

if(!$totalOthers) {
    return;
}
?>

@inject('listProperty', '\App\Services\Filter\Html\ListProperty')

<div class="filter-field">
    <div class="option-title">{{ $title }}</div>

    <div class="filter-form">
        <div class="list-group">
            @foreach($bucketNames as $bn)
                @if(!empty($options[$bn]['buckets']))
                    @foreach($options[$bn]['buckets'] as $k => $option)
                        <div class="list-item">
                            @php($id = $bn.'_'.$k)
                            @php($attr = ['class' => 'checkbox', 'id' => $id])
                                
                            {!! Form::checkbox('filter['.$bn.'][]', $option['key'], (isset($attributes[$bn]) && in_array($option['key'], $attributes[$bn])), $attr) !!}

                            <label for="{{ $id }}">
                                {{ $listProperty->title($option['key']) }}
                            </label>
                        </div>
                    @endforeach
                @endif
            @endforeach
        </div>
    </div>
</div>
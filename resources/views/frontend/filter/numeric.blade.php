@inject('listProperty', '\App\Services\Filter\Html\ListProperty')

@if(empty($options[$name]['buckets']))
<?php return ?>
@endif

@extends('frontend.filter.option')

@section('option-title-'.$name, $title)

@section('option-form-'.$name)
    @php($items = collect($options[$name]['buckets'])->take(12)->sort())
    @php($chunkCount = isset($category) && $category->id === 52 && $items->count() / 6 >= 2 ? 6 : 4)
    @php($chunkCount = $items->count() > 4 ? ceil($items->count() / 2) : 4)
    @php($chunkCount = isset($category) && $category->id === 28 ? 4 : $chunkCount)
    @php($items = $items->chunk($chunkCount))

    @foreach($items as $group)
        <div class="list-group">
        @foreach($group as $k => $option)
            <div class="list-item">
                @php($id = $name.'_'.$k)

                @php($attr = ['class' => 'checkbox', 'id' => $id])

                {!! Form::checkbox('filter['.$name.'][]', $option['key'], (isset($attributes[$name]) && in_array($option['key'], $attributes[$name])), $attr) !!}

                <label for="{{ $id }}">
                    {{ $option['key'] }}
                </label>
            </div>
        @endforeach
        </div>
    @endforeach
@stop
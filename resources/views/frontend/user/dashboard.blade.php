@extends('frontend.layouts.master')

@section('title', $user->name)

@section('body-attr', 'class="user-page"')

@section('content')
    @include('frontend.user.partials.user')
    
    <div class="uk-container uk-container-center favorite-products">
        @if($favorites->count())
            @foreach($favorites->chunk(4) as $items)
                <div class="uk-grid products-grid product-items" data-uk-grid-match="{target:'.product-desc'}">
                    @foreach($items as $k => $product)
                        <div class="uk-width-medium-1-4" itemprop="itemListElement" itemscope itemtype="https://schema.org/Offer">
                            @include('frontend.product.preview', ['inRow' => 4, 'product' => $product->getPreviewData()])
                        </div>
                    @endforeach
                </div>
            @endforeach

            <div class="pagination-box">
                @include('frontend.category.partials.pagination', ['pagination' => $favorites])
            </div>
        @else      
            <div class="empty-results">
                У Вас нет избранных мастер-классов. Выберите нужный мастер-класс <a href="/">тут</a>.
            </div>  
        @endif
    </div>
@endsection
@extends('frontend.layouts.master')

@section('title', 'Обсуждения '.$user->name)

@section('body-attr', 'class="user-page"')

@section('content')
    @include('frontend.user.partials.user')
    
    @include('frontend.user.comments.items')
@endsection
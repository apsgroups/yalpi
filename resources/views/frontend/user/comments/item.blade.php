<div class="comment" itemprop="review" itemscope itemtype="https://schema.org/Review">
    @if($comment->commentable)
    <div class="comment-title">
        <a href="{{ $comment->commentable->link() }}#comment{{ $comment->id }}">{{ $comment->commentable->title }}</a>
    </div>
    @endif

    <div class="comment-body">
        @if($comment->user->image)
        <div class="img">
            <img src="{{ img_src($comment->user->image, 'comment') }}" alt="{{ $comment->name }}">
        </div>
        @endif

        <div class="comment-desc">
            @if($comment->rate > 0)
                <div class="rate">{!! str_repeat('<img src="/images/layout/reviews/star.svg" alt="" />', $comment->rate) !!}</div>
            @endif

            <div class="created-date">
                @php($date = new Date($comment->created_at))
                <meta itemprop="datePublished" content="{{ $date->format('Y-m-d') }}">
                {{ $date->format('d F Y H:i') }}
            </div>

            <div class="name" itemprop="author" itemscope itemtype="https://schema.org/Person">
                <span itemprop="name">{{ $comment->name }}</span>
            </div>

            <div class="property comment" itemprop="reviewBody">
                {{ $comment->comment }}
            </div>
        </div>
    </div>
</div>
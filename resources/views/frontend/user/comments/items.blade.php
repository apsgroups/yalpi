<div class="uk-container uk-container-center favorite-products">
    @if($comments->count())
        <div id="comments" class="shadow-block">
            <div id="comment-items">
                @foreach($comments as $comment)
                    @include('frontend.user.comments.item', compact('comment'))
                @endforeach
            </div>

            <div class="pagination-box">
                @include('frontend.category.partials.pagination', ['pagination' => $comments])
            </div>
        </div>
    @else      
        <div class="empty-results">
            Оставьте комментарий под любым уроком, чтобы участвовать в обсуждениях.
        </div>  
    @endif
</div>
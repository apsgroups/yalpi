@extends('frontend.layouts.master')

@section('title', 'Skills '.$user->name)

@section('body-attr', 'class="user-page"')

@section('content')
    @include('frontend.user.partials.user')
    
    <div class="uk-container uk-container-center">
        <div class="empty-results">
            Страница на доработке. Здесь скоро будет новый функционал :)
        </div>
    </div>
@endsection
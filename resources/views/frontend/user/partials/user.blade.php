<?php 
$karma = $user->comments()->sum('popularity');
$image = $user->image ? img_src($user->image, 'avatar') : '/images/layout/user/profile.svg';
?>

<div class="user-dashboard">
	<div class="uk-container uk-container-center">
		<div class="user-account">
			<div class="user-account-main">
			    <div class="user-img" id="upload-drop" class="uk-placeholder">
			        <a class="uk-form-file"><img src="{{ $image }}" alt="{!! $user->name !!}"><input id="upload-select" type="file"></a>
			    </div>

				<h1>
					{!! $user->name !!}
					<span class="karma">{{ sprintf("%+d", $karma) }}</span>
				</h1>
	        
	        	<a href="{!! route('frontend.user.profile.edit') !!}" class="user-settings-btn{{ (route('frontend.user.profile.edit') === request()->fullUrl().'/' ? ' active' : '') }}"></a>
			</div>

			<div class="user-account-data">
		        <div><span>Дата регистрации</span> {!! $user->created_at->format('d.m.Y') !!}</div>
		        <div><span>Почта</span> {!! $user->email !!}</div>
			</div>
		</div>
	</div>
</div>

@include('frontend.user.partials.nav')

<div class="uk-container uk-container-center">
	@include('frontend.includes.messages')
</div>

@push('head')
	{!! Html::style(elixir('css/user.css')) !!}

	@if(!empty($mobile))
	    {!! Html::style(elixir('css/mobile/user.css')) !!}
	@endif
@endpush
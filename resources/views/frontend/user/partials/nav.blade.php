<?php

$comments = auth()->user()->comments()->published(1)->count();

$urls = [
	'frontend.user.dashboard' => 'Мастер классы',
	'frontend.user.profile.comments' => 'Обсуждения <span>'.$comments.'</span>',
	'frontend.user.profile.tasks' => 'Задания и тесты <span>(скоро)</span>',
	'frontend.user.profile.skills' => 'Skills <span>(скоро)</span>'
];

?>
<div class="user-nav" id="user-nav">
	<nav class="uk-container uk-container-center">
		<ul>
			@foreach($urls as $route => $title)
				<?php $url = route($route); ?>
				<li><a href="{{ $url }}"{!! (parse_url($url, PHP_URL_PATH) === '/'.request()->path().'/' ? ' class="active"' : '') !!}>{!! $title !!}</a></li>
			@endforeach
		</ul>
	</nav>
</div>
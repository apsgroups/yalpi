@extends('frontend.layouts.master')

@include('frontend.includes.seometa_group', ['group' => 'favorite'])

@section('breadcrumbs', Breadcrumbs::render('frontend.favorite'))

@section('content')
    <h1 class="uk-text-center uppercase">Избранные товары</h1>

    <div class="block-area">
        <div id="filter-results">
            @include('frontend.favorite.partials.products')
        </div>

        @if($products->count())
            {!! Form::open(['method' => 'post', 'route' => 'favorite.addtocart', 'class' => 'uk-text-center']) !!}
            <button class="uk-button uk-button-large uk-button-primary uk-margin favorite-to-cart" type="submit">Переместить все товары в корзину</button>
            {!! Form::close() !!}
        @endif
    </div>
@endsection
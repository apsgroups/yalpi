<div id="favorites" class="{{ (Favorite::count() > 0 ? ' active' : '') }}">
    <span id="favorites-total">{{ Favorite::count() }}</span>
    <a href="{{ route('favorite.index') }}"><i class="uk-icon-heart-o"></i></a>
</div>
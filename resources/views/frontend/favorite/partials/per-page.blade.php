@inject('urlService', '\App\Services\Frontend\UrlService')

<div class="limitbox control-links uk-margin uk-margin-top">
    <span class="title">На странице:</span>

    @foreach(config('site.perPage.limits') as $limit => $text)
        <a href="{{ $urlService::currentUrl(['per_page' => $limit]) }}" class="{{ ($limit === intval($perPage) ? 'active' : '') }}">{{ $text }}</a>
    @endforeach
</div>
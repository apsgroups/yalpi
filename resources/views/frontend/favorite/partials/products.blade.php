@php($compositions = isset($compositions) ? $compositions : collect([]))

@if(count($products))
    @if($compositions->isEmpty())
        @include('frontend.category.partials.layouts.'.$layoutService->getLayout(), ['addToCartBtn' => true])

        @if($pagination && $pagination->hasMorePages() && $pagination->currentPage() < $pagination->lastPage())
            <div class="loadmore-block uk-margin-large-top">
                @include('frontend.category.partials.loadmore')
            </div>
        @endif

        <div class="uk-margin-top uk-clearfix">
            @if($pagination)
                <div class="uk-float-left">
                    <div class="uk-margin uk-position-relative pagination-box">
                        @include('frontend.category.partials.pagination')
                    </div>
                </div>
            @endif

            <div class="uk-float-right">
                <div class="uk-float-right uk-margin-left">
                    @include('frontend.category.partials.total')
                </div>

                <div class="uk-float-right separator-controls">|</div>

                <div class="uk-float-right uk-margin-right">
                    @include('frontend.category.partials.per-page')
                </div>
            </div>
        </div>

        <div class="uk-clearfix"></div>
    @else
        @include('frontend.category.partials.compositions')
    @endif
@else
    <div class="uk-text-center">У Вас нет избранных товаров</div>
@endif
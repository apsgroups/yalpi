@inject('urlService', '\App\Services\Frontend\UrlService')

<div class="sorting control-links uk-margin uk-margin-top">
    <span class="title">Сортировать по:</span>

    @foreach($sortingService->getNames() as $attribute)
        {!! link_to(
                url($urlService::currentUrl($sortingService->getQuery($attribute))),
                trans('frontend.sorting.'.$attribute),
                ['class' => ($sortingService->isActive($attribute) ? 'active' : '')]
            ) !!}
    @endforeach
</div>
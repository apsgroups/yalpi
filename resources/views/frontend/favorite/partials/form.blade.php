<div class="uk-padding-remove filter uk-width-1-2 uk-container-center uk-text-center uk-margin-bottom">
    <div class="search-form-ext">
    {!! Form::open(['route' => 'search.index', 'id' => 'filter-form', 'class' => 'uk-form no-options', 'method' => 'get']) !!}
        <div class="uk-grid">
            <div class="uk-width-1-2">
                <div class="search-input search-form">
                    {!! Form::text('term', $term, ['placeholder' => 'Начните поиск...']) !!}
                    <button type="submit"></button>
                </div>
            </div>

            <div class="uk-width-1-2">
                <div class="option-wrapper">
                    <div class="option-btn">
                        <span class="option-title">
                            Выберите категорию поиска
                            <span></span>

                            <div class="pointer-top"></div>
                        </span>
                    </div>

                    <div class="delete"></div>

                    <div class="option-form">
                        <div class="filter-form">
                            @php($name = 'categories')

                            @foreach($categories as $id => $title)
                            <div>
                                {!! Form::checkbox($name.'[]', $id, false, ['class' => 'checkbox', 'id' => $name.'_'.$id]) !!}

                                <label for="{{ $name.'_'.$id }}">{{ $title }}</label>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>

    {!! Form::close() !!}
    </div>
</div>
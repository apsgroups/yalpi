<div class="uk-grid list-layout">
    @foreach($products as $k => $product)
        <div class="uk-width-medium-1-1 {{ ($k === 0) ? '' : 'uk-grid-margin' }}">
            <div class="uk-clearfix preview-item uk-padding-bottom">
                <div class="uk-grid">
                    <div class="uk-width-3-4">
                        <div class="uk-float-left">
                            <figure class="uk-overlay uk-text-center uk-overlay-hover">
                                @include('frontend.product.partials.preview.badge')

                                <img data-original="{{ img_src($product->image, 'list') }}" class="lazy" />

                                <noscript>
                                    <img src="{{ img_src($product->image, 'list') }}">
                                </noscript>

                                <div class="uk-overlay-panel uk-overlay-fade uk-overlay-background">
                                    <div class="uk-vertical-align uk-height-1-1">
                                        <div class="uk-vertical-align-middle">
                                            <a href="{{ route('frontend.product.quick', $product->id) }}" class="uk-button uk-button-primary btn-empty quick">Быстрый просмотр</a>
                                        </div>
                                    </div>
                                </div>

                                <a class="uk-position-cover" href="{{ product_url($product) }}"></a>
                            </figure>
                        </div>

                        <div class="desc-text uk-panel uk-panel-space">
                            <div class="uk-text-muted uk-margin-small">
                                {{ $product->getDimension()->getText() }}
                            </div>

                            <h4 class="uk-margin-top-remove lighter">
                                <a href="{{ product_url($product) }}">{{ $product->title }}</a>
                            </h4>

                            <div class="uk-text-justify uk-text-muted uk-margin">
                                {!! $product->description !!}
                            </div>

                            <ul class="points uk-margin">
                                <li>Без предоплат! Оплата при получении</li>
                                <li>Доставка по Москве и МО от 2 000 руб. Возможен самовывоз</li>
                                <li>Гарантия производителя 5 лет</li>
                            </ul>

                            <div></div>
                        </div>
                    </div>

                    <div class="uk-width-1-4 uk-text-center uk-panel uk-panel-space">
                        <div class="uk-float-right">
                            <div class="uk-margin-bottom">
                                @if($product->price > 0)
                                <div class="price big {{ ($product->price_old > 0) ? 'new' : '' }} uk-margin-small">
                                    <span>{{ format_price($product->price) }}</span>
                                    <i class="uk-icon-rub"></i>
                                </div>
                                @endif

                                @if($product->price_old > 0)
                                <div class="uk-text-muted price old">{{ format_price($product->price_old) }} <i class="uk-icon-rub"></i></div>
                                @endif
                            </div>

                            <div>
                                <a class="uk-button uk-button-large uk-button-primary addtocart" data-product-id="{{ $product->id }}">Заказать</a>
                            </div>

                            <div class="uk-margin-top">
                                <a href="{{ route('feedback.oneclick', [$product->id]) }}"
                                    class="btn-popup uk-button uk-button-large uk-button-primary uk-margin uk-button-second"
                                    data-product-id="{{ $product->id }}">
                                        Купить в 1 клик
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
</div>
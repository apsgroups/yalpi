@foreach($products->chunk(3) as $items)
    <div class="uk-grid" data-uk-grid-margin>
        @foreach($items as $k => $product)
            <div class="uk-width-medium-1-3">
                @include('frontend.product.preview', ['favorite' => true])
            </div>
        @endforeach
    </div>
@endforeach
@extends('frontend.layouts.master')

@include('frontend.includes.seometa_group', ['group' => 'cart'])

@section('breadcrumbs', Breadcrumbs::render('frontend.cart'))

@section('content')
<div class="cart-page">
    <h3 class="uk-text-center uppercase uk-heading-large">Корзина</h3>

    @if($cart::amount() > 0)
        <div class="block-area uk-margin-large-bottom">
            @include('frontend.cart.partials.items')
        </div>

        @include('frontend.cart.partials.checkout')
    @else
        <div class="uk-text-center">
            <p>В Вашей корзине <b>нет товаров!</b></p>

            <div class="uk-margin-top">
                <a href="/catalog/" class="uk-button uk-button-large uk-button-primary">В каталог</a>
            </div>
        </div>
    @endif
</div>
@endsection

@if($cart::amount() > 0)
    @section('remarketing-data')
        <script type="text/javascript">
            var google_tag_params = {
                ecomm_prodid: ["{!! collect($products)->implode('product_id', '", "') !!}"],
                ecomm_pagetype: 'cart',
                ecomm_totalvalue: {{ $cart::cost() }},
            };
        </script>
    @endsection
@endif
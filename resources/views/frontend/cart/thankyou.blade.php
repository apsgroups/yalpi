@extends('frontend.layouts.master')

@section('breadcrumbs', Breadcrumbs::render('frontend.cart.thankyou', $order))

@section('content')
<div class="uk-text-center">
    <h3 class="uppercase uk-heading-large">Ура! Заказ успешно оформлен.</h3>

    <p class="uppercase uk-text-large">Спасибо за доверие к нашей компании</p>
    <p>
        Ваш заказ принят. В {!! worktime_msg() !!} мы свяжемся с Вами для уточнения деталей заказа.
    </p>
</div>
@endsection
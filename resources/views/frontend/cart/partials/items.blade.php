{!! Form::open(['route' => 'cart.recalc', 'class' => 'cart-form uk-form']) !!}
<table class="uk-table products">
    <thead>
        <tr>
            <th colspan="2">Товары</th>
            <th>Количество</th>
            <th>Цена за 1 товар</th>
            <th>Общая цена</th>
            <th>Удалить</th>
        </tr>
    </thead>

    <tbody>
    @foreach($products as $key => $product)
        @include('frontend.cart.partials.item')
    @endforeach
        <tr class="cart-divider-bottom">
            <td colspan="6">
                 
            </td>
        </tr>

        <tr>
            <td colspan="3">
                <div class="uk-grid">
                    <div class="uk-width-1-2">
                        <a href="/" class="link-as-text hover uppercase">
                            <span class="uk-icon-angle-left"></span>
                            Вернуться к выбору товаров
                        </a>
                    </div>
                    {{--<div class="uk-width-1-2">--}}
                        {{--У Вас есть промокод?--}}
{{--                        {!! Form::text('coupon', null, ['class' => 'uk-form-width-small uk-margin-small-left uk-margin-small-right', 'id' => 'coupon-input']) !!}--}}

                        {{--<button class="uk-button uk-button-danger" type="button" id="submit-coupon">Ввести</button>--}}
                    {{--</div>--}}
                </div>
            </td>

            <td class="total">
                Итого:

                @if($coupon = $cart::getCoupon())
                {{--<div class="coupon-code-val">Скидка:</div>--}}
                @endif
            </td>

            <td class="center" id="cart-total-sum">
                <div class="big price uk-margin-small">
                    <span class="cart-cost">{{ format_price($cart::cost()) }}</span>
                    {!! config('site.price.currency') !!}
                </div>

                @if($couponDiscount = $cart::couponDiscount())
                {{--<div class="coupon-code-val">{{ format_price($couponDiscount) }} <i class="uk-icon-rub"></i></div>--}}
                @endif
            </td>
            <td></td>
        </tr>
    </tbody>
</table>
{!! Form::close() !!}
<tr class="cart-divider" data-cart-product="{{ $key }}">
    <td colspan="6">
        <div></div>
    </td>
</tr>

<tr class="product-row" data-cart-product="{{ $key }}">
    <td class="image">
        @if($product['model']->image)
            <a href="{{ $product['model']->link() }}" target="_blank">
                <img src="{{ img_src($product['image'], 'small') }}" />
            </a>
        @endif
    </td>
    <td class="item-title">
        <div>
            <a href="{{ $product['model']->link() }}" class="link-as-text hover title" target="_blank">{{ $product['title'] }}</a>
        </div>

        <div class="uk-text-muted">
            {{ $product['model']->getDimension()->getText() }}

            @if(!empty($product['properties']))
                @foreach($product['properties'] as $property)
                    <div>{{ $property['title'] }}: {{ $property['plaintext'] }}</div>
                @endforeach
            @endif

            @if(!empty($product['attribute']))
                <div>{{ $product['attribute']['title'] }}: {{ $product['attribute']['value'] }}</div>
            @endif
        </div>


        @if(!empty($product['modules']))
            <span class="toggle-modules">Что сюда входит</span>
        @endif
    </td>
    <td class="center">
        <div class="quantity-control">
            <button class="minus" type="submit">-</button>
            {!! Form::text('products['.$key.']', $product['amount'], ['class' => 'amount']) !!}
            <button class="plus" type="submit">+</button>
        </div>
    </td>
    <td class="center">

        @if($product['price'] > 0)
            <div class="big price {{ ($product['price_old'] > 0 ? 'new' : '') }} uk-margin-small">
                <span>{{ format_price($product['price']) }}</span>
                {!! config('site.price.currency') !!}
            </div>
        @endif

        @if($product['price_old'] > 0)
            <div class="uk-text-muted price old">{{ format_price($product['price_old']) }} {!! config('site.price.currency') !!}</div>
        @endif

    </td>
    <td class="center">
        <div class="big price uk-margin-small">
            <span data-total-price="{{ $key }}">{{ format_price($product['cost']) }}</span>
            {!! config('site.price.currency') !!}
        </div>
    </td>
    <td class="center">
        <button class="remove-cart" data-product="{{ $key  }}" type="button">x</button>
    </td>
</tr>

@if(!empty($product['modules']))
    <tr class="cart-modules-row-holder" data-modules-row-holder="{{ $key }}">
        <td class="image"></td>
        <td colspan="4" class="cart-modules-row">
            <div class="cart-modules" data-modules="{{ $key }}">
                <table>
                    @foreach($product['modules'] as $key => $m)
                        @include('frontend.cart.partials.module', ['product' => $m])
                    @endforeach
                </table>
            </div>
        </td>
        <td></td>
    </tr>
@endif
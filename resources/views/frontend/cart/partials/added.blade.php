<h3 class="uk-text-center">Товар успешно добавлен в корзину</h3>

<div class="uk-grid">
    @if($product['model']->image)
    <div class="uk-width-1-2">
        <img src="{{ img_src($product['image'], 'cart-added') }}" />
    </div>
    @endif

    <div class="uk-width-1-{{ ($product['image'] ? '2' : '1') }} product-view">
        <h4>{{ $product['title'] }}</h4>

        <div class="uk-margin-top">
            @if($product['price'] > 0)
                <div class="price {{ ($product['price_old'] > 0 ? 'new' : '') }}">
                    <span class="digit">{{ format_price($product['price']) }}</span>
                    <span class="currency">{!! config('site.price.currency') !!}</span>
                </div>
            @endif

            @if($product['price_old'] > 0)
                <div class="price-old">
                    <span class="digit">{{ format_price($product['price_old']) }}</span>
                    <span class="currency">{!! config('site.price.currency') !!}</span>
                </div>
            @endif
        </div>

        <div class="small-colors uk-margin-top">
            @include('frontend.product.partials.color-box')
        </div>

        <a href="{{ route('cart.index') }}" class="uk-width-1-1 uk-margin-top uk-button uk-button-large uk-button-primary">Оформить заказ</a>
    </div>
</div>
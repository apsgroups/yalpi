<div class="uk-form-row">
    <div class="uk-text-center">
        <a class="toggle-btn"
            data-target="#comment-block"
            data-hide="Скрыть коммментарий"
            data-show="Добавить комментарий"
            href="#"
            >Добавить комментарий</a>
    </div>

    <div id="comment-block" class="hidden">
        <label class="uk-form-label" id="comment">Комментарий</label>
        <div class="uk-form-controls">
            {!! Form::textarea('comment', null, ['id' => 'comment', 'rowspan' => 3]) !!}
        </div>
    </div>
</div>
<div class="uk-form-row uk-margin-bottom">
    <label class="uk-form-label" for="name" id="name">Ваше имя*</label>
    <div class="uk-form-controls">
        {!! Form::text('name', null, ['id' => 'name', 'class' => 'required']) !!}
    </div>
</div>

<div class="uk-grid uk-margin-top">
    <div class="uk-width-1-2">
        <div class="uk-form-row">
            <label class="uk-form-label" for="phone" id="phone">Телефон*</label>
            <div class="uk-form-controls">
                {!! Form::text('phone', null, ['id' => 'phone', 'class' => 'phone-mask required']) !!}
            </div>
        </div>
    </div>

    <div class="uk-width-1-2">
        <div class="uk-form-row">
            <label class="uk-form-label" for="email" id="email">E-mail*</label>
            <div class="uk-form-controls">
                {!! Form::text('email', null, ['id' => 'email', 'class' => 'email required']) !!}
            </div>
        </div>
    </div>
</div>

{!! Form::hidden('client_id', null, ['id' => 'client_id']) !!}
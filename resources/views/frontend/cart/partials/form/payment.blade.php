@php($paymentId = config('site.checkout.default.payment_id'))

<div class="uk-form-row payment-row">
    <h4>Способы оплаты</h4>

    <div class="uk-form-controls-condensed">
        <div class="uk-grid">
            <div class="uk-width-1-2">
                <div class="radio-switcher">
                    <label for="payment-id-1" data-toggle="payment-info" data-input="#payment_id" data-value="1" class="{{ ($paymentId === 1 ? 'active' : '') }}">
                        Наличными
                    </label>

                    <label for="payment-id-2" data-toggle="payment-info" data-input="#payment_id" data-value="2" class="{{ ($paymentId === 2 ? 'active' : '') }}">
                        Картой
                    </label>

                    {!! Form::hidden('payment_id', $paymentId, ['id' => 'payment_id']) !!}
                </div>
            </div>

            <div class="uk-width-1-2">
                <div class="payment-info item-1 small {{ ($paymentId !== 1 ? 'hidden' : '') }}">
                    Оплата при получении товара
                </div>

                <div class="payment-info item-2 small {{ ($paymentId !== 2 ? 'hidden' : '') }}">
                    Оплатить банковской картой можно с помощью интернет-банка или в магазине через терминал.
                </div>
            </div>
        </div>
    </div>
</div>
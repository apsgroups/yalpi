@php($shippingId = config('site.checkout.default.shipping_id'))

<div class="uk-form-row shipping-row">
    <h4>Доставка</h4>

    <div class="uk-form-controls-condensed">

        <div class="uk-grid">
            <div class="uk-width-1-2">
                <div class="radio-switcher">
                    <label for="shipping-id-1" data-toggle="shipping-info" data-input="#shipping_id" data-value="1" class="{{ ($shippingId === 1 ? 'active' : '') }}">
                        Нужна доставка
                    </label>

                    <label for="shipping-id-2" data-toggle="shipping-info" data-input="#shipping_id" data-value="2" class="{{ ($shippingId === 2 ? 'active' : '') }}">
                        Заберу сам
                    </label>

                    {!! Form::hidden('shipping_id', $shippingId, ['id' => 'shipping_id']) !!}
                </div>
            </div>
        </div>
    </div>

    <div class="shipping-info item-1 {{ ($shippingId !== 1 ? 'hidden' : '') }}">
        <div class="uk-margin-top">
            <label>Адрес доставки</label>
        </div>

        <div class="uk-grid">
            <div class="uk-width-3-10">
                {!! Form::text('city', null, ['id' => 'city', 'placeholder' => 'Город']) !!}
            </div>

            <div class="uk-width-3-10">
                {!! Form::text('street', null, ['id' => 'street', 'placeholder' => 'Улица']) !!}
            </div>

            <div class="uk-width-2-10">
                {!! Form::text('home', null, ['id' => 'home', 'placeholder' => 'Дом']) !!}
            </div>

            <div class="uk-width-2-10">
                {!! Form::text('apartment', null, ['id' => 'apartment', 'placeholder' => 'Квартира']) !!}
            </div>
        </div>
    </div>
</div>
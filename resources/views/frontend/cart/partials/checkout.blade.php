<div class="checkout-form">

    <div class="uk-grid">
        <div class="uk-width-1-4"> </div>

        <div class="uk-width-2-4">
            <h3 class="uppercase uk-text-center uk-heading-large">Оформление заказа</h3>
        </div>

        <div class="uk-width-1-4"> </div>

        <div class="uk-width-1-4"> </div>

        <div class="uk-width-2-4">
            {!! Form::open(['method' => 'post', 'route' => 'cart.checkout', 'class' => 'uk-form', 'id' => 'checkout-form']) !!}
            <div class="block-area fieldset">
                <div class="uk-form-controls uk-form-controls-text">
                    @include('frontend.cart.partials.form.personal')
                    @include('frontend.cart.partials.form.payment')
                    @include('frontend.cart.partials.form.shipping')
                    @include('frontend.cart.partials.form.comment')
                </div>
            </div>

            @include('frontend.feedback.partials.accept', ['acceptTxt' => 'Оформить заказ'])

            <div class="uk-form-row uk-margin-top uk-text-center">
                <div class="uk-margin-top">
                    <button class="uk-button uk-button-large uk-button-primary uk-margin btn-checkout" type="submit" data-goal="oformzak">Оформить заказ</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>


        <div class="uk-width-1-4">
            <div id="time-reserve" data-remein="1800">
                <div>Товар зарезервирован для Вас на:</div>

                <span class="remain">
                    <span class="minutes">00</span> мин
                    <span class="sec">00</span> сек
                </span>
            </div>
        </div>
    </div>
</div>
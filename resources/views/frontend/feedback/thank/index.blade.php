@extends('frontend.layouts.master')

@include('frontend.includes.seometa_group', ['group' => 'thank'])

@section('content')

@include('frontend.feedback.partials.menu')

<h1 class="uppercase uk-text-center">Оставить благодарность</h1>

<p class="uk-text-center">
    Всем нравится, когда их благодарят, и наша компания не исключение.
    Лучшим показателем качества нашей работы служат ваши благодарности и положительные
    эмоции в адрес нашей компании. Для нас важно каждое мнение!
</p>

<div class="uk-grid">
    <div class="uk-container-center uk-width-1-3">
        <div class="fieldset block-area">
            {!! Form::open(['route' => ['feedback.thank'], 'method' => 'post', 'class' => 'validate-form uk-form thank']) !!}
                <div class="uk-form-row">
                    <label class="uk-form-label" for="name">Имя*</label>
                    <div class="uk-form-controls">
                        {!! Form::text('name', null, ['id' => 'name', 'class' => 'required']) !!}
                    </div>
                </div>

                <div class="uk-form-row">
                    <label class="uk-form-label" for="email">Электронная почта*</label>
                    <div class="uk-form-controls">
                        {!! Form::text('email', null, ['id' => 'email', 'class' => 'required email']) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label" for="order_number">Номер заказа*</label>
                    <div class="uk-form-controls">
                        {!! Form::text('order_number', null, ['id' => 'order_number', 'class' => 'required']) !!}
                    </div>
                </div>


                <div class="uk-form-row">
                    <label class="uk-form-label" for="message">Сообщение*</label>
                    <div class="uk-form-controls">
                        {!! Form::textarea('message', null, ['id' => 'message', 'class' => 'required']) !!}
                    </div>
                </div>

                <?php $acceptTxt = 'Поблагодарить :)'; ?>

                @include('frontend.feedback.partials.accept', ['acceptTxt' => $acceptTxt])

                <div class="uk-form-row uk-text-center uk-margin-top">
                    <button type="submit" class="uk-button uk-button-large uk-button-primary big">Поблагодарить :)</button>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection
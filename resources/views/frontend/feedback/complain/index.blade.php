@extends('frontend.layouts.master')

@include('frontend.includes.seometa_group', ['group' => 'complain'])

@section('content')
@include('frontend.feedback.partials.menu')

<h1 class="uppercase uk-text-center">Оставить жалобу</h1>

<p class="uk-text-center">
    В нашей компании работают живые люди, поэтому мы не застрахованы от ошибок или форсмажорных обстоятельств.
    Но вы всегда можете уведомить нас об любом прошествии, связанным с некачественной работой наших сотрудников.
    По каждому сообщению будет проведена проверка и ни один факт нарушений не останется без внимания.
</p>

<div class="uk-grid">
    <div class="uk-container-center uk-width-1-3">
        <div class="fieldset block-area">
            {!! Form::open(['route' => ['feedback.complain'], 'method' => 'post', 'class' => 'validate-form uk-form complain']) !!}
                <div class="uk-form-row">
                    <label class="uk-form-label" for="name">Имя*</label>
                    <div class="uk-form-controls">
                        {!! Form::text('name', null, ['id' => 'name', 'class' => 'required']) !!}
                    </div>
                </div>

                <div class="uk-form-row">
                    <label class="uk-form-label" for="email">Электронная почта*</label>
                    <div class="uk-form-controls">
                        {!! Form::text('email', null, ['id' => 'email', 'class' => 'required email']) !!}
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label" for="order_number">Номер заказа*</label>
                    <div class="uk-form-controls">
                        {!! Form::text('order_number', null, ['id' => 'order_number', 'class' => 'required']) !!}
                    </div>
                </div>


                <div class="uk-form-row">
                    <label class="uk-form-label" for="message">Суть проблемы*</label>
                    <div class="uk-form-controls">
                        {!! Form::textarea('message', null, ['id' => 'message', 'class' => 'required']) !!}
                    </div>
                </div>

                <?php $acceptTxt = 'Оставить жалобу :('; ?>

                @include('frontend.feedback.partials.accept', ['acceptTxt' => $acceptTxt])

                <div class="uk-form-row uk-text-center uk-margin-top">
                    <button type="submit" class="uk-button uk-button-large uk-button-primary uk-width-1-1">Оставить жалобу :(</button>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection
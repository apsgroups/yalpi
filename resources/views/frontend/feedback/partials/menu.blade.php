<ul class="tab-menu">
    <li class="{{ Active::route('feedback.complain') }}">
        {!! link_to_route('feedback.complain', 'Пожаловаться') !!}
    </li>

    <li class="{{ Active::route('feedback.thank') }}">
        {!! link_to_route('feedback.thank', 'Поблагодарить') !!}
    </li>
</ul>
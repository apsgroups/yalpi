<div class="uk-form-row uk-margin-top">
    <label class="uk-form-label uk-text-muted" style="font-size: 12px" for="accept">
        {!! Form::checkbox('accept', 1, 1, ['id' => 'accept', 'class' => 'required']) !!}
        Нажимая кнопку «{{ (isset($acceptTxt) ? $acceptTxt : 'Отправить') }}» вы даете <a href="/uploads/media/polozhenie-o-konfidencialnosti-personalnyh-dannyh.pdf" target="_blank">согласие на обработку персональных данных</a>. 
    </label>
</div> 
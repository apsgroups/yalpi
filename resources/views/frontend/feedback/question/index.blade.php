@include('frontend.includes.seometa_group', ['group' => 'question'])

{!! Form::open(['route' => ['feedback.question'], 'method' => 'post', 'class' => 'validate-form ajax-form uk-form']) !!}
    <h3 class="uk-text-center uk-margin-top-remove uppercase">Задайте свой вопрос</h3>

    <p class="uk-text-center">У Вас остались вопросы? Задайте их нашему менеджеру!</p>

    <div class="uk-form-row">
        <label class="uk-form-label" for="name">Ваше имя*</label>
        <div class="uk-form-controls">
            {!! Form::text('name', null, ['id' => 'name', 'class' => 'uk-form-width-large required']) !!}
        </div>
    </div>

    <div class="uk-form-row">
        <label class="uk-form-label" for="email">E-mail*</label>
        <div class="uk-form-controls">
            {!! Form::text('email', null, ['id' => 'email', 'class' => 'uk-form-width-large email required']) !!}
        </div>
    </div>

    <div class="uk-form-row">
        <label class="uk-form-label" for="phone">Номер телефона*</label>
        <div class="uk-form-controls">
            {!! Form::text('phone', null, ['id' => 'phone', 'class' => 'uk-form-width-large phone-mask required']) !!}
        </div>
    </div>

    <div class="uk-form-row">
        <label class="uk-form-label" for="message">Вопрос*</label>
        <div class="uk-form-controls">
            {!! Form::textarea('message', null, ['id' => 'message', 'class' => 'uk-form-width-large required', 'rows' => 4]) !!}
        </div>
    </div>

    @include('frontend.feedback.partials.accept')

    <div class="uk-form-row uk-text-center">
        <button type="submit" id="send-question" class="uk-margin-top uk-button uk-button-large uk-button-primary">Задать вопрос</button>
    </div>
{!! Form::close() !!}
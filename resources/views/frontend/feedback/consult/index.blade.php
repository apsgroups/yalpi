@include('frontend.includes.seometa_group', ['group' => 'consult'])

{!! Form::open(['route' => ['feedback.consult'], 'method' => 'post', 'class' => 'validate-form ajax-form uk-form']) !!}

<h1 class="uk-h3 uk-text-center uk-margin-top-remove uppercase">Получить консультацию</h1>

<p class="uk-text-center">Введите имя и телефон чтобы получить консультацию. Мы свяжемся с вами в ближайшее время.</p>

@if($product)
<div class="uk-grid">
    @if($product->image)
    <div class="uk-width-1-2">
        <img src="{{ img_src($product->image) }}" />
    </div>
    @endif

    <div class="product-view uk-width-1-{{ ($product->image ? '2' : '1') }}">
        <h4>{{ $product->title }}</h4>

        @if($product->price > 0)
            <div class="price {{ ($product->price_old > 0 ? 'new' : '') }}">
                <span class="digit">{{ format_price($product->price) }}</span>
                <span class="currency">{!! config('site.price.currency') !!}</span>
            </div>
        @endif

        @if($product->price_old > 0)
            <div class="price-old">
                <span class="digit">{{ format_price($product->price_old) }}</span>
                <span class="currency">{!! config('site.price.currency') !!}</span>
            </div>
        @endif
@endif

        <div class="uk-margin-top">
            <div class="uk-form-row">
                <div>
                    <label class="uk-form-label" for="name">Имя*</label>
                </div>
                <div class="uk-form-controls">
                    {!! Form::text('name', null, ['id' => 'name', 'class' => 'uk-form-width-large required']) !!}
                </div>
            </div>

            <div class="uk-form-row">
                <div>
                    <label class="uk-form-label" for="phone">Телефон*</label>
                </div>
                <div class="uk-form-controls">
                    {!! Form::text('phone', null, ['id' => 'phone', 'class' => 'phone-mask uk-form-width-large required']) !!}
                </div>
            </div>

            @include('frontend.feedback.partials.accept') 

            <div class="uk-form-row uk-text-center">
                <button type="submit" id="send-consult" class="uk-margin-top uk-button uk-button-large uk-button-primary uk-width-1-1" data-goal="konsult2">Отправить</button>
            </div>
        </div>

@if($product)
            {!! Form::hidden('product_id', $product->id) !!}
    </div>
</div>
@endif
{!! Form::close() !!}


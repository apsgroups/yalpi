@include('frontend.includes.seometa_group', ['group' => 'cost'])

{!! Form::open(['files' => true, 'route' => ['feedback.proposal'], 'method' => 'post', 'class' => 'validate-form ajax-form uk-form']) !!}
    <h1 class="uk-text-center uppercase uk-h3">Что можно улучшить на сайте?</h1>

    <div class="uk-form-row">
        <label class="uk-form-label" for="name">Ваше имя*</label>
        <div class="uk-form-controls">
            {!! Form::text('name', null, ['id' => 'name', 'class' => 'uk-form-width-large required']) !!}
        </div>
    </div>

    <div class="uk-form-row">
        <label class="uk-form-label" for="email">E-mail</label>
        <div class="uk-form-controls">
            {!! Form::text('email', null, ['id' => 'email', 'class' => 'uk-form-width-large email required']) !!}
        </div>
    </div>

    <div class="uk-form-row">
        <label class="uk-form-label" for="message">Сообщение</label>
        <div class="uk-form-controls">
            {!! Form::textarea('message', null, ['id' => 'message', 'class' => 'uk-form-width-large required', 'style' => 'height: 75px']) !!}
        </div>
    </div>

    <div class="uk-form-row">
        <div class="uk-form-controls">
            <input type="file" name="files[]" id="files" class="inputfile maxfiles-validate" accept="image/*" multiple />
            <label for="files" class="uk-button uk-button-primary"><i class="uk-icon-paperclip"></i> Прикрепить скриншот</label>
            <div class="inputfile-list uk-text-muted uk-text-small uk-margin-top"></div>
        </div>
    </div>

    @include('frontend.feedback.partials.accept', ['acceptTxt' => 'Отправить'])

    <div class="uk-form-row uk-text-center" style="margin-top: 20px">
        <button type="submit" class="uk-button uk-button-large uk-button-primary uk-width-1-1">Отправить</button>
    </div>
{!! Form::close() !!}
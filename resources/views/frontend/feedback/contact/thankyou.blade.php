<h3 class="uk-text-center uppercase">Спасибо, ваша заявка №{{ $feedback->id }} принята!</h3>

<div class="uk-text-center">
    <p>В {!! worktime_msg() !!} наш менеджер свяжется с Вами.</p>
</div>
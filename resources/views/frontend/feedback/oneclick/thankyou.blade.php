<div class="uk-text-center">
    <h3 class="uppercase uk-text-center">Ваш заказ оформлен!</h3>

    <p>
        Спасибо за заказ, менеджер свяжется с Вами в {!! worktime_msg() !!}.
    </p>

    <p>
        <a href="{{ route('frontend.index') }}" class="uk-margin-top uk-button uk-button-large uk-button-primary">на главную</a>
    </p>
</div>
{!! Form::open(['route' => ['feedback.oneclick', $product->id], 'method' => 'post', 'class' => 'validate-form ajax-form uk-form']) !!}

<h3 class="uk-text-center uk-margin-top-remove uppercase">Купить в 1 клик</h3>

<div class="uk-grid">
    @if($product->image)
    <div class="uk-width-1-2">
        <img src="{{ img_src($product->image, 'cart-added') }}" />

        <div class="uk-margin-top uk-text-muted uk-text-center">
            Мы свяжемся с Вами для <br />уточнения деталей заказа
        </div>
    </div>
    @endif

    <div class="product-view uk-width-1-{{ ($product->image ? '2' : '1') }}">
        <h4>{{ $product->title }}</h4>

        <div class="uk-margin-bottom">
            @if($product->price > 0)
                <div class="price {{ ($product->price_old > 0 ? 'new' : '') }}">
                    <span class="digit">{{ format_price($product->price) }}</span>
                    <span class="currency">{!! config('site.price.currency') !!}</span>
                </div>
            @endif

            @if($product->price_old > 0)
                <div class="price-old">
                    <span class="digit">{{ format_price($product->price_old) }}</span>
                    <span class="currency">{!! config('site.price.currency') !!}</span>
                </div>
            @endif
        </div>

        @if($colors)
            <div class="small-colors">
                @include('frontend.product.partials.color-box')

                @foreach($colors as $color)
                    {!! Form::hidden('properties[]', $color['active']) !!}
                @endforeach
            </div>
        @endif

        <div class="uk-margin-top">
            <div class="uk-form-row">
                <div>
                    <label class="uk-form-label" for="name">Имя*</label>
                </div>
                <div class="uk-form-controls">
                    {!! Form::text('name', null, ['id' => 'name', 'class' => 'uk-form-width-large uk-form-width-large required']) !!}
                </div>
            </div>

            <div class="uk-form-row">
                <div>
                    <label class="uk-form-label" for="phone">Телефон*</label>
                </div>
                <div class="uk-form-controls">
                    {!! Form::text('phone', null, ['id' => 'phone', 'class' => 'phone-mask uk-form-width-large required']) !!}
                </div>
            </div>

        </div>
    </div>

    @include('frontend.feedback.partials.accept')

    <div class="uk-width-1-1 uk-margin-top uk-text-center">
        <button type="submit" class="uk-button uk-button-large uk-button-primary uk-width-1-2" data-goal="kupklik2">Отправить</button>
    </div>
</div>
{!! Form::close() !!}


@extends('frontend.layouts.master')

@include('frontend.includes.seometa_group', ['group' => 'catalog'])

@section('content')
    <div class="catalog-loader">
        <ul data-uk-switcher="{connect:'#catalog-tabs'}" class="catalog-nav">
            <li>
                <a href="{{ route('frontend.category.catalog') }}#vitrina" id="showcase-btn">
                    Витрина
                </a>
                Новинки, скидки, рекомендации
            </li>
            <li class="uk-active">
                <a href="{{ route('frontend.category.catalog') }}" id="catalog-btn">
                Каталог
                </a>
                Более 3 000 товаров
            </li>
        </ul>

        <ul id="catalog-tabs" class="uk-switcher">
            <li>
                @widget('CatalogWidget', ['type' => 'catalog'])
            </li>
            <li class="uk-active">
                <h1 class="uk-text-center uppercase uk-margin-large-bottom">Каталог</h1>
                @widget('menuWidget', ['menu' => 'catalog', 'layout' => 'showcase', 'only_showcase' => 1])
            </li>
        </ul>

        <div class="loader"><i class="uk-icon-spinner uk-icon-spin"></i></div>
    </div>

        @widget('Product.PromoWidget')
@stop

@section('subscribe')
    @include('frontend.includes.partials.subscribe')
@endsection
@if(view()->exists('frontend.category.partials.children.'.$category->child_template))
    @include('frontend.category.partials.children.'.$category->child_template)
@else
    @include('frontend.category.partials.children.default')
@endif
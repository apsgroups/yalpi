@extends('frontend.layouts.master')

@section('title', $meta->title().($pagination->currentPage() > 1 ? ' | Страница '.$pagination->currentPage() : ''))

@include('frontend.includes.seometa', ['model' => $category])

@section('breadcrumbs', Breadcrumbs::render('frontend.category', $category)) 

@include('frontend.category.partials.opengraph')

@section('body-attr', 'class="grey-bg"')

@section('content')
<div>
    <div class="uk-container uk-container-center">
        <div class="uk-clearfix category-heading-inner">
            <h1 class="category-heading page-heading uk-float-left">
                {!! $meta->heading() !!}
            </h1>

            @if($categoryTotal)
                <div class="category-total uk-float-left">{{ $categoryTotal }} {{ total_products_morph($categoryTotal) }}</div>
            @endif
        </div>
    </div>
</div>

<?php
$deepLvl = isset($category) && $category->parent && $category->parent->parent_id > 0;
?>
<div class="category-bg">
    <div class="uk-container uk-container-center">
        @if(!$deepLvl)
            @include('frontend.category.partials.moneyback')

            {!! Widgets::position('category-text') !!}

            @if($pagination->currentPage() === 1)
                @widget('menuWidget', ['menu' => 'catalog-homepage', 'layout' => 'catalog-children', 'product_count' => true])
            @endif
        @endif

        @include('frontend.category.partials.children.tags')

        @if($deepLvl)
        <div class="sidebar-content">
            <div class="sidebar">
                @widget('menuWidget', ['menu' => 'catalog-homepage', 'layout' => 'sidebar-categories', 'product_count' => true])
            </div>
            <div class="main-content">
        @endif

            @include('frontend.category.partials.filters.helper.index')

            @if(!$category->hide_products)
            <div id="filter-results" class="block-area margin">
                @include('frontend.category.partials.products', ['itemPropUrl' => $category->link()])
            </div>
            @endif

        @if($deepLvl)
            </div>
        </div>
        @endif
    </div>

    <div class="white-bg">
        <div class="uk-container uk-container-center">
            @include('frontend.category.partials.advantages')
        </div>
    </div>

    <div class="uk-container uk-container-center">
        @if(isset($meta) && !empty($pagination) && $pagination->currentPage() === 1 && !empty($category))
        <div class="loadmore-hide">
            @if(!empty($category) && $category->info_enabled)
                @if($videos = $category->getInfoVids())
                    @include('frontend.category.partials.info.video')
                @endif
                
                @if($articles = $category->getInfoContent())
                    @include('frontend.category.partials.info.articles')
                @endif

                @if($faq = $category->getInfoContent('faq'))
                    @include('frontend.category.partials.info.faq')
                @endif

                @if(!empty($category->info_reviews))
                    @include('frontend.category.partials.info.reviews')
                @endif

                <div class="block-desc">{!! resize_text_img($meta->text()) !!}</div>
            @else
                <div class="block-desc">
                    {!! resize_text_img($meta->text()) !!}
                </div>
            @endif
        </div>
        @endif
    </div>
</div>
@stop

{{--Remove the canonical tag from the subcategory pagination (temporarily)--}}
@if($pagination->currentPage() > 1 && !$category->parent_id)
@push('head')
<link rel="canonical" href="{{ $category->link(true) }}" />
@endpush
@endif
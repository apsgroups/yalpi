<div class="uk-position-relative uk-nbfc">
    <a href="{{ $category->link() }}" class="uk-display-block animation-scale">
        <img src="{{ img_src($category->image, $imageName) }}" alt="{{ $category->title }}" />

        <span class="category-info">
            <span class="title">{{ $category->title }}</span>

            <span class="models-info">
                <span class="total-products">{{ $category->products_count }}</span>
                <span class="anchor">посмотреть все <i class="uk-icon-long-arrow-right"></i></span>
            </span>
        </span>
    </a>
</div>
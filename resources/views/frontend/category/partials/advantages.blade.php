<noindex>
    <div class="advantages category-advantages">
        <div class="block-title">Преимущества он-лайн обучения в Yalpi</div>

        <div class="uk-grid">
            <div class="uk-width-1-3">
                <div class="item">
                    <div class="img"><img src="/images/layout/advantages/1.png" alt="Учитесь быстро!"></div>
                    <div class="text">
                        <div class="title">Учитесь быстро!</div>
                        <div class="desc">Обучайтесь формате небольших мастер-классов и интенсивов отпреподавателей со всего мира.  </div>
                    </div>
                </div>
            </div>
            <div class="uk-width-1-3">
                <div class="item">
                    <div class="img"><img src="/images/layout/advantages/2.png" alt="Ищите практиков"></div>
                    <div class="text">
                        <div class="title">Ищите практиков</div>
                        <div class="desc">Рейтинги, отзывы и подборки помогут определить тех преподавателей у которых хочется учиться. </div>
                    </div>
                </div>
            </div>
            <div class="uk-width-1-3">
                <div class="item">
                    <div class="img"><img src="/images/layout/advantages/3.png" alt="Будьте в тренде!"></div>
                    <div class="text">
                        <div class="title">Будьте в тренде!</div>
                        <div class="desc">Следите за актуальными темами в вашей профессии, добавляйте уроки в избранное и учитесь в своем темпе.
</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</noindex>
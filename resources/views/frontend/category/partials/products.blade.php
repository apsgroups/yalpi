@php($compositions = isset($compositions) ? $compositions : collect([]))

@php($filterLayout = isset($category) && strpos($category->link(), '/kukhni/') !== 0)

@if(count($products))
    <div class="uk-clearfix">
        <div class="uk-float-left">
            @include('frontend.category.partials.sorting')
        </div>

        <div class="uk-float-right">
            <div class="pagination-short">
                @include('frontend.category.partials.per-page')
            </div>
        </div>
    </div>
    
    <div class="products-grid-block" itemprop="hasOfferCatalog" itemscope itemtype="https://schema.org/OfferCatalog">
        @include('frontend.category.partials.layouts.'.$layoutService->getLayout())
    </div>

    <div class="uk-position-relative">
        @if($pagination && $pagination->hasMorePages() && $pagination->currentPage() < $pagination->lastPage())
        <div class="loadmore-block uk-margin-large-top">
            @include('frontend.category.partials.loadmore')
        </div>
        @endif

        @if($pagination)
            <div class="pagination-box uk-clearfix">
                @include('frontend.category.partials.pagination')
            </div>
        @endif
    </div>

    <div class="loadmore-hide">
        @include('frontend.category.partials.tags')
    </div>

    <div class="uk-clearfix"></div>
@else
    @if(!request('filter') && request()->is('rasprodazhi*'))
    <p class="uk-text-center">
        Уважаемые клиенты! На данный момент товаров данной категории нет в распродаже. Приносим свои извинения.<br />
        Вы можете посмотреть все товары со скидкой в общем разделе <a href="/rasprodazhi/"><strong>"Распродажа"</strong></a>
    </p>
    @else
    <p class="uk-text-center">Товаров по заданным критериям нет</p>
    @endif
@endif
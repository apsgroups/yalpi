{!! Form::open(['url' => $filterUrl, 'class' => 'uk-form uk-form-horizontal filter', 'id' => 'filter-form', 'method' => 'get']) !!}
	<div class="category-helper">
		@if($deepLvl)
			<div class="text sm">Какой урок выбрать? <span>Помогу подобрать.</span></div>
		@else
			<div class="text">Не уверен, какой урок выбрать? Помогу подобрать.</div>
		@endif
		
		@if(request('filter') || count($products))
			<noindex>
				<div class="uk-clearfix" id="filter-box">
					<div class="switchers">
						<div class="filter-toggle uk-hidden" data-id="ext">{{ ($category->filter_button ? $category->filter_button : 'Что изучаем?') }}</div>
						<div class="filter-toggle uk-hidden" data-id="teaching">Профессия</div>
						<div class="filter-toggle uk-hidden" data-id="rate">Рейтинг</div>
					</div>
				</div>
			</noindex>
		@endif
	</div>

	<div id="ext-filter" class="uk-hidden uk-clearfix filter-box">
		@if($category->primaryProperty && !empty($options[$category->primaryProperty->slug]))
			<div id="primary-property"> </div>
		@endif

		@if($category->secondaryProperty && !empty($options[$category->secondaryProperty->slug]))
			<div id="secondary-property"> </div>
		@endif

		<div id="ext-property"> </div>
	</div>

	<div id="teaching-filter" class="uk-hidden uk-clearfix filter-box">
		<div id="teaching-property"></div>
		<div id="language-property"></div>
	</div>

	<div id="rate-filter" class="uk-hidden uk-clearfix filter-box">
		<div id="rate-property"></div>
	</div>

	<div class="filter-loader"></div>

	{!! Form::hidden('category_id', $category->id, ['id' => 'filter-category-id']) !!}
{!! Form::close() !!}
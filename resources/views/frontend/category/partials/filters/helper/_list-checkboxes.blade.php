@inject('listProperty', '\App\Services\Filter\Html\ListProperty')

@php($property = \App\Models\Property::where('slug', $name)->first())

@if(!$property || empty($options[$name]['buckets']) || (count($options[$name]['buckets']) <= 1 && empty($showSingle)))
<?php return ?>
@endif

<div>
    <div class="uk-text-bold">{{ $property->title }}</div>

	@foreach($options[$name]['buckets'] as $k => $option)
        <div>
            @php($id = $name.'_'.$k)

            @php($attr = ['class' => 'checkbox', 'id' => $id])
            
            {!! Form::checkbox('filter['.$name.'][]', $option['key'], (isset($attributes[$name]) && in_array($option['key'], $attributes[$name])), $attr) !!}

            <label for="{{ $id }}">
                {{ $listProperty->title($option['key']) }}
            </label>
        </div>
    @endforeach
</div>
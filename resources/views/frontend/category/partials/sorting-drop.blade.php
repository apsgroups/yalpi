@if($sortUrls)
    <!--noindex-->
    <div class="sorting control-links uk-clearfix">
        @foreach(['popular', 'lower-price', 'higher-price'] as $attribute)
            <a href="{{ $sortUrls[$attribute] }}" class="{{ ($sortingService->isActive($attribute) ? 'active' : '') }}">
                @if($attribute === 'popular')
                    <span class="sort-icon-holder"><i class="sort-icon asc active"></i></span>
                @else
                    {!! $sortingService->getIconDirection($attribute) !!}
                @endif

                {{ trans('frontend.sorting.'.$attribute) }}
            </a>
        @endforeach
    </div>
    <!--/noindex-->
@endif
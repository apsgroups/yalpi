@inject('urlService', '\App\Services\Frontend\UrlService')

<div class="view-toggle uk-margin uk-margin-top">
    <a href="{{ $urlService::currentUrl($layoutService->getQuery('list')) }}" class="{{ ($layoutService->isActive('list') ? 'active' : '') }}">
        <i class="uk-icon-list-ul"></i>
    </a>

    <a href="{{ $urlService::currentUrl($layoutService->getQuery('grid')) }}" class="{{ ($layoutService->isActive('grid') ? 'active' : '') }}">
        <i class="uk-icon-th"></i>
    </a>
</div>
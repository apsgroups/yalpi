<?php

$meta = isset($meta) ? $meta : \App\Services\Meta\MetaFactory::getInstance($category);

$og = [
    'url' => $category->link(true),
    'title' => preg_replace('/"(.*?)"/', '«$1»', $meta->title()),
    'description' => preg_replace('/"(.*?)"/', '«$1»', $meta->description()),
    'type' => 'product.group',
];

$imageModel = $products->first();
	
if(!empty($imageModel->image)) {
	$og['image'] = url(img_src($imageModel->image, 'large'));
}
?>

@push('head')
    @include('frontend.includes.opengraph', compact('og'))
@endpush
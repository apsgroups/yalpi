@if($pagination && $pagination->hasMorePages() && $pagination->currentPage() < $pagination->lastPage())
    <button class="uk-button loadmore" data-url="{{ $pagination->nextPageUrl() }}">Показать ещё</button>
@endif
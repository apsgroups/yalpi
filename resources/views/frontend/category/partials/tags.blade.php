@if(isset($tags) && $tags->count())
	<nav class="category-tags">
	    <ul>
	        @foreach($tags as $tag)
	            <li><a href="{{ $tag->tagable->link() }}">{{ $tag->title }}</a></li>
	        @endforeach
	    </ul>
	</nav>
@endif
<div class="category-reviews">
    <div class="block-title">Отзывы учащихся</div>

    <div class="uk-grid">
    	@foreach($category->info_reviews as $review)
    		@continue($review['text'] === '')
        	<div class="uk-width-1-4">
        		@include('frontend.category.partials.info.review')
        	</div>
    	@endforeach
    </div>
</div>
<div class="info-nav">
	<div class="title"><span class="s-icon icon-info-video"></span> Видео</div>

	<ul>
		@foreach($videos as $video => $title)
			<li>
				@if(reset($videos) === $title)
					<a href="//www.youtube.com/embed/{{ $video }}?enablejsapi=1" class="video-img fancybox fancybox.iframe" data-fancybox-width="1099" data-fancybox-height="618" rel="nofollow">
				        <img src="{{ img_youtube($video, 'info-videos') }}" title="{{ $title }}" alt="{{ $title }}" />
				        <span class="play"></span>
				    </a>

				    @continue(true)
				@endif

				<a href="//www.youtube.com/embed/{{ $video }}?enablejsapi=1" class="fancybox fancybox.iframe" data-fancybox-width="1099" data-fancybox-height="618" rel="nofollow">{{ $title }}</a>
			</li>
		@endforeach
	</ul>
</div>
<div class="info-nav">
	<div class="title"><span class="s-icon icon-info-articles"></span> Статьи</div>

	<ul>
		@foreach($articles as $article)
			<li>
				<a href="{{ $article->link() }}">{{ $article->title }}</a>
			</li>
		@endforeach
	</ul>
</div>
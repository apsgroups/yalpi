<div class="info-nav">
	<div class="title"><span class="s-icon icon-info-question"></span> Часто задаваемые вопросы</div>

	<ul>
		@foreach($faq as $article)
			<li>
				<a href="/voprosy-i-otvety/">{{ $article->title }}</a>
			</li>
		@endforeach
	</ul>
</div>
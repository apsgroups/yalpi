<div class="uk-h1">Полезная информация</div>

<div class="uk-grid">
	<div class="uk-width-1-2">
		<div class="text-info uk-margin-top">
			Современная кухня совмещает в себе простоту, свежий  привлекательный внешний вид и, разумеется, удобство  и многофункционально
		</div>

		<div class="uk-margin-large-top">
		</div>
	</div>

	<div class="uk-width-1-2 uk-clearfix">
		<div class="info-nav-clmn">
			@include('frontend.category.partials.info.video', ['videos' => ['McsPFjBhAb8' => 'Кухня Шале - купить в Москве', 'RrbOAfWEyEs' => 'Кухня Фьюжн - купить в Москве', '2cXzJKXNgHc' => 'Кухня Сити - купить в Москве']])
			
			@include('frontend.category.partials.info.articles', ['articles' => \App\Models\Content::ofType('articles')->published(1)->orderBy('created_at', 'desc')->take(5)->get()])

			@include('frontend.category.partials.info.faq')
			
			@include('frontend.category.partials.info.reviews')
		</div>
	</div>
</div>
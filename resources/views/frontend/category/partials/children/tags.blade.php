@if($children->count() > 0)
	<nav class="category-children-tags category-tags">
	    <ul>
	        @foreach($children as $child)
	            <li><a href="{{ category_url($child) }}">{{ $child->getChildTitle($category) }}</a></li>
	        @endforeach
	    </ul>
	</nav>
@endif
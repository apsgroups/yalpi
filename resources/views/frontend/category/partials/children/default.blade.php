@if($category->children->count() > 0)
    <div class="category-children">
        @foreach($category->children as $c)
            <div class="item">
                <div class="category-desc-tooltip" data-id="{{ $c->id }}">
                    <div class="img">
                        <a href="{{ $c->link() }}"><img src="/uploads/category-children.svg" alt="{{ $c->title }}"></a>
                    </div>

                    <div class="level-score">
                        <div>Уровень сложности</div>

                        <div class="points">
                            @for($i=1; $i <= 5; $i++)
                                <i{!! ($c->level_score >= $i ? ' class="active"' : '') !!}></i>
                            @endfor
                        </div>
                    </div>

                    <div class="title">
                        <a href="{{ $c->link() }}">{{ $c->title }}</a>
                    </div>
                </div>

                @if($children = $c->children()->published(1)->orderBy('ordering', 'asc')->get())
                    <div class="show-children"></div>
                @endif
            </div>
        @endforeach
    </div>
@endif
@if($children->count() > 0)
    <div class="block-area margin">
        <div class="categories-grid uk-text-center uk-width-medium-1-1">
            <ul class="category-child-icons">
            @foreach($children as $child) 
                <li><a href="{{ category_url($child) }}" class="icon-holder" {!! ($child->id !== $category->id ? ' data-icon="'. $child->icon .'"' : '') !!}><i class="s-icon icon-{{ $child->icon.($child->id === $category->id ? '-active' : '') }} {!! (strpos($child->icon, 'zoom-') !== false ? 'zoom-cat-icon' : '') !!}"></i> <span class="{!! ($child->id === $category->id ? ' uk-text-bold' : '') !!} {!! (strpos($child->icon, 'zoom-') !== false ? 'zoom-cat-text' : '') !!}">{{ $child->getChildTitle($category) }}

                @if(strpos($child->icon, 'zoom-') !== false)
                <sup>{{ $child->products()->published(1)->count() }}</sup>
                @endif
                </span></a></li>
            @endforeach
            </ul>
        </div>
    </div>
@endif
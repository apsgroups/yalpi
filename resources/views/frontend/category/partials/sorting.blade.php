@if($sortUrls)
    <!--noindex-->
    <div class="sorting control-links uk-clearfix">
        <div class="lbl">Сортировать:</div> 

        <div class="links">
            @foreach($sortingService->getNames() as $attribute)
                <a href="{{ $sortUrls[$attribute] }}" class="{{ ($sortingService->isActive($attribute) ? 'active' : '') }}"><span>{{ trans('frontend.sorting.'.$attribute) }}</span></a>
            @endforeach
        </div>
    </div>
    <!--/noindex-->
@endif
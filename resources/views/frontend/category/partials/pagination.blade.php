{!! (new App\Pagination($pagination))->render() !!}

<div class="pagination-status">
    @php($currentTotalByPage = $pagination->currentPage() * $perPage)
    Вы просмотрели {{ ($currentTotalByPage > $pagination->total() ? $pagination->total() : $currentTotalByPage) }} из {{ $pagination->total() }} 
</div>
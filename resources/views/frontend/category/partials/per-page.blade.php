<noindex>
    <div class="limitbox control-links">
        <span class="lbl">Уроков на странице:</span>

        <span class="buttons">
        @foreach(config('site.perPage.limits.'.$perPageKey) as $limit => $text)
                <a href="{{ $perPageUrls[$limit] }}" class="{{ ($limit === intval($perPage) ? 'active' : '') }}">{{ $text }}</a>
            @endforeach
    </span>
    </div>
</noindex>

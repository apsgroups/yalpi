@foreach($products->chunk($inRow) as $items)
    <div class="uk-grid products-grid product-items">
      @include('frontend.category.partials.layouts.grid_products')
    </div>
@endforeach
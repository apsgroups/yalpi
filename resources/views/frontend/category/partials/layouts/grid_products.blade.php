@foreach($items as $k => $product)
    <div class="uk-width-medium-1-{{ $inRow }}" itemprop="itemListElement" itemscope itemtype="https://schema.org/Offer">
        @include('frontend.product.preview', ['linkToModule' => isset($moduleParent), 'inRow' => $inRow])
    </div>
@endforeach
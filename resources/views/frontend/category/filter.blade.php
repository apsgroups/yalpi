<?php

$show = false;
// $fields = ['style', 'placement', 'obshchij-cvet', 'door-count', 'height', 'width', 'depth'];

foreach($options as $v) {
    if(!empty($v['buckets'])) {
        $show = true;
        break;
    }
}
?>

@if((request('filter') || count($products)) && $show)
<div class="filter" id="filter-box">
    {!! Form::open(['url' => $filterUrl, 'class' => 'uk-form uk-form-horizontal', 'id' => 'filter-form', 'method' => 'get']) !!}
        <div class="uk-width-1-1">
            @if(!empty($options['style']['buckets']) && count($options['style']['buckets']) > 1)
            <div class="filter-field">
                @include('frontend.filter.list', ['title' => 'Стиль', 'name' => 'style'])
            </div>
            @endif
            
            @if(!empty($options['obshchij-cvet']['buckets']) && count($options['obshchij-cvet']['buckets']) > 1)
            <div class="filter-field">
                @include('frontend.filter.list', ['title' => 'Цвет', 'name' => 'obshchij-cvet'])
            </div>
            @endif

            @if(!empty($options['naznachenie']['buckets']) && count($options['naznachenie']['buckets']))
                <div class="filter-field">
                    @include('frontend.filter.list', ['title' => 'Назначение', 'name' => 'naznachenie', 'showSingle' => isset($category) && $category->id === 436])
                </div>
            @endif
        </div>

        {!! Form::hidden($context, $contextId) !!}
    {!! Form::close() !!}

    <div class="filter-loader"></div>
</div>
@endif

<div class="filter-field2"></div>
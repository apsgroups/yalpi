<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="_token" content="{{ csrf_token() }}" />

        <title>@yield('title', \Settings::get('page_title'))</title>

        <meta name="description" content="@yield('metadesc', \Settings::get('page_title'))">

        @hasSection('metakeys-off')
        @else
        <meta name="keywords" content="@yield('metakeys', \Settings::get('metakeys'))">
        @endif

        @include('includes.partials.yandex-verification')

        @include('includes.partials.favicon')

        <!-- Styles -->
        @yield('before-styles-end')

        <style>
            html {
                min-height: 101%;
                overflow-y: visible;
            }
        </style>

        {!! Html::style(elixir('css/frontend.css')) !!}

        @yield('after-styles-end')

        @stack('head')

        <meta name="viewport" content="width=1430">

        @include('includes.partials.google-site-verification')

        <!--[if lt IE 12]>
        <link rel="stylesheet" href="https://rawgit.com/codefucker/finalReject/master/reject/reject.css" media="all" />
        <script type="text/javascript" src="/js/reject.min.js" data-text="К сожалению, браузер, которым вы пользуйтесь, устарел, и не может нормально отображать сайт. Пожалуйста, скачайте любой из следующих браузеров:"></script>
        <![endif]-->

        <script>(window.Image ? (new Image()) : document.createElement('img')).src = 'https://vk.com/rtrg?p=VK-RTRG-222565-5i6oC';</script>

        @include('includes.vk')
        
        @include('includes.fb')

        @include('includes.gtm.head')
    </head>
    <body @yield('body-attr')>
        @include('includes.partials.metrika')

        @include('includes.gtm.body')

        @include('includes.partials.ga')

        <header class="@stack('header-css')">
            @include('frontend.includes.header.main')
        </header>

        <div id="content-container">
            @yield('breadcrumbs')

            @yield('content')
        </div>

        @yield('after-content')

        @include('frontend.includes.info')

        <div id="to-top"></div>

        <footer>
            @include('frontend.includes.footer.index')
        </footer>

        <!-- JavaScripts -->
        {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>--}}
        {{--<script>window.jQuery || document.write('<script src="{{asset('js/vendor/jquery/jquery-2.1.4.min.js')}}"><\/script>')</script>--}}
        {{--{!! Html::script('js/vendor/bootstrap/bootstrap.min.js') !!}--}}

        @yield('before-scripts-end')
        {!! Html::script(elixir('js/app.js')) !!}

        @stack('js-footer')

        @if(App::environment('production'))
        @include('includes.partials.livechat')
        @endif

        @yield('after-scripts-end')
    </body>
</html>
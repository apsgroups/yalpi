@extends('frontend.layouts.master')

@section('breadcrumbs', Breadcrumbs::render('frontend.search', $term))

@section('content')
    <h1 class="uk-text-center uppercase">
        @if(!empty($term))
        Результаты поиска по запросу: {{ $term }}
        @else
        Поиск по каталогу
        @endif
    </h1>
    
    <div class="uk-container uk-container-center">
        <div id="filter-results" class="block-area">
            @if(!empty($products))
                @include('frontend.category.partials.products')
            @elseif($term)
                @include('frontend.search.partials.empty_results')
            @endif
        </div>
    </div>
@endsection
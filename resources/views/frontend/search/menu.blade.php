<div id="autocomplete-results">
    <div class="menu-container start-search">
    	<div class="nav">
    		@widget('menuWidget', ['menu' => 'popular-areas', 'layout' => 'search', 'product_count' => 1])
    	</div>

    	<div class="nav">
			@widget('menuWidget', ['menu' => 'promising-areas', 'layout' => 'search', 'product_count' => 1])
		</div>
    </div>
</div>

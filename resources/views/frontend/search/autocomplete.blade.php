@if($categories->count() || $products->count() || $modules->count())
<div id="autocomplete-results">
    <div>
        <div class="menu-container">
            @if($categories->count())
                <div class="nav">
                    <div class="header star">Результаты поиска</div>
                    <ul>
                        @foreach($categories as $category)
                            <li>
                                <a href="{{ $category->link() }}">
                                    <span class="title">{!! highlight($category->title, $term) !!}</span>

                                    @if($category->product_count > 0)
                                        <span class="total">{{ $category->product_count.' '.total_products_morph($category->product_count) }}</span>
                                    @endif
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="nav">
                @widget('menuWidget', ['menu' => 'promising-areas', 'layout' => 'search', 'product_count' => 1, 'limit' => 6])

                <div class="more-inner">
                    <a href="{{ route('search.index', ['', 'term' => $term]) }}" class="uk-button action-btn">Все направления</a>
                </div>
            </div>
        </div>

        <div class="menu-container">
                @if($products->count())
                    <div class="nav products">
                        <div class="header star">Подходящие уроки</div>
                        <ul>
                            @foreach($products as $product)
                                <li>
                                    <a href="{{ $product->link() }}">
                                        <span class="category-title title">
                                            {!! highlight($product->category->title, $term) !!}
                                        </span>
                                        <span class="category-arrow">
                                            &rarr;
                                        </span>
                                        <span class="product-title">                                        
                                            {!! highlight($product->title, $term) !!}
                                        </span>
                                    </a>
                                </li>
                            @endforeach
                        </ul>

                        <div class="more-inner">
                            <a href="{{ route('search.index', ['', 'term' => $term]) }}" class="uk-button action-btn">Все уроки</a>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endif
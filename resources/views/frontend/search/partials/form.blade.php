{!! Form::open(['route' => 'search.index', 'id' => 'filter-form', 'class' => 'uk-form', 'method' => 'get']) !!}
<div class="uk-padding-remove filter uk-container-center uk-margin-bottom block-area">
    <div class="search-form-ext">
        <div class="uk-grid">
            <div class="uk-width-1-2">
                <div class="search-input search-form">
                    {!! Form::text('term', $term, ['placeholder' => 'Начните поиск...']) !!}
                    <button type="submit"></button>
                </div>
            </div>
        </div>
    </div>
</div>

@include('frontend.search.partials.tags')

{!! Form::close() !!}
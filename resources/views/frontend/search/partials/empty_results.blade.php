<div class="empty-results-text">
    <div class="uk-width-1-2 uk-container-center uk-clearfix">
        <div class="text uk-float-left">По Вашему запросу <b>ничего не найдено</b></div>
        <div class="uk-float-right">
            {!! link_to_route('feedback.consult', 'Получить консультацию', [], ['class' => 'btn-link btn-popup uk-button uk-button-primary']) !!}
        </div>
    </div>
</div>

<h2 class="uk-margin-large-top uk-text-center uppercase">Вам также может быть интересно</h2>

<div class="empty-results-products">
    <h3 class="uppercase uk-margin-bottom-remove">Кухни</h3>
    @include('frontend.category.partials.layouts.'.$layoutService->getLayout(), ['products' => $altProducts[0]])

    <h3 class="uppercase uk-margin-bottom-remove uk-margin-large-top">Шкафы</h3>
    @include('frontend.category.partials.layouts.'.$layoutService->getLayout(), ['products' => $altProducts[1]])
</div>

<div class="uk-text-center">
    <a href="{{ category_url(\App\Models\Category::find(\Settings::get('sale_category_id'))) }}" class="uk-margin-large-top uk-button uk-button-large uk-button-primary btn-red">
        Распродажа мебели
        <i class="uk-icon-arrow-circle-right"></i>
    </a>
</div>
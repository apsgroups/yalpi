@if(!empty($options['product_type_id']['buckets']) || !empty($options['obshchij-cvet']['buckets']))
    <div class="separate-filter filter uk-container-center uk-margin-bottom uk-clearfix" id="filter-box">
        @if(!empty($options['product_type_id']['buckets']) && count($options['product_type_id']['buckets']) > 1)
            <div class="filter-field">
                @include('frontend.filter.type', ['title' => 'Тип', 'name' => 'product_type_id'])
            </div>
        @endif

        @if(!empty($options['obshchij-cvet']['buckets']) && count($options['obshchij-cvet']['buckets']) > 1)
            <div class="filter-field">
                @include('frontend.filter.list', ['title' => 'Цвет', 'name' => 'obshchij-cvet'])
            </div>
        @endif
    </div>
@endif
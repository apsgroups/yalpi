@if(!empty($options['product_type_id']['buckets']))
    <div class="search-tags">
        @foreach($options['product_type_id']['buckets'] as $tag)
        	@php($type = \App\Models\ProductType::find($tag['key'])) 

        	@if($type && !empty($type->search_url))
        		<a href="{{ $type->search_url }}">{{ $type->title }}</a>
        	@endif
        @endforeach
    </div>
@endif
<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="_token" content="{{ csrf_token() }}" />

        <title>#YaLpi - master class from all around the world</title>

        @include('includes.partials.yandex-verification')

        @include('includes.partials.favicon')

        <!-- Styles -->
        @yield('before-styles-end')

        <style>
            html {
                min-height: 101%;
                overflow-y: visible;
            }
        </style>

        {!! Html::style(elixir('css/frontend.css')) !!}

        <meta name="viewport" content="width=1430">

        @include('includes.partials.google-site-verification')

        <!--[if lt IE 12]>
        <link rel="stylesheet" href="https://rawgit.com/codefucker/finalReject/master/reject/reject.css" media="all" />
        <script type="text/javascript" src="/js/reject.min.js" data-text="К сожалению, браузер, которым вы пользуйтесь, устарел, и не может нормально отображать сайт. Пожалуйста, скачайте любой из следующих браузеров:"></script>
        <![endif]-->

        <script>(window.Image ? (new Image()) : document.createElement('img')).src = 'https://vk.com/rtrg?p=VK-RTRG-222565-5i6oC';</script>

        @include('includes.vk')
        
        @include('includes.fb')

        @include('includes.gtm.head')
    </head>
    <body @yield('body-attr')>
        @include('includes.gtm.body')

        @include('includes.partials.ga')

        <header class="@stack('header-css')">
            @include('frontend.includes.header.main_simple')
        </header>

        <div id="content-container">
            <div class="white-bg">
                @yield('breadcrumbs')
            </div>           

            <div class="uk-container uk-container-center">
                <div class="cosmos">
                    <div class="text-inner uk-text-center uk-width-1-1">
                        <div class="date">1 March, 2020 – the launch in Europe and the USA</div>
                        <div class="title">Master class from all around the world</div>
                    </div>
                    
                    @include('frontend.dummy._form')
                </div>

                <div class="uk-text-center">
                    <a href="{{ \Settings::get('social_instagram') }}" class="big-btn" rel="nofollow" target="_blank"><i class="uk-icon-instagram"></i> Subscribe to our Instagram</a>
                </div>
            </div>
        </div>

        @include('frontend.includes.info')

        <footer>
            @include('frontend.includes.footer.index')
        </footer>

        <!-- JavaScripts -->
        {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>--}}
        {{--<script>window.jQuery || document.write('<script src="{{asset('js/vendor/jquery/jquery-2.1.4.min.js')}}"><\/script>')</script>--}}
        {{--{!! Html::script('js/vendor/bootstrap/bootstrap.min.js') !!}--}}

        {!! Html::script(elixir('js/app.js')) !!}

        @include('includes.partials.livechat')
        
        @include('includes.partials.metrika')
    </body>
</html>
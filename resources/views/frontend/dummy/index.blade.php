@extends('frontend.layouts.master')

@include('frontend.includes.seometa_group', ['group' => 'homepage'])

<?php
$og = [
    'url' => url('/'),
    'title' => \Settings::getOr('homepage_page_title', 'page_title'),
    'description' => \Settings::getOr('homepage_metadesc', 'metadesc'),
    'image' => '',
    'type' => 'product',
];

?>

@section('body-attr', 'class="simple-theme"')

@section('content')
    <div class="uk-container uk-container-center">
        <div class="cosmos">
            <div class="text-inner uk-text-center uk-width-1-1">
                <div class="date">Запуск 12 января 2020 года</div>
                <div class="title">Все уроки в СНГ на одной площадке</div>
                <div class="desc">Учитесь из любой точки вселенной</div>
            </div>
            
            @include('frontend.dummy._form')
        </div>

        <div class="uk-text-center">
            <a href="{{ \Settings::get('social_instagram') }}" class="big-btn" rel="nofollow" target="_blank"><i class="uk-icon-instagram"></i> Подписывайтесь на наш Instagram</a>
        </div>
    </div>
@endsection
{!! Form::open(['route' => ['feedback.consult'], 'method' => 'post', 'class' => 'validate-form ajax-form uk-form']) !!}
    <div class="uk-text-center">{{ trans('frontend.dummy.form.desc') }}</div>

    <div class="inputs">
        {!! Form::text('name', null, ['id' => 'name', 'class' => 'required', 'placeholder' => trans('frontend.dummy.form.name')]) !!}
        {!! Form::text('phone', null, ['id' => 'phone', 'class' => 'phone-mask required', 'placeholder' => trans('frontend.dummy.form.phone')]) !!}
        <button type="submit" class="uk-button">{{ trans('frontend.dummy.form.send') }}</button>
    </div>
{!! Form::close() !!}
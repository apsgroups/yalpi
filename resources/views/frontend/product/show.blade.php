﻿@extends('frontend.layouts.master')

@include('frontend.includes.seometa', ['model' => $product])

@section('breadcrumbs', Breadcrumbs::render('frontend.product', $product))

@include('frontend.product.partials.opengraph')

@section('content')
    <div itemscope itemtype="https://schema.org/Product">
        <div class="main-container" id="product-container">
            <div class="uk-container uk-container-center">
                <div class="product-body">
                    @if($product->product_type_id === 8 && (int)$product->price === 0)
                        <div class="video-wrap">
                            <iframe width="770" height="433" src="https://www.youtube.com/embed/{{ $product->video }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div>
                    @endif

                    <div class="meta-wrap">
                        <div class="meta-block shadow-block">
                            <div class="name">
                                <h1 itemprop="name">{{ $product->title }}</h1>
                            </div>              
                            
                            <div class="meta-row">
                                <div class="rate-stars">
                                    <span class="title">Рейтинг:</span>
                                    <span class="rate">
                                        {!! str_repeat('<span class="star"></span>', $product->rate) !!}
                                    </span>
                                </div>
                                <div class="rate-counter">
                                    <span class="title">Отзывы:</span>
                                    <span>{{ $commentsCount }}</span>
                                </div>

                                <div class="labels">
                                    @if($product->bestseller)
                                        <span class="label hit">ХИТ</span>
                                    @endif

                                    @if($product->online_learning)
                                        <span class="label learning">Он-лайн обучение</span>
                                    @endif
                                </div>
                            </div>

                            <meta itemprop="description" content="{{ $product->title }}" />

                            <div class="preview-desc">{{ $product->preview_title }}</div>

                            @if($authors->count())
                            <?php
                                $authors = $authors->map(function ($item, $key) {
                                    $item->link = link_to_route('author.items', $item->name, [$item->slug]);

                                    return $item;
                                });
                            ?>
                            <div class="meta-row">
                                <div class="authors"><span class="title">Авторы:</span> {!! $authors->implode('link', ', ') !!}</div>
                            </div>
                            @endif

                            <div class="meta-row">
                                <div><span class="title">Создание:</span> {{ $product->created_at->format('d.m.Y') }}</div>
                                <div><span class="title">Обновление:</span> {{ $product->updated_at->format('d.m.Y') }}</div>

                                @if($product->language)
                                <div class="language"><span class="title">Субтитры:</span> {{ $product->language->title }}</div>
                                @endif
                            </div>
                        </div>
                    </div>

                    @include('frontend.product.partials.tabs')

                    @include('frontend.product.partials.start-free')

                    @include('frontend.product.partials.program')

                    @if($product->product_type_id === 8 && (int)$product->price === 0)
                        @include('frontend.product.partials.advantages_video')
                    @else
                        @include('frontend.product.partials.advantages')
                    @endif
                </div>

                <div class="card" id="product-card">
                    <div class="card-inner">
                        @if(!Auth::check())
                        	@if($icon = $product->getIconPath())
                            <div class="img-cnt img-bg-{{ $product->icon_bg }}">
                                <div>
                                    <img itemprop="image" src="{{ $icon }}" class="product-preview" alt="{{ $product->title }}" title="{{ $product->title }}" />
                                </div>
                            </div>
                            @endif

                            <div class="inner{{ ($icon ? ' with-icon' : '') }}">
                                @include('frontend.product.partials.price')

                                <div class="uk-text-center"><a href="{{ route('auth.login') }}" class="btn-popup login action"><span>Войти</span></a></div>

                                @include('frontend.product.partials.features')  
                            </div>
                        @else
                            @include('frontend.product.partials.soon')
                        @endif
                    </div>

                    @include('frontend.product.partials.favorite')
                </div>
            </div>
        </div>
    
        <div class="uk-margin-large-top uk-container uk-container-center">
            {{-- @include('frontend.product.partials.authors') --}}

            @include('frontend.comments.index', ['item' => $product, 'comments' => $comments])

            @include('frontend.product.partials.related')
        </div>
    </div>
@endsection

@section('remarketing-data')
    <script type="text/javascript">
        var google_tag_params = {
            ecomm_prodid: '{{ $product->id }}',
            ecomm_pagetype: 'product',
            ecomm_totalvalue: {{ $product->price }},
        };
    </script>
@endsection

@section('container', 'container-main')

@section('body-attr', 'class="product-page"')
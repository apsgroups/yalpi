<div class="item">
	<div itemprop="itemOffered" itemscope itemtype="https://schema.org/Product">
	    @php($imgSize = isset($imgSize) ? $imgSize : 'preview3')
	    @php($link = $product->url)
	    @php($useIcon = true)

	    @if(isset($favorites) && is_array($favorites))
	    	<button type="button" data-id="{{ $product->id }}" class="fav-btn {{ (in_array($product->id, $favorites) ? ' active' : '') }}"></button>
	    @endif
		
		<a href="{{ $link }}" class="item-link" {!! (isset($newWindow) ? 'target="_blank"' : '') !!}></a>
		
		@if($product->image)
			@php($useIcon = false)
		    <div class="uk-position-relative">
		    	@if($product->icon)
			    	<div class="item-icon"><img src="{{ $product->icon }}" alt=""></div>
			    @endif
			    
			    <div class="img-cnt">			        
		            @include('frontend.product.partials.preview.badge')

		            @php($src = $product->image ? img_src($product->image, $imgSize) : '/uploads/sample.svg')

		            <img itemprop="image" data-src="{{ $src }}" src="{{ $src }}" style="height: auto; width: auto;" class="product-preview" data-product-id="{{ $product->id }}" alt="{{ $product->title }}" title="{{ $product->title }}" />
			    </div>
		    </div>
	    @endif
		
		<div class="info-inner">
			<div class="info{{ ($useIcon ? ' with-icon' : '') }}">
			    <div class="product-desc">
			        <meta itemprop="name" content="{{ $product->title }}" />
			        <meta itemprop="description" content="{{ $product->title }}. Цена - {{ format_price($product->price) }} руб. в интернет-магазине." />

			        <div class="title">{{ $product->title }}</div>
			    </div>

		        <div class="author">
			        @if(count($product->authors))
			            {{ $product->authors[0] }}

			            @if(count($product->authors) > 1)
			                и соавторы
			            @endif
			        @endif
		        </div>

		        <div class="info-icons">
			        <span class="star">{{ $product->rate }}</span>

			        <span class="views">{{ $product->view_hits }}</span>

			        <span class="comments">{{ $product->comments_count }}</span>

				    <span class="product-price {{ ($product->price > 0 ? 'pay' : '') }}">
				        {{ ($product->price > 0 ? 'Платно' : 'Бесплатно') }}
				    </span>
		        </div>
			</div>
		</div>
	</div>
</div>
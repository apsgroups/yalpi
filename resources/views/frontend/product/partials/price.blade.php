<?php
    $price = format_price(session('prices.'.$product->id.'.price', $product->price));
    $priceOld = format_price(session('prices.'.$product->id.'.price_old', $product->price_old));
    $discount = format_price(session('prices.'.$product->id.'.discount', $product->discount));
?>

<div class="price-box" itemprop="offers" itemscope itemtype="https://schema.org/Offer">
    <meta itemprop="price" content="{{ $product->price }}" />
    <meta itemprop="priceCurrency" content="RUB" />
    <link itemprop="availability" href="https://schema.org/InStock" />

    <span class="price">
        @if($price > 0)
        <span id="product-price" class="digit">{{ $price }}</span>
        <span class="currency">{!! config('site.price.currency') !!}</span>
        @else
        Бесплатно
        @endif
    </span>

    @if($discount)
        <div class="price-old-wrapper">
            <span class="price-old">
                <span class="digit" id="product-price-old">{{ $priceOld }}</span>
                <span class="currency">{!! config('site.price.currency') !!}</span>
            </span>

            <span class="discount">
                Скидка <span id="product-discount">{{ $discount }}</span>%
            </span>
        </div>
    @endif
</div>
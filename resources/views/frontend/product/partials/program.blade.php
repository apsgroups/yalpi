@if(!$courceProgram->isEmpty())
<div class="block-cources">
	<div class="block-title">Программа урока:</div>

	<ul class="block-items">
		@foreach($courceProgram as $k => $cource)
		<li>
			<div class="preview">
				<a href="//www.youtube.com/embed/{{ $cource->video }}?enablejsapi=1" class="play-btn fancybox fancybox.iframe" rel="gallery nofollow"></a>
				<div class="title-wrap">
					<span class="title toggle">{{ $cource->title }}</span>
					<span class="duration">2,5 часа</span>
				</div>
				<div class="button toggle"></div>
			</div>
			<div class="toggle-body">
				<div class="desc">
					@if($cource->image)
					<div class="img">
						<a href="//www.youtube.com/embed/{{ $cource->video }}?enablejsapi=1" class="play-btn fancybox fancybox.iframe" rel="gallery nofollow"></a>
							<img src="{{ img_src($cource->image, 'preview') }}">
						</a>
					</div>
					@endif

					<div class="text">
						<div class="title">Описание блока:</div>
						<ul>
							<li>
								{!! $cource->desc !!}
							</li>
						</ul>
					</div>
				</div>
			</div>
		</li>
		@endforeach
	</ul>
</div>
@endif
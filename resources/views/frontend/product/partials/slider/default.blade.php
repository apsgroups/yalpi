<figure class="uk-overlay uk-overlay-hover">
    <img src="{{ img_src($product->image, 'grid') }}" />

    <div class="uk-overlay-panel uk-overlay-fade uk-overlay-background">
        <div class="uk-vertical-align uk-height-1-1">
            <div class="uk-vertical-align-middle">
                <a href="{{ product_url($product) }}" class="uk-button uk-button-primary btn-empty">Подробнее</a>
            </div>
        </div>
    </div>

    <a class="uk-position-cover" href="{{ product_url($product) }}"></a>
</figure>

<h4 class="uk-margin-top-remove lighter">
    <a href="{{ product_url($product) }}">{{ $product->title }}</a>
</h4>

@if($product->price > 0)
<div class="big price {{ ($product->price_old > 0 ? 'new' : '') }} uk-margin-small">
    <span>{{ format_price($product->price) }}</span>
    {!! config('site.price.currency') !!}
</div>
@endif

@if($product->price_old > 0)
<div class="uk-margin-left uk-text-muted price old">{{ format_price($product->price_old) }} {!! config('site.price.currency') !!}</div>
@endif

<a class="uk-button uk-button-large uk-button-primary uk-margin addtocart" data-product-id="{{ $product->id }}">В корзину</a>
<div class="uk-margin-top {{ isset($layout) ? $layout : 'default' }}">
    <div data-uk-slideset="{default: 5}">
        <div class="uk-slidenav-position uk-margin item-slideset">
            <ul class="uk-slideset uk-grid uk-flex-center uk-grid-width-1-5" data-uk-grid-match="{target:'figure'}">
                @foreach($products as $product)
                <li>
                    <div class="preview-item slide-item uk-padding-bottom">
                        @include('frontend.product.preview')
                    </div>
                </li>
                @endforeach
            </ul>

            <a href="" class="uk-slidenav uk-slidenav-contrast uk-slidenav-previous" data-uk-slideset-item="previous"></a>
            <a href="" class="uk-slidenav uk-slidenav-contrast uk-slidenav-next" data-uk-slideset-item="next"></a>
        </div>
    </div>
</div>
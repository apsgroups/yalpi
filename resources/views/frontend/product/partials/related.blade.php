@if($relateds->count())
<div class="block-related">
	<div class="block-title">Рекомендованные уроки:</div>

	<div class="uk-grid uk-grid-margin product-items">
		@foreach($relateds as $p)
			<div class="uk-width-1-4">
				@include('frontend.product.preview', ['product' => $p->getPreviewData()])
			</div>
		@endforeach
	</div>
</div>
@endif
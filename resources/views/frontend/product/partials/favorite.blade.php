@if(Auth::check())
    <div class="fav-btn-block">
    	<button type="button" data-id="{{ $product->id }}" class="fav-btn {{ ($favorite ? 'active' : '') }}">
    	{{ ($favorite ? 'В избранном' : 'Добавить в избранное') }}</button>

    	<div class="fav-tip">Добавьте видео чтобы вернуться к нему, досмотреть, пройти тест и получить бейдж.</div>
    </div>
@endif
@php($position = 'top-left')

@if($product->novelty)
    @include('frontend.product.partials.preview.badge-item', ['name' => 'novelty', 'text' => trans('frontend.novelty'), 'position' => $position])

    @php($position = 'top-right')
@endif


@if($product->in_stock > 0 && $product->virtual_discount > 0)
    @include('frontend.product.partials.preview.badge-item', ['name' => 'sale', 'text' => '- '.$product->virtual_discount.' %', 'position' => $position])

    @php($position = 'bottom-left')
@elseif($product->discount > 0)    
    @include('frontend.product.partials.preview.badge-item', ['name' => 'sale', 'text' => '- '.$product->discount.' %', 'position' => $position])

    @php($position = 'bottom-left')
@endif

@if($product->soon)
    @php($position = 'top-right')

    @include('frontend.product.partials.preview.badge-item', ['name' => 'soon', 'text' => 'Скоро', 'position' => $position])

    @php($position = 'bottom-right')
@endif


@if($product->bestseller)
    @include('frontend.product.partials.preview.badge-item', ['name' => 'bestseller', 'text' => trans('frontend.bestseller'), 'position' => $position])

    @php($position = 'bottom-right')
@endif
<div class="{{ $name }} product-badge uk-position-{{ $position }} uppercase">
    {{ $text }}
</div>
@if($product->image)
    <div class="uk-thumbnail uk-overlay-hover" data-uk-modal="{target:'#modal-{{ $product->id }}'}">
        <figure class="uk-overlay">
            <img src="{{ img_src($product->image, 'middle') }}" />
            <figcaption class="uk-overlay-panel uk-overlay-icon uk-overlay-background uk-overlay-fade"></figcaption>
            <a class="uk-position-cover" href="#"></a>
        </figure>
    </div>

    <div id="modal-{{ $product->id }}" class="uk-modal">
        <div class="uk-modal-dialog uk-modal-dialog-lightbox">
            <a href="" class="uk-modal-close uk-close uk-close-alt"></a>
            <img src="{{ img_src($product->image, 'large') }}" />
        </div>
    </div>
@endif
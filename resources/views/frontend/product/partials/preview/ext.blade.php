<div class="preview-item-ext {{ ($product->product_type_id === 1 ? '' : 'padding') }} ">
    <div class="product-ext-preview">
        <ul data-ext-preview-id="{{ $product->id }}">
            @php($totalImg = 0)

            @php($imgSize = empty($imgSize) ? 'grid-4' : $imgSize)

            @if($product->image)
                @php($totalImg = 1)

                <li>
                    <span data-large="{{ img_src($product->image, $imgSize) }}" data-href="{{ $link }}" class="ext-preview">
                        <img src="{{ img_src($product->image, 'preview_ext') }}" alt="{{ $product->shortTitle }}" title="{{ $product->shortTitle }}" />
                    </span>
                </li>
            @endif

            @if($product->images->count())
                @foreach($product->images as $k => $img)
                    @continue((!empty($product->image->id) && $product->image->id === $img->id) || $totalImg >= 4)
                    <li>
                        <span data-large="{{ img_src($img, $imgSize) }}" data-href="{{ $link }}" class="ext-preview">
                            <img src="{{ img_src($img, 'preview_ext') }}" alt="{{ $product->shortTitle }}" title="{{ $product->shortTitle }}" />
                        </span>
                    </li>
                    @php($totalImg++)
                @endforeach
            @endif

            @if($totalImg < 4 && $product->videoGallery)
                @foreach($product->videoGallery as $video)
                    @continue($totalImg > 4)
                    <li>
                        <span class="video-preview ext-video ext-preview" data-large="{{ img_youtube($video, $imgSize) }}">
                            <a href="//www.youtube.com/embed/{{ $video }}?enablejsapi=1" class="video-goal fancybox fancybox.iframe" data-fancybox-width="1099" data-fancybox-height="618" data-goal="open_video" rel="nofollow">
                                <img src="{{ img_youtube($video, 'preview_ext') }}" title="{{ $product->title }}" alt="{{ $product->title }}" />
                                <i class="uk-icon-play"></i>
                            </a>
                        </span>
                    </li>
                    @php($totalImg++)
                @endforeach
            @endif
        </ul>

        <div class="ext-buttons">
            <a href="#" class="btn-favorite {{ (Favorite::isActive($product->id) ? 'active' : '') }}" data-product-id="{{ $product->id }}">
                <i class="uk-icon-heart-o"></i>
            </a>

            <a href="#" class="addtocart" data-product-id="{{ $product->id }}">
                <i class="uk-icon-shopping-cart"></i>
            </a>
        </div>
    </div>
</div>
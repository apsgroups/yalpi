<div id="product-tabs">
    <div>
        <ul class="uk-tab" data-uk-tab="{connect:'#product-tabs-switcher'}">
            @if($product->experience != '')
            <li><a href="#" data-tab-btn="experience">Что ты получишь</a></li>
            @endif
            @if($product->description)
            <li><a href="#" data-tab-btn="description">Описание</a></li>
            @endif
        </ul>
    </div>

    <ul id="product-tabs-switcher" class="uk-switcher">
        @if($product->experience != '')
        <li>
            @include('frontend.product.partials.experience')

            @if(!$product->video_format)
                {{-- @include('frontend.product.partials.you-get') --}}
            @endif
        </li>
        @endif
        @if($product->description)
        <li>
            <div class="block-desc">
                {!! $product->description !!}
            </div>
        </li>
        @endif
    </ul>
</div>
@if($lastSeen->count() > 0)
     <div class="slider-box last-seen-slider uk-margin-large-top">
         <div class="block-title">Недавно просмотренные</div>
         @include('frontend.product.partials.slider.index', ['products' => $lastSeen, 'newWindow' => true])
     </div>
@endif
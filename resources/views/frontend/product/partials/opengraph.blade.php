<?php

$meta = isset($meta) ? $meta : \App\Services\Meta\MetaFactory::getInstance($product);

$og = [
    'url' => $product->link(true),
    'title' => preg_replace('/"(.*?)"/', '«$1»', $meta->title()),
    'description' => preg_replace('/"(.*?)"/', '«$1»', $meta->description()),
    'type' => 'product.item',
];

if(!empty($product->image)) {
	$og['image'] = url(img_src($product->image, 'large'));
}
?>

@push('head')
    @include('frontend.includes.opengraph', compact('og'))
@endpush
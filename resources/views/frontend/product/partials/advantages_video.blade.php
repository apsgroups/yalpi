<noindex>
    <div class="advantages">
        <div class="block-title">Преимущества он-лайн обучения в Yalpi</div>

        <div class="uk-grid uk-grid-margin uk-text-center">
            <div class="uk-width-1-3">
                <div class="img"><img src="/images/layout/advantages/skill.svg" alt="Прокачай скиллы"></div>
                <div class="title">Прокачай скиллы</div>
                <div class="desc">Посмотри видео, пройди тест (бета), получи бейдж (диплом)</div>
            </div>
            <div class="uk-width-1-3">
                <div class="img"><img src="/images/layout/advantages/free.svg" alt="Это бесплатно"></div>
                <div class="title">Это бесплатно</div>
                <div class="desc">Мы собрали тысячи качественных, бесплатных мастер-классов и уроков</div>
            </div>
            <div class="uk-width-1-3">
                <div class="img"><img src="/images/layout/advantages/share.svg" alt="Делись опытом"></div>
                <div class="title">Делись опытом</div>
                <div class="desc">Делись опытом и мнением о пройденом материале и зарабатывай баллы</div>
            </div>
        </div>
    </div>
</noindex>
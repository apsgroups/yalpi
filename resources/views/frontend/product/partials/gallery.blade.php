<div class="uk-position-relative">
    @if($product->soon)
        @include('frontend.product.partials.preview.badge-item', ['name' => 'soon', 'text' => 'Скоро', 'position' => 'top-left'])
    @endif

    @if($product->image)
    <a class="fancybox" rel="gallery" href="{{ img_src($product->image, 'xlarge') }}" id="product-image">
        <img itemprop="image" src="{{ img_src($product->image, 'large') }}" data-product-id="{{ $product->id }}" title="{{ $product->title }}" alt="{{ $product->title }}" />
    </a>
    @endif

    <?php
    $gallery = [];

    if($product->videoGallery) {
        foreach($product->videoGallery as $video) {
            $gallery[] = [
                    'type' => 'video',
                    'href' => '//www.youtube.com/embed/'.$video.'?enablejsapi=1',
                    'preview' => img_youtube($video, 'gallery_item'),
            ];
        }
    }

    if($product->images->count() > 1) {
        foreach($product->images as $image) {
            if($product->image && $product->image->id === $image->id) {
                continue;
            }

            $gallery[] = [
                'type' => 'image',
                'model' => $image,
                'href' => img_src($image, 'xlarge'),
                'preview' => img_src($image, 'gallery_item'),
            ];
        }
    }

    $galleryCount = count($gallery);
    ?>

    @if($galleryCount && $galleryCount <= 6)
    <div data-uk-slideset="{default: 5}" class="gallery uk-margin-top uk-slidenav-position">
            <ul class="uk-slideset uk-grid uk-grid-medium uk-grid-width-medium-1-5">
                @foreach($gallery as $v)
                    <li>
                        <a class="fancybox {{ ($v['type'] == 'video' ? 'video-goal fancybox.iframe video-preview' : '') }} " rel="gallery" href="{{ $v['href'] }}" 
                        {!! ($v['type'] == 'video' ? 'data-fancybox-width="1099" data-fancybox-height="618" data-goal="open_video"' : '') !!} >
                            <img itemprop="image" src="{{ $v['preview'] }}" title="{{ $product->title }}" alt="{{ $product->title }}" />

                            @if($v['type'] == 'video')
                                <i class="uk-icon-play"></i>
                            @endif
                        </a>
                    </li>
                @endforeach
            </ul>

            @if($galleryCount > 6)
                <a href="" class="uk-slidenav uk-slidenav-previous" data-uk-slider-item="previous"></a>
                <a href="" class="uk-slidenav uk-slidenav-next" data-uk-slider-item="next"></a>
            @endif
    </div>
    @endif

    @if($galleryCount > 6)
    <div data-uk-slider="{default: 5}" class="gallery uk-margin-top">
        <div class="uk-slider-container uk-slidenav-position">
            <ul class="uk-slider uk-grid uk-grid-medium uk-grid-width-medium-1-5">
                @foreach($gallery as $v)
                    <li>
                        <a class="fancybox {{ ($v['type'] == 'video' ? 'video-goal fancybox.iframe video-preview' : '') }} " rel="gallery" href="{{ $v['href'] }}" {!! ($v['type'] == 'video' ? 'data-fancybox-width="1099" data-fancybox-height="618" data-goal="open_video"' : '') !!} >
                            <img itemprop="image" src="{{ $v['preview'] }}" title="{{ $product->title }}" alt="{{ $product->title }}" />

                            @if($v['type'] == 'video')
                                <i class="uk-icon-play"></i>
                            @endif
                        </a>
                    </li>
                @endforeach
            </ul>

            @if($galleryCount > 6)
                <a href="" class="uk-slidenav uk-slidenav-previous" data-uk-slider-item="previous"></a>
                <a href="" class="uk-slidenav uk-slidenav-next" data-uk-slider-item="next"></a>
            @endif
        </div>
    </div>
    @endif
</div>
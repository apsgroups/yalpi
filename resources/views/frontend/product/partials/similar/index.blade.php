@if($similarProducts && $similarProducts->count())
    <div class="uk-grid" id="similar-products">
        @include('frontend.product.partials.similar.grid')
    </div>

    @if($similarProducts->count() >= 4)
    <div class="uk-text-center">
        <button id="more-similar-products" data-url="{{ route('product.similar', [$product->id]) }}">Показать больше</button>
    </div>
    @endif
@endif
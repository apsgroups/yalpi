@foreach($similarProducts as $p)
    <div class="uk-width-1-4 uk-margin-bottom">
        @include('frontend.product.partials.similar.product', ['product' => $p, 'newWindow' => true])
    </div>
@endforeach
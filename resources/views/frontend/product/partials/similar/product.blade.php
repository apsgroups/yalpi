@php($ext = true || isset($category))

<div class="preview-item uk-padding-bottom product-parent {{ ($ext ? 'has-ext-preview' : '') }}">
    <div class="preview-main">
        @php($link = product_url($product))

        <div class="uk-position-relative img-cnt">
            <figure class="uk-text-center">
                @include('frontend.product.partials.preview.badge')

                @if($product->image)
                <div class="uk-vertical-align uk-height-1-1 uk-width-1-1 uk-position-absolute uk-position-top more-link">
                    <div class="uk-vertical-align-middle">
                        <div class="product-see"><a href="{{ $link }}" {!! (isset($newWindow) ? 'target="_blank"' : '') !!}><i class="uk-icon-hand-pointer-o"></i> Посмотреть</a></div>
                    </div>
                </div>

                <?php $imgSize = 'grid-4'; ?>

                <img data-src="{{ img_src($product->image, $imgSize) }}" src="{{ img_src($product->image, $imgSize) }}" class="product-preview" data-product-id="{{ $product->id }}" alt="{{ $product->shortTitle }}" title="{{ $product->shortTitle }}" />

                <a class="uk-position-cover" href="{{ $link }}" {!! (isset($newWindow) ? 'target="_blank"' : '') !!}></a>
                @endif
            </figure>
        </div>

        <div class="product-desc">
            @if($product->type->display_tag && !empty($category->tag))
                <p class="short-tag">{{ $category->tag }}</p>
            @endif

            <a href="{{ $link }}" itemprop="url" class="title" {!! (isset($newWindow) ? 'target="_blank"' : '') !!}>{{ $product->shortTitle }}</a>

            @if(!empty($product->color_title))
                <div class="uk-text-muted color">
                    {{ $product->color_title }}
                </div>
            @endif
        </div>

        <div class="product-price">
            @if($product->price_old > 0)
                <span class="price old">{{ format_price($product->price_old) }} {!! config('site.price.currency') !!}</span>
            @endif

            @if($product->price > 0)
                <span class="price big {{ ($product->price_old > 0) ? 'new' : '' }} uk-margin-small">
                    <span>{{ format_price($product->price) }}</span>
                    {!! config('site.price.currency') !!}
                </span>
            @endif
        </div>

        @if(!empty($addToCartBtn))
            <a class="uk-button uk-button-large uk-button-primary uk-margin addtocart" data-product-id="{{ $product->id }}">В корзину</a>
        @endif
    </div>

    @if($ext)
        @include('frontend.product.partials.preview.ext')
    @endif
</div>
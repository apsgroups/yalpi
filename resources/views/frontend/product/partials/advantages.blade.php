<noindex>
    <div class="advantages">
        <div class="block-title">Преимущества он-лайн обучения в Yalpi</div>

        <div class="uk-grid uk-grid-margin uk-text-center">
            <div class="uk-width-1-3">
                <div class="img"><img src="/images/layout/advantages/work.svg" alt="Получи новую работу!"></div>
                <div class="title">Получи новую работу!</div>
                <div class="desc">Проходи мастер-классы и гарантированно работай в новой профессии</div>
            </div>
            <div class="uk-width-1-3">
                <div class="img"><img src="/images/layout/advantages/warranty.svg" alt="Гарантия на обучение"></div>
                <div class="title">Гарантия на обучение</div>
                <div class="desc">Мы собрали подробную библиотеку онлайн-уроков от специалистов всех областей</div>
            </div>
            <div class="uk-width-1-3">
                <div class="img"><img src="/images/layout/advantages/cashback.svg" alt="Не нравится обучение?"></div>
                <div class="title">Не нравится обучение?</div>
                <div class="desc">Вернем деньги за пройденный коучинг в течение 30 дней</div>
            </div>
        </div>
    </div>
</noindex>
@if($product->experience != '')
<div class="experience uk-clearfix">
    <ul>
        @foreach($experience as $text)
            <li>{!! $text !!}</li>
        @endforeach
    </ul>
</div>
@endif
@if($authors->count())
<div class="block-authors">
    <div class="block-title">Об авторах:</div>

    @foreach($authors as $author)
    	<div class="item">
    		<div class="personal">
    			@if($author->image)
    			<div class="img"><img src="{{ img_src($author->image, 'avatar') }}" alt="{{ $author->title }}" /></div>
    			@endif

				<div class="text">						
	    			<div class="name">{{ $author->title }}</div>

	    			<div class="about">
	    				{!! $author->about !!}
					</div>
				</div>
    		</div>

    		<div class="connect">
    			<div class="inner">
    				@if($author->slogan)
	    			<div class="quote">{!! $author->slogan !!}</div>
					@endif
					
	    			<ul class="social">
	    				<?php 
	    				$socials = [
	    					'fb' => 'facebook', 
	    					'vk' => 'vk', 
	    					'yt' => 'youtube', 
	    					'instagram' => 'instagram',
	    					'twitter' => 'twitter',
	    				];
	    				?>

	    				@foreach($socials as $k => $v)
	    					@continue($author->$v == '')
	    					<li><a href="{{ $author->$v }}" rel="nofollow" class="{{ $k }}"></a></li>
	    				@endforeach
	    			</ul>
    			</div>
    		</div>
    	</div>
    @endforeach
</div>
@endif
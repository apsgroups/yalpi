<div class="you-get uk-grid">
	<?php
	$items = [
		'complex' => 'Начальная сложность',
		'discuss' => 'Обсуждение заданий',
		'help' => 'Онлайн-помощь от преподавателя',
		'certificate' => 'Сертификат для трудоустройства'
	];
	?>

	@foreach($items as $k => $v)
	<div class="item uk-width-1-4">
		<div class="img">
			<img src="/images/layout/you-get/{{ $k }}.svg" alt="{{ $v }}">
		</div>
		<div class="title">{{ $v }}</div>
	</div>
	@endforeach
</div>
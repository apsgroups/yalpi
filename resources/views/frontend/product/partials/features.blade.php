<ul class="features">
@if($features->count())
	@foreach($features as $f)
    	<li class="icon-{{ $f->icon ?: 'point' }}">{{ $f->text }}</li>
	@endforeach
@else
	<li class="icon-point">Сохраните видео и изучайте когда удобно</li>
	<li class="icon-point">Участвуйте в дискуссиях по мастер-классу</li>
	<li class="icon-point">Задавайте вопросы участникам сообщества</li>
	<li class="icon-point">Проходите тесты и получайте дипломы (скоро)</li>
@endif
</ul>
@if(!empty($og))
	@foreach($og as $k => $v)
	<meta property="og:{{ $k }}" content="{{ $v }}" />
	@endforeach
@endif
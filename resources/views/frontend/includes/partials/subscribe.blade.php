<div class="subscribe-widget uk-text-center">
    <h3>БУДЬТЕ В КУРСЕ АКЦИЙ И СКИДОК!</h3>

    {!! Form::open(['route' => 'subscribe.add', 'class' => 'validate-form ajax-form uk-form', 'method' => 'post']) !!}
        <button class="uk-button uk-button-primary">Подписаться</button>
        {!! Form::text('email', null, ['class' => 'email required', 'placeholder' => 'Введите ваш E-mail']) !!}
    {!! Form::close() !!}
</div>
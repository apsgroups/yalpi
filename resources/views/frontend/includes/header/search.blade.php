{!! Form::open(['route' => 'search.index', 'class' => 'uk-form search-form', 'method' => 'get', 'id' => 'search-holder']) !!}
    {!! Form::text('term', request('term'), ['placeholder' => 'Более 3000 курсов с гарантией результата', 'autocomplete' => 'off', 'id' => 'search-box']) !!}
    <button type="submit"></button>
{!! Form::close() !!}
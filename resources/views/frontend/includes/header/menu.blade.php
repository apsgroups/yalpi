<div class="main-menu {{ request()->get('menu_pref') }}" id="main-menu">
    <div class="main-menu-inner">
        <a href="#" id="main-menu-btn" class="catalog-btn">Каталог уроков</a>
        @widget('menuWidget', ['menu' => 'catalog-homepage', 'layout' => 'dropdown', 'product_count' => 1])
    </div>
</div>
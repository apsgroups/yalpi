<div class="uk-container uk-container-center"> 
    <div class="header-row h-top"> 
        <div class="h-left">
            <div class="logo-box">
                @if(!request()->is('/'))
                    <a href="{{ route('frontend.index') }}" class="logo"></a>
                @else
                    <div class="logo"></div>
                @endif
                
                <div class="site-desc">
                    Мастер-классы со всего мира
                </div>
            </div>
        </div>

        <div class="h-full">
            <div class="h-menu">
                <div class="uk-button-dropdown city-select" data-uk-dropdown="{mode:'click'}">
                    <button class="uk-button">Русский</button>
                    <div class="uk-dropdown uk-dropdown-bottom">
                        <a href="{{ route('frontend.english') }}">English</a>
                    </div>
                </div>

                @widget('menuWidget', ['menu' => 'top', 'layout' => 'top'])
            </div>

            <nav class="user-menu menu">                    
                <ul>
                    @if(Auth::check() && $user = auth()->user())
                        <li>
                            <div id="notifications" class="uk-button-dropdown" data-uk-dropdown="{pos:'bottom-center', mode:'click'}">
                                <button class="uk-button">
                                    <img src="/images/layout/user/bell.svg" alt="">
                                    @php($notifies = auth()->user()->notify_count)
                                    <span class="count{{ ($notifies > 0 ? ' active' : '') }}">{{ $notifies }}</span>
                                </button>

                                <div class="uk-dropdown" id="notifications-content">
                                </div>
                            </div>
                        </li>
                        <li>
                            <a href="{{ route('frontend.user.dashboard') }}" class="login">
                                @if($avatar = img_src($user->image, 'avatar'))
                                    <img src="{{ $avatar }}" alt="{{ $user->name }}" title="{{ $user->name }}">
                                @endif
                                {{ $user->name }}
                            </a>
                        </li>
                    @else
                        <li><a href="{{ route('auth.login') }}" class="btn-popup login"><span>Войти</span></a></li>
                        <li><a href="{{ route('auth.register') }}"><span>Регистрация</span></a></li>
                    @endif
                </ul>
            </nav>
        </div>
    </div>

    <div class="header-row"> 
        <div class="h-left">
            @include('frontend.includes.header.menu')
        </div>

        <div class="h-full">
            @include('frontend.includes.header.search')
        </div>
    </div>
</div>
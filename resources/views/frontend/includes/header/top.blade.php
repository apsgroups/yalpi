<div class="top-panel">
    <div class="uk-container uk-container-center">
        <div class="uk-clearfix">
            <div class="uk-float-right">
                @widget('menuWidget', ['menu' => 'service', 'layout' => 'flat'])
            </div>

            <div class="uk-float-left">
                {!! link_to_route('feedback.consult', 'Получить консультацию', [], ['class' => 'btn-link btn-popup', 'data-goal' => 'konsult']) !!}
            </div>
        </div>
    </div>
</div>
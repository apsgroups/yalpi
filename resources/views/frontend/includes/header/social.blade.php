<div class="socials">
    @foreach(['fb', 'vkontakte', 'twitter', 'instagram'] as $social)
        @if($link = \Settings::get('social_'.$social))
        <!--noindex-->
        <a href="{{ $link }}" class="soc-link" rel="nofollow noopener">
            <img src="/images/layout/header/{{ $social }}.svg" alt="{{ $social }}"></span>
        </a>
        <!--/noindex-->
        @endif
    @endforeach
</div>
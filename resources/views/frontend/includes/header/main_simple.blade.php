<div class="uk-container uk-container-center"> 
    <div class="header-row h-top"> 
        <div class="logo-box">
            @if(!request()->is('/'))
                <a href="{{ route('frontend.index') }}" class="logo"></a>
            @else
                <div class="logo"></div>
            @endif
            
            <div class="site-desc">
                {{ trans('frontend.logo.desc') }}
            </div>
        </div>

        @include('frontend.includes.header.social')

        <div>
            <a href="/blog/" class="catalog-btn">{{ trans('frontend.catalog-btn') }}</a>
        </div>
    </div>
</div>
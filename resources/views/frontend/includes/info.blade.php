@if(Settings::get('info_enabled') === '1' && strpos(url()->current(), '/news/') === false)
<div class="info-box inactive" data-info="newyear">
    <i class="uk-icon-close close"></i>

    <div class="info-icon"><i class="uk-icon-info-circle"></i></div>
    <!-- <div class="info-box-title">ВНИМАНИЕ</div> -->
    <a href="{{ Settings::get('info_url') }}">{!! Settings::get('info_title') !!}<br /><br />Подробнее</a>
</div>
@endif

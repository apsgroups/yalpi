@section('title', \Settings::getOr($group.'_page_title', 'page_title'))
@section('metadesc', \Settings::getOr($group.'_metadesc', 'metadesc'))
@section('metakeys', \Settings::getOr($group.'_metakeys', 'metakeys'))
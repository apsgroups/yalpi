@php($meta = isset($meta) ? $meta : \App\Services\Meta\MetaFactory::getInstance($model))
@section('title', preg_replace('/"(.*?)"/', '«$1»', $meta->title()))
@section('metadesc', preg_replace('/"(.*?)"/', '«$1»', $meta->description()))

@if($meta->keywords() === 'off')
	@section('metakeys-off', 'true')
@else
	@section('metakeys', preg_replace('/"(.*?)"/', '«$1»', $meta->keywords()))
@endif
<div class="uk-container uk-container-center uk-clearfix">
    @if(Auth::check())
        <div class="logo-clmn">
            <div class="logo-box">
                @if(!request()->is('/'))
                    <a href="{{ route('frontend.index') }}" class="logo"></a>
                @else
                    <div class="logo"></div>
                @endif

                <div class="site-desc">
                    Мастер-классы со всего мира
                </div>
            </div>

            <a href="#" class="big-btn">Стать преподавателем</a>
        </div>
    @endif

    <div class="links">
        @if(Auth::check())
            @widget('menuWidget', ['menu' => 'footer', 'layout' => 'footer'])
        @endif

        <div class="small-text">            
            <span class="copyright">
                © {{ date('Y') }}, {{ Settings::get('title') }}.
            </span>

            <span class="pay">
                Оплата Viza, Mastercard
            </span>

            <span class="oferta">
                {!! link_to('/informatsiya-dlya-pokupatelya/', 'Сайт не является договором оферты') !!}
            </span>
        </div>
    </div>
</div>
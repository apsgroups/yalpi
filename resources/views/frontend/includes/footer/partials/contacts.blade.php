<div class="contacts">
    {!! Settings::get('footer_address') !!}

    <div class="uk-margin-top uk-position-relative">
        @include('frontend.includes.footer.partials.share')
    </div>
</div>
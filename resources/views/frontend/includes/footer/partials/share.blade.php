<script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
<script src="//yastatic.net/share2/share.js"></script>

<div class="share-us uk-clearfix">
	<span class="uk-display-inline-block uk-float-left" style="line-height: 18px">Расскажите о нас</span>

	<div class="ya-share2 uk-display-inline-block uk-margin-left uk-float-left" data-url="{{ url('/') }}" data-services="vkontakte,facebook,odnoklassniki,moimir,twitter,gplus" data-size="s" style="line-height: 18px"></div>
</div>
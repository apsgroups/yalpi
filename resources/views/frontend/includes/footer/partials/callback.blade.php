{!! Form::open(['route' => ['feedback.callback'], 'method' => 'post', 'class' => 'callback validate-form ajax-form uk-form']) !!}
    <div class="we-call-text uppercase uk-margin-bottom">
        Остались вопросы? Мы перезвоним!
    </div>

    <div class="uk-grid">
        <div class="uk-width-2-3">
            {!! Form::text('phone', null, ['id' => 'phone', 'class' => 'phone-mask uk-form-width-large required', 'placeholder' => 'Введите Ваш телефон']) !!}
        </div>

        <div class="uk-width-1-3">
            <button type="submit" class="uk-button">Отправить</button>
        </div>
    </div>

    {!! Form::hidden('area', 'footer') !!}
{!! Form::close() !!}
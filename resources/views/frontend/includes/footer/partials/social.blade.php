<div class="socials">
    @foreach(['vkontakte', 'fb', 'twitter', 'odnoklassniki', 'instagram', 'youtube'] as $social)
        @if($link = \Settings::get('social_'.$social))
        <!--noindex-->
        <a href="{{ $link }}" class="soc-link" rel="nofollow noopener">
            <span class="s-icon icon-{{ $social }}"></span>
        </a>
        <!--/noindex-->
        @endif
    @endforeach
</div>
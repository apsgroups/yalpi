<div class="worktime">
    <div class="phone">
        {{ Settings::get('phone') }}
    </div>

    <div>
        {!! implode(", ", explode("\n", Settings::get('worktime'))) !!}
    </div>
</div>
global.jQuery = require('jquery');

require('jquery-ui');

require('./../plugin/tokenize/jquery.tokenize.js');

require('./../plugin/fancybox/jquery.fancybox.js');

require('./plugin/icheck/icheck.min.js');

(function($) {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
    });

    toastr.options = {
        "closeButton": true,
        "debug": false,
        "progressBar": true,
        "positionClass": "toast-top-center",
        "onclick": null,
        "showDuration": "400",
        "hideDuration": "1000",
        "timeOut": "1000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };

    $('.image-manager').on('click', '.remove-img', function(e) {
        e.preventDefault();

        var $btn = $(this);

        $.ajax({
            url: '/admin/image/destroy/',
            data: {id: $btn.attr('data-id')},
            type: 'post',
            success: function(data) {
                $('[data-img-id='+$btn.attr('data-id')+']').remove();
            }
        });
    });

    $(".fancybox").fancybox();

    if($('.search-tags').length) {            
        $('.search-tags').tokenize({
            datas: '/admin/tags/search/',
            searchParam: 'query',
            valueField: 'id',
            textField: 'title',
            htmlField: 'title',
            searchMinLength: 1,
            autosize: true,
            sortable: true,
            placeholder: 'Начните вводить имя тега или укажите его id'
        });
    }

    if($('.search-products').length) {   
        $('.search-products').tokenize({
            datas: '/admin/product/search/',
            searchParam: 'query',
            valueField: 'id',
            textField: 'title',
            htmlField: 'title',
            searchMinLength: 1,
            autosize: true,
            sortable: true,
            placeholder: 'Начните вводить имя товаров или укажите его id или URL'
        });
    }

    if($('.search-related-content').length) {   
        $('.search-related-content').tokenize({
            datas: '/admin/content/search/',
            searchParam: 'query',
            valueField: 'id',
            textField: 'title',
            htmlField: 'title',
            searchMinLength: 1,
            autosize: true,
            sortable: true,
            placeholder: 'Начните вводить название статьи или укажите её id или URL'
        });
    }

    if($('.search-author').length) {   
        $('.search-author').tokenize({
            datas: '/admin/author/search/',
            searchParam: 'query',
            valueField: 'id',
            textField: 'title',
            htmlField: 'title',
            searchMinLength: 1,
            autosize: true,
            sortable: true,
            placeholder: 'Начните вводить имя автора или id, или URL'
        });
    }

    $('table.sortable tbody').sortable({
        helper: fixWidthHelper,

        update: function (event, ui) {
            var data = $(this).sortable('serialize');
            data += '&category_id='+ $("[name='filter[category_id]']").val();

            var url = '/admin/product/sort/';

            if($('table.sortable').hasClass('sets-ordering')) {
                url = '/admin/product/sets-ordering/';
            }

            $.ajax({
                data: data,
                type: 'POST',
                url: url
            });
        }
    }).disableSelection();

    $('table.sortable-sets-ordering tbody').sortable({
        helper: fixWidthHelper,

        update: function (event, ui) {
            var ordering = 0;

            $(this).find('.order-field').each(function() {
                $(this).val(ordering);
                ordering++;
            });
        }
    }).disableSelection();

    $('table.sortable-properties tbody').sortable({
        helper: fixWidthHelper,

        update: function (event, ui) {
            var data = $(this).sortable('serialize');

            $.ajax({
                data: data,
                type: 'POST',
                url: '/admin/properties/sort/'
            });
        }
    }).disableSelection();

    $('.sortable-rows').sortable({

        update: function (event, ui) {
            var data = $(this).sortable('serialize');
            console.log(data);
        }
    }).disableSelection();

    $('table.sortable-property-values tbody').sortable({
        helper: fixWidthHelper,

        update: function (event, ui) {
            var data = $(this).sortable('serialize');

            $.ajax({
                data: data,
                type: 'POST',
                url: '/admin/property-values/sort/'
            });
        }
    }).disableSelection();

    $('table.widget-sortable tbody').sortable({
        helper: fixWidthHelper,

        update: function (event, ui) {
            var data = $(this).sortable('serialize');

            // POST to server using $.post or $.ajax
            $.ajax({
                data: data,
                type: 'POST',
                url: '/admin/widget/sort/'
            });
        }
    }).disableSelection();

    function fixWidthHelper(e, ui) {
        ui.children().each(function() {
            $(this).width($(this).width());
        });
        return ui;
    }

    if($('.select-items').length) {
        $('.select-items').tokenize({
            autosize: true,
            sortable: true,
            placeholder: 'Выберите',
            displayDropdownOnFocus:true,
            nbDropdownElements: 50
        });
    }

    $('.cache-clear').click(function(e) {
        e.preventDefault();

        var $btn = $(this);

        $.ajax({
            url: $btn.attr('href'),
            beforeSend: function() {
                $btn.css('opacity', 0.5);
            },
            success: function(data) {
                $btn.css('opacity', 1);

                if(data === 'ok') {
                    toastr.success('Кэш очищен');
                    return;
                }

                toastr.error('Произошла ошибка: '+data);
            }
        });
    });

    function menuHide(val)
    {
        if (val < 2) {
            jQuery('#menuselect-group').hide();
        } else {
            jQuery('#menuselect-group').show();
        }
    }

    menuHide($('#form-assignment').val());

    $('#form-assignment').change(function() {
        menuHide($(this).val());
    });

    $('.icheck').iCheck({
        checkboxClass: 'icheckbox_flat-blue',
        radioClass: 'iradio_flat-blue'
    });

    $('.check-item').iCheck({
        checkboxClass: 'icheckbox_flat-blue',
        radioClass: 'iradio_flat-blue'
    });

    $('.check-item').on('ifChecked', function(event) {
        var id = $(this).val();
        $('<input type="hidden" value="'+id+'" name="items[]" />').appendTo('.action-form');
    });

    $('.check-item').on('ifUnchecked', function(event) {
        var id = $(this).val();
        $('[value='+id+']', '.action-form').remove();
    });

    $('.action-block').hide();

    $('.route', '.action-form').change(function() {
        var action = $('option:selected', this).attr('data-action');

        $('.action-block').hide();

        $('.action-'+action).show();
    });

    $('.action-form').submit(function(e) {
        var route = $('.route', this).val();

        if(!route) {
            e.preventDefault();
            return false;
        }

        if($("input[name='items[]']", this).length < 1) {
            toastr.error('Отметьте один или более элементов');
            e.preventDefault();
            return;
        }

        $(this).attr('action', route);

        return true;
    });

    //Enable check and uncheck all functionality
    $(".checkbox-toggle").click(function () {
        var clicks = $(this).data('clicks');
        if (clicks) {
            //Uncheck all checkboxes
            $(".check-item").iCheck("uncheck");
            $(".fa", this).removeClass("fa-check-square-o").addClass('fa-square-o');
        } else {
            //Check all checkboxes
            $(".check-item").iCheck("check");
            $(".fa", this).removeClass("fa-square-o").addClass('fa-check-square-o');
        }
        $(this).data("clicks", !clicks);
    });

    $('.promo-tab').sortable({
        handle: ".handle",
        items: "> .repeatable-row",
        stop: function( event, ui ) {
            var ordering = 0;

            $(this).find('.order-field').each(function() {
                $(this).val(ordering);
                ordering++;
            });
        }
    }).disableSelection();

    var freezeRow = function($check) {
        var readonly = !$check.prop('checked');

        $check.closest('.freeze-row').find('.freeze').prop('readonly', readonly);
    };

    $(document).on('change', '.freeze-input', function() {
        console.log('ok');
        freezeRow($(this));
    });

    $('.freeze-input').each(function() {
        freezeRow($(this));
    });

    $('[data-token-order]').each(function() {
        var order =  $.parseJSON($(this).attr('data-token-order'));

        console.log(order);

        if(!order.length) {
            return;
        }

        $(this).tokenize().clear();

        for(var i=0; i<order.length; i++) {
            var title = $(this).find('[value='+order[i]+']').text();
            $(this).tokenize().tokenAdd(order[i], title);
        }
    });

    if($('.select-tags').length) {
        $('.select-tags').tokenize({
            autosize: true,
            sortable: true,
            placeholder: 'Выберите',
            displayDropdownOnFocus:true,
            searchParam: 'term',
            datas: '/admin/tag/search'
        });
    }

    if($('#product-id').length) {
        var chosen = require('./../plugin/chosen/chosen.jquery.js');

        var id = parseInt($('#product-id').val());

        $.ajax({
            url: '/admin/product/ajax-list/'+id,
            success: function(data) {
                for (var key in data) {
                    $('.ajax-list[data-id='+key+']').empty().html(data[key]);

                    $('.ajax-list[data-id='+key+']').find('.chosen-select').chosen();
                }
            }
        });
    }
})(jQuery);


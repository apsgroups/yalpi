$(function() {
    var setContentId = function(item) {
        $('.content-id').val(item.value);
        $('.content-title').empty().text(item.text+' ('+item.value+')');
    };

    $('.content-search').typeahead({
        onSelect: function(item) {
            setContentId(item);
        },
        ajax: {
            url: "/admin/content/search/",
            preDispatch: function (query) {
                return {
                    query: query
                }
            },
            timeout: 500,
            displayField: "title",
            triggerLength: 1
        }
    });
});
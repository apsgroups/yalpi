$(function() {
    var productId = 0;

    var setProductIdToModule = function(id) {
        productId = id;
    };

    var addProductModule = function(id, qtt) {
        $('#module-status').empty();

        if(!id) {
            $('#module-status').html('<div class="text-danger">Не найден товар</div>');
            return false;
        }

        if(!qtt)
            qtt = 1;

        $.ajax({
            url: '/admin/product/module/'+id+'/',
            data: {quantity: qtt},
            dataType: 'html',
            404: function() {
                $('#module-status').html('<div class="text-danger">Товар не найден</div>');
            },
            error: function() {
                $('#module-status').html('<div class="text-danger">Произошла ошибка</div>');
            },
            success: function(data) {
                $(data).appendTo('#modules-table');
            }
        });
    };

    $('#module-manager').on('click', '.add', function() {
        addProductModule(productId, $('#module_qtt').val());
    });

    $('#module-manager').on('click', '.remove-row', function() {
        $(this).closest('tr').remove();
    });

    var calculatePrices = function(type) {
        var total = 0;

        $('.module-'+type+':not(.hide)').each(function() {
            total += parseFloat($(this).text()) * $(this).closest('tr').find('.module-quantity').val();
        });

        $('.module-total-'+type).empty().text(total);
    };

    var moduleAttribute = function(select) {
        var $row = $(select).closest('tr');

        var id = $(select).val();

        id = id == '0' ? 'default' : id;

        var fields = ['ext-id', 'sku', 'price', 'price-base', 'discount'];

        for(var i = 0; i <= fields.length; i++) {
            $('.module-'+fields[i], $row).addClass('hide');

            var show = 'default';
            if($('.module-'+fields[i]+'[data-attr-id='+id+']', $row).length) {
                show = id;
            }

            $('.module-'+fields[i]+'[data-attr-id='+show+']', $row).removeClass('hide');
        }

        calculatePrices('price');
        calculatePrices('price-base');
    };

    $('#module-manager').on('click', '.module-attribute', function() {
        moduleAttribute(this);
    });

    $('.module-attribute').each(function() {
        moduleAttribute(this);
    });

    $('#module-manager .search').typeahead({
        onSelect: function(item) {
            setProductIdToModule(item.value);
        },
        ajax: {
            url: "/admin/product/search/",
            timeout: 500,
            displayField: "title",
            triggerLength: 1
        }
    });
});
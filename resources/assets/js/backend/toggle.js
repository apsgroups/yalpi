$(function() {
    $(document).on('submit', '.toggle-form', function(e) {
        e.preventDefault();

        var $form = $(this);
        var $btn = $('.btn', $form);

        $.ajax({
            url: $form.attr('action'),
            type: 'post',
            data: $form.serialize(),
            dataType: 'json',
            beforeSend: function () {
                $btn.prop('disabled', true);
            },
            success: function(data) {
                $btn.prop('disabled', false);

                if(data.status === 'success') {
                    if(data.state === '1') {
                        $('input[name=value]', $form).val(0);
                        $('i', $btn).removeClass('glyphicon-remove').addClass('glyphicon-ok');
                        $btn.removeClass('btn-danger').addClass('btn-success');
                    } else {
                        $('input[name=value]', $form).val(1);
                        $('i', $btn).removeClass('glyphicon-ok').addClass('glyphicon-remove');
                        $btn.removeClass('btn-success').addClass('btn-danger');
                    }

                    toastr.success(data.msg);
                    return;
                }

                toastr.error(data.msg);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $btn.prop('disabled', false);
                toastr.error("Error: " + errorThrown);
            }
        });
    });
});
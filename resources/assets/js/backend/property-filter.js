window.$ = window.jQuery = require('jquery');
window.$ = $.extend(require('jquery-ui-bundle'));
var chosen = require('./../plugin/chosen/chosen.jquery.js');

$(function() {
  $('[name="filter[productTypes][]"]').chosen({width: '100%'});

  $('[name="filter[title]"]').autocomplete({
    source: function(request, response) {
      $.ajax({
        url: '/admin/property/by-title',
        data: {
          title: request.term,
        },
        success: function(data) {
          response($.map(data, function(item) {
            return item.title;
          }));
        },
      });
    },
  });

  $('[name="filter[value]"]').autocomplete({
    source: function(request, response) {
      $.ajax({
        url: '/admin/property/by-value',
        data: {
          value: request.term,
        },
        success: function(data) {
          response($.map(data, function(item) {
            return item.title;
          }));
        },
      });
    },
  });
});
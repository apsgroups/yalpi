$(function() {
    $('.form-type').hide();
    $('#type-'+$('#type-list').val()).show();

    $(document).on('change', '#type-list', function() {
        $('.form-type').hide();
        $('#type-'+$(this).val()).show();

        //$('.chosen-select').trigger("chosen:updated");
    });

    $(document).on('change', '.set-item-id', function() {
        var $select = $(this);
        $('#item-id').val($(this).val());
    });

    var setContentId = function(item) {
        $('#item-id').val(item.value);
        $('#contentTitle').empty().text(item.text+' ('+item.value+')');
    };

    $('#content-search').typeahead({
        onSelect: function(item) {
            setContentId(item);
        },
        ajax: {
            url: "/admin/content/search/",
            preDispatch: function (query) {
                return {
                    query: query,
                    contentTypeId: $('#content-id').val()
                }
            },
            timeout: 500,
            displayField: "title",
            triggerLength: 1
        }
    });
});
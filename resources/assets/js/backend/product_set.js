$(function() {
    $('#product-set-box').on('click', '.remove-row', function(e) {
        e.preventDefault();

        var id = $(this).attr('data-id');

        var $row = $(this).closest('tr');

        $.ajax({
            url: '/admin/product-set/product/'+id,
            type: 'delete',
            error: function() {
                toastr.error('Ошибка при удалении.');
            },
            success: function(response) {
                $row.remove();

                toastr.success('Товар удален из набора');
            }
        });
    });

    $('#product-set-box').on('click', '.update-set', function(e) {
        e.preventDefault();

        var id = $('#product-set-id').val();

        $.ajax({
            url: '/admin/product-set/'+id,
            type: 'put',
            data: {title: $('#set-edit-title').val(), property_id: $('#set-edit-property-id').val()},
            error: function() {
                toastr.error('Ошибка при сохранении.');
            },
            success: function(response) {
                $('#product-set-title').empty().text($('#set-edit-title').val());
                toastr.success('Набор сохранен');
            }
        });
    });

    $('#product-set-box').on('click', '.close-set', function(e) {
        e.preventDefault();

        $('#product-set-box').empty();

        toastr.success('Редактируемый товар не будет сохранен в закрытом наборе');
    });

    $('#product-set-box').on('click', '.remove-set', function(e) {
        e.preventDefault();

        var id = $('#product-set-id').val();

        $.ajax({
            url: '/admin/product-set/'+id,
            type: 'delete',
            error: function() {
                toastr.error('Ошибка при удалении.');
            },
            success: function(response) {
                $('#product-set-box').empty();
                toastr.success('Набор удален');
            }
        });
    });

    $('#product-set-manager').on('click', '.add', function(e) {
        e.preventDefault();

        $.ajax({
            url: '/admin/product-set/',
            type: 'post',
            data: {title: $('.search', '#product-set-manager').val(), property_id: $('#set_property_id').val()},
            error: function() {
                $('.status', '#product-set-manager').empty();
                toastr.error('Ошибка при создании.');
            },
            success: function(response) {
                $('.status', '#product-set-manager').empty();

                showProductSet(response.id);

                toastr.success('Набор создан');
            }
        });
    });

    var showProductSet = function(id) {
        $.ajax({
            url: '/admin/product-set/'+id,
            error: function() {
                $('.status', '#product-set-manager').empty();
                toastr.error('Набор не найден');
            },
            success: function(response) {
                $('#product-set-box').empty().html(response);

                bindProductSearch();
            }
        });
    };

    $('.search', '#product-set-manager').typeahead({
        onSelect: function(item) {
            showProductSet(item.value);
        },
        ajax: {
            url: '/admin/product-set/search/',
            timeout: 500,
            displayField: 'title',
            valueField: 'id',
            triggerLength: 1,
            preProcess: function(response) {
                if(!response.length) {
                    $('.status', '#product-set-manager').empty().html('Набор с заданным именем не найден. <a href="#" class="add">Создать</a>?');
                    return;
                }

                $('.status', '#product-set-manager').empty();

                return response;
            }
        }
    });

    var addProductToSet = function(id) {
        var setId = $('#product-set-id').val();

        $.ajax({
            url: '/admin/product-set/product/'+setId+'/'+id+'/',
            type: 'post',
            success: function(response) {
                $('#set-table').append(response);
            }
        });
    };

    var bindProductSearch = function() {
        $('.search', '#product-set-box').typeahead({
            onSelect: function(item) {
                addProductToSet(item.value);
            },
            ajax: {
                url: '/admin/product-set/search-product/',
                timeout: 500,
                displayField: 'title',
                valueField: 'id',
                triggerLength: 1
            }
        });
    };

    var syncProductSetTitle = function() {
        var title = $('#title').val();

        $('#product-set-title-helper').empty().text(title);
    };

    $('#title').change(function() {
        syncProductSetTitle();
    });

    $('#product-set-title-helper').click(function(e) {
        e.preventDefault();

        $('.search', '#product-set-manager').val($(this).text()).trigger('keyup');
    });

    bindProductSearch();

    syncProductSetTitle();
});
global.jQuery = require('jquery');

(function($) {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
    });

    var setStatus = function(message, box) {
        $('.import-status', box).empty().text(message);
    };

    var step = 0;

    var process = function(type) {
        var box = '#import-box-'+type;

        $.ajax({
            url: '/admin/import/run/'+type+'/',
            dataType: 'json',
            cache: false,
            beforeSend: function() {
                $('.progress-bar', box).addClass('active');

                if(step == 0) {
                    $('.progress-bar', box).width('100%');
                    setStatus('Получение данных. Это может занять несколько минут.', box);
                }
            },
            error: function() {
                setStatus('Произошла ошибка');
            },
            success: function(response) {
                step = response.step;

                var percent = Math.round(response.processed / (response.total / 100));
                setStatus(percent+'% ('+response.processed+'/'+response.total+')', box);

                $('.progress-bar', box).width(percent+'%');

                if(response.processed >= response.total) {
                    setStatus('Импорт завершен!', box);
                    $('.progress-bar', box).removeClass('active');
                    return;
                }

                process(type);
            }
        });
    };

    $('.run-import').click(function() {
        $(this).remove();

        process($(this).attr('data-type'));
    });

    $('.report-delete').click(function(e) {
        e.preventDefault();

        var id = $(this).attr('data-id');

        $.ajax({
            url: '/admin/import/report/destroy/'+id+'/',
            type: 'post',
            success: function(data) {
                $('#report-'+id).remove();
            }
        });
    });

    $('.report-clean').click(function(e) {
        e.preventDefault();

        var operation = $(this).attr('data-operation');

        $.ajax({
            url: '/admin/import/report/clean/'+operation+'/',
            type: 'post',
            success: function(data) {
                $('.report-list[data-operation='+operation+']').empty();
            }
        });
    });
})(jQuery);
(function($) {
    $('#fine-uploader-gallery').fineUploader({
        template: 'qq-template-gallery',
        request: {
            params: {
                '_token': $('meta[name="_token"]').attr('content'),
                'model': imgMorph.type,
                'model_id': imgMorph.id
            },
            endpoint: '/admin/image/upload/'
        },
        thumbnails: {
            placeholders: {
                waitingPath: '/js/plugin/fine-uploader/placeholders/waiting-generic.png',
                notAvailablePath: '/js/plugin/fine-uploader/placeholders/not_available-generic.png'
            }
        },
        validation: {
            allowedExtensions: ['jpeg', 'jpg', 'gif', 'png']
        }
    });
})(jQuery);
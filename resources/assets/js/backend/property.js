global.jQuery = require('jquery');

require('jquery-ui');

require('./../plugin/tokenize/jquery.tokenize.js');
var chosen = require('./../plugin/chosen/chosen.jquery.js');

(function($){
    var setOrdering = function() {
        var ordering = 0;

        $('.property-list').find('.order-field').each(function() {
            $(this).val(ordering);
            ordering++;
        });
    };

    var setPropertyType = function(id, type) {
        $.ajax({
            url: '/admin/property/load-manager/',
            data: {id: id, type: type},
            success: function(data) {
                $('#property-manager').empty().html(data);
            }
        });
    };

    $(document).on('change', '.type-switcher', function() {
        setPropertyType($('#property-id').val(), $(this).val());
    });

    var repeatableOrder = function($repeatable) {
        $('.repeatable-row', $repeatable).each(function(i) {
            $('select, input, textarea', $(this)).each(function() {
                try {
                    var name = $(this).attr('name').replace(/\[\d+\]\[/, '['+i+'][');
                    $(this).attr('name', name);
                } catch (error) {
                }
            });
        });
    };

    $(document).on('click', '.repeatable .add', function() {
        var $repeatable = $(this).closest('.repeatable');

        var $newRow = $('.repeatable-row:first', $repeatable).clone(true);

        if(!$(this).hasClass('last')) {
            $newRow.insertAfter($(this).closest('.repeatable-row'));
        } else {
            $newRow.insertAfter('.repeatable-row:last', $repeatable);
        }

        $newRow.find('input, select, textarea').val('');

        $('.no-clone', $newRow).remove();

        $('.cke', $newRow).remove();

        repeatableOrder();

        $newRow.find('.chosen-select').removeClass("chzn-done").removeAttr("id").css("display", "block").next().remove();
        $newRow.find('.chosen-select').chosen();

        if($('textarea', $newRow).hasClass('ckeditor')) {
            var id = new Date().getUTCMilliseconds();

            $('textarea', $newRow).attr('id', 'ckeditor-'+id).attr('css', '');

            CKEDITOR.replace('ckeditor-'+id, {
                filebrowserBrowseUrl: "https://mebel169.ru/elfinder/ckeditor/",
                removeDialogTabs: 'link:upload;image:upload',
                width: '100%'
            });
        }
        setOrdering();
    });

    $(document).on('click', '.repeatable .remove-row', function() {
        var $repeatable = $(this).closest('.repeatable');
        var $row = $(this).closest('.repeatable-row');

        if ($('.repeatable-row', $repeatable).length === 1) {
            $('input, select', $row).val('');
            return;
        }

        $row.remove();
        repeatableOrder();
        setOrdering();
    });

    $('.property-list').sortable({
        handle: ".handle",
        items: "> .repeatable-row",
        stop: function( event, ui ) {
            setOrdering();
        }
    }).disableSelection();

    if($('.search-property-values').length) {
        $('.search-property-values').tokenize({
            datas: '/admin/property-values/search/',
            searchParam: 'query',
            valueField: 'id',
            textField: 'title',
            htmlField: 'title',
            searchMinLength: 1,
            autosize: true,
            sortable: true,
            placeholder: 'Начните вводить наименование свойства или укажите его id'
        });
    }
})(jQuery);
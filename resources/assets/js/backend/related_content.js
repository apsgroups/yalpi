$(function() {
    var addContent = function(item) {
        var $item = $('<div />')
            .attr('id', 'related-content-'+item.value)
            .text(item.text)
            .addClass('related-content-item')
            .appendTo('.related-content');

        $('<input />')
            .attr({'type': 'hidden', 'name': 'related_content['+item.value+'][id]'}) 
            .val(item.value)
            .appendTo($item);

        $('<input />')
            .attr({'type': 'hidden', 'name': 'related_content['+item.value+'][ordering]', 'class': 'related-content-ordering'}) 
            .val($('.related-content-ordering:last').val() + 1)
            .appendTo($item);

        $('<button />')
            .attr({'type': 'button', 'class': 'related-content-remove btn btn-danger btn-sm'}) 
            .html('<i class="glyphicon glyphicon-remove"></i>')
            .appendTo($item); 
    };

    $('.related-content').on('click', '.related-content-remove', function(e) {
        var $item = $(e.target).closest('.related-content-item').remove();
    });

    $('.content-search').typeahead({
        onSelect: function(item) {
            addContent(item);
        },
        ajax: {
            url: "/admin/content/search/",
            preDispatch: function (query) {
                return {
                    query: query
                }
            },
            timeout: 500,
            displayField: "title",
            triggerLength: 1
        }
    });
});
$(function() {
    $('.form-type').hide();
    $('#type-'+$('#type-list').val()).show();

    $(document).on('change', '#type-list', function() {
        $('.form-type').hide();
        $('#type-'+$(this).val()).show();
    });

    $(document).on('change', '.set-item-id', function() {
        var $select = $(this);
        $('#item-id').val($(this).val());
    });
});
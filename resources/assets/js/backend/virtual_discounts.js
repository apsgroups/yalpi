global.jQuery = require('jquery');

(function($) {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
    });

    var setStatus = function(message) {
        $('.action-status').empty().text(message);
    };

    var process = function(page, step) {
        var $progress = $('.progress-bar');

        var data = {step: step};

        if(page > 1) {
            data['page'] = page;
        }

        $.ajax({
            url: '/admin/virtual-discounts/run/',
            data: data,
            dataType: 'json',
            beforeSend: function() {
                $progress.addClass('active');

                if(page === 1 && step === 1) {
                    $progress.width('100%');
                    setStatus('Генерирую скидки для всех товаров');
                }

                if(page === 1 && step === 2) {
                    $progress.width(0);
                    setStatus('Генерация скидок для модулей');
                }
            },
            error: function() {
                setStatus('Произошла ошибка');
                $progress.removeClass('active');
            },
            success: function(response) {
                if(typeof response.error != 'undefined') {
                    setStatus(response.error);
                    $progress.removeClass('active');
                    return;
                }

                var processed = response.per_page * page;

                var percent = Math.round(processed / (response.total / 100));

                setStatus(percent+'% ('+processed+'/'+response.total+')');

                $progress.width(percent+'%');

                if(step === 2 && processed >= response.total) {
                    setStatus('Генерация скидок завершена!');
                    $progress.removeClass('active');
                    return;
                }

                page++;

                if(step === 1 && processed >= response.total) {
                    step = 2;
                    page = 1;
                }

                process(page, step);
            }
        });
    };

    $('.run-virtual-discounts').click(function() {
        $(this).remove();

        process(1, 1);
    });
})(jQuery);
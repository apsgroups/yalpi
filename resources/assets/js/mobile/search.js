global.jQuery = require('jquery');

var $ = window.jQuery;

$(function() {
    var delay = null;
    var menu = null;

    $('.search-input-menu').on('focus', function() {
        var $input = $(this);

        var $form = $(this).closest('form');

        if($form.find(".autocomplete-results").length) {
            $form.find(".autocomplete-results").addClass("uk-display-block");
        } else if($input.val() !== '') {
            searchQuery($input);
        } else {
            loadMenu($form);
        }
    });

    var loadMenu = function($form) {
        if(menu !== null) {
            $(menu).appendTo($form.find('.search-holder')).addClass('visible');
            return;
        }

        $.ajax({
            url: '/search/menu/',
            success: function(data) {
                $(data).appendTo($form.find('.search-holder')).addClass('visible');
                menu = data;
            }
        });
    }

    var searchQuery = function($input) {
        var $form = $input.closest('form');

        clearTimeout(delay),

        delay = setTimeout(function() {
            if(!$input.val().length) {
                $form.find(".search-reset").hide();
                hideAutomplete();
                return;
            }
            
            $form.find(".search-reset").show();
                        
            if($input.val().length <= 2) {
                hideAutomplete();
                return;
            }

            $.ajax({
                url: '/search/autocomplete/',
                data: {term: $input.val()},
                success: function(data) {
                    $form.find('.search-holder').empty();

                    if(data) {
                        $(data).appendTo($form.find('.search-holder')).addClass('visible');
                        return;
                    }
                }
            });
        }, 300);
    }

    $('.search-input').on('input', function() {
        var $input = $(this);

        searchQuery($input);
    });

    var hideAutomplete = function() {
        $(".search-reset").hide();
        $(".search-holder").empty();
    }
});
$(function() {
	$('.index > li > .toggle', '#main-menu').click(function() {
		$li = $(this).parent();

		if($li.hasClass('opened')) {
			$li.find('> ul').slideUp('fast', function() {
				$li.removeClass('opened')
			});
		} else {
			$li.find('> ul').slideDown('fast', function() {
				$li.addClass('opened')
			});
		}
	});

	$('.index ul .toggle', '#main-menu').click(function() {
		$li = $(this).parent();

		$('#main-menu-header').empty().text($(this).prev().text());

		$li.closest('ul').addClass('opened');

		$('.current-active', '#main-menu').removeClass('current-active');

		$li.addClass('active current-active');

		$('.index', '#main-menu').addClass('opened');

		$('#main-menu-header').addClass('back');
	});

	$('#main-menu-header').click(function(e) {
		$li = $('.current-active', '#main-menu');

		$li.removeClass('active current-active');

		$li.parent().removeClass('opened');

		$li.parent().parent().addClass('current-active');

		title = $('.current-active', '#main-menu').find('> *').first().text();

		if($li.parent().parent().parent().hasClass('index')) {
			$('.index', '#main-menu').removeClass('opened');
			$('#main-menu-header').removeClass('back');
			title = '';
		}

		$('#main-menu-header').empty().text(title);
	});
});
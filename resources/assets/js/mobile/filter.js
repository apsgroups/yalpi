global.jQuery = require('jquery');

var $ = window.jQuery;
var updateProducts = require('./../frontend/update-products.js');

(function ($) {
    var excludeParams = function($form) {
        var exclude = [];

        var $disabled = $form.find('input:disabled');

        $disabled.prop('disabled', false);

        var data = $form.find('input').not(exclude.join(', ')).serialize();

        $disabled.prop('disabled', true);

        return data;
    };

    var applyFilter = function() {
        var $form = $('#filter-form-modal');
        
        var data = excludeParams($form);

        var beforeLoad = function() {
            $('#sort-links-modal').remove();
        };

        var afterLoad = function(data) {
            $('.total-filtered').empty();

            if(data.total_filtered && $('input:checked', '#filter-form').length) {
                $('.total-filtered').html(data.total_filtered);;
            }

            filterOptions();
            activateOptions();
        };

        updateProducts($form.attr('action'), data, beforeLoad, afterLoad);
    };

    var syncCheckboxes = function(name, property) {
        var $box = $('#'+property.position+'-property');
        
        var $field = $box.find('#filter-field-'+name);

        if(!property.total) {
            $field.remove();
            return;
        }

        if(!$field.length) {
            $field = $('<div />').addClass('option-row filter-field').attr('id', 'filter-field-'+name).attr('data-id', name).appendTo($box);
            $('<div class="option-title">'+property.title+'</div>').appendTo($field);
            $('<div class="option-body" />').appendTo($field);
        }

        $field.find('.checkbox-row').addClass('dirty');

        $.each(property.options, function(k, opt) {
            if(!opt.active) {
                return;
            }

            var uid = 'checkbox-row-'+name+'-'+opt.id;

            var checkId = 'input-'+name+'-'+opt.id;

            if($('#'+uid).length) {
                $('#'+uid).remove();
            }

            var labelText = opt.title;

            if(name === 'rate') {
                if(opt.id > 0) {
                    var img = '<img src="/images/layout/star.svg" alt="'+opt.title+'" />';
                    labelText = img.repeat(opt.id);
                } else {
                    labelText = 'Еще нет отзывов';
                }
            }

            var html = '<div id="'+uid+'" class="checkbox-lbl rate-lbl">'+
                            '<input type="checkbox" name="filter['+name+'][]" value="'+opt.id+'" id="'+checkId+'" class="checkbox" />'+
                            '<label for="'+checkId+'">'+labelText+'</label>'
                        '</div>';

            $(html).appendTo($field.find('.option-body'));

            $('#'+checkId).prop('checked', opt.selected);
            // $('#'+checkId).prop('disabled', !opt.active);
        });

        $field.find('.dirty').remove();

        $field.removeClass('dirty');
    }

    var filterOptions = function(pageLoaded) {
        var $form = $('#filter-form-modal');

        if(!$form.length || $form.hasClass('no-options')) {
            return;
        }

        var types = ['primary', 'secondary', 'teaching', 'rate'];

        var q = pageLoaded ? location.search.replace('?', '') + '&category_id='+$('#filter-category-id').val() : $form.serialize();

        $.ajax({
            url: '/filter/options/',
            dataType: 'json',
            data: q,
            success: function(data) {
                $('.filter-field', '#filter-popup').addClass('dirty');

                $.each(data.options, function(name, item) {
                    syncCheckboxes(name, item);
                });

                $('.filter-field.dirty', '#filter-popup').remove();

                $('.option-group', '#filter-popup').each(function() {
                    var id = $(this).attr('data-id');

                    if($('.filter-field', this).length) {
                        $(this).removeClass('uk-hidden');
                    } else {
                        $(this).addClass('uk-hidden');
                    }
                });

                $('#filter-total-text').empty().html(data.total_text);

                if(pageLoaded) {
                    activateOptions();
                }
            }
        });
    };

    $('#filter-form-modal').submit(function(e) {
        e.preventDefault();
        applyFilter();
    });

    let resetOption = function($option) {
        $option.find('input[type=checkbox]').prop('checked', false);
        removeOptionFromSelected($option);
    }

    let resetFilter = function() {
        $('#filter-form-modal').trigger('reset');

        $('.option-row', '#filter-form-modal').each(function() {
            resetOption($(this));
        });

        $('.filter-reset-btn').remove();

        filterOptions();

        applyFilter();
    }

    let resetButtons = function() {
        let selected = $('.option-row.active', '#filter-form-modal').length;

        if(!selected) {
            $('.filter-reset-btn').remove();
            return;
        }

        let $smBtn = $('#filter-reset-sm');

        if(!$smBtn.length) {
            $smBtn = $('<span id="filter-reset-sm" class="filter-reset-btn" />').text('Очистить').appendTo('#selected-options');

            $smBtn.click(function() {
                resetFilter();
            });
        } else {
            $smBtn.appendTo('#selected-options');
        }

        let $lgBtn = $('#filter-reset-lg');

        if(!$lgBtn.length) {
            $lgBtn = $('<span id="filter-reset-lg" class="filter-reset-btn" />').text('Очистить всё').appendTo('#filter-reset-row');

            $lgBtn.click(function() {
                resetFilter();
            });
        }
    }

    let addOptionToSelected = function($option) {
        let id = $option.attr('data-id');

        let attrId = 'selected-option-'+id;
        let title = $('.option-title', $option).text();

        if($('#'+attrId).length) {
            return;
        }

        let $btn = $('<span id="'+attrId+'" />').text(title).appendTo('#selected-options');

        $btn.click(function() {
            resetOption($option);
            applyFilter();
        });

        $('#selected-options').addClass('active');

        resetButtons();
    }

    let removeOptionFromSelected = function($option) {
        let id = $option.attr('data-id');
        
        let attrId = 'selected-option-'+id;

        $('#'+attrId).remove();

        $option.removeClass('opened');
        $option.find('.option-body').stop().slideUp('fast');

        resetButtons();

        if(!$('span', '#selected-options').length) {
            $('#selected-options').removeClass('active');
        }
    }

    let changeOptionStatus = function($option) {
        if($option.find('input:checked').length > 0) {
            $option.addClass('active');
            addOptionToSelected($option);
        } else {
            $option.removeClass('active');
            removeOptionFromSelected($option);
        }

        let $group = $option.closest('.option-group');

        if($group.find('.filter-field.active').length) {
            $group.addClass('active');
        } else {
            $group.removeClass('active');
        }
    }

    $('#filter-form-modal').on('change', 'input[type=checkbox]', function() {
        changeOptionStatus($(this).closest('.filter-field'));
        filterOptions();
    });

    let activateOptions = function() {
        $('.filter-field', '#filter-form-modal').each(function() {
            changeOptionStatus($(this));
        });
    }

    $('#filter-form-modal').on('click', '.option-title', function(e) {
        e.preventDefault();

        let $parent = $(this).closest('.option-row');

        if($parent.hasClass('opened')) {
            $parent.removeClass('opened');
            $parent.find('> .option-body').stop().slideUp('fast');
        } else {
            $parent.addClass('opened');
            $parent.find('> .option-body').stop().slideDown('fast');
        }
    });

    $('body').on('click', '#select-sorting', function(e) {
        e.preventDefault();

        $('#sort-links-modal').appendTo('body');
        $('#sort-links-modal').addClass('active');
    });

    $('body').on('click', '#sort-links-close', function(e) {
        e.preventDefault();

        $('#sort-links-modal').removeClass('active');
    });

    $('body').on('click', '#sort-links a', function(e) {
        $('#sort-links-modal').removeClass('active');
    });

    $('body').on('click', '.open-filter', function(e) {
        Offcanvas.show('#filter-modal', {mode: 'none'});
        $('body').addClass('filter-active');
    });

    $('#filter-header a').click(function(e) {
        e.preventDefault();

        Offcanvas.hide('#filter-modal');
        $('body').removeClass('filter-active');
    });

    $('#filter-form-modal').submit(function(e) {
        Offcanvas.hide('#filter-modal');
        $('body').removeClass('filter-active');
    });

    $('.filter-reset-btn').click(function(e) {
        resetFilter();
    });

    filterOptions(true);
}(jQuery));
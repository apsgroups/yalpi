global.jQuery = require('jquery');

var $ = window.jQuery;

require('jquery-validate');
require('jquery-validate');
require('maskedinput');

$(function() {
    var ajaxSubmit = function(form) {
        $.ajax({
            url: form.action,
            type: form.method,
            processData: false,
            contentType: false,
            data: new FormData(form),
            dataType: 'json',
            beforeSend: function() {
                $('.uk-alert', form).remove();

            	var $btn = $('button', form);
                $btn.css('min-width', $btn.width());
                $btn.attr('data-text', $btn.text());
                $btn.prop('disabled', true).empty().html('<i class="uk-icon-spinner uk-icon-spin"></i>');
            },
            error: function(xhr, status, errorThrown) {
                $('button', form).prop('disabled', false).empty().html($('button', form).attr('data-text'));

                if(xhr.status === 422) {
                    var $alert = $('<div class="uk-margin uk-alert uk-alert-danger" />').prependTo(form);

                    for(var error in xhr.responseJSON) {
                        for (var i = 0; i < xhr.responseJSON[error].length; i++) {
                            $alert.html('<div>'+xhr.responseJSON[error][i]+'</div>');
                        }
                    }
                }
            },
            success: function(response) {
                $('button', form).prop('disabled', false).empty().html($('button', form).attr('data-text'));

                if(typeof response.redirect !== 'undefined') {
                    window.location.href = response.redirect;

                    return;
                }

                if(typeof response.errors !== 'undefined' && response.errors.length) {
                    var $alert = $('<div class="uk-margin uk-alert uk-alert-danger" />').prependTo(form);

                    for(var i in response.errors) {
                        $alert.html('<div>'+response.errors[i]+'</div>');
                    }

                    return;
                }

                $(form).empty().html(response.msg);
            }
        });
    };

    // Limit the number of files in a FileList.
    $.validator.addMethod( "maxfiles", function( value, element, param ) {
        if ( this.optional( element ) ) {
            return true;
        }

        if ( $( element ).attr( "type" ) === "file" ) {
            if ( element.files && element.files.length > param ) {
                return false;
            }
        }

        return true;
    }, $.validator.format("Выберите не более {0} файлов."));

    $.validator.addMethod("maxsize", function( value, element, param ) {
        if (this.optional(element) ) {
            return true;
        }

        param = 1024*1024*param;

        if ( $( element ).attr( "type" ) === "file" ) {
            if ( element.files && element.files.length ) {
                for ( var i = 0; i < element.files.length; i++ ) {
                    if ( element.files[ i ].size > param ) {
                        return false;
                    }
                }
            }
        }

        return true;
    }, $.validator.format("Максимальный размер файла {0} MB.") );


    $.validator.addClassRules("maxfiles-validate", {
         maxfiles: 3,
         maxsize: 4
    });

    var bindForms = function(element) {
        $('.validate-form', element).each(function() {
            $(this).validate({
                errorClass: "uk-form-danger",
                highlight: function (element, errorClass, validClass) {
                    if($('label[for='+element.id+']').length) {
                        $('label[for='+element.id+']').addClass('error-lbl');
                    }

                    $(element).addClass(errorClass);
                },
                unhighlight: function (element, errorClass, validClass) {
                    if($('label[for='+element.id+']').length) {
                        $('label[for='+element.id+']').removeClass('error-lbl');
                    }

                    $(element).removeClass(errorClass);
                },
                errorPlacement: function(error, element) {
                    if(element.hasClass('maxfiles-validate')) {
                        $(error).appendTo(element.parent());
                    }
                },
                submitHandler: function(form) {
                    if($(form).hasClass('ajax-form')) {
                        ajaxSubmit(form);

                        return false;
                    }

                    var $btn = $('button', form);
                    $btn.css('min-width', $btn.width());

                    $btn.prop('disabled', true).empty().html('<i class="uk-icon-spinner uk-icon-spin"></i>');

                    return true;
                }
            });
        });

        $(".phone-mask").mask('+7(999) 999-99-99?99999');

        $('.phone-mask').on('keydown.mask reset.mask blur.mask keypress.mask', function() {
            var $note = $(this).parent().find('.phone-note');

            if(!$note.length && $(this).val().length > 17) {
                $('<div />').addClass('phone-note').text('Превышено количество символов').insertAfter($(this));
            } else if($note.length && $(this).val().length <= 17) {
                $note.remove();
            }
        });

        $('.inputfile').change(function() {
            $('.inputfile-list').empty();

            if($(this).valid()) {                    
                var files = $(this).prop('files');

                for (var i = 0; i < files.length; i++) {
                    $('<div>'+files[i].name+'</div>').appendTo('.inputfile-list');
                }    
            }     
        });
    };

    bindForms(document);

    $('body').on('click', '.comment .reply-btn', function() {
        $(this).addClass('uk-hidden');
        
        var $comment = $(this).closest('.comment');

        var id = $comment.attr('data-id');

        $.ajax({
            url: '/comments/'+id+'/reply/',
            type: 'get',
            dataType: 'html',
            success: function(response) {
                var $form = $(response).appendTo($comment);
                bindForms($form.parent());
            }
        });
    });

    $('body').on('click', '.btn-popup:not(.popup-active)', function(e) {
        e.preventDefault();

        $(this).addClass('popup-active');

        var $btn = $(this);

        var formData = {};

        $.ajax({
            url: $btn.attr('href'),
            data: formData,
            success: function(response) {
                if($('#dialog-content').length) {
                    $('#dialog-content').empty().html(response);
                    bindForms($('#dialog-content'));
                    return;
                }

                var modal = UIkit.modal.dialog($('<a class="uk-modal-close uk-close"></a><div id="dialog-content">'+response+'</div>'), {bgclose: true, modal: false});

                modal.on('show.uk.modal', function() {
                    $('.uk-modal').addClass($btn.attr('data-modal-class'));

                    bindForms(modal.element);
                });

                modal.show();

                $btn.removeClass('popup-active');
            }
        });
    });
});
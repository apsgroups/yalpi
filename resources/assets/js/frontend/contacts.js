global.jQuery = require('jquery');

var $ = window.jQuery;

$(function() {
	if ($('.contacts-container').length) {
		if ($('#content-container .metro .kuncevo.active').length) {
			$('#content-container')
				.css({
					'background': 'url(\'/images/layout/kuncevo_contacts_bg2.jpg\') 0 128px no-repeat',
					'background-size': '2050px auto'
				});
		}

		if ($('#content-container .metro .medvedkovo.active').length) {
			$('#content-container')
				.css({
					'background': 'url(\'/images/layout/medvedkovo_contacts_bg.jpg\') 0 128px no-repeat',
					'background-size': '2050px auto'
				});
		}

	}
	$('*[uk-switcher] li').click(function () {
		$(this).siblings().removeClass('uk-open');
		$(this).addClass('uk-open');
		var switch_el = $(this).parent().next('ul').children('li');
		switch_el.hide();
		$(switch_el[$(this).index()]).show();
	});

	$('*[uk-switcher] li:eq(0)').click();

	$('.interactive_popup .close, #interactive_contacts_background').click(function () {
		$('.interactive_popup_btn').removeClass('active');
		$('#interactive svg path, #interactive svg ellipse').attr('style', 'fill: rgba(255, 255, 255, 0.3);');
		$('#interactive svg path#interactive_contacts_background').attr('style', 'fill: rgba(255, 255, 255, 0.5);');
	    $('.interactive_popup').hide();
	});

	$('.interactive_popup_btn').click(function () {
		$('.interactive_popup .close').click();
		$($(this).attr('xlink:href')).show();
		$(this).addClass("active");

		$('#interactive svg path, #interactive svg ellipse').attr('style', 'fill: rgba(0, 0, 0, 0.5);');
		return false;
	});
});
global.jQuery = require('jquery');

require('jquery-validate');

var $ = window.jQuery;

$(function() {
    $("#checkout-form").validate({
        submitHandler: function(form) {
            $('button', form).prop('disabled', true);
            return true;
        },
        errorPlacement: function(error, element) { },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass("uk-form-danger");

            $(element.form).find("label[for=" + element.id + "]")
                .addClass("uk-text-danger");
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass("uk-form-danger");

            $(element.form).find("label[for=" + element.id + "]")
                .removeClass("uk-text-danger");
        },
        city: {
            required: function(element){
                return $("#shipping_id").val() === '1';
            }
        },
        street: {
            required: function(element){
                return $("#shipping_id").val() === '1';
            }
        },
        home: {
            required: function(element){
                return $("#shipping_id").val() === '1';
            }
        },
        apartment: {
            required: function(element){
                return $("#shipping_id").val() === '1';
            }
        }
    });

    var updateMiniCart = function(amount, word) {
        var text = amount > 0 ? amount : 0;
        $('.status', '#minicart').empty().html(text);
    };

    $('body').on('click', '.addtocart', function(e) {
        e.preventDefault();

        var $productId = $(this).attr('data-product-id');
        var cartData = {'product_id': $productId, properties: [], attribute: null};

        $('.product-property.active').each(function() {
            var property = {
                id: $(this).parent().attr('data-property-id'),
                value: $(this).attr('data-property-value-id')
            };

            cartData.properties.push(property);
        });

        if($('.button-attribute.active').length) {
            cartData.attribute = $('.button-attribute.active').attr('data-id');
        }

        var $desk = $('#selected-desk-id[data-product-id='+$productId+']');
        if($desk.length) {
            cartData.desk = $desk.val();
        }

        $.ajax({
            url: '/cart/add/',
            data: cartData,
            type: 'post',
            dataType: 'json',
            success: function(data) {
                updateMiniCart(data.amount, data.word);

                var modal = UIkit.modal.dialog($('<a class="uk-modal-close uk-close"></a>'+data.content), {bgclose: true, modal: false});

                modal.show();
            }
        });
    });

    var xhrSubmitCart;

    var submitCart = function() {
        var $form = $('.cart-form');

        if(xhrSubmitCart && xhrSubmitCart.readystate != 4){
            xhrSubmitCart.abort();
        }

        xhrSubmitCart = $.ajax({
            url: $form.attr('action'),
            data: $form.serialize(),
            type: 'post',
            dataType: 'json',
            success: function(data) {
                updateDetailCart(data);
            }
        });
    };

    var updateDetailCart = function(data) {
        updateMiniCart(data.amount, data.word);

        if(data.amount >= 1) {
            $('.cart-cost').html(data.cost);
        } else {
            $('.cart-page').hide();
            location.reload();
        }

        for (var key in data.products) {
            if (data.products.hasOwnProperty(key)) {
                $("[data-total-price='"+key+"']").empty().text(data.products[key].cost);

                if(data.products[key].modules) {
                    $.each(data.products[key].modules, function(i, v) {
                        var $modulueBox = $("[data-modules='"+key+"']");
                        $('[data-module-quantity='+v.id+']', $modulueBox).empty().text(v.amount);
                        $('[data-module-price='+v.id+']', $modulueBox).empty().text(v.price);
                    });
                }
            }
        }
    };

    var removeFromCart = function(id) {
        $.ajax({
            url: '/cart/remove/',
            data: {id: id},
            type: 'post',
            dataType: 'json',
            success: function(data) {
                updateDetailCart(data);
            }
        });
    };

    $('.quantity-control .plus, .quantity-control .minus', document).bind('click', function() {
        var $btn = $(this);

        var $input = $(this).parent().find('.amount');

        var amount = $input.val();

        if($btn.hasClass('plus')) {
            amount++;
        } else if($btn.hasClass('minus') && amount > 1) {
            amount--;
        } else {
            amount = 1;
        }

        $input.val(amount);
    });

    $('.quantity-control .amount', document).bind('change', function() {
        submitCart();
    });

    $('.cart-form', document).bind('submit', function(e) {
        e.preventDefault();
        submitCart();
    });

    $('.remove-cart', document).bind('click', function() {
        var id = $(this).attr('data-product');
        $("[data-modules-row-holder='"+id+"']").remove();
        $("[data-cart-product='"+id+"']").remove();

        removeFromCart(id);
    });

    $('.toggle-modules', document).bind('click', function() {
        var id = $(this).closest('.product-row').attr('data-cart-product');
        $("[data-modules='"+id+"']").stop().slideToggle();
    });

    if($('#time-reserve').length > 0) {
        var remain = parseInt($('#time-reserve').attr('data-remein'));

        function parseTime(timestamp){
            if (timestamp <= 0) {
                timestamp = 0;
            }

            var hour = Math.floor(timestamp/60/60);
            var mins = Math.floor((timestamp - hour*60*60)/60);
            var secs = Math.floor(timestamp - hour*60*60 - mins*60);

            $('.minutes', '#time-reserve').text(mins);
            $('.sec', '#time-reserve').text(secs);
        }

        var intervalId = setInterval(function(){
            remain = remain - 1;
            parseTime(remain);
            if(remain <= 0){
                $('#time-reserve').remove();
                clearInterval(intervalId);
            }
        }, 1000);
    }
});
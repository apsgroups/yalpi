global.jQuery = require('jquery');

var $ = window.jQuery;

$(function() {
	var bindRemove = function() {
		$('a', '#notifications').click(function(e) {
			var id = $(this).attr('data-id');

			$.post('/notifications/'+id+'/');
		});
	}

	$('#notifications').on('show.uk.dropdown', function() {
		if($(this).hasClass('loaded')) {
			return;
		}

		$(this).addClass('loaded');

		$.ajax({
			url: '/notifications/',
			success: function(data) {
				$('#notifications-content').html(data);
				bindRemove();
			}
		});
	});
});
global.$ = require('jquery');
global.jQuery = global.$;

require('./ajaxSetup.js');

require('./uikit/uikit.js');

require('./form.js');

require('./filter.js');

require('./loadmore.js');

require('./product.js');

require('./../mobile/offcanvas.js');

require('./../mobile/filter.js');

require('./comments.js');

require('./../mobile/search.js');

require('./../plugin/sly/sly.min.js');

require('./uikit/components/upload.js');

require('./user.js');

require('./favorite.js');

require('./notifications.js');

$(function() {
    if($(".slider").length) {
        $(".slider").owlCarousel({
            loop:true,
            nav:true,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:1
                },
                1000:{
                    items:1
                }
            }
        });
    }

    if($(".news-slider").length) {
        $(".news-slider").owlCarousel({
            loop: true,
            nav: true,
            margin: 10,
            responsive:{
                0:{
                    items: 2
                },
                600:{
                    items: 3
                },
                1000:{
                    items: 4
                }
            }
        });
    }

    if($(".products-slider").length) {
        $(".products-slider").owlCarousel({
            loop: true,
            nav: true,
            margin: 10,
            responsive:{
                0:{
                    items: 2
                },
                600:{
                    items: 3
                },
                1000:{
                    items: 4
                }
            }
        });
    }

    $('.accordeon .toggle').click(function() {
    	var $parent = $(this).closest('.item');

    	if($(this).hasClass('active')) {
    		$(this).removeClass('active');
    		$parent.find('.content').stop().slideUp();
    	} else {
    		$(this).addClass('active');
    		$parent.find('.content').stop().slideDown();
    	}
    });

    if($("#category-reviews").length > 0) {
        $('#category-reviews').sly({
            horizontal: 4,
            itemNav: 'basic',
            smart: 1,
            mouseDragging: 1,
            touchDragging: 1,
            releaseSwing: 1,
            startAt: 0,
            scrollBar: $(this).next(),
            scrollBy: 1,
            speed: 300,
            elasticBounds: 1,
            dragHandle: 1,
            dynamicHandle: 1
        });
    }

    if($("#user-nav").length > 0) {
        $('.uk-container', "#user-nav").sly({
            horizontal: 1,
            itemNav: 'basic',
            smart: 1,
            mouseDragging: 1,
            touchDragging: 1,
            releaseSwing: 1,
            startAt: 0,
            speed: 300,
            elasticBounds: 1,
            dragHandle: 1,
            dynamicHandle: 1
        });
    }

    var showCategories = function() {
        if(!$('#all-categories').length) {
            return;
        }

        var ww = parseInt($(window).width());

        var total = $('.category-children .item').length;

        $('#all-categories').hide();

        if((ww < 470 && total > 4) || (ww < 620 && total > 6)) {
            $('#all-categories').show();
        }        
    }

    showCategories();

    $(window).resize(function() {
        showCategories();
    });

    $('#all-categories').click(function() {
        $('.category-children .item').removeClass('after-4 after-6');

        $(this).remove();
    });

    $('body').on('click', 'a.video', function(e) {
        e.preventDefault();

        var href = $(this).attr('href');
        var html = '<a class="uk-modal-close uk-close"></a>'+
            '<div id="dialog-content">'+
                '<iframe width="560" height="315" src="'+href+'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'+
            '</div>';

        var modal = UIkit.modal.dialog($(html), {bgclose: true, modal: false});

        modal.on('show.uk.modal', function() {
            $('.uk-modal').addClass('video-modal');
        });

        modal.show();
    });

    if($(".category-children-inner").length > 0) {
        $('.category-children-inner').sly({
            horizontal: 4,
            itemNav: 'basic',
            smart: 1,
            mouseDragging: 1,
            touchDragging: 1,
            releaseSwing: 1,
            startAt: 0,
            scrollBy: 1,
            speed: 300,
            elasticBounds: 1,
            dragHandle: 1,
            dynamicHandle: 1
        });
    }
});
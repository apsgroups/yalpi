ymaps.ready(init);

var myMap,
myPlacemark;

function init(){
    myMap = new ymaps.Map("map", {
        center: [37.556160594996605, 55.77713522579245],
        zoom: 10
    });

    var balloonContent = '<div style="width: 260px;"><b>Магазин, офис и склад в Кунцево</b><div>ул. Молодогвардейская, д. 54, стр. 4.<br />Тел: <span class="ya-phone call_phone_2">+7 (495) 984-16-99</span></div></div>';

    myPlacemark = new ymaps.Placemark([37.397053944106226, 55.73559128281092], {
        hintContent: '«Склад Дверей 169»',
        balloonContent: balloonContent
    }, {
        iconLayout: 'default#image',
        iconImageHref: '/uploads/media/contacts/map-icon-1.png',
        iconImageSize: [46, 54],
        iconImageOffset: [-18, -52]
    });

    myMap.geoObjects.add(myPlacemark);

    balloonContent = '<div style="width: 260px;"><b>Фирменный магазин дверей и мебели в Медведково</b><div>ул. Осташковская, д. 22.<br />Тел: <span class="ya-phone call_phone_2">+7 (495) 984-16-99</span></div></div>';

    myPlacemark = new ymaps.Placemark([37.67600675210854, 55.886390238713226], {
        hintContent: 'Фирменный салон',
        balloonContent: balloonContent
    }, {
        iconLayout: 'default#image',
        iconImageHref: '/uploads/media/contacts/map-icon-2.png',
        iconImageSize: [46, 54],
        iconImageOffset: [-18, -52]
    });

    myMap.geoObjects.add(myPlacemark);

    myMap.controls.add(new ymaps.control.ZoomControl());
}
global.jQuery = require('jquery');

var $ = window.jQuery;
var updateProducts = require('./update-products.js');

$(function() {
    var excludeParams = function($form) {
        var exclude = [];

        var $disabled = $form.find('input:disabled');

        $disabled.prop('disabled', false);

        var data = $form.find('input').not(exclude.join(', ')).serialize();

        $disabled.prop('disabled', true);

        return data;
    };

    var applyFilter = function() {
        var $form = $('#filter-form');


        if($('#filter-box').hasClass('loader')) {
            return;
        }

        var data = excludeParams($form);

        var beforeLoad = function() {
            $('#filter-box').addClass('loader');
        };

        var afterLoad = function(data) {
            $('#filter-box').removeClass('loader');

            if(typeof data.filterbox !== 'undefined') {
                $('#filter-box').remove();
                $(data.filterbox).appendTo($form);
            }

            $('.total-filtered').empty();

            if(data.total_filtered && $('input:checked', '#filter-form').length) {
                $('.total-filtered').html(data.total_filtered);;
            }

            filterOptions();
        };

        updateProducts($form.attr('action'), data, beforeLoad, afterLoad);
    };

    var syncCheckboxes = function(name, property) {
        var $box = $('#'+property.position+'-property');
        
        var $field = $box.find('#filter-field-'+name);

        if(!property.total) {
            $field.remove();
            return;
        }

        if(!$field.length) {
            $field = $('<div />').addClass('filter-field').attr('id', 'filter-field-'+name).appendTo($box);
            $('<div class="uk-text-bold title">'+property.title+'</div>').appendTo($field);
            $('<div class="list" />').appendTo($field);
        }

        $field.find('.checkbox-row').addClass('dirty');

        $.each(property.options, function(k, opt) {
            if(!opt.active) {
                return;
            }

            var uid = 'checkbox-row-'+name+'-'+opt.id;

            var checkId = 'input-'+name+'-'+opt.id;

            if($('#'+uid).length) {
                $('#'+uid).remove();
            }

            var labelText = opt.title;

            if(name === 'rate') {
                if(opt.id > 0) {
                    var img = '<img src="/images/layout/star.svg" alt="'+opt.title+'" />';
                    labelText = img.repeat(opt.id);
                } else {
                    labelText = 'Еще нет отзывов';
                }
            }

            var html = '<div id="'+uid+'" class="checkbox-row">'+
                            '<input type="checkbox" name="filter['+name+'][]" value="'+opt.id+'" id="'+checkId+'" class="checkbox" />'+
                            '<label for="'+checkId+'">'+labelText+'</label>'
                        '</div>';

            $(html).appendTo($field.find('.list'));

            $('#'+checkId).prop('checked', opt.selected);
            // $('#'+checkId).prop('disabled', !opt.active);
        });

        $field.find('.dirty').remove();

        $field.removeClass('dirty');
    }

    var syncSelect = function(name, property) {
        if(!property.total) {
            return;
        }

        var $box = $('#'+property.position+'-property');
        
        var $field = $box.find('#property-'+name);

        var $list;

        if(!$field.length) {
            $field = $('<div />').addClass('filter-field').attr('id', 'property-'+name).appendTo($box);

            var html = '<div class="uk-text-bold title">'+property.title+'</div>'+
                        '<div class="uk-button-dropdown" data-uk-dropdown="{mode:\'click\'}">'+
                            '<button class="uk-button">Ничего не выбрано</button>'+
                            '<div class="uk-dropdown">'+
                                '<ul class="uk-nav uk-nav-dropdown"></ul>'+
                            '</div>'+
                        '</div>';

            $(html).appendTo($field);
        }

        $list = $field.find('ul');

        $list.find('li').addClass('dirty');

        $.each(property.options, function(k, opt) {
            if(!opt.active) {
                return;
            }
            
            var uid = 'option-'+name+'-'+opt.id;

            var checkId = 'input-'+name+'-'+opt.id;

            if($('#'+uid).length) {
                $('#'+uid).remove();
            }

            var html = '<li id="'+uid+'" class="checkbox-row">'+
                            '<input type="checkbox" name="filter['+name+'][]" value="'+opt.id+'" id="'+checkId+'" class="checkbox" />'+
                            '<label for="'+checkId+'">'+opt.title+'</label>'+
                        '</li>';

            $(html).appendTo($list);

            $('#'+checkId).prop('checked', opt.selected);

            $('#'+checkId).prop('disabled', !opt.active);
        });

        $list.find('.dirty').remove();

        $field.removeClass('dirty');
    }

    var filterOptions = function(pageLoaded) {
        var $form = $('#filter-form');

        if(!$form.length || $form.hasClass('no-options')) {
            return;
        }

        var types = ['primary', 'secondary', 'teaching', 'rate']

        var q = pageLoaded ? location.search.replace('?', '') + '&category_id='+$('#filter-category-id').val() : $form.serialize();

        $.ajax({
            url: '/filter/options/',
            dataType: 'json',
            data: q,
            success: function(data) {
                $('.filter-field', '#filter-form').addClass('dirty');

                $.each(data.options, function(name, item) {
                    if(item.tag === 'checkbox') {
                        syncCheckboxes(name, item);
                        return;
                    }

                    syncSelect(name, item);
                });

                $('.filter-field.dirty', '#filter-form').remove();

                $('.filter-toggle', '#filter-box').each(function() {
                    var id = $(this).attr('data-id');

                    if($('#'+id+'-filter').find('.filter-field').length) {
                        $(this).removeClass('uk-hidden');
                    } else {
                        $(this).addClass('uk-hidden');
                    }
                });
            }
        });
    };

    $('#filter-form').submit(function(e) {
        e.preventDefault();

        var $search = $('[name=term]', '#filter-form');

        if($search.length > 0) {
    		updateSearchText($search.val());
        }

        applyFilter();
    });

    $('body').on('change', '.filter input[type=checkbox]', function(e) {
        applyFilter();

        var $dropdown = $(this).closest('.uk-button-dropdown');

        if($dropdown.length) {
            var checkedTotal = $dropdown.find('input:checked').length;

            var text = checkedTotal > 0 ? 'Выбрано значений: '+checkedTotal : 'Ничего не выбрано';

            $dropdown.find('button').empty().text(text);
        }

        if($("input[name='"+$(this).attr('name')+"']:checked").length) {
            $(this).closest('.checkbox-row').addClass('active');
        } else {
            $(this).closest('.checkbox-row').removeClass('active');
        }
    });

    $('body').on('change', '.filter input[type=text]', function(e) {
        if($(this).attr('name') === 'term') {
            $('input[type=checkbox]', '#filter-box').prop('checked', false);
            updateSearchText($(this).val());
        }

        applyFilter();
    });

    $('body').on('click', '.limitbox a, .view-toggle a, .sorting a', function(e) {
        updateProducts($(this).attr('href'), '');

        e.preventDefault();
    });

    $('body').on('click', '.filter-toggle', function(e) {
        var id = $(this).attr('data-id');

        var active = $(this).hasClass('active');

        $('.filter-toggle').removeClass('active');
        $('.filter-box').addClass('uk-hidden');

        if(active) {
            $('#'+id+'-filter').addClass('uk-hidden');
        } else {
            $(this).addClass('active');
            $('#'+id+'-filter').removeClass('uk-hidden');
        }
    });

    filterOptions(true);
});

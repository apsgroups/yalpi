global.jQuery = require('jquery');

var $ = window.jQuery;

$(function() {
    $('#dropdown-menu > ul').menuAim({
        submenuDirection: "right",
        openClassName: "active",
        activate: activateSubmenu,
        activationDelay: 0
    });

    function activateSubmenu(row) {
        var $li = $(row);
        var $ul = $li.find('> ul');

        let $parent = $li.parent();

        $parent.find('> li.active').removeClass('active');
        $li.addClass('active');

        $parent.nextAll('ul').remove();

        let id = $li.attr('data-id');

        if(!$ul.length && id) {
            $.ajax({
                url: '/menu/children/'+id+'/',
                beforeSend: function() {

                },
                success: function(data) {
                    $(data).appendTo($li);

                    if($li.hasClass('active')) {
                        $ul = $li.find('ul');
                        appendChildrenMenu($ul, $parent);
                    }
                } 
            });
        } else if($ul.length) {
            appendChildrenMenu($ul, $parent);
        }
    }

    function deactivateSubmenu(row) {
        var $li = $(row);
        var $ul = $row.find('> ul');
        
        let $parent = $li.parent();

        $parent.find('> li.active').removeClass('active');

        $parent.nextAll('ul').remove();
    }

    $('.active', '#main-menu').mouseleave(function() {
        $(this).removeClass('locked');
    });

    $('header > div:not(#main-menu), #content-container, footer').hover(function() {
        $('.active', '#main-menu').removeClass('locked');
    });

    let hideDropdown = function() {
        $('#dropdown-menu > ul:not(".index")').remove();
        $('#dropdown-menu > ul > li.active').removeClass('active');
        $('#dropdown-menu > ul').trigger('aim-active-row-reset');
        $('.main-menu-inner').removeClass('active');
        $('#menu-overlay, #header-overlay').remove();
        $('header').removeClass('front');
    }

    let dropTimerId;

    $('#main-menu-btn').hover(function() {
        dropTimerId = setTimeout(function() {
            $('.main-menu-inner').addClass('active');
            $("#autocomplete-results").removeClass("uk-display-block");
            
            $('header').addClass('front');

            $('#search-overlay, #menu-overlay, #header-overlay').remove();
            
            $('<div />').attr('id', 'menu-overlay').appendTo('body');
            
            $('<div />').attr('id', 'header-overlay').appendTo('header');

            $('#menu-overlay, #header-overlay').mouseenter(function() {
                hideDropdown();
            });
        }, 200);
    }, function() {
        clearTimeout(dropTimerId);
    });

    let appendChildrenMenu = function($ul, $parent) {
        let $list = $ul.clone().insertAfter($parent);

        $list.addClass('level-'+$parent.parent().find('> ul').length);

        $list.menuAim({
            submenuDirection: "right",
            openClassName: "active",
            activate: activateSubmenu,
            activationDelay: 0
        });
    }

    $('.sidebar-menu .toggle').click(function(e) {
        e.preventDefault();

        var $parent = $(this).closest('li');

        if($parent.hasClass('active')) {
            $parent.find('> ul').hide();
            
            $parent.removeClass('active opened');

            return;
        }

        $parent.find('> ul').show();
        
        $parent.addClass('active');

        if(!$parent.find('.selected').length) {
            $parent.addClass('opened');
        }
    });

    $('.sidebar-menu .selected').each(function() {
        $(this).parent().closest('li').addClass('active');
    });
});
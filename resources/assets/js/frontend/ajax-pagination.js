global.jQuery = require('jquery');

var $ = window.jQuery;

module.exports = function($pagination, onClick) {
    var ajaxPagination = function($pagination, onClick) {
        $('a', $pagination).each(function() {
            var href = $(this).attr('href');
            $(this).attr('href', '#');
            $(this).attr('data-href', href);

            $(this).click(function(e) {
                e.preventDefault();

                onClick(this);
            });
        });
    };

    return ajaxPagination($pagination, onClick);
};
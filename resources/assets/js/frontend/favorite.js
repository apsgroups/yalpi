global.jQuery = require('jquery');

var $ = window.jQuery;

var updateProducts = require('./update-products.js');

$(function() {
    $('body').on('click', '.fav-btn', function(e) {
        e.preventDefault();

        var $btn = $(this);

        var id = $btn.attr('data-id');

        var action = ($btn.hasClass('active')) ? 'remove' : 'add';

        $.ajax({
            url: '/favorite/'+action+'/'+id+'/',
            type: 'get',
            dataType: 'json',
            statusCode: {
                401: function(response) {
                    $.get('/login/').done(function(response) {
                        var modal = UIkit.modal.dialog($('<a class="uk-modal-close uk-close"></a>'+response), {bgclose: true, modal: false});

                        modal.show();
                    });
                }
            },
            success: function(data) {
                var $favTotal = $('#favorites-total');
                $favTotal.empty().text(data.total);

                if(data.total > 0) {
                    $('#favorites').addClass('active');
                } else {
                    $('#favorites').removeClass('active');
                }

                if($btn.hasClass('update')) {
                    updateProducts('/favorite/', []);
                    return;
                }

                if(data.active) {
                    $('.fav-btn[data-id='+id+']').addClass('active').empty().text('В избранном');
                } else {
                    $('.fav-btn[data-id='+id+']').removeClass('active').empty().text('Добавить в избранное');
                }
            }
        });
    });
});
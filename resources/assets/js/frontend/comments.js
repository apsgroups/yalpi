global.jQuery = require('jquery');

var $ = window.jQuery;

$(function() {
    var itemId = $('#comments').attr('data-id');
    var itemType = $('#comments').attr('data-scope');

    var like = function(id, action) {
        $.ajax({
            url: '/comments/'+id+'/like/',
            type: 'post',
            dataType: 'json',
            data: {action: action},
            success: function(response) {
                var $comment = $('.comment-actions[data-id='+id+']');

                $('.status .active', $comment).removeClass('active');

                $('.status .'+response.action+'s', $comment).addClass('active');

                $('.likes .value', $comment).empty().text(response.likes);
                $('.dislikes .value', $comment).empty().text(response.dislikes);
            }
        });
    };

    var likes = function() {
        $('.likes, .dislikes', '#comments').click(function() {
            var id = $(this).closest('[data-id]').attr('data-id');
            like(id, $(this).hasClass('likes'));
        });
    };

    $('#comments').on('click', '.likes, .dislikes', function() {
        var id = $(this).closest('[data-id]').attr('data-id');
        like(id, $(this).hasClass('likes'));
    });

    $('.filter-body a', '#comments').click(function(e) {
        e.preventDefault();

        $('.filter-body .active').removeClass('active');

        $(this).addClass('active');

        let url = $(this).attr('data-href');

        $.ajax({
            url: url,
            dataType: 'html',
            success: function(content) {
                $('#comment-items').empty().html(content);
            }
        })
    });

    $('#comments').on('click', '#comments-pagination a', function(e) {
        e.preventDefault();

        let url = $(this).attr('data-href');

        $.ajax({
            url: url,
            dataType: 'html',
            success: function(content) {
                $('#comments-pagination').remove();
                $(content).appendTo('#comment-items');
            }
        })
    });
    
    $('.star', '#comment-rating').hover(function() {
        $(this).prevAll().addClass('hover');
        $(this).addClass('hover');
        $(this).nextAll().removeClass('hover');
    }, function() {
        $('.star', '#comment-rating').removeClass('hover');
    });

    $('.star', '#comment-rating').click(function() {
        $('.star', '#comment-rating').removeClass('hover');
        $(this).prevAll().addClass('active');
        $(this).addClass('active');
        $(this).nextAll().removeClass('active');
        $('#comment-rate').val($(this).index()+1).prop('checked', true);
    });
});
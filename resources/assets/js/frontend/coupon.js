global.jQuery = require('jquery');

var $ = window.jQuery;

require('jquery-validate');

$(function() {
    var copyToClipboard = function(text) {
        if (window.clipboardData && window.clipboardData.setData) {
            // IE specific code path to prevent textarea being shown while dialog is visible.
            return clipboardData.setData("Text", text);

        } else if (document.queryCommandSupported && document.queryCommandSupported("copy")) {
            var textarea = document.createElement("textarea");
            textarea.textContent = text;
            textarea.style.position = "fixed";  // Prevent scrolling to bottom of page in MS Edge.
            document.body.appendChild(textarea);
            textarea.select();
            try {
                return document.execCommand("copy");  // Security exception may be thrown by some browsers.
            } catch (ex) {
                console.warn("Copy to clipboard failed.", ex);
                return false;
            } finally {
                document.body.removeChild(textarea);
            }
        }
    };

    $('#subscribe-coupon').validate({
        errorClass: "uk-form-danger",
        errorPlacement: function(error, element) {},
        submitHandler: function(form) {
            $.ajax({
                url: $(form).attr('action'),
                type: 'post',
                dataType: 'json',
                data: $(form).serialize(),
                beforeSend: function() {
                    $('input[type=text]', $(form)).prop('disabled', true);
                    $('#discount_send').empty().prop('disabled', true).html('<i class="uk-icon-spinner uk-icon-spin"></i>');
                },
                error: function() {

                },
                success: function(response) {
                    $('input[type=text]', $(form)).prop('disabled', false);
                    $('input[type=submit]', $(form)).prop('disabled', false);

                    $(form).find('.discount_img').addClass('uk-hidden')
                    $(form).find('#subscribe-result').removeClass('uk-hidden');

                    $(form).addClass('arrow');

                    $('#discount_send').addClass('uk-hidden');

                    if(typeof response.errors !== 'undefined') {
                        $(form).find('#coupon-area').addClass('uk-hidden');

                        $(form).addClass('error');

                        for(var error in response.errors) {
                            $('<div class="error-text">'+response.errors[error]+'</div>').appendTo('#subscribe-result');
                        }

                        return false;
                    }

                    $(form).find('#coupon-area').removeClass('uk-hidden');

                    $('#coupon-code').show().empty().text(response.coupon);

                    $('#copy-coupon').click(function(e) {
                        e.preventDefault();
                        copyToClipboard(response.coupon);
                    })

                }
            });

            return false;
        }
    });

    $('#subscribe-coupon [name=email]').focus(function() {
        if($('#subscribe-coupon').hasClass('error')) {
            $('#subscribe-coupon').find('.discount_img').removeClass('uk-hidden');
            $('#subscribe-coupon').removeClass('arrow error');
            $('#discount_send').removeClass('uk-hidden')
            $('.error-text', '#subscribe-coupon').remove();
            $('#subscribe-result').addClass('uk-hidden');
            $('#discount_send').empty().text('Получить скидку').prop('disabled', false);
        }                
    });

    $('#submit-coupon').click(function() {
        if($.trim($('#coupon-input').val()) === '') {
            $('#coupon-input').addClass('uk-form-danger');
            return;
        }
        
        $('#coupon-input').removeClass('uk-form-danger');

        var $btn = $(this);

        $btn.empty().prop('disabled', true).html('<i class="uk-icon-spinner uk-icon-spin"></i>');

        $.ajax({
            url: '/cart/coupon/',
            data: {coupon: $('#coupon-input').val()},
            dataType: 'json',
            type: 'post',
            success: function(response) {
                $btn.empty().prop('disabled', false).html('Ввести');

                if(typeof response.cost !== 'undefined') {                        
                    $('.cart-cost').empty().text(response.cost);

                    $('<div class="coupon-code-val">Скидка:</div>').appendTo('.total');
                    $('<div class="coupon-code-val">'+response.discount+' <i class="uk-icon-rub"></i></div>').appendTo('#cart-total-sum');

                    return;
                }

                $('#coupon-input').addClass('uk-form-danger');
            }
        });
    });
});
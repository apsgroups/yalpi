global.jQuery = require('jquery');

var $ = window.jQuery;

$(function() {
    var productId = $('#product-reviews').attr('data-id');

    var like = function(id, action) {
        $.ajax({
            url: '/product/reviews/'+id+'/like/',
            type: 'post',
            dataType: 'json',
            data: {action: action},
            success: function(response) {
                var $review = $('.review[data-id='+id+']');

                $('.status .active', $review).removeClass('active');

                $('.status .'+response.action+'s', $review).addClass('active');

                $('.likes .value', $review).empty().text(response.likes);
                $('.dislikes .value', $review).empty().text(response.dislikes);
            }
        });
    };

    var likes = function() {
        $('.likes, .dislikes', '#product-reviews').click(function() {
            var id = $(this).closest('[data-id]').attr('data-id');
            like(id, $(this).hasClass('likes'));
        });
    };

    $('#product-reviews').on('click', '.likes, .dislikes', function() {
        var id = $(this).closest('[data-id]').attr('data-id');
        like(id, $(this).hasClass('likes'));
    });

    $('.filter-body a', '#product-reviews').click(function(e) {
        e.preventDefault();

        $('.filter-body .active').removeClass('active');

        $(this).addClass('active');

        let url = $(this).attr('data-href');

        $.ajax({
            url: url,
            dataType: 'html',
            success: function(content) {
                $('#product-reviews-items').empty().html(content);
            }
        })
    });

    $('#product-reviews').on('click', '#reviews-pagination a', function(e) {
        e.preventDefault();

        let url = $(this).attr('data-href');

        $.ajax({
            url: url,
            dataType: 'html',
            success: function(content) {
                $('#reviews-pagination').remove();
                $(content).appendTo('#product-reviews-items');
            }
        })
    });
    
    $('.star', '#review-rating').hover(function() {
        $(this).prevAll().addClass('hover');
        $(this).addClass('hover');
        $(this).nextAll().removeClass('hover');
    }, function() {
        $('.star', '#review-rating').removeClass('hover');
    });

    $('.star', '#review-rating').click(function() {
        $('.star', '#review-rating').removeClass('hover');
        $(this).prevAll().addClass('active');
        $(this).addClass('active');
        $(this).nextAll().removeClass('active');
        $('#review-rate').val($(this).index()+1).prop('checked', true);
    });
});
global.$ = require('jquery');
global.jQuery = global.$;

require('./ajaxSetup.js');

require('./uikit/uikit.js');

require('../plugin/menu-aim/jquery.menu-aim.js');

require('./menu.js');

require('./cart.js');

require('./favorite.js');

require('./form.js');

require('./filter.js');

//require('./pagination.js');

require('./general.js');

require('./product.js');

require('./loadmore.js');

require('./comments.js');

require('./collapse.js');

require('./contacts.js');

require('./coupon.js');

require('./search.js');

require('./uikit/components/upload.js');

require('./user.js');

require('./notifications.js');
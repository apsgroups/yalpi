var $ = window.jQuery;

$(document).ready(function() {
    var activateTab = function($btn) {
        var ind = $btn.index();

        var $block = $btn.closest('.tab-carousel');

        $block.find('.tab-item.active').removeClass('active');
        $block.find('.switcher .active').removeClass('active');

        $block.find('.tab-item:eq('+ind+')').addClass('active');

        $btn.addClass('active');
    };

    activateTab($('.tab-carousel .switcher li').first());

    $('.tab-carousel .switcher li').click(function() {
        activateTab($(this))
    });

    $(".tab-carousel .owl-carousel").owlCarousel({
        loop:true,
        margin:30,
        nav:true,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:3
            },
            1000:{
                items:4
            }
        }
    });
});
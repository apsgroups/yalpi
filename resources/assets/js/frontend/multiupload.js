global.$.fineUploader = require('./../plugin/fine-uploader/jquery.fine-uploader.min.js');

(function($) {
    var uploadToken = $('#upload_token').val();

    $('#file-uploader').fineUploader({
        template: 'qq-template-gallery',
        request: {
            params: {
                'upload_token': uploadToken,
                '_token': $('meta[name="_token"]').attr('content')
            },
            endpoint: '/garantiya/upload/'
        },
        thumbnails: {
            placeholders: {
                waitingPath: '/js/plugin/fine-uploader/placeholders/waiting-generic.png',
                notAvailablePath: '/js/plugin/fine-uploader/placeholders/not_available-generic.png'
            }
        },
        validation: {
            allowedExtensions: ['jpeg', 'jpg', 'gif', 'png'],
            acceptFiles: "image/*;capture=camera"
        },
        onComplete: function(id, fileName, responseJSON){
            console.log(id);
            console.log(fileName);
            console.log(responseJSON);
        }
    });
})(jQuery);
global.jQuery = require('jquery');

var $ = window.jQuery;

module.exports = function() {
    var alignShortTags = function() { 
        $('.products-grid:not(.short-tag-fixed)').each(function(i) {
            $(this).addClass('short-tag-fixed');

            if(!$('.short-tag', this).length) {
                return;
            }
     
            $('.product-desc', this).each(function(ind) {
                if($('.short-tag', this).length > 0) {
                    return;
                }

                $('<div />').addClass('short-tag').html('&nbsp;').prependTo(this);
            });
        });
    };

    return alignShortTags();
};
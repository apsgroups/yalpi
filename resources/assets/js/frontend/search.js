global.jQuery = require('jquery');

var $ = window.jQuery;

$(function() {
    var delay = null;
    var menu = null;

    $('#search-box').on('focus', function() {
        var $input = $(this);

        if($("#autocomplete-results").length) {
            $("#search-holder").addClass("focused");
            $("#autocomplete-results").addClass("uk-display-block");

            addOverlay();
        } else {
            loadMenu();
        }
    });

    var loadMenu = function() {
        if(menu !== null) {
            $(menu).appendTo('#search-holder').addClass('uk-display-block');
            return;
        }

        $.ajax({
            url: '/search/menu/',
            success: function(data) {
                addOverlay();
                $(data).appendTo('#search-holder').addClass('uk-display-block');
                menu = data;
            }
        });
    }

    $('#search-box').on('input', function() {
        var $input = $(this);

        clearTimeout(delay),

        delay = setTimeout(function() {
            if(!$input.val().length) {
                hideAutomplete();
                return;
            }
                        
            if($input.val().length <= 2) {
                hideAutomplete();
                return;
            }

            $.ajax({
                url: '/search/autocomplete/',
                data: {term: $input.val()},
                success: function(data) {
                    $('#autocomplete-results').remove();

                    if(data) {
                        addOverlay();
                        $(data).appendTo('#search-holder').addClass('uk-display-block');
                        return;
                    }

                    hideOverlay();
                }
            });
        }, 300);
    });

    var hideOverlay = function() {
        $('#search-overlay').remove();
    }

    var addOverlay = function() {
        $('#menu-overlay').remove();
        $('.main-menu-inner').removeClass('active');
        $('#dropdown-menu > ul:not(.index)').remove();
        $('#dropdown-menu > ul > li.active').removeClass('active');
        $('header').addClass('front');

        if($('#search-overlay').length) {
            return;
        }

        var header = parseInt($('header').height());
        var height = parseInt($('body').height()) - header;
        $('<div />').attr('id', 'search-overlay').appendTo('body');
    }

    $(document).on("click", function(e) {
        if($("#search-box").length && !$(e.target).closest("#search-holder").length) {
            hideAutomplete();
        }
    });

    var hideAutomplete = function() {
        $("#search-holder").removeClass("focused");
        $("#autocomplete-results").removeClass("uk-display-block");
        hideOverlay();
    }

    $('#search-box').on('focus', function() {
        var text = $(this).attr('placeholder');
        $(this).data('placeholder', text);
        $(this).attr('placeholder', '');
    });

    $('#search-box').on('blur', function() {
        var text = $(this).data('placeholder');
        $(this).attr('placeholder', text);
    });
});
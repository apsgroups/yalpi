global.jQuery = require('jquery');

var $ = window.jQuery;

var ajaxPagination = require('./ajax-pagination.js');

module.exports = function(url, data, beforeLoad, afterLoad) {
    var removeLoader = function() {
        $('#filter-loader').remove();
    };

    var addLoader = function() {
        removeLoader();

        $('<div class="loader" id="filter-loader" />').appendTo('#filter-results');
    };

    var updateProducts = function(url, data, beforeLoad, afterLoad) {
        $.ajax({
            url: url,
            data: data,
            dataType: 'json',
            cache: false,
            beforeSend: function() {
                if(typeof beforeLoad == "function") {
                    beforeLoad();
                }

                addLoader();
            },
            success: function(data) {
                if(typeof data.url !== 'undefined') {
                    history.replaceState(null, null, data.url);
                }

                if(typeof data.filterUrl !== 'undefined') {
                    $('#filter-form').attr('action', data.filterUrl);
                }

                $('#filter-results').empty().html(data.products);

                $('html').trigger('changed.uk.dom');

                if(typeof data.title !== 'undefined') {
                    $('title').empty().html(data.title);
                }

                if(typeof data.heading !== 'undefined') {
                    $('.page-heading').empty().html(data.heading);
                }

                if(typeof data.ajax_pagination !== 'undefined') {
                    ajaxPagination($('#filter-results').find('.uk-pagination'), function(btn) {
                        updateProducts($(btn).attr('data-href'));
                    });
                }

                removeLoader();

                if(typeof afterLoad == "function") {
                    afterLoad(data);
                }
            }
        });
    };

    return updateProducts(url, data, beforeLoad, afterLoad);
};
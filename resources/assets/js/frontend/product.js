global.jQuery = require('jquery');

var $ = window.jQuery;

$(function() { 
    $('.block-cources .toggle').click(function(e) {
        e.preventDefault();

        var $li = $(this).closest('li');

        if($li.hasClass('active')) {
            $li.find('.toggle-body').stop().slideUp();
            $li.removeClass('active');
        } else {
            $li.find('.toggle-body').stop().slideDown();
            $li.addClass('active');
        }
    });

    $('a', '#promo-nav').click(function() {
        var categoryId = $(this).attr('data-category');
        var $content = $('#promo-'+categoryId);

        if($content.find('.uk-grid').length) {
            return;
        }

        $.ajax({
            url: '/products/promo/'+categoryId,
            dataType: 'html',
            beforeSend: function() {
                $content.empty().html('<i class="uk-icon-spinner uk-icon-spin"></i>');
            },
            success: function(data)  {
                $content.empty().html(data);
            }
        });
    });

    $(window).scroll(function () {
        if($('#product-container').length) {
            var scroll = $(this).scrollTop();
            var parent = $('#product-container').offset();

            var parentBottom = parent.top + $('#product-container').height();

            var cardBottom = parentBottom - $('#product-card').height();

            if(scroll >= parent.top) {
                var top = scroll - parent.top;

                if(scroll > cardBottom) {
                    top = $('#product-container').height() - $('#product-card').height();
                }

                $('#product-card').css({
                    'top': top
                });
            } else {
                $('#product-card').attr('style', '');
            }
        }
    });
});
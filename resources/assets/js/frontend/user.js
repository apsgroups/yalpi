global.jQuery = require('jquery');

var $ = window.jQuery;

$(function() {
	var settings    = {
		type: 'json',

		param: 'avatar',

        action: '/profile/avatar/',

        allow : '*.(jpg|jpeg|gif|png)',

        loadstart: function() {
            $("#upload-drop").addClass('loading');
        },

        progress: function(percent) {
        },

        allcomplete: function(response) {
            $("#upload-drop").removeClass('loading');

            $('img', '#upload-drop').attr('src', response.src);
        }
    };

    var select = UIkit.uploadSelect($("#upload-select"), settings);
});
global.jQuery = require('jquery');

var $ = window.jQuery;

$(function() {
    $('.per-page-box', document).bind('change', function() {
        $(this).parent().submit();
    });
});
global.jQuery = require('jquery');

var $ = window.jQuery;

$(function() { 
    $('body').on('click', '.loadmore', function(e) {
        var $btn = $(this);

        $.ajax({
            url: $btn.attr('data-url'),
            dataType: 'json',
            data: {loadmore: true},
            beforeSend: function() {
                $('.loadmore').empty().html('<i class="uk-icon-spinner uk-icon-spin"></i>');
            },
            success: function(data)  {
                if(typeof data.history === 'undefined' || data.history === 1) {
                    history.pushState(null, null, $btn.attr('data-url'));
                }

                $('.loadmore-hide').hide();

                $(data.products).appendTo('.products-grid-block', '#filter-results');

                $('html').trigger('changed.uk.dom');

                $('.loadmore-block').empty().html(data.loadmore);

                $('.pagination-box').empty().html(data.pagination);

                if(typeof data.title !== 'undefined') {
                    $('title').empty().html(data.title);
                }

                if(typeof data.ajax_pagination !== 'undefined') {
                    ajaxPagination($('#filter-results').find('.uk-pagination'), function(btn) {
                        updateProducts($(btn).attr('data-href'));
                    });
                }
            }
        });
    });
});
global.jQuery = require('jquery');

var $ = window.jQuery;

$(function() {
    // to top
    if ($('#to-top').length > 0) {
        $('#to-top').click(function() {
            $('html, body').stop().animate({scrollTop:0}, 200, 'linear');
        });

        $(window).scroll(function() {
            if ($(window).scrollTop() > 0 && $(window).scrollTop() > 200) {
                $('#to-top').show().css({'display': 'block', 'position': 'fixed'});
            } else {
                $('#to-top').hide();
            }
        });
    }
});
global.jQuery = require('jquery');

var $ = window.jQuery;

$(function() {
    $('.discussion-reply-btn', '#product-discussion').click(function() {
        var id = $(this).attr('data-id');

        var $form = $('#discussion-reply-form');

        $form.appendTo('#discussion-'+id);

        $form.removeClass('uk-hidden');

        $('#discussion-quest-form').addClass('uk-hidden');

        $form.find('input[name=parent_id]').val(id);
    });

    $('.discussion-quest-btn', '#product-discussion').click(function() {
        var id = $(this).attr('data-id');

        var $form = $('#discussion-quest-form');

        $form.appendTo('#discussion-'+id);

        $('#discussion-reply-form').addClass('uk-hidden');

        $form.removeClass('uk-hidden');

        $form.find('input[name=parent_id]').val('');
    });
});
global.jQuery = require('jquery');

var $ = window.jQuery;

$(function() {
    var shippingMap = function() {
        var parentHeight = parseInt($('.shipping-map').parent().height()) - 29;
        var titleHeight = parseInt($('.map-title').height());

        $('.shipping-map').height((parentHeight - titleHeight) + 'px');

        //window.ymaps.container.getElement().style.height = (parentHeight - titleHeight)+'px';
        //myMap.container.fitToViewport();

        window.myMap.container.fitToViewport();
        //window.ymaps.ready(window.init);
    };

    var action = function($btn) {  
        var $main = $btn.closest('.collapsed');

        var $content = $main.find('.collapsed-content');

        var textAttr = 'collapse';

        if($btn.hasClass('active')) {
            $main.addClass('active');
            $content.stop().slideDown(300, function() {
                shippingMap();
            }); 
        } else {
            $content.stop().slideUp(300, function() {
                $main.removeClass('active');
            }); 
            textAttr = 'show';
        }

        var text = $btn.attr('data-'+textAttr);
        $btn.empty().text(text);
    }; 

    $('.toggle-collapse').click(function() {
        $(this).toggleClass('active');

        action($(this));
    });

    $('[data-collapse-block]').click(function() {
        var block = $(this).attr('data-collapse-block');
        
        $('#'+block).toggleClass('active');

        action($('#'+block));
    });

    $('.toggle-collapse').each(function() {
        action($(this));
    });
});
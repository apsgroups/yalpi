/**
 * @license Copyright (c) 2003-2016, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
    config.width = 700;
	config.height = 300;

	config.protectedSource.push( /<i[\s\S]*?\>/g ); //allows beginning <i> tag
	config.protectedSource.push( /<\/i[\s\S]*?\>/g ); //allows ending </i> tag
	config.protectedSource.push( /<span[\s\S]*?\>/g ); //allows beginning <span> tag
	config.protectedSource.push( /<\/span[\s\S]*?\>/g ); //allows ending </span> tag

};

CKEDITOR.dtd.$removeEmpty.span = 0;
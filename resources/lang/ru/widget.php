<?php

return [
    'tab' => [
        'main' => 'Основное',
        'assign' => 'Привязка',
    ],
    'product' => [
        'slider' => [
            'badge' => [
                'novelty' => 'Новинки',
                'sale' => 'Специальные предложения',
            ]
        ],
    ],
];
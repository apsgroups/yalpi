<?php

return [
    'cart' => 'Корзина',
    'callback' => 'Заказ звонка',
    'consult' => 'Заказ звонка',
    'oneclick' => 'В 1 клик',
    'thank' => 'Благодарность',
    'contact' => 'Обратная связь',
    'question' => 'Обратная связь',
    'complain' => 'Жалоба',

    'prefixes' => [
        'city' => 'г. :city',
        'street' => 'ул. :street',
        'home' => 'дом :home',
        'apartment' => 'кв. :apartment'
    ]
];
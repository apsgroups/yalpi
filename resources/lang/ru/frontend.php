<?php

return [
    'sorting' => [
        'popular' => 'Популярное',
        'lower-price' => 'Дешевле',
        'higher-price' => 'Дороже',
    ],
    'novelty' => 'Новинка',
    'bestseller' => 'Лидер',
    'dummy' => [
    	'form' => [
    		'desc' => 'Будь с нами, пока мы стартуем и получи Vip-статус:',
    		'name' => 'Имя',
    		'phone' => 'Телефон',
    		'send' => 'Отправить'
    	],
    ],
    'logo' => [
    	'desc' => 'Мастер-классы со всего мира',
    ],
    'catalog-btn' => 'Он-лайн обучеие'
];
<?php

return [
    'tab' => [
        'main' => 'Главное',
        'seo' => 'SEO',
        'property' => 'Свойства',
        'products' => 'Товары',
        'contacts' => 'Контакты',
        'footer' => 'Футер',
        'feedback' => 'Формы обратной связи',
        'sitemap' => 'Карта сайта',
        'reviews' => 'Отзывы',
        'market' => 'Яндекс.Маркет',
        'subscribe' => 'Подписка',
        'virtual_discounts' => 'Виртуальные скидки',
        'crm' => 'CRM',
        'order_xml' => 'Выгрузка заказов',
        'info' => 'Инфо',
    ],
];
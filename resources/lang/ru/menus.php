<?php

return [
    'backend' => [
        'access' => [
            'title' => 'Пользователи',

            'permissions' => [
                'all' => 'Все права',
                'create' => 'Создать право',
                'edit' => 'Удалить право',
                'groups' => [
                    'all' => 'Все группы',
                    'create' => 'Создать группу',
                    'edit' => 'Изменить группу',
                    'main' => 'Группы',
                ],
                'main' => 'Права',
                'management' => 'Управление правами',
            ],

            'roles' => [
                'all' => 'Все роли',
                'create' => 'Создать роль',
                'edit' => 'Изменить роль',
                'management' => 'Управление ролями',
                'main' => 'Роли',
            ],

            'users' => [
                'all' => 'Все пользователи',
                'change-password' => 'Изменить пароль',
                'create' => 'Создать пароль',
                'deactivated' => 'Деактивировать ',
                'deleted' => 'Удалить пользователя',
                'edit' => 'Изменить пользователя',
                'main' => 'Пользователи',
            ],
        ],

        'log-viewer' => [
            'main' => 'Логи',
            'dashboard' => 'Основное',
            'logs' => 'Логи',
        ],

        'sidebar' => [
            'dashboard' => 'Админ панель',
            'general' => 'Общее',
            'categories' => 'Категории',
            'products' => 'Товары',
            'properties' => 'Свойства товаров',
        ],
    ],

    'language-picker' => [
        'language' => 'Language',
        /**
         * Add the new language to this array.
         * The key should have the same language code as the folder name.
         * The string should be: 'Language-name-in-your-own-language (Language-name-in-English)'.
         * Be sure to add the new language in alphabetical order.
         */
        'langs' => [
            'de' => 'German',
            'en' => 'English',
            'es' => 'Spanish',
            'fr' => 'French',
            'it' => 'Italian',
            'pt-BR' => 'Brazilian Portuguese',
            'sv' => 'Swedish',
        ],
    ],
];
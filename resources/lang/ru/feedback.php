<?php

return [
    'oneclick' => [
        'title' => 'Заказ в 1 клик',
    ],
    'callback' => [
        'title' => 'Заказ звонка',
    ],
    'consult' => [
        'title' => 'Получить консультацию',
    ],
    'complain' => [
        'title' => 'Жалоба'
    ],
    'thank' => [
        'title' => 'Благодарность'
    ],
    'contact' => [
        'title' => 'Обратная связь'
    ],
    'question' => [
        'title' => 'Задать вопрос'
    ],
    'director' => [
        'title' => 'Директору'
    ],
    'proposal' => [
        'title' => 'Что можно улучшить на сайте?'
    ]
];
<?php

return [
    'dummy' => [
    	'form' => [
    		'desc' => 'Join us while we start and get a VIP – status',
            'name' => 'Your name',
            'phone' => 'Your phone',
            'send' => 'Send!',
    	],
    ],
    'logo' => [
        'desc' => 'Master class from all around the world',
    ],
    'catalog-btn' => 'Online education'
];
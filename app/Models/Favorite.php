<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Favorite extends Model
{
    protected $fillable = [
        'product_id',
        'user_id',
    ];

    public function scopeOfProduct($query, $id)
    {
        return $query->where('product_id', $id);
    }

    public function scopeOfUser($query, $id)
    {
        return $query->where('user_id', $id);
    }

    public function user()
    {
        return $this->belongsTo('App\Models\Access\User\User');
    }

    public function product()
    {
        return $this->belongsTo('App\Models\Product');
    }
}

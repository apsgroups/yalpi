<?php

namespace App\Models;

use App\Models\Traits\TagTrait;
use Illuminate\Database\Eloquent\Model;

class PrettyUrl extends Model
{
    use TagTrait;

    protected $fillable = [
        'pretty_url',
        'description',
        'heading',
        'page_title',
        'metadesc',
        'metakeys',
        'category_id',
        'options',
        'options',
        'price_min',
        'price_max',
    ];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CommentLike extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'user_id',
        'comment_id',
        'like',
    ];

    public function comment()
    {
        return $this->belongsTo('App\Models\Comment');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\Access\User\User');
    }

    public function scopeOfUser($query, $id)
    {
        return $query->where('user_id', $id);
    }
}

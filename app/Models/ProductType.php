<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductType extends Model
{
    protected $fillable = [
        'title',
        'module_title',
        'sale_category_id',
        'display_name',
        'preview_dimensions',
        'search_priority',
        'search_keys',
        'search_url',
        'display_tag',
        'modules'
    ];

    public function products()
    {
        return $this->hasMany('App\Models\Product');
    }

    public function groups()
    {
        return $this->hasMany('App\Models\ProductTypeGroup');
    }

    public function saleCategory()
    {
        return $this->belongsTo('App\Models\Category', 'sale_category_id');
    }

    public function properties()
    {
        return $this->belongsToMany('App\Models\Property');
    }
}
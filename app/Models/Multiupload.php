<?php

namespace App\Models;

use App\Models\Traits\ImageTrait;
use Illuminate\Database\Eloquent\Model;

class Multiupload extends Model
{
    use ImageTrait;

    protected $fillable = [
        'upload_token',
        'title',
    ];

    public function getThumbnailPath()
    {
        return $this->id;
    }

    public function scopeToken($query, $token)
    {
        return $query->where('upload_token', $token);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductHit extends Model
{
    protected $fillable = [
        'product_id',
        'total',
    ];

    public function product()
    {
        return $this->belongsTo('App\Models\Product');
    }
}
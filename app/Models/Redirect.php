<?php

namespace App\Models;

use App\Models\Traits\RedirectTrait;
use Illuminate\Database\Eloquent\Model;

class Redirect extends Model
{
    use RedirectTrait;

    protected $fillable = [
        'from_url',
        'item_type',
        'item_id',
        'to_url',
    ];

    public function item()
    {
        return $this->morphTo('item');
    }

    public function link()
    {
        if($this->item && $this->item_type !== self::class) {
            return $this->item->link();
        }

        return $this->to_url;
    }
}

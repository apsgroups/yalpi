<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notify extends Model
{
    protected $fillable = [
        'user_id',
    ];

    public static function boot()
    {
        static::saved(function ($notify) {
        	$notify->user->notify_count = $notify->user->notifies()->where('active', 1)->count();
        	$notify->user->save();
        });

        parent::boot();
    }

    public function user()
    {
        return $this->belongsTo('App\Models\Access\User\User');
    }

    public function notifable()
    {
        return $this->morphTo();
    }
}

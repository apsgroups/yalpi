<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\ImageTrait;
use App\Models\Traits\SlugTrait;

class Author extends Model
{
    use SlugTrait,
        ImageTrait;

    protected $fillable = [
        'name',
        'slug',
        'about',
        'slogan',
        'facebook',
        'vk',
        'twitter',
        'instagram',
        'youtube',
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\Access\User');
    }

    public function getThumbnailPath()
    {
        return '/authors/'.$this->id;
    }

    protected function getTitleAttribute()
    {
        return $this->name;
    }

    public function products()
    {
        return $this->belongsToMany('App\Models\Product')->withPivot('ordering');
    }

    public function scopeOrdered($query) 
    {
        return $query->orderBy('author_product.ordering', 'asc');
    }

    public function getPageTitle()
    {
        return 'Уроки от '.$this->name;
    }

    public function getMetaDesc()
    {
        return '';
    }

    public function getMetaKeys()
    {

    }

    public function getHeading()
    {
        return 'Уроки от '.$this->name;
    }
}

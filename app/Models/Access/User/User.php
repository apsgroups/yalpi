<?php

namespace App\Models\Access\User;

use App\Models\Access\User\Traits\UserAccess;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\Access\User\Traits\Attribute\UserAttribute;
use App\Models\Access\User\Traits\Relationship\UserRelationship;
use App\Models\Traits\ImageTrait;

/**
 * Class User
 * @package App\Models\Access\User
 */
class User extends Authenticatable
{
    use SoftDeletes, UserAccess, UserAttribute, UserRelationship, ImageTrait;

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function categories()
    {
        return $this->hasMany('App\Models\Categories');
    }

    public function products()
    {
        return $this->hasMany('App\Models\Product');
    }

    public function notifies()
    {
        return $this->hasMany('App\Models\Notify');
    }

    public function favorites()
    {
        return $this->hasMany('App\Models\Favorite');
    }

    public function favoriteProducts()
    {
        return $this->belongsToMany('App\Models\Product', 'favorites', 'user_id', 'product_id')->withPivot('created_at');
    }

    public function comments()
    {
        return $this->hasMany('App\Models\Comment');
    }

    public function getThumbnailPath()
    {
        return $this->id;
    }

    public function getTitleAttribute()
    {
        return $this->name;
    }
}

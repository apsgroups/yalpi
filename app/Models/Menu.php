<?php

namespace App\Models;

use App\Models\Traits\PublishedTrait;
use App\Models\Traits\SlugTrait;
use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    use SlugTrait, PublishedTrait;

    protected $fillable = [
        'title',
        'slug',
        'published',
        'description',
        'user_id',
    ];

    public function scopeSlug($query, $slug)
    {
        return $query->where('slug', $slug);
    }

    public function items()
    {
        return $this->hasMany('App\Models\MenuItem');
    }

    public function widgets()
    {
        return $this->hasMany('App\Models\Widgets\MenuWidget');
    }
}
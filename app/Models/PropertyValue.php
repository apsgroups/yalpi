<?php

namespace App\Models;

use App\Models\Traits\ImageTrait;
use App\Models\Traits\SlugTrait;
use Illuminate\Database\Eloquent\Model;

class PropertyValue extends Model
{
    use SlugTrait, ImageTrait;

    public function getParentContextName()
    {
        return 'property_id';
    }

    protected $fillable = [
            'title',
            'slug',
            'plural',
            'ordering',
            'image_id',
            'property_id',
        ];

    public $timestamps = false;

    public function property()
    {
        return $this->belongsTo('App\Models\Property');
    }

    public function productValues()
    {
        return $this->hasMany('App\Models\ProductProperty');
    }

    public function getThumbnailPath()
    {
        return $this->property->slug.'/'.$this->slug;
    }

    public function getPluralAttribute($value)
    {
        if(empty($value)) {
            return $this->title;
        }

        return $value;
    }

    public function parents()
    {
        return $this->belongsToMany('App\Models\PropertyValue', 'property_value_parents', 'property_value_id', 'parent_id');
    }

    public function children()
    {
        return $this->belongsToMany('App\Models\PropertyValue', 'property_value_parents', 'parent_id', 'property_value_id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Popular extends Model
{
    protected $fillable = [
        'category_id',
        'ordering',
    ];

    public function popularable()
    {
        return $this->morphTo();
    }

    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }
}

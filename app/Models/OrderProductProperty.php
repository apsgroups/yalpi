<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderProductProperty extends Model
{
    protected $fillable = [
        'order_product_id',
        'product_property_id',
        'title',
        'content'
    ];

    public static function boot()
    {
        parent::boot();

        static::saving(function($model)
        {
            if(!$model->productProperty) {
                return;
            }

            $model->content = $model->productProperty->property->title;
            $model->title = $model->productProperty->propertyValue->title;
        });
    }

    public function products()
    {
        return $this->belongsTo('\App\Models\OrderProduct');
    }

    public function productProperty()
    {
        return $this->belongsTo('\App\Models\ProductProperty');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SortTemplate extends Model
{
    protected $fillable = [
        'title',
    ];

    public function categories()
    {
        return $this->belongsToMany('App\Models\Category')->withPivot('ordering', 'items');
    }
}

<?php

namespace App\Models;

use App\Models\Traits\PublishedTrait;
use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    use PublishedTrait;

    protected $fillable = [
        'title',
        'published',
        'tagable_type',
        'tagable_id',
    ];

    public function tagable()
    {
        return $this->morphTo('tagable');
    }

    public function categories()
    {
        return $this->morphedByMany('App\Models\Category', 'taggable');
    }

    public function prettyUrls()
    {
        return $this->morphedByMany('App\Models\PrettyUrl', 'taggable');
    }

    public function delete()
    {
        $this->categories()->detach();

        $this->prettyUrls()->detach();

        return parent::delete();
    }
}
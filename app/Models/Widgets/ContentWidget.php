<?php

namespace App\Models\Widgets;

use Illuminate\Database\Eloquent\Model;

class ContentWidget extends Model
{
    use WidgetTrait;

    protected $fillable = [
        'widget_id',
        'content_id',
        'layout',
    ];

    public function content()
    {
        return $this->belongsTo('App\Models\Content');
    }
}

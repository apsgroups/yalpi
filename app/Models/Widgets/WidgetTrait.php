<?php
/**
 * Created by PhpStorm.
 * User: Fikret
 * Date: 14.04.2016
 * Time: 18:25
 */

namespace App\Models\Widgets;


trait WidgetTrait {
    public function widget()
    {
        return $this->belongsTo('App\Models\Widgets\Widget');
    }
} 
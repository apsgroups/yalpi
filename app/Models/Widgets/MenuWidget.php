<?php

namespace App\Models\Widgets;

use Illuminate\Database\Eloquent\Model;

class MenuWidget extends Model
{
    use WidgetTrait;

    protected $fillable = [
        'widget_id',
        'menu_id',
        'layout',
    ];

    public function menu()
    {
        return $this->belongsTo('App\Models\Menu');
    }
}
<?php

namespace App\Models\Widgets;

use Illuminate\Database\Eloquent\Model;

class PromoWidget extends Model
{
    use WidgetTrait;

    protected $fillable = [
        'widget_id',
        'title',
        'ordering',
    ];

    public function categories()
    {
        return $this->belongsToMany('App\Models\Category');
    }
}

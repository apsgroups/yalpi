<?php

namespace App\Models\Widgets;;

use App\Models\Traits\PublishedTrait;
use Illuminate\Database\Eloquent\Model;

class Widget extends Model
{
    use PublishedTrait;

    protected $fillable = [
        'title',
        'type',
        'published',
        'publish_up',
        'publish_down',
        'position',
        'assign',
        'ordering',
        'note',
        'on_product',
    ];

    public function assigns()
    {
        return $this->hasMany('App\Models\Widgets\WidgetAssign');
    }

    public function menuWidget()
    {
        return $this->hasOne('App\Models\Widgets\MenuWidget');
    }

    public function contentWidget()
    {
        return $this->hasOne('App\Models\Widgets\ContentWidget');
    }

    public function promoWidgetTabs()
    {
        return $this->hasMany('App\Models\Widgets\PromoWidget');
    }

    public function scopeSearch($query, $search)
    {
        return $query->where(function($query) use($search) {
            $query->where('title', 'LIKE', '%'.$search.'%')
                ->orWhere('id', 'LIKE', '%'.$search.'%');
        });
    }
}

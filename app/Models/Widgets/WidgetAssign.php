<?php

namespace App\Models\Widgets;

use Illuminate\Database\Eloquent\Model;

class WidgetAssign extends Model
{
    protected $fillable = [
        'widget_id',
        'menu_item_id',
    ];

    public function widget()
    {
        return $this->belongsTo('App\Models\Widgets\Widget');
    }

    public function menuItem()
    {
        return $this->belongsTo('App\Models\MenuItem');
    }
}
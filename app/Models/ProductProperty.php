<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductProperty extends Model
{
    protected $fillable = [
        'property_value_id',
        'property_id',
        'product_id',
        'value_double',
        'value_string',
        'title',
    ];

    public $timestamps = false;

    public function product()
    {
        return $this->belongsTo('App\Models\Product');
    }

    public function property()
    {
        return $this->belongsTo('App\Models\Property');
    }

    /**
     * @deprecated use propertyValue() instead
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function propertyValues()
    {
        return $this->belongsTo('App\Models\PropertyValue', 'property_value_id');
    }

    public function propertyValue()
    {
        return $this->belongsTo('App\Models\PropertyValue', 'property_value_id');
    }

    public function scopeById($query, $propertyId)
    {
        return $query->where('property_id', $propertyId);
    }
}

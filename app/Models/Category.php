<?php

namespace App\Models;

use App\Models\Traits\FillableTrait;
use App\Models\Traits\ImageTrait;
use App\Models\Traits\MarketTrait;
use App\Models\Traits\MenuItemTrait;
use App\Models\Traits\PopularTrait;
use App\Models\Traits\PublishedTrait;
use App\Models\Traits\RedirectTrait;
use App\Models\Traits\SeoMetaTrait;
use App\Models\Traits\SitemapTrait;
use App\Models\Traits\SlugTrait;
use App\Models\Traits\TagTrait;
use Illuminate\Database\Eloquent\Model;
use Elasticquent\ElasticquentTrait;

class Category extends Model
{
    use SlugTrait,
        PublishedTrait,
        ImageTrait,
        MenuItemTrait,
        SeoMetaTrait,
        PopularTrait,
        SitemapTrait,
        FillableTrait,
        MarketTrait,
        TagTrait,
        ElasticquentTrait,
        RedirectTrait;

    protected $protected = ['id'];

    protected $fillable = [
        'title',
        'alter_title',
        'slug',
        'description',
        'description_bottom',
        'mobile_description_bottom',
        'parent_id',
        'image_id',
        'user_id',
        'composition',
        'show_as_child',
        'search_form',
        'in_row',
        'model',
        'promo_widget',
        'position',
        'type',
        'meta_category_id',
        'meta_product_id',
        'meta_filter_id',
        'child_template',
        'icon',
        'virtual_discounts',
        'autosale',
        'market',
        'vendor',
        'alt_image_id',
        'meter_price',
        'extra_buttons',
        'complects_note',
        'show_filter',
        'hide_products',
        'disabled_options',
        'filter_options',
        'dimension_filter',
        'shipping_rate',
        'modules',
        'assign_modules',
        'info_faq',
        'info_articles',
        'info_reviews',
        'info_vids',
        'info_enabled',
        'autofiltering',
        'autofiltering_category_id',
        'automorph',
        'morph',
        'created_by_import',
        'sub_categories',
        'sub_category_title',
        'sub_category_alt_title',
        'use_sub_category_alt_title',
        'showroom',
        'tag',
        'out_of_stock',
        'landing',
        'docs',
        'product_priority',
        'name_priorities',
        'breadcrumb_title',
        'search_image_id',
        'level_score',
        'filters_deactivate',
        'filters_activate',
        'filter_button',
        'primary_property_id',
        'secondary_property_id',
        'img_bg'
    ];

    protected $casts = [
        'disabled_options' => 'array',
        'filter_options' => 'array',
        'info_faq' => 'array',
        'info_articles' => 'array',
        'info_reviews' => 'array',
        'morph' => 'array',
        'sub_categories' => 'array',
        'docs' => 'array',
        'name_priorities' => 'array',
        'filters_deactivate' => 'array',
        'filters_activate' => 'array',
    ];

    public function getIndexSettings()
    {
        return ['analysis' => config('elasticquent.analysis')];
    }

    public static function boot()
    {
        static::saved(function ($category) {
            (new \App\Services\DenormUrl())->saveCategoryUrl($category);

            if(!$category->parent_id) {
                $category->products()->update(['icon_bg' => $category->img_bg]);
            }
        });

        parent::boot();
    }

    public function link($absolute = false)
    {
        // $this->denorm_url = category_url($this);

        if($absolute) {
            return url($this->denorm_url);
        }

        return $this->denorm_url;
    }

    public function getMappingProperties()
    {
        return [
                'title' => [
                    'type' => 'string',
                    'analyzer' => 'my_analyzer',
                ],
                'published' => [
                    'type' => 'boolean',
                ],
            ];
    }

    public function getIndexDocumentData()
    {
        return [
            'id'      => $this->id,
            'title'   => $this->title,
            'published'   => $this->published,
        ];
    }

    public function getParentContextName()
    {
        return 'parent_id';
    }

    public function parent()
    {
        return $this->belongsTo('App\Models\Category', 'parent_id', 'id');
    }

    public function primaryProperty()
    {
        return $this->belongsTo('App\Models\Property', 'primary_property_id');
    }

    public function secondaryProperty()
    {
        return $this->belongsTo('App\Models\Property', 'secondary_property_id');
    }

    public function children()
    {
        return $this->hasMany('App\Models\Category', 'parent_id', 'id')->orderBy('categories.ordering', 'asc');
    }

    public function products()
    {
        return $this->belongsToMany('App\Models\Product')->withPivot('ordering', 'automodule');
    }

    public function primaryProducts()
    {
        return $this->hasMany('App\Models\Product');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\Access\User');
    }

    public function scopeRoot($query)
    {
        return $query->whereNull('parent_id')->orderBy('ordering', 'asc');
    }

    public function getInRow()
    {
        $inRow = $this->in_row ?: \Settings::get('in_row');

        if(is_mobile()) {
            $inRow = 3;
        }

        return $inRow;
    }

    public function metaCategory()
    {
        return $this->belongsTo('App\Models\Meta', 'meta_category_id');
    }

    public function metaFilter()
    {
        return $this->belongsTo('App\Models\Meta', 'meta_filter_id');
    }

    public function metaProduct()
    {
        return $this->belongsTo('App\Models\Meta', 'meta_product_id');
    }

    public function altImage()
    {
        return $this->belongsTo('App\Models\Image', 'alt_image_id');
    }

    public function searchImage()
    {
        return $this->belongsTo('App\Models\Image', 'search_image_id');
    }

    public function getMetaDesc()
    {
        return $this->metadesc ?: $this->title.\Settings::get('category_prefix_metadesc');
    }

    public function getPageTitle()
    {
        return $this->page_title ?: $this->title.\Settings::get('category_prefix_page_title');
    }

    public function getInfoVids()
    {
        if(empty($this->info_vids)) {
            return [];
        }

        $rows = explode("\n", $this->info_vids);

        $videos = [];

        foreach ($rows as $key => $value) {
            $parts = explode('|', $value);

            if(count($parts) !== 2) {
                continue;
            }

            $videos[$parts[0]] = $parts[1];
        }

        return $videos;
    }

    public function getInfoContent($type = 'articles')
    {
        $var = 'info_'.$type;

        if(empty($this->$var)) {
            return false;
        }

        return \App\Models\Content::published(1)->whereIn('id', $this->$var)->orderBy('created_at', 'desc')->get();
    }

    public function mobileImage()
    {
        return $this->belongsTo('App\Models\Image', 'mobile_image_id');
    }

    public function getChildTitle($category = null)
    {
        $alt = $this->sub_category_title;

        if($category && $category->use_sub_category_alt_title) {
            $alt = $this->sub_category_alt_title;
        }

        return (!empty($alt) ? $alt : $this->title);
    }

    public function getBreadcrumbTitle()
    {
        if($this->breadcrumb_title) {
            return $this->breadcrumb_title;
        }

        return $this->title;
    }
}
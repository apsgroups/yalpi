<?php

namespace App\Models;

use App\Models\Traits\PublishedTrait;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use PublishedTrait;

    protected $fillable = [
        'name',
        'comment',
        'rate',
    ];

    protected $casts = [
        'popularity' => 'integer',
        'likes' => 'integer',
        'dislikes' => 'integer',
        'user_id' => 'integer',
        'user_id' => 'published',
    ];

    public static function boot()
    {
        static::saved(function ($comment) {
        	if(!$comment->commentable) {
        		return;
        	}

            $comments = $comment->commentable->comments()->published(1)->where('rate', '>', 0)->where('parent_id', null)->get();

            $comment->commentable->rate = $comments->avg('rate');
            $comment->commentable->rate_round = floor($comment->commentable->rate);
            $comment->commentable->rate_count = $comment->commentable->comments()->published(1)->count();

            if(isset($comment->commentable->comments_count)) {
                $comment->commentable->comments_count = $comment->commentable->comments()->published(1)->count();
            }

            $comment->commentable->save();

            $parent = $comment->parent;

            if($parent && $comment->published && $parent->user_id !== $comment->user_id && !$comment->notifies()->where('user_id', $parent->user_id)->count()) {
                $comment->notifies()->create(['user_id' => $parent->user_id]);
            }
        });

        parent::boot();
    }

    public function notifies()
    {
        return $this->morphMany('App\Models\Notify', 'notifable');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\Access\User\User');
    }

    public function parent()
    {
        return $this->belongsTo('App\Models\Comment', 'parent_id');
    }

    public function replies()
    {
        return $this->hasMany('App\Models\Comment', 'parent_id');
    }

    public function commentable()
    {
        return $this->morphTo();
    }
}
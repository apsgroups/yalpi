<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'name',
        'user_id',
        'email',
        'phone',
        'comment',
        'mounting',
        'payment_id',
        'shipping_id',
        'city',
        'street',
        'home',
        'apartment',
        'hash',
        'exported',
        'client_id',
    ];

    public function products()
    {
        return $this->hasMany('App\Models\OrderProduct');
    }

    public function payment()
    {
        return $this->belongsTo('App\Models\Payment');
    }

    public function shipping()
    {
        return $this->belongsTo('App\Models\Shipping');
    }
}

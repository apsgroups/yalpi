<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductSet extends Model
{
    protected $fillable = [
        'title',
        'property_id'
    ];

    public function products()
    {
        return $this->hasMany('App\Models\Product');
    }

    public function property()
    {
        return $this->belongsTo('App\Models\Property');
    }
}

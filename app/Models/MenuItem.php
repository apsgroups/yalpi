<?php

namespace App\Models;

use App\Models\Traits\ImageTrait;
use App\Models\Traits\PublishedTrait;
use App\Models\Traits\RedirectTrait;
use App\Models\Traits\SlugTrait;
use Illuminate\Database\Eloquent\Model;

class MenuItem extends Model
{
    use SlugTrait,
        PublishedTrait,
        ImageTrait,
        RedirectTrait;

    public function getParentContextName()
    {
        return 'parent_id';
    }

    protected $fillable = [
        'title',
        'slug',
        'parent_id',
        'published',
        'type',
        'item_id',
        'menu_id',
        'description',
        'user_id',
        'ordering',
        'link',
        'icon',
        'target',
        'image_id',
        'bold',
        'noindex',
        'showcase',
        'only_showcase',
        'note',
        'bold_catalog',
        'hide_children_mainmenu',
        'hide_sidebar',
        'created_by_import',
        'show_product_count',
        'label_text',
        'label_style',
    ];

    public static function boot()
    {
        static::saved(function ($menu) {
            (new \App\Services\DenormUrl())->saveMenuUrl($menu);
        });

        parent::boot();
    }

    public function menu()
    {
        return $this->belongsTo('App\Models\Menu');
    }

    public function parent()
    {
        return $this->belongsTo('App\Models\MenuItem', 'parent_id', 'id');
    }

    public function children()
    {
        return $this->hasMany('App\Models\MenuItem', 'parent_id', 'id')->orderBy('ordering', 'asc');
    }

    public function scopeOfType($query)
    {
        return $query->where('type', 'catalog');
    }

    public function scopeOfCategory($query, $id)
    {
        return $query->where(['type' => 'category', 'item_id' => $id]);
    }

    public function scopeOfContent($query, $id)
    {
        return $query->where(['type' => 'content', 'item_id' => $id]);
    }

    public function scopeOfContentType($query, $id)
    {
        return $query->where(['type' => 'contentType', 'item_id' => $id]);
    }

    public function scopeChildsOf($query, $id)
    {
        return $query->where('parent_id', $id)->orderBy('ordering', 'asc');
    }

    public function widgetAssign()
    {
        return $this->hasMany('App\Models\WidgetAssign');
    }

    public function getThumbnailPath()
    {
        return $this->id;
    }

    public function link($absolute = false)
    {
        // $this->denorm_url = menu_url($this->id);

        if($absolute) {
            return url($this->denorm_url);
        }

        return $this->denorm_url;
    }

    public function getBreadcrumbTitle()
    {
        if($this->type === 'category' && $category = Category::find($this->item_id)) {
            if($category->breadcrumb_title) {
                return $category->breadcrumb_title;
            }
        }

        return $this->title;
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    protected $fillable = [
        'name',
        'email',
        'phone',
        'city',
        'subscribe',
        'order_number',
        'message',
        'order_id',
        'product_id',
        'additional_1',
        'additional_2',
        'additional_3',
        'is_opt',
    ];

    public function product()
    {
        return $this->belongsTo('App\Models\Product');
    }

    public function order()
    {
        return $this->belongsTo('App\Models\Order');
    }

    public function multiupload()
    {
        return $this->morphOne('App\Models\Multiupload', 'multiuploadable');
    }
}

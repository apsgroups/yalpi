<?php

namespace App\Models;

use App\Models\Traits\PublishedTrait;
use App\Models\Traits\SlugTrait;
use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    use SlugTrait, PublishedTrait;

    protected $fillable = [
        'title',
        'slug',
        'type',
        'ordering',
        'position',
        'multiple',
        'published',
        'filter',
        'single_title',
        'display_tag',
    ];

    protected $casts = [
        'published' => 'integer',
        'filter' => 'integer',
        'multiple' => 'integer',
    ];

    public static function boot()
    {
        parent::boot();

        static::saving(function($model)
        {
            if(empty($model->single_title)) {
                $model->single_title = $model->title;
            }
        });
    }

    public function values()
    {
        return $this->hasMany('App\Models\PropertyValue');
    }

    public function productProperties()
    {
        return $this->hasMany('App\Models\ProductProperty');
    }

    public function productSets()
    {
        return $this->hasMany('App\Models\ProductSet');
    }

    public function scopeProduct($query, $productId)
    {
        return $query->where('product_id', $productId);
    }

    public function scopeFilter($query, $state)
    {
        return $query->where('filter', $state);
    }

    public function productTypes()
    {
        return $this->belongsToMany('App\Models\ProductType');
    }
}

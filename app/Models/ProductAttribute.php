<?php

namespace App\Models;

use App\Models\Traits\ImageTrait;
use App\Models\Traits\ProductPriceTrait;
use Illuminate\Database\Eloquent\Model;

class ProductAttribute extends Model
{
    use ProductPriceTrait,
        ImageTrait;

    protected $fillable = [
        'product_id',
        'attribute_id',
        'sku',
        'image_id',
        'price',
        'price_old',
        'price_wholesale',
        'price_old_wholesale',
        'price_base',
        'price_base_wholesale',
        'discount',
        'discount_from',
        'ordering',
        'value',
        'override_price',
        'ext_id',
        'description',
        'took_import',
        'updated_by_import',
        'in_stock',
    ];

    public function product()
    {
        return $this->belongsTo('App\Models\Product');
    }

    public function attribute()
    {
        return $this->belongsTo('App\Models\Attribute');
    }

    public function getTitleAttribute($value)
    {
        return $this->value;
    }

    public function getThumbnailPath()
    {
        return $this->product->link().'attributes/'.$this->attribute->id.'/'.$this->id;
    }

    public function getPrice()
    {
        if($this->override_price) {
            return $this->price;
        }

        return $this->product->price;
    }

    public function getPriceBase()
    {
        if($this->override_price) {
            return $this->price_base;
        }

        return $this->product->price_base;
    }

    public function getPriceWholesale()
    {
        if($this->override_price) {
            return $this->price_wholesale;
        }

        return $this->product->price_wholesale;
    }

    public function getPriceOld()
    {
        if($this->override_price) {
            return $this->price_old;
        }

        return $this->product->price_old;
    }

    public function getSku()
    {
        if($this->sku) {
            return $this->sku;
        }

        return $this->product->sku;
    }

    public function getImage()
    {
        if($this->image) {
            return $this->image;
        }

        return $this->product->image;
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\SlugTrait;

class Language extends Model
{
    use SlugTrait;

    protected $fillable = [
        'title',
        'slug',
    ];
}

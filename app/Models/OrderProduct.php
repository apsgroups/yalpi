<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderProduct extends Model
{
    protected $fillable = [
        'title',
        'sku',
        'price',
        'amount',
        'order_id',
        'product_id',
        'attribute_title',
        'attribute_value',
        'product_attribute_id',
        'parent_id',
        'desk_id',
        'desk_title',
    ];

    public function order()
    {
        return $this->belongsTo('App\Models\Order');
    }

    public function product()
    {
        return $this->belongsTo('App\Models\Product');
    }

    public function modules()
    {
        return $this->hasMany('App\Models\OrderProduct', 'parent_id');
    }

    public function composition()
    {
        return $this->belongsTo('App\Models\OrderProduct', 'parent_id');
    }

    public function productAttribute()
    {
        return $this->belongsTo('App\Models\ProductAttribute');
    }

    public function orderProductProperty()
    {
        return $this->hasMany('App\Models\OrderProductProperty');
    }

    public function desk()
    {
        return $this->belongsTo('App\Models\Product', 'desk_id');
    }
}
<?php

namespace App\Models\Import;

use Illuminate\Database\Eloquent\Model;

class ImportMissing extends Model
{
    protected $fillable = [
        'articul',
        'name',
        'sku',
    ];
}

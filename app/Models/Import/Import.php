<?php

namespace App\Models\Import;

use Illuminate\Database\Eloquent\Model;

class Import extends Model
{
    protected $fillable = [
        'type',
        'manual',
        'abort',
    ];

    protected $casts = [
        'manual' => 'integer',
        'total' => 'integer',
        'processed' => 'integer',
        'abort' => 'boolean',
    ];
}

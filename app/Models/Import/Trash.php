<?php

namespace App\Models\Import;

use Illuminate\Database\Eloquent\Model;

class Trash extends Model
{
    protected $fillable = [
        'articul',
    ];
}
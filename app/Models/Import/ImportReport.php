<?php

namespace App\Models\Import;

use Illuminate\Database\Eloquent\Model;

class ImportReport extends Model
{
    protected $fillable = [
        'operation',
        'user_id',
    ];

    public function scopeOperation($query, $operation)
    {
        return $query->where('operation', $operation)->orderBy('id', 'desc');
    }
}

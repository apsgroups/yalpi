<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReviewResponse extends Model
{
    protected $fillable = [
        'name',
        'response',
        'post',
        'user_id',
    ];

    public function review()
    {
        return $this->belongsTo('App\Models\Review');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}

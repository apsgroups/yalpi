<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\ImageTrait;

class Cource extends Model
{
    use ImageTrait;

    public function scopeFree($query, $state=1)
    {
        return $query->where('free', $state);
    }

    public function scopeOrdered($query)
    {
        return $query->orderBy('ordering', 'asc');
    }

    public function getThumbnailPath()
    {
        return '/cources/'.$this->id;
    }
}

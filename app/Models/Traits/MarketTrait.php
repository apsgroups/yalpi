<?php

namespace App\Models\Traits;

trait MarketTrait
{
    public function getFillableMarketTrait()
    {
        return [
            'sales_notes',
            'market_title',
            'market_available',
            'market_available_apply',
            'market_category_id',
        ];
    }

    public function market_category()
    {
        return $this->belongsTo('App\Models\MarketCategory');
    }
}
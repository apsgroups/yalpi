<?php

namespace App\Models\Traits;

trait CommentTrait
{
    public function comments()
    {
        return $this->morphMany('App\Models\Comment', 'commentable');
    }
}
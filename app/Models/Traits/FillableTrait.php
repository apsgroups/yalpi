<?php

namespace App\Models\Traits;

trait FillableTrait {

    public function getFillable()
    {
        $fillable = $this->fillable;

        $class = static::class;

        foreach (class_uses_recursive($class) as $trait) {
            if (method_exists($this, $method = 'getFillable'.class_basename($trait))) {
                $fillable = array_merge($fillable, call_user_func([$class, $method]));
            }
        }

        return $fillable;
    }
}
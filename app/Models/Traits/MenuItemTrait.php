<?php

namespace App\Models\Traits;

trait MenuItemTrait
{
    public function menuItem()
    {
        $className = camel_case(class_basename($this));

        return $this->hasMany('App\Models\MenuItem', 'item_id')->where('type', $className);
    }
}
<?php

namespace App\Models\Traits;

trait PopularTrait
{
    public function populars()
    {
        return $this->morphMany('App\Models\Popular', 'popularable');
    }
}
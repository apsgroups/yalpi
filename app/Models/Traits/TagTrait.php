<?php

namespace App\Models\Traits;

trait TagTrait
{
    public function tags()
    {
        return $this->morphToMany('App\Models\Tag', 'taggable');
    }

    public function tagLinks()
    {
        return $this->morphMany('App\Models\Tag', 'taggable');
    }
}
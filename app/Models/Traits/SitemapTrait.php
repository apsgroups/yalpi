<?php

namespace App\Models\Traits;

trait SitemapTrait
{
    public function getFillableSitemapTrait()
    {
        return [
            'sitemap',
            'priority',
            'changefreq',
            'sitemap_items',
        ];
    }

    public function scopeSitemap($query, $state=1)
    {
        return $query->where('sitemap', $state);
    }
}
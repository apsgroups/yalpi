<?php

namespace App\Models\Traits;

trait ImageTrait
{
    public function images()
    {
        return $this->morphMany('App\Models\Image', 'imageable');
    }

    public function image()
    {
        return $this->belongsTo('App\Models\Image');
    }

    public function getThumbnailPath()
    {
        return $this->link();
    }
}
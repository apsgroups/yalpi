<?php

namespace App\Models\Traits;

trait SeoMetaTrait
{
    public function getFillableSeoMetaTrait()
    {
        return [
            'metadesc',
            'metakeys',
            'page_title',
        ];
    }

    public function getMetaDesc()
    {
        return $this->metadesc;
    }

    public function getMetaKeys()
    {
        return $this->metakeys;
    }

    public function getPageTitle()
    {
        return $this->page_title ?: $this->title;
    }
}
<?php

namespace App\Models\Traits;

trait RedirectTrait
{
    public function redirect()
    {
        return $this->morphOne('App\Models\Redirect', 'item');
    }
}
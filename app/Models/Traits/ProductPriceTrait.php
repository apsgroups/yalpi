<?php

namespace App\Models\Traits;

trait ProductPriceTrait
{
    public static function boot()
    {
        parent::boot();

        static::saving(function($product)
        {
            $product->setPriceAttribute();
            $product->setPriceOldAttribute();
            $product->setPriceWholesaleAttribute();
            $product->setPriceOldWholesaleAttribute();
        });
    }

    public function getPriceAttribute($value)
    {
        if(is_wholesale()) {
            return $this->price_wholesale;
        }

        return $value;
    }

    public function getPriceOldAttribute($value)
    {
        if(is_wholesale()) {
            return $this->price_old_wholesale;
        }

        return $value;
    }

    public function setPriceAttribute()
    {
        $price = $this->getAttribute('price_base');
        $discount = $this->getAttribute('discount');

        if($discount > 0 && $this->getAttribute('discount_from') <= 1) {
            $price = $price - ($price / 100 * $discount);
            $price = round($price);
        }

        $virtualDiscount = $this->getAttribute('virtual_discount');

        if(\Settings::get('virtual_discount_stock') && $virtualDiscount > 0 && !$this->getAttribute('in_stock')) {
            //$price = ($price / (100 - $virtualDiscount)) * 100;
            //$price = round($price);
        }

        $this->attributes['price'] = $price;
    }

    public function setPriceOldAttribute()
    {
        $virtualDiscount = $this->getAttribute('virtual_discount');
        $discount = $this->getAttribute('discount');

        if(!$discount && \Settings::get('virtual_discount_stock') && $virtualDiscount > 0 && !$this->getAttribute('in_stock')) {
            $this->attributes['price_old'] = 0;
            return;
        }

        $priceOld = 0;

        if($discount > 0 && $this->getAttribute('discount_from') <= 1) {
            $priceOld = $this->getAttribute('price_base');
        }

        if($virtualDiscount > 0 && $this->getAttribute('in_stock')) {
            $priceOld = ($this->getAttribute('price_base') / (100 - $virtualDiscount)) * 100;
            $priceOld = round($priceOld);
        }

        $this->attributes['price_old'] = $priceOld;
    }

    public function setPriceWholesaleAttribute()
    {
        $price = $this->getAttribute('price_base');
        $discount = $this->getAttribute('discount');

        if($discount > 0 && $this->getAttribute('discount_from') <= 1) {
            $price = $price - ($price / 100 * $discount);
        }

        if($price > 0) {
            $price = $price - ($price / 100 * config('price.wholesale', 40));
        }

        $this->attributes['price_wholesale'] = round($price);
    }

    public function setPriceOldWholesaleAttribute()
    {
        $price = 0;

        if($this->getAttribute('discount') > 0 && $this->getAttribute('discount_from') <= 1) {
            $price = $this->getAttribute('price_base');
            $price = $price - ($price / 100 * config('price.wholesale', 40));
        }

        $this->attributes['price_old_wholesale'] = $price;
    }
}
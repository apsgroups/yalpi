<?php

namespace App\Models\Traits;

trait SlugTrait
{
    protected $slugSourceName = 'title';
    protected $slugName = 'slug';
    protected $parentContextName;

    public static function bootSlugTrait()
    {
        static::saving(function($model){
            $model->createSlug();
        });
    }

    public function getSlugName()
    {
        return $this->slugName;
    }

    public function getSlug()
    {
        return $this->getAttribute($this->getSlugName());
    }

    public function setSlug($slug)
    {
        return $this->setAttribute($this->getSlugName(), $slug);
    }

    public function getSlugSourceName()
    {
        return $this->slugSourceName;
    }

    public function getSlugSource()
    {
        return $this->getAttribute($this->getSlugSourceName());
    }

    public function getParentName()
    {
        return $this->parentContextName;
    }

    public function getParentSource()
    {
        return $this->getAttribute($this->getSlugSourceName());
    }

    public function getParentContextName()
    {
        return $this->parentContextName;
    }

    public function getParentContext()
    {
        return $this->getAttribute($this->getParentContextName());
    }

    protected function createSlug($loop=0)
    {
        $slug = $this->getSlug();

        if (!empty($slug))
            return false;

        $append = ($loop > 0) ? ' '.$loop : '';

        $slug = str_sef_slug($this->getSlugSource().$append);

        $query = $this->newQuery()->where($this->getSlugName(), $slug);

        if($this->getKey()) {
            $query->where($this->getKeyName(), '<>', $this->getKey());
        }

        if(!is_null($this->getParentContextName())) {
            $query->where($this->getParentContextName(), $this->getParentContext());
        }

        if(!$query->exists()) {
            $this->setSlug($slug);
            return true;
        }

        return $this->createSlug($loop+1);
    }

    public function scopeSlug($query, $slug)
    {
        return $query->where($this->getSlugName(), $slug);
    }
}
<?php

namespace App\Models\Traits;

trait PublishedTrait
{
    protected $publishedName = 'published';

    public function getFillablePublishedTrait()
    {
        return [
            $this->getPublishedName(),
        ];
    }

    public function getPublishedName()
    {
        return $this->publishedName;
    }

    public function scopePublished($query, $state=1)
    {
        return $query->where($this->getPublishedName(), $state);
    }
}
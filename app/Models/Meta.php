<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Meta extends Model
{
    protected $fillable = [
        'title',
        'meta_title',
        'meta_description',
        'meta_keywords',
        'heading',
        'description',
    ];

    public function forCategory()
    {
        return $this->hasMany('\App\Models\Category', 'meta_category_id');
    }

    public function forProduct()
    {
        return $this->hasMany('\App\Models\Category', 'meta_product_id');
    }
}

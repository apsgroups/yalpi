<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductTypeGroup extends Model
{
    protected $fillable = [
        'title',
    ];

    public function properties()
    {
        return $this->belongsToMany('App\Models\Property', 'group_property', 'group_id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Shipping extends Model
{
    protected $fillable = [
        'title',
        'price',
        'note',
        'publishing',
        'ordering',
    ];

    public function orders()
    {
        return $this->hasMany('App\Models\Order');
    }
}

<?php

namespace App\Models;

use App\Facades\Properties;
use App\Models\Traits\FillableTrait;
use App\Models\Traits\ImageTrait;
use App\Models\Traits\MarketTrait;
use App\Models\Traits\PopularTrait;
use App\Models\Traits\ProductPriceTrait;
use App\Models\Traits\PublishedTrait;
use App\Models\Traits\RedirectTrait;
use App\Models\Traits\SeoMetaTrait;
use App\Models\Traits\SitemapTrait;
use App\Services\Frontend\DimensionService;
use App\Services\Frontend\ProductPropertyService;
use Elasticquent\ElasticquentTrait;
use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\SlugTrait;
use App\Models\Traits\CommentTrait;

class Product extends Model
{
    use SlugTrait,
        PublishedTrait,
        ImageTrait,
        ElasticquentTrait,
        SeoMetaTrait,
        PopularTrait,
        SitemapTrait,
        FillableTrait,
        MarketTrait,
        RedirectTrait,
        ProductPriceTrait,
        CommentTrait;

    protected $guarded = ['id'];

    protected $fillable = [
        'title',
        'preview_title',
        'slug',
        'description',
        'price_base',
        'discount',
        'category_id',
        'product_type_id',
        'user_id',
        'image_id',
        'novelty',
        'sale',
        'preorder',
        'bestseller',
        'override_meta',
        'soon',
        'out_of_stock',
        'start_learning',
        'experience',
        'priority',
        'language_id',
        'video',
        'icon_pack',
        'icon_name',
        'video_format',
        'price_type',
        'online_learning'
    ];

    protected $casts = [
        'price' => 'double',
        'product_type_id' => 'integer',
        'override_meta' => 'integer',
        'video_format' => 'integer',
    ];

    protected $propertyValues;

    public function getIndexSettings()
    {
        return ['analysis' => config('elasticquent.analysis')];
    }

    public function link($absolute = false)
    {
        $url = product_url($this);

        if($absolute) {
            $url = url($url);
        }

        return $url;
    }

    public function getMappingProperties()
    {
        $mapping = new \App\Services\Filter\MappingService();

        return $mapping->getMapping();
    }

    public function getIndexDocumentData()
    {
        $properties = $this->properties()->get();

        $categories = $this->categories()->get();
        
        $composition = $this->categories()->where('composition', 1)->first();

        $categoryOrdering = $categories->pluck('pivot')->map(function ($item, $key) {
            unset($item['product_id']);
            return $item;
        })->toArray();


        // $searchKeys[] = $this->type->title;

        $searchKeys = $categories->pluck('title')->toArray();
        $searchKeys[] = $this->createPreviewTitle();
        $searchKeys[] = $this->sku;
        $searchKeys[] = $this->type->search_keys;

        $categoryPriority = $this->getCategoryPriority();

        $doc = [
            'id'      => $this->id,
            'title'   => $this->title,
            'search_keys'   => implode(', ', $searchKeys),
            'created_at'   => $this->created_at->timestamp,
            'novelty'   => $this->novelty,
            'sale'   => $this->sale,
            'published'   => $this->published,
            'preorder'   => $this->preorder,
            'in_stock'   => $this->in_stock > 0 ? 1 : 0,
            'price'   => doubleval($this->price),
            'discount'   => doubleval($this->discount),
            'product_categories' => array_values(array_unique($categories->pluck('id')->toArray())),
            'category_order' => $categoryOrdering,
            'product_composition' => @$composition->id,
            'product_type_id' => $this->product_type_id,
            'search_priority' => (int)$this->type->search_priority,
            'category_priority' => (int)$categoryPriority,
            'sets_ordering' => (int)$this->sets_ordering,
            'out_of_stock' => (int)$this->out_of_stock,
            'price_priority' => $categoryPriority > 0 ? doubleval($this->price) : 0,
            'rate' => (int)$this->rate,
            'price-type' => $this->price_type,
        ];

        foreach($properties as $prop) {
            // color sort priority
            $value = ($prop->property->type === 'double') ? $prop->value_double : @$prop->property_value_id;

            if($prop->property->multiple) {
                $doc[$prop->property->slug][] = $value;
            } else {
                $doc[$prop->property->slug] = $value;
            
                if($prop->property->type === 'double') {
                    $doc['range_'.$prop->property->slug] = (int)$value;
                }
            }
        }

        // $doc = $this->addPrimaryProperty($doc);
        
        return $doc;
    }

    /*
    первый уровень предполагал выводить из админки по опциям, а не по файлу.
    То есть, в опции E-mail указываем родительскую опцию https://yalpi.org/admin/property-values/118/edit/ 
   https://prnt.sc/qu62hw и получаем  https://prnt.sc/qu62oq 
    protected function addPrimaryProperty($properties)
    {
        $root = $this->categories()->whereNull('parent_id')->first();

        if(!$root || !$root->primaryProperty || !$root->secondaryProperty || empty($properties[$root->secondaryProperty->slug])) {
            return $properties;
        }

        $properties[$root->primaryProperty->slug] = \DB::table('property_value_parents')
                                    ->whereIn('property_value_id', $properties[$root->secondaryProperty->slug])
                                    ->pluck('parent_id');

        return $properties;
    }
    */


    protected function getCategoryPriority()
    {
        if($this->order_priority) {
            return $this->order_priority;
        }

        $categoryPriority = $this->category->product_priority;

        if($categoryPriority) {
            return $categoryPriority;
        }

        $categories = $this->categories()->get();

        foreach ($categories as $c) {
            if($c->product_priority > 0) {
                $categoryPriority = $c->product_priority;
                continue;
            }

            if(!$c->name_priorities) {
                continue;
            }

            foreach ($c->name_priorities as $key => $value) {
                if($value['priority'] > $categoryPriority && $value['name'] && preg_match('/'.$value['name'].'/', $this->title.' '.$this->preview_title)) {
                    $categoryPriority = $value['priority'];
                }
            }
        }

        return $categoryPriority;
    }

    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }

    public function language()
    {
        return $this->belongsTo('App\Models\Language');
    }

    public function categories()
    {
        return $this->belongsToMany('App\Models\Category')->withPivot('ordering');
    }

    public function authors()
    {
        return $this->belongsToMany('App\Models\Author')->withPivot('ordering');
    }

    public function relateds()
    {
        return $this->belongsToMany('App\Models\Product', 'product_related', 'product_id', 'related_id')->withPivot('ordering');
    }

    public function scopeOrdered($query) 
    {
        return $query->orderBy('product_related.ordering', 'asc');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\Access\User\User');
    }

    public function type()
    {
        return $this->belongsTo('App\Models\ProductType', 'product_type_id');
    }

    public function properties()
    {
        return $this->hasMany('App\Models\ProductProperty');
    }

    public function features()
    {
        return $this->hasMany('App\Models\Feature');
    }

    public function cources()
    {
        return $this->hasMany('App\Models\Cource');
    }

    public function contents()
    {
        return $this->belongsToMany('App\Models\Content')->withPivot('ordering');
    }

    public function hit()
    {
        return $this->hasOne('App\Models\Product');
    }

    public function getParentContextName()
    {
        return 'category_id';
    }

    public function getCategoryIdList()
    {
        return $this->categories()->lists('category_id')->all();
    }

    public function propertyBySlug($slug)
    {
        $property = Properties::slug($slug);

        if(!$property)
            return null;

        return $this->getProductProperty($property->id);
    }

    public function getPropertyValues()
    {
        $productPropertyService = new ProductPropertyService();
        $productPropertyService->setProduct($this);

        return $productPropertyService->getPropertyValue();
    }

    protected function loadPropertyValues()
    {
        $properties = $this->properties;

        $values = [];

        foreach($properties as $prop) {
            $property = Properties::get($prop->property_id);
            $value = $prop->{Properties::getField($prop->property_id)};

            if($property->multiple) {
                $values[$prop->property_id][] = $value;
            } else {
                $values[$prop->property_id] = $value;
            }
        }

        $this->propertyValues = $values;
    }

    public function getProductProperty($id)
    {
        if(is_null($this->propertyValues))
            $this->loadPropertyValues();

        if(isset($this->propertyValues[$id]))
            return $this->propertyValues[$id];

        return null;
    }

    public function getProperties($id)
    {
        $productPropertyService = new ProductPropertyService();
        $productPropertyService->setProduct($this);

        return $productPropertyService;
    }

    public function getMetaDesc()
    {
        return $this->metadesc ?: $this->title.\Settings::get('product_prefix_metadesc');
    }

    public function getPageTitle()
    {
        return $this->page_title ?: $this->title.\Settings::get('product_prefix_page_title');
    }

    protected function getUrlAttribute()
    {
        return $this->link();
    }

    public function createPreviewTitle()
    {
        $title = $this->preview_title ?: $this->title;

        if(!empty($this->type)) {
            $title = $this->type->display_name ?  $this->type->title.' '.$title : $title;

            $separator = in_array($this->product_type_id, config('properties.dimensions.preview_separators', [])) ? ' | ' : ' ';

            $title = $this->type->preview_dimensions && $this->dimensions ? $title.$separator.$this->dimensions : $title;
        }

        return $title;
    }

    public function getIconPath()
    {
        if(!$this->icon_pack || !$this->icon_name) {
            return '';
        }

        return '/images/icon'/*$this->root_category_id*/.'/'.$this->icon_pack.'/'.$this->icon_name.'.svg';
    }

    public function getPreviewData()
    {
        $p = new \stdClass();

        $p->id = $this->id;
        $p->slug = $this->slug;
        $p->title = $this->title;
        $p->preview_title = $this->preview_title;
        $p->shortTitle = $this->title;
        $p->price = $this->price;
        $p->price_old = $this->price_old;
        $p->discount = $this->discount;
        $p->novelty = $this->novelty;
        $p->bestseller = $this->bestseller;
        $p->sale = $this->sale;
        $p->preorder = $this->preorder;
        $p->in_stock = $this->in_stock;
        $p->rate = $this->rate_round;
        $p->icon_bg = $this->icon_bg;
        $p->view_hits = $this->view_hits;
        $p->comments_count = $this->comments_count;
        $p->icon = $this->getIconPath();

        $p->type = new \stdClass();
        $p->type->id = $this->type->id;
        $p->type->title = $this->type->title;
        $p->type->display_tag = $this->type->display_tag; 

        $p->category = new \stdClass();
        $p->category->id = $this->category->id;
        $p->category->title = $this->category->title;
        $p->category->tag = $this->category->tag;

        $p->authors = $this->authors()->ordered()->get()->pluck('name')->toArray();

        $p->soon = $this->soon;
        $p->product_type_id = $this->product_type_id;
        $p->url = $this->link();
        $p->video = $this->video;
        $p->image = $this->image;

        return $p;
    }
}
<?php

namespace App\Models;

use App\Models\Traits\FillableTrait;
use App\Models\Traits\ImageTrait;
use App\Models\Traits\MenuItemTrait;
use App\Models\Traits\PublishedTrait;
use App\Models\Traits\RedirectTrait;
use App\Models\Traits\SeoMetaTrait;
use App\Models\Traits\SitemapTrait;
use App\Models\Traits\SlugTrait;
use Illuminate\Database\Eloquent\Model;
use \Conner\Tagging\Taggable;
use App\Models\Traits\CommentTrait;

class Content extends Model
{
    use SlugTrait,
        PublishedTrait,
        ImageTrait,
        MenuItemTrait,
        SeoMetaTrait,
        SitemapTrait,
        FillableTrait,
        RedirectTrait,
        Taggable,
        CommentTrait;

    public function getParentContextName()
    {
        return 'content_type_id';
    }

    protected $fillable = [
        'title',
        'slug',
        'intro',
        'more',
        'wholesaler',
        'image_id',
        'content_type_id',
        'user_id',
        'visual_editor',
        'more_mobile',
    ];

    public function type()
    {
        return $this->belongsTo('App\Models\ContentType', 'content_type_id');
    }

    public function scopeOfType($query, $slug)
    {
        return $query->whereHas('type', function($query) use($slug) {
            $query->where('slug', $slug);
        });
    }

    public function products()
    {
        return $this->belongsToMany('App\Models\Product')->withPivot('ordering');
    }

    public function user()
    {
        return $this->belongsTo(\App\Models\Access\User\User::class);
    }

    public function relatedContent()
    {
        return $this->belongsToMany('App\Models\Content', 'content_related', 'content_id', 'related_id' )->withPivot('ordering');
    }

    public function getIntro($limit = 300)
    {
        $intro = strip_tags($this->intro) ?: strip_tags($this->more);

        return str_limit($intro, $limit);
    }

    public function link($absolute = false)
    {
        $url = content_url($this);

        if($absolute) {
            $url = url($url);
        }

        return $url;
    }

    public function metaForCategory()
    {
        $this->belongsTo('\App\Models\Meta');
    }

    public function getMetaDesc()
    {
        if($this->content_type_id === 1) {
            return $this->title.' | Последние новости из мира кухонь и мебели.';
        }

        return $this->metadesc;
    }

    public function getPageTitle()
    {
        if($this->content_type_id === 1) {
            return $this->title.' - Новости Mebel169.ru от '.$this->created_at->format('d.m.Y');
        }

        return $this->page_title ?: $this->title;
    }
}

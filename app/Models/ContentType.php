<?php

namespace App\Models;

use App\Models\Traits\FillableTrait;
use App\Models\Traits\MenuItemTrait;
use App\Models\Traits\PublishedTrait;
use App\Models\Traits\SitemapTrait;
use App\Models\Traits\SlugTrait;
use App\Models\Traits\SeoMetaTrait;
use Illuminate\Database\Eloquent\Model;

class ContentType extends Model
{
    use SlugTrait,
        MenuItemTrait,
        PublishedTrait,
        FillableTrait,
        SitemapTrait,
        SeoMetaTrait;

    protected $fillable = [
        'title',
        'slug',
        'description',
        'admin_menu',
        'user_id',
        'per_page',
    ];

    public function contents()
    {
        return $this->hasMany('App\Models\Content');
    }

    public function link($absolute = false)
    {
        $url = content_type_url($this);

        if($absolute) {
            $url = url($url);
        }

        return $url;
    }
}
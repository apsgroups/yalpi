<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $fillable = [
        'title',
        'original_name',
        'ext',
        'thumbnail',
        'ext_preview',
    ];

    public function imageable()
    {
        return $this->morphTo();
    }

    public function product()
    {
        return $this->belongsTo('App\Models\Product');
    }

    public function content()
    {
        return $this->belongsTo('App\Models\Content');
    }

    public function propertyValue()
    {
        return $this->belongsTo('App\Models\PropertyValue');
    }

    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }
}

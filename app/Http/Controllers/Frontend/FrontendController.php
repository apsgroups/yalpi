<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;

/**
 * Class FrontendController
 * @package App\Http\Controllers
 */
class FrontendController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view(mobile_prefix().'.homepage.index');
    }

    public function english()
    {
        \App::setLocale('en');

        return view(mobile_prefix().'.dummy.english');
    }
}

<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class WholesalerController extends Controller
{
    public function on()
    {
        session(['wholesaler.on' => true]);

        return view('frontend.wholesaler.on');
    }

    public function off()
    {
        session(['wholesaler.on' => false]);

        return view('frontend.wholesaler.off');
    }

    public function access()
    {
        if(request()->ajax()) {
            return view('frontend.wholesaler.partials.access');
        }

        return view('frontend.wholesaler.index');
    }

    public function password()
    {
        $password = \Settings::get('price_password', 'bestprice')

        if(request()->get('password') === $password) {
            return ['status' => true];
        }

        return ['status' => false];
    }
}

<?php
namespace App\Http\Controllers\Frontend\Feedback;

use App\Http\Controllers\Controller;
use App\Models\Feedback;
use Illuminate\Http\Request;

abstract class FeedbackController extends Controller
{
    protected $feedback;

    public function index()
    {
        $feedbackName = $this->feedback->getName();

        if(\Request::ajax()) {
            return view($this->layout('index'));
        }

        return view(mobile_prefix().'.feedback.index', compact('feedbackName'));
    }

    protected function sendBase($attributes)
    {
            $feedback = $this->feedback->send($attributes);
        try {
        } catch(\Exception $e) {
            if(request()->ajax()) {
                return response()->json([
                    'status' => 'error',
                    'msg' => $e->getMessage()
                ]);
            }

            return redirect()->back()->withInput()->withFlashDanger($e->getMessage());
        }

        if(request()->ajax()) {
            return response()->json([
                'status' => 'ok',
                'msg' => view($this->layout('thankyou'), compact('feedback'))->render()
            ]);
        }

        return redirect()->route('feedback.'.$this->getName().'.thankyou')->with('feedback.id', $feedback->id);
    }

    public function thankYou()
    {
        $id = session()->pull('feedback.id');

        if(empty($id)) {
            return redirect()->route('frontend.index');
        }

        $feedback = Feedback::findOrFail($id);

        $feedbackName = $this->getName();

        return view(mobile_prefix().'.feedback.thankyou', compact('feedback', 'feedbackName'));
    }

    protected function getName()
    {
        return $this->feedback->getName();
    }

    protected function layout($name)
    {
        return mobile_prefix().'.feedback.'.$this->getName().'.'.$name;
    }
} 
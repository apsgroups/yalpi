<?php

namespace App\Http\Controllers\Frontend\Feedback;

use App\Http\Requests\Frontend\Feedback\Director;

class DirectorController extends FeedbackController
{
    public function __construct()
    {
        $this->feedback = new \App\Services\Feedback\Director();
    }

    public function send(Director $request)
    {
        return parent::sendBase($request->all());
    }
}
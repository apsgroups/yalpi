<?php

namespace App\Http\Controllers\Frontend\Feedback;

use App\Http\Requests\Frontend\Feedback\Contact;

class ContactController extends FeedbackController
{
    public function __construct()
    {
        $this->feedback = new \App\Services\Feedback\Contact();
    }

    public function send(Contact $request)
    {
        return parent::sendBase($request->all());
    }
}
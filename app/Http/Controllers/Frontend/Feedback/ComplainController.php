<?php

namespace App\Http\Controllers\Frontend\Feedback;

use App\Http\Requests\Frontend\Feedback\Complain;

class ComplainController extends FeedbackController
{
    public function __construct()
    {
        $this->feedback = new \App\Services\Feedback\Complain();
    }

    public function send(Complain $request)
    {
        return $this->sendBase($request->all());
    }
}

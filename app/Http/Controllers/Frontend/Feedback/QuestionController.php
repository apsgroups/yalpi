<?php

namespace App\Http\Controllers\Frontend\Feedback;

use App\Http\Requests\Frontend\Feedback\Question;

class QuestionController extends FeedbackController
{
    public function __construct()
    {
        $this->feedback = new \App\Services\Feedback\Question();
    }

    public function send(Question $request)
    {
        return parent::sendBase($request->all());
    }
}
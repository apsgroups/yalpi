<?php

namespace App\Http\Controllers\Frontend\Feedback;

use App\Http\Requests\Frontend\Feedback\Oneclick;
use App\Repositories\Frontend\Product\ProductContract;

class OneclickController extends FeedbackController
{
    protected $products;

    public function __construct(ProductContract $products)
    {
        $this->products = $products;
        $this->feedback = new \App\Services\Feedback\Oneclick();
    }

    public function index()
    {
        $id = request()->route('id');

        $product = $this->products->findById($id);

        $feedbackName = $this->feedback->getName();

        $properties = request()->get('properties');
        $colors = $this->feedback->getColors($properties);

        if(request()->ajax()) {
            return view($this->layout('index'), compact('product', 'colors'));
        }

        return view(mobile_prefix().'.feedback.index', compact('feedbackName', 'product', 'colors'));
    }

    public function send(Oneclick $request, $id)
    {
        $attributes = $request->all();
        $attributes['product_id'] = $id;
        $attributes['properties'] = $request->get('properties', []);

        return $this->sendBase($attributes);
    }
}

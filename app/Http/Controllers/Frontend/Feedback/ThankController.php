<?php

namespace App\Http\Controllers\Frontend\Feedback;

use App\Http\Requests\Frontend\Feedback\Thank;

class ThankController extends FeedbackController
{
    public function __construct()
    {
        $this->feedback = new \App\Services\Feedback\Thank();
    }

    public function send(Thank $request)
    {
        return $this->sendBase($request->all());
    }
}

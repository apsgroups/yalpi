<?php

namespace App\Http\Controllers\Frontend\Feedback;

use App\Http\Requests\Frontend\Feedback\Consult;
use App\Repositories\Backend\Product\ProductContract;
use Illuminate\Support\Facades\Input;

class ConsultController extends FeedbackController
{
    protected $products;

    public function __construct(ProductContract $products)
    {
        $this->products = $products;
        $this->feedback = new \App\Services\Feedback\Consult();
    }

    public function index()
    {
        $id = request()->input('product_id');

        $product = null;

        if($id) {
            $product = $this->products->findById($id);
        }

        $feedbackName = $this->feedback->getName();

        if(request()->ajax()) {
            return view($this->layout('index'), compact('product'));
        }

        return view(mobile_prefix().'.feedback.index', compact('feedbackName', 'product'));
    }

    public function send(Consult $request)
    {
        return $this->sendBase($request->all());
    }
}
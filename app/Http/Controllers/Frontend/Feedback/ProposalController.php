<?php

namespace App\Http\Controllers\Frontend\Feedback;

use App\Http\Requests\Frontend\Feedback\Proposal;

class ProposalController extends FeedbackController
{
    public function __construct()
    {
        $this->feedback = new \App\Services\Feedback\Proposal();
    }

    public function send(Proposal $request)
    {
        return parent::sendBase($request->all());
    }
}
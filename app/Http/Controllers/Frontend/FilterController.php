<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Services\Filter\ProductFilter;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\PropertyValue;
use App\Models\Property;

class FilterController extends Controller
{
    public function options(Request $request)
    {
        $filter = new ProductFilter();

        $category = Category::findOrFail($request->input('category_id'));

        $attributes = $request->all();

        $selectedFilters = [];

        $attributes['product_categories'][] = $category->id;

        if(!empty($attributes['filter'])) {
        	$selectedFilters = $attributes['filter'];

            $attributes = array_merge($attributes, $attributes['filter']);
            unset($attributes['filter']);
        }

        if(!empty($attributes['term'])) {
            $filter->setTerm($attributes['term']);
        }

        $filter->setAttributes($attributes);
        $filter->setPerPage(1000);
        // $filter->setPostFilter(false);
        $filter->apply();

        $options = $filter->getOptions();
        $pagination = $filter->getPagination();

        $result = [];

        $properties = [];

        $primarySlug = $category->primaryProperty ? $category->primaryProperty->slug : null;
        $secondarySlug = $category->secondaryProperty ? $category->secondaryProperty->slug : null;

        foreach($options as $name => $buckets) {
            $labels = in_array($name, ['in_stock', 'novelty', 'sale']);

            $properties[$name]['total'] = 0;

            if(isset($buckets['buckets'])) {
                foreach($buckets['buckets'] as $bucket) {
                    if($labels && $bucket['key_as_string'] === 'false') {
                        continue;
                    }

                    if($labels) {
                        $bucket['key'] = 1;
                    }

                    @$result[$name][$bucket['key']] = (isset($bucket['active']['doc_count'])) ? $bucket['active']['doc_count'] : $bucket['active']['value'];

                    $properties[$name]['total'] += $result[$name][$bucket['key']];
                }

                if($name === 'rate' && !empty($result[$name])) {
                    $properties[$name] = [
                        'options' => [],
                        'title' => 'Рейтинг урока',
                        'position' => 'rate',
                        'tag' => 'checkbox',
                        'total' => 0,
                    ];

                    foreach ($result[$name] as $rate => $total) {
                        $properties[$name]['options'][$rate] = [
                            'id' => $rate,
                            'title' => $rate,
                            'active' => $total,
                            'selected' => isset($selectedFilters[$name]) && in_array($rate, $selectedFilters[$name]),
                        ];

                        $properties[$name]['total'] += $total;
                    }

                    ksort($properties[$name]['options']);

                    continue;
                }

                if(isset($result[$name])) {
                    $properties[$name]['options'] = PropertyValue::select('id', 'title')->whereIn('id', array_keys($result[$name]))->orderBy('ordering', 'asc')->get()->toArray();

                    $property = Property::where('slug', $name)->first();

                    $properties[$name]['title'] = $property ? $property->title : '';
                    $properties[$name]['position'] = !empty($property->position) ? $property->position : 'ext';
                    $properties[$name]['tag'] = !empty($property->display_tag) ? $property->display_tag : 'select';
                    $properties[$name]['ordering'] = !empty($property->ordering) ? $property->ordering : 1;

                    foreach ($properties[$name]['options'] as $key => $value) {
                    	$active = $result[$name][$value['id']];
                    	$active = $name === $primarySlug ? 1 : $active;

                        $properties[$name]['options'][$key]['active'] = $active;
                    	$properties[$name]['options'][$key]['selected'] = isset($selectedFilters[$name]) && in_array($value['id'], $selectedFilters[$name]);
                    }
                }
            }

            if(isset($buckets['min_value']['value'])) {
                $result[$name]['min'] = $buckets['min_value']['value'];
            }

            if(isset($buckets['max_value']['value'])) {
                $result[$name]['max'] = $buckets['max_value']['value'];
            }

            if($labels && empty($result[$name])) {
                $result[$name][1] = 0;
            }
        }

        if(!is_mobile() && !$category->parent_id) {
            if(empty($selectedFilters[$primarySlug])) {
                unset($properties[$secondarySlug]);
            }

            if(empty($selectedFilters[$secondarySlug])) {
                foreach ($properties as $n => $p) {
                    if(isset($p['position']) && $p['position'] === 'ext') {
                        unset($properties[$n]);
                    }
                }
            }
        }

        if($category->parent_id && isset($properties[$primarySlug])) {
            unset($properties[$primarySlug]);
        }

        foreach (['product_type_id', 'product_categories'] as $n) {
            if(isset($properties[$n])) {
                unset($properties[$n]);
            }
        }

        uasort($properties, function($a, $b) {
            if(!isset($a['ordering']) || !isset($b['ordering'])) {
                return 0;
            }

            return ($a['ordering'] > $b['ordering']) ? 1 : -1;

        });

        $response['options'] = $properties;

        $total = $pagination->total();

        $response['total_text'] = $total > 0 ? 'Показать '.$total.' '. total_results_morph($total) : 'Нет результатов';

        return $response;
    }
}
<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;

class LandingController extends Controller
{
    public function index()
    {
        $id = 11;
        $take = 9;

        $category = Category::find($id);

        $products = $category->products()->published(1)->orderBy('pivot_ordering', 'asc')->get();

        $productSets = [];

        foreach($products as $product) {
            $set = [];

            $setId = $product->preview_title;//$product->product_set_id ? $product->product_set_id : 'p'.$product->id;

            $color = [];
            $color['active'] = false;

            if(!isset($productSets[$setId])) {
                $set['id'] = $setId;

                $set['img'] = img_src($product->image, 'landing');
                $set['title'] = $product->preview_title;
                $set['dimensions'] = $product->dimensions_sm;
                $set['price'] = number_format($product->price, 0, '', '');
                $set['id'] = $product->product_set_id;
                $set['slug'] = $product->slug;
                $set['products'] = [];
                
                $color['active'] = true;
            } else {
                $set = $productSets[$setId];

                if(count($set['products']) > 12) {
                    continue;
                }
            }

            $color['img'] = img_src($product->image, 'landing');
            $color['id'] = $product->id;

            try {
                $c = $product->properties()->where('property_id', 8)->firstOrFail()->propertyValue;
                $color['icon'] = img_src($c->image, 'landing');
                $color['color'] = $c->title;
            } catch(\Exception $e) {
                $color['icon'] = '';
                $color['color'] = '';
            }

            $set['products'][] = $color;

            $productSets[$setId] = $set;
        }

        return view('frontend.landing.index', compact('productSets'));
    }
}
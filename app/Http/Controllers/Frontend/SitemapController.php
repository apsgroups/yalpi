<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;

use App\Http\Requests;
use App\Models\Category;
use App\Models\ContentType;

class SitemapController extends Controller
{
    protected $changefreq = 'weekly';
    protected $cacheTime = 1140;

    public function html()
    {
        return view('frontend.sitemap.html');
    }

    public function index()
    {
        $sitemap = app()->make('sitemap');

        $secure = (bool)(!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') || $_SERVER['SERVER_PORT'] == 443;

        $sitemap->setCache('laravel.sitemap.'.$secure, $this->cacheTime);

        if($sitemap->isCached()) {
            return $sitemap->render('sitemapindex');
        }

        $sitemap->addSitemap($this->sitemapUrl('pages'));

        $categories = Category::published(1)->where(function ($query) {
            $query->sitemap(1)->orWhere('sitemap_items', 1);
        })->where('parent_id', null)->get();

        foreach($categories as $category) {
            $sitemap->addSitemap($this->sitemapUrl('category/'.$category->slug));
        }

        $contentTypes = ContentType::published(1)->where(function ($query) {
            $query->sitemap(1)->orWhere('sitemap_items', 1);
        })->get();

        foreach($contentTypes as $contentType) {
            $sitemap->addSitemap($this->sitemapUrl('content/'.$contentType->id));
        }

        return $sitemap->render('sitemapindex');
    }

    protected function sitemapUrl($name)
    {
        return rtrim(url('sitemaps/'.$name.'.xml'), '/');
    }

    public function pages()
    {
        $secure = (bool)(!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') || $_SERVER['SERVER_PORT'] == 443;

        $sitemap = app()->make('sitemap');
        $sitemap->setCache('laravel.sitemap.pages.'.$secure, $this->cacheTime);

        if($sitemap->isCached()) {
            return $sitemap->render('xml');
        }

        $link = url('/');
        $sitemap->add($link);

        foreach(config('sitemap.routes') as $route) {
            $link = route($route);
            $sitemap->add($link);
        }

        return $sitemap->render('xml');
    }

    public function content($id)
    {
        $secure = (bool)(!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') || $_SERVER['SERVER_PORT'] == 443;

        $sitemap = app()->make('sitemap');
        $sitemap->setCache('laravel.sitemap.content.'.$secure.'.'.$id, $this->cacheTime);

        if($sitemap->isCached()) {
            return $sitemap->render('xml');
        }

        $contentType = ContentType::published(1)->findOrFail($id);

        if($contentType->sitemap) { 
            $link = $contentType->link(true);
            $sitemap->add($link);
        }
 
        if($contentType->sitemap_items) {
            $contents = $contentType->contents()->published(1)->sitemap(1)->get();

            foreach($contents as $content) {
                $link = $content->link(true);
                $sitemap->add($link);
            }
        }

        return $sitemap->render('xml');
    }

    public function category($slug)
    {
        $secure = (bool)(!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') || $_SERVER['SERVER_PORT'] == 443;

        $sitemap = app()->make('sitemap');
        $sitemap->setCache('laravel.sitemap.category.'.$secure.'.'.$slug, $this->cacheTime);

        if($sitemap->isCached()) {
            return $sitemap->render('xml');
        }

        $category = Category::published(1)->slug($slug)->firstOrFail();

        if($category->sitemap) { 
            $link = $category->link(true);
            $sitemap->add($link);
        }

        $children = $this->getCategories($category->id);

        foreach ($children as $child) {
            if($child->sitemap) {
                $link = $child->link(true);
                $sitemap->add($link);
            }

            if($child->sitemap_items) {
                $child->primaryProducts()->published(1)->sitemap(1)->chunk(200, function ($products) use(&$sitemap) {
                    foreach($products as $product) {
                        $link = $product->link(true);
                        $sitemap->add($link);
                    }
                });
            }
        }

        if($category->sitemap_items) {
            $category->primaryProducts()->published(1)->sitemap(1)->chunk(200, function ($products) use(&$sitemap) {
                foreach($products as $product) {
                    $link = $product->link(true);
                    $sitemap->add($link);
                }
            });
        }

        return $sitemap->render('xml');
    }

    protected function getCategories($parent) 
    { 
    	$categories = Category::published(1)->where('parent_id', $parent)->get();

    	$list = [];
    	foreach ($categories as $category) {
    		$list[] = $category;

    		if($category->children) {
    			$list = array_merge($list, $this->getCategories($category->id));
    		} 
    	}

    	return $list;
    }

    protected function getPriority($url)
    {
        $base = 1;

        $url = str_replace(url('/'), '', $url);

        if(!$url) {
            return floatval($base);
        }

        $url = rtrim($url, '/');
        $segments = explode('/', $url);

        $priority = $base - (0.2 * count($segments)); 

        return max(floatval($priority), 0.4);
    }
}

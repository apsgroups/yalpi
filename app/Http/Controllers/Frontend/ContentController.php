<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Content;
use App\Models\ContentType;
use App\Http\Controllers\Controller;
use App\Services\Frontend\SortingService;
use Illuminate\Http\Request;
use Conner\Tagging\Model\Tag;

class ContentController extends Controller
{
    public function index(Request $request)
    {
        $id = $request->input('id');

        $contentType = \Cache::remember('contenttype.'.$id, config('cache.time'), function() use ($id) {
            return ContentType::findOrFail($id);
        });

        $perPage = $contentType->per_page ? $contentType->per_page : config('site.perPage.default.content');

        if(is_mobile()) {
            $perPage = 4;
        }

        $wholesale = access()->allow('view-wholesale');
        $page = $request->get('page', 1);

        $key = 'contents.list.'.$id.'.page.'.$page.'.perpage.'.$perPage.'.wholesaler.'.$wholesale;

        $contents = \Cache::remember($key, config('cache.time'), function() use($contentType, $wholesale, $perPage) {
            $contents = $contentType->contents()->published(1);
            return $contents->orderBy('created_at', 'desc')->paginate($perPage);
        });

        $template = mobile_prefix().'.content.'.$contentType->slug;
        $view = view()->exists($template.'.index') ? $template : mobile_prefix().'.content';

        $perPageUrls = [];
        $inRow = 5;

        $products = $this->getContentProducts($contents);

        foreach(config('site.perPage.limits.'.$inRow) as $key => $value) {
            $perPageUrls[$key] = content_type_url($contentType, $value);
        }

        $pagination = $contents;

        $tags = Content::existingTags()->take(3);

        $assign = compact(
            'contentType',
            'contents',
            'pagination',
            'perPage',
            'products',
            'perPageUrls',
            'inRow',
            'tags'
        );

        $view = $request->ajax() ? $view.'.partials.index' : $view.'.index';

        if($request->ajax() && $request->loadmore) {
            return [
                'products' => view()->make($template.'.partials.items', $assign)->render(),
                'pagination' => view()->make($template.'.pagination.index', $assign)->render(),
                'loadmore' => view()->make($template.'.pagination.loadmore', $assign)->render(),
                'title' => $contentType->title.($contents->currentPage() > 1 ? ' | Страница '.$contents->currentPage() : ''),
                'url' => content_type_url($contentType),
            ];
        } elseif($request->ajax()) {
            return [
                'products' => view()->make($view, $assign)->render(),
                'url' => $page > 1 ? $contents->url($page) : content_type_url($contentType),
                'title' => $contentType->title.($contents->currentPage() > 1 ? ' | Страница '.$contents->currentPage() : ''),
                'heading' => $contentType->title.($contents->currentPage() > 1 ? ' | Страница '.$contents->currentPage() : ''),
                'filterUrl' => ''
            ];
        }

        return view($view, $assign);
    }

    protected function getContentProducts($contents) {
        $products = [];

        foreach($contents as $content) {
            $key = 'content.list.'.$content->id.'.products';
            $products[$content->id] = \Cache::remember($key, config('cache.time'), function () use ($content) {
                return $content->products()->with('image', 'category')->published(1)->take(3)->get();
            });
        }

        return $products;
    }

    public function show(Request $request)
    {
        $id = $request->get('id');

        $content = Content::published()->findOrFail($id);

        $content->increment('hits');

        $prev = Content::published()->ofType($content->type->slug)->where('id', '<', $id)->orderBy('id', 'desc')->first();
        $next = Content::published()->ofType($content->type->slug)->where('id', '>', $id)->orderBy('id', 'asc')->first(); 
 
        $products = $content->products()->published()->get();

        $relatedContent = $content->relatedContent()->published()->get();

        if(!$relatedContent->count()) {
            $relatedContent = Content::withAnyTag($content->tagged->pluck('tag_name')->toArray())
                    ->orderBy('created_at', 'desc')
                    ->with('image')
                    ->published(1)
                    ->where('id', '<>', $content->id)
                    ->take(2)
                    ->get();
        }

        $template = mobile_prefix().'.content.'.$content->type->slug.'.show';

        if(view()->exists($template.'.'.$content->slug)) {
            $template = $template.'.'.$content->slug;
        } else {
            $template = view()->exists($template) ? $template : mobile_prefix().'.content.show';
        }

        $comments = $content->comments()->where('parent_id', null)->published(1)->orderBy('popularity', 'desc')->orderBy('id', 'desc')->paginate(10);

        $comments->setPath(route('frontend.comments.index', ['content', $content->id]));

        $commentsCount = $content->comments()->published(1)->count();

        $userLikes = [];

        $user = auth()->user();

        if($user && $user->id && $comments->count()) {
            $userLikes = \App\Models\CommentLike::ofUser($user->id)->whereIn('id', $comments->pluck('id'))->get()->keyBy('comment_id')->toArray();
        }

        return view($template, compact(
            'content',
            'products',
            'relatedContent',
            'prev',
            'next',
            'userLikes',
            'comments',
            'commentsCount'
        ));
    }

    public function byTag($tag)
    {
        $tag = Tag::where('slug', $tag)->firstOrFail();

        $page = request()->get('page', 1);

        $limit = 10;

        $contents = Content::published(1)
                ->orderBy('created_at', 'desc')
                ->with('image')
                ->withAnyTag([$tag->slug])
                ->paginate($limit);

        return view('frontend.content.byTag', compact('contents', 'tag'));
    }
}

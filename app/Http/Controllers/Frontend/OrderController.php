<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Order;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{
    public function xml($hash)
    {
        $order = Order::where('hash', $hash)->firstOrFail();

        $products = [];

        $orderProducts = $order->products()->whereNull('parent_id')->with('modules.product')->get();

        foreach($orderProducts as $orderProduct) {
            if($orderProduct->modules->count()) {
                foreach($orderProduct->modules as $module) {
                    $products[] = [
                        'ext_id' => $module->product->ext_id,
                        'discount' => ($module->product->discount_from <= 1) ? $module->product->discount : 0,
                        'price' => $module->product->price_base,
                        'count' => $module->amount,
                    ];
                }

                continue;
            }

            $product = [];

            $product['count'] = $orderProduct->amount;

            if($orderProduct->productAttribute) {
                $product['ext_id'] = $orderProduct->productAttribute->ext_id;
                $product['price'] = $orderProduct->productAttribute->getPriceBase();

                $discount = $orderProduct->productAttribute->discount;
                $discountFrom = $orderProduct->productAttribute->discount_from;

                $product['discount'] = ($discountFrom <= 1) ? $discount : 0;
            } else {
                $product['ext_id'] = $orderProduct->product->ext_id;
                $product['price'] = $orderProduct->product->price_base;

                $discount = $orderProduct->product->discount;
                $discountFrom = $orderProduct->product->discount_from;

                $product['discount'] = ($discountFrom <= 1) ? $discount : 0;
            }

            $products[] = $product;
        }


        $content = view('frontend.order.xml', compact('order', 'products'));

        return response($content, 200)
            ->header('Content-Type', 'text/xml; charset=utf-8');
    }
}

<?php

namespace App\Http\Controllers\Frontend;

use App\Facades\Cart;
use App\Models\Order;
use App\Repositories\Frontend\Product\ProductContract;
use App\Services\OrderService;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class CartController extends Controller
{
    protected $products;

    public function __construct(ProductContract $products)
    {
        $this->products = $products;
    }

    public function index()
    {
        $cart = new Cart;

        $products = $cart::getProducts();

        return view(mobile_prefix().'.cart.index', compact('cart', 'products'));
    }

    public function add(Request $request) {
        $id = $request->input('product_id');
        $amount = $request->input('amount', 1);
        $properties = $request->input('properties', []);
        $attribute = $request->input('attribute');

        $modules = $request->session()->get('modules.'.$id);
        $desk = $request->input('desk');

        $key = Cart::add($id, $attribute, $properties, $amount, $modules, $desk);

        $product = Cart::getProductByKey($key);

        $params['content'] = view()
            ->make(mobile_prefix().'.cart.partials.added', compact('product'))
            ->render();

        return $this->ajaxOrRedirect($request, 'Товар добавлен в корзину!', $params);
    }

    public function remove(Request $request) {
        Cart::remove($request->input('id'));

        return $this->ajaxOrRedirect($request, 'Товар удален!');
    }

    public function recalc(Request $request) {
        $products = $request->input('products');

        foreach($products as $id => $amount) {
            Cart::recalc($id, $amount);
        };

        return $this->ajaxOrRedirect($request, 'Корзина обновлена!');
    }

    public function clear()
    {
        return Cart::clear();
    }

    public function checkout(Requests\Frontend\Cart\Checkout $request)
    {
        try {
            $orderService = new OrderService();

            $orderService->setPayment($request->input('payment_id'));
            $orderService->setShipping($request->input('shipping_id'));
            $orderService->setMounting($request->input('mounting'));

            $products = Cart::getProducts();

            foreach($products as $key => $item) {
                $propertyIds = Cart::getPropertiesId($item['properties']);

                $attributeId = !empty($item['attribute']['id']) ? $item['attribute']['id'] : null;

                $orderService->setProduct($item['model'], $attributeId, $propertyIds, $item['amount'], $item['modules'], $item['desk']);
            }

            $orderService->setName($request->input('name'));
            $orderService->setPhone($request->input('phone'));
            $orderService->setEmail($request->input('email'));
            $orderService->setCity($request->input('city'));
            $orderService->setStreet($request->input('street'));
            $orderService->setHome($request->input('home'));
            $orderService->setApartment($request->input('apartment'));

            $orderService->setComment($request->input('comment'));

            $orderService->setClientId($request->input('client_id'));

            $orderService->setCoupon(Cart::getCoupon(), Cart::couponDiscount());

            $order = $orderService->save();
        } catch(\Exception $e) {
            \Log::alert($e);

            return redirect()->back()->withInput()->withFlashDanger($e->getMessage());
        }

        Cart::clear();

        return redirect()->route('cart.thankyou')->with('checkout.order_id', $order->id);
    }

    public function thankYou()
    {
        $id = session('checkout.order_id');

        if(empty($id)) {
            return redirect()->route('cart.index');
        }

        $order = Order::find($id);

        return view(mobile_prefix().'.cart.thankyou', compact('order'));
    }

    protected function ajaxOrRedirect(Request $request, $message, $params = [])
    {
        $data = [
            'amount' => Cart::amount(),
            'word' => total_products_morph(Cart::amount()),
            'cost' => format_price(Cart::cost()),
            'products' => Cart::responseProducts(),
        ];

        $data = array_merge($data, $params);

        if($request->ajax()) {
            return response()->json($data);
        }

        return redirect()->back()->withFlashSuccess($message);
    }

    public function coupon(Requests\Frontend\Cart\Coupon $request)
    {
        if(Cart::setCoupon($request->coupon)) {
            if($request->ajax()) {
                return response()->json(['cost' => format_price(Cart::cost()), 'discount' => format_price(Cart::couponDiscount())]);
            }

            return redirect()->back()->withFlashSuccess('Купон '.$request->coupon.' применён!');
        }

        if($request->ajax()) {
            return response()->json(['error' => 'Купон не найден']);
        }

        return redirect()->back()->withFlashDanger('Купон не найден!');
    }
}
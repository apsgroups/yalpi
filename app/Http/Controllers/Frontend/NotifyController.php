<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Notify;

/**
 * Class ProfileController
 * @package App\Http\Controllers\Frontend
 */
class NotifyController extends Controller
{
    protected $user;

    public function __construct()
    {
        $this->user = auth()->user();
    }

    public function index()
    {
        $notifications = $this->user->notifies()->where('active', 1)->with('notifable')->orderBy('id', 'desc')->paginate(10);

        return view('frontend.notify.index', compact('notifications'))
            ->withUser($this->user);
    }

    public function remove($id)
    {
        $notify = Notify::findOrFail($id);

        if($notify->user_id !== $this->user->id) {
            abort(404);
        }

        $notify->active = 0;
        $notify->save();
    }
}
<?php

namespace App\Http\Controllers\Frontend;

use App\Models\MenuItem;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MenuController extends Controller
{
    public function children($id)
    {
        $parent = MenuItem::findOrFail($id);

        $children = $parent->children()->published(1)->orderBy('ordering', 'asc')->get();

        $targets = config('menu.target');

        $items = [];

        foreach ($children as $child) {
            $item = $child->toArray();

            $item['active'] = 0;
            $item['disabled'] = 0;
            $item['min_price'] = 0;
            $item['marker'] = 0;
            $item['children'] = $child->children()->published(1)->first();

            $item['link'] = $child->link();

            if(!$item['link']) {
                $item['link'] = '#';
            }

            $item['target'] = $child->target && !empty($targets[$child->target]) ? $targets[$child->target] : '';

            $rel = [];

            if($child->noindex) {
                $rel[] = 'nofollow';            
            }

            if($child->target) {
                $rel[] = 'noopener';            
            }

            $item['rel'] = implode(' ', $rel);

            $original = $child;

            if($item['type'] === 'menu_item') {
                $original = $this->findOriginal($item['item_id']);
            }

            if($original->type === 'category') {
                $category = \App\Models\Category::find($original->item_id);
                
                if($category) {
                    $item['category'] = $category->toArray();

                    $item['category_product_count'] = $category->products()->published(1)->count();
                }
            }
            
            $items[] = $item;
        }

        return view('widgets.menu.dropdown.children', ['items' => $items, 'parent' => $parent]);
    }

    protected function findOriginal($id)
    {
        $menuItem = MenuItem::find($id);

        if(!$menuItem) {
            return;
        }

        if($menuItem->type === 'menu_item') {
            return $this->findOriginal($menuItem->item_id);
        }

        return $menuItem;
    }
}
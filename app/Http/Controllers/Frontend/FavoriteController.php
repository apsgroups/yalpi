<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Facades\Favorite as FavoriteFacade;

class FavoriteController extends Controller
{
    public function add($id)
    {
        FavoriteFacade::add($id);

        return response()->json([
            'total' => FavoriteFacade::count(),
            'active' => true,
        ]);
    }

    public function remove($id)
    {
        FavoriteFacade::remove($id);

        return response()->json([
            'total' => FavoriteFacade::count(),
            'active' => false,
        ]);
    }

    public function clear()
    {
        FavoriteFacade::clear();

        return redirect()->route('favorite.index')->withFlashSuccess('Избранные товары удалены!');
    }
}

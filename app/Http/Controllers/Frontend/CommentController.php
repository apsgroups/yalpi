<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Frontend\Comment\Store;
use App\Http\Requests\Frontend\Comment\ReplyStore;
use App\Models\Product;
use App\Models\Content;
use App\Models\Comment;
use App\Models\CommentLike;

class CommentController extends Controller
{
    public function index($scope, $id)
    {
        $model = $this->getCommentItem($scope);

        if(!$model) {
            abort(404);
            return;
        }

        $item = $model::findOrFail($id);

        $rate = request('rate', null);
 
        $comments = $item->comments()->where('parent_id', null)->published(1);

        if($rate > 0) {
            $comments = $comments->where('rate', $rate);
        }

        $comments = $comments->orderBy('popularity', 'desc')->orderBy('id', 'desc')->paginate(10);

        $userLikes = [];

        $user = auth()->user();

        if($user && $user->id && $comments->count()) {
            $userLikes = \App\Models\CommentLike::ofUser($user->id)->whereIn('id', $comments->pluck('id'))->get()->keyBy('comment_id')->toArray();
        }

        return view('frontend.comments.items', compact('comments', 'item', 'userLikes'));
    }

    protected function getCommentItem($scope)
    {
        $map = ['product' => Product::class, 'content' => Content::class];

        if(!isset($map[$scope])) {
            return false;
        }

        return $map[$scope];
    }

    public function like($commentId)
    {
        $comment = Comment::findOrFail($commentId);

        $user = auth()->user();

        $data = ['user_id' => $user->id, 'comment_id' => $commentId];

        $like = CommentLike::where($data)->first();

        $action = request('action') === 'true' ? 1 : 0;

        $isNew = !$like;

        if($isNew) {
            $data['like'] = $action;
            $like = CommentLike::create($data);
        }

        $slug = 'none';

        if($like->like === $action && !$isNew) {
            $action ? $comment->likes-- : $comment->dislikes--;
            $like->delete();
        } else {
            if(!$isNew) {
                $action ? $comment->dislikes-- : $comment->likes--;
            }

            $action ? $comment->likes++ : $comment->dislikes++;
            $like->like = $action;
            $like->save();

            $slug = $action ? 'like' : 'dislike';
        }

        $comment->popularity = $comment->likes - $comment->dislikes;

        $comment->save();

        return [
            'action' => $slug,
            'likes' => (int)$comment->likes,
            'dislikes' => (int)$comment->dislikes
        ];
    }

    public function create($scope, $id)
    {
        $model = $this->getCommentItem($scope);

        if(!$model) {
            abort(404);
            return;
        }

        $item = $model::findOrFail($id);

        return view('frontend.comments.create', compact('model', 'item'));
    }

    public function reply($commentId)
    {
        $comment = Comment::findOrFail($commentId);
        
        return view('frontend.comments.reply', compact('comment'));
    }

    public function store(Store $request, $scope, $id)
    {
        $model = $this->getCommentItem($scope);

        if(!$model) {
            abort(404);
            return;
        }

        $item = $model::findOrFail($id);

        $comment = new Comment();
        $comment->comment = $request->comment;
        $comment->user_id = auth()->user()->id;
        $comment->name = auth()->user()->name;
        $comment->rate = request('rate', 0);
        $comment->published = !config('site.comments.pre_moderation');

        $comment = $item->comments()->save($comment);
       
        $this->sendToManager($comment);

        return response()->json([
            'status' => 'ok',
            'msg' => view('frontend..comments.thankyou', compact('comment'))->render()
        ]);
    }

    public function replyStore(ReplyStore $request, $commentId)
    {
        $parent = Comment::findOrFail($commentId);

        $comment = new Comment();
        $comment->parent_id = $parent->id;
        $comment->commentable_id = $parent->commentable_id;
        $comment->commentable_type = $parent->commentable_type;
        $comment->comment = $request->comment;
        $comment->user_id = auth()->user()->id;
        $comment->name = auth()->user()->name;
        $comment->published = !config('site.comments.pre_moderation');
        $comment->save();

        $this->sendToManager($comment);

        return response()->json([
            'status' => 'ok',
            'msg' => view('frontend.comments.reply-thankyou', compact('comment'))->render()
        ]);
    }

    protected function sendToManager($comment) 
    {
        $emails = \Settings::get('email_product_reviews', config('mail.from.address'));
        $emails = explode(',', $emails);

        foreach($emails as $email) {
            if(empty($email)) {
                continue;
            }

	        \Mail::send('emails.comments.index', compact('comment'), function ($message) use($email, $comment) {
        		$from = \Settings::get('email', config('mail.from.address'));
	            $name = \Settings::get('title', config('mail.from.name'));

	            $message->from($from, $name);

	            $message->to(trim($email))
	                ->subject('Добавлен комментарий');
	        });
        }
    }
}

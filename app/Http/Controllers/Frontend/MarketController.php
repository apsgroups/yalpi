<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;

class MarketController extends Controller
{
    public function index()
    {
        $market = \Cache::remember('market.index', config('cache.time'), function() {
            $categories = Category::published(1)->where('market', 1)->get();

            $products = Product::with('category', 'market_category', 'image', 'type', 'properties', 'properties.property')
            	->where('price_base', '>', 0)
                ->market(1)
                ->whereIn('category_id', $categories->pluck('id'))
                ->published();

                $layout = '';

            return view()->make('frontend.market.index', compact('products', 'categories', 'layout'))->render();
        });

        return response($market, 200)
            ->header('Content-Type', 'application/xml; charset=utf-8');
    }

    public function all()
    {
        ini_set('max_execution_time', 600);

        $market = \Cache::remember('market.index.all', config('cache.time'), function() {
            $categories = Category::published(1)->get();

            $products = Product::with('category', 'market_category', 'image', 'properties', 'properties.property')
                ->where('price_base', '>', 0)
                ->where(function ($query) {
                    $query->where('discount', 0)
                      ->orWhereBetween('discount', [5, 95]);
                })
                ->whereIn('category_id', $categories->pluck('id'))
                ->published();

            $layout = '_all';

            return view()->make('frontend.market.index', compact('products', 'categories', 'layout'))->render();
        });

        return response($market, 200)
            ->header('Content-Type', 'application/xml; charset=utf-8');
    }

    public function turbo()
    {
        ini_set('max_execution_time', 600);

        $market = \Cache::remember('market.turbo', config('cache.time'), function() {
            $categories = Category::whereIn('id', [27])->get();

            $products = Product::with('category', 'market_category', 'image', 'properties', 'properties.property', 'categories')
                ->where('price_base', '>', 0)
                ->join('category_product', 'products.id', '=', 'category_product.product_id')
                ->whereIn('category_product.category_id', $categories->pluck('id'))
                ->published();

            $layout = '_turbo';

            return view()->make('frontend.market.index', compact('products', 'categories', 'layout'))->render();
        });

        return response($market, 200)
            ->header('Content-Type', 'application/xml; charset=utf-8');
    }
}
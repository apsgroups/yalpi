<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Frontend\Subscribe\AddRequest;
use App\Models\Order;
use GuzzleHttp\Client;

class SubscribeController extends Controller {

    public function add(AddRequest $request)
    {
        $response = $this->subscribe($request->email);

        $thank = !empty($request->shortbox) ? 'frontend.includes.partials.subscribe_thank' : 'frontend.subscribe._thank';

        if(!empty($response['error'])) {
            \Log::info('Subscribe error: '.$request->email.' '.$response['error']);
        }

        if($request->ajax()) {
            return [
                'msg' => view()->make($thank)->render(),
            ];
        }

        return view('frontend.subscribe.index');
    }

    public function coupon(AddRequest $request)
    {
        $response = $this->subscribe($request->email);

        if(Order::where('email', $request->email)->exists()) {
            return [
                'errors' => ['По данному электронному адресу уже производилась покупка'],
            ];
        }

        if(!empty($response['error'])) {
            \Log::info('Subscribe error: '.$request->email.' '.$response['error']);

            $error = 'Произошла ошибка.';

            if($request->ajax()) {
                return [
                    'errors' => [$error],
                ];
            }

            return back()
                ->withInput()
                ->withFlashDanger($error);
        }

        return ['coupon' => 'YALPI'];
    }

    protected function subscribe($email)
    {
        $client = new Client();

        $response = $client->request('POST', 'http://api.unisender.com/ru/api/subscribe?format=json', [
            'form_params' => [
                'api_key' => \Settings::get('subscribe_api_key'),
                'list_ids' => \Settings::get('subscribe_list_ids'),
                'double_optin' => \Settings::get('subscribe_double_optin', 0),
                'fields' => [
                    'email' => $email,
                ],
            ]
        ]);

        return json_decode($response->getBody(), true);
    }
}
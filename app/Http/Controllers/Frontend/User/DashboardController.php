<?php

namespace App\Http\Controllers\Frontend\User;

use App\Http\Requests;
use App\Http\Controllers\Controller;

/**
 * Class DashboardController
 * @package App\Http\Controllers\Frontend
 */
class DashboardController extends Controller
{
    protected $user;

    public function __construct()
    {
        $this->user = auth()->user();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $perPage = 12;

    	$favorites = $this->user->favoriteProducts()->orderBy('id', 'desc')->paginate($perPage);

    	return view(mobile_prefix().'.user.dashboard', compact('favorites', 'perPage'))
            ->withUser(access()->user());
    }
}

<?php

namespace App\Http\Controllers\Frontend\User;

use App\Http\Controllers\Controller;
use App\Repositories\Frontend\User\UserContract;
use App\Http\Requests\Frontend\User\UpdateProfileRequest;
use App\Http\Requests\Frontend\User\AvatarUpload;

/**
 * Class ProfileController
 * @package App\Http\Controllers\Frontend
 */
class ProfileController extends Controller
{
    protected $user;

    public function __construct()
    {
        $this->user = auth()->user();
    }

    /**
     * @return mixed
     */
    public function edit()
    {
        return view(mobile_prefix().'.user.profile.edit')
            ->withUser(access()->user());
    }

    /**
     * @param  UserContract         $user
     * @param  UpdateProfileRequest $request
     * @return mixed
     */
    public function update(UserContract $user, UpdateProfileRequest $request)
    {
        $user->updateProfile(access()->id(), $request->all());
        return redirect()->route('frontend.user.dashboard')->withFlashSuccess(trans('strings.frontend.user.profile_updated'));
    }

    public function avatar(UserContract $user, AvatarUpload $request)
    {
        $image = $user->updateAvatar(access()->id(), $request->all());
        
        return ['src' => img_src($image, 'avatar')];
    }

    public function comments()
    {
        $comments = $this->user->comments()->published(1)->with('commentable')->orderBy('id', 'desc')->paginate(12);

        return view(mobile_prefix().'.user.comments', compact('comments'))
            ->withUser($this->user);
    }

    public function tasks()
    {
        return view(mobile_prefix().'.user.tasks')
            ->withUser($this->user);
    }

    public function skills()
    {
        return view(mobile_prefix().'.user.skills')
            ->withUser($this->user);
    }
}
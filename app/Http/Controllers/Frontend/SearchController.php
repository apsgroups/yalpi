<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Category;
use App\Models\Product;
use App\Models\ProductType;
use App\Repositories\Frontend\Category\CategoryContract;
use App\Repositories\Frontend\Product\ProductContract;

use App\Http\Controllers\Controller;
use App\Services\Filter\ProductFilter;
use App\Services\Frontend\LayoutService;
use App\Services\Frontend\SortingService;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    protected $products;

    protected $categories;

    public function __construct(ProductContract $products, CategoryContract $categories)
    {
        $this->products = $products;
        $this->categories = $categories;
    }

    public function index(Request $request, $sortAttribute = '')
    {
        $inRow = \Settings::get('in_search_row');

        $term = $request->input('term');

        $page = $request->input('page');
        $perPageKey = $inRow;
        $perPage = $request->input('per_page', config('site.perPage.default.'.$inRow));

        $layoutService = new LayoutService();
        $layout = $request->input($layoutService->getKeyName());
        $layoutService->setLayout($layout);

        $limits = config('site.perPage.limits.'.$inRow);

        if(!isset($limits[$perPage])) {
            $perPage = config('site.perPage.default.'.$inRow);
        }

        if(is_mobile()) {
            $inRow = config('mobile.search.in_row');
            $perPage = config('mobile.search.per_page');
        }

        $sortingService = new SortingService();

        //$sortAttribute = $request->input($sortingService->getKeyAttribute());
        $sortDirection = $request->input($sortingService->getKeyDirection());

        $sortingService->setAttribute($sortAttribute);
        $sortingService->setDirection($sortDirection);

        $attributes = $request->all();

        if(!empty($attributes['product_categories'])) {
            $attributes['product_categories'] = collect($attributes['product_categories'])->filter(function ($value, $key) {
                return $value > 0;
            })->toArray();

            if(empty($attributes['product_categories'])) {
                unset($attributes['product_categories']);
            }
        }

        $filterAttr = $attributes;

        if(!empty($attributes['filter'])) {
            $filterAttr = array_merge($attributes, $attributes['filter']);
            unset($filterAttr['filter']);
        }

        $products = [];

        if(!empty($term)) {
            $filter = new ProductFilter();
            $filter->setAttributes($filterAttr);
            $filter->setSortBy($sortingService->convertAttributeToField());
            $filter->setSortDir($sortingService->getDirection());

            $filter->setPage($perPage, $page);
            $filter->setTerm($term);
            $filter->apply();

            $options = $filter->getOptions();
            $pagination = $filter->getPagination();
            $pagination->appends(\Request::except('id', 'category_id', 'menu_item_id', 'loadmore'));

            $ids = $filter->getIds();


            $products = collect([]);

            if($ids) {
                $productList = \App\Models\Product::whereIn('id', $ids)
                    ->published(1)
                    ->with('image', 'images', 'type', 'category')
                    ->orderByRaw(\DB::raw('FIELD(id, '.implode(',', $ids).')'))
                    ->get();

                $products = [];

                foreach ($productList as $p) {
                    $products[] = $p->getPreviewData();
                }

                $products = collect($products);
            }
        }

        $currentSortBy = $sortingService->getAttribute() == $sortingService->getDefaultAttribute() ? null : $sortingService->getAttribute();
        $currentPerPage = config('site.perPage.default.'.$inRow) == $perPage ? null : $perPage;

        $viewPref = mobile_prefix();

        $view = $viewPref.'.search.index';

        if($request->ajax()) {
            $view = $viewPref.'.category.partials.products';

            if(empty($products)) {
                $view = $viewPref.'.search.partials.empty_results';
            }
        } else {
            $categories = $this->categories->getCategoriesForSearch();
        }

        // needs leading sort_by in array for search route
        $origAttr = ['sort_by' => ''];

        if(!empty($attributes['sort_by'])) {
            $origAttr = array_merge($origAttr, $attributes);
        }

        $originalUrl = route('search.index', $origAttr);

        $sortUrls = [];
        foreach($sortingService->getNames() as $sortName) {
            $urlAttributes = [];

            $urlAttributes['sort_by'] = '';

            if($sortName != $sortingService->getDefaultAttribute()) {
                $urlAttributes['sort_by'] = $sortName;
            }

            if(isset($attributes['term'])) {
                $urlAttributes['term'] = $attributes['term'];
            }

            if(isset($attributes['filter'])) {
                $urlAttributes['filter'] = $attributes['filter'];
            }

            if(isset($attributes['product_categories'])) {
                $urlAttributes['product_categories'] = $attributes['product_categories'];
            }

            $urlAttributes['per_page'] = $currentPerPage;

            $sortUrls[$sortName] = route('search.index', $urlAttributes);
        }

        $perPageUrls = [];

        foreach(config('site.perPage.limits.'.$inRow, []) as $key => $value) {
            $urlAttributes = [];

            $urlAttributes['sort_by'] = $currentSortBy;

            if(isset($attributes['term'])) {
                $urlAttributes['term'] = $attributes['term'];
            }

            if(isset($attributes['filter'])) {
                $urlAttributes['filter'] = $attributes['filter'];
            }

            if(isset($attributes['product_categories'])) {
                $urlAttributes['product_categories'] = $attributes['product_categories'];
            }

            if($key != config('site.perPage.default.'.$inRow)) {
                $urlAttributes['per_page'] = $key;
            } else {
                unset($urlAttributes['per_page']);
            }

            $perPageUrls[$key] = route('search.index', $urlAttributes);
        }

        $altProducts = [];

        if(empty($products)) {
            $ids = \Settings::get('search_empty_category_id');
            $ids = explode(',', $ids);

            if(!empty($ids)) {
                foreach($ids as $id) {
                    $category = Category::find($id);

                    if($category) {                            
                        $altProducts[] = $category->products()->published(1)->orderBy(\DB::raw('RAND()'))->take(5)->get();
                    }
                }
            }
        }

        if(!empty($attributes['filter'])) {
            $attributes = array_merge($attributes['filter'], $attributes);
        }

        $context = 'search';

        $assign = compact(
            'altProducts',
            'attributes',
            'term',
            'products',
            'perPage',
            'pagination',
            'layoutService',
            'sorting',
            'categories',
            'sortingService',
            'inRow',
            'originalUrl',
            'currentPerPage',
            'currentSortBy',
            'sortUrls',
            'perPageUrls',
            'context',
            'options',
            'perPageKey'
        );

        $urlAttributes = [];
        $urlAttributes['sort_by'] = $currentSortBy;

        $queryAttributes = request()->only('term', 'product_categories', 'filter');

        $urlAttributes = array_merge($urlAttributes, $queryAttributes);

        if($currentPerPage) {
            $urlAttributes['per_page'] = $currentPerPage;
        }

        if($request->ajax() && $request->loadmore) {
            return [
                'products' => view()->make($viewPref.'.category.partials.layouts.grid', $assign)->render(),
                'pagination' => view()->make($viewPref.'.category.partials.pagination', $assign)->render(),
                'loadmore' => view()->make($viewPref.'.category.partials.loadmore', $assign)->render(),
                'title' => 'Поиск | Страница '.$pagination->currentPage()
            ];
        } elseif($request->ajax()) {
            $filerAttributes = collect($urlAttributes)->except('term', 'product_categories', 'filter')->toArray();

            return [
                'products' => view()->make($view, $assign)->render(),
                'url' => route('search.index', $urlAttributes),
                'filterUrl' => route('search.index', $filerAttributes),
            ];
        }

        return view($view, $assign);
    }

    public function autocomplete(Request $request)
    {
        $term = mb_strtolower($request->term, 'utf-8');

        $result = $this->searchAutocomplete($term);

        $useSpeller = true;

        if($result['products']->count() || count($result['categories'])) {
            $useSpeller = false;
        }

        if($useSpeller) {
            $term = $this->speller($term);
        }

        $result = $this->searchAutocomplete($term);

        return view(mobile_prefix().'.search.autocomplete', $result);
    }

    protected function speller($term)
    {
        $texts = explode(" ", $term);

        $q = [];
        $w = [];

        foreach ($texts as $key => $value) {
            $value = trim($value);

            if($value !== '') {
                $q[] = http_build_query(['text' => $value]);
                $w[] = $value;
            }
        }

        $response = file_get_contents('https://speller.yandex.net/services/spellservice.json/checkTexts?'.implode('&', $q));

        $result = json_decode($response, true);

        $errors = false;

        foreach ($result as $key => $value) { 
            if(!empty($value[0]['s'][0])) {
                $w[$key] = $value[0]['s'][0];

                $errors = true;
            }
        }

        if(!$errors) {
            return $term;
        }

        return implode(' ', $w);
    }

    protected function searchCategories($term)
    {
        $query = [
            'type' => 'categories',
            'body' => [
                "query" => [
                    'bool' => [
                        'filter' => [
                            'term' => [
                                'published' => 1
                            ]
                        ],
                        'must' => [
                            'match' => [
                                'title' => $term,
                            ]
                        ]
                    ]
                ],
                'highlight' => [
                    'fields' => ['title' => ['type' => 'plain']],
                ],
                'size' => 7,
                'from' => 0
            ],
        ];

        $search = \App\Models\Category::complexSearch($query);
        $hits = $search->getHits();

        if(empty($hits['hits'])) {
            return Category::published(1)->where('categories.title', 'like', '%'.$term.'%')
                        ->take(7)
                        ->get();
        }

        $ids = array_pluck($hits['hits'], '_id');
        
        $categories = Category::whereIn('id', $ids)
                        ->orderByRaw(\DB::raw('FIELD(id, '.implode(',', $ids).')'))
                        ->get()
                        ->keyBy('id');

        foreach ($hits['hits'] as $key => $value) {
            if(empty($categories[$value['_id']])) {
                continue;
            }
            
            $categories[$value['_id']]->title = $value['highlight']['title'][0];
        }

        return $categories;
    }

    protected function searchProducts($term)
    {
        $query = [
            'type' => 'products',
            'body' => [
                "query" => [
                    'bool' => [
                        'filter' => [
                            'term' => [
                                'published' => 1
                            ],
                        ],
                        'must' => [
                            'match' => [
                                'search_keys' => $term,
                            ]
                        ]
                    ]
                ],
                'highlight' => [
                    'fields' => ['title' => ['type' => 'plain']],
                ],
                'size' => 6,
                'from' => 0
            ],
        ];

        $search = \App\Models\Product::complexSearch($query);
        $hits = $search->getHits();

        if(empty($hits['hits'])) {
            return Product::published(1)->where('title', 'like', '%'.$term.'%')
                ->published(1)
                ->with('category')
                ->take(6)
                ->get()
                ->keyBy('id');
                
            return collect([]);
        }

        $ids = array_pluck($hits['hits'], '_id');
        
        $products = Product::whereIn('id', $ids)
                        ->orderByRaw(\DB::raw('FIELD(id, '.implode(',', $ids).')'))
                        ->get()
                        ->keyBy('id');

        return $products;
    }

    protected function searchAutocomplete($term)
    { 
        $categories = $this->searchCategories($term);

        foreach ($categories as $k => $c) {
            $categories[$k]->product_count = $c->products()->published(1)->count();
        }

        $products = $this->searchProducts($term);

        return compact('products', 'categories', 'term');
    }

    public function menu()
    {
        return view(mobile_prefix().'.search.menu');
    }
}
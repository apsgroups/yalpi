<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Repositories\Frontend\Product\ProductContract;
use App\Services\Frontend\LayoutService;
use App\Services\Meta\MetaFactory;
use Illuminate\Http\Request;
use App\Services\Frontend\SortingService;
use App\Models\Author;

class AuthorController extends Controller
{
    protected $products;

    public function __construct(ProductContract $products)
    {
        $this->products = $products;
    }

    public function items($slug, Request $request)
    {
        $author = Author::slug($slug)->firstOrFail();

        $inRow = 4;

        $page = (int)$request->input('page');

        $perPageKey = $inRow;
        $perPage = (int)$request->input('per_page', config('site.perPage.default.'.$perPageKey));
        
        $limits = config('site.perPage.limits.'.$perPageKey);

        if(!isset($limits[$perPage])) {
            $perPage = config('site.perPage.default.'.$perPageKey);
        }

        $sortingService = new SortingService();

        $sortDir = $sortingService->getDirection();

        $sortBy = $sortingService->convertAttributeToField(null);

        $sortByField = $sortingService->convertAttributeToField();

        $products = $author->products()->published(1)->orderBy('id', 'desc')->paginate($perPage);

        $pagination = $products;

        $products = \Cache::remember('author.'.$author->id.'.products.'.request('page'), config('cache.time'), function () use($products) {
            $list = [];

            foreach ($products as $k => $p) {
                $list[] = $p->getPreviewData();
            }

            return collect($list);
        });

        $view = $request->ajax() ? 'category.partials.products' : 'author.index';
        $view = mobile_prefix().'.'.$view;

        $sortUrls = [];

        $perPageUrls = [];

        $layoutService = new LayoutService();
        $layoutService->setLayout(null);

        $meta = MetaFactory::getInstance($author, ['total' => $pagination->total()]);

        $assign = compact(
            'meta',
            'author',
            'products',
            'perPage',
            'pagination',
            'layoutService',
            'sortingService',
            'inRow',
            'sortUrls',
            'perPageUrls',
            'perPageKey'
        );

        if($request->ajax() && $request->loadmore) {
            return [
                'products' => view()->make(mobile_prefix().'.category.partials.layouts.grid', $assign)->render(),
                'pagination' => view()->make(mobile_prefix().'.category.partials.pagination', $assign)->render(),
                'loadmore' => view()->make(mobile_prefix().'.category.partials.loadmore', $assign)->render(),
                'title' => $meta->title().($pagination->currentPage() > 1 ? ' | Страница '.$pagination->currentPage() : ''),
                'url' => route('author.items', [$author->slug]),
            ];
        } elseif($request->ajax()) {
            return [
                'products' => view()->make($view, $assign)->render(),
                'url' => route('author.items', [$author->slug]),
                'title' => $meta->title().($pagination->currentPage() > 1 ? ' | Страница '.$pagination->currentPage() : ''),
                'heading' => $meta->heading()
            ];
        }

        return view($view, $assign);
    }
}
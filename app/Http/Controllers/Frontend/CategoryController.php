<?php

namespace App\Http\Controllers\Frontend;

use App\Models\PropertyValue;
use App\Models\Property;
use App\Repositories\Frontend\Category\CategoryContract;
use App\Http\Controllers\Controller;
use App\Repositories\Frontend\Product\ProductContract;
use App\Services\Frontend\LayoutService;
use App\Services\MenuRoute;
use App\Services\Meta\MetaFactory;
use Illuminate\Http\Request;
use App\Services\Filter\ProductFilter;
use App\Services\Frontend\SortingService;
use App\Models\Category;
use App\Facades\Favorite as FavoriteFacade;

class CategoryController extends Controller
{
    protected $categories;
    protected $products;

    public function __construct(CategoryContract $categories, ProductContract $products)
    {
        $this->categories = $categories;
        $this->products = $products;
    }

    public function index(Request $request)
    {
        $id = (int)$request->input('id');

        $category = Category::findOrFail($id);

        if(!$category->published) {
            abort(404);
        }

        if(trim($category->link(), '/') !== str_replace(['/lower-price', '/higher-price'], ['', ''], $request->path())) {
            return redirect($category->link(), 301);
        }

        $inRow = $category->getInRow();

        $page = (int)$request->input('page');

        $perPageKey = $inRow;
        $perPage = (int)$request->input('per_page', config('site.perPage.default.'.$perPageKey));
        
        $limits = config('site.perPage.limits.'.$perPageKey);

        if(!isset($limits[$perPage])) {
            $perPage = config('site.perPage.default.'.$perPageKey);
        }

        $mode = $request->input('mode');

        if($mode) {
            session(['mode.'.$category->id => $mode]);
        } else {
            $mode = session('mode.'.$category->id, 'products');
        }

        if($mode === 'compositions' || (!$request->input('per_page') && (int)$id === 382)) {
            $perPage = 100;
        }

        $sortingService = new SortingService();

        $attributes = $request->all();

        $attributes['product_categories'][] = ($category->autofiltering && $category->autofiltering_category_id) ? $category->autofiltering_category_id : $id;

        $sortDir = $sortingService->getDirection();

        $sortBy = $sortingService->convertAttributeToField(null);

        $filterAttributes = [];
        if(!empty($attributes['filter'])) {
            $attributes = array_merge($attributes, $attributes['filter']);
            $filterAttributes = $attributes['filter'];
            unset($attributes['filter']);
        }

        $skipFilterOptions = [];

        if($category->autofiltering && $category->filter_options) {
            foreach ($category->filter_options as $key => $v) {
                if(stripos($key, 'range') !== false) {
                    $property = Property::find(str_replace('range_', '', $key));

                    $skipFilterOptions[] = $property->slug;

                    if($v['min'] > 0) {                            
                        $attributes['range_'.$property->slug]['min'] = $v['min'];
                    }

                    if($v['max'] > 0) {                            
                        $attributes['range_'.$property->slug]['max'] = $v['max'];
                    }

                    continue;
                }

                $propertyValue = PropertyValue::find($v);

                if(!$propertyValue) {
                    continue;
                }

                $attributes[$propertyValue->property->slug][] = $v;

                $skipFilterOptions[] = $propertyValue->property->slug;
            }
        }

        $sortByField = $sortingService->convertAttributeToField();

        try {                
            $filter = new ProductFilter();
            $filter->setAttributes($attributes);

            //$sortByFilter = ($sortByField === 'category' && $mode === 'compositions') ? 'sets_ordering' : $sortByField;
            $sortByFilter = $sortByField;
            $filter->setSortBy($sortByFilter);

            $filter->setSortDir($sortDir);

            $filter->setPage($perPage, $page);
            $filter->apply();

            $options = $filter->getOptions();
            $pagination = $filter->getPagination();

            $ids = $filter->getIds();

            $products = collect([]);

            if($ids) {
                $cacheKey = 'category.'.$id.'.products.'.md5(implode('.', $ids));

                $products = \Cache::remember($cacheKey, config('cache.time'), function () use($ids) {
                    $products = \App\Models\Product::whereIn('id', $ids)
                        ->published(1)
                        ->with('image', 'images', 'type', 'category', 'categories')
                        ->orderByRaw(\DB::raw('FIELD(id, '.implode(',', $ids).')'))
                        ->get();

                    $list = [];

                    foreach ($products as $k => $p) {
                        $list[] = $p->getPreviewData();
                    }

                    return collect($list);
                });
            }
        } catch(\Elasticsearch\Common\Exceptions\NoNodesAvailableException $e) {
            \Cache::remember('elastic_server_status', 60, function() use ($e) {
                report_email($e);
                return 'ok';
            });

            $options = ['obshchij-cvet' => ['buckets' => '']];

            $orderByField = [
                'category' => 'pivot_ordering',
                'price' => 'price',
            ];

            $orderByDir = [
                'category' => 'asc',
                'price' => $sortDir,
            ];

            $products = $category->products()
                    ->published(1)
                    ->with('image', 'images', 'type', 'category')
                    ->orderBy($orderByField[$sortByField], $orderByDir[$sortByField])
                    ->paginate($perPage);

            $pagination = $products;
        }

        $productSets = ($mode === 'compositions') ? $this->products->productSets($products) : null;

        $currentSortBy = $sortingService->getAttribute() == $sortingService->getDefaultAttribute() ? null : $sortingService->getAttribute();
        $currentPerPage = config('site.perPage.default.'.$perPageKey) == $perPage ? null : $perPage;

        if($perPage !== 1000 && (int)$id === 382) {
            $currentPerPage = $perPage;
        }

        if($mode === 'compositions') {
            $currentPerPage = null;
        }

        $originalUrl = category_url($category, $filterAttributes, $currentSortBy, $currentPerPage);

        if($pagination) {                
            $pagination->setPath($originalUrl);
        }

        $lastSeen = $this->products->getLastSeen();

        $view = $request->ajax() ? 'category.partials.products' : 'category.index';
        $view = mobile_prefix().'.'.$view;

        $filterUrl = category_url($category, [], $currentSortBy, $currentPerPage);

        $context = 'category_id';
        $contextId = $id;

        $sortUrls = [];
        foreach($sortingService->getNames() as $sortName) {
            $sortUrls[$sortName] = category_url($category, $filterAttributes, $sortName, $currentPerPage);
        }

        $perPageUrls = [];

        if($perPageConfig = config('site.perPage.limits.'.$perPageKey)) {
            foreach($perPageConfig as $key => $value) {
                $perPageUrls[$key] = category_url($category, $filterAttributes, $currentSortBy, $value);
            }
        }

        foreach ($options as $key => $value) {
            if(in_array($key, $skipFilterOptions)) {
                unset($options[$key]['buckets']);
            }
        }
        
        if($category->filters_activate) {
            $activatedProperties = Property::whereIn('id', $category->filters_activate)
                ->orderByRaw(\DB::raw('FIELD(id, '.implode(',', $category->filters_activate).')'))
                ->get();

            $activatedOptions = [];

            foreach($activatedProperties as $k => $v) {
                if(empty($options[$v->slug]['buckets'])) {
                    continue;
                }

                $activatedOptions[$v->slug] = $options[$v->slug];
            }

            $options = $activatedOptions;
        }

        if($category->filters_deactivate) {
            $deactivatedProperties = Property::whereIn('id', $category->filters_deactivate)->get();

            foreach($deactivatedProperties as $k => $v) {
                unset($options[$v->slug]);
            }
        }

        if($category->disabled_options) {
            foreach($options as $k => $v) {
                if(empty($v['buckets'])) {
                    continue;
                }

                foreach($v['buckets'] as $bk => $bucket) {
                    if(in_array($bucket['key'], $category->disabled_options)) {
                        unset($options[$k]['buckets'][$bk]);
                    }
                }
            }
        }

        $layoutService = new LayoutService();
        $layout = $mode === 'compositions' ? 'sets' : null;
        $layoutService->setLayout($layout);

        $meta = MetaFactory::getInstance($category, ['filter' => request('filter'), 'total' => $pagination->total()]);

        $tags = $category->tags()->published(1)->with('tagable')->orderBy('taggables.ordering', 'ASC')->get();

        $categoryTotal = $pagination->total();
        $filteredTotal = '';

        if(request('filter')) {
            $categoryTotal = $category->products()->published(1)->count();
            $filteredTotal = 'Найдено <span>'.$pagination->total().'</span> '.total_products_morph($pagination->total());
        }

        $children = $this->categories->getChildrenWithoutComposition($id, ['image']); 

        if(!$children->count() && $category->parent && $category->parent->child_template === 'icons') {
            $category->child_template = 'icons'; 
            $children = $this->categories->getChildrenWithoutComposition($category->parent_id, ['image']);
        }

        $favorites = FavoriteFacade::all()->toArray();

        $assign = compact(
            'meta',
            'options',
            'category',
            'products',
            'perPage',
            'pagination',
            'layoutService',
            'sortingService',
            'lastSeen',
            'inRow',
            'filterUrl',
            'originalUrl',
            'context',
            'contextId',
            'attributes',
            'compositions',
            'currentPerPage',
            'currentSortBy',
            'sortUrls',
            'perPageUrls',
            'productSets',
            'mode',
            'tags',
            'perPageKey',
            'categoryTotal',
            'filteredTotal',
            'children',
            'favorites'
        );

        if($request->ajax() && $request->loadmore) {
            return [
                'products' => view()->make(mobile_prefix().'.category.partials.layouts.grid', $assign)->render(),
                'pagination' => view()->make(mobile_prefix().'.category.partials.pagination', $assign)->render(),
                'loadmore' => view()->make(mobile_prefix().'.category.partials.loadmore', $assign)->render(),
                'title' => $meta->title().($pagination->currentPage() > 1 ? ' | Страница '.$pagination->currentPage() : ''),
                'url' => category_url($category, $filterAttributes, $currentSortBy, $currentPerPage),
            ];
        } elseif($request->ajax()) {
            return [
                'total_filtered' => $filteredTotal,
                'products' => view()->make($view, $assign)->render(),
                'url' => category_url($category, $filterAttributes, $currentSortBy, $currentPerPage),
                'title' => $meta->title().($pagination->currentPage() > 1 ? ' | Страница '.$pagination->currentPage() : ''),
                'heading' => $meta->heading(),
                'filterUrl' => $filterUrl,
            ];
        }

        return view($view, $assign);
    }

    public function modules(Request $request)
    {
        $id = $request->input('module_parent_id');
        $product = $this->products->findById($id);

        return redirect($product->link(), 301);
    }

    public function sortColors($list)
    {
        if(empty($list)) {
            return $list;
        }

        $collection = collect($list)->keyBy('key');

        $ids = $collection->pluck('key')->toArray();

        if(empty($ids)) {
            return $list;
        }

        $values = PropertyValue::whereIn('id', $ids)
            ->orderBy('ordering')
            ->get()
            ->keyBy('id');

        if(empty($values)) {
            return $list;
        }

        $results = [];

        foreach($values as $v) {
            $result = $collection[$v->id];
            $result['ordering'] = $v->ordering;
            $results[] = $result;
        }
        
        return $results;
    }

    public function catalog()
    {
        MenuRoute::setActive();

        return view(mobile_prefix().'.category.catalog');
    }
}
<?php

namespace App\Http\Controllers\Frontend\Auth;

use App\Events\Frontend\Auth\UserRegistered;
use App\Http\Controllers\Controller;
use App\Http\Requests\Frontend\Auth\RegisterRequest;
use App\Services\Access\Traits\ConfirmUsers;
use App\Services\Access\Traits\UseSocialite;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use App\Repositories\Frontend\User\UserContract;
use App\Services\Access\Traits\AuthenticatesAndRegistersUsers;

/**
 * Class AuthController
 * @package App\Http\Controllers\Frontend\Auth
 */
class AuthController extends Controller
{

    use AuthenticatesAndRegistersUsers, ConfirmUsers, ThrottlesLogins, UseSocialite;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/profile/favorites/';

    /**
     * Where to redirect users after they logout
     *
     * @var string
     */
    protected $redirectAfterLogout = '/';

    /**
     * @param UserContract $user
     */
    public function __construct(UserContract $user)
    {
        $this->user = $user;
    }

    public function showLoginForm()
    {
        $view = mobile_prefix().'.auth.login';

        if(\Request::ajax()) {
            $view = mobile_prefix().'.auth.login_form';
        }

        return view($view)
            ->withSocialiteLinks($this->getSocialLinks());
    }

    public function showRegistrationForm()
    {
        $view = 'frontend.auth.register';

        if(\Request::ajax()) {
            $view = 'frontend.auth.register_form';
        }

        return view($view)
            ->withSocialiteLinks($this->getSocialLinks());
    }

    public function register(RegisterRequest $request)
    {
        $data = $request->all();

        if (config('access.users.confirm_email') && config('access.users.forced_auth') === false) {
            $user = $this->user->create($data, false, null);
            event(new UserRegistered($user));

            if(\Request::ajax()) {
                return ['msg' => trans('exceptions.frontend.auth.confirmation.created_confirm')];
            }

            return redirect()->route('frontend.index')->withFlashSuccess(trans('exceptions.frontend.auth.confirmation.created_confirm'));
        } else {
            auth()->login($this->user->create($data, false, $data['member'] ?? null));
            event(new UserRegistered(access()->user()));

            if(\Request::ajax()) {
                return ['redirect' => $this->redirectPath()];
            }

            return redirect($this->redirectPath());
        }
    }
}
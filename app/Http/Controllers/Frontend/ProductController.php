<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Category;
use App\Models\Product;
use App\Models\ProductHit;
use App\Repositories\Frontend\Product\ProductContract;

use App\Http\Controllers\Controller;
use App\Services\Frontend\LayoutService;
use App\Services\Frontend\SortingService;
use App\Services\Meta\MetaFactory;
use Illuminate\Http\Request;
use App\Facades\Favorite as FavoriteFacade;

class ProductController extends Controller
{
    protected $products;

    public function __construct(ProductContract $products)
    {
        $this->products = $products;
    }

    public function show(Request $request)
    {
        $id = $request->get('id');

        $product = Product::findOrFail($id);

        if(trim($product->link(), '/') !== $request->path()) {
            return redirect($product->link(), 301);
        }

        if($product->published === 0) {
            return redirect($product->category->link(), 301);
        }

        $product->load([
            'image', 
            'images', 
            'category',
            'type',
            'categories'
        ]);

        $product->view_hits += 1;
        $product->save();

        $lastSeen = $this->products->getLastSeen();

        $this->products->setLastSeen($id);

        $similarProducts = $this->products->similarProducts($product);

        $features = $this->products->features($product);

        $experience = $this->products->experience($product);

        $freeCources = $product->cources()->free(1)->ordered()->get();
        $courceProgram = $product->cources()->free(0)->ordered()->get();
        $authors = $product->authors()->ordered()->get();
        $relateds = $product->relateds()->ordered()->get();

        $meta = MetaFactory::getInstance($product);

        $comments = $product->comments()->where('parent_id', null)->published(1)->orderBy('popularity', 'desc')->orderBy('id', 'desc')->paginate(10);

        $comments->setPath(route('frontend.comments.index', ['product', $product->id]));

        $commentsCount = $product->comments()->published(1)->count();

        $userLikes = [];

        $user = auth()->user();

        $favorite = false;

        if($user && $user->id) {
            if($comments->count()) {
                $userLikes = \App\Models\CommentLike::ofUser($user->id)->whereIn('comment_id', $comments->pluck('id'))->get()->keyBy('comment_id')->toArray();
            }

            $favorite = FavoriteFacade::isActive($product->id);
        }

        return view(mobile_prefix().'.product.show', compact(
            'meta',
            'product',
            'lastSeen',
            'similarProducts',
            'comments',
            'userLikes',
            'features',
            'experience',
            'freeCources',
            'courceProgram',
            'authors',
            'relateds',
            'commentsCount',
            'favorite'
        ));
    }

    public function similarProducts($id)
    {
        $product = $this->products->findById($id);
        $similarProducts = $this->products->similarProducts($product, 4, 40);

        return view('frontend.product.partials.similar.grid', compact('similarProducts'));
    }

    public function similar($id)
    {
        $product = $this->products->findById($id);
        $modules = $this->products->similarModules($product);

        $items = [];

        foreach($modules as $module) {
            $items[] = view()->make('frontend.product.partials.modules.ajax_product', compact('product', 'module'))->render();
        }

        return [
            'products' => $items,
            'nextUrl' => $modules->nextPageUrl(),
        ];
    }

    public function promo($id)
    {
        return \Cache::remember('promo.content.'.$id, config('cache.time'), function() use($id) {
            $category = Category::findOrFail($id);

            $saleCategoryId = \Settings::get('sale_category_id');

            if(empty($saleCategoryId)) {
                return '';
            }

            $saleCategory = Category::published(1)->find($saleCategoryId);

            if(empty($saleCategory)) {
                return '';
            }

            $limit = 4;

            $products = $category->products()
                    ->with(['image', 'category'])
                    ->published(1)
                    ->whereHas('categories', function ($query) use($saleCategory, $category) {
                        $query->where('categories.id', $saleCategory->id);
                    })
                    ->orderBy('pivot_ordering', 'asc')
                    ->take($limit)
                    ->get();

            return view()->make('widgets.product.promo._products', compact('products'))->render();
        });
    }
}
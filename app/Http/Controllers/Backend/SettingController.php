<?php

namespace App\Http\Controllers\Backend;

use App\Models\Setting;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class SettingController extends Controller
{
    public function index()
    {
        $feedbacks = [];
        foreach(config('site.feedbacks') as $name) {
            $class = '\App\Services\Feedback\\'.ucfirst($name);

            if(!class_exists($class)) {
                throw new \Exception('Class '.$class.' doesnt exists');
            }

            $feedbacks[] = new $class();
        }

        return view('backend.setting.index', compact('feedbacks'));
    }

    public function store(Request $request)
    {
        $settings = $request->input('settings');

        foreach($settings as $title => $content) {
            $setting = Setting::firstOrNew(['title' => $title]);
            $setting->content = $content;
            $setting->save();
        }

        \Cache::forget('settings');

        return back()->withFlashSuccess('Настройки сохранены');
    }
}

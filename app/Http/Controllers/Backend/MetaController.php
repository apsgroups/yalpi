<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Backend\Traits\RouteAfterSaveTrait;
use App\Models\Category;
use App\Models\Meta;
use App\Repositories\Backend\Category\CategoryContract;
use App\Http\Requests\Backend\Meta\Save;

use App\Http\Controllers\Controller;

class MetaController extends Controller
{
    use RouteAfterSaveTrait;

    protected $categories;

    public function __construct(CategoryContract $categoryRepository)
    {
        $this->categories = $categoryRepository;
    }

    public function index()
    {
        $limit = request()->get('limit', 10);

        $model = Meta::query();

        $filter = request()->get('filter', []);

        if(!empty($filter['search'])) { 
            $search = $filter['search'];
            $model->where(function($query) use($search) {
                $query->where('title', 'LIKE', '%'.$search.'%')
                    ->orWhere('id', $search)
                    ->orWhere('meta_title', 'LIKE', '%'.$search.'%')
                    ->orWhere('meta_description', 'LIKE', '%'.$search.'%')
                    ->orWhere('meta_keywords', 'LIKE', '%'.$search.'%') 
                    ->orWhere('heading', 'LIKE', '%'.$search.'%');
            });
        } 

        if(!empty($filter['category_id'])) {
            $category = Category::find($filter['category_id']);

            $fields = [$category->meta_category_id, $category->meta_filter_id, $category->meta_product_id];

            if(!empty($filter['type'])) {
                $fields = [$category->{$filter['type']}];
            }

            $model->whereIn('id', $fields);
        } 

        if(empty($filter['category_id']) && !empty($filter['type'])) {
            $ids = Category::where($filter['type'], '>', 0)->get()->pluck($filter['type']);

            if($ids) {
                $model->whereIn('id', $ids);
            }
        }

        $items = $model->orderBy('id', 'desc')->paginate($limit);


        $appends = [];

        if(!empty($filter)) {
            $appends['filter'] = $filter;
        }

        if(!empty(request()->get('limit'))) {
            $appends['limit'] = $limit;
        }

        if(!empty($appends)) {
            $items->appends($appends);
        }

        $categories = $this->categories->getParentsForSelect();


        return view('backend.meta.index', compact('items', 'categories', 'filter'));
    }

    public function create()
    {
        $categories = $this->categories->getParentsForSelect();

        $useInProducts = [];
        $useInCategories = [];
        $useInFilters = [];

        return view('backend.meta.create', compact('categories', 'useInProducts', 'useInCategories', 'useInFilters'));
    }

    public function store(Save $request)
    {
        $meta = Meta::create($request->all());

        $this->updateCategories($meta->id, 'meta_category_id', $request->categories);
        $this->updateCategories($meta->id, 'meta_product_id', $request->products);
        $this->updateCategories($meta->id, 'meta_filter_id', $request->filters);

        return redirect($this->routeAfterSave('meta', [$meta->id]))
            ->withFlashSuccess('Мета-шаблон создан!');
    }

    public function edit($id)
    {
        $meta = Meta::findOrFail($id);

        $categories = $this->categories->getParentsForSelect();

        $useInCategories = Category::where('meta_category_id', $id)->get()->pluck('id')->toArray();
        $useInProducts = Category::where('meta_product_id', $id)->get()->pluck('id')->toArray();
        $useInFilters = Category::where('meta_filter_id', $id)->get()->pluck('id')->toArray();

        return view('backend.meta.edit', compact('meta', 'categories', 'useInProducts', 'useInCategories', 'useInFilters'));
    }

    public function update($id, Save $request)
    {
        $meta = Meta::findOrFail($id);

        $meta->update($request->all());

        $this->updateCategories($id, 'meta_category_id', $request->categories);
        $this->updateCategories($id, 'meta_product_id', $request->products);
        $this->updateCategories($id, 'meta_filter_id', $request->filters);

        return redirect($this->routeAfterSave('meta', [$id]))
            ->withFlashSuccess('Мета-шаблон сохранен');
    }

    public function destroy($id)
    {
        Meta::destroy($id);

        Category::where('meta_category_id', $id)->update(['meta_category_id' => null]);
        Category::where('meta_product_id', $id)->update(['meta_product_id' => null]);
        Category::where('meta_filter_id', $id)->update(['meta_filter_id' => null]);

        return back()->withSuccessMessage('Элемент удален!');
    }

    protected function updateCategories($id, $field, $categories)
    {
        Category::where($field, $id)->update([$field => null]);

        if($categories) {
            Category::whereIn('id', $categories)->update([$field => $id]);
        }
    }
}

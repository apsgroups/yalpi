<?php

namespace App\Http\Controllers\Backend\Traits;

trait RouteAfterSaveTrait
{
    protected function routeAfterSave($pref, $params = [])
    {
        $action = 'edit';

        if(\Request::input('save-and-new')) {
            $action = 'create';
            array_pop($params);
        }

        if(\Request::input('save-and-close')) {
            $action = 'index';
            array_pop($params);
        }

        return route('admin.'.$pref.'.'.$action, $params);
    }
}
<?php

namespace App\Http\Controllers\Backend\Traits;

use Illuminate\Http\Request;

trait ToggleTrait
{
    public function toggle(Request $request, $id)
    {
        try {
            $model = app()->make($this->modelName)->findOrFail($id);

            $model->setAttribute($request->input('field'), $request->input('value'));
            $model->save();

            if(method_exists($model, 'getIndexDocumentData')) {                    
                try {
                    $model->updateIndex();
                } catch(\Elasticsearch\Common\Exceptions\Missing404Exception $e) {
                    $model->addToIndex();
                }
            }
        } catch(\Exception $e) {
            return response()->json(['status' => 'fail', 'msg' => $e->getMessage()]);
        }

        return response()->json(['status' => 'success', 'msg' => 'Состояние изменено', 'state' => $model->getAttribute($request->input('field'))]);
    }
}
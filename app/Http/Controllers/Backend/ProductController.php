<?php

namespace App\Http\Controllers\Backend;

use App\Events\ProductSaved;
use App\Facades\Properties;
use App\Http\Controllers\Backend\Traits\RouteAfterSaveTrait;
use App\Http\Controllers\Backend\Traits\ToggleTrait;
use App\Models\Attribute;
use App\Models\MarketCategory;
use App\Models\Product;
use App\Models\ProductType;
use App\Models\Property;
use App\Repositories\Backend\Category\CategoryContract;
use App\Repositories\Backend\Product\ProductContract;
use App\Models\Language;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Product\Create;

use Illuminate\Support\Facades\Input;

use App\Http\Requests\Backend\Product\Batch;
use App\Http\Requests\Backend\Product\BatchCategories;
use App\Http\Requests\Backend\Product\BatchPrimaryCategory;

class ProductController extends Controller
{
    use ToggleTrait, RouteAfterSaveTrait;

    protected $modelName = '\App\Models\Product';

    protected $products;

    protected $categories;

    public function __construct(ProductContract $products, CategoryContract $categoryRepository)
    {
        $this->products = $products;

        $this->categories = $categoryRepository;
    }

    public function index()
    {
        $filter = Input::get('filter', []);

        $products = $this->products;

        if(!empty($filter)) {
            if(isset($filter['search'])) {
                $products->setSearchWord($filter['search']);
            }

            if(isset($filter['ext_id'])) {
                $products->setFilledOrEmpty('ext_id', $filter['ext_id']);
            }

            if(isset($filter['property'])) {
                $products->setPropertyFilter($filter['property']);
            }

            $products->setBooleanFilter(collect($filter)->only([
                'published',
                'out_of_stock',
                'import',
                'market',
                'yandex_sitemap',
                'google_sitemap',
                'user_id',
            ])->toArray());
        }

        $sort = Input::get('sort', 'id');
        $dir = Input::get('dir', 'desc');
        $limit = Input::get('limit', 10);

        $view = '';

        if(in_array($sort, ['sets_ordering', 'category'])) {
            $dir = 'asc';
            $view = '-sorting';
            $limit = 10000;
        }

        if(!empty($filter['category_id'])) {
            $products->setCategoryFilter($filter['category_id'], ($sort === 'category' ? $dir : false));
        }

        $propertyId = Input::get('property_id');
        if($sort !== 'property_id' && $sort !== 'category') {
            $products->setSort($sort, $dir);
        } elseif($propertyId) {
            $products->setSortByProperty($propertyId, $dir);
        }

        $products = $products->paginateWith($limit, ['category', 'properties', 'user', 'type']);

        if(!empty($filter)) {
            $products->appends(['filter' => $filter]);
        }

        if(!empty($sort)) {
            $products->appends(['sort' => $sort, 'dir' => $dir]);
        }

        $products->appends(['limit' => $limit]);

        $categories = $this->categories->getParentsForSelect();

        return view('backend.product.index'.$view, compact('products', 'filter', 'categories'));
    }

    public function create()
    {
        $categories = $this->categories->getParentsForSelect();

        $properties = Property::all();

        $types = ProductType::all()->pluck('title', 'id')->toArray();

        $languages = Language::all()->pluck('title', 'id');

        $featureIcons = config('site.feature_icons', []);

        $features = null;

        $freeCources = null;

        $courceProgram = null;

        $rateSelect = range(0, 5);

        $authors = collect([]);

        $relateds = collect([]);

        $priceTypeProperty = $properties->where('slug', 'price-type')->first();

        $priceTypes = [];

        if($priceTypeProperty) {
            $priceTypes = $priceTypeProperty->values()->orderBy('ordering', 'asc')->get()->pluck('title', 'id');
        }

        return view('backend.product.create', compact(
            'categories', 
            'properties', 
            'types',
            'featureIcons',
            'features',
            'freeCources',
            'courceProgram',
            'rateSelect',
            'languages',
            'authors',
            'relateds',
            'priceTypes'));
    }

    public function store(Create $request)
    {
        $product = $this->products->create($request->all());
        event(new ProductSaved($product, $request->all()));

        return redirect($this->routeAfterSave('product', [$product->id]))
            ->withFlashSuccess('Товар создан');
    }

    public function edit($id)
    {
        $categories = $this->categories->getParentsForSelect();

        $product = $this->products->findById($id);

        $properties = Properties::all();

        $types = ProductType::all()->pluck('title', 'id');

        $languages = Language::all()->pluck('title', 'id');

        $populars = $product->populars()->pluck('category_id')->all();

        $featureIcons = config('site.feature_icons', []);

        $features = $product->features()->orderBy('ordering', 'asc')->get();

        $freeCources = $product->cources()->free()->ordered()->get();

        $courceProgram = $product->cources()->free(0)->ordered()->get();

        $authors = $product->authors()->ordered()->lists('authors.name', 'authors.id');
        
        $relateds = $product->relateds()->ordered()->lists('products.title', 'products.id');

        $rateSelect = range(0, 5);

        $priceTypeProperty = $properties->where('slug', 'price-type')->first();

        $priceTypes = [];

        if($priceTypeProperty) {
            $priceTypes = $priceTypeProperty->values()->orderBy('ordering', 'asc')->get()->pluck('title', 'id');
        }

        return view('backend.product.edit', compact(
            'product',
            'categories',
            'properties',
            'types',
            'populars',
            'featureIcons',
            'features',
            'freeCources',
            'courceProgram',
            'rateSelect',
            'languages',
            'authors',
            'relateds',
            'priceTypes'
            ));
    }

    public function update(Create $request, $id)
    {
        $attributes = $request->all();

        $product = $this->products->update($id, $attributes);

        event(new ProductSaved($product, $attributes));

        return redirect($this->routeAfterSave('product', [$id]))
            ->withFlashSuccess('Товар сохранен');
    }

    public function search()
    {
        $query = \Request::input('query');
        $products = Product::where('title', 'like', '%'.$query.'%')
            ->orWhere('slug', $query)
            ->orWhere('sku', $query)
            ->orWhere('ext_id', $query)
            ->orWhere('id', $query)
            ->take(5)
            ->get();

        $list = [];
        foreach($products as $product) {
            $list[] = [
                    'title' => view('backend.product.partials.modules.search_result', compact('product'))->render(),
                    'id' => $product->id,
                ];
        }

        return response()->json($list);
    }

    public function module($id)
    {
        $module = $this->products->findById($id);

        $attribute = null;

        $quantity = \Request::input('quantity');

        return view('backend.product.partials.modules.item', compact('module', 'attribute', 'quantity'));
    }

    public function saveSort()
    {
        $this->products->saveSortByCategory(Input::get('items'), Input::get('category_id'));

        return $this->back('Сортировка изменена!');
    }

    public function destroy($id)
    {
        $this->products->destroy($id);

        return $this->back('Товар удален!');
    }

    public function batchCopy()
    {
        $items = \Request::input('items');

        foreach($items as $id) {
            $this->products->copy($id);
        }

        return $this->back('Товар скопирован!');
    }

    public function batchDestroy(Batch $request)
    {
        foreach($request->items as $id) {
            $this->products->destroy($id);
        }

        return $this->back('Товары удалены!');
    }

    public function batchPublish(Batch $request)
    {
        $this->products->batchUpdate($request->items, ['published' => 1]);

        return $this->back('Товары опубликованы!');
    }

    public function batchUnpublish(Batch $request)
    {
        $this->products->batchUpdate($request->items, ['published' => 0]);

        return $this->back('Товары опубликованы!');
    }

    public function batchPrimaryCategory(BatchPrimaryCategory $request)
    {
        $this->products->batchUpdate($request->items, ['category_id' => $request->category_id]);

        return $this->back('Категория задана!');
    }

    public function batchAttachCategories(BatchCategories $request)
    {
        $products = $this->products->byIds($request->items);

        foreach($products as $product) {
            $product = $this->products->attachCategories($product, $request->categories);
            
            try {
                $product->updateIndex();
            } catch(\Elasticsearch\Common\Exceptions\Missing404Exception $e) {
                $product->addToIndex();
            }
        }

        return $this->back('Категория добавлены к товарам!');
    }

    public function batchDetachCategories(BatchCategories $request)
    {
        $products = $this->products->byIds($request->items);

        foreach($products as $product) {
            $product = $this->products->detachCategories($product, $request->categories);
            
            try {
                $product->updateIndex();
            } catch(\Elasticsearch\Common\Exceptions\Missing404Exception $e) {
                $product->addToIndex();
            }
        }

        return $this->back('Категории у товаров удалены!');
    }

    public function batchSyncCategories(BatchCategories $request)
    {
        $products = $this->products->byIds($request->items);

        foreach($products as $product) {
            $this->products->syncCategories($product, $request->categories);
        }

        return $this->back('Категории установлены!');
    }

    protected function back($message)
    {
        return back()->withFlashSuccess($message);
    }

    public function ajaxList($id)
    {
        $data['categories'] = $this->categories->getParentsForSelect();

        $data['vendors'] = $this->categories->getVendors();

        if($id > 0) {
            $data['product'] = $this->products->findById($id);
            $data['populars'] = $data['product']->populars()->pluck('category_id')->all();
        }

        $fields = [
            'categories',
            'category_id',
            'popular',
            'vendor_id',
        ];

        $result = [];

        foreach ($fields as $n) {
            $result[$n] = view()->make('backend.product.partials.form.categories.'.$n, $data)->render();
        }

        return $result;
    }
}

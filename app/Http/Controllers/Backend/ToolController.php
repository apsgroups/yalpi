<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Image;
use App\Models\Menu;
use App\Models\MenuItem;
use App\Models\Product;
use App\Models\ProductProperty;
use App\Models\ProductSet;
use App\Models\ProductType;
use App\Models\Property;
use App\Models\PropertyValue;
use App\Services\Image\ImageService;
use App\Repositories\Backend\Category\CategoryContract;
use App\Repositories\Backend\Product\ProductContract;
use Elasticsearch\Common\Exceptions\Missing404Exception;

class ToolController extends Controller
{
    protected $categories;

    protected $products;

    public function __construct(CategoryContract $categories, ProductContract $products)
    {
        $this->categories = $categories;
        $this->products = $products;
    }
    
    public function reindexCategories()
    {
        // \App\Models\Category::deleteIndex();

        try {
            \App\Models\Category::putMapping($ignoreConflicts = true);
        } catch(Missing404Exception $e) {
            \App\Models\Category::createIndex($shards = null, $replicas = null);
            \App\Models\Category::putMapping($ignoreConflicts = true);
        }

        \App\Models\Category::reindex();

        return redirect()
            ->route('admin.dashboard')
            ->withFlashSuccess('Индексация категорий завершена!');    }
    
    public function reindex()
    {
        $page = request()->get('page', 1);

        if($page === 1) {
            try {
                \App\Models\Product::deleteIndex();
                \App\Models\Product::putMapping($ignoreConflicts = true);
            } catch(Missing404Exception $e) {
                \App\Models\Product::createIndex($shards = null, $replicas = null);
                \App\Models\Product::putMapping($ignoreConflicts = true);
            }
        }

        $limit = 100;

        $products =  \App\Models\Product::paginate($limit);
        $products->reindex();

        if($products->hasMorePages()) {
            $next = $products->currentPage() + 1;
            return 'Page '.$products->currentPage().'/'. ceil($products->total() / $limit).'<script>window.location.href="/admin/tool/reindex/?page='.$next.'"</script>';
        }

        return redirect()
            ->route('admin.dashboard')
            ->withFlashSuccess('Индексация товаров завершена!');
    }

    public function copyMenu($id, $to)
    {
        $menu = Menu::findOrFail($id);

        $this->copyMenuItems($menu, $to);
    }

    protected function copyMenuItems($menu, $to, $parent = null, $setParent = null)
    {
        $items = $menu->items()->where('parent_id', $parent)->get();

        if(!$items->count()) {
            return;
        }

        foreach($items as $item) {
            $data = [
                'title' => $item->title,
                'slug' => $item->slug.'-copy',
                'published' => $item->published,
                'type' => $item->type,
                'item_id' => $item->item_id,
                'ordering' => $item->ordering,
                'link' => $item->link,
                'user_id' => \Auth::id(),
                'menu_id' => $to,
                'parent_id' => $setParent,
                'description' => $item->description,
                'icon' => $item->icon,
                'target' => $item->target,
                'page_title' => $item->page_title,
                'bold' => $item->bold,
                'noindex' => $item->bold,
                'note' => $item->note,
                'showcase' => $item->showcase,
                'only_showcase' => $item->only_showcase,
                'bold_catalog' => $item->bold_catalog,
                'route' => $item->route,
            ];

            $types = [
                'catalog',
                'category',
                'contentType',
                'content',
                'reviews',
                'route',
            ];

            if(in_array($item->type, $types)) {
                $data['type'] = 'menu_item';
                $data['item_id'] = $item->id;
            }

            $newItem = MenuItem::create($data);

            $this->copyMenuItems($menu, $to, $item->id, $newItem->id);
        }
    }

    public function cleanCache()
    {
        try {
            \Cache::flush();
        } catch(\Exception $e) { }

        return 'ok';
    }

    public function cleanOpCache()
    {
        try {
            @opcache_reset();
        } catch(\Exception $e) { }

        return 'ok';
    }
    
    public function denormMenu()
    {
        $items = MenuItem::all();

        foreach ($items as $item) {
            $item->denorm_url = menu_url($item->id);

            $item->save();
        }
    }

    public function denormCategories()
    {
        $categories = Category::where('denorm_url', null)->orderBy('id', 'desc')->get();

        foreach ($categories as $category) {
            $category->denorm_url = category_url($category);

            $category->save();
        }
    }

    public function rootParentCategories()
    {
        $categories = Category::where('model', 1)->orderBy('id', 'desc')->get();

        foreach ($categories as $category) {
            $category->root_parent_id = $this->getRootParentId($category);

            $category->save();
        }
    }

    protected function getRootParentId($category) 
    {        
        if($category->parent) {
            return $this->getRootParentId($category->parent);
        }

        return $category->id;
    }

    public function autosale()
    {
        $limit = 1;

        $categories = Category::paginate($limit);

        if(empty($categories)) {
            return [];
        }

        $rows = [];

        foreach($categories as $category) {
            $this->categories->autoSale($category);
            $rows[$category->id] = $category->title;
        }

        if($categories->hasMorePages()) {
            $next = $categories->currentPage() + 1;
            return implode('<br />', $rows).'<br />Page '.$categories->currentPage().'/'. ceil($categories->total() / $limit).'<script>window.location.href="/admin/tool/autosale/?page='.$next.'"</script>';
        }

        return redirect()
            ->route('admin.category.index')
            ->withFlashSuccess('Распродажа обновлена!');
    }

    public function youtubeImages()
    {
        $products = Product::all();

        foreach($products as $p) {
            if(!$p->video) {
                continue;
            }

            $code = str_replace('https://www.youtube.com/watch?v=', '', $p->video);

            try {
                $imageService = new \App\Services\Image\ImageService('', $p);
                $image = $imageService->saveByUrl('https://img.youtube.com/vi/'.$code.'/maxresdefault.jpg');

                $p->image_id = $image->id;
                $p->save();
            } catch (\Exception $e) {
                try {
                    $imageService = new \App\Services\Image\ImageService('', $p);
                    $image = $imageService->saveByUrl('https://img.youtube.com/vi/'.$code.'/0.jpg');

                    $p->image_id = $image->id;
                    $p->save();
                } catch (\Exception $e) {
                    
                }
            }
        }

        return redirect()
            ->route('admin.category.index')
            ->withFlashSuccess('Изображения присвоены!');
    }
}

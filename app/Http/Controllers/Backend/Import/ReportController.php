<?php

namespace App\Http\Controllers\Backend\Import;

use App\Models\Import\ImportReport;
use App\Services\Import\Report\ReportDestroyer;
use App\Services\Import\Report\ReportFile;
use App\Http\Controllers\Controller;

class ReportController extends Controller
{
    public function download($id)
    {
        $report = ImportReport::findOrFail($id);

        $path = ReportFile::getFilepath($report);

        $name = $report->operation.'_'.$report->created_at->format('d-m-Y-H-i-s').'.csv';

        return response()->download($path, $name, ['Content-Type' => 'text/csv']);
    }

    public function destroy($id)
    {
        ReportDestroyer::destroy($id);

        return 'ok';
    }

    public function clean($operation)
    {
        $reports = ImportReport::operation($operation)->get();

        foreach($reports as $report) {
            ReportDestroyer::destroy($report->id);
        }

        return 'ok';
    }
}
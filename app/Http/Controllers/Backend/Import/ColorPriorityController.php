<?php

namespace App\Http\Controllers\Backend\Import;

use App\Http\Requests\Backend\Import\ColorPriority;
use App\Models\Import\ImportReport;
use App\Services\Import\ColorPriority\Import;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Property;
use App\Models\PropertyValue;

class ColorPriorityController extends Controller
{
    public function index()
    {
        $reports = ImportReport::operation('color-priority-import')->get();

        $propertyId = \Settings::get('body_color_id');

        $property = null;

        $propertyValues = [];

        if($propertyId) {
            $property = Property::find($propertyId);

            if($property) {
                $propertyValues = $property->values()
                    ->with('image')
                    ->orderBy('sort_priority', 'desc')
                    ->get();
            }
        }

        return view('backend.import.color-priority.index', compact('reports', 'property', 'propertyValues'));
    }

    public function upload(ColorPriority $request)
    {
        $import = new Import();

        $import->run();

        return redirect()->route('admin.import.color-priority.index')->withFlashSuccess('Импорт завершен!');
    }
}
<?php

namespace App\Http\Controllers\Backend\Import;

use App\Http\Requests\Backend\Import\Articul;
use App\Models\Import\ImportReport;
use App\Services\Import\Articul\Export;
use App\Services\Import\Articul\Import;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ArticulController extends Controller
{
    public function index()
    {
        $reports = ImportReport::operation('articul-import')->get();

        return view('backend.import.articul.index', compact('reports'));
    }

    public function upload(Articul $request)
    {
        $import = new Import();

        $import->run($request->save_empty);

        return redirect()->route('admin.import.articul.index')->withFlashSuccess('Импорт завершен!');
    }

    public function export()
    {
        $export = new Export();

        $export->run();

        return response()->download($export->getPath(), null, ['Content-Type' => 'text/csv']);
    }
}
<?php

namespace App\Http\Controllers\Backend\Import;

use App\Http\Requests\Backend\Import\Category;
use App\Models\Import\ImportReport;
use App\Services\Import\Category\Import;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    public function index()
    {
        $reports = ImportReport::operation('category-import')->get();

        return view('backend.import.category.index', compact('reports'));
    }

    public function upload(Category $request)
    {
        $import = new Import();

        $import->run();

        return redirect()->route('admin.import.category.index')->withFlashSuccess('Импорт завершен!');
    }
}
<?php

namespace App\Http\Controllers\Backend\Import;

use App\Http\Requests\Backend\Import\Product;
use App\Models\Import\ImportReport;
use App\Services\Import\Product\Import;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    public function index()
    {
        $reports = ImportReport::operation('product-import')->get();

        return view('backend.import.product.index', compact('reports'));
    }

    public function upload(Product $request)
    {
        $import = new Import();

        $import->run();

        return redirect()->route('admin.import.product.index')->withFlashSuccess('Импорт завершен!');
    }
}
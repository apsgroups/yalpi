<?php

namespace App\Http\Controllers\Backend\Import;

use App\Models\Import\ImportReport;
use App\Services\Import\CategoryBreadcrumbs\Import;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryBreadcrumbsController extends Controller
{
    public function index()
    {
        $reports = ImportReport::operation('category-breadcrumbs-import')->get();

        return view('backend.import.category-breadcrumbs.index', compact('reports'));
    }

    public function upload()
    {
        $import = new Import();

        $import->run();

        return redirect()->route('admin.import.category-breadcrumbs.index')->withFlashSuccess('Импорт завершен!');
    }
}
<?php

namespace App\Http\Controllers\Backend\Import;

use App\Models\Import\ImportMissing;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MissingController extends Controller
{
    public function index(Request $request)
    {
        $limit = $request->get('limit', 10);

        $articuls = ImportMissing::getQuery();

        $search = $request->search;

        if($search) {
            $articuls->where('articul', 'like', '%'.$search.'%')
                ->orWhere('name', 'like', '%'.$search.'%');
        }

        $articuls = $articuls->paginate($limit);

        $articuls->appends($request->only('search'));

        return view('backend.import.missing', compact('articuls', 'search'));
    }

    public function download()
    {
        $articuls = ImportMissing::all();

        $fp = tmpfile();

        fprintf($fp, chr(0xEF).chr(0xBB).chr(0xBF));

        foreach($articuls as $articul) {
            fputcsv($fp, [
                $articul->id,
                $articul->name,
                $articul->articul,
            ], ';');
        }

        rewind($fp);

        $content = stream_get_contents($fp);

        fclose($fp);

        $filename = 'missing_'.date('d-m-Y-H-i-s').'.csv';

        $headers = [
            'Content-type' => 'text/csv',
            'Content-Disposition' => 'attachment; filename="'.$filename.'"',
            'Content-Length' => strlen($content),
        ];

        return response($content, 200, $headers);
    }
}
<?php

namespace App\Http\Controllers\Backend\Import;

use App\Http\Requests\Backend\Import\CategoryLink;
use App\Models\Import\ImportReport;
use App\Services\Import\CategoryLink\Import;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryLinkController extends Controller
{
    public function index()
    {
        $reports = ImportReport::operation('category-link-import')->get();

        return view('backend.import.category-link.index', compact('reports'));
    }

    public function upload(CategoryLink $request)
    {
        $import = new Import();

        $import->run();

        return redirect()->route('admin.import.category-link.index')->withFlashSuccess('Импорт завершен!');
    }
}
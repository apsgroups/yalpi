<?php

namespace App\Http\Controllers\Backend\Import;

use App\Http\Requests\Backend\Import\CategoryImage;
use App\Models\Import\ImportReport;
use App\Services\Import\CategoryImage\Import;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryImageController extends Controller
{
    public function index()
    {
        $reports = ImportReport::operation('category-image-import')->get();

        return view('backend.import.category-image.index', compact('reports'));
    }

    public function upload(CategoryImage $request)
    {
        $import = new Import();

        $import->run();

        return redirect()->route('admin.import.category-image.index')->withFlashSuccess('Импорт завершен!');
    }
}
<?php

namespace App\Http\Controllers\Backend\Import;

use App\Http\Requests\Backend\Import\CategoryPriority;
use App\Models\Import\ImportReport;
use App\Services\Import\CategoryPriority\Import;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryPriorityController extends Controller
{
    public function index()
    {
        $reports = ImportReport::operation('category-priority-import')->get();

        return view('backend.import.category-priority.index', compact('reports'));
    }

    public function upload(CategoryPriority $request)
    {
        $import = new Import();

        $import->run();

        return redirect()->route('admin.import.category-priority.index')->withFlashSuccess('Импорт завершен!');
    }
}
<?php

namespace App\Http\Controllers\Backend\Import;

use App\Http\Requests\Backend\Import\Meta;
use App\Models\Import\ImportReport;
use App\Services\Import\Meta\Import;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MetaController extends Controller
{
    public function index()
    {
        $reports = ImportReport::operation('meta-import')->get();

        return view('backend.import.meta.index', compact('reports'));
    }

    public function upload(Meta $request)
    {
        $import = new Import();

        $import->run();

        return redirect()->route('admin.import.meta.index')->withFlashSuccess('Импорт завершен!');
    }
}
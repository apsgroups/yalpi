<?php

namespace App\Http\Controllers\Backend\Import;

use App\Http\Requests\Backend\Import\CategoryTag;
use App\Models\Import\ImportReport;
use App\Services\Import\CategoryTag\Import;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryTagController extends Controller
{
    public function index()
    {
        $reports = ImportReport::operation('category-tag-import')->get();

        return view('backend.import.category-tag.index', compact('reports'));
    }

    public function upload(CategoryTag $request)
    {
        $import = new Import();

        $import->run();

        return redirect()->route('admin.import.category-tag.index')->withFlashSuccess('Импорт завершен!');
    }
}
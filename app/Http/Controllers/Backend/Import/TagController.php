<?php

namespace App\Http\Controllers\Backend\Import;

use App\Http\Requests\Backend\Import\Tag;
use App\Models\Import\ImportReport;
use App\Services\Import\Tag\Import;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TagController extends Controller
{
    public function index()
    {
        $reports = ImportReport::operation('tag-import')->get();

        return view('backend.import.tag.index', compact('reports'));
    }

    public function upload(Tag $request)
    {
        $import = new Import();

        $import->run();

        return redirect()->route('admin.import.tag.index')->withFlashSuccess('Импорт завершен!');
    }
}
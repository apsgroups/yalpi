<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Batch;
use App\Models\Feedback;
use App\Services\Image\ImageDestroyer;

class FeedbackController extends Controller
{
    public function index()
    {
        $limit = request()->get('limit', 20);

        $feedbackList[''] = 'Выберите';

        foreach(config('site.feedbacks') as $name) {
            $feedbackList[$name] = trans('feedback.'.$name.'.title');
        }

        $filter = request()->get('filter');

        $feedbacks = Feedback::query();

        if(!empty($filter['search'])) {
            $feedbacks->where(function($query) use($filter) {
                $query->orWhere('id', $filter['search'])
                    ->orWhere('name', 'LIKE', '%'.$filter['search'].'%')
                    ->orWhere('phone', 'LIKE', '%'.$filter['search'].'%')
                    ->orWhere('email', 'LIKE', '%'.$filter['search'].'%')
                    ->orWhere('city', 'LIKE', '%'.$filter['search'].'%');

                return $query;
            });
        }

        if(!empty($filter['feedback'])) {
            $feedbacks->where('feedback', $filter['feedback']);
        }

        $feedbacks = $feedbacks->orderBy('id', 'desc')
            ->paginate($limit)
            ->appends(compact('filter', 'limit'));

        return view('backend.feedback.index', compact('feedbacks', 'feedbackList', 'filter'));
    }

    public function show($id)
    {
        $feedback = Feedback::findOrFail($id);

        return view('backend.feedback.show', compact('feedback'));
    }

    public function destroy($id)
    {
        $this->deleteFeedback($id);

        return back()->withFlashSuccess('Сообщение удалено!');
    }

    public function batchDestroy(Batch $request)
    {
        foreach($request->items as $id) {
            $this->deleteFeedback($id);
        }

        return back()->withFlashSuccess('Сообщения удалены!');
    }

    protected function deleteFeedback($id)
    {
        $feedback = Feedback::find($id);

        if(empty($feedback)) {
            return;
        }

        if($feedback->multiupload && $feedback->multiupload->images->count()) {
            foreach($feedback->multiupload->images as $image) {
                try {
                    ImageDestroyer::destroy($image->id);
                } catch(\InvalidArgumentException $e) { }
            }

            $feedback->multiupload->delete();
        }

        $feedback->delete();
    }
}

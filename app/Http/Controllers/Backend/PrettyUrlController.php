<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Backend\Traits\RouteAfterSaveTrait;
use App\Models\PrettyUrl;
use App\Models\Property;
use App\Models\PropertyValue;
use App\Repositories\Backend\Category\CategoryContract;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PrettyUrlController extends Controller
{
    use RouteAfterSaveTrait;

    protected $categories;

    public function __construct(CategoryContract $categoryRepository)
    {
        $this->categories = $categoryRepository;
    }

    public function index()
    {
        $limit = request()->get('limit', 10);

        $model = PrettyUrl::query();

        $filter = request()->get('filter', []);

        if(!empty($filter['search'])) {
            $search = $filter['search'];
            $model->where(function($query) use($search) {
                $query->where('pretty_url', 'LIKE', '%'.$search.'%')
                    ->orWhere('id', $search)
                    ->orWhere('page_title', 'LIKE', '%'.$search.'%')
                    ->orWhere('metadesc', 'LIKE', '%'.$search.'%')
                    ->orWhere('metakeys', 'LIKE', '%'.$search.'%')
                    ->orWhere('heading', 'LIKE', '%'.$search.'%');
            });
        }

        if(!empty($filter['category_id'])) {
            $model->where('category_id', $filter['category_id']);
        }

        $items = $model->paginate($limit);

        $appends = [];

        if(!empty($filter)) {
            $appends['filter'] = $filter;
        }

        if(!empty(request()->get('limit'))) {
            $appends['limit'] = $limit;
        }

        if(!empty($appends)) {
            $items->appends($appends);
        }

        $categories = $this->categories->getParentsForSelect();

        return view('backend.pretty-url.index', compact('items', 'categories', 'filter'));
    }

    public function create()
    {
        $categories = $this->categories->getParentsForSelect();

        $properties = Property::filter(1)->get();

        $options = PropertyValue::whereIn('property_id', $properties->pluck('id'))->get()->pluck('title', 'id')->toArray();

        $activeOptions = [];

        return view('backend.pretty-url.create', compact('categories', 'options', 'activeOptions'));
    }

    public function store(Requests\Backend\PrettyUrl\Save $request)
    {
        $data = $request->all();

        natsort($data['options']);

        $data['options'] = implode(',', $data['options']);

        $item = PrettyUrl::create($data);

        return redirect($this->routeAfterSave('pretty-url', [$item->id]))
            ->withFlashSuccess('URL для фильтра создан!');
    }

    public function edit($id)
    {
        $item = PrettyUrl::findOrFail($id);

        $categories = $this->categories->getParentsForSelect();

        $properties = Property::filter(1)->get();

        $options = PropertyValue::whereIn('property_id', $properties->pluck('id'))->get()->pluck('title', 'id')->toArray();

        $activeOptions = explode(',', $item->options);

        return view('backend.pretty-url.edit', compact('item', 'categories', 'options', 'activeOptions'));
    }

    public function update($id, Requests\Backend\PrettyUrl\Save $request)
    {
        $item = PrettyUrl::findOrFail($id);

        $data = $request->all();

        natsort($data['options']);

        $data['options'] = implode(',', $data['options']);

        $item->update($data);

        return redirect($this->routeAfterSave('pretty-url', [$id]))
            ->withFlashSuccess('URL сохранен');
    }

    public function destroy($id)
    {
        PrettyUrl::destroy($id);

        return back()->withSuccessMessage('Элемент удален!');
    }
}

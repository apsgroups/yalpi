<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Backend\Traits\ToggleTrait;
use App\Models\Menu;

use App\Http\Requests\Backend\Menu\Create;
use App\Http\Controllers\Controller;

class MenuController extends Controller
{
    use ToggleTrait;

    protected $modelName = '\App\Models\Menu';

    public function index()
    {
        $limit = request()->get('limit', 10);
        $menus = Menu::paginate($limit);
        return view('backend.menu.index', compact('menus'));
    }

    public function create()
    {
        return view('backend.menu.create');
    }

    public function store(Create $request)
    {
        $attributes = $request->all();
        $attributes['user_id'] = auth()->id();

        $menu = Menu::create($attributes);

        return redirect()
            ->route('admin.menu.index', [$menu->id])
            ->withFlashSuccess('Меню создано');
    }

    public function edit($id)
    {
        $menu = Menu::findOrFail($id);

        return view('backend.menu.edit', compact('menu'));
    }

    public function update(Create $request, $id)
    {
        $menu = Menu::findOrFail($id);

        $menu->update($request->all());

        return redirect()
            ->route('admin.menu.edit', [$menu->id])
            ->withFlashSuccess('Меню сохранено');
    }

    public function destroy($id)
    {
        $menu = Menu::findOrFail($id);

        try {
            $itemsCount = $menu->items()->count();
            if($itemsCount > 0) {
                throw new \Exception('Меню содержит элементы - '.$itemsCount.' шт.');
            }

            $widgetsCount = $menu->widgets()->count();
            if($widgetsCount > 0) {
                throw new \Exception('На меню ссылаются виджеты - '.$widgetsCount.' шт.');
            }

            $menu->delete();
        } catch (\Exception $e) {
            return back()->withFlashDanger($e->getMessage());
        }

        return back()->withFlashSuccess('Меню удалено!');
    }
}

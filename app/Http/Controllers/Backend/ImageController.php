<?php

namespace App\Http\Controllers\Backend;

use App\Models\Category;
use App\Models\Image;
use App\Models\Product;
use App\Services\Image\ImageDestroyer;
use App\Services\Image\ImageService;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ImageController extends Controller
{
    public function upload(Request $request)
    {
        if(!$request->hasFile('qqfile'))
            return response()->json(['success' => false]);

        $model = app()->make($request->input('model'))::findOrFail($request->input('model_id'));

        $image = new ImageService('qqfile', $model);
        $image->upload();

        return response()->json(['success' => true]);
    }

    public function destroy(Request $request)
    {
        try {
            ImageDestroyer::destroy($request->id);
        } catch(\Exception $e) {

        }

        return;
    }
}
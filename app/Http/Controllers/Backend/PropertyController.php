<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Backend\Traits\RouteAfterSaveTrait;
use App\Http\Controllers\Backend\Traits\ToggleTrait;
use App\Http\Requests\Backend\Property\Save;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\ProductType;
use App\Repositories\Backend\Property\PropertyContract;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class PropertyController extends Controller
{
    use ToggleTrait, RouteAfterSaveTrait;

    protected $modelName = '\App\Models\Property';

    protected $properties;

    public function __construct(PropertyContract $properties)
    {
        $this->properties = $properties;
    }

    public function index()
    {
        $filter = Input::get('filter', []);

        $limit = request()->get('limit', 10);

        $orderBy = request('sort', 'id');
        $orderDir = request('dir', 'asc');

        if($orderBy === 'ordering') {
            $orderDir = 'asc';
            $limit = 500;
        }

        if (!empty($filter)) {
            if (isset($filter['productTypes'])) {
                $this->properties->setProductTypesFilter($filter['productTypes']);
            }

            if (isset($filter['title'])) {
                $this->properties->setPropertyTitleFilter($filter['title']);
            }

            if (isset($filter['value'])) {
                $this->properties->setPropertyValueFilter($filter['value']);
            }

            $this->properties->setBooleanFilter(collect($filter)->only(['filter', 'published'])->toArray());
        }

        $properties = $this->properties->setSort($orderBy, $orderDir )->paginate($limit);
        $productTypes = ProductType::lists('title', 'id')->sort()->prepend('Не указан', 0);

        return view('backend.property.index', compact('properties', 'productTypes', 'filter'));
    }

    public function create()
    {
        $productTypes = ProductType::lists('title', 'id')->sort();
        $productTypesSelected = [];

        return view('backend.property.create', compact('productTypes', 'productTypesSelected'));
    }

    public function store(Save $request)
    {
        try {
            $property = $this->properties->create($request->all());
        } catch (\Exception $e) {
            return back()->withFlashDanger($e->getMessage());
        }

        return redirect($this->routeAfterSave('property', [$property->id]))->withFlashSuccess('Свойство создано!');
    }

    public function edit($id)
    {
        $property = $this->properties->findById($id);
        $productTypes = ProductType::lists('title', 'id')->sort();
        $productTypesSelected = $property->productTypes->pluck('id')->all();

        return view('backend.property.edit', compact('property', 'productTypes', 'productTypesSelected'));
    }

    public function update(Save $request, $id)
    {
        try {
            $this->properties->update($id, $request->all());
        } catch (\Exception $e) {
            return back()->withFlashDanger($e->getMessage());
        }

        return redirect($this->routeAfterSave('property', [$id]))->withFlashSuccess('Свойство сохранено!');
    }

    public function destroy($id)
    {
        try {
            $this->properties->destroy($id);
        } catch (\Exception $e) {
            return back()->withFlashDanger($e->getMessage());
        }

        return back()->withFlashSuccess('Свойство удалено!');
    }

    public function loadManager(Request $request)
    {
        $view = 'backend.property.elements.'.$request->input('type').'.manage';

        if(!view()->exists($view)) {
            abort(404);
        }

        return view($view);
    }

    public function byTitle() {
        $title = request()->get('title', '');

        return $this->properties->setPropertyTitleFilter($title)->get();
    }

    public function byValue() {
        $value = request()->get('value', '');

        return $this->properties->setPropertyValueFilter($value)->get();
    }

    public function sort()
    {
        foreach(Input::get('item') as $ordering => $id) {
            \DB::table('properties')
                ->where('id', $id)
                ->update(['ordering' => $ordering]);
        }

        return 'ok';
    }
}

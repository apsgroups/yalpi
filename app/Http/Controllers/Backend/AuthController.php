<?php

namespace App\Http\Controllers\Backend;

use App\Events\Frontend\Auth\UserRegistered;
use App\Http\Controllers\Controller;
use App\Http\Requests\Frontend\Auth\RegisterRequest;
use App\Services\Access\Traits\ConfirmUsers;
use App\Services\Access\Traits\UseSocialite;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use App\Repositories\Frontend\User\UserContract;
use App\Services\Access\Traits\AuthenticatesAndRegistersUsers; 

/**
 * Class AuthController
 * @package App\Http\Controllers\Frontend\Auth
 */
class AuthController extends Controller
{

    use AuthenticatesAndRegistersUsers, ConfirmUsers, ThrottlesLogins, UseSocialite;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/admin/product/';

    /**
     * Where to redirect users after they logout
     *
     * @var string
     */
    protected $redirectAfterLogout = '/admin/';

    /**
     * @param UserContract $user
     */
    public function __construct(UserContract $user)
    {
        $this->user = $user;
    } 

    public function showLoginForm()
    { 
        $view = 'backend.auth.login';

        if(\Request::ajax()) {
            $view = 'backend.auth.login_form';
        }

        return view($view)
            ->withSocialiteLinks($this->getSocialLinks());
    }

    public function showRegistrationForm()
    {
        $prefix = '';

        $member = request()->input('member');

        if ($member === 'wholesaler') {
            $prefix = '_'.$member;
        }

        return view('backend.auth.register'.$prefix);
    }

    public function register(RegisterRequest $request)
    {
        $data = $request->all();

        if(!isset($data['member'])) {
            $data['member'] = null;
        }

        if($data['member'] === 'wholesaler') {
            $data['password'] = str_random(8);
        }

        if (config('access.users.confirm_email') && config('access.users.forced_auth') === false) {
            $user = $this->user->create($data, false, $data['member']);
            event(new UserRegistered($user));
            return redirect()->route('frontend.index')->withFlashSuccess(trans('exceptions.frontend.auth.confirmation.created_confirm'));
        } else {
            auth()->login($this->user->create($data, false, $data['member'] ?? null));
            event(new UserRegistered(access()->user()));
            return redirect($this->redirectPath());
        }
    }
}
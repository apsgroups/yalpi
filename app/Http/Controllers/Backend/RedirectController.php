<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Backend\Traits\RouteAfterSaveTrait;
use App\Http\Requests\Backend\Batch;
use App\Http\Requests\Backend\Redirect\Save;
use App\Http\Requests\Backend\Redirect\Upload;
use App\Models\Content;
use App\Models\Redirect;

use App\Http\Controllers\Controller;
use App\Services\Redirect\Url;
use Illuminate\Database\Eloquent\Model;

class RedirectController extends Controller
{
    use RouteAfterSaveTrait;

    public function index()
    {
        $limit = request()->get('limit', 20);
        $filter = request()->get('filter', ['search' => '']);

        $redirects = Redirect::query();

        if(!empty($filter['search'])) {
            $redirects->where(function($query) use($filter) {
                $query->where('from_url', 'LIKE', '%'.$filter['search'].'%')
                    ->orWhere('to_url', 'LIKE', '%'.$filter['search'].'%')
                    ->orWhere('id', '=', $filter['search']);
            });
        }

        $redirects = $redirects->with('item')->orderBy('id', 'desc')->paginate($limit);
        $redirects->appends(request()->all());

        return view('backend.redirect.index', compact('redirects', 'limit', 'filter'));
    }

    public function create()
    {
        return view('backend.redirect.create');
    }

    public function store(Save $request)
    {
        $model = app()->make($request->item_type);

        if($model instanceof Redirect) {
            $redirect = $model->create($request->all());
        } else {
            $model = $model->findOrFail($request->item_id);
            $redirect = $model->redirect()->create(['from_url' => $request->from_url]);
        }

        return redirect($this->routeAfterSave('redirect', [$redirect->id]))
            ->withFlashSuccess('Редирект сохранен!');
    }

    public function edit($id)
    {
        $redirect = Redirect::findOrFail($id);

        return view('backend.redirect.edit', compact('redirect'));
    }

    public function update(Save $request, $id)
    {
        $redirect = Redirect::findOrFail($id);

        $attr = $request->all();

        $redirect->update($attr);

        return redirect($this->routeAfterSave('redirect', [$id]))
            ->withFlashSuccess('Редирект сохранен!');
    }

    public function destroy($id)
    {
        Redirect::destroy($id);

        return back()->withFlashSuccess('Редирект удален!');
    }

    public function batchDestroy(Batch $request)
    {
        foreach($request->items as $id) {
            Redirect::destroy($id);
        }

        \Cache::flush();

        return back()->withFlashSuccess('Редиректы удалены!');
    }

    public function upload(Upload $request)
    {
        $file = $request->file('urls')->openFile();
        $content = $file->fread($file->getSize());
        $urls = explode("\n", $content);

        $created = 0;
        $skip = 0;

        foreach($urls as $url) {
            $url = trim($url);

            if($url == '') {
                $skip++;
                continue;
            }

            $row = str_getcsv($url, ';');

            $url = trim($row[0]);

            Redirect::where('from_url', ltrim($url, '/'))->delete();

            if(!empty($row[1])) {       
                $redirectTo = $row[1];         
            } else {    
                $redirectTo = Url::find($url);

                if($redirectTo instanceof Model) {
                    if($redirectTo->link() === $url) {
                        $skip++;
                        continue;
                    }

                    $redirectTo->redirect()->create([
                        'from_url' => ltrim($url, '/'),
                    ]);

                    $created++;

                    continue;
                }
            }

            $model = Redirect::create([
                'item_type' => Redirect::class,
                'from_url' => ltrim($url, '/'),
                'to_url' => $redirectTo,
            ]);
            
            $created++;
        }

        return redirect(route('admin.redirect.index'))->withFlashSuccess('Пропущено: '.$skip.', Создано: '.$created);
    }
}

<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Backend\Traits\RouteAfterSaveTrait;
use App\Http\Controllers\Backend\Traits\ToggleTrait;
use App\Models\MarketCategory;
use App\Models\Meta;
use App\Models\PropertyValue;
use App\Models\Property;
use App\Services\Image\Image;
use Illuminate\Http\Request;

use App\Http\Requests\Backend\Category\CreateRequest;
use App\Http\Controllers\Controller;

use App\Repositories\Backend\Category\CategoryContract;

class CategoryController extends Controller
{
    use ToggleTrait, RouteAfterSaveTrait;

    protected $modelName = '\App\Models\Category';

    protected $categories;

    public function __construct(CategoryContract $categories)
    {
        $this->categories = $categories;
    }

    public function index()
    {
        $categories = $this->categories->tree();

        return view('backend.category.index', compact('categories'));
    }

    public function create()
    {
        $parents = $this->categories->getParentsForSelect();

        $marketCategories = ['выбрать'] + MarketCategory::all()->pluck('title', 'id')->toArray();

        $positionList = config('backend.category.positions');

        $positions = array_combine($positionList, $positionList);

        $types = config('backend.category.types');

        $childTemplates = config('backend.category.child_templates');

        $meta = ['выбрать'] + Meta::all()->pluck('title', 'id')->toArray();

        $populars = [];

        $selectArticles = \App\Models\Content::ofType('articles')->get()->pluck('title', 'id')->toArray();

        $selectFaq = \App\Models\Content::ofType('faq')->get()->pluck('title', 'id')->toArray();

        $properties = Property::filter(1)->get()->pluck('title', 'id')->toArray();

        $rangeProperties = Property::where('type', 'double')->filter(1)->get();

        $propertyValuesList = PropertyValue::with(['property' => function($query) {
            $query->where('filter', 1);
        }])->get();
        
        $propertyValues = [];

        foreach ($propertyValuesList as $key => $value) {
            if(!$value->property) {
                continue;
            }
            
            $propertyValues[$value->id] = $value->property->title.': '.$value->title;
        }

        $activeDisabledOptions = [];

        $activeFilterOptions = [];

        $filtersActivated = [];

        $filtersDeactivated = [];

        $tags = collect([]);

        $shippingRates = config('site.shipping_rate', []);
        $shippingRates[0] = 'выбрать';

        return view('backend.category.create', compact(
            'parents',
            'marketCategories',
            'positions',
            'types',
            'meta',
            'childTemplates',
            'populars',
            'propertyValues',
            'activeDisabledOptions',
            'activeFilterOptions',
            'shippingRates',
            'selectArticles',
            'selectFaq', 
            'rangeProperties',
            'properties',
            'filtersActivated',
            'filtersDeactivated',
            'tags'
        ));
    }

    public function store(CreateRequest $request)
    {
        try {
            $category = $this->categories->create($request->all());
        } catch(\Exception $e) {
            return redirect(route('admin.category.create'))->withFlashDanger($e->getMessage());
        }

        return redirect($this->routeAfterSave('category', [$category->id]))->withFlashSuccess('Категория сохранена!');
    }

    public function edit($id)
    {
        $category = $this->categories->findById($id);
        $parents = $this->categories->getParentsForSelect();

        if(!$category) {
            abort(404);
        }

        $populars = $category->populars()->orderBy('ordering', 'asc')->pluck('category_id')->all();

        $marketCategories = ['выбрать'] + MarketCategory::all()->pluck('title', 'id')->toArray();

        $positionList = config('backend.category.positions');

        $positions = array_combine($positionList, $positionList);

        $types = config('backend.category.types');

        $meta = ['выбрать'] + Meta::all()->pluck('title', 'id')->toArray();

        $childTemplates = config('backend.category.child_templates');

        $rangeProperties = Property::where('type', 'double')->filter(1)->get();

        $propertyValuesList = PropertyValue::with(['property' => function($query) {
            $query->where('filter', 1);
        }])->get();
        
        $propertyValues = [];

        foreach ($propertyValuesList as $key => $value) {
            if(!$value->property) {
                continue;
            }

            $propertyValues[$value->id] = $value->property->title.': '.$value->title;
        }

        if($category->filters_activate) {
            $properties = Property::filter(1)
                ->orderByRaw(\DB::raw('FIELD(id, '.implode(',', $category->filters_activate).')'))
                ->get()->pluck('title', 'id')->toArray();
        } else {
            $properties = Property::filter(1)->get()->pluck('title', 'id')->toArray();
        }

        $activeDisabledOptions = $category->disabled_options;

        $activeFilterOptions = $category->filter_options;

        $shippingRates = config('site.shipping_rate', []);
        $shippingRates[0] = 'выбрать';

        $selectArticles = \App\Models\Content::ofType('articles')->get()->pluck('title', 'id')->toArray();

        $selectFaq = \App\Models\Content::ofType('faq')->get()->pluck('title', 'id')->toArray();

        $tags = $category->tags()->published(1)->with('tagable')->orderBy('taggables.ordering', 'ASC')->get()->pluck('title', 'id');

        $filtersActivated = $category->filters_activate;

        $filtersDeactivated = $category->filters_deactivate;

        return view('backend.category.edit', compact(
            'category',
            'parents',
            'populars',
            'marketCategories',
            'positions',
            'types',
            'meta',
            'childTemplates',
            'propertyValues', 
            'activeDisabledOptions',
            'activeFilterOptions',
            'shippingRates',
            'selectArticles',
            'selectFaq',
            'rangeProperties',
            'tags',
            'properties',
            'filtersActivated',
            'filtersDeactivated'
        ));
    }

    public function update(CreateRequest $request, $id)
    {
        try {
            $this->categories->update($id, $request->all());
        } catch(\Exception $e) {
            return redirect(route('admin.category.edit', [$id]))->withFlashDanger($e->getMessage());
        }

        return redirect($this->routeAfterSave('category', [$id]))->withFlashSuccess('Категория сохранена!');
    }

    public function sortTree(Request $request)
    {
        $this->categories->sortTree($request->input('data'));

        return response()->json(['status' => 'OK']);
    }

    public function destroy($id)
    {
        try {
            $this->categories->destroy($id);
        } catch (\Exception $e) {
            return back()->withFlashDanger($e->getMessage());
        }

        return back()->withFlashSuccess('Категория удалена!');
    }

    public function toggle(Request $request, $id)
    {
        $field = $request->input('field');

        try {
            $model = app()->make($this->modelName)->findOrFail($id);

            $model->setAttribute($request->input('field'), $request->input('value'));
            $model->save();

            if($field === 'autosale') {
                $this->categories->autoSale($model);
            }

            try {
                $model->updateIndex();
            } catch(\Elasticsearch\Common\Exceptions\Missing404Exception $e) {
                $model->addToIndex();
            }
        } catch(\Exception $e) {
            return response()->json(['status' => 'fail', 'msg' => $e->getMessage()]);
        }

        return response()->json(['status' => 'success', 'msg' => 'Состояние изменено', 'state' => $model->getAttribute($request->input('field'))]);
    }
}

<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\Backend\Widget\Save;
use App\Models\Widgets\Widget;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Backend\Traits\ToggleTrait;
use App\Http\Controllers\Backend\Traits\RouteAfterSaveTrait;
use App\Repositories\Backend\Widget\WidgetContract;
use App\Services\Widget\WidgetAssign;
use App\Services\Widget\Form\WidgetFormCollection;
use App\Services\Widget\WidgetPosition;
use App\Services\Widget\WidgetType;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class WidgetController extends Controller
{
    use ToggleTrait, RouteAfterSaveTrait;

    protected $modelName = '\App\Models\Widgets\Widget';

    protected $widgetContract;

    public function __construct(WidgetContract $widgetContract)
    {
        $this->widgetContract = $widgetContract;
    }

    public function index(Request $request)
    {
        $limit = $request->get('limit', config('backend.limit', 20));

        $sort = $request->get('sort', 'id');
        $dir = $request->get('dir', 'desc');

        $widgets = Widget::orderBy($sort, $dir);

        $filter = $request->get('filter', []);
        if(!empty($filter)) {
            if($filter['search'] !== '') {
                $widgets = $widgets->search($filter['search']);
            }

            $filterBy = ['position', 'type', 'published'];
            $filtering = array_only($request->input('filter'), $filterBy);

            foreach($filtering as $k => $value) {
                if($value === '') {
                    unset($filtering[$k]);
                }
            }

            $widgets = $widgets->where($filtering);
        }

        $filterDefault = ['position' => null, 'type' => null, 'published' => null, 'search' => null];

        $filter = array_merge($filterDefault, $filter);

        $widgets = $widgets->paginate($limit);

        $widgets->appends(['limit' => $limit]);
        $widgets->appends($filter);

        $types = ['' => 'Все'] + config('widget.types');

        $positions = ['' => 'Все'] + config('widget.positions');

        return view('backend.widget.index', compact('widgets', 'types', 'positions', 'filter'));
    }

    public function create()
    {
        $positions = (new WidgetPosition())->all();
        $types = (new WidgetType())->all();

        $assignItems = (new WidgetAssign())->getItems();

        $forms = (new WidgetFormCollection())->create();

        return view('backend.widget.create', compact('types', 'positions', 'assignItems', 'forms'));
    }

    public function store(Save $request)
    {
        try {
            $widget = $this->widgetContract->create($request->all());
        } catch(\Exception $e) {
            return back()
                ->withInput()
                ->withFlashDanger('Не удалось сохранить: '.$e->getMessage());
        }

        return redirect($this->routeAfterSave('widget', [$widget->id]))
            ->withFlashSuccess('Виджет создан и сохранен!');
    }

    public function edit($id)
    {
        $widget = $this->widgetContract->findById($id);

        $positions = (new WidgetPosition())->all();
        $types = (new WidgetType())->all();

        $assignItems = (new WidgetAssign())->getItems();

        $forms = (new WidgetFormCollection())->edit($widget);

        return view('backend.widget.edit', compact('widget', 'types', 'positions', 'assignItems', 'forms'));
    }

    public function update(Save $request, $id)
    {
        try {
            $this->widgetContract->update($id, $request->all());
        } catch(\Exception $e) {
            return back()->withInput()
                ->withFlashDanger('Не удалось сохранить: '.$e->getMessage());
        }

        return redirect($this->routeAfterSave('widget', [$id]))
            ->withFlashSuccess('Виджет сохранен!');
    }

    public function destroy($id)
    {
        try {
            $this->widgetContract->destroy($id);
        } catch (ModelNotFoundException $e) {
            return back()->withFlashDanger('Виджет не найден!');
        }

        return back()->withFlashSuccess('Виджет удален!');
    }

    public function sort(Request $request)
    {
        $this->widgetContract->sort($request->items);
    }
}

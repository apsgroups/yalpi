<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use App\Services\Discount\VirtualDiscount;
use Illuminate\Http\Request;

class VirtualDiscountController extends Controller
{
    const PER_PAGE = 5;

    public function index()
    {
        return view('backend.virtual-discounts.index');
    }

    public function run(Request $request)
    {
        set_time_limit(180);

        $categories = $this->categoryIds();

        if(empty($categories)) {
            $this->refreshStatus();
            $this->cleanDiscounts([]);

            try {
                \Cache::flush();
            } catch(\Exception $e) { }

            return [
                'error' => 'Нет категорий для виртуальных скидок'
            ];
        }

        $page = (int)$request->get('page', 1);
        $step = (int)$request->get('step', 1);

        if($step === 1 && $page === 1) {
            $this->refreshStatus();
            $this->cleanDiscounts($categories);
        }

        if($step === 1) {
            $products = $this->getProducts($categories);
        } else {
            $products = $this->getModules($categories);
        }

        if($products) { 
            foreach($products as $product) {
                if($step === 1 && !$product->moduleParents->isEmpty()) {
                    continue;
                }

                $discount = new \App\Services\Discount\VirtualDiscount($product);
                $discount->apply();
            }
        }

        if($step === 2 && !$products->hasMorePages()) {
            $this->sendEmail();

            try {
                \Cache::flush();
            } catch(\Exception $e) { }
        }

        return [
            'per_page' => self::PER_PAGE,
            'total' => $products->total(),
        ];
    }

    protected function sendEmail()
    {
        $added = Product::where('virtual_discount_status', VirtualDiscount::STATUS_ADDED)->get();
        $updated = Product::where('virtual_discount_status', VirtualDiscount::STATUS_UPDATED)->get();

        if($added->isEmpty() && $updated->isEmpty()) {
            return;
        }

        \Mail::send('emails.virtual-discounts.manager', compact('added', 'updated'), function ($message) {
            $message->from(config('mail.from.address'), config('mail.from.name'));

            $message->to(\Settings::get('virtual_discount_email'))
                ->subject(\Settings::get('virtual_discount_subject'));
        });
    }

    protected function refreshStatus()
    {
        \DB::table('products')->update(['virtual_discount_status' => 0]);
    }

    protected function cleanDiscounts($categories)
    {
        if(empty($categories)) { 
            $products = Product::whereNotIn('category_id', $categories)
                ->where('virtual_discount', '>', 0)
                ->get();
        } else {
            $products = Product::where('virtual_discount', '>', 0)
                ->get();
        }

        if($products->isEmpty()) {
            return;
        }

        foreach($products as $product) {
            $sale = ($product->virtual_discount > 0 || $product->discount > 0) ? 1 : 0;

            $product->update([
                'virtual_discount' => 0,
                'virtual_discount_id' => 0,
                'sale' => $sale,
            ]);

            $product->addToIndex();
        }
    }

    protected function categoryIds()
    {
        return Category::where('virtual_discounts', 1)
            ->get()
            ->pluck('id')
            ->toArray();
    }

    protected function getModules($categories)
    {
        return Product::whereIn('category_id', $categories)
            ->has('moduleParents')
            ->with('moduleParents')
            ->orderBy('id')
            ->paginate(self::PER_PAGE);
    }

    protected function getProducts($categories)
    {
        return Product::whereIn('category_id', $categories)
            ->with('moduleParents')
            ->orderBy('id')
            ->paginate(self::PER_PAGE);
    }
}

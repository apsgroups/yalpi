<?php

namespace App\Http\Controllers\Backend;

use App\Models\Review;
use App\Models\ReviewResponse;
use Illuminate\Http\Request;

use App\Http\Requests\Backend\ReviewResponse\StoreRequest;
use App\Http\Controllers\Controller;

class ReviewResponseController extends Controller
{
    public function create($id)
    {
        $review = Review::findOrFail($id);

        return view('backend.review-response.create', compact('review'));
    }

    public function store(StoreRequest $request, $id)
    {
        $review = Review::findOrFail($id);

        try {
            $attributes = $request->all();
            $attributes['user_id'] = auth()->id();

            $response = $review->response()->create($attributes);
        } catch (\Exception $e) {
            return back()->withInput()->withFlashDanger($e->getMessage());
        }

        return redirect()
            ->route('admin.review-response.edit', [$response->id])
            ->withFlashSuccess('Ответ добавлен!');
    }

    public function edit($id)
    {
        $response = ReviewResponse::findOrFail($id);
        $review = $response->review;

        return view('backend.review-response.edit', compact('response', 'review'));
    }

    public function update(StoreRequest $request, $id)
    {
        $review = ReviewResponse::findOrFail($id);

        try {
            $review->update($request->all());
        } catch (\Exception $e) {
            return back()->withFlashDanger($e->getMessage());
        }

        return redirect()
            ->route('admin.review-response.edit', [$review->id])
            ->withFlashSuccess('Ответ сохранен!');
    }
}
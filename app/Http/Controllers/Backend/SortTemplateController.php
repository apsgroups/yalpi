<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Backend\Traits\RouteAfterSaveTrait;
use App\Models\Category;
use App\Models\Product;
use App\Models\SortTemplate;
use Illuminate\Http\Request;
use App\Repositories\Backend\Category\CategoryContract;

use App\Http\Requests\Backend\SortTemplate\Save;
use App\Http\Controllers\Controller;

class SortTemplateController extends Controller
{
    use RouteAfterSaveTrait;

    protected $categoryContract;

    public function __construct(CategoryContract $categoryContract)
    {
        $this->categoryContract = $categoryContract;
    }

    public function index()
    {
        $items = SortTemplate::paginate(20);

        $categories = $this->categoryContract->getParentsForSelect();

        $sortTemplates = SortTemplate::all()->pluck('title', 'id');

        return view('backend.sort-templates.index', compact('items', 'categories', 'sortTemplates'));
    }

    public function assign(Request $request)
    {
        set_time_limit(300);

        $sortTemplate = SortTemplate::findOrFail($request->sort_template_id);

        foreach ($request->category_id as $id) {            
            $toCategory = Category::find($id);

            if(!$toCategory) {
                continue;
            }

            $position = 0;

            $limitProducts = $toCategory->products()
                ->published(1)
                ->orderBy('pivot_ordering', 'asc')
                ->get();

            $names = [];

            if(!$limitProducts->count()) {
                return back()->withFlashDanger('Категория пуста');
            }

            \DB::table('category_product')->where(['category_id' => $toCategory->id])->update(['ordering' => \DB::raw('ordering+1000')]);

            foreach($sortTemplate->categories()->orderBy('pivot_ordering', 'asc')->get() as $category) {
                $products = $category->products()
                    ->published(1)
                    ->where('in_stock', '>', 0)
                    ->whereIn('products.id', $limitProducts->pluck('id')->toArray())
                    ->orderBy('pivot_ordering', 'asc')
                    ->take($category->pivot->items)
                    ->get();

                if(!$products->count()) {
                    continue;
                }

                $products = $products->shuffle()->all();

                foreach($products as $k => $product) {
                    $position++;

                    \DB::table('category_product')->where(['product_id' => $product->id, 'category_id' => $toCategory->id])->update(['ordering' => $position]);

                    $names[] = $product->title.', '.$product->id.', '.$position;
                }
            }

            $toCategory->products()->chunk(50, function($items) {
                foreach($items as $p) {
                    $p->updateIndex();
                }
            });
        }

        return back()->withFlashSuccess('Шаблон сортировки применен!');
    }

    public function create()
    {
        $categories = $this->categoryContract->getParentsForSelect();

        return view('backend.sort-templates.create', compact('categories'));
    }

    public function store(Save $request)
    {
        try {
            $sortTemplate = SortTemplate::create($request->all());

            $params = [];

            foreach($request->params as $p) {
                $params[$p['category_id']] = [
                    'items' => $p['items'],
                    'ordering' => $p['ordering']
                ];
            }

            $sortTemplate->categories()->sync($params);
        } catch(\Exception $e) {
            return back()->withFlashDanger($e->getMessage());
        }

        return redirect($this->routeAfterSave('sort-template', [$sortTemplate->id]))
            ->withFlashSuccess('Шаблон сортировки создан');
    }

    public function edit($id)
    {
        $item = SortTemplate::findOrFail($id);

        $params = [];

        foreach($item->categories()->orderBy('pivot_ordering')->get() as $category) {
            $params[] = [
                'items' => $category->pivot->items,
                'ordering' => $category->pivot->ordering,
                'category_id' => $category->id,
            ];
        }

        $categories = $this->categoryContract->getParentsForSelect();

        return view('backend.sort-templates.edit', compact('item', 'categories', 'params'));
    }

    public function update(Save $request, $id)
    {
        $sortTemplate = SortTemplate::findOrFail($id);

        $params = [];

        foreach($request->params as $p) {
            $params[$p['category_id']] = [
                'items' => $p['items'],
                'ordering' => $p['ordering']
            ];
        }

        $sortTemplate->categories()->sync($params);

        try {
            $sortTemplate->update($request->all());
        } catch(\Exception $e) {
            return redirect()->back()->withFlashDanger($e->getMessage());
        }

        return redirect($this->routeAfterSave('sort-template', [$id]))
            ->withFlashSuccess('Шаблон сортировки сохранен');
    }

    public function destroy($id)
    {
        try {
            SortTemplate::destroy($id);
        } catch(\Exception $e) {
            return back()->withFlashDanger($e->getMessage());
        }

        return back()->withFlashSuccess('Шаблон сортировки удален!');
    }
}

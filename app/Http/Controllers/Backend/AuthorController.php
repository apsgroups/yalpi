<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Backend\Traits\RouteAfterSaveTrait;
use App\Models\Author;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Author\Save;
use App\Services\Image\ImageDestroyer;
use App\Services\Image\ImageService;

class AuthorController extends Controller
{
    use RouteAfterSaveTrait;

    protected $modelName = Author::class;

    public function index()
    {
        $items = Author::paginate(20);

        return view('backend.authors.index', compact('items'));
    }

    public function create()
    {
        return view('backend.authors.create');
    }

    public function store(Save $request)
    {
        try {
            $author = Author::create($request->all());
            $author = $this->saveItem($author);
        } catch(\Exception $e) {
            return back()->withFlashDanger($e->getMessage());
        }

        return redirect($this->routeAfterSave('author', [$author->id]))
            ->withFlashSuccess('Автор создан');
    }

    public function edit($id)
    {
        $item = Author::findOrFail($id);

        return view('backend.authors.edit', compact('item'));
    }

    public function update(Save $request, $id)
    {
        $author = Author::findOrFail($id);

        try {
            $author->update($request->all());
            $author = $this->saveItem($author);
        } catch(\Exception $e) {
            return redirect()->back()->withFlashDanger($e->getMessage());
        }

        return redirect($this->routeAfterSave('author', [$id]))
            ->withFlashSuccess('Автор сохранен');
    }

    public function destroy($id)
    {
        $author = Author::findOrFail($id);

        try {
            if($author->image) {                    
                ImageDestroyer::destroy($author->image->id);
            }

            Author::destroy($id);
        } catch(\Exception $e) {
            return back()->withFlashDanger($e->getMessage());
        }

        return back()->withFlashSuccess('Автор удален!');
    }

    protected function saveItem($item)
    { 
        if(request()->hasFile('image')) {
            $image = (new ImageService('image', $item))->upload();
            $item->image_id = $image->id;
            $item->save();
        }

        return $item;
    }

    public function search(Request $request)
    {
        $query = $request->input('query');

        $items = Author::where('name', 'like', '%'.$query.'%')->orWhere('slug', $query)
            ->orWhere('id', $query)
            ->take(5)
            ->get();

        $list = [];
        
        foreach($items as $item) {
            $list[] = [
                'title' => $item->name.'/'.$item->slug.'/'.$item->id,
                'id' => $item->id,
            ];
        }

        return response()->json($list);
    }
}

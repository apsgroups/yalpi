<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Backend\Traits\RouteAfterSaveTrait;
use App\Http\Controllers\Backend\Traits\ToggleTrait;
use App\Models\ContentType;
use App\Http\Requests\Backend\ContentType\Save;

use App\Http\Controllers\Controller;

class ContentTypeController extends Controller
{
    use ToggleTrait, RouteAfterSaveTrait;

    protected $modelName = '\App\Models\ContentType';

    public function index()
    {
        $items = ContentType::paginate(20);

        return view('backend.content-types.index', compact('items'));
    }

    public function create()
    {
        return view('backend.content-types.create');
    }

    public function store(Save $request)
    {
        $attributes = $request->all();
        $attributes['user_id'] = auth()->id();

        $contentType = ContentType::create($attributes);

        return redirect($this->routeAfterSave('content-type', [$contentType->id]))
            ->withFlashSuccess('Тип контента создан');
    }

    public function edit($id)
    {
        $item = ContentType::findOrFail($id);

        return view('backend.content-types.edit', compact('item'));
    }

    public function update(Save $request, $id)
    {
        $contentType = ContentType::findOrFail($id);

        $contentType->update($request->all());

        return redirect($this->routeAfterSave('content-type', [$id]))
            ->withFlashSuccess('Тип контента сохранен');
    }

    public function destroy($id)
    {
        $contentType = ContentType::findOrFail($id);

        $contentCount = $contentType->contents->count();
        if($contentCount > 0) {
            return back()->withFlashDanger('Тип связан с '.$contentCount.' элементом/ами! Удалите элементы или укажите им другой тип.');
        }

        $menuItemsCount = $contentType->menuItem->count();
        if ($menuItemsCount) {
            return back()->withFlashDanger('Тип связан с '.$menuItemsCount.' пунктом/ами меню! Удалите их.');
        }

        ContentType::destroy($id);

        return back()->withFlashSuccess('Тип контента удален!');
    }
}

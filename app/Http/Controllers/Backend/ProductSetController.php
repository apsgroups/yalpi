<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\Backend\ProductSet\Save;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\ProductSet;
use App\Models\Property;

class ProductSetController extends Controller
{
    public function search()
    {
        $query = request()->get('query');
        return ProductSet::where('title', 'like', '%'.$query.'%')
            ->take(5)
            ->get();
    }

    public function show($id)
    {
        $productSet = ProductSet::with('products')->findOrFail($id);

        $properties = Property::where('type', 'color')->get()->pluck('title', 'id');

        return view('backend.product.partials.product-set.show', compact('productSet', 'properties'));
    }

    public function store(Save $request)
    {
        $set = ProductSet::create($request->all());

        return $set;
    }

    public function update(Save $request, $id)
    {
        ProductSet::findOrFail($id)->update($request->all());
    }

    public function destroy($id)
    {
        ProductSet::destroy($id);
    }

    public function attachProduct($set, $id)
    {
        $product = Product::findOrFail($id);

        $product->update(['product_set_id' => $set]);

        return view('backend.product.partials.product-set.product', compact('product'));
    }

    public function detachProduct($id)
    {
        $product = Product::findOrFail($id);

        $product->update(['product_set_id' => null]);
    }

    public function searchProduct()
    {
        $query = request()->get('query');

        return Product::where('title', 'like', '%'.$query.'%')
            ->orWhere('slug', $query)
            ->orWhere('sku', $query)
            ->orWhere('ext_id', $query)
            ->orWhere('id', $query)
            ->take(5)
            ->get();
    }
}

<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Backend\Traits\RouteAfterSaveTrait;
use App\Models\Language;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Language\Save;

class LanguageController extends Controller
{
    use RouteAfterSaveTrait;

    protected $modelName = Language::class;

    public function index()
    {
        $items = Language::paginate(20);

        return view('backend.languages.index', compact('items'));
    }

    public function create()
    {
        return view('backend.languages.create');
    }

    public function store(Save $request)
    {
        try {
            $language = Language::create($request->all());
        } catch(\Exception $e) {
            return back()->withFlashDanger($e->getMessage());
        }

        return redirect($this->routeAfterSave('language', [$language->id]))
            ->withFlashSuccess('Язык создан');
    }

    public function edit($id)
    {
        $item = Language::findOrFail($id);

        return view('backend.languages.edit', compact('item'));
    }

    public function update(Save $request, $id)
    {
        $language = Language::findOrFail($id);

        try {
            $language->update($request->all());
        } catch(\Exception $e) {
            return redirect()->back()->withFlashDanger($e->getMessage());
        }

        return redirect($this->routeAfterSave('language', [$id]))
            ->withFlashSuccess('Язык сохранен');
    }

    public function destroy($id)
    {
        $language = Language::findOrFail($id);

        try {
            Language::destroy($id);
        } catch(\Exception $e) {
            return back()->withFlashDanger($e->getMessage());
        }

        return back()->withFlashSuccess('Язык удален!');
    }
}

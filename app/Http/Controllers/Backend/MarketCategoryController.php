<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Backend\Traits\RouteAfterSaveTrait;
use App\Models\MarketCategory;
use App\Http\Requests\Backend\MarketCategory\Save;

use App\Http\Controllers\Controller;

class MarketCategoryController extends Controller
{
    use RouteAfterSaveTrait;

    public function index()
    {
        $limit = request()->get('limit', 20);

        $categories = MarketCategory::orderBy('id', 'desc')->paginate($limit);

        return view('backend.market-categories.index', compact('categories'));
    }

    public function create()
    {
        return view('backend.market-categories.create');
    }

    public function store(Save $request)
    {
        $category = MarketCategory::create($request->all());

        return redirect($this->routeAfterSave('market-category', [$category->id]))
            ->withFlashSuccess('Категория создана');
    }

    public function edit($id)
    {
        $category = MarketCategory::findOrFail($id);

        return view('backend.market-categories.edit', compact('category'));
    }

    public function update(Save $request, $id)
    {
        $category = MarketCategory::findOrFail($id);

        $category->update($request->all());

        return redirect($this->routeAfterSave('market-category', [$id]))
            ->withFlashSuccess('Категория сохранена');
    }

    public function destroy($id)
    {
        MarketCategory::destroy($id);

        return back()->withFlashSuccess('Категория удалена!');
    }
}

<?php

namespace App\Http\Controllers\Backend\Export;

use App\Http\Requests\Backend\Export\ProductRequest;
use App\Services\Export\ProductExport;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    public function index()
    {
        return view('backend.export.product.index');
    }

    public function export(ProductRequest $request)
    {
        $export = new ProductExport();

        $path = $export->run($request->category_id);

        return response()->download($path, null, ['Content-Type' => 'text/csv']);
    }
}
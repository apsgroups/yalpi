<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Order;

class OrderController extends Controller
{
    protected $products;

    public function index()
    {
        $orders = Order::orderBy('created_at', 'desc')->paginate(20);

        return view('backend.order.index', compact('orders'));
    }

    public function edit($id)
    {
        $order = Order::with([
            'products' => function ($query) {
                $query->whereNull('parent_id');
            },
            'products.orderProductProperty',
            'shipping',
            'payment',
            'products.modules.product',
            'products.desk'
        ])->findOrFail($id);

        return view('backend.order.edit', compact('order'));
    }

    public function destroy($id)
    {
        Order::destroy($id);

        return back()->withFlashSuccess('Заказ удален!');
    }

    public function create()
    {

    }

    public function store()
    {

    }

    public function update($id)
    {

    }
}
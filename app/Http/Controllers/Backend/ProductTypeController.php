<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Backend\Traits\RouteAfterSaveTrait;
use App\Http\Controllers\Backend\Traits\ToggleTrait;
use App\Models\ProductType;
use App\Models\ProductTypeGroup;
use App\Models\Property;
use App\Repositories\Backend\Category\CategoryContract;
use Illuminate\Http\Request;

use App\Http\Requests\Backend\ProductType\Save;
use App\Http\Controllers\Controller;

class ProductTypeController extends Controller
{
    use ToggleTrait, RouteAfterSaveTrait;

    protected $categoryContract;

    protected $modelName = ProductType::class;

    public function __construct(CategoryContract $categoryContract)
    {
        $this->categoryContract = $categoryContract;
    }

    public function index()
    {
        $items = ProductType::paginate(20);

        return view('backend.product-types.index', compact('items'));
    }

    public function create()
    {
        $properties = Property::all()->pluck('title', 'id');

        $categories = $this->categoryContract->getParentsForSelect();

        return view('backend.product-types.create', compact('properties', 'categories'));
    }

    public function store(Save $request)
    {
        try {
            $productType = ProductType::create($request->all());

            $groups = $request->input('groups');
            if($groups) {
                foreach($groups as $item) {
                    $group = $productType->groups()->create($item);

                    if(isset($item['properties'])) {
                        $properties = array_where($item['properties'], function ($key, $value) {
                            return !empty($value);
                        });

                        $group->properties()->sync($properties);
                    }
                }
            }
        } catch(\Exception $e) {
            return back()->withFlashDanger($e->getMessage());
        }

        return redirect($this->routeAfterSave('product-type', [$productType->id]))
            ->withFlashSuccess('Тип товара создан');
    }

    public function edit($id)
    {
        $item = ProductType::findOrFail($id);

        $properties = Property::all()->pluck('title', 'id');

        $categories = $this->categoryContract->getParentsForSelect();

        return view('backend.product-types.edit', compact('item', 'properties', 'categories'));
    }

    public function update(Save $request, $id)
    {
        $productType = ProductType::findOrFail($id);

        $groups = $request->input('groups');

        $list = [];

        foreach($groups as $k => $row) {
            if(!empty($row['id'])) {
                $group = ProductTypeGroup::find($row['id']);
                $group->update([
                        'title' => $row['title']
                    ]);
            } else {
                $group = $productType->groups()->create([
                    'title' => $row['title']
                ]);
            }

            if(isset($row['properties'])) {
                $properties = array_where($row['properties'], function ($key, $value) {
                    return !empty($value);
                });

                $group->properties()->sync($properties);
            }

            $list[] = $group->id;
        }

        if($list) {
            $productType->groups()->whereNotIn('id', $list)->delete();
        }

        try {
            $productType->update($request->all());
        } catch(\Exception $e) {
            return redirect()->back()->withFlashDanger($e->getMessage());
        }

        return redirect($this->routeAfterSave('product-type', [$id]))
            ->withFlashSuccess('Тип товара сохранен');
    }

    public function destroy($id)
    {
        $productType = ProductType::findOrFail($id);

        $productCount = $productType->products->count();
        if($productCount > 0) {
            return back()->withFlashDanger('Тип связан с '.$productCount.' товаром/ами! Удалите товары или укажите им другой тип.');
        }

        try {
            ProductType::destroy($id);
        } catch(\Exception $e) {
            return back()->withFlashDanger($e->getMessage());
        }

        return back()->withFlashSuccess('Тип товара удален!');
    }
}

<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Backend\Traits\RouteAfterSaveTrait;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Attribute\AttributeSave;
use App\Models\Attribute;

class AttributeController extends Controller
{
    use RouteAfterSaveTrait;

    public function index()
    {
        $limit = request()->get('limit', 20);

        $attributes = Attribute::paginate($limit);

        return view('backend.attributes.index', compact('attributes'));
    }

    public function create()
    {
        return view('backend.attributes.create');
    }

    public function store(AttributeSave $request)
    {
        $attribute = Attribute::create($request->all());

        return redirect($this->routeAfterSave('attribute', [$attribute->id]))
            ->withFlashSuccess('Атрибут создан');
    }

    public function edit($id)
    {
        $attribute = Attribute::findOrFail($id);

        return view('backend.attributes.edit', compact('attribute'));
    }

    public function update(AttributeSave $request, $id)
    {
        $attribute = Attribute::findOrFail($id);

        $attribute->update($request->all());

        return redirect($this->routeAfterSave('attribute', [$id]))
            ->withFlashSuccess('Атрибут сохранен');
    }

    public function destroy($id)
    {
        try {
            Attribute::destroy($id);
        } catch(\Exception $e) {
            return back()->withFlashDanger($e->getMessage());
        }

        return back()->withFlashSuccess('Атрибут удален!');
    }
}

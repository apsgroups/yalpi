<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Backend\Traits\RouteAfterSaveTrait;
use App\Http\Controllers\Backend\Traits\ToggleTrait;
use App\Models\Content;
use App\Models\ContentType;
use App\Models\Menu;
use App\Models\MenuItem;
use App\Repositories\Backend\Category\CategoryContract;
use App\Services\Image\ImageService;
use App\Services\MenuService;

use App\Http\Requests\Backend\MenuItem\Create;
use App\Http\Requests\Backend\MenuItem\MassCreate;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class MenuItemController extends Controller
{
    use ToggleTrait, RouteAfterSaveTrait;

    protected $modelName = '\App\Models\MenuItem';

    protected $categories;

    public function __construct(CategoryContract $categories)
    {
        $this->categories = $categories;
    }

    public function index($menu)
    {
        $menu = Menu::slug($menu)->first();

        $items = (new MenuService)->getRoot($menu->id);

        return view('backend.menu-item.index', compact('menu', 'items'));
    }

    public function create($menu)
    {
        $menu = Menu::slug($menu)->first();
        $parents = (new MenuService)->getParentsForSelect($menu->id);
        $types = ['выбрать'] + config('menu.types');
        $target = config('menu.target');

        $categories = $this->getOptionsCategory();
        $contentType = $this->getOptionsContentType();
        $menuItems = $this->getOptionsMenuItems();

        $content = '';

        return view('backend.menu-item.create', compact(
            'menu',
            'parents',
            'types',
            'menuItems',
            'categories',
            'contentType',
            'content',
            'target'
        ));
    }

    public function categoryCreate($menu)
    {
        $menu = Menu::slug($menu)->first();
        $parents = (new MenuService)->getParentsForSelect($menu->id);

        $categories = $this->categories->tree();

        return view('backend.menu-item.category-create', compact(
            'menu',
            'parents',
            'categories'
        ));
    }

    public function pointerCreate($menu)
    {
        $menu = Menu::slug($menu)->first();
        $parents = (new MenuService)->getParentsForSelect($menu->id);

        $menus = Menu::all();

        foreach ($menus as $m) {
            $m->children = (new MenuService)->getRoot($m->id);
        }

        return view('backend.menu-item.pointer-create', compact(
            'menu',
            'parents',
            'menus'
        ));
    }

    public function store(Create $request, $menu)
    {
        $menu = Menu::slug($menu)->first();

        $attributes = $request->all();

        if($attributes['type'] === 'content' && !empty($attributes['content_id'])) {
            $attributes['item_id'] = $attributes['content_id'];
        }

        $attributes['parent_id'] = empty($attributes['parent_id']) ? null : $attributes['parent_id'];
        $attributes['user_id'] = auth()->id();

        $attributes['ordering'] = (int)MenuItem::where('parent_id', $attributes['parent_id'])->max('ordering');

        try {
            $item = MenuItem::create($attributes);

            if($request->hasFile('image')) {
                $image = (new ImageService('image', $item))->upload();
                $item->image_id = $image->id;
                $item->save();
            }
        } catch(\Exception $e) {
            return redirect()
                ->route('admin.menu-item.create', [$menu->slug])
                ->withInput()
                ->withFlashDanger($e->getMessage());
        }

        return redirect($this->routeAfterSave('menu-item', [$menu->slug, $item->id]))
            ->withFlashSuccess('Пункт меню сохранен');
    }

    public function categoryStore(MassCreate $request, $menu)
    {
        $menu = Menu::slug($menu)->first(); 

        $parent = empty($request->parent_id) ? null : $request->parent_id;
        
        $attributes['user_id'] = auth()->id();

        $attributes['type'] = 'category';

        $attributes['published'] = (int)$request->published;
        
        if($parent) { 
            $widgets = \App\Models\Widgets\WidgetAssign::where('menu_item_id', $parent)->get();
        }

        $categories = \App\Models\Category::all()->keyBy('id');

        $ids = [];

        foreach ($request->items as $key => $value) {
            $attributes['item_id'] = $value;
            $attributes['parent_id'] = !empty($ids[$categories[$value]->parent_id]) ? $ids[$categories[$value]->parent_id] : $parent;
            $attributes['ordering'] = (int)MenuItem::where('parent_id', $attributes['parent_id'])->max('ordering');
            $attributes['ordering']++;
            $attributes['title'] = $categories[$value]->title;
            $attributes['menu_id'] = $menu->id;
            $attributes['slug'] = $categories[$value]->slug;
        
            $item = MenuItem::create($attributes);

            if(!empty($widgets) && $widgets->count()) {
                foreach ($widgets as $w) {
                    \App\Models\Widgets\WidgetAssign::create([
                        'menu_item_id' => $item->id,
                        'widget_id' => $w->widget_id,
                    ]);
                }
            }

            $ids[$value] = $item->id;
        }

        return redirect($this->routeAfterSave('menu-item', [$menu->slug, $item->id]))
            ->withFlashSuccess('Пункт меню сохранен');
    }

    public function pointerStore(MassCreate $request, $menu)
    {
        $menu = Menu::slug($menu)->first(); 

        $parent = empty($request->parent_id) ? null : $request->parent_id;
        
        $attributes['user_id'] = auth()->id();

        $attributes['type'] = 'menu_item';

        $attributes['published'] = (int)$request->published;

        $items = MenuItem::all()->keyBy('id');

        $ids = [];

        foreach ($request->items as $key => $value) {
            $attributes['item_id'] = $value;
            $attributes['parent_id'] = !empty($ids[$items[$value]->parent_id]) ? $ids[$items[$value]->parent_id] : $parent;
            $attributes['ordering'] = (int)MenuItem::where('parent_id', $attributes['parent_id'])->max('ordering');
            $attributes['ordering']++;
            $attributes['title'] = $items[$value]->title;
            $attributes['menu_id'] = $menu->id;
            $attributes['slug'] = $items[$value]->slug.'-menu-item';
        
            $item = MenuItem::create($attributes);

            $ids[$value] = $item->id;
        }

        return redirect($this->routeAfterSave('menu-item', [$menu->slug, $item->id]))
            ->withFlashSuccess('Пункт меню сохранен');
    }

    public function edit($menu, $id)
    {
        $item = MenuItem::findOrFail($id);
        $menu = $item->menu;

        $types = ['выбрать'] + config('menu.types');
        $target = config('menu.target');

        $parents = (new MenuService)->getParentsForSelect($menu->id);

        $categories = $this->getOptionsCategory();
        $contentType = $this->getOptionsContentType();
        $menuItems = $this->getOptionsMenuItems();

        $content = '';
        if($item->type === 'content' && $item->item_id > 0) {
            $content = Content::find($item->item_id);
        }

        return view('backend.menu-item.edit', compact(
            'menu',
            'item',
            'parents',
            'types',
            'categories',
            'contentType',
            'menuItems',
            'content',
            'target'
        ));
    }

    public function update(Create $request, $menu, $id)
    {
        $item = MenuItem::findOrFail($id);
        $menu = $item->menu;

        $attributes = $request->all();

        if($attributes['type'] === 'content' && !empty($attributes['content_id'])) {
            $attributes['item_id'] = $attributes['content_id'];
        }

        $attributes['parent_id'] = empty($attributes['parent_id']) ? null : $attributes['parent_id'];
        $attributes['user_id'] = auth()->id();

        try {
            if($request->hasFile('image')) {
                $image = (new ImageService('image', $item))->upload();
                $attributes['image_id'] = $image->id;
            }

            $item->update($attributes);
        } catch(\Exception $e) {
            return redirect()
                ->route('admin.menu-item.create', [$menu->slug])
                ->withFlashDanger($e->getMessage());
        }

        return redirect($this->routeAfterSave('menu-item', [$menu->slug, $item->id]))
            ->withFlashSuccess('Пункт меню сохранен');
    }

    public function sort(Request $request)
    {
        (new MenuService())->sortTree($request->input('data'));

        return response()->json(['status' => 'OK']);
    }

    public function loadOptions()
    {
        $type = Input::get('type');

        $method = 'getOptions'.ucfirst($type);

        if(!method_exists($this, $method)) {
            abort(404);
        }

        $list = call_user_func([$this, $method]);

        return response()->json($list);
    }

    protected function getOptionsMenuItems()
    {
        $menuItems = [];

        $menus = Menu::all();
        foreach($menus as $menu) {
            $menuItems[$menu->title] = (new MenuService)->getParentsForSelect($menu->id);
        }

        return $menuItems;
    }

    protected function getOptionsCategory()
    {
        return $this->categories->getParentsForSelect();
    }

    protected function getOptionsContentType()
    {
        return ContentType::all()->pluck('title', 'id');
    }

    public function destroy($menu, $id)
    {
        try {
            (new MenuService())->destroy($id);
        } catch (\Exception $e) {
            return back()->withFlashDanger($e->getMessage());
        }

        return back()->withFlashSuccess('Пункт меню удален!');
    }
}

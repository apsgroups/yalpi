<?php

namespace App\Http\Controllers\Backend;

use App\Exceptions\GeneralException;
use App\Http\Controllers\Backend\Traits\RouteAfterSaveTrait;
use App\Services\Image\ImageDestroyer;
use App\Services\Image\ImageService;
use Illuminate\Http\Request;
use App\Models\Content;
use App\Models\ContentType;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Backend\Traits\ToggleTrait;

use App\Http\Requests\Backend\Content\Save;

class ContentController extends Controller
{
    use ToggleTrait, RouteAfterSaveTrait;

    protected $modelName = '\App\Models\Content';

    public function index($type)
    {
        $limit = request()->get('limit', 10);

        $content = ContentType::slug($type)->first();

        $items = Content::ofType($type)->orderBy('id', 'desc')->paginate($limit);

        return view($this->viewName('index', $type), compact('items', 'content'));
    }

    public function create($type)
    {
        $content = ContentType::slug($type)->first();

        $selectedTags = [];

        return view($this->viewName('create', $type), compact('content', 'selectedTags'));
    }

    public function store(Save $request, $slug)
    {
        $attributes = $request->all();
        $attributes['user_id'] = auth()->id();
        $attributes['image_id'] = empty($attributes['image_id']) ? null : $attributes['image_id'];
        $attributes['tags'] = empty($attributes['tags']) ? [] : $attributes['tags'];

        if($attributes['content_type_id'] == 7) {
        	$attributes['more'] = str_replace(['<span>', '</span>'], ['', ''], $attributes['more']);
        }

        try {
            $item = Content::create($attributes); 
            $item->retag($attributes['tags']);
 
            $item = $this->saveItem($item);
        } catch(\Exception $e) {
            return back()
                ->withInput()
                ->withFlashDanger('Не удалось сохранить: '.$e->getMessage());
        }

        return redirect($this->routeAfterSave('content', [$slug, $item->id]))
            ->withFlashSuccess('Контент создан и сохранен!');
    }

    public function edit($type, $id)
    {
        $item = Content::findOrFail($id);

        $content = $item->type;

        $products = $item->products()->orderBy('ordering')->lists('products.title', 'products.id');
        $relatedContents = $item->relatedContent()->orderBy('content_related.ordering')->lists('contents.title', 'contents.id');

        $selectedTags = $item->tagged->pluck('tag_name', 'tag_name')->toArray();

        return view($this->viewName('edit', $type), compact('item', 'content', 'products', 'relatedContents', 'selectedTags'));
    }

    public function update(Save $request, $slug, $id)
    {
        $item = Content::findOrFail($id);

        try {
            $attributes = $request->all();
            $attributes['image_id'] = empty($attributes['image_id']) ? null : $attributes['image_id'];
            $attributes['tags'] = empty($attributes['tags']) ? [] : $attributes['tags'];

            $item->update($attributes);
            $item->retag($attributes['tags']);

            $item = $this->saveItem($item);
        } catch(\Exception $e) {
            return back()
                ->withInput()
                ->withFlashDanger('Не удалось сохранить: '.$e->getMessage());
        }

        return redirect($this->routeAfterSave('content', [$slug, $item->id]))
            ->withFlashSuccess('Контент сохранен!');
    }

    protected function viewName($name, $type)
    {
        $view = 'backend.content.'.$type.'.'.$name;

        if(view()->exists($view)) {
            return $view;
        }

        return 'backend.content.'.$name;
    }

    public function search(Request $request)
    {
        $query = $request->input('query');

        $model = Content::where('title', 'like', '%'.$query.'%');

        $contentTypeId = $request->input('contentTypeId');
        if((int)$contentTypeId > 0) {
            $model = $model->where('content_type_id', $contentTypeId);
        }

        $contents = $model->orWhere('slug', $query)
            ->orWhere('id', $query)
            ->take(5)
            ->get();

        $list = [];
        foreach($contents as $content) {
            $list[] = [
                'title' => view('backend.content.partials.search_result', compact('content'))->render(),
                'id' => $content->id,
            ];
        }

        return response()->json($list);
    }

    public function destroy($id)
    {
        $content = Content::findOrFail($id);

        $menuItemsCount = $content->menuItem->count();
        if ($menuItemsCount) {
            return back()->withFlashDanger('У элемента есть '.$menuItemsCount.' пункта/ов меню! Удалите их.');
        }

        try {
            if($content->image) {
                ImageDestroyer::destroy($content->image->id);
            }
        } catch(\InvalidArgumentException $e) {

        }

        $content->image_id = null;

        $content->delete();

        return back()->withSuccessMessage('Элемент удален!');
    }

    protected function saveItem($item)
    {
        $products = $this->syncRelation('products'); 
        $item->products()->sync($products);

        $contents = $this->syncRelation('related_contents'); 
        $item->relatedContent()->sync($contents);

        if(request()->hasFile('image')) {
            $image = (new ImageService('image', $item))->upload();
            $item->image_id = $image->id;
            $item->save();
        }

        return $item;
    }

    protected function syncRelation($name)
    { 
        $items = request()->input($name);

        $sync = [];

        if($items) {
            foreach($items as $order => $id) {
                $sync[$id]['ordering'] = $order;
            }
        }

        return $sync;
    }
}

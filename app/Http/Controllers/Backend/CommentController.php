<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Backend\Traits\RouteAfterSaveTrait;
use App\Http\Controllers\Backend\Traits\ToggleTrait;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Comment\Batch;
use App\Models\Product;
use App\Models\Comment;

class CommentController extends Controller
{
    use ToggleTrait, RouteAfterSaveTrait;

    protected $modelName = '\App\Models\Comment';

    public function index()
    {
        $limit = request('limit', 10);

        $sort = request('sort', 'id');

        $dir = request('dir', 'desc');

        $commentableId = request('item_id');

        $scope = request('scope');

        $comments = Comment::query();

        $comments->with('commentable')->orderBy($sort, $dir);

        $item = null;

        if($commentableId && $scope && $model = $this->getCommentItem($scope)) {
            $item = $model::findOrFail($commentableId);
            $comments->where('commentable_id', $commentableId);
        }

        $comments = $comments->paginate($limit);

        if($commentableId && $model) {
            $comments->appends(['item_id' => $commentableId, 'scope' => $scope]);
        }

        $comments->appends(compact('limit', 'sort', 'dir'));

        return view('backend.comments.index', compact('comments', 'item'));
    }

    protected function getCommentItem($scope)
    {
        $map = ['product' => Product::class, 'content' => Content::class];

        if(!isset($map[$scope])) {
            return false;
        }

        return $map[$scope];
    }

    public function edit($id)
    {
        $comment = Comment::findOrFail($id);

        return view('backend.comments.edit', compact('comment'));
    }

    public function update($id)
    {
        $comment = Comment::findOrFail($id);
        $comment->update(request()->all());

        $comment->published = request()->get('published');
        $comment->save();

        return redirect($this->routeAfterSave('comment', [$id]))
            ->withFlashSuccess('Комментарий сохранен');
    }

    public function destroy($id)
    {
        Comment::destroy($id);

        return $this->back('Комментарий удален!');
    }

    public function publish($id)
    {
        $comment = Comment::findOrFail($id);
        $comment->published = 1;
        $comment->save();

        return redirect($this->routeAfterSave('comment', [$id]))
            ->withFlashSuccess('Комментарий опубликован');
    }

    public function batchDestroy(Batch $request)
    {
        Comment::destroy($request->items);

        return $this->back('Комментарии удалены!');
    }

    public function batchPublish(Batch $request)
    {
        \DB::table('comments')->whereIn('id', $request->items)->update(['published' => 1]);

        return $this->back('Комментарии опубликованы!');
    }

    public function batchUnpublish(Batch $request)
    {
        \DB::table('comments')->whereIn('id', $request->items)->update(['published' => 0]);

        return $this->back('Комментарии опубликованы!');
    }

    protected function back($message)
    {
        return back()->withFlashSuccess($message);
    }
}

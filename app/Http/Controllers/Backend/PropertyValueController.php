<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Backend\Traits\RouteAfterSaveTrait;
use App\Http\Requests\Backend\PropertyValue\Save;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Models\Property;
use App\Models\PropertyValue;
use App\Services\Image\ImageDestroyer;
use App\Services\Image\ImageService;

class PropertyValueController extends Controller
{
    use RouteAfterSaveTrait;

    protected $modelName = '\App\Models\PropertyValue';

    public function index($id)
    {
        $property = Property::findOrFail($id);

        $propertyValues = $property->values()->orderBy('ordering', 'asc')->get();

        return view('backend.property-value.index', compact('property', 'propertyValues'));
    }

    public function create($id)
    {
        $property = Property::findOrFail($id);

        $properties = Property::all()->pluck('title', 'id')->toArray();

        $parents = [];
        $selectedParents = [];

        return view('backend.property-value.create', compact('property', 'properties', 'parents', 'selectedParents'));
    }

    public function store(Save $request)
    {
        $propertyValue = PropertyValue::create($request->all());

        $this->afterSave($propertyValue);

        return redirect($this->routeAfterSave('property-value', [$propertyValue->id]))->withFlashSuccess('Значение создано!');
    }

    public function edit($id)
    {
        $propertyValue = PropertyValue::findOrFail($id);

        $properties = Property::all()->pluck('title', 'id')->toArray();

        $property = $propertyValue->property;

        $parents = $propertyValue->parents()->pluck('property_values.title', 'property_values.id')->toArray();

        $selectedParents = array_keys($parents);

        return view('backend.property-value.edit', compact('property', 'propertyValue', 'properties', 'parents', 'selectedParents'));
    }

    public function update(Save $request, $id)
    {
        $propertyValue = PropertyValue::findOrFail($id);
        $propertyValue->update($request->all());

        $this->afterSave($propertyValue);

        return redirect($this->routeAfterSave('property-value', [$id]))->withFlashSuccess('Значение сохранено!');
    }

    protected function afterSave($propertyValue)
    {
        if(request()->hasFile('image')) {
            $image = (new ImageService('image', $propertyValue))->upload();
            $propertyValue->image_id = $image->id;
            $propertyValue->save();
        }

        $parents = request('parent_values', []);

        $propertyValue->parents()->sync($parents);

        return $propertyValue;
    }

    public function destroy($id)
    {
        $propertyValue = PropertyValue::findOrFail($id);

        try {
            if($propertyValue->image) {
                ImageDestroyer::destroy($propertyValue->image->id);
            }
        } catch (\Exception $e) { }

        $propertyValue->delete();

        return back()->withFlashSuccess('Значение удалено!');
    }

    public function search()
    {
        $query = \Request::input('query');

        $propertyValues = PropertyValue::where('title', 'like', '%'.$query.'%')
            ->orWhere('slug', $query)
            ->orWhere('id', $query)
            ->take(5)
            ->get();

        $list = [];

        foreach($propertyValues as $pv) {
            $list[] = [
                    'title' => $pv->property->title .': '.$pv->title,
                    'id' => $pv->id,
                ];
        }

        return response()->json($list);
    }

    public function sort()
    {
        foreach(Input::get('item') as $ordering => $id) {
            \DB::table('property_values')
                ->where('id', $id)
                ->update(['ordering' => $ordering]);
        }

        return 'ok';
    }
}

<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Backend\Traits\RouteAfterSaveTrait;
use App\Http\Controllers\Backend\Traits\ToggleTrait;
use App\Models\Tag;
use App\Http\Requests\Backend\Tag\Save;

use App\Http\Controllers\Controller;

class TagController extends Controller
{
    use ToggleTrait, RouteAfterSaveTrait;

    protected $modelName = '\App\Models\Tag';

    public function index()
    {
        $limit = request()->get('limit', 10);

        $tags = Tag::paginate($limit);

        return view('backend.tag.index', compact('tags'));
    }

    public function create()
    {
        return view('backend.tag.create');
    }

    public function store(Save $request)
    {
        $attributes = $request->all();

        $tag = Tag::create($attributes);

        return redirect($this->routeAfterSave('tag', [$tag->id]))
            ->withFlashSuccess('Тег создан');
    }

    public function edit($id)
    {
        $tag = Tag::findOrFail($id);

        return view('backend.tag.edit', compact('tag'));
    }

    public function update(Save $request, $id)
    {
        $tag = Tag::findOrFail($id);

        $tag->update($request->all());

        return redirect($this->routeAfterSave('tag', [$id]))
            ->withFlashSuccess('Тег сохранен');
    }

    public function destroy($id)
    {
        $tag = Tag::destroy($id);

        return back()->withFlashSuccess('Тег удален!');
    }

    public function search()
    {
        $query = \Request::input('query');
        
        $tags = Tag::where('title', 'like', '%'.$query.'%')->orWhere('id', $query)->take(5)->get();

        $list = [];
        
        foreach($tags as $tag) {
            $list[] = [
                    'title' => $tag->title,
                    'id' => $tag->id,
                ];
        }

        return response()->json($list);
    }
}
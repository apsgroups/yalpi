<?php

Route::group(['middleware' => 'web'], function() {
    /**
     * Switch between the included languages
     */

    Route::group(['namespace' => 'Language'], function () {
        require (__DIR__ . '/Routes/Language/Language.php');
    });

    /**
     * Frontend Routes
     * Namespaces indicate folder structure
     */
    Route::group(['namespace' => 'Frontend'], function () {
        require (__DIR__ . '/Routes/Frontend/Frontend.php');
        require (__DIR__ . '/Routes/Frontend/Subscribe.php');
        require (__DIR__ . '/Routes/Frontend/Feedback.php');
        require (__DIR__ . '/Routes/Frontend/Access.php');
        require (__DIR__ . '/Routes/Frontend/Search.php');
        require (__DIR__ . '/Routes/Frontend/Market.php');
        require (__DIR__ . '/Routes/Frontend/Sitemap.php');
        require (__DIR__ . '/Routes/Frontend/Filter.php');
        require (__DIR__ . '/Routes/Frontend/Cart.php');
        require (__DIR__ . '/Routes/Frontend/Favorite.php');
        require (__DIR__ . '/Routes/Frontend/Product.php');
        require (__DIR__ . '/Routes/Frontend/Comment.php');
        require (__DIR__ . '/Routes/Frontend/Category.php');
        require (__DIR__ . '/Routes/Frontend/Order.php');
        require (__DIR__ . '/Routes/Frontend/Parser.php');
        require (__DIR__ . '/Routes/Frontend/Menu.php');
        require (__DIR__ . '/Routes/Frontend/Author.php');
        require (__DIR__ . '/Routes/Frontend/Notify.php');
    });
});

/**
 * Backend Routes
 * Namespaces indicate folder structure
 * Admin middleware groups web, auth, and routeNeedsPermission
 */

Route::group(['namespace' => 'Backend', 'prefix' => 'admin'], function () {
    require(__DIR__ . '/Routes/Backend/Index.php');

    Route::group(['middleware' => 'admin'], function() {
        /**
         * These routes need view-backend permission
         * (good if you want to allow more than one group in the backend,
         * then limit the backend features by different roles or permissions)
         *
         * Note: Administrator has all permissions so you do not have to specify the administrator role everywhere.
         */

        require (__DIR__ . '/Routes/Backend/Dashboard.php');
        require (__DIR__ . '/Routes/Backend/Tool.php');
        require (__DIR__ . '/Routes/Backend/Access.php');
        require (__DIR__ . '/Routes/Backend/LogViewer.php');

        require (__DIR__ . '/Routes/Backend/Setting.php');
        require (__DIR__ . '/Routes/Backend/Order.php');
        require (__DIR__ . '/Routes/Backend/Category.php');
        require (__DIR__ . '/Routes/Backend/Product.php');
        require (__DIR__ . '/Routes/Backend/ProductType.php');
        require (__DIR__ . '/Routes/Backend/Property.php');
        require (__DIR__ . '/Routes/Backend/Image.php');
        require (__DIR__ . '/Routes/Backend/ContentType.php');
        require (__DIR__ . '/Routes/Backend/Content.php');
        require (__DIR__ . '/Routes/Backend/Menu.php');
        require (__DIR__ . '/Routes/Backend/MenuItem.php');
        require (__DIR__ . '/Routes/Backend/Widget.php');
        require (__DIR__ . '/Routes/Backend/Import.php');
        require (__DIR__ . '/Routes/Backend/Export.php');
        require (__DIR__ . '/Routes/Backend/MarketCategory.php');
        require (__DIR__ . '/Routes/Backend/Feedback.php');
        require (__DIR__ . '/Routes/Backend/Meta.php');
        require (__DIR__ . '/Routes/Backend/Redirect.php');
        require (__DIR__ . '/Routes/Backend/Attribute.php');
        require (__DIR__ . '/Routes/Backend/VirtualDiscount.php');
        require (__DIR__ . '/Routes/Backend/ProductSet.php');
        require (__DIR__ . '/Routes/Backend/PrettyUrl.php');
        require (__DIR__ . '/Routes/Backend/Tag.php');
        require (__DIR__ . '/Routes/Backend/SortTemplate.php');
        require (__DIR__ . '/Routes/Backend/Language.php');
        require (__DIR__ . '/Routes/Backend/Author.php');
        require (__DIR__ . '/Routes/Backend/PropertyValue.php');
        require (__DIR__ . '/Routes/Backend/Comment.php');
    });

});

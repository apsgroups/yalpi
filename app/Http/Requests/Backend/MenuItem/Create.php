<?php

namespace App\Http\Requests\Backend\MenuItem;

use App\Http\Requests\Request;

class Create extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'type' => 'required',
            'menu_id' => 'required',
            'slug' => 'unique:menu_items,slug,'.Request::get('id').',id,parent_id,'.Request::get('parent_id').',menu_id,'.Request::get('menu_id'),
        ];
    }
}

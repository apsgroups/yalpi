<?php

namespace App\Http\Requests\Backend\PrettyUrl;

use App\Http\Requests\Request;

class Save extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'pretty_url' => 'required',
            'options' => 'required',
            'category_id' => 'required',
        ];
    }
}

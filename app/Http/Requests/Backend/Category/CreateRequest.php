<?php

namespace App\Http\Requests\Backend\Category;

use App\Http\Requests\Request;

class CreateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'slug' => 'unique:categories,slug,'.Request::get('id').',id,parent_id,'.Request::get('parent_id'),
            'image' => 'max:4096|mimes:jpeg,png',
        ];
    }
}

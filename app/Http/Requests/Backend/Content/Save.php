<?php

namespace App\Http\Requests\Backend\Content;

use App\Http\Requests\Request;

class Save extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'published' => 'boolean',
            'slug' => 'alpha_dash|unique:contents,slug,'.Request::get('id').',id,content_type_id,'.Request::get('content_type_id'),
        ];
    }
}

<?php

namespace App\Http\Requests\Backend\Product;

use App\Http\Requests\Request;

class Create extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'published' => 'boolean',
            'slug' => 'alpha_dash|unique:products,slug,'.Request::get('id').',id,category_id,'.Request::get('category_id'),
            'price' => 'numeric',
            'wholesale_price' => 'numeric',
            'discount' => 'numeric',
            'image' => 'max:4096|mimes:jpeg,png',
            'category_id' => 'required',
        ];
    }
}

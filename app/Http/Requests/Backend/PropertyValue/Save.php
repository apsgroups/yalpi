<?php

namespace App\Http\Requests\Backend\PropertyValue;

use App\Http\Requests\Request;

class Save extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'slug' =>'alpha_dash|unique:property_values,slug,'.Request::get('id').',id,property_id,'.Request::get('property_id')
        ];
    }
}

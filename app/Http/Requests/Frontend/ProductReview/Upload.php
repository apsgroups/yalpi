<?php

namespace App\Http\Requests\Frontend\ProductReview;

use App\Http\Requests\Request;

class Upload extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'qqfile' => 'required|max:4096|mimes:jpeg,png',
        ];
    }
}
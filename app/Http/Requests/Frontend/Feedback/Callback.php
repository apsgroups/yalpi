<?php

namespace App\Http\Requests\Frontend\Feedback;

use App\Http\Requests\Request;

class Callback extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required_unless:area,footer',
            'phone' => 'required',
        ];
    }
}

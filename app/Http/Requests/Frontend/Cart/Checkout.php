<?php

namespace App\Http\Requests\Frontend\Cart;

use App\Http\Requests\Request;

class Checkout extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'payment_id' => 'required',
            'shipping_id' => 'required',
            'name' => 'required',
            'phone' => 'required',
            'email' => 'required|email',
        ];
    }
}

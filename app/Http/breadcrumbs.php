<?php

require __DIR__ . '/Breadcrumbs/Backend/Backend.php';

require __DIR__ . '/Breadcrumbs/Frontend/Frontend.php';
require __DIR__ . '/Breadcrumbs/Frontend/Category.php';
require __DIR__ . '/Breadcrumbs/Frontend/Product.php';
require __DIR__ . '/Breadcrumbs/Frontend/Favorite.php';
require __DIR__ . '/Breadcrumbs/Frontend/Cart.php';
require __DIR__ . '/Breadcrumbs/Frontend/Content.php';
require __DIR__ . '/Breadcrumbs/Frontend/Search.php';
require __DIR__ . '/Breadcrumbs/Frontend/Feedback.php';
require __DIR__ . '/Breadcrumbs/Frontend/Author.php';
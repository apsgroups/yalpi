<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

/**
 * Class Authenticate
 * @package App\Http\Middleware
 */
class Dummy
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if(request('auth')) {
            session(['fake_auth' => 1]);
            return $next($request);
        }

        if(session('fake_auth')) {
            return $next($request);
        }

        if ((!starts_with($request->path(), 'admin') && !starts_with($request->path(), 'blog') && !starts_with($request->path(), 'consult') && !starts_with($request->path(), 'en')) && Auth::guard($guard)->guest()) {
            return response(view(mobile_prefix().'.dummy.index'));
        }

        return $next($request);
    }
}
<?php

namespace App\Http\Middleware;

use Closure;

class OptimizeMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);
        $buffer = $response->getContent();

        $replace = array(
            //'/<!--[^\[](.*?)[^\]]-->/s' => '',
            //"/<\?php/"                  => '<?php ',
            "/\n([\S])/"                => '$1',
            "/\r/"                      => ' ',
            "/\n/"                      => ' ',
            "/\t/"                      => ' ',
            "/ +/"                      => ' ',
        );

        if(strpos($request->path(), 'yandex') !== false) {
            return $next($request);
        }

        $buffer = preg_replace(array_keys($replace), array_values($replace), $buffer);
        $response->setContent($buffer);
        //ini_set('zlib.output_compression', 'On'); // If you like to enable GZip, too!
        return $response;
    }
}
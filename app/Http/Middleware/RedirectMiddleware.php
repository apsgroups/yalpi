<?php

namespace App\Http\Middleware;

use App\Models\Redirect;
use Closure;

class RedirectMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
    	$path = $request->path();

        if(preg_match('/\/+popular$/', $path)) {
            $link = preg_replace('/\/+popular$/', '/', $path);
            return redirect($link, 301);
        }

        $redirect = $this->isRedirect($request->path());

        if($redirect) {
            $link = $redirect->link();

            $link = strpos($link, '.') === false ? $link : rtrim($link, '/');
            
            return redirect($link, 301);
        }
    	
    	// redirects ?page=1 in pagination
    	if(request()->get('page') === '1') {
    		$params = $_GET;

    		unset($params['page']);

    		if(!empty($params)) {
    			$path .= '?'.http_build_query($params);
    		}

            return redirect($path, 301);
    	}

        if(preg_match('/\/modules\/(.+)/', $request->path(), $match)) {
            $product = \App\Models\Product::slug($match[1])->first();

            if($product) {
                return redirect($product->link(), 301);
            }

        }

        return $next($request);
    }

    protected function isRedirect($path) {
        $path .= strpos($path, '.') === false ? '/' : '';
        $redirect = Redirect::where('from_url', $path)->first();

        return ($redirect) ? $redirect : false;
    }
}

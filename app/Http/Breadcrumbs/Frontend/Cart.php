<?php

Breadcrumbs::register('frontend.cart', function ($breadcrumbs) {
    $breadcrumbs->parent('frontend.index');

    $breadcrumbs->push('Корзина', route('cart.index'));
});

Breadcrumbs::register('frontend.cart.thankyou', function ($breadcrumbs, $order) {
    $breadcrumbs->parent('frontend.cart');

    $breadcrumbs->push('Заказ №'.$order->id.' оформлен!');
});
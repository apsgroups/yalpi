<?php

Breadcrumbs::register('frontend.product', function ($breadcrumbs, $product) {
    $breadcrumbs->parent('frontend.category', $product->category);

    $breadcrumbs->push($product->title, product_url($product));
});

Breadcrumbs::register('frontend.product.modules', function ($breadcrumbs, $product) {
    $breadcrumbs->parent('frontend.product', $product);

    $breadcrumbs->push('Модули', modules_url($product));
});

Breadcrumbs::register('frontend.product.module', function ($breadcrumbs, $product, $module) {
    $breadcrumbs->parent('frontend.product.modules', $product);

    $breadcrumbs->push($module->title, module_url($product, $module));
});
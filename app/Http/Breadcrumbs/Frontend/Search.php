<?php

Breadcrumbs::register('frontend.search', function ($breadcrumbs, $term) {
    $breadcrumbs->parent('frontend.index');

    $breadcrumbs->push('Поиск по каталогу', route('search.index'));

    if($term) {
        $breadcrumbs->push('Результаты по запросу "'.$term.'"');
    }
});
<?php

Breadcrumbs::register('frontend.favorite', function($breadcrumbs) {
    $breadcrumbs->parent('frontend.index');

    $breadcrumbs->push('Избранные товары', route('frontend.index'));
});
<?php

Breadcrumbs::register('frontend.feedback', function ($breadcrumbs, $title) {
    $breadcrumbs->parent('frontend.index');

    $breadcrumbs->push($title);
});
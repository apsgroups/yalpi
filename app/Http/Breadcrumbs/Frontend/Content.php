<?php

Breadcrumbs::register('frontend.content.type', function($breadcrumbs, $contentType) {
    $breadcrumbs->parent('frontend.index');

    $path = (new \App\Services\MenuService())->getBreadcrumbPath($contentType->id, 'contentType');

    if(!$path) {
        $path[] = [
            'title' => $contentType->title,
            'url' => content_type_url($contentType),
        ];
    }

    foreach($path as $crumb) {
        $breadcrumbs->push($crumb['title'], $crumb['url']);
    }
});

Breadcrumbs::register('frontend.content.show', function($breadcrumbs, $content) {

    $path = (new \App\Services\MenuService())->getBreadcrumbPath($content->id, 'content');

    if(!$path) {
        $breadcrumbs->parent('frontend.content.type', $content->type);

        $path[] = [
            'title' => $content->title,
            'url' => content_url($content),
        ];
    } else {
        $breadcrumbs->parent('frontend.index');
    }

    foreach($path as $crumb) {
        $breadcrumbs->push($crumb['title'], $crumb['url']);
    }
});
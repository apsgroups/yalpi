<?php

Breadcrumbs::register('frontend.catalog', function ($breadcrumbs) {
    $breadcrumbs->parent('frontend.index');

    // $breadcrumbs->push('Каталог', catalog_url());
});

Breadcrumbs::register('frontend.category', function ($breadcrumbs, $category) {
    $breadcrumbs->parent('frontend.catalog');

    $categoryContract = app()->make('\App\Repositories\Frontend\Category\CategoryContract');

    $path = $categoryContract->getBreadcrumbsPath($category->id);

    //$path = (new \App\Services\MenuService())->getBreadcrumbPath($category->id, 'category');

    if(!$path) {
        return null;
    }

    foreach($path as $crumb) {
        $breadcrumbs->push($crumb['title'], $crumb['url']);
    }
});
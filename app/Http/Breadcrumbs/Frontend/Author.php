<?php

Breadcrumbs::register('frontend.author', function($breadcrumbs, $author) {
    $breadcrumbs->parent('frontend.index');

    $breadcrumbs->push($author->name, route('author.items', [$author->slug]));
});

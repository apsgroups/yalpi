<?php

Route::resource('widget', 'WidgetController', [
    'names' => [
        'index' => 'admin.widget.index',
        'create' => 'admin.widget.create',
        'store' => 'admin.widget.store',
        'edit' => 'admin.widget.edit',
        'update' => 'admin.widget.update',
        'destroy' => 'admin.widget.destroy',
    ],
    'except' => ['show'],
]);

Route::post('widget/sort', [
    'as' => 'admin.widget.sort',
    'uses' => 'WidgetController@sort',
]);

Route::post('widget/{id}/toggle', [
    'as' => 'admin.widget.toggle',
    'uses' => 'WidgetController@toggle',
]);
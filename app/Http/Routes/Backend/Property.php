<?php

Route::resource('property', 'PropertyController', [
    'names' => [
        'index' => 'admin.property.index',
        'create' => 'admin.property.create',
        'store' => 'admin.property.store',
        'edit' => 'admin.property.edit',
        'update' => 'admin.property.update',
        'destroy' => 'admin.property.destroy',
    ],
    'except' => ['show'],
]);

Route::get('property/load-manager', 'PropertyController@loadManager');
Route::get('property/by-title', 'PropertyController@byTitle')->name('property.byTitle');
Route::get('property/by-value', 'PropertyController@byValue')->name('property.byValue');
Route::post('properties/sort', 'PropertyController@sort')->name('property.sort');

Route::post('property/{id}/toggle', [
    'as' => 'admin.property.toggle',
    'uses' => 'PropertyController@toggle',
]);
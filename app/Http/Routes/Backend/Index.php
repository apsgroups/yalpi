<?php

/**
 * These routes require the user NOT be logged in
 */
Route::group(['middleware' => 'web'], function() {
    Route::group(['middleware' => 'guest'], function () {
        // Authentication Routes
        Route::get('login', 'AuthController@showLoginForm')
            ->name('admin.auth.login');
        Route::post('login', 'AuthController@login')->name('admin.auth.login.submit');
    });
});
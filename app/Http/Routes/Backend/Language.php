<?php

Route::resource('language', 'LanguageController', [
    'names' => [
        'index' => 'admin.language.index',
        'create' => 'admin.language.create',
        'store' => 'admin.language.store',
        'edit' => 'admin.language.edit',
        'update' => 'admin.language.update',
        'destroy' => 'admin.language.destroy',
    ],
    'except' => ['show'],
]);
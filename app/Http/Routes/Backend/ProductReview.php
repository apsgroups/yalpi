<?php

Route::resource('product-review', 'ProductReviewController', [
    'names' => [
        'index' => 'admin.product-review.index',
        'edit' => 'admin.product-review.edit',
        'update' => 'admin.product-review.update',
        'destroy' => 'admin.product-review.destroy',
    ],
    'except' => ['show'],
]);

Route::group(['prefix' => 'product-review'], function() {
    Route::get('/{id}/publish', [
        'as' => 'admin.product-review.publish',
        'uses' => 'ProductReviewController@publish',
    ]);

    Route::post('/{id}/toggle', [
        'as' => 'admin.productreview.toggle',
        'uses' => 'ProductReviewController@toggle',
    ]);

    Route::group(['prefix' => 'batch'], function() {
        foreach(config('backend.actions.product-review') as $route => $title) {
            Route::post('/'.$route, [
                'as' => 'admin.product-review.batch.'.$route,
                'uses' => 'ProductReviewController@batch'.studly_case($route),
            ]);
        }
    });
});
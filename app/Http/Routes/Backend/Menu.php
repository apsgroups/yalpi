<?php

Route::resource('menu', 'MenuController', [
    'names' => [
        'index' => 'admin.menu.index',
        'create' => 'admin.menu.create',
        'store' => 'admin.menu.store',
        'edit' => 'admin.menu.edit',
        'update' => 'admin.menu.update',
        'destroy' => 'admin.menu.destroy',
    ],
    'except' => ['show'],
]);

Route::post('menu/{id}/toggle', [
    'as' => 'admin.menu.toggle',
    'uses' => 'MenuController@toggle',
]);
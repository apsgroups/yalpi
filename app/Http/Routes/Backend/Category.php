<?php

Route::resource('category', 'CategoryController', [
    'names' => [
        'index' => 'admin.category.index',
        'create' => 'admin.category.create',
        'store' => 'admin.category.store',
        'edit' => 'admin.category.edit',
        'update' => 'admin.category.update',
        'destroy' => 'admin.category.destroy',
    ],
    'except' => ['show'],
]);

Route::post('category/sort', [
    'as' => 'admin.category.sort',
    'uses' => 'CategoryController@sortTree',
]);

Route::post('category/{id}/toggle', [
    'as' => 'admin.category.toggle',
    'uses' => 'CategoryController@toggle',
]);
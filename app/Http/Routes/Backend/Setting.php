<?php

Route::get('settings', [
    'as' => 'admin.setting',
    'uses' => 'SettingController@index',
]);

Route::post('settings', [
    'as' => 'admin.setting.store',
    'uses' => 'SettingController@store',
]);
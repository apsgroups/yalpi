<?php
Route::group(['prefix' => 'product-set'], function() {
    Route::get('/search', 'ProductSetController@search');

    Route::get('/search-product', 'ProductSetController@searchProduct');

    Route::post('/product/{set}/{id}', 'ProductSetController@attachProduct');

    Route::delete('/product/{id}', 'ProductSetController@detachProduct');
});

Route::resource('product-set', 'ProductSetController', [
    'names' => [
        'index' => 'admin.product-set.show',
        'store' => 'admin.product-set.store',
        'update' => 'admin.product-set.update',
        'destroy' => 'admin.product-set.destroy',
    ],
    'except' => ['index', 'edit', 'create'],
]);

<?php

Route::resource('product', 'ProductController', [
    'names' => [
        'index' => 'admin.product.index',
        'create' => 'admin.product.create',
        'store' => 'admin.product.store',
        'edit' => 'admin.product.edit',
        'update' => 'admin.product.update',
        'destroy' => 'admin.product.destroy',
    ],
    'except' => ['show'],
]);

Route::group(['prefix' => 'product'], function() {
    Route::get('/search', 'ProductController@search');

    Route::get('/module/{id}', 'ProductController@module');

    Route::post('/{id}/toggle', [
        'as' => 'admin.product.toggle',
        'uses' => 'ProductController@toggle',
    ]);

    Route::get('/ajax-list/{id}', 'ProductController@ajaxList');

    Route::post('/sort', [
        'as' => 'admin.product.sort',
        'uses' => 'ProductController@saveSort',
    ]);

    Route::post('/sets-ordering', [
        'as' => 'admin.product.sets_ordering',
        'uses' => 'ProductController@saveSetsOrdering',
    ]);

    Route::group(['prefix' => 'batch'], function() {
        foreach(config('backend.actions.product') as $route => $title) {
            Route::post('/'.$route, [
                'as' => 'admin.product.batch.'.$route,
                'uses' => 'ProductController@batch'.studly_case($route),
            ]);
        }
    });
});
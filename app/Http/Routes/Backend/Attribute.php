<?php

Route::resource('attributes', 'AttributeController', [
    'names' => [
        'index' => 'admin.attribute.index',
        'create' => 'admin.attribute.create',
        'store' => 'admin.attribute.store',
        'edit' => 'admin.attribute.edit',
        'update' => 'admin.attribute.update',
        'destroy' => 'admin.attribute.destroy',
    ],
    'except' => ['show'],
]);
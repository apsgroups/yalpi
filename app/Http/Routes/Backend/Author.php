<?php

Route::get('author/search', 'AuthorController@search');

Route::resource('author', 'AuthorController', [
    'names' => [
        'index' => 'admin.author.index',
        'create' => 'admin.author.create',
        'store' => 'admin.author.store',
        'edit' => 'admin.author.edit',
        'update' => 'admin.author.update',
        'destroy' => 'admin.author.destroy',
    ],
    'except' => ['show'],
]);
<?php

Route::resource('market-categories', 'MarketCategoryController', [
    'names' => [
        'index' => 'admin.market-category.index',
        'create' => 'admin.market-category.create',
        'store' => 'admin.market-category.store',
        'edit' => 'admin.market-category.edit',
        'update' => 'admin.market-category.update',
        'destroy' => 'admin.market-category.destroy',
    ],
    'except' => ['show'],
]);
<?php

Route::group(['prefix' => 'products'], function() {
    Route::get('/', [
        'as' => 'admin.export.index',
        'uses' => 'ProductController@index'
    ]);

    Route::post('/export', [
        'as' => 'admin.export.product.export',
        'uses' => 'ProductController@export'
    ]);
});
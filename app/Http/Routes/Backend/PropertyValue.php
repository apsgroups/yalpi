<?php

Route::get('property/{property}/values', 'PropertyValueController@index')->name('admin.property-value.index');

Route::get('property/{property}/create', 'PropertyValueController@create')->name('admin.property-value.create');

Route::get('property-values/search', 'PropertyValueController@search')->name('admin.property-value.search');

Route::post('property-values/sort', 'PropertyValueController@sort')->name('admin.property-value.sort');

Route::resource('property-values', 'PropertyValueController', [
    'names' => [
        'store' => 'admin.property-value.store',
        'edit' => 'admin.property-value.edit',
        'update' => 'admin.property-value.update',
        'destroy' => 'admin.property-value.destroy',
    ],
    'except' => ['show'],
]);
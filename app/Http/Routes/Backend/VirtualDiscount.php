<?php

Route::group(['prefix' => 'virtual-discounts'], function() {
    Route::get('/', [
        'as' => 'admin.virtual-discounts.index',
        'uses' => 'VirtualDiscountController@index',
    ]);

    Route::get('/run', [
        'as' => 'admin.virtual-discounts.run',
        'uses' => 'VirtualDiscountController@run',
    ]);
});
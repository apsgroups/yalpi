<?php

Route::get('menu/items/load-options', 'MenuItemController@loadOptions');

Route::post('menu/items/sort', [
    'as' => 'admin.menu-item.sort',
    'uses' => 'MenuItemController@sort',
]);



Route::get('menu/items/{menu}', [
    'as' => 'admin.menu-item.index',
    'uses' => 'MenuItemController@index',
]);

Route::get('menu/items/{menu}/create', [
    'as' => 'admin.menu-item.create',
    'uses' => 'MenuItemController@create',
]);

Route::get('menu/items/{menu}/category-create', [
    'as' => 'admin.menu-item.category-create',
    'uses' => 'MenuItemController@categoryCreate',
]);

Route::get('menu/items/{menu}/pointer-create', [
    'as' => 'admin.menu-item.pointer-create',
    'uses' => 'MenuItemController@pointerCreate',
]);

Route::post('menu/items/{menu}', [
    'as' => 'admin.menu-item.store',
    'uses' => 'MenuItemController@store',
]);

Route::post('menu/items/{menu}/category-store', [
    'as' => 'admin.menu-item.category-store',
    'uses' => 'MenuItemController@categoryStore',
]);

Route::post('menu/items/{menu}/pointer-store', [
    'as' => 'admin.menu-item.pointer-store',
    'uses' => 'MenuItemController@pointerStore',
]);

Route::get('menu/items/{menu}/{id}/edit', [
    'as' => 'admin.menu-item.edit',
    'uses' => 'MenuItemController@edit',
]);

Route::put('menu/items/{menu}/{id}', [
    'as' => 'admin.menu-item.update',
    'uses' => 'MenuItemController@update',
]);

Route::delete('menu/items/{menu}/{id}/destroy', [
    'as' => 'admin.menu-item.destroy',
    'uses' => 'MenuItemController@destroy',
]);

Route::post('menu/items/{id}/toggle', [
    'as' => 'admin.menu-item.toggle',
    'uses' => 'MenuItemController@toggle',
]);
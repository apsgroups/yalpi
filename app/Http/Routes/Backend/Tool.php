<?php

Route::get('tool/reindex', [
    'as' => 'admin.tool.products.reindex',
    'uses' => 'ToolController@reindex',
]);

Route::get('tool/reindex-categories', [
    'as' => 'admin.tool.categories.reindex',
    'uses' => 'ToolController@reindexCategories',
]);

Route::get('tool/copy-menu/{id}/{to}', [
    'uses' => 'ToolController@copyMenu',
]);

Route::get('tool/cache/clear', 'ToolController@cleanCache')->name('admin.tool.cache.clear');

Route::get('tool/cache/clear/op', 'ToolController@cleanOpCache')->name('admin.tool.cache.opclear');

Route::get('tool/denorm/menu', 'ToolController@denormMenu');

Route::get('tool/denorm/categories', 'ToolController@denormCategories');

Route::get('tool/root-parent-categories', 'ToolController@rootParentCategories');

Route::get('tool/youtube-images', 'ToolController@youtubeImages');

Route::get('tool/autosale', [
    'uses' => 'ToolController@autosale',
])->name('admin.tool.autosale');
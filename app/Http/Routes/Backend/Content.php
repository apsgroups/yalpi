<?php

Route::get('content/search', 'ContentController@search');

Route::get('content/{type}', [
    'as' => 'admin.content.index',
    'uses' => 'ContentController@index',
]);

Route::get('content/{type}/create', [
    'as' => 'admin.content.create',
    'uses' => 'ContentController@create',
]);

Route::post('content/{type}', [
    'as' => 'admin.content.store',
    'uses' => 'ContentController@store',
]);

Route::get('content/{type}/{id}/edit', [
    'as' => 'admin.content.edit',
    'uses' => 'ContentController@edit',
]);

Route::put('content/{type}/{id}', [
    'as' => 'admin.content.update',
    'uses' => 'ContentController@update',
]);

Route::delete('content/{id}', [
    'as' => 'admin.content.destroy',
    'uses' => 'ContentController@destroy',
]);

Route::post('content/{id}/toggle', [
    'as' => 'admin.content.toggle',
    'uses' => 'ContentController@toggle',
]);
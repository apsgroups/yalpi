<?php

Route::resource('order', 'OrderController', [
    'names' => [
        'index' => 'admin.order.index',
        'create' => 'admin.order.create',
        'store' => 'admin.order.store',
        'edit' => 'admin.order.edit',
        'update' => 'admin.order.update',
        'destroy' => 'admin.order.destroy',
    ],
    'except' => ['show'],
]);
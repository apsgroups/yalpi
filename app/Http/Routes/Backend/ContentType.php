<?php

Route::resource('content-types', 'ContentTypeController', [
    'names' => [
        'index' => 'admin.content-type.index',
        'create' => 'admin.content-type.create',
        'store' => 'admin.content-type.store',
        'edit' => 'admin.content-type.edit',
        'update' => 'admin.content-type.update',
        'destroy' => 'admin.content-type.destroy',
    ],
    'except' => ['show'],
]);

Route::post('content-types/{id}/toggle', [
    'as' => 'admin.content-type.toggle',
    'uses' => 'ContentTypeController@toggle',
]);
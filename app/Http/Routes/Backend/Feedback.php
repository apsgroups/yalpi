<?php

Route::resource('feedback', 'FeedbackController', [
    'names' => [
        'index' => 'admin.feedback.index',
        'show' => 'admin.feedback.show',
        'destroy' => 'admin.feedback.destroy',
    ],
    'except' => ['create', 'store', 'update'],
]);

Route::group(['prefix' => 'batch'], function() {
    foreach(config('backend.actions.feedback') as $route => $title) {
        Route::post('/'.$route, [
            'as' => 'admin.feedback.batch.'.$route,
            'uses' => 'FeedbackController@batch'.studly_case($route),
        ]);
    }
});
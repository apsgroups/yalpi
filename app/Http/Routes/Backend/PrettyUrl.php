<?php

Route::resource('pretty-url', 'PrettyUrlController', [
    'names' => [
        'index' => 'admin.pretty-url.index',
        'create' => 'admin.pretty-url.create',
        'store' => 'admin.pretty-url.store',
        'edit' => 'admin.pretty-url.edit',
        'update' => 'admin.pretty-url.update',
        'destroy' => 'admin.pretty-url.destroy',
    ],
    'except' => ['show'],
]);
<?php

Route::resource('tags', 'TagController', [
    'names' => [
        'index' => 'admin.tag.index',
        'create' => 'admin.tag.create',
        'store' => 'admin.tag.store',
        'edit' => 'admin.tag.edit',
        'update' => 'admin.tag.update',
        'destroy' => 'admin.tag.destroy',
    ],
    'except' => ['show'],
]);

Route::post('tags/{id}/toggle', [
    'as' => 'admin.tag.toggle',
    'uses' => 'TagController@toggle',
]);

Route::post('tags/{id}/toggle', [
    'as' => 'admin.tag.toggle',
    'uses' => 'TagController@toggle',
]);

Route::get('tags/search', 'TagController@search');
<?php

Route::group(['prefix' => 'redirect'], function() {
    Route::group(['prefix' => 'batch'], function() {
        foreach(config('backend.actions.redirect') as $route => $title) {
            Route::post('/'.$route, [
                'as' => 'admin.redirect.batch.'.$route,
                'uses' => 'RedirectController@batch'.studly_case($route),
            ]);
        }
    });

    Route::post('upload', [
        'as' => 'admin.redirect.upload',
        'uses' => 'RedirectController@upload',
    ]);
});

Route::resource('redirect', 'RedirectController', [
    'names' => [
        'index' => 'admin.redirect.index',
        'create' => 'admin.redirect.create',
        'store' => 'admin.redirect.store',
        'edit' => 'admin.redirect.edit',
        'update' => 'admin.redirect.update',
        'destroy' => 'admin.redirect.destroy',
    ],
    'except' => ['show'],
]);

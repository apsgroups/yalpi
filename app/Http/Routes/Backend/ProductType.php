<?php

Route::resource('product-type', 'ProductTypeController', [
    'names' => [
        'index' => 'admin.product-type.index',
        'create' => 'admin.product-type.create',
        'store' => 'admin.product-type.store',
        'edit' => 'admin.product-type.edit',
        'update' => 'admin.product-type.update',
        'destroy' => 'admin.product-type.destroy',
    ],
    'except' => ['show'],
]);

Route::group(['prefix' => 'product-type'], function() {
    Route::post('/{id}/toggle', [
        'as' => 'admin.producttype.toggle',
        'uses' => 'ProductTypeController@toggle',
    ]);
});
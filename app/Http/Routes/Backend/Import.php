<?php

Route::group(['prefix' => 'import', 'namespace' => 'Import'], function() {
    require (__DIR__ . '/Import/Missing.php');

    require (__DIR__ . '/Import/Articuls.php');

    require (__DIR__ . '/Import/Meta.php');

    require (__DIR__ . '/Import/Tag.php');

    require (__DIR__ . '/Import/Category.php');

    require (__DIR__ . '/Import/CategoryTag.php');

    require (__DIR__ . '/Import/CategoryImage.php');

    require (__DIR__ . '/Import/CategoryLink.php');

    require (__DIR__ . '/Import/Report.php');

    require (__DIR__ . '/Import/CategoryPriority.php');

    require (__DIR__ . '/Import/ColorPriority.php');

    require (__DIR__ . '/Import/CategoryBreadcrumbs.php');

    require (__DIR__ . '/Import/Product.php');
});
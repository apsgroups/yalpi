<?php

Route::group(['prefix' => 'meta'], function() {
    Route::get('/', [
        'as' => 'admin.import.meta.index',
        'uses' => 'MetaController@index'
    ]);

    Route::post('/', [
        'as' => 'admin.import.meta.upload',
        'uses' => 'MetaController@upload'
    ]);
});
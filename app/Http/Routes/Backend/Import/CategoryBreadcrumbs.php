<?php

Route::group(['prefix' => 'category-breadcrumbs'], function() {
    Route::get('/', [
        'as' => 'admin.import.category-breadcrumbs.index',
        'uses' => 'CategoryBreadcrumbsController@index'
    ]);

    Route::post('/', [
        'as' => 'admin.import.category-breadcrumbs.upload',
        'uses' => 'CategoryBreadcrumbsController@upload'
    ]);
});
<?php

Route::group(['prefix' => 'tags'], function() {
    Route::get('/', [
        'as' => 'admin.import.tag.index',
        'uses' => 'TagController@index'
    ]);

    Route::post('/', [
        'as' => 'admin.import.tag.upload',
        'uses' => 'TagController@upload'
    ]);
});
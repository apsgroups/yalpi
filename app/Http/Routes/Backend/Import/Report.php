<?php

Route::group(['prefix' => 'report'], function() {
    Route::get('/download/{id}', [
        'as' => 'admin.import.report.download',
        'uses' => 'ReportController@download'
    ]);

    Route::post('/destroy/{id}', [
        'as' => 'admin.import.report.destroy',
        'uses' => 'ReportController@destroy'
    ]);

    Route::post('/clean/{operation}', [
        'as' => 'admin.import.report.clean',
        'uses' => 'ReportController@clean'
    ]);
});
<?php

Route::group(['prefix' => 'category-tags'], function() {
    Route::get('/', [
        'as' => 'admin.import.category-tag.index',
        'uses' => 'CategoryTagController@index'
    ]);

    Route::post('/', [
        'as' => 'admin.import.category-tag.upload',
        'uses' => 'CategoryTagController@upload'
    ]);
});
<?php

Route::group(['prefix' => 'category-images'], function() {
    Route::get('/', [
        'as' => 'admin.import.category-image.index',
        'uses' => 'CategoryImageController@index'
    ]);

    Route::post('/', [
        'as' => 'admin.import.category-image.upload',
        'uses' => 'CategoryImageController@upload'
    ]);
});
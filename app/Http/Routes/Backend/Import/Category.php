<?php

Route::group(['prefix' => 'categories'], function() {
    Route::get('/', [
        'as' => 'admin.import.category.index',
        'uses' => 'CategoryController@index'
    ]);

    Route::post('/', [
        'as' => 'admin.import.category.upload',
        'uses' => 'CategoryController@upload'
    ]);
});
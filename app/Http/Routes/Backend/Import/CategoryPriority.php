<?php

Route::group(['prefix' => 'category-priority'], function() {
    Route::get('/', [
        'as' => 'admin.import.category-priority.index',
        'uses' => 'CategoryPriorityController@index'
    ]);

    Route::post('/', [
        'as' => 'admin.import.category-priority.upload',
        'uses' => 'CategoryPriorityController@upload'
    ]);
});
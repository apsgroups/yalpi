<?php

Route::group(['prefix' => 'category-links'], function() {
    Route::get('/', [
        'as' => 'admin.import.category-link.index',
        'uses' => 'CategoryLinkController@index'
    ]);

    Route::post('/', [
        'as' => 'admin.import.category-link.upload',
        'uses' => 'CategoryLinkController@upload'
    ]);
});
<?php

Route::group(['prefix' => 'products'], function() {
    Route::get('/', [
        'as' => 'admin.import.product.index',
        'uses' => 'ProductController@index'
    ]);

    Route::post('/', [
        'as' => 'admin.import.product.upload',
        'uses' => 'ProductController@upload'
    ]);
});
<?php

Route::group(['prefix' => 'color-priority'], function() {
    Route::get('/', [
        'as' => 'admin.import.color-priority.index',
        'uses' => 'ColorPriorityController@index'
    ]);

    Route::post('/', [
        'as' => 'admin.import.color-priority.upload',
        'uses' => 'ColorPriorityController@upload'
    ]);
});
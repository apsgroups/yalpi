<?php

Route::group(['prefix' => 'missing'], function() {
    Route::get('/', [
        'as' => 'admin.import.missing.index',
        'uses' => 'MissingController@index'
    ]);

    Route::get('/download', [
        'as' => 'admin.import.missing.download',
        'uses' => 'MissingController@download'
    ]);
});
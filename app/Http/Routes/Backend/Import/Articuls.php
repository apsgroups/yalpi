<?php

Route::group(['prefix' => 'articuls'], function() {
    Route::get('/', [
        'as' => 'admin.import.articul.index',
        'uses' => 'ArticulController@index'
    ]);

    Route::post('/', [
        'as' => 'admin.import.articul.upload',
        'uses' => 'ArticulController@upload'
    ]);

    Route::get('/export', [
        'as' => 'admin.import.articul.export',
        'uses' => 'ArticulController@export'
    ]);
});
<?php

Route::resource('comment', 'CommentController', [
    'names' => [
        'index' => 'admin.comment.index',
        'edit' => 'admin.comment.edit',
        'update' => 'admin.comment.update',
        'destroy' => 'admin.comment.destroy',
    ],
    'except' => ['show'],
]);

Route::group(['prefix' => 'comment'], function() {
    Route::get('/{id}/publish', [
        'as' => 'admin.comment.publish',
        'uses' => 'CommentController@publish',
    ]);

    Route::post('/{id}/toggle', [
        'as' => 'admin.comment.toggle',
        'uses' => 'CommentController@toggle',
    ]);

    Route::group(['prefix' => 'batch'], function() {
        foreach(config('backend.actions.comment') as $route => $title) {
            Route::post('/'.$route, [
                'as' => 'admin.comment.batch.'.$route,
                'uses' => 'CommentController@batch'.studly_case($route),
            ]);
        }
    });
});
<?php

Route::resource('sort-template', 'SortTemplateController', [
    'names' => [
        'index' => 'admin.sort-template.index',
        'create' => 'admin.sort-template.create',
        'store' => 'admin.sort-template.store',
        'edit' => 'admin.sort-template.edit',
        'update' => 'admin.sort-template.update',
        'destroy' => 'admin.sort-template.destroy',
    ],
    'except' => ['show'],
]);

Route::post('sort-template/assign', [
    'as' => 'admin.sort-template.assign',
    'uses' => 'SortTemplateController@assign',
]);
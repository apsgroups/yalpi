<?php

Route::resource('meta', 'MetaController', [
    'names' => [
        'index' => 'admin.meta.index',
        'create' => 'admin.meta.create',
        'store' => 'admin.meta.store',
        'edit' => 'admin.meta.edit',
        'update' => 'admin.meta.update',
        'destroy' => 'admin.meta.destroy',
    ],
    'except' => ['show'],
]);
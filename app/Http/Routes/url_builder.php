<?php

function per_page_url($url, $limit) {
    $parts = parse_url($url);

    $params = [];

    if(!empty($parts['query'])) {
        parse_str($parts['query'], $params);
    }

    $params['per_page'] = $limit;


    if(empty($parts['path'])) {
        $parts['path'] = '';
    }

    return $parts['path'].'?'.http_build_query($params);
}

function content_type_url(\App\Models\ContentType $type, $perPage = null)
{
    $menu = \App\Models\MenuItem::ofContentType($type->id)->published()->first();

    if($menu) {
        return $menu->link();
    }

    $url = '/'.$type->slug.'/';

    if($perPage) {
        $url = per_page_url($url, $perPage);
    }

    return $url;
}

function content_url($content)
{
    if(is_numeric($content)) {
        $content = \App\Models\Content::find($content);
    }

    if(empty($content) || !($content instanceof \App\Models\Content)) {
        return null;
    }

    $menu = \App\Models\MenuItem::ofContent($content->id)->published()->first();

    if($menu) {
        return $menu->link();
    }

    $url = content_type_url($content->type);

    $url .= '/'.$content->slug.'/';

    return preg_replace('/(\/+)/', '/', $url);
}

function module_url(\App\Models\Product $product, \App\Models\Product $module)
{
    return modules_url($product).$module->slug.'/';
}

function sort_url($url, $attribute)
{
    $attribute = ($attribute === 'popular') ? '' : $attribute.'/';

    $parts = parse_url($url);

    $params = [];

    if(!empty($parts['query'])) {
        parse_str($parts['query'], $params);
    }

    if(empty($parts['path'])) {
        $parts['path'] = '';
    }

    $query = http_build_query($params);

    if($query) {
        $query = '?'.$query;
    }

    return $parts['path'].$attribute.$query;
}

function filter_nonsef_url($url, $attributes)
{
    if(empty($attributes)) {
        return $url;
    }

    $parts = parse_url($url);

    $params = [];

    if(!empty($parts['query'])) {
        parse_str($parts['query'], $params);
    }

    $params['filter'] = $attributes;

    if(empty($parts['path'])) {
        $parts['path'] = '';
    }

    return $parts['path'].'?'.http_build_query($params);
}

function product_url(\App\Models\Product $product, $module = null)
{
    if($product->category) {
        $url = $product->category->link();
    } else {
        $url = route('product.show', [$product->slug]);
    }

    $url .= '/'.$product->slug.'/';

    return preg_replace('/(\/+)/', '/', $url);
}

function filter_url(array $filters = [])
{
    if(empty($filters)) {
        return '';
    }

    $append = [];

    $allowed = ['price', 'novelty'];

    $labels = ['novelty'];

    $properties = \App\Models\Property::whereIn('slug', array_keys($filters))
        ->orderBy('ordering', 'asc')
        ->get()
        ->keyBy('slug');

    if(empty($properties)) {
        return '';
    }

    foreach($properties as $property) {
        $param = $filters[$property->slug];

        if(isset($param['min']) || isset($param['max'])) {
            $append[] = $property->slug.'-'.@intval($param['min']).'-'.@intval($param['max']);
            continue;
        }

        if(is_array($param)) {
            $propertyValues = $property->values()
                ->whereIn('id', $param)
                ->get()
                ->pluck('slug')
                ->toArray();

            if(empty($propertyValues)) {
                continue;
            }

            $append[] = implode('/', $propertyValues);

            continue;
        }

        $append[] = $param;
    }

    foreach($allowed as $name) {
        if(empty($filters[$name])) {
            continue;
        }

        $param = $filters[$name];

        if(isset($param['min']) || isset($param['max'])) {
            $param = @intval($param['min']).'-'.@intval($param['max']);
        }

        if(in_array($name, $labels)) {
            $append[] = $name;
            continue;
        }

        $append[] = $name.'-'.$param;
    }

    if(empty($append)) {
        return '';
    }

    return implode('/', $append).'/';
}

function category_url(\App\Models\Category $category, array $filters = [], $sortBy = null, $perPage = null)
{
    $url = '';

    $menu = \App\Models\MenuItem::ofCategory($category->id)->published()->first();

    if($menu) {
        $url = $menu->link();
    } else {
        $repository = app()->make('App\Repositories\Frontend\Category\CategoryContract');

        if($category->parent_id) {
            try {
                $parent = $repository->findById($category->parent_id);

                $url .= '/'.category_url($parent);
            } catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
                return '/';
            }
        }

        $url .= '/'.$category->slug.'/';

        $url = preg_replace('/(\/+)/', '/', $url);
    }

    $sefFilter = false;

    if($filters && $prettyUrl = pretty_url($category->id, $filters)) {
        $url = $prettyUrl;
        $filters = [];
    }

    return $url.modify_url($filters, $sortBy, $perPage, $sefFilter);
}

function pretty_url($id, $filters)
{
    $attr = array_flatten($filters);

    natsort($attr);

    $prettyUrl = \App\Models\PrettyUrl::where([
        'category_id' => $id,
        'options' => implode(',', $attr),
    ])->first();

    if(!$prettyUrl) {
        return false;
    }

    return $prettyUrl->pretty_url;
}

function modules_url(\App\Models\Product $product, array $filters = [], $sortBy = null, $perPage = null)
{
    $url = Cache::remember('modules_url.'.$product->id, config('cache.time'), function() use($product) {
        $url = product_url($product);

        return $url.'modules/';
    });

    return $url.modify_url($filters, $sortBy, $perPage);
}

function modify_url(array $filters = [], $sortBy = null, $perPage = null, $sefFilter = false)
{
    $url = '';

    if($sefFilter) {
        $filter = filter_url($filters);

        if($filter) {
            $url .= $filter;
        }
    } else {
        $url = filter_nonsef_url($url, $filters);
    }

    if($sortBy) {
        $url = sort_url($url, $sortBy);
    }

    if($perPage) {
        $url = per_page_url($url, $perPage);
    }

    return $url;
}

function catalog_url()
{
    return Cache::remember('catalog_url', config('cache.time'), function() {
        $menu = \App\Models\MenuItem::ofType('catalog')->published()->first();

        if($menu) {
            return menu_url($menu->id);
        }

        return route('frontend.category.catalog');
    });
}

function menu_url($id)
{
    $menu = \App\Models\MenuItem::find($id);

    if(!$menu || !$menu->published) {
        return '';
    }

    if($menu->type === 'link') {
        return $menu->link;
    }

    if($menu->type === 'menu_item') {
        return menu_url($menu->item_id);
    }

    $url = '';

    if($menu && $menu->parent_id) {
        $url .= '/'.menu_url($menu->parent_id);
    }

    if($menu->type !== 'header') {
        $url .= '/'.$menu->slug.'/';
    }

    return preg_replace('/(\/+)/', '/', $url);
}
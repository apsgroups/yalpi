<?php

Route::group(['prefix' => 'favorite', 'middleware' => 'auth'], function() {
    Route::get('/', [
        'as' => 'favorite.index',
        'uses' => 'FavoriteController@index',
    ]);

    Route::get('add/{id}', [
        'as' => 'favorite.add',
        'uses' => 'FavoriteController@add',
    ]);

    Route::get('remove/{id}', [
        'as' => 'favorite.remove',
        'uses' => 'FavoriteController@remove',
    ]);

    Route::post('clear', [
        'as' => 'favorite.clear',
        'uses' => 'FavoriteController@clear',
    ]);
});
<?php

Route::post('subscribe', [
    'as' => 'subscribe.add',
    'uses' => 'SubscribeController@add',
]);

Route::post('subscribe/coupon', [
    'as' => 'subscribe.coupon',
    'uses' => 'SubscribeController@coupon',
]);
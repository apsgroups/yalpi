<?php

/**
 * Frontend Controllers
 */
Route::get('/', 'FrontendController@index')->name('frontend.index');
Route::get('en', 'FrontendController@english')->name('frontend.english');

/**
 * These frontend controllers require the user to be logged in
 */
Route::group(['middleware' => 'auth'], function () {
    Route::group(['namespace' => 'User'], function() {
        Route::get('profile/favorites', 'DashboardController@index')->name('frontend.user.dashboard');
        Route::get('profile/comments', 'ProfileController@comments')->name('frontend.user.profile.comments');
        Route::get('profile/zadaniya-i-testy', 'ProfileController@tasks')->name('frontend.user.profile.tasks');
        Route::get('profile/skills', 'ProfileController@skills')->name('frontend.user.profile.skills');
        Route::get('profile/edit', 'ProfileController@edit')->name('frontend.user.profile.edit');
        Route::post('profile/avatar', 'ProfileController@avatar')->name('frontend.user.profile.avatar');
        Route::patch('profile/update', 'ProfileController@update')->name('frontend.user.profile.update');
    });
});

Route::group(['prefix' => 'blog/tags'], function() {
    Route::get('{tag}', [
        'as' => 'content.byTag',
        'uses' => 'ContentController@byTag',
    ]);
});
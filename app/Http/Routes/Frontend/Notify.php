<?php

Route::group(['middleware' => 'auth'], function () {
    Route::get('notifications', 'NotifyController@index');
    Route::post('notifications/{id}', 'NotifyController@remove');
});
<?php

Route::get('sitemap', [
    'as' => 'sitemap.html',
    'uses' => 'SitemapController@html',
]);

Route::get('sitemap.xml', [
    'as' => 'sitemap',
    'uses' => 'SitemapController@index',
]);

Route::group(['prefix' => 'sitemaps'], function() {
    Route::get('category/{id}.xml', [
        'as' => 'sitemap.category',
        'uses' => 'SitemapController@category',
    ]);

    Route::get('content/{id}.xml', [
        'as' => 'sitemap.content',
        'uses' => 'SitemapController@content',
    ]);

    Route::get('pages.xml', [
        'as' => 'sitemap.pages',
        'uses' => 'SitemapController@pages',
    ]);
});
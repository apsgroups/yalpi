<?php

Route::group(['prefix' => 'filter'], function() {
    Route::get('options', 'FilterController@options');
    
    Route::get('products', 'FilterController@products')
        ->name('frontend.filter.products');
});
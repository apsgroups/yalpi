<?php

Route::group(['prefix' => 'product/{productId}/reviews'], function() {
    Route::get('create', [
        'as' => 'frontend.product.reviews.create',
        'uses' => 'ProductReviewController@create',
        'middleware' => 'auth',
    ]);

    Route::post('/', [
        'as' => 'frontend.product.reviews.store',
        'uses' => 'ProductReviewController@store',
        'middleware' => 'auth',
    ]);

    Route::get('/', [
        'as' => 'frontend.product.reviews.index',
        'uses' => 'ProductReviewController@index',
        //'middleware' => 'ajax',
    ]);
});

Route::group(['prefix' => 'product/reviews'], function() {
    Route::post('/{reviewId}/like', [
        'as' => 'frontend.product.reviews.like',
        'uses' => 'ProductReviewController@like',
        'middleware' => 'auth',
    ]);

    Route::get('/{reviewId}/reply', [
        'as' => 'frontend.product.reviews.reply',
        'uses' => 'ProductReviewController@reply',
        'middleware' => 'auth',
    ]);

    Route::post('/{reviewId}/reply', [
        'as' => 'frontend.product.reviews.reply.store',
        'uses' => 'ProductReviewController@replyStore',
        'middleware' => 'auth',
    ]);
});
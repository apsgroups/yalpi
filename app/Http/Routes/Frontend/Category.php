<?php

Route::get('/catalog', [
    'as' => 'frontend.category.catalog',
    'uses' => 'CategoryController@catalog',
]);
<?php

Route::group(['prefix' => 'authors'], function() {
    Route::get('{slug}', [
        'as' => 'author.items',
        'uses' => 'AuthorController@items',
    ]);
});
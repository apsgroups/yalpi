<?php

Route::get('order/xml/{uuid}', [
    'as' => 'frontend.order.xml',
    'uses' => 'OrderController@xml',
]);
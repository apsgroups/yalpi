<?php

Route::group(['namespace' => 'Feedback'], function () {
    // Oneclick
    Route::group(['prefix' => 'oneclick'], function () {
        Route::get('thank-you', [
            'as' => 'feedback.oneclick.thankyou',
            'uses' => 'OneclickController@thankYou',
        ]);

        Route::get('{id}', [
            'as' => 'feedback.oneclick',
            'uses' => 'OneclickController@index',
        ]);

        Route::post('{id}', [
            'as' => 'feedback.oneclick',
            'uses' => 'OneclickController@send',
        ]);
    });

    // Callback
    Route::group(['prefix' => 'callback'], function () {
        Route::get('thank-you', [
            'as' => 'feedback.callback.thankyou',
            'uses' => 'CallbackController@thankYou',
        ]);

        Route::get('{id?}', [
            'as' => 'feedback.callback',
            'uses' => 'CallbackController@index',
        ]);

        Route::post('/', [
            'as' => 'feedback.callback',
            'uses' => 'CallbackController@send',
        ]);
    });

    // Consult
    Route::group(['prefix' => 'consult'], function () {
        Route::get('thank-you', [
            'as' => 'feedback.consult.thankyou',
            'uses' => 'ConsultController@thankYou',
        ]);

        Route::get('{id?}', [
            'as' => 'feedback.consult',
            'uses' => 'ConsultController@index',
        ]);

        Route::post('/', [
            'as' => 'feedback.consult',
            'uses' => 'ConsultController@send',
        ]);
    });

    // Complain
    Route::group(['prefix' => 'pozhalovatsya'], function () {
        Route::get('/', [
            'as' => 'feedback.complain',
            'uses' => 'ComplainController@index',
        ]);

        Route::post('/', [
            'as' => 'feedback.complain',
            'uses' => 'ComplainController@send',
        ]);

        Route::get('thank-you', [
            'as' => 'feedback.complain.thankyou',
            'uses' => 'ComplainController@thankYou',
        ]);
    });

    // Thank
    Route::group(['prefix' => 'poblagodarit'], function () {
        Route::get('/', [
            'as' => 'feedback.thank',
            'uses' => 'ThankController@index',
        ]);

        Route::post('/', [
            'as' => 'feedback.thank',
            'uses' => 'ThankController@send',
        ]);

        Route::get('thank-you', [
            'as' => 'feedback.thank.thankyou',
            'uses' => 'ThankController@thankYou',
        ]);
    });

    // Contact
    Route::group(['prefix' => 'contact'], function () {
        Route::get('/', [
            'as' => 'feedback.contact',
            'uses' => 'ContactController@index',
        ]);

        Route::post('/', [
            'as' => 'feedback.contact',
            'uses' => 'ContactController@send',
        ]);

        Route::get('thank-you', [
            'as' => 'feedback.contact.thankyou',
            'uses' => 'ContactController@thankYou',
        ]);
    });

    // Question
    Route::group(['prefix' => 'question'], function () {
        Route::get('/', [
            'as' => 'feedback.question',
            'uses' => 'QuestionController@index',
        ]);

        Route::post('/', [
            'as' => 'feedback.question',
            'uses' => 'QuestionController@send',
        ]);

        Route::get('thank-you', [
            'as' => 'feedback.contact.question',
            'uses' => 'QuestionController@thankYou',
        ]);
    });

    // Director
    Route::group(['prefix' => 'director'], function () {
        Route::get('/', [
            'as' => 'feedback.director',
            'uses' => 'DirectorController@index',
        ]);

        Route::post('/', [
            'as' => 'feedback.director',
            'uses' => 'DirectorController@send',
        ]);

        Route::get('thank-you', [
            'as' => 'feedback.director.thankyou',
            'uses' => 'DirectorController@thankYou',
        ]);
    });

    Route::group(['prefix' => 'proposal'], function () {
        Route::get('/', [
            'as' => 'feedback.proposal',
            'uses' => 'ProposalController@index',
        ]);

        Route::post('/', [
            'as' => 'feedback.proposal',
            'uses' => 'ProposalController@send',
        ]);

        Route::get('thank-you', [
            'as' => 'feedback.proposal.thankyou',
            'uses' => 'ProposalController@thankYou',
        ]);
    });
});
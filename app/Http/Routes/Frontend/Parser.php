<?php

if(Request::segment(1) !== 'admin') {
    $routeService = new \App\Services\RouteParserService();
    $routeService->setSegments(request()->segments());
    if($routeService->parse()) {
        \Illuminate\Support\Facades\Request::merge($routeService->getQuery());

        Route::get('{query}', [
            'uses' => $routeService->getAction(),
        ])->where('query', '.+');
    }
}
<?php

Route::group(['prefix' => 'product'], function() {
    Route::post('module/quantity', [
        'as' => 'product.module.quantity',
        'uses' => 'ProductController@moduleQuantity',
        //'middleware' => 'ajax',
    ]);

    Route::get('{id}/module/similar', [
        'as' => 'product.module.similar',
        'uses' => 'ProductController@similar',
        //'middleware' => 'ajax',
    ]);

    Route::get('{id}/similar', [
        'as' => 'product.similar',
        'uses' => 'ProductController@similarProducts',
    ]);

    Route::get('filter', [
        'as' => 'frontend.product.filter',
        'uses' => 'ProductController@filter',
    ]);
});
<?php

Route::get('search/autocomplete', [
    'as' => 'search.autocomplete',
    'uses' => 'SearchController@autocomplete',
]);

Route::get('search/menu', [
    'as' => 'search.menu',
    'uses' => 'SearchController@menu',
]);

Route::get('search/{sort_by?}', [
    'as' => 'search.index',
    'uses' => 'SearchController@index',
])->where('sort_by', '^(higher-price|lower-price)$');
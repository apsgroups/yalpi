<?php

Route::group(['prefix' => '{scope}/{id}/comments'], function() {
    Route::get('create', [
        'as' => 'frontend.comments.create',
        'uses' => 'CommentController@create',
        'middleware' => 'auth',
    ]);

    Route::post('/', [
        'as' => 'frontend.comments.store',
        'uses' => 'CommentController@store',
        'middleware' => 'auth',
    ]);

    Route::get('/', [
        'as' => 'frontend.comments.index',
        'uses' => 'CommentController@index',
        //'middleware' => 'ajax',
    ]);
});

Route::group(['prefix' => 'comments'], function() {
    Route::post('/{id}/like', [
        'as' => 'frontend.comments.like',
        'uses' => 'CommentController@like',
        'middleware' => 'auth',
    ]);

    Route::get('/{id}/reply', [
        'as' => 'frontend.comments.reply',
        'uses' => 'CommentController@reply',
        'middleware' => 'auth',
    ]);

    Route::post('/{id}/reply', [
        'as' => 'frontend.comments.reply.store',
        'uses' => 'CommentController@replyStore',
        'middleware' => 'auth',
    ]);
});
<?php

Route::group(['prefix' => 'cart'], function() {
    Route::get('/', [
        'as' => 'cart.index',
        'uses' => 'CartController@index',
    ]);

    Route::post('add', [
        'as' => 'cart.add',
        'uses' => 'CartController@add',
    ]);

    Route::post('checkout', [
        'as' => 'cart.checkout',
        'uses' => 'CartController@checkout',
    ]);

    Route::get('thank-you', [
        'as' => 'cart.thankyou',
        'uses' => 'CartController@thankyou',
    ]);

    Route::get('clear', [
        'as' => 'cart.clear',
        'uses' => 'CartController@clear',
    ]);

    Route::post('recalc', [
        'as' => 'cart.recalc',
        'uses' => 'CartController@recalc',
    ]);

    Route::post('remove', [
        'as' => 'cart.remove',
        'uses' => 'CartController@remove',
    ]);

    Route::post('coupon', [
        'as' => 'cart.coupon',
        'uses' => 'CartController@coupon',
    ]);
});
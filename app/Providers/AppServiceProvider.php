<?php

namespace App\Providers;

use App\Vendor\laravel\framework\src\Illuminate\Routing\UrlGenerator;
use Carbon\Carbon;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    protected $bindings = [
        \App\Repositories\Backend\Category\CategoryContract::class => \App\Repositories\Backend\Category\EloquentCategoryRepository::class,
        \App\Repositories\Backend\Product\ProductContract::class => \App\Repositories\Backend\Product\EloquentProductRepository::class,
        \App\Repositories\Backend\Widget\WidgetContract::class => \App\Repositories\Backend\Widget\EloquentWidgetRepository::class,
        \App\Repositories\Backend\Property\PropertyContract::class => \App\Repositories\Backend\Property\EloquentPropertyRepository::class,

        \App\Repositories\Frontend\Category\CategoryContract::class => \App\Repositories\Frontend\Category\EloquentCategoryRepository::class,
        \App\Repositories\Frontend\Product\ProductContract::class => \App\Repositories\Frontend\Product\EloquentProductRepository::class,
    ];

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // In case if Redis server has gone away for some reasons  set up drivers to 'file'
        try {
            \Illuminate\Support\Facades\Redis::ping();
        } catch (\Exception $e) {       
            \Config::set('session.driver', 'file');
            \Config::set('cache.default', 'file');

            \Cache::remember('redis_server_status', 60, function() use ($e) {
                report_email($e);
                return 'ok';
            });
        }

        /**
         * Application locale defaults for various components
         *
         * These will be overridden by LocaleMiddleware if the session local is set
         */

        /**
         * setLocale for php. Enables ->formatLocalized() with localized values for dates
         */
        setLocale(LC_TIME, config('app.locale_php'));

        /**
         * setLocale to use Carbon source locales. Enables diffForHumans() localized
         */
        Carbon::setLocale(config('app.locale'));

        \Blade::directive('src', function($arguments) {
            list($image, $size) = explode(', ', ltrim(rtrim($arguments, ')'), '('));

            return 'src="<?php echo img_src('.$image.', '.$size.') ?>"
                 srcset="<?php echo img_src('.$image.', '.$size.'.\'_2x\') ?> 2x"
                    <?php echo @getimagesize(public_path(img_src('.$image.', '.$size.')))[3] ?>';
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerBindings();

        $this->app->singleton('properties', function ($app) {
            return new \App\Services\Properties($app['App\Models\Property']);
        });

        $this->app->singleton('cart', function ($app) {
            return new \App\Services\Cart\CartService(
                $app['App\Repositories\Frontend\Product\ProductContract'],
                $app['Illuminate\Http\Request']
                );
        });

        $this->app->singleton('favorite', function ($app) {
            return new \App\Services\FavoriteService($app['App\Services\FavoriteService']);
        });

        $this->app->singleton('compare', function ($app) {
            return new \App\Services\CompareService($app['App\Repositories\Frontend\Compare\CompareContract']);
        });

        $this->app->singleton('block', function ($app) {
            return new \App\Services\Content\BlockService();
        });

        $this->app->singleton('settings', function ($app) {
            return new \App\Services\SettingService;
        });

        $this->app->singleton('widgets', function ($app) {
            return new \App\Services\Widget\Render\WidgetRender();
        });

        $this->app->bind('url', function($app) {
            return new UrlGenerator($app['router']->getRoutes(), request());
        });
    }

    protected function registerBindings()
    {
        foreach($this->bindings as $abstract => $concrete) {
            $this->app->bind($abstract, $concrete);
        }
    }


}


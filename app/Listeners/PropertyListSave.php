<?php

namespace App\Listeners;

use App\Events\PropertySaved;
use App\Services\Image\ImageService;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\PropertyValue;

class PropertyListSave
{

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PropertySaved  $event
     * @return void
     */
    public function handle(PropertySaved $event)
    {
        if(!in_array($event->property->type, ['list', 'color']))
            return;

        $ids = [];

        foreach($event->attributes['values'] as $k => $value) {
            if(empty($value['image_id'])) {
                $value['image_id'] = null;
            }

            if(isset($value['id']) && $value['id'] > 0) {
                $model = PropertyValue::findOrFail($value['id']);
                $model->update($value);
            } else {
                $model = $event->property
                    ->values()
                    ->create(array_only($value, ['title', 'slug', 'image_id']));
            }

            $ids[] = $model->id;

            $imgKey = 'values.'.$k.'.image';
            if(request()->hasFile($imgKey)) {
                $image = (new ImageService($imgKey, $model))->upload();

                $model->image_id = $image->id;
                $model->save();
            }
        }

        PropertyValue::whereNotIn('id', $ids)
            ->where('property_id', $event->property->id)
            ->delete();
    }
}

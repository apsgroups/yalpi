<?php

namespace App\Listeners;

use App\Events\ProductSaved;
use App\Models\Product;
use App\Models\ProductProperty;
use App\Models\Property;
use App\Models\PropertyValue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Input;

class ProductPropertyListSave
{

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ProductSaved  $event
     * @return void
     */
    public function handle(ProductSaved $event)
    {
        $properties = Property::whereIn('type', ['color', 'list', 'double'])->get();

        if($properties->count() === 0)
            return;


        $productProperties = !empty($event->attributes['property']) ? $event->attributes['property'] : [];

        $list = [];
        foreach($properties as $property) {
            if(!isset($productProperties[$property->id])) {
                $list[] = $property->id;
                continue;
            }

            $this->save($event->product, $property, $productProperties[$property->id]);
        }

        if($list) {
            ProductProperty::where('product_id', $event->product->id)->whereIn('property_id', $list)->delete();
        }
    }

    protected function save(Product $product, Property $property, $value) {
        $fields = [
            'list' => 'property_value_id',
            'color' => 'property_value_id',
            'double' => 'value_double',
            'string' => 'value_string',
        ];

        $list = [];

        if(!$property->multiple) {
            $value = (array)$value;
        }

        $isList = in_array($property->type, ['color', 'list']);

        $propertyValue = [
            'property_id' => $property->id,
            'product_id' => $product->id,
        ];

        foreach($value as $content) {
            if(empty($content)) {
                continue;
            }

            $row = $propertyValue;

            // for search condition in list and colors
            if($isList) {
                $row[$fields[$property->type]] = $content;
            }

            $productProperty = ProductProperty::where($row)->first();

            $row[$fields[$property->type]] = $content;

            $row['title'] = $content;

            if($fields[$property->type] === 'property_value_id') {
                $row['title'] = PropertyValue::find($content)->title;
            }

            if($productProperty) {
                if(!$isList) {
                    $productProperty->update($row);
                }

                $list[] = $productProperty->id;
                continue;
            }

            $list[] = ProductProperty::create($row)->id;
        }

        if($list) {
            ProductProperty::where($propertyValue)->whereNotIn('id', $list)->delete();
        } else {
            ProductProperty::where($propertyValue)->delete();
        }
    }
}

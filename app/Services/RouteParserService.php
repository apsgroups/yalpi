<?php

namespace App\Services;

use App\Models\PrettyUrl;
use App\Models\Property;
use App\Models\PropertyValue;
use App\Services\Frontend\SortingService;
use Illuminate\Support\Facades\Request;

class RouteParserService
{
    protected $segments;
    protected $query = [];
    protected $type;
    protected $actions = [
        'catalog' => 'CategoryController@catalog',
        'category' => 'CategoryController@index',
        'product' => 'ProductController@show',
        'modules' => 'CategoryController@modules',
        'contentType' => 'ContentController@index',
        'content' => 'ContentController@show',
        'menu_item' => 'FrontendController@index',
    ];

    /**
     * @return mixed
     */
    public function getAction()
    {
        $type = $this->getType();
        return $this->actions[$type];
    }

    /**
     * @return mixed
     */
    public function getSegments()
    {
        return $this->segments;
    }

    /**
     * @param mixed $path
     */
    public function setSegments($segments)
    {
        $this->segments = $segments;
    }

    /**
     * @return mixed
     */
    public function getQuery($name = null)
    {
        if(is_null($name)) {
            return $this->query;
        }

        if(!isset($this->query[$name])) {
            return null;
        }

        return $this->query[$name];
    }

    public function setQuery($query)
    {
        $this->query = $query;
    }

    /**
     * @param mixed $id
     */
    public function addQuery($name, $value, $array = false)
    {
        if($array) {
            $this->query[$name][] = $value;
            return;
        }

        $this->query[$name] = $value;
    }

    /**
     * @return mixed
     */
    public function getType($type=null)
    {
        if(is_null($this->type)) {
            return $type;
        }

        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    protected function removeSegment($slug = null)
    {
        $segments = $this->getSegments();

        if($slug) {
            $key = array_search($slug, $segments);
            unset($segments[$key]);
            $segments = array_values($segments);
        } else {
            array_shift($segments);
        }

        $this->setSegments($segments);
    }

    public function parse()
    {
        $this->findPrettyUrl();

        $this->findMenuItem();

        $this->findCategory();
        $this->findProduct();

        $this->findModules();
        //$this->findFilterParams();
        //$this->findContentType();
        $this->findContent();
        $this->findOrdering();

        if($this->getType() && !empty($this->getSegments())) {
            return null;
        }

        $data = [
            'query' => $this->getQuery(),
            'type' => $this->getType(),
        ];

        if(empty($data['type'])) {
            return null;
        }

        $this->setType($data['type']);
        $this->setQuery($data['query']);

        return $data['type'];
    }

    protected function findPrettyUrl()
    {
        $segments = $this->getSegments();

        if(empty($segments)) {
            return;
        }

        $sort = $segments[count($segments) - 1];

        $sortingService = new SortingService();

        $url = '/'.implode('/', $segments).'/';

        $rmCount = count($this->getSegments())-1;

        if(in_array($sort, $sortingService->getNames())) {
            $url = str_replace($sort.'/', '', $url);
            $rmCount--;
        }

        $prettyUrl = PrettyUrl::where('pretty_url', $url)->first();

        if(empty($prettyUrl)) {
            return;
        }

        for($i = 0; $i <= $rmCount; $i++) {
            $this->removeSegment();
        }

        $this->setType('category');
        $this->addQuery('id', $prettyUrl->category_id);

        $menuItem = \App\Models\MenuItem::ofCategory($prettyUrl->category_id)->published(1)->first();

        if($menuItem) {
            $this->addQuery('menu_item_id', $menuItem->id);
        }

        $options = explode(',', $prettyUrl->options);

        $propertyValues = PropertyValue::whereIn('id', $options)->with('property')->get();

        $filter = [];

        foreach($propertyValues as $v) {
            $filter[$v->property->slug][] = $v->id;
        }

        $this->addQuery('filter', $filter);

        $this->findOrdering();
    }

    protected function findMenuItem()
    {
        $segments = $this->getSegments();

        if(empty($segments)) {
            return;
        }

        $parentId = null;
        foreach($segments as $slug) {
            $menuItem = \App\Models\MenuItem::slug($slug)->where('parent_id', $parentId)->first();
            if(empty($menuItem)) {
                return;
            }

            if(in_array($menuItem->type, ['link', 'header'])) {
                return;
            }

            $parentId = $menuItem->id;

            if($menuItem->type === 'menu_item') {
                $menuItem = \App\Models\MenuItem::find($menuItem->item_id);

                \Log::alert('Requested MENU_ITEM '.$menuItem->id.', '. $_SERVER['REQUEST_URI']);

                return;
            }

            if(empty($menuItem)) {
                \Log::info('Empty menu item id in '. $_SERVER['REQUEST_URI']);
                //return;
            }

            $this->setType($menuItem->type);

            $this->addQuery('id', $menuItem->item_id);
            $this->addQuery('menu_item_id', $menuItem->id);
            $this->removeSegment();
        }
    }

    protected function findCategory()
    {
        $segments = $this->getSegments();

        if(empty($segments) || ($this->getType('category') !== 'category')) {
            return;
        }

        foreach($segments as $slug) {
            $category = \App\Models\Category::slug($slug)->where('parent_id', $this->getQuery('id'))->first();

            if(empty($category)) {
                return;
            }

            $this->setType('category');
            $this->addQuery('id', $category->id);
            $this->removeSegment();
        }

    }

    protected function findProduct($module = false)
    {
        $segments = $this->getSegments();

        if(empty($segments) || !in_array($this->getType(), ['category', 'product', 'modules'])) {
            return;
        }

        foreach($segments as $slug) {
            $product = \App\Models\Product::slug($slug);

            if($module) {
                $product = $product->whereHas('moduleParents', function ($query) {
                    $query->where('product_id', $this->getQuery('module_parent_id'));
                });
            } else {
                $product = $product->where('category_id', $this->getQuery('id'));
            }

            $product = $product->first();
            
            if(empty($product)) {
                return;
            }

            $this->setType('product');
            $this->addQuery('id', $product->id);
            $this->removeSegment();
        }
    }

    protected function findModules()
    {
        $segments = $this->getSegments();

        if(empty($segments)) {
            return;
        }

        if($this->getType() != 'product') {
            return;
        }

        if(empty($segments) || $segments[0] !== 'modules') {
            return;
        }

        $this->setType('modules');
        $this->addQuery('module_parent_id', $this->getQuery('id'));
        $this->removeSegment();

        $this->findProduct(true);
    }

    protected function findFilterParams()
    {
        $segments = $this->getSegments();

        if(empty($segments)) {
            return;
        }

        if(!in_array($this->getType(), ['category', 'modules'])) {
            return;
        }

        if($this->getType() === 'category') {                
            try {
                $categoryId = $this->getQuery('id');
                $category = \App\Models\Category::find($categoryId);

                if($category && !$category->meta_filter_id) {
                    return;
                }
            } catch(\Exception $e) { }
        }
        
        $propertiesIds = Property::where('filter', 1)->get();

        $propertyValues = PropertyValue::with('property')
                ->whereIn('slug', $segments)
                ->whereIn('property_id', $propertiesIds->pluck('id'))
                ->orderBy('ordering')
                ->get()
                ->keyBy('slug');

        $query = request()->get('filter');

        foreach($segments as $slug) {
            if(empty($propertyValues[$slug])) {
                continue;
            }

            $query[$propertyValues[$slug]->property->slug][] = $propertyValues[$slug]->id;

            $this->removeSegment($slug);
        }

        $this->addQuery('filter', $query);

        $segments = $this->getSegments();

        $property = Property::where('type', 'double')->get();

        $allowed = array_merge($property->pluck('slug')->toArray(), ['price', 'novelty']);

        $labels = ['novelty'];

        foreach($segments as $slug) {
            $field = explode('-', $slug);

            if(!in_array($field[0], $allowed)) {
                return;
            }

            $param = null;

            if(in_array($field[0], $labels)) {
                $param = [true];
            }

            if(count($field) == 3) {
                $param = [
                    'min' => $field[1],
                    'max' => $field[2],
                ];
            }

            if(count($field) == 2) {
                $param = $field[1];
            }

            if($param === null) {
                return;
            }

            $query[$field[0]] = $param;
            $this->removeSegment();
        }

        $this->addQuery('filter', $query);
    }

    protected function findContentType()
    {
        $segments = $this->getSegments();

        if(empty($segments) || $this->getType('contentType') !== 'contentType') {
            return;
        }

        foreach($segments as $slug) {
            $contentType = \App\Models\ContentType::slug($slug)->first();
            if(empty($contentType)) {
                return;
            }

            $this->setType('contentType');
            $this->addQuery('id', $contentType->id);
            $this->removeSegment();
        }
    }

    protected function findContent()
    {
        $segments = $this->getSegments();

        if(empty($segments) || !in_array($this->getType('content'), ['content', 'contentType'])) {
            return;
        }

        foreach($segments as $slug) {
            $content = \App\Models\Content::slug($slug);

            if($this->getType() === 'contentType') {
                $content = $content->where('content_type_id', $this->getQuery('id'));
            } else {
                return;
            }

            $content = $content->first();

            if(empty($content)) {
                return;
            }

            $this->setType('content');
            $this->addQuery('id', $content->id);
            $this->removeSegment();
        }
    }

    protected function findOrdering()
    {
        $segments = $this->getSegments();

        if(empty($segments) || !in_array($this->getType(), ['category', 'modules'])) {
            return;
        }

        $sortingService = new SortingService();
        if(!in_array($segments[0], $sortingService->getNames())) {
            return;
        }

        $this->addQuery($sortingService->getKeyAttribute(), $segments[0]);
        $this->removeSegment();

        if(empty($segments[1]) || !in_array($segments[1], ['asc', 'desc'])) {
            return;
        }

        $this->addQuery($sortingService->getKeyDirection(), $segments[1]);
        $this->removeSegment();
    }
}
<?php

namespace App\Services;

use App\Models\Setting;

class SettingService
{
    protected $settings;

    protected function getSettings()
    {
        if(is_null($this->settings)) {
            $this->setSettings();
        }

        return $this->settings;
    }

    protected function setSettings()
    {
        $this->settings = \Cache::remember('settings', config('cache.time'), function() {
            return Setting::all()->keyBy('title');
        });
    }

    public function get($title, $default = null)
    {
        $settings = $this->getSettings();

        if(isset($settings[$title])) {
            return $settings[$title]->content;
        }

        return $default;
    }

    public function getOr($primary, $secondary, $default = null)
    {
        if($param = $this->get($primary)) {
            return $param;
        }

        if($param = $this->get($secondary)) {
            return $param;
        }

        return $default;
    }

    public function emails($title)
    {
        $settings = $this->get($title, '');

        return explode(',', $settings);
    }
}
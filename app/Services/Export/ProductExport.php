<?php

namespace App\Services\Export;

class ProductExport {
    protected $path;

    public function run($categoryId)
    {
        $category = \App\Models\Category::findOrFail($categoryId);

        $products = $category->products()->get();

        $headers = ['ID', 'URL', 'Наименование'];

        $ind = [];

        $rows = [];

        foreach ($products as $product) {
            $data = [$product->id, url($product->link()), $product->title];
            $specs = convert_characteristics($product->characteristics);

            foreach ($specs as $value) {
                if(!in_array($value[0], $headers)) {
                    $headers[] = $value[0];
                    $ind[$value[0]] = count($headers) - 1;
                }

                $data[$ind[$value[0]]] = $value[1];
            }

            $rows[] = $data;         
        }

        $fh = fopen($this->getPath($categoryId), 'a');

        fputcsv($fh, $headers, ';');

        foreach ($rows as $row) {
            $r = [];

            foreach ($headers as $key => $value) {
                $r[] = !empty($row[$key]) ? $row[$key] : '';
            }
            
            fputcsv($fh, $r, ';');
        }

        fclose($fh);

        return $this->getPath();
    }

    protected function getPath($categoryId = 0)
    {
        if(empty($this->path)) {
            $this->path = storage_path($categoryId.'_product_export_'.date('Y-m-d-h-i-s').'.csv');
        }

        return $this->path;
    }
}
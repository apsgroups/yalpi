<?php

namespace App\Services\Meta;

use App\Models\Content;
use App\Models\PrettyUrl;
use App\Models\Property;
use App\Models\PropertyValue;
use Illuminate\Database\Eloquent\Model;

class MetaFilter extends MetaBase
{
    protected $template;

    protected $filter;

    protected $properties;

    protected $content;

    public function __construct(Model $model, $config)
    {
        parent::__construct($model);

        $this->filter = $config['filter'];

        $this->template = $this->model->metaFilter;

        $this->content = $this->findContent();
    }

    protected function findContent()
    {
        $url = category_url($this->model, $this->filter);

        return PrettyUrl::where('pretty_url', $url)->first();
    }

    public function title()
    {
        if(!empty($this->content) && !empty($this->content->page_title)) {
            return $this->content->page_title;
        }

        if(empty($this->template)) {
            return parent::title();
        }

        return $this->parseTemplate($this->template->meta_title);
    }

    public function description()
    {
        if(!empty($this->content) && !empty($this->content->metadesc)) {
            return $this->content->metadesc;
        }

        if(empty($this->template)) {
            return parent::description();
        }

        return $this->parseTemplate($this->template->meta_description);
    }

    public function keywords()
    {
        if(!empty($this->content) && !empty($this->content->metakeys)) {
            return $this->content->metakeys;
        }

        if(empty($this->template)) {
            return parent::keywords();
        }

        return $this->parseTemplate($this->template->meta_keywords);
    }

    public function heading()
    {
        if(!empty($this->content) && !empty($this->content->heading)) {
            return strip_tags($this->content->heading);
        }

        if(empty($this->template)) {
            return $this->model->alter_title ?: $this->model->title;
        }

        return $this->parseTemplate($this->template->heading);
    }

    public function text()
    {
    	if($this->model->id == 12 && count($this->filter) === 1 && !empty($this->filter['placement']) && count($this->filter['placement']) === 1 && $this->filter['placement'][0] == 39) {
            $category = \App\Models\Category::find(37);
            if($category && $category->description_bottom) {
            	return $category->description_bottom;
            }
    	}

        if(!empty($this->content)) {
           return  $this->content->description;
        }

        if(empty($this->template)) {
            return '';
        }

        return $this->parseTemplate($this->template->description);
    }

    protected function lower($string)
    {
        return mb_strtolower(mb_substr($string, 0, 1)).mb_substr($string, 1);
    }

    protected function parseTemplate($template)
    {
        $search = [
            '*category*' => $this->model->title,
            '*category|lower*' => $this->lower($this->model->title),
            '*price*' => $this->price(),
        ];

        $properties = $this->getParamReplace($template);

        $search = array_merge($search, $properties);

        $meta = str_replace(array_keys($search), array_values($search), $template);

        $meta = preg_replace('/\*.+?\*/', '', $meta);

        $meta = preg_replace('/\s+/', ' ', $meta);

        return trim($meta);
    }

    protected function getProperties()
    {
        if(!empty($this->properties)) {
            return $this->properties;
        }

        $fields = array_keys($this->filter);

        $this->properties = Property::whereIn('slug', $fields)->get();

        return $this->properties;
    }

    protected function getParamReplace($template)
    {
        $search = [];

        $properties = $this->getProperties();

        foreach($properties as $property) {
            $values = $this->getFilterValue($property->slug);

            preg_match('/(\*([^*]*)?('.$property->slug.'(\|(lower|plural))?)(.*?)\*)/', $template, $match);

            if(empty($match)) {
                continue;
            }

            if($property->type == 'double') {
                $tagValue = $this->range($values);
            } else {
                $propertyValues = $property->values()->whereIn('id', $values)->get();

                $field = (strpos($template, '|plural') !== false) ? 'plural' : 'title';

                $tagValue = $propertyValues->implode($field, ', ');

                if(strpos($template, '|lower') !== false) {
                    $tagValue =  mb_strtolower($tagValue);
                }
/*

                if($match[5] == 'plural') {
                    $tagValue = $propertyValues->implode('plural', ', ');
                } elseif($match[5] == 'lower') {
                    $tagValue =  mb_strtolower($propertyValues->implode('title', ', '));
                } else {
                    $tagValue = $propertyValues->implode('title', ', ');
                }*/
            }

            $search[$match[0]] = $match[2].$tagValue.$match[6];

            $search[$match[0]] = str_replace(['|lower', '|plural'], ['', ''], $search[$match[0]]);
        }

        return $search;
    }

    protected function getFilterValue($name)
    {
        if(isset($this->filter[$name])) {
            return $this->filter[$name];
        }

        return null;
    }

    protected function price()
    {
        $price = $this->getFilterValue('price');

        if(empty($price)) {
            return '';
        }

        if(isset($price['min'])) {
            $price['min'] = format_price($price['min']).' руб.';
        }

        if(isset($price['max'])) {
            $price['max'] = format_price($price['max']).' руб.';
        }

        return $this->range($price);
    }

    protected function range($range)
    {
        if(!empty($range['min']) && empty($range['max'])) {
            return 'от '.$range['min'];
        }

        if(!empty($range['min']) && !empty($range['max'])) {
            return 'от '.$range['min'].' до '.$range['max'];
        }

        if(empty($range['min']) && !empty($range['max'])) {
            return 'до '.$range['max'];
        }

        return '';
    }
}
<?php

namespace App\Services\Meta;

use App\Models\Category;
use Illuminate\Database\Eloquent\Model;

class MetaFactory
{
    public static function getInstance(Model $model, $config = null)
    {
        if($model instanceof Category && !empty($config['filter'])) {
            return new MetaFilter($model, $config);
        }

        $class = '\App\Services\Meta\Meta'.class_basename($model);

        if(!class_exists($class)) {
            $class = '\App\Services\Meta\MetaBase';
        }

        return new $class($model, $config);
    }
}
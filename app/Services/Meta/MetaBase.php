<?php

namespace App\Services\Meta;

use Illuminate\Database\Eloquent\Model;

class MetaBase
{
    protected $model;

    protected $config;

    public function __construct(Model $model, $config = null)
    {
        $this->model = $model;
        
        $this->config = $config;
    }

    public function title()
    {
        return $this->model->getPageTitle();
    }

    public function heading()
    {
        return $this->model->getHeading();
    }

    public function description()
    {
        return $this->model->getMetaDesc();
    }

    public function keywords()
    {
        return $this->model->getMetaKeys();
    }
}
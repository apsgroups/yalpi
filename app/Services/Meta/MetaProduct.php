<?php

namespace App\Services\Meta;

use App\Models\Product;
use Illuminate\Database\Eloquent\Model;

class MetaProduct extends MetaBase
{
    protected $template;

    public function __construct(Product $model, $config = null)
    {
        parent::__construct($model, $config);

        $this->template = $this->model->category->metaProduct;
    }

    public function title()
    {
        if($this->model->override_meta || empty($this->template)) {
            return parent::title();
        }

        return $this->parseTemplate($this->template->meta_title);
    }

    public function description()
    {
        if($this->model->override_meta || empty($this->template)) {
            return parent::description();
        }

        return $this->parseTemplate($this->template->meta_description);
    }

    public function keywords()
    {
        if($this->model->override_meta || empty($this->template)) {
            return parent::keywords();
        }

        return $this->parseTemplate($this->template->meta_keywords);
    }

    protected function lower($string)
    {
        return mb_strtolower(mb_substr($string, 0, 1)).mb_substr($string, 1);
    }

    protected function parseTemplate($template)
    {
        $authors = $this->model->authors->implode('title', ', ');

        $search = [
            '*title*' => $this->model->title,
            '*title|lower*' => $this->lower($this->model->title),
            '*category*' => $this->model->category->title,
            '*category|lower*' => $this->lower($this->model->category->title),
            '*price*' => number_format($this->model->price, 0, '', ' '),
            '*type_title*' => $this->model->type->title,
            '*type_title|lower*' => $this->lower($this->model->type->title),
            '*heading*' => $this->model->alter_title ?: $this->model->title,
            '*heading|lower*' => $this->lower(($this->model->alter_title ?: $this->model->title)),
            '*author*' => !empty($authors) ? ' от ' . $authors : '',
        ];

        $properties = $this->model->properties()->with('property')->get();

        foreach($properties as $property) {
            $search['*'.$property->property->slug.'*'] = $property->title;
            $search['*'.$property->property->slug.'|lower*'] = $this->lower($property->title);
        }

        return str_replace(array_keys($search), array_values($search), $template);
    }
}
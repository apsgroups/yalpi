<?php

namespace App\Services\Meta;

use Illuminate\Database\Eloquent\Model;
use App\Services\Filter\ProductFilter;
use App\Models\Product;
use App\Models\Property;
use App\Models\PropertyValue;

class MetaCategory extends MetaBase
{
    protected $template;

    protected $minPrice = false;

    public function __construct(Model $model, $config = null)
    {
        parent::__construct($model, $config);

        $this->template = $this->model->metaCategory;
    }

    public function title()
    {
        if(empty($this->template)) {
            return parent::title();
        }

        return $this->parseTemplate($this->template->meta_title);
    }

    public function heading()
    {
        if (empty($this->template)) {
            return $this->model->alter_title ?: $this->model->title;
        }

        return $this->parseTemplate($this->template->heading);
    }

    public function description()
    {
        if(empty($this->template)) {
            return parent::description();
        }

        return $this->parseTemplate($this->template->meta_description);
    }

    public function keywords()
    {
        if(empty($this->template)) {
            return parent::keywords();
        }

        return $this->prepareKeywords($this->parseTemplate($this->template->meta_keywords));
    }

    public function text()
    {
        return $this->model->description_bottom;
    }

    protected function lower($string)
    {
        return mb_strtolower(mb_substr($string, 0, 1)).mb_substr($string, 1);
    }

    protected function prepareKeywords($string)
    {
        return str_replace(['-', '«', '»'], [' ', '', ''], mb_strtolower($string));
    }

    protected function parseTemplate($template)
    {
        $search = [
            '*title*' => $this->model->title,
            '*title|lower*' => $this->lower($this->model->title),
            '*title_r*' => !empty($this->model->morph['R']) ? $this->model->morph['R'] : '',
            '*title_r|lower*' => !empty($this->model->morph['R']) ? $this->lower($this->model->morph['R']) : '',
            '*title_v*' => !empty($this->model->morph['R']) ? $this->model->morph['V'] : '',
            '*title_v|lower*' => !empty($this->model->morph['R']) ? $this->lower($this->model->morph['V']) : '',
            '*total*' => isset($this->config['total']) ? $this->config['total'] : 0,
            '*min_price*' => format_price($this->minPrice()),
            '*heading*' => $this->model->alter_title ?: $this->model->title,
            '*heading|lower*' => $this->lower(($this->model->alter_title ?: $this->model->title)),
        ];

        return str_replace(array_keys($search), array_values($search), $template);
    }

    public function minPrice()
    {
        if($this->minPrice !== false) {
            return $this->minPrice;
        }

        $this->minPrice = 0;

        $category = $this->model;

        $this->minPrice = \Cache::remember('category.'.$category->id.'.minPrice', config('cache.time'), function () use($category) {
            $attributes = [];

            $attributes['product_categories'][] = ($category->autofiltering && $category->filter_options) ? $category->autofiltering_category_id : $category->id;
            
            if($category->autofiltering && $category->filter_options) {
                foreach ($category->filter_options as $key => $v) {
                    if(stripos($key, 'range') !== false) {
                        $property = Property::find(str_replace('range_', '', $key));

                        if($v['min'] > 0) {                            
                            $attributes['range_'.$property->slug]['min'] = $v['min'];
                        }

                        if($v['max'] > 0) {                            
                            $attributes['range_'.$property->slug]['max'] = $v['max'];
                        }

                        continue;
                    }

                    $propertyValue = PropertyValue::find($v);

                    if(!$propertyValue) {
                        continue;
                    }

                    $attributes[$propertyValue->property->slug][] = $v;
                }
            }

            try {                
                $filter = new ProductFilter();

                $filter->setAttributes($attributes);

                $filter->setSortBy('price');

                $filter->setSortDir('asc');

                $filter->setPage(1, 1);

                $filter->apply();

                $ids = $filter->getIds();
            } catch(\Exception $e) {
                return 0;
            }

            if(!$ids) {
                return 0;
            }

            $product = Product::find($ids[0]);

            return $product->price;
        });

        return $this->minPrice;
    }
}
<?php

namespace App\Services;

use App\Models\Favorite;

class FavoriteService
{
    public function isActive($id)
    {
        $user = auth()->user();

        if(!$user) {
            return;
        }

        $favorite = $user->favorites()->ofProduct($id)->first();

        return $favorite;
    }

    public function all()
    {
        $user = auth()->user();

        if(!$user) {
            return collect([]);
        }

        return $user->favorites()->pluck('product_id');
    }

    public function count()
    {
        $user = auth()->user();

        if(!$user) {
            return 0;
        }

        return $user->favorites()->count();
    }

    public function add($id)
    {
        $user = auth()->user();

        if(!$user) {
            return;
        }

        return Favorite::firstOrCreate(['user_id' => $user->id, 'product_id' => $id]);
    }

    public function remove($id)
    {
        $user = auth()->user();

        if(!$user) {
            return;
        }

        $user->favorites()->ofProduct($id)->delete();
    }

    public function clear()
    {
        $user = auth()->user();

        if(!$user) {
            return;
        }

        $user->favorites()->delete();
    }
}
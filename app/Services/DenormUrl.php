<?php
namespace App\Services;

use App\Models\MenuItem;
use App\Models\Category;

class DenormUrl
{
    public function saveMenuUrl($menuItem)
    {
        $old = $menuItem->denorm_url;

        // ['header', 'horizontal_divider', 'menu_text', 'vertical_divider', 'space']
        $url = null;

        if(in_array($menuItem->type, ['menu_item', 'link', 'catalog', 'category', 'content', 'contentType'])) {
            $url = menu_url($menuItem->id);
        }

        if($old === $url) {
            return;
        }

        \DB::table('menu_items')
        ->where('id', $menuItem->id)
        ->update(['denorm_url' => $url]);

        $this->menuItemLinks($menuItem);

        $this->menuChildren($menuItem->children);

        if($menuItem->type === 'category') {
            $category = Category::find($menuItem->item_id);

            $this->saveCategoryUrl($category);
        }
    }

    public function menuItemLinks($menuItem) 
    {
        $items = MenuItem::where(['type' => 'menu_item', 'item_id' => $menuItem->id])->get();

        foreach($items as $item) {
            $this->saveMenuUrl($item);
        }
    }

    public function menuChildren($children) 
    {
        if(!$children->count()) {
            return;
        }
        
        foreach($children as $item) {
            $this->saveMenuUrl($item);
        }
    }

    public function categoryChildren($children) 
    {
        if(!$children->count()) {
            return;
        }

        foreach($children as $item) {
            $this->saveCategoryUrl($item);
        }
    }

    public function saveCategoryUrl($category)
    {
        if(!$category) {
            return;
        }

        $old = $category->denorm_url;

        $url = category_url($category);

        if($old === $url) {
            return;
        }

        \DB::table('categories')
            ->where('id', $category->id)
            ->update(['denorm_url' => $url]);

        $this->categoryChildren($category->children);
    }
}
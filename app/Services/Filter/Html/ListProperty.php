<?php

namespace App\Services\Filter\Html;

use App\Models\Property;
use App\Models\PropertyValue;

class ListProperty
{
    protected $properties;

    public function __construct()
    {
        $this->properties = [];
    }

    public function title($id)
    {
        if(empty($id)) {
            return '';
        }

        $propertyValue = $this->getPropertyValue($id);

        if(!$propertyValue || !$propertyValue->title) {
            return null;
        }

        return $propertyValue->title;
    }

    protected function getPropertyValue($id)
    {
        if((int)$id === 0) {
            return false;
        }

        if(isset($this->properties[$id])) {
            return $this->properties[$id];
        }

        $this->properties[$id] = PropertyValue::find($id);

        if(!$this->properties[$id]) {
            return false;
        }

        return $this->properties[$id];
    }

    public function image($id)
    {
        $propertyValue = $this->getPropertyValue($id);

        if(!$propertyValue || !$propertyValue->image_id || !$propertyValue->image) {
            return false;
        }

        return $propertyValue->image;
    }
}
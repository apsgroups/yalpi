<?php

namespace App\Services\Filter;

use App\Models\Property;

class MappingService
{
    protected $mapping;

    protected $types = [
        'double' => 'double',
        'list' => 'integer',
        'color' => 'integer',
        'default' => 'string',
    ];

    public function getMapping()
    {
        $mapping = config('elasticquent.mapping', []);

        $properties = Property::all();

        foreach($properties as $property) {
            $mapping[$this->getPropertyKey($property)] = [
                'type' => $this->getPropertyType($property),
                'index' => 'not_analyzed',
            ];
        }

        return $mapping;
    }

    public function getPropertyType(Property $property)
    {
        if(isset($this->types[$property->type])) {
            return $this->types[$property->type];
        }

        if($property->multiple) {
            return 'object';
        }

        return $this->types['default'];
    }

    public function getPropertyKey(Property $property)
    {
        return $property->slug;
    }
}
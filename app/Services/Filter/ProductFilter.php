<?php

namespace App\Services\Filter;

class ProductFilter
{
    protected $options;
    protected $hits;
    protected $from = 0;
    protected $perPage;
    protected $postFilter = true;
    protected $attributes;
    protected $aggs = true;
    protected $term = false;
    protected $pagination;
    protected $sortBy;
    protected $sortDir;

    protected function getQueryBuilder()
    {
        $queryBuilder = new \App\Services\Filter\FilterQueryBuilder();
        $queryBuilder->setFilterValues($this->getAttributes());
        $queryBuilder->getAggQuery();

        return $queryBuilder;
    }

    /**
     * @return mixed
     */
    public function getSortBy()
    {
        return $this->sortBy;
    }

    /**
     * @param mixed $sortBy
     */
    public function setSortBy($sortBy)
    {
        $this->sortBy = $sortBy;
    }

    /**
     * @return mixed
     */
    public function getSortDir()
    {
        return $this->sortDir;
    }

    /**
     * @param mixed $sortDir
     */
    public function setSortDir($sortDir)
    {
        $this->sortDir = $sortDir;
    }

    public function apply()
    {
        $queryBuilder = $this->getQueryBuilder();

        $query['body']['sort'] = $queryBuilder->getSortQuery($this->getSortBy(), $this->getSortDir());

        if(empty($query['body']['sort'])) {
            unset($query['body']['sort']);
        }

        $query['body']['query'] = $queryBuilder->getQuery();

        if($this->getTerm()) {
            $query['body']['query']['bool']['must']['bool'] = $queryBuilder->getTermQuery($this->getTerm());
        }

        if($this->getAggs()) {
            $query['body']['aggs'] = $queryBuilder->getAggQuery();
        }

        if($this->getPerPage()) {
            $query['body']['size'] = $this->getPerPage();
        }

        if($this->getFrom()) {
            $query['body']['from'] = $this->getFrom();
        }

        if($this->getPostFilter()) {
            $query['body']['post_filter'] = $queryBuilder->getPostFilterQuery();
        }

        $query['index'] = config('elasticquent.default_index');
        
        if($this->getTerm()) {
            $q = $query['body']['query'];

            $query['body']['query'] = [
                'function_score' => [
                    'query' => $q,
                    'field_value_factor' => [
                        'field' => 'search_priority',
                        'factor' => 100,
                        'modifier' => 'square',
                        'missing' => 1
                    ]
                ]
            ];
        }
        
        $this->search($query);
    }

    protected function search($query)
    {
        $search = \App\Models\Product::complexSearch($query);

        if($this->getPerPage()) {
            $this->setPagination($search->paginate($this->getPerPage()));
        }

        if($this->getAggs()) {
            $this->setOptions($search->getAggregations());
        }

        $this->setHits($search->getHits());
    }

    public function getIds()
    {
        $hits = $this->getHits();

        if(empty($hits['hits'])) {
            return [];
        }


        return array_pluck($hits['hits'], '_id');
    }

    /**
     * @return mixed
     */
    public function getPagination()
    {
        return $this->pagination;
    }

    /**
     * @param mixed $pagination
     */
    public function setPagination($pagination)
    {
        $this->pagination = $pagination;
    }

    /**
     * @return mixed
     */
    public function getAggs()
    {
        return $this->aggs;
    }

    /**
     * @param mixed $aggs
     */
    public function setAggs($aggs)
    {
        $this->aggs = $aggs;
    }

    /**
     * @return mixed
     */
    public function getTerm()
    {
        return $this->term;
    }

    /**
     * @param mixed $aggs
     */
    public function setTerm($term)
    {
        $this->term = $term;
    }

    /**
     * @return mixed
     */
    public function getPostFilter()
    {
        return $this->postFilter;
    }

    /**
     * @param mixed $postFilter
     */
    public function setPostFilter($postFilter)
    {
        $this->postFilter = $postFilter;
    }

    /**
     * @return mixed
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * @param mixed $attributes
     */
    public function setAttributes($attributes)
    {
        $this->attributes = $attributes;
    }

    /**
     * @return mixed
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * @param mixed $from
     */
    public function setFrom($from)
    {
        $this->from = $from;
    }

    public function setPage($perPage, $page)
    {
        if($perPage > 0) {
            $this->setPerPage($perPage);
        }

        $perPage = $this->getPerPage();

        $from = (($page - 1) * $perPage);

        if($from > 1) {
            $this->setFrom($from);
        }
    }

    /**
     * @return mixed
     */
    public function getHits()
    {
        return $this->hits;
    }

    /**
     * @param mixed $hits
     */
    public function setHits($hits)
    {
        $this->hits = $hits;
    }

    /**
     * @return mixed
     */
    public function getPerPage()
    {
        return $this->perPage;
    }

    /**
     * @param mixed $perPage
     */
    public function setPerPage($perPage)
    {
        $this->perPage = $perPage;
    }

    /**
     * @return mixed
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * @param mixed $options
     */
    public function setOptions($options)
    {
        $this->options = $options;
    }


}
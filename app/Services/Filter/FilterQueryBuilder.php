<?php

namespace App\Services\Filter;

use App\Services\Filter\Value\ValueFactory;

class FilterQueryBuilder
{
    protected $values;

    protected $filters;

    protected $valueFactory;

    protected $fields;

    public function __construct()
    {
        $this->fields = (new \App\Services\Filter\Field\FieldList())->getAll();
    }

    public function getFilterValues()
    {
        return $this->getValueFactory()->getValues();
    }

    public function setValueFactory(ValueFactory $value)
    {
        $this->valueFactory = $value;
    }

    public function getValueFactory()
    {
        return $this->valueFactory;
    }

    public function setFilterValues(array $values)
    {
        $valueFactory = new \App\Services\Filter\Value\ValueFactory();
        $valueFactory->fill($values);
        $this->setValueFactory($valueFactory);
    }

    protected function getFields()
    {
        return $this->fields;
    }

    protected function filterCondition()
    {
        if(!is_null($this->filters)) {
            return $this->filters;
        }

        $this->filters = [];

        $filterValues = $this->getFilterValues();

        if(empty($filterValues)) {
            return $this->filters;
        }

        foreach($filterValues as $value) {
            $name = $value->getName();

            if($name == 'module_parents' || $name == 'product_categories') {
                continue;
            }

            if($value instanceof \App\Services\Filter\Value\Range) {
                $this->filters[$name]['range'][$name]['gte'] = $value->getMin();

                if($value->getMax() >= $value->getMin()) {
                    $this->filters[$name]['range'][$name]['lte'] = $value->getMax();
                }
            } else {
                $field = [];
                foreach($value->get() as $term) {
                    $field['bool']['should'][]['term'][$name] = $term;
                }

                $this->filters[$name] = $field;
            }
        }

        return $this->filters;
    }


    protected function aggFilter($ignoreField)
    {
        $filterCondition = $this->filterCondition();

        if(isset($filterCondition[$ignoreField])) {
            unset($filterCondition[$ignoreField]);
        }

        $filters = [];

        if(count($filterCondition) === 0) {
            return $filters;
        }

        if(count($filterCondition) > 0) {
            $filters = array_values($filterCondition);
        }

        if(count($filters) === 1) {
            return $filters[0];
        }

        return [
            'bool' => [
                'must' => $filters
            ]
        ];
    }

    public function getPostFilterQuery()
    {
        $filterCondition = $this->filterCondition();
        $filters = array_values($filterCondition);

        return [
            'bool' => [
                'must' => $filters
            ]
        ];
    }

    public function getQuery()
    {
        $query = [
            'bool' => [
                'filter' => [
                    'bool' => [
                        'must' => [
                            [
                                'term' => [
                                    'published' => 1,
                                ],
                            ],
                        ],
                    ]
                ],
            ],
        ];

        $categories = $this->getValueFactory()->getValueByName('product_categories');

        if($categories && $categories->get()[0]) {
            foreach($categories->get() as $id) {
                $query['bool']['filter']['bool']['must'][]['term']['product_categories'] = $id;
            }
        } else {
            $moduleParents = $this->getValueFactory()->getValueByName('module_parents');

            if($moduleParents && $moduleParents->get()[0]) {
                foreach($moduleParents->get() as $id) {
                    $query['bool']['filter']['bool']['must'][]['term']['module_parents'] = $id;
                }
            }
        }

        return $query;
    }

    public function getTermQuery($term)
    {
        $query = [
            'should' => [/*
                [
                    'match' => [
                        'title' => [
                            'query' => $term, 
                            'operator' => 'and'
                        ]
                    ],
                ],*/
                [
                    'match' => [
                        'search_keys' => [
                            'query' => $term, 
                            'minimum_should_match' => 5
                        ]
                    ],
                ],
               // ['match' => ['search_keys' => $term]],
            ],
        ];

        return $query;
    }

    public function getAggQuery()
    {
        $query = [];

        foreach($this->getFields() as $field) {
            $name = $field->getName();

            if($field->isRange()) {
                $agg = $this->setRangeField($name);
            } else {
                $agg = $this->setTermsField($name);
            }

            $query[$name] = $agg;
        }

        return $query;
    }

    public function getSortQuery($attribute, $direction)
    {
        if(empty($attribute)) {
            return null;
        }

        if($attribute === 'category') {
            $categories = $this->getValueFactory()->getValueByName('product_categories');

            if($categories && $categories->get()) {
                return [
                    'out_of_stock' => [
                            'order' => 'asc',
                        ],
                    'search_priority' => [
                            'order' => 'desc',
                        ],
                    'category_priority' => [
                            'order' => 'desc',
                        ],
                    'price_priority' => [
                            'order' => 'asc',
                        ],/*
                    'in_stock' => [
                            'order' => 'desc',
                        ],*/
                    'category_order.ordering' => [
                            'order' => 'asc',
                            'nested_path' => 'category_order',
                            'nested_filter' => [
                                'term' => [
                                    'category_order.category_id' => $categories->get()[0],
                                ],
                            ],
                        ],
                    ];
            }

            return null;
        }

        return [
            'out_of_stock' => [
                    'order' => 'asc',
                ],
            'search_priority' => [
                    'order' => 'desc',
                ],
            $attribute => [
                'order' => $direction,
            ],
        ];
    }

    public function setRangeField($name)
    {
        $filter = $this->aggFilter($name);

        if($filter) {
            $query['filter'] = $filter;
        } else {
            $query['filter']['range'][$name]['gte'] = 0;
        }

        $query['aggs']['min_value']['min']['field'] = $name;
        $query['aggs']['max_value']['max']['field'] = $name;

        return $query;
    }

    public function setTermsField($name)
    {
        $filter = $this->aggFilter($name);

        $query['terms']['field'] = $name;
        $query['terms']['size'] = 10000;

        if($filter) {
            $query['aggs']['active']['filter'] = $filter;
        } else {
            $query['aggs']['active']['value_count']['field'] = $name;
        }

        return $query;
    }
}
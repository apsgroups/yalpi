<?php

namespace App\Services\Filter\Field;

class FieldList
{
    protected $fields;

    public function __construct()
    {
        //$this->addField('price', true);
        $this->addField('novelty');
        //$this->addField('sale');
        $this->addField('in_stock');
        $this->addField('product_categories');
        $this->addField('module_parents');
        $this->addField('product_composition');
        $this->addField('product_type_id');
        $this->addField('rate');

        $this->properties();
    }

    public function getAll()
    {
        return $this->fields;
    }

    protected function properties()
    {
        $properties = \App\Models\Property::filter(1)
                ->published(1)
                ->get();

        if($properties->count() === 0) {
            return;
        }

        foreach($properties as $property) {
            if($property->type === 'double') {
                $this->addField('range_'.$property->slug, true);
            }

            $this->addField($property->slug, false);
        }

        return;
    }

    protected function addField($name, $range = false)
    {
        $field = new \App\Services\Filter\Field\Field();
        $field->setName($name);
        $field->setRange($range);
        $this->fields[$name] = $field;
    }
}
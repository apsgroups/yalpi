<?php

namespace App\Services\Filter\Field;

class Field
{
    protected $name = false;
    protected $range = false;

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setRange($range)
    {
        $this->range = $range;
    }

    public function isRange()
    {
        return $this->range;
    }
}
<?php

namespace App\Services\Filter\Value;

use App\Services\Filter\Field\Field;

class ValueFactory
{
    protected $values;

    protected $inputValues;

    public function fill(array $inputValues)
    {
        $this->setInputValues($inputValues);

        $fields = (new \App\Services\Filter\Field\FieldList())->getAll();

        foreach ($fields as $field) {
            $this->createValue($field);
        }
    }

    public function getValues()
    {
        return $this->values;
    }

    public function getValueByName($name)
    {
        if(isset($this->values[$name])) {
            return $this->values[$name];
        }

        return null;
    }

    protected function setInputValues($values)
    {
        $this->inputValues = $values;
    }

    protected function getInputValue($name)
    {
        if(isset($this->inputValues[$name])) {
            return $this->inputValues[$name];
        }

        return null;
    }

    protected function createValue(Field $field)
    {
        if ($field->isRange()) {
            $value = $this->addRangeValue($field);
        } else {
            $value = $this->addPlainValue($field);
        }

        if(!is_null($value)) {
            $this->addValue($value);
        }
    }

    protected function addRangeValue(Field $field)
    {
        $name = $field->getName();
        $range = $this->getInputValue($name);

        if(is_null($range)) {
            return null;
        }

        $value = new Range();

        if(isset($range['min'])) {
            $min = doubleval($range['min']);
            $value->setMin($min);
        }

        if(isset($range['max'])) {
            $max = doubleval($range['max']);
            $value->setMax($max);
        }

        $value->setName($name);

        return $value;
    }

    protected function addPlainValue(Field $field)
    {
        $fill = $this->getInputValue($field->getName());

        if(is_null($fill)) {
            return null;
        }

        $value = new Plain();

        $value->set($fill);

        $value->setName($field->getName());

        return $value;
    }

    protected function addValue($value)
    {
        $this->values[$value->getName()] = $value;
    }
}
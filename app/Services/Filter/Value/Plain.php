<?php

namespace App\Services\Filter\Value;

class Plain
{
    protected $values;

    protected $name;

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function set($value)
    {
        $this->values = $value;
    }

    public function get()
    {
        return $this->values;
    }
}
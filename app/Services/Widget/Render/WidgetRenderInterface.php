<?php

namespace App\Services\Widget\Render;

use App\Models\Widgets\Widget;

interface WidgetRenderInterface
{
    public function display(Widget $widget);
}
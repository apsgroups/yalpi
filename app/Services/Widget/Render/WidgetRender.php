<?php

namespace App\Services\Widget\Render;

use App\Models\Widgets\Widget;
use Carbon\Carbon;

class WidgetRender
{
    protected $menuItemId;

    protected $onProduct;

    protected $widgets;

    public function __construct()
    {
        $this->menuItemId = request()->input('menu_item_id');

        $isProduct = false;

        $currentRoute = \Route::getCurrentRoute();

        if($currentRoute) {
            $isProduct = strpos($currentRoute->getActionName(), 'ProductController@show') !== false;
        }

        $this->setIsOnProduct($isProduct);

        $widgets = $this->getWidgetsRecords();

        $widgets = $this->filterAssignedWidgets($widgets);

        $this->setWidgets($widgets);
    }

    public function setIsOnProduct($value)
    {
        $this->onProduct = $value;
    }

    protected function isOnProduct()
    {
        return $this->onProduct;
    }

    protected function setWidgets($widgets)
    {
        $this->widgets = $widgets;
    }

    protected function getWidgets()
    {
        return $this->widgets;
    }

    public function position($position)
    {
        $widgets = $this->getWidgets()->groupBy('position');

        if(empty($widgets[$position])) {
            return '';
        }

        return $this->getHtml($widgets[$position]);
    }

    protected function getHtml($widgets)
    {
        $html = [];

        foreach($widgets as $widget) {
            $html[] = $this->renderWidget($widget);
        }

        return implode("\n", $html);
    }

    protected function renderWidget(Widget $widget)
    {
        $name = ucfirst($widget->type);

        $class = '\App\Services\Widget\\'.$name.'\\'.$name.'WidgetRender';

        if(!class_exists($class)) {
            throw new \Exception('Class '.$class.' doesn\'t exists');
        }

        $instance = new $class();

        if(!method_exists($instance, 'display')) {
            throw new \Exception('Method display not implemented');
        }

        return call_user_func([$instance, 'display'], $widget);
    }

    protected function getWidgetsRecords()
    {
        return Widget::published(1)
        /*
            ->where(function ($query) {
                $query->where('publish_up', '>=', Carbon::now()->toDateTimeString())
                    ->orWhere('publish_up', '=', '0000-00-00 00:00:00');
            })
            ->where(function ($query) {
                $query->where('publish_down', '<=', Carbon::now()->toDateTimeString())
                    ->orWhere('publish_down', '=', '0000-00-00 00:00:00');
            })
            */
            ->where('assign', '<>', 1)
            ->with('assigns')
            ->orderBy('ordering', 'asc')
            ->get();
    }

    protected function filterAssignedWidgets($widgets)
    {
        return $widgets->filter(function ($value, $key) {
            if(!$value->on_product && $this->isOnProduct()) {
                return false;
            }

            if($value->assign === 0) {
                return true;
            }

            if($value->assign === 2 && $value->assigns->contains('menu_item_id', $this->menuItemId)) {
                return true;
            }

            if($value->assign === 3 && !$value->assigns->contains('menu_item_id', $this->menuItemId)) {
                return true;
            }

            return false;
        });
    }
}
<?php

namespace App\Services\Widget\Menu;

use App\Models\Menu;
use App\Models\Widgets\Widget;
use App\Services\Widget\Form\WidgetFormInterface;

class MenuWidgetForm implements WidgetFormInterface
{
    protected $view = 'backend.widget.partials.form.menu.index';

    public function create()
    {
        return $this->getView();
    }

    public function edit(Widget $widget)
    {
        $menuWidget = $widget->menuWidget;

        $selected = null;
        $layout = null;

        if($menuWidget) {
            $selected = $menuWidget->menu_id;
            $layout = $menuWidget->layout;
        }

        return $this->getView(['selected' => $selected, 'layout' => $layout]);
    }

    protected function getMenus()
    {
        return Menu::all()->pluck('title', 'id');
    }

    protected function getView(array $params = [])
    {
        $params['menus'] = $this->getMenus();

        $params['layouts'] = config('widget.menu.layouts');

        return view()->make($this->view, $params)->render();
    }
}
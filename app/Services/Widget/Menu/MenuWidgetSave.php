<?php

namespace App\Services\Widget\Menu;

use App\Models\Widgets\MenuWidget;
use App\Services\Widget\Save\WidgetSaveAbstract;

class MenuWidgetSave extends WidgetSaveAbstract
{
    public function save()
    {
        $attributes = $this->getAttributes();

        if(empty($attributes)) {
            return false;
        }

        $widget = $this->getWidget();

        $menuWidget = $widget->menuWidget;

        if(empty($menuWidget)) {
            $widget->menuWidget()->create($attributes);
        } else {
            $menuWidget->update($attributes);
        }

        return true;
    }
}
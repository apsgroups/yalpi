<?php

namespace App\Services\Widget\Menu;

use App\Models\Widgets\Widget;
use App\Services\Widget\Render\WidgetRenderInterface;

class MenuWidgetRender implements WidgetRenderInterface
{
    public function display(Widget $widget)
    {
        if(empty($widget->menuWidget->menu_id)) {
            return '';
        }

        $config = [
            'menu_id' => $widget->menuWidget->menu_id,
            'layout' => $widget->menuWidget->layout,
            'title' => $widget->title,
            'title_href' => '',
        ];

        $params = explode("\n", $widget->note);

        if($params) {
            foreach ($params as $key => $value) {
                $row = explode('=', $value);
                
                if(count($row) === 2) {
                    $config[$row[0]] = $row[1];
                }
            }
        }

        return \Widget::run('menuWidget', $config);
    }
}
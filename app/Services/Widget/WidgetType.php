<?php

namespace App\Services\Widget;

class WidgetType
{
    protected $types;

    public function __construct()
    {
        $this->types = config('widget.types');
    }

    public function all()
    {
        return $this->types;
    }
}
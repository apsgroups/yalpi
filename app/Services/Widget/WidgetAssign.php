<?php

namespace App\Services\Widget;

use App\Models\Menu;

class WidgetAssign
{
    protected function getMenus()
    {
        return Menu::all();
    }

    public function getItems()
    {
        $menus = $this->getMenus();

        $items = [];

        foreach($menus as $menu) {
            $items[$menu->id]['item'] = $menu;
            $items[$menu->id]['childs'] = $this->getMenuItems($menu);
        }

        return $items;
    }

    protected function getMenuItems(Menu $menu, $parent = null)
    {
        $menuItems = $menu->items()->with('children')->childsOf($parent)->get();

        $items = [];

        foreach($menuItems as $item) {
            $menuItem = [
                'item' => $item,
                'childs' => $this->getMenuItems($menu, $item->id),
            ];

            $items[] = $menuItem;
        }

        return $items;
    }
}
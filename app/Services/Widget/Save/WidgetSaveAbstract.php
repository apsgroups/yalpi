<?php

namespace App\Services\Widget\Save;

use App\Models\Widgets\Widget;

abstract class WidgetSaveAbstract implements WidgetSaveInterface
{
    protected $widget;

    protected $attributes;

    public function __construct(Widget $widget, array $attributes = [])
    {
        $this->widget = $widget;
        $this->attributes = $attributes;
    }

    abstract public function save();

    /**
     * @return array
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * @return Widget
     */
    public function getWidget()
    {
        return $this->widget;
    }

}
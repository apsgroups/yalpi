<?php

namespace App\Services\Widget\Save;

use App\Models\Widgets\Widget;

class WidgetSave
{
    protected $widget;

    protected $attributes;

    public function __construct(Widget $widget, array $attributes = [])
    {
        $this->widget = $widget;
        $this->attributes = $this->getWidgetAttributes($attributes);
    }

    public function save()
    {
        $name = ucfirst($this->getName());

        $class = '\App\Services\Widget\\'.$name.'\\'.$name.'WidgetSave';

        if(!class_exists($class)) {
            throw new \Exception('Class '.$class.' doesnt exists');
        }

        $instance = new $class($this->widget, $this->attributes);

        if(!method_exists($instance, 'save')) {
            throw new \Exception('Methos save not implement');
        }

        return call_user_func([$instance, 'save']);
    }

    protected function getWidgetAttributes(array $attributes)
    {
        $name = $this->getName();

        if(empty($attributes['params'][$name])) {
            return [];
        }

        return $attributes['params'][$name];
    }

    protected function getName()
    {
        return $this->widget->type;
    }
}
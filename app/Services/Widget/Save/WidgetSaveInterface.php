<?php

namespace App\Services\Widget\Save;

use App\Models\Widgets\Widget;

interface WidgetSaveInterface
{
    public function __construct(Widget $widget, array $attributes = []);

    public function save();
}
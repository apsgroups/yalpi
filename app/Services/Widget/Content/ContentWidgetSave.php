<?php

namespace App\Services\Widget\Content;

use App\Models\Widgets\MenuWidget;
use App\Services\Widget\Save\WidgetSaveAbstract;

class ContentWidgetSave extends WidgetSaveAbstract
{
    public function save()
    {
        $attributes = $this->getAttributes();

        if(empty($attributes)) {
            return false;
        }

        $widget = $this->getWidget();

        $contentWidget = $widget->contentWidget;

        if(empty($contentWidget)) {
            $widget->contentWidget()->create($attributes);
        } else {
            $contentWidget->update($attributes);
        }

        return true;
    }
}
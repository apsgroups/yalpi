<?php

namespace App\Services\Widget\Content;

use App\Models\Menu;
use App\Models\Widgets\Widget;
use App\Services\Widget\Form\WidgetFormInterface;

class ContentWidgetForm implements WidgetFormInterface
{
    protected $view = 'backend.widget.partials.form.content.index';

    public function create()
    {
        return $this->getView();
    }

    public function edit(Widget $widget)
    {
        $contentWidget = $widget->contentWidget;

        $params = [];

        if($contentWidget && $contentWidget->content) {
            $params['content'] = $contentWidget->content;
        }

        return $this->getView($params);
    }

    protected function getView(array $params = [])
    {
        return view()->make($this->view, $params)->render();
    }
}
<?php

namespace App\Services\Widget\Content;

use App\Models\Widgets\Widget;
use App\Services\Widget\Render\WidgetRenderInterface;

class ContentWidgetRender implements WidgetRenderInterface
{
    public function display(Widget $widget)
    {
        if(!$widget->contentWidget->content_id) {
            return '';
        }

        $config = [
            'id' => $widget->contentWidget->content_id,
            'layout' => $widget->contentWidget->layout,
        ];

        return \Widget::run('contentWidget', $config);
    }
}
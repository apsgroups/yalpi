<?php

namespace App\Services\Widget\Promo;

use App\Models\Widgets\PromoWidget;
use App\Services\Widget\Save\WidgetSaveAbstract;

class PromoWidgetSave extends WidgetSaveAbstract
{
    public function save()
    {
        $attributes = $this->getAttributes();

        if(empty($attributes)) {
            return false;
        }

        $widget = $this->getWidget();

        $ids = [];

        foreach($attributes as $k => $v) {
            if(!$v['id']) {
                $tab = $widget->promoWidgetTabs()->create([
                    'title' => $v['title'],
                    'ordering' => $v['ordering'],
                ]);
            } else {
                $tab = PromoWidget::find($v['id']);

                if(empty($tab)) {
                    continue;
                }

                $tab->update([
                    'title' => $v['title'],
                    'ordering' => $v['ordering'],
                ]);
            }

            $v['categories'] = empty($v['categories']) ? [] : $v['categories'];
            $tab->categories()->sync($v['categories']);

            $ids[] = $tab->id;
        }

        if($ids) {
            $widget->promoWidgetTabs()->whereNotIn('id', $ids)->delete();
        }

        return true;
    }
}
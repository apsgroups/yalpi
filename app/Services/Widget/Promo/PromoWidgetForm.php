<?php

namespace App\Services\Widget\Promo;

use App\Models\Widgets\Widget;
use App\Services\Widget\Form\WidgetFormInterface;

class PromoWidgetForm implements WidgetFormInterface
{
    protected $view = 'backend.widget.partials.form.promo.index';

    public function create()
    {
        return $this->getView(['tabs' => collect([])]);
    }

    public function edit(Widget $widget)
    {
        $tabs = $widget->promoWidgetTabs()->with('categories')->orderBy('ordering', 'asc')->get();

        return $this->getView(['tabs' => $tabs]);
    }

    protected function getView(array $params = [])
    {
        $categoryContract = app()->make('App\Repositories\Backend\Category\CategoryContract');

        $params['categories'] = $categoryContract->getParentsForSelect();

        return view()->make($this->view, $params)->render();
    }
}
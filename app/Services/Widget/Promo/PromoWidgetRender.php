<?php

namespace App\Services\Widget\Promo;

use App\Models\Category;
use App\Models\Product;
use App\Models\Widgets\Widget;
use App\Services\Widget\Render\WidgetRenderInterface;

class PromoWidgetRender implements WidgetRenderInterface
{
    public function display(Widget $widget)
    {
        $tabs = $widget->promoWidgetTabs()
            ->orderBy('ordering', 'asc')
            ->get()
            ->keyBy('id');


        if(empty($tabs)) {
            return '';
        }

        $products = [];

        foreach($tabs as $k => $tab) {
            $category = $tab->categories()->published(1)->first();
            
            $tabs[$k]->url = $category->link();

            if(empty($category)) {
                continue;
            }

            $products[$tab->id] = $category->products()->with(['image', 'category'])
                ->published(1)
                ->where('price', 0)
                //->where('rate_round', 4)
                ->take(config('site.product.promo_widget.limit', 10))
                ->inRandomOrder()
                ->get();


        }

        return view("frontend.widgets.promo.index", [
            'products' => $products,
            'tabs' => $tabs,
            'inRow' => config('site.product.promo_widget.in_row', 3),
        ]);
    }
}
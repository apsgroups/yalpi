<?php

namespace App\Services\Widget\Form;

class WidgetFormEdit extends WidgetFormAbstract
{
    protected $widget;
    protected $name;

    public function __construct($name, $widget)
    {
        $this->widget = $widget;
        $this->name = $name;
    }

    public function getWidget()
    {
        return $this->widget;
    }

    public function getName()
    {
        return $this->name;
    }

    protected function callMethod(WidgetFormInterface $instance)
    {
        return call_user_func([$instance, $this->getMethodName()], $this->getWidget());
    }

    protected function getMethodName()
    {
        return 'edit';
    }

}
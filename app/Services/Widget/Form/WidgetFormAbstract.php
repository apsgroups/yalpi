<?php

namespace App\Services\Widget\Form;

abstract class WidgetFormAbstract
{
    protected $name;

    public function __construct($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getHtml()
    {
        $name = ucfirst($this->getName());

        $class = '\App\Services\Widget\\'.$name.'\\'.$name.'WidgetForm';

        if(!class_exists($class)) {
            return 'no params';
        }

        $instance = new $class();

        if(!method_exists($instance, $this->getMethodName())) {
            return 'no params';
        }

        return $this->callMethod($instance);
    }

    abstract protected function callMethod(WidgetFormInterface $instance);

    abstract protected function getMethodName();
}
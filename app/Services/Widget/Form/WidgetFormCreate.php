<?php

namespace App\Services\Widget\Form;

class WidgetFormCreate extends WidgetFormAbstract
{
    protected function callMethod(WidgetFormInterface $instance)
    {
        return call_user_func([$instance, $this->getMethodName()]);
    }

    protected function getMethodName()
    {
        return 'create';
    }

}
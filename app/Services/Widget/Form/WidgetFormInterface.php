<?php

namespace App\Services\Widget\Form;

use App\Models\Widgets\Widget;

interface WidgetFormInterface
{
    public function create();

    public function edit(Widget $widget);
}
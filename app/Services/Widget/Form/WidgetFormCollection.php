<?php

namespace App\Services\Widget\Form;

use App\Models\Widgets\Widget;
use App\Services\Widget\WidgetType;

class WidgetFormCollection
{
    protected $names;

    protected $forms;

    public function __construct()
    {
        $this->names = (new WidgetType())->all();
    }

    public function create()
    {
        foreach($this->names as $name => $title) {
            $instance = new WidgetFormCreate($name);

            $this->add($instance);
        }

        return $this->get();
    }

    public function edit(Widget $widget)
    {
        foreach($this->names as $name => $title) {
            $instance = new WidgetFormEdit($name, $widget);

            $this->add($instance);
        }

        return $this->get();
    }

    protected function add($form)
    {
        $this->forms[$form->getName()] = $form;
    }

    protected function get()
    {
        return collect($this->forms);
    }
}
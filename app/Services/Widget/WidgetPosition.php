<?php

namespace App\Services\Widget;

class WidgetPosition
{
    protected $positions;

    public function __construct()
    {
        $this->positions = config('widget.positions');
    }

    public function all()
    {
        return $this->positions;
    }
}
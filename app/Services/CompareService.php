<?php

namespace App\Services;

use App\Repositories\Frontend\Compare\CompareContract;

class CompareService
{
    protected $compare;

    protected $compareContract;

    public function __construct(CompareContract $compareContract)
    {
        $this->compareContract = $compareContract;
    }

    public function isActive($productId)
    {
        $favorites = $this->getCompare();
        if(isset($favorites[$productId])) {
            return true;
        }

        return false;
    }

    public function count()
    {
        return count($this->getCompare());
    }

    protected function getCompare()
    {
        if(is_null($this->compare)) {
            $this->setCompare();
        }

        return $this->compare;
    }

    protected function setCompare()
    {
        $this->compare = $this->compareContract->all();
    }

    public function add($id)
    {
        $this->compareContract->add($id);

        $this->compare[$id] = $id;
    }

    public function remove($id)
    {
        $this->compareContract->remove($id);

        unset($this->compare[$id]);
    }

    public function clear()
    {
        $this->compareContract->clear();

        $this->compare = [];
    }
}
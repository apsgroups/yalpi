<?php

namespace App\Services\Image;


use App\Models\Image;

class ImageDestroyer {
    public static function destroy($id)
    {
        $image = Image::findOrFail($id);

        $original = \App\Services\Image\ImageService::getFullpath($image);

        @unlink(public_path($original));

        $image->delete();
    }
} 
<?php
namespace App\Services\Image;

use App\Models\Image;
use Symfony\Component\HttpFoundation\File\Exception\UploadException;

class ImageService
{
    protected $field;

    protected $morphTo;

    protected $model;

    public function __construct($field, $morphTo)
    {
        $this->field = $field;

        $this->morphTo = $morphTo;
    }

    public static function getFullpath($model)
    {
        return rtrim(self::getPath(), '/').'/'.self::getFilename($model);
    }

    public static function getPath()
    {
        return config('site.image_folder', 'images/uploads/');
    }

    public static function getThumbnailPath($model)
    {
        $location = class_basename($model);
        $location = strtolower($location);
        $location = str_plural($location);

        $path[] = config('site.image_thumbnail', 'images');
        $path[] = $location;

        $path[] = trim($model->getThumbnailPath(), '/');

        return implode('/', $path);
    }

    /**
     * @return \App\Models\Image;
     */
    protected function createModel($file)
    {
        $ext = $file->getClientOriginalExtension();

        $image = $this->morphTo->images()->create([
            'title' => $this->morphTo->title,
            'thumbnail' => self::getThumbnailPath($this->morphTo),
            'original_name' => $file->getClientOriginalName(),
            'ext' => strtolower($ext),
        ]);

        $this->model = $image;

        return $image;
    }

    public static function getFilename(Image $model)
    {
        return $model->id.'.'.strtolower($model->ext);
    }

    protected function process($file)
    {
        $model = $this->createModel($file);

        $path = public_path(self::getPath());

        \File::exists($path) or \File::makeDirectory($path, 0755, true);

        $file->move($path, static::getFilename($model));

        return $model;
    }

    public function saveByUrl($file)
    {
        $image = $this->morphTo->images()->create([
            'title' => $this->morphTo->title,
            'thumbnail' => self::getThumbnailPath($this->morphTo),
            'original_name' => pathinfo($file, PATHINFO_BASENAME),
            'ext' => strtolower(pathinfo($file, PATHINFO_EXTENSION)),
        ]);

        $this->model = $image;

        $path = public_path(self::getPath());

        \File::exists($path) or \File::makeDirectory($path, 0755, true);

        if(strpos($file, 'http') !== false) {
            copy($file, $path.'/'.static::getFilename($image));
        } else {
            \File::move($file, $path.'/'.static::getFilename($image));
        }

        return $image;
    }

    /**
     * @return \App\Models\Image;
     */
    public function upload()
    {
        if (!request()->hasFile($this->field)) {
            throw new UploadException('File not found');
        }

        $file = request()->file($this->field);

        if(!is_array($file)) {
            return $this->process($file);
        }

        foreach ($file as $key => $f) {
            $this->process($f);
        }
    }
}
<?php

namespace App\Services\Image;

use Intervention\Image\Facades\Image as ImageTool;

use \App\Models\Image;

class ImageThumb
{
    protected $config;

    protected $image;

    protected $method;

    protected $size;

    public function __construct($image, $size)
    {
        $this->image = $image;

        $this->size = !empty($size) ? $size : config('thumbnail.size', 'default');

        $this->config = $this->getConfig();

        $this->method = $this->getMethod();
    }

    protected function getMethod()
    {
        return !empty($this->config['method']) ? $this->config['method'] : config('thumbnail.method', 'widen');
    }

    protected function getConfig()
    {
        $namespace = str_plural(strtolower(class_basename($this->image->imageable_type)));

        $config = config('thumbnail.sizes.'.$namespace.'.'.$this->size);

        if(!empty($config)) {
            return $config;
        }

        if(ends_with($this->size, ['_2x', '_3x'])) {
            $size = rtrim($this->size, '_2x');
            $size = rtrim($size, '_3x');
            $config = config('thumbnail.sizes.'.$namespace.'.'.$size);

            $coof = ends_with($this->size, '_2x') ? 2 : 3;

            $config[0] = $config[0] ? $config[0] * $coof : $config[0];
            $config[1] = $config[1] ? $config[1] * $coof : $config[1];
        }

        return $config;
    }

    public function thumbnail()
    {
        $thumbnail = $this->getThumbFolder(); 

        if(file_exists(public_path($thumbnail))) {
            return $thumbnail;
        }

        $original = !empty($this->image->original_path) ? $this->image->original_path : \App\Services\Image\ImageService::getFullpath($this->image);

        if(!file_exists(public_path($original))) {
            return false;
        }

		try {
			$src = ImageTool::make(public_path($original));
		} catch(\Exception $e) {
			return $original;
		}

        $src = $this->applyMethod($src);

        if(!empty($this->config['watermark'])) {
            $src = $this->applyWatermark($src, $this->config['watermark']);
        }

        $quality = empty($this->config['quality']) ? config('thumbnail.quality', 85) : $this->config['quality'];

        $sharpen = !isset($this->config['sharpen']) ? config('thumbnail.sharpen', 0) : $this->config['sharpen'];
        
        $src->sharpen($sharpen)->save(public_path($thumbnail), $quality);

        return $thumbnail;
    }

    public function applyWatermark($src, array $config)
    {
        $watermark = resource_path('assets/images/watermarks/'.$config['src']);

        return $src->insert($watermark, $config['position'], $config['x'], $config['y']);
    }

    protected function getThumbFolder()
    {
        $thumbnail = rtrim($this->image->thumbnail, '/').'-'.$this->image->id.'-'.$this->size.'.'.strtolower($this->image->ext);

        \File::exists(dirname($thumbnail)) or \File::makeDirectory(dirname($thumbnail), 0755, true);

        return '/'.$thumbnail;
    }

    protected function applyMethod($src)
    {
        $methodName = $this->method.'Image';

        if(!method_exists($this, $methodName)) {
            throw new \BadMethodCallException('Thumb method '.$this->method.' not found');
        }

        $src = call_user_func([$this, $methodName], $src);

        return $src;
    }

    protected function fitImage($src)
    {
        $src->fit($this->config[0], $this->config[1], function ($constraint) {
            $constraint->upsize();
        });

        return $src;
    }

    protected function fitByHeightImage($src)
    {
        if($src->width() < $this->config[0])  {
            $src->heighten($this->config[1], function ($constraint) {
                $constraint->upsize();
            });

            return $src;
        }

        $src->fit($this->config[0], $this->config[1], function ($constraint) {
            $constraint->upsize();
        });

        return $src;
    }

    protected function widenImage($src)
    {
        $src->widen($this->config[0], function ($constraint) {
            $constraint->upsize();
        });

        return $src;
    }
}
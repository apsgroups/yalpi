<?php

namespace App\Services\Cart;

use App\Models\Image;
use App\Models\Product;
use App\Models\ProductAttribute;
use App\Models\ProductProperty;
use App\Models\PropertyValue;
use App\Repositories\Frontend\Product\ProductContract;
use Illuminate\Http\Request;

class CartService
{
    protected $products;

    protected $cart;

    protected $session;

    protected $sessionKey = 'cart';

    protected $refreshProducts = true;

    protected $productList;

    protected $coupon;

    public function __construct(ProductContract $products, Request $request)
    {
        $this->products = $products;

        $this->session = $request->session();

        $this->cart = $this->session->get($this->sessionKey);

        $this->coupon = $this->session->get('coupon');
    }

    public function getCart()
    {
        return $this->cart;
    }

    public function clear()
    {
        $this->cart = null;
        $this->coupon = null;
        $this->store();
    }

    public function setCoupon($coupon)
    {
        $coupon = mb_strtoupper(trim($coupon));

        if($coupon === 'HAPPY169') {
            $this->coupon = $coupon;
            $this->session->put('coupon', $coupon);
            return true;
        }

        return false;
    }

    public function getCoupon()
    {
        return $this->coupon;
    }

    public function couponDiscount()
    {
        if($this->getCoupon() === 'HAPPY169' && $this->productsCost() >= 10000) {
            return 500; 
        }

        return 0;
    }

    protected function getKey($id, $attribute, $properties, $modules, $desk)
    {
        $keys[] = $id;

        if(!empty($attribute)) {
            $keys[] = 'attribute';
            $keys[] = $attribute;
        }

        if(!empty($properties)) {
            $keys[] = 'properties';
            $ids = $this->getPropertiesId($properties);
            $keys = array_merge($keys, $ids);
        }

        if(!empty($modules)) {
            $keys[] = 'modules';
            ksort($modules);

            $ids = implode('-', array_keys($modules));
            $qtt = implode('-', $modules);
            $keys[] = md5($ids.'-'.$qtt);
        }

        if(!empty($desk)) {
            $keys[] = 'desk-'.$desk;
        }

        return implode('-', $keys);
    }

    public function amount()
    {
        $cart = $this->getCart();

        if(empty($cart)) {
            return 0;
        }

        $products = $this->getProducts();

        if(empty($products)) {
            return 0;
        }

        $amount = 0;
        foreach($products as $key => $row) {
            $amount += $row['amount'];
        }

        return $amount;
    }

    public function cost()
    {
        $cost = $this->productsCost();

        $cost -= $this->couponDiscount();

        return $cost;
    }

    protected function productsCost()
    {
        $cart = $this->getCart();

        if(empty($cart)) {
            return 0;
        }

        $products = $this->getProducts();

        if(empty($products)) {
            return 0;
        }

        $cost = 0;
        foreach($products as $key => $row) {
            $cost += $row['price'] * $row['amount'];
        }

        return $cost;
    }

    public function getProducts()
    {
        if($this->getRefreshProducts() === false) {
            return $this->productList;
        }

        $ids = $this->getProductsId();

        if(empty($ids)) {
            return [];
        }

        $products = Product::whereIn('id', $ids)->with('image')->get()->keyBy('id');

        $cart = $this->getCart();

        foreach($cart as $k => $v) {
            if(empty($products[$v['product_id']])) {
                unset($cart[$k]);
                continue;
            }

            $cart[$k] = $this->formatCartProduct( $products[$v['product_id']], $cart[$k]);
        }

        $this->setRefreshProducts(false);

        $this->productList = $cart;

        return $this->productList;
    }

    protected function formatCartProduct(Product $product, array $cartProduct, $isModule = false)
    {
        $cartProduct['price'] = $product->price;
        $cartProduct['price_old'] = $product->price_old;
        $cartProduct['image'] = $product->image;

        if(!empty($cartProduct['attribute'])) {
            $productAttribute = $this->attribute($cartProduct['attribute']['id']);
            $cartProduct['price'] = $productAttribute['price'];
            $cartProduct['price_old'] = $productAttribute['price_old'];
            $cartProduct['image'] = $productAttribute['image'];
        }

        $cartProduct['title'] = $product->title;
        $cartProduct['sku'] = $product->sku;
        $cartProduct['model'] = $product;

        if(!$isModule) {
            $modules = $this->getModules($product, $cartProduct);

            if(!empty($cartProduct['modules'])) {
                $moduleCollection = collect($modules);

                $cartProduct['price'] = $moduleCollection->sum('module_price');
                $cartProduct['price_old'] = $moduleCollection->sum('module_price_old');

                if($cartProduct['price'] === $cartProduct['price_old']) {
                    $cartProduct['price_old'] = 0;
                }
            }

            $cartProduct['modules'] = $modules;

            if(!empty($cartProduct['desk'])) {
                $cartProduct['desk'] = Product::find($cartProduct['desk']);
            }
        }

        $cartProduct['cost'] = $cartProduct['price'] * $cartProduct['amount'];

        return $cartProduct;
    }

    protected function getModules(Product $product, $cartProduct)
    {
        $defaultModules = $product->modules()->with('image')->get();

        $items = [];

        foreach($defaultModules as $m) {
            $quantity = isset($cartProduct['modules'][$m->id]) ? $cartProduct['modules'][$m->id] : $m->pivot->quantity;

            $cartModule = [];
            $cartModule['amount'] = $cartProduct['amount'] * $quantity;
            $cartModule['module_amount'] = $quantity;
            $cartModule['module_price'] = $quantity * $m->price;

            $modPriceOld = $m->price_old > 0 ? $m->price_old : $m->price;
            $cartModule['module_price_old'] = $quantity * $modPriceOld;

            if($cartModule['amount'] < 1) {
                continue;
            }

            unset($cartModule['modules']);

            $items[] = $this->formatCartProduct($m, $cartModule, true);

            unset($cartProduct['modules'][$m->id]);
        }

        if(!empty($cartProduct['modules'])) {
            $additionalModules = Product::published(1)
                ->whereIn('id', array_keys($cartProduct['modules']))
                ->with('image')
                ->get();

            foreach($additionalModules as $m) {
                $quantity = $cartProduct['modules'][$m->id];

                $cartModule = [];
                $cartModule['amount'] = $cartProduct['amount'] * $quantity;
                $cartModule['module_amount'] = $quantity;
                $cartModule['module_price'] = $quantity * $m->price;

                $modPriceOld = $m->price_old > 0 ? $m->price_old : $m->price;
                $cartModule['module_price_old'] = $quantity * $modPriceOld;

                if($cartModule['amount'] < 1) {
                    continue;
                }

                unset($cartModule['modules']);

                $items[] = $this->formatCartProduct($m, $cartModule, true);
            }
        }


        return $items;
    }

    public function getProductByKey($key)
    {
        $cartProduct = $this->get($key);

        if(empty($cartProduct)) {
            return null;
        }

        $product = $this->products->findById($cartProduct['product_id']);

        return $this->formatCartProduct($product, $cartProduct);
    }

    protected function getProductsId()
    {
        $cart = $this->getCart();

        return collect($cart)->pluck('product_id')->toArray();
    }

    public function add($id, $attributeId = null, $properties = [], $amount=1, $modules = [], $desk = null)
    {
        $key = $this->getKey($id, $attributeId, $properties, $modules, $desk);

        $prev = $this->get($key);

        if(!empty($prev['amount'])) {
            $amount += $prev['amount'];
        }

        $this->set($id, $attributeId, $properties, $amount, $modules, $desk);

        return $key;
    }

    public function recalc($key, $amount=null)
    {
        $product = $this->get($key);

        if(!empty($product)) {
            $this->cart[$key]['amount'] = $amount;
        }

        $this->store();
    }

    public function remove($key)
    {
        unset($this->cart[$key]);

        $this->store();
    }

    protected function get($key)
    {
        if(empty($this->cart[$key])) {
            return [];
        }

        return $this->cart[$key];
    }

    protected function set($id, $attributeId, $properties, $amount, $modules, $desk)
    {
        $key = $this->getKey($id, $attributeId, $properties, $modules, $desk);

        if(is_null($amount)) {
            unset($this->cart[$key]);
        } else {
            $this->cart[$key] = [
                'product_id' => $id,
                'attribute' => $this->attribute($attributeId),
                'properties' => $this->modifyProperties($properties),
                'amount' => $amount,
                'modules' => $modules,
                'desk' => $desk,
            ];
        }

        $this->store();
    }

    protected function attribute($id)
    {
        if(empty($id)) {
            return null;
        }

        $productAttribute = ProductAttribute::find($id);

        if(empty($productAttribute)) {
            return null;
        }

        return [
            'id' => $productAttribute->id,
            'title' => $productAttribute->attribute->title,
            'value' => $productAttribute->value,
            'price' => $productAttribute->getPrice(),
            'price_old' => $productAttribute->getPriceOld(),
            'image' => $productAttribute->getImage(),
        ];
    }

    public function getPropertiesId($properties)
    {
        return collect($properties)->pluck('value')->toArray();
    }

    protected function modifyProperties($properties)
    {
        if(empty($properties)) {
            return $properties;
        }

        $ids = $this->getPropertiesId($properties);

        $values = ProductProperty::whereIn('id', $ids)->with('propertyValue', 'propertyValue.property')->get();

        foreach($values as $k => $value) {
            $properties[$k]['property_id'] = $value->propertyValue->property->id;
            $properties[$k]['title'] = $value->propertyValue->property->single_title;
            $properties[$k]['image_id'] = $value->propertyValue->image_id;
            $properties[$k]['plaintext'] = $value->propertyValue->title;
        }

        return $properties;
    }

    protected function store()
    {
        $this->setRefreshProducts(true);

        $this->session->put('cart', $this->cart);

        $this->session->put('coupon', $this->coupon);
    }

    protected function getRefreshProducts()
    {
        return $this->refreshProducts;
    }

    protected function setRefreshProducts($state)
    {
        $this->refreshProducts = $state;
    }

    public function responseProducts()
    {
        $items = [];

        $cart = $this->getCart();

        if(empty($cart)) {
            return [];
        }

        $products = $this->getProducts();

        foreach($cart as $key => $row) {
            if(empty($products[$key])) {
                continue;
            }

            $modules = [];

            if(!empty($products[$key]['modules'])) {
                foreach($products[$key]['modules'] as $m) {
                    $modules[] = [
                        'id' => $m['model']->id,
                        'price' => format_price($m['cost']),
                        'amount' => $m['amount'],
                    ];
                }
            }

            $items[$key] = [
                'key' => $key,
                'product_id' => $row['product_id'],
                'amount' => $row['amount'],
                'cost' => format_price($products[$key]['cost']),
                'modules' => $modules,
            ];
        }

        return $items;
    }

    public function getColors($key)
    {
        $product = $this->get($key);

        $names = config('settings.colors');
        if(!$names || empty($product['properties'])) {
            return [];
        }

        $colors = [];
        foreach($names as $name) {
            if($color = $this->getColorValue($name, $product['properties'])) {
                $colors[] = $color;
            }
        }

        return $colors;
    }

    protected function getColorValue($name, $properties)
    {
        $colorId = \Settings::get($name);

        if(empty($colorId)) {
            return [];
        }

        $color = collect($properties)->where('property_id', (int)$colorId)->first();

        if(!$color) {
            return [];
        }

        return [
            'property_id' => $color['property_id'],
            'title' => $color['title'],
            'active' => $color['value'],
            'values' => [
                [
                    'id' => $color['value'],
                    'plaintext' => $color['plaintext'],
                    'image' => Image::find($color['image_id']),
                ]
            ]
        ];
    }
}
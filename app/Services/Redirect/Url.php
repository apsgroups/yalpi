<?php

namespace App\Services\Redirect;

use App\Models\Category;
use App\Models\Content;
use App\Models\MenuItem;
use App\Models\Product;

class Url
{
    public static function find($url)
    {
        $parts = explode("/", $url);

        // trim first part before slash
        if(empty($parts[0])) {
            unset($parts[0]);
        }

        // trim empty part after slash
        if(empty($alias)) {
            array_pop($parts);
        }

        $reverse = array_reverse($parts);

        foreach($reverse as $alias) {
            $link = '/'.implode('/', $parts).'/';

            $search = ['slug' => $alias, 'published' => true];

            $product = Product::where($search)->first();
            if($product) {
                return $product;
            }

            $content = Content::where($search)->first();
            if($content) {
                return $content;
            }

            $menuItems = MenuItem::where($search)->whereIn('type', config('menu.search_redirects'))->get();

            foreach($menuItems as $item) {
                if($item->link() === $link) {
                    return $item;
                }
            }

            $categories = Category::where($search)->get();

            foreach($categories as $category) {
                if($category->link() === $link) {
                    return $category;
                }
            }

            array_pop($parts);
        }

        return '/';
    }
}
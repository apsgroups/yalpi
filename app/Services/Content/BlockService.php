<?php

namespace App\Services\Content;

use App\Models\Content;

class BlockService
{
    protected $contentType = 'blocks';

    public function get($slug)
    {
        return \Cache::remember('blocks.'.$slug, config('cache.time'), function () use($slug) {
            return Content::ofType($this->contentType)
                ->slug($slug)
                ->published()
                ->first();
        });
    }
}
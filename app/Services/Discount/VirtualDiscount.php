<?php

namespace App\Services\Discount;


use App\Models\Product;

class VirtualDiscount {
    const STATUS_ADDED = 1;
    const STATUS_NOT_MODIFIED = 2;
    const STATUS_UPDATED = 3;

    protected $product;

    protected $discountTypes;

    public function __construct(Product $product)
    {
        $this->product = $product;

        $this->discountTypes = config('site.virtual-discounts');
    }

    protected function getDiscountId()
    {
        $compositions = $this->product->moduleParents()->where('virtual_discount', '>', 0)->get();

        if($compositions->count()) {
            return $compositions->first()->virtual_discount_id;
        }

        if($this->canApplyMaxDiscount()) {
            return $this->findMaxDiscountType();
        }

        return $this->findDiscountId($this->product->price_base);
    }

    protected function getDiscountValue($id)
    {
        if(empty($this->discountTypes[$id])) {
            return 0;
        }

        $discount = $this->discountTypes[$id];

        $compositions = $this->product->moduleParents()->where('virtual_discount', '>', 0)->get();

        if(\Settings::get('virtual_discount_stock') && $compositions->count() && !$this->product->in_stock) {
            return $this->getDiscountMaxValue($discount);
        }

        if($compositions->count()) {
            return $compositions->first()->virtual_discount;
        }

        if($this->canApplyMaxDiscount()) {
            return $this->getDiscountMaxValue($discount);
        }

        if($id === $this->product->virtual_discount_id) {
            return $this->product->virtual_discount;
        }

        return $this->getDiscountRandValue($discount);
    }

    protected function getDiscountMaxValue($discount)
    {
        return max($discount['list']);
    }

    protected function getDiscountRandValue($discount)
    {
        $key = array_rand($discount['list']);

        return $discount['list'][$key];
    }

    protected function findMaxDiscountType()
    {
        foreach($this->discountTypes as $id => $discount) {
            if(!empty($discount['from']) && empty($discount['to'])) {
                return $id;
            }
        }

        return 0;
    }

    protected function findDiscountId($price)
    {
        foreach($this->discountTypes as $id => $discount) {
            if(!empty($discount['to']) && empty($discount['from']) && $price < $discount['to']) {
                return $id;
            }

            if(!empty($discount['to']) && !empty($discount['from']) && $price >= $discount['from'] && $price < $discount['to']) {
                return $id;
            }

            if(empty($discount['to']) && $price >= $discount['from']) {
                return $id;
            }
        }

        return null;
    }

    protected function canApplyMaxDiscount()
    {
        if($this->product->modules->isEmpty()) {
            return false;
        }

        foreach($this->product->modules as $module) {
            if($module->moduleParents->count() > 1) {
                return true;
            }
        }

        return false;
    }

    public function applyToModules()
    {
        if($this->product->modules->isEmpty()) {
            return false;
        }

        foreach($this->product->modules as $module) {
            $discount = new \App\Services\Discount\VirtualDiscount($module);
            $discount->apply();
        }
    }

    public function apply()
    {
        $id = $this->getDiscountId();

        $discount = $this->getDiscountValue($id);

        $this->applyToModules();

        $sale = ($discount > 0 || $this->product->discount > 0) ? 1 : 0;

        $this->product->update([
            'virtual_discount' => $discount,
            'virtual_discount_id' => $id,
            'virtual_discount_status' => $this->getDiscountStatus($id, $discount),
            'sale' => $sale,
        ]);

        $this->product->addToIndex();

        return $discount;
    }

    protected function getDiscountStatus($id, $discount)
    {
        if(!$this->product->virtual_discount && $discount > 0) {
            return self::STATUS_ADDED;
        }

        if($id === $this->product->virtual_discount_id && $discount === $this->product->virtual_discount) {
            return self::STATUS_NOT_MODIFIED;
        }

        return self::STATUS_UPDATED;
    }
} 
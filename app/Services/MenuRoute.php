<?php

namespace App\Services;

class MenuRoute
{
    public static function setActive()
    {
        $route = request()->route()->getName();

        $menuItem = \Cache::remember('menu.route.'.$route, config('cache.time'), function() use($route) {
            return \App\Models\MenuItem::where('route', $route)->first();
        });

        if(!$menuItem) {
            return;
        }
        
        request()->merge(['menu_item_id' => $menuItem->id]);
    }
}
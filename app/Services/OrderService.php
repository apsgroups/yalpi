<?php

namespace App\Services;

use App\Exceptions\Frontend\OrderException;
use App\Models\Order;
use App\Models\OrderProduct;
use App\Models\OrderProductProperty;
use App\Models\Payment;
use App\Models\Product;
use App\Models\ProductAttribute;
use App\Models\Shipping;

class OrderService
{
    protected $order;

    protected $products;

    protected $mounting = 0;

    protected $subtotal;

    protected $total;

    protected $amount;

    protected $payment;

    protected $shipping;

    protected $name = '';

    protected $email = '';

    protected $phone = '';

    protected $user_id;

    protected $comment = '';

    protected $oneclick = 0;

    protected $city = '';

    protected $home = '';

    protected $street = '';

    protected $apartment = '';

    protected $client_id = '';

    protected $coupon = '';

    protected $coupon_discount = 0;

    /**
     * @return Order
     */
    public function getOrder()
    {
        if(empty($this->order)) {
            $this->order = new Order;
        }

        return $this->order;
    }

    /**
     * @param Order $order
     */
    public function setOrder($order)
    {
        $this->order = $order;
    }

    /**
     * @return mixed
     */
    public function getProducts()
    {
        return $this->products;
    }

    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }

    public function getUserId()
    {
        if(is_null($this->user_id)) {
            $this->setUserId(auth()->id());
        }

        return $this->user_id;
    }

    public function setClientId($client_id)
    {
        $this->client_id = $client_id;
    }

    public function getClientId()
    {
        return $this->client_id;
    }

    public function setOneclick($state)
    {
        $this->oneclick = (int)$state;
    }

    public function setCoupon($coupon, $discount)
    {
        $this->coupon = $coupon;
        $this->coupon_discount = $discount;
    }

    public function getCoupon()
    {
        return $this->coupon;
        $this->discount = $discount;
    }

    public function getCouponDiscount()
    {
        return $this->coupon_discount;
    }

    public function getOneclick()
    {
        return $this->oneclick;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    public function getPhone()
    {
        return $this->phone;
    }

    public function getStreet()
    {
        return $this->street;
    }

    public function setStreet($street)
    {
        $this->street = $street;
    }

    public function getApartment()
    {
        return $this->apartment;
    }

    public function setApartment($apartment)
    {
        $this->apartment = $apartment;
    }

    public function getCity()
    {
        return $this->city;
    }

    public function setCity($city)
    {
        $this->city = $city;
    }

    public function getHome()
    {
        return $this->home;
    }

    public function setHome($home)
    {
        $this->home = $home;
    }

    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    public function getComment()
    {
        return $this->comment;
    }

    public function save()
    {
        if(!$product=$this->getProducts()) {
            throw new OrderException('Товары не заданы!');
        }

        $order = $this->getOrder();

        $order->name = $this->getName();
        $order->phone = $this->getPhone();
        $order->email = $this->getEmail();
        $order->city = $this->getCity();
        $order->street = $this->getStreet();
        $order->home = $this->getHome();
        $order->apartment = $this->getApartment();

        $order->comment = $this->getComment();

        $order->payment_total = $this->getPaymentTotal();
        $order->shipping_total = $this->getShippingTotal();
        $order->mounting = $this->getMounting();
        $order->mounting_total = $this->getMountingTotal();
        $order->subtotal = $this->getSubtotal();
        $order->total = $this->getTotal();
        $order->amount = $this->getAmount();
        $order->oneclick = $this->getOneclick();
        $order->client_id = $this->getClientId();
        $order->hash = \Uuid::generate();
        $order->coupon = $this->getCoupon();
        $order->coupon_discount = $this->getCouponDiscount();

        if($payment = $this->getPayment()) {
            $order->payment()->associate($payment);
        }

        if($shipping = $this->getShipping()) {
            $order->shipping()->associate($shipping);
        }

        \DB::beginTransaction();

        try {
            $order->save();
            $order = $this->saveProducts($order);
            $this->setOrder($order);
            \DB::commit();
        } catch(\Exception $e) {
            \DB::rollBack();

            throw $e;
        }

        return $order;
    }

    protected function saveProducts(Order $order)
    {
        $orderProducts = $this->getProducts();

        foreach($orderProducts as $product) {
            $modelOrderProduct = $order->products()->create($product['order_product']);

            if(!empty($product['properties'])) {
                foreach($product['properties'] as $propertyId) {
                    $modelOrderProduct->orderProductProperty()->create([
                        'product_property_id' => $propertyId
                    ]);
                }
            }

            if(!empty($product['order_product']['modules'])) {
                $this->saveModules($modelOrderProduct, $product['order_product']['modules']);
            }
        }

        return $order;
    }

    protected function saveModules(OrderProduct $orderProduct, array $modules)
    {
        foreach($modules as $m) {
            $orderProduct->modules()->create([
                'product_id' => $m['model']->id,
                'order_id' => $orderProduct->order_id,
                'title' => $m['model']->title,
                'sku' => $m['model']->sku,
                'price' => $m['model']->price,
                'slug' => $m['model']->slug,
                'amount' => $m['amount'],
            ]);
        }

        return $orderProduct;
    }

    /**
     * @param mixed $products
     */
    public function setProduct(Product $product, $attributeId, $properties, $amount = 1, $modules = [], $desk = null)
    {
        $price = $product->price;

        if($modules) {
            $price = collect($modules)->sum('module_price');
        }

        $orderProduct = [
            'order_product' => [
                'product_id' => $product->id,
                'title' => $product->title,
                'sku' => $product->sku,
                'price' => $price,
                'slug' => $product->slug,
                'amount' => $amount,
                'attribute_id' => $attributeId,
                'modules' => $modules,
            ],
            'properties' => $properties,
        ];

        if($attributeId) {
            $productAttribute = ProductAttribute::find($attributeId);

            if($productAttribute) {
                $orderProduct['order_product']['price'] = $productAttribute->getPrice();
                $orderProduct['order_product']['sku'] = $productAttribute->getSku();
                $orderProduct['order_product']['attribute_title'] = $productAttribute->attribute->title;
                $orderProduct['order_product']['attribute_value'] = $productAttribute->value;
                $orderProduct['order_product']['product_attribute_id'] = $productAttribute->id;
            }
        }

        if($desk) {
            $orderProduct['order_product']['desk_id'] = $desk->id;
            $orderProduct['order_product']['desk_title'] = $desk->title;
        }

        $this->products[] = $orderProduct;

        $this->addToSubtotal($orderProduct['order_product']['price'] * $amount);
        $this->addToAmount($amount);
    }

    protected function addToAmount($amount)
    {
        $amount = $this->getAmount() + $amount;
        $this->setAmount($amount);
    }

    protected function setAmount($amount)
    {
        $this->amount = $amount;
    }

    public function getAmount()
    {
        return $this->amount;
    }

    public function setPayment($id)
    {
        $this->payment = Payment::find($id);
    }

    public function getPayment()
    {
        return $this->payment;
    }

    public function getPaymentTotal()
    {
        if($this->getPayment()) {
            return $this->getPayment()->price;
        }

        return 0;
    }

    public function setShipping($id)
    {
        $this->shipping = Shipping::find($id);
    }

    public function getShipping()
    {
        return $this->shipping;
    }

    public function getShippingTotal()
    {
        if($this->getShipping()) {
            return $this->getShipping()->price;
        }

        return 0;
    }

    protected function addToSubtotal($sum)
    {
        $this->subtotal = $this->getSubtotal() + $sum;
    }

    public function getSubtotal()
    {
        return $this->subtotal;
    }

    public function getTotal()
    {
        return $this->getPaymentTotal() + $this->getShippingTotal() + $this->getSubtotal() + $this->getMountingTotal() - $this->getCouponDiscount();
    }

    public function getMountingTotal()
    {
        $mounting = $this->getMounting();

        if($mounting) {
            return config('order.mounting', 0);
        }

        return 0;
    }

    public function setMounting($state)
    {
        $this->mounting = (bool)$state;
    }

    public function getMounting()
    {
        return $this->mounting;
    }
}
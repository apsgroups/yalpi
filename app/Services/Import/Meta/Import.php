<?php

namespace App\Services\Import\Meta;

use App\Models\Category;
use App\Models\Content;
use App\Models\ContentType;
use App\Models\Product;
use App\Services\Import\Report\Report;

class Import
{
    protected $delimiter = ';';

    protected $path;

    protected $report;

    public function __construct()
    {
        $this->report = Report::getInstance('meta-import');
    }

    public function run()
    {
        $path = $this->upload();

        $row = 0;

        if (($handle = fopen($path, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 0, $this->delimiter)) !== FALSE) {
                $row++;

                if($row === 1) {
                    continue;
                }

                $url = $data[0];

                $routeService = new \App\Services\RouteParserService();
                $segments = explode('/', trim(parse_url($url, PHP_URL_PATH), '/'));
                $segments = array_map('trim', $segments);
                $routeService->setSegments($segments);

                if(!$routeService->parse()) {
                    $this->report->fail($url, 'Parse failed');
                    continue;
                }

                $parts = explode('Controller@', $routeService->getAction());

                $methodName = 'update'.$parts[0].ucfirst($parts[1]);

                if(!method_exists($this, $methodName)) {
                    $this->report->fail($url, 'Method '.$methodName.' not found');
                    continue;
                }

                try {
                    $meta = [
                        'title' => $data[1],
                        'desc' => $data[2],
                        'h1' => $data[3],
                    ];

                    $model = call_user_func([$this, $methodName], $routeService->getQuery(), $meta);

                    $this->report->success($url, $routeService->getAction(), $model);
                } catch(\Exception $e) {
                    $this->report->fail($url, $e->getMessage());
                }

            }

            $this->report->save();

            fclose($handle);
        }

        \File::cleanDirectory(dirname($path));
    }

    protected function upload()
    {
        $path = storage_path('app/import/meta/import');

        \File::cleanDirectory($path);

        \File::exists($path) or \File::makeDirectory($path, 0755, true);

        $file = time().'.csv';

        request()->file('file')->move($path, $file);

        return $path.'/'.$file;
    }

    protected function updateContentShow(array $query, array $meta)
    {
        $content = Content::findOrFail($query['id']);

        $attr = array_filter([
            'page_title' => $meta['title'],
            'metadesc' => $meta['desc'],
            'title' => $meta['h1'],
        ]);

        $content->update($attr);

        return $content;
    }

    protected function updateCategoryIndex(array $query, array $meta)
    {
        $category = Category::findOrFail($query['id']);

        $metaFactory = \App\Services\Meta\MetaFactory::getInstance($category);

        $attr = array_filter([
            'page_title' => $meta['title'],
            'metadesc' => $meta['desc'],
            'alter_title' => $meta['h1'],
            'metakeys' => $metaFactory->keywords(),
        ]);

        $category->update($attr);

        if($category->meta_category_id > 0) {
            throw new \Exception('Category '.$category->title.' has META-template');
        }

        return $category;
    }

    protected function updateContentIndex(array $query, array $meta)
    {
        $type = ContentType::findOrFail($query['id']);

        $attr = array_filter([
            'page_title' => $meta['title'],
            'title' => $meta['h1'],
            'metadesc' => $meta['desc'],
        ]);

        $type->update($attr);

        return $type;
    }

    protected function updateProductShow(array $query, array $meta)
    {
        $product = Product::findOrFail($query['id']);

        $attr = array_filter([
            'page_title' => $meta['title'],
            'title' => $meta['h1'],
            'metadesc' => $meta['desc'],
            'override_meta' => 1,
        ]);

        $product->update($attr);

        return $product;
    }

    protected function updateCategoryModules(array $query, array $meta)
    {
        $product = Product::findOrFail($query['id']);

        $attr = array_filter([
            'modules_page_title' => $meta['title'],
            'modules_title' => $meta['h1'],
            'modules_metadesc' => $meta['desc'],
        ]);

        $product->update($attr);

        return $product;
    }
}
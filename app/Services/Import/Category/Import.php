<?php

namespace App\Services\Import\Category;

use Validator;
use App\Models\Category;
use App\Models\MenuItem;
use App\Services\Import\Report\Report;
use App\Repositories\Backend\Category\CategoryContract;

class Import
{
    protected $delimiter = ';';

    protected $path;

    protected $report;

    protected $headers = [];

    protected $alias = [
            'url' => 'url',
            'синоним (чпу)' => 'slug',
            'заголовок' => 'title',
            'родительская' => 'parent',
            'путь' => 'path',
            'seo-текст' => 'description_bottom',
            'title' => 'page_title',
            'description' => 'metadesc',
            'keywords' => 'metakeys',
            'вопросы-ответы' => 'info_faq',
            'статья' => 'info_articles',
            'отзыв' => 'info_reviews',
            'сайдбар' => 'sidebar',
            'h1' => 'alter_title',
            'посадочная' => 'landing'
        ];

    protected $categoryContract;

    public function __construct()
    {
        $this->report = Report::getInstance('category-import');
        $this->categoryContract = app()->make('\App\Repositories\Backend\Category\CategoryContract');
    }

    public function run()
    {
        $path = $this->upload();

        $this->categories($path);

        $this->report->save();

        \Cache::flush();
    }

    protected function getUrlQuery($url)
    {
        $routeService = new \App\Services\RouteParserService();

        $path = parse_url($url, PHP_URL_PATH);

        $segments = explode('/', trim($path, '/'));

        $segments = array_map('trim', $segments);

        $routeService->setSegments($segments);

        if(!$routeService->parse()) {
            throw new \Exception('Can\'t parse URL '.$url);
        }

        $query = $routeService->getQuery();

        $query['type'] = $routeService->getType();

        return $query;
    }

    protected function categories($path)
    {
        $row = 0;

        $categories = [];

        $headers = [];

        if (($handle = fopen($path, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 0, $this->delimiter)) !== FALSE) {
                $row++;

                if($row === 1) {
                    foreach ($data as $key => $value) {
                        $this->setHeader($key, $value);

                        $this->report->info('Найден заголовок '.$this->getHeader($key));
                    }

                    continue;
                }

                $this->report->info('Добавление категорий');

                $this->addCategory($data);
            }
        }

        fclose($handle);

        \File::cleanDirectory(dirname($path));

        return $categories;
    }

    protected function addCategory($data)
    {
        $post = $this->mapCategory($data);

        $post['published'] = 1;
        $post['in_row'] = 4;
        $post['sitemap'] = 1;
        $post['show_filter'] = 1;
        $post['info_faq'] = !empty($post['info_faq']) ? $this->findArticles($post['info_faq']) : null;
        $post['info_articles'] = !empty($post['info_articles']) ? $this->findArticles($post['info_articles']) : null;
        $post['info_enabled'] = !empty($post['info_faq']) || !empty($post['info_articles']) || !empty($post['info_reviews']) ? 1 : 0;

        $url = $this->findParent($post['url']);

        if(!empty($url['id'])) {
            $post['parent_id'] = $url['id'];
            
            $this->report->success('Найдена родительская категория', $post['parent_id']);
        }

        $validator = Validator::make($post, [
            'url' => 'required',
            'slug' => 'required',
            'title' => 'required',
        ]);

        if ($validator->fails()) {
            $this->report->fail('Не удалось создать категорию: '.implode(';', $validator->errors()->all()));
            return;
        }

        $category = Category::where(['slug' => $post['slug'], 'parent_id' => $post['parent_id']])->first();

        if($category) {
            $category->update($post);
            $this->report->success('Категория '.$post['title'].' с ID '.$category->id.'. обновлена. Родитель '.$post['parent_id']);
        } else {
            try {
                $post['created_by_import'] = 1;
                $category = $this->categoryContract->create($post);
                $this->report->success('Создана категория', $category->id, $category->title);
            } catch(\Exception $e) {
                $this->report->fail('Не удалось создать категорию '.$post['title'].'. Ошибка: '.$e->getMessage());
                return;
            } 
        }

        if(!empty($url['menu_item_id'])) {
            $this->report->info('Создание дочернего пункта меню категории. Родитель '.$url['menu_item_id']);

            $parentMenu = MenuItem::find($url['menu_item_id']);

            $menuItem = MenuItem::where(['type' => 'category', 'item_id' => $category->id])->first();

            if(!$menuItem) {
                $menuItem = $this->addMenuItem($category->title, $category->slug, $category->id, 'category', $parentMenu->id, $parentMenu->menu_id);
            } else {
                $menuItem->title = $category->title;
                $menuItem->save();

                $this->report->success('Пункт меню уже существует.', $menuItem->id, $menuItem->title);
            }

            if(!empty($post['sidebar']) && $post['sidebar'] === '1') {
                array_unshift($post['path'], $post['parent']);
                
                $this->report->info('Путь в сайдбаре: '.implode('>', $post['path']));

                $this->linkSidebarMenuItem($menuItem, $post['path']);
            }
        }
    }

    private function linkSidebarMenuItem($menuItem, $path)
    {
        $parent = null;

        $defaultMenuId = 27;

        foreach ($path as $title) {
            $parentId = !empty($parent->id) ? $parent->id : null;

            $item = MenuItem::where(['title' => $title, 'parent_id' => $parentId])
                ->whereIn('menu_id', [27, 13])
                ->first();

            if(empty($item)) {
                $menuId = !empty($parent->menu_id) ? $parent->menu_id : $defaultMenuId;

                $item = $this->addMenuItem($title, str_slug($title), 0, 'header', $parentId, $menuId);
            }

            $parent = $item;
        }

        $parentId = !empty($parent->id) ? $parent->id : null;

        $menuId = !empty($item->menu_id) ? $item->menu_id : $defaultMenuId;

        $this->addMenuItem($menuItem->title, $menuItem->slug, $menuItem->id, 'menu_item', $parentId, $menuId, $menuId);

        return $item;
    }

    private function addMenuItem($title, $slug, $itemId, $type, $parentId, $menuId)
    {
        $attributes = [
            'user_id' => auth()->id(),
            'title' => $title,
            'slug' => $slug,
            'type' => $type,
            'item_id' => $itemId,
            'parent_id' => $parentId,
            'published' => 1,
            'menu_id' => $menuId
        ];

        $search = [
            'type' => $type, 
            'item_id' => $itemId, 
            'parent_id' => $parentId,
            'menu_id' => $menuId
        ];

        if($type === 'header') {
            $search['title'] = $title;
        }

        $item = MenuItem::where($search)->first();

        if(!$item) {
            $attributes['created_by_import'] = 1;

            $item = MenuItem::create($attributes);

            $this->report->success('Создан пункт меню', $item->id, $item->title);

            return $item;
        }

        $item->update($attributes);

        $this->report->success('Обновлен пункт меню: '. $item->id.', '.$item->title);

        return $item;
    }

    private function findParent($url)
    {
        $parts = explode('/', $url);

        array_pop($parts);

        $url = implode('/', $parts);

        try {                    
            $query = $this->getUrlQuery($url);

            if($query['type'] === 'category' && !empty($query['id'])) {
                return $query;
            }

            $this->report->fail('Не найден родитель для '.$url);
        } catch (\Exception $e) {
            $this->report->fail('Не найден родитель для '.$url.'. Ошибка: '.$e->getMessage());
        }

        return null;
    }

    private function findArticles($urls)
    {
        $ids = [];

        foreach ($urls as $url) {
            try {                    
                $query = $this->getUrlQuery($url);

                if($query['type'] === 'content' && !empty($query['id'])) {
                    $ids[] = $query['id'];
                    continue;
                }

                $this->report->fail('Не найден элемент с URL '.$url);
            } catch (\Exception $e) {
                $this->report->fail('Не найден элемент с URL '.$url.'. Ошибка: '.$e->getMessage());
            }
        }

        return $ids;
    }

    protected function mapCategory($row)
    {
        $mapped = [];

        $castAsArray = ['path', 'info_faq', 'info_articles', 'info_reviews'];

        foreach ($row as $key => $value) {
            $header = $this->getHeader($key);

            if(in_array($header, $castAsArray)) {  
                if(trim($value) !== '') {
                    $mapped[$header][] = $value;
                }

                continue;
            }

            $mapped[$header] = $value;
        }

        return $mapped;
    }

    private function getHeader($num)
    {
        return $this->headers[$num];
    }

    private function setHeader($num, $name)
    {
        $name = trim(mb_strtolower($name, 'utf-8'));

        $m = [];

        if(preg_match('/(путь|вопросы-ответы|статья|отзыв)/', $name, $m)) {
            $name = $m[1];
        }

        $header = isset($this->alias[$name]) ? $this->alias[$name] : $name;

        $this->headers[$num] = $header;
    }

    protected function upload()
    {
        $path = storage_path('app/import/meta/import');

        \File::cleanDirectory($path);

        \File::exists($path) or \File::makeDirectory($path, 0755, true);

        $file = time().'.csv';

        request()->file('file')->move($path, $file);

        return $path.'/'.$file;
    }
}
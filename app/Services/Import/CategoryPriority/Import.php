<?php

namespace App\Services\Import\CategoryPriority;

use Validator;
use App\Models\Category;
use App\Services\Import\Report\Report;
use App\Repositories\Backend\Category\CategoryContract;

class Import
{
    protected $delimiter = ';';

    protected $path;

    protected $report;

    protected $headers = [];

    protected $categoryContract;

    public function __construct()
    {
        $this->report = Report::getInstance('category-priority-import');
    }

    public function run()
    {
        $path = $this->upload();

        $this->categories($path);

        $this->report->save();
    }

    protected function categories($path)
    {
        $row = 0;

        $categories = [];

        $headers = [];

        if (($handle = fopen($path, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 0, $this->delimiter)) !== FALSE) {
                $row++;

                if($row === 1) {
                    continue;
                }

                $this->updateCategory($data[0], $data[1], $data[2]);
            }
        }

        fclose($handle);

        \File::cleanDirectory(dirname($path));

        return $categories;
    }

    protected function updateCategory($title, $parentTitle, $priority)
    {
        $categories = Category::where('title', $parentTitle)->get();

        $category = $this->find($categories, $title);

        if(!$category) {
            $this->report->fail('Категория '.$title.' не найдена в БД. Родительская: '.$parentTitle);
            return;
        }

        $category->product_priority = $priority;

        $category->save();

        $this->report->success($category);

        if($category->children) {
            foreach ($category->children as $category) {
                $category->product_priority = $priority;
                $category->save();
                $this->report->success($category);
            }
        }
    }

    protected function find($categories, $title)
    {
        foreach ($categories as $c) {
            if(mb_strtolower($c->title, 'utf-8') === mb_strtolower($title, 'utf-8')) {
                return $c;
            }
        }

        foreach ($categories as $c) {
            $category = $this->find($c->children, $title);

            if($category) {
                return $category;
            }
        }
    }

    protected function upload()
    {
        $path = storage_path('app/import/category-priority');

        \File::cleanDirectory($path);

        \File::exists($path) or \File::makeDirectory($path, 0755, true);

        $file = time().'.csv';

        request()->file('file')->move($path, $file);

        return $path.'/'.$file;
    }
}
<?php

namespace App\Services\Import\Item;

use App\Events\ProductSaved;
use App\Models\ProductProperty;
use App\Models\Property;

class Characteristic extends Base
{
    public function import()
    {
        if(is_null($this->model)) {
            return false;
        }

        $attributes['property'] = $this->convertOptions();

        event(new ProductSaved($this->model, $attributes));

        $this->model->addToIndex();

        return true;
    }

    protected function convertOptions()
    {
        $attributes = $this->attributes;

        $properties = Property::whereIn('id', array_keys($attributes))->get();

        foreach($properties as $property) {
            if(empty($attributes[$property->id])) {
                continue;
            }

            if(in_array($property->type, ['double'])) {
                $attributes[$property->id] = str_replace(',', '.', $attributes[$property->id]);
            }

            if(in_array($property->type, ['list', 'color'])) {
                $attributes[$property->id] = $this->getPropertyIdByTitle($property, $attributes[$property->id]);
            }
        }

        return $attributes;
    }

    protected function getPropertyIdByTitle($property, $title) {
        if(empty($title)) {
            return 0;
        }

        $value = $property->values()->where('title', $title)->first();

        if(empty($value)) {
            $value = $property->values()->create(['title' => $this->attributes[$property->id]]);
        }

        return $value->id;
    }
}
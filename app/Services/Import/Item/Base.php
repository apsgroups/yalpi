<?php

namespace App\Services\Import\Item;

use App\Exceptions\ImportException;
use App\Exceptions\ImportMissingException;
use App\Models\Product;

abstract class Base
{
    protected $attributes;

    protected $model;

    protected $productContract;

    public function __construct($attributes)
    {
        $this->productContract = app()->make('\App\Repositories\Backend\Product\ProductContract');

        $this->attributes = $attributes;

        $this->model = $this->model();
    }

    abstract public function import();

    protected function model()
    {
        $product = Product::where(['ext_id' => $this->attributes['code']])->first();

        if($product && !$product->import) {
            return null;
        }

        if(empty($product)) {
            throw new ImportMissingException($this->attributes['code'], $this->attributes['name'], $this->attributes['articul']);
        }

        $product->took_import = true;
        $product->updated_by_import = true;

        return $product;
    }
}
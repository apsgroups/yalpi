<?php

namespace App\Services\Import\Product;

use Validator;

use App\Events\ProductSaved;
use App\Models\Product;
use App\Models\Property;
use App\Models\Category;
use App\Models\MenuItem;
use App\Models\Author;
use App\Models\Language;
use App\Models\ProductType;
use App\Services\Import\Report\Report;
use App\Repositories\Backend\Category\CategoryContract;
use App\Repositories\Backend\Product\ProductContract;

class Import
{
    protected $delimiter = ';';

    protected $path;

    protected $report;

    protected $icons = [];
    
    protected $iconPack = [];

    protected $headers = [];

    protected $headerNames = [];
    
    protected $alias = [
            'url' => 'product_url',
            'url-урока' => 'buy_url',
            'наименование урока' => 'title',
            'тезисное описание' => 'preview_title',
            'категория' => 'path',
            'преподаватель' => 'authors',
            'о преподавателе' => 'author_about',
            'формат' => 'product_type',
            'тип обучения' => 'learning_type',
            'язык' => 'language',
            'рейтинг' => 'rate',
            'оценка' => 'rate_count',
            'вес' => 'priority',
            'фильтр' => 'filters',
            'посадочная' => 'landing',
            'цена' => 'price',
            'пак иконок' => 'icon_pack'
        ];

    protected $categoryContract;

    protected $productContract;

    public function __construct()
    {
        $this->report = Report::getInstance('product-import');
        $this->categoryContract = app()->make('\App\Repositories\Backend\Category\CategoryContract');
        $this->productContract = app()->make('\App\Repositories\Backend\Product\ProductContract');
    }

    public function run()
    {
        $path = $this->upload();

        $this->process($path);

        $this->report->save();

        \Cache::flush();
    }

    protected function getUrlQuery($url)
    {
        $routeService = new \App\Services\RouteParserService();

        $path = parse_url($url, PHP_URL_PATH);

        $segments = explode('/', trim($path, '/'));

        $segments = array_map('trim', $segments);

        $routeService->setSegments($segments);

        if(!$routeService->parse()) {
            throw new \Exception('Can\'t parse URL '.$url);
        }

        $query = $routeService->getQuery();

        $query['type'] = $routeService->getType();

        return $query;
    }

    protected function process($path)
    {
        set_time_limit(1800);

        $row = 0;

        $categories = [];

        $headers = [];

        if (($handle = fopen($path, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 0, $this->delimiter)) !== FALSE) {
                $row++;

                if($row === 1) {
                    foreach ($data as $key => $value) {
                        $this->setHeader($key, $value);

                        $this->report->info('Найден заголовок '.$this->getHeader($key));
                    }

                    continue;
                }

                $this->report->info('Добавление товаров');

                $this->addProduct($data);
            }
        }

        fclose($handle);

        \File::cleanDirectory(dirname($path));

        return $categories;
    }

    protected function getAuthors($authors, $about)
    {
        $ids = [];

        foreach ($authors as $key => $value) {
            $author = Author::firstOrCreate(['name' => trim($value)]);

            if($about) {
                $author->about = $about;
                $author->save();
            }

            $ids[] = $author->id;
        }

        return $ids;
    }

    protected function getLanguage($title)
    {
        $language = Language::firstOrCreate(['title' => trim($title)]);

        return $language->id;
    }

    protected function getPriceType($title)
    {
        $priceTypes = config('site.price_type');

        $ind = array_search($title, $priceTypes);

        if($ind !== false) {
            return $ind;
        }

        return 0;
    }

    protected function getPrice($value)
    {
        if($this->getPriceType($value) === 0) {
            return preg_replace('/[^0-9]/i', '', $value);
        }

        return 0;
    }

    protected function getProductType($title)
    {
        $productType = ProductType::firstOrCreate(['title' => trim($title)]);

        return $productType->id;
    }

    protected function addProduct($data)
    {
        $post = $this->mapColumns($data);
     
        if(empty($post['filters'])) {
            $post['filters'] = [];
        }

        if(empty($post['path'])) {
            $this->report->fail('Не удалось найти категории для товара: '.$post['title']);
            return;
        }

        if(!empty($post['product_url'])) {
            $url = explode('/', trim($post['product_url'], '/'));
            $categoryIds = $this->processCategories($post);
            $slug = end($url);
        } else {
            $categoryIds = $this->getCategoriesByPath($post['path']);
            $slug = str_slug($post['title']);
        }

        if(empty($categoryIds)) {
            $this->report->fail('Не удалось найти категории для товара: '.$post['title']);
            return;
        }

        if(empty($post['author_about'])) {
            $post['author_about'] = '';
        }

        $attr = [
            'title' => $post['title'],
            'published' => 1,
            'sitemap' => 1,
            'category_id' => end($categoryIds),
            'categories' => $categoryIds,
            'slug' => $slug,
            'property' => $this->findOrCreateProperties($post['filters']),
            'authors' => $this->getAuthors(explode(',', $post['authors']), $post['author_about']),
            'language_id' => $this->getLanguage($post['language']),
            'price_base' => $this->getPrice($post['price']),
            'price_type' => $this->getPriceType($post['price']),
            'product_type_id' => $this->getProductType($post['product_type']),
            'start_learning' => $post['buy_url'],
            'priority' => !empty($post['priority']) ? $post['priority'] : 0,
            'rate_count' => !empty($post['rate_count']) ? $post['rate_count'] : 0,
            'free_cources' => [],
            'cource_program' => [],
            'icon_pack' => !empty($post['icon_pack']) ? $post['icon_pack'] : null,
            'icon_name' => null,
            'rate' => $post['rate'],
        ];

        $validator = Validator::make($attr, [
            'category_id' => 'required',
            'title' => 'required',
        ]);

        if(!empty($attr['icon_pack']) && $icon = $this->getIconName(34/*$attr['category_id']*/, $attr['icon_pack'])) {
            $attr['icon_name'] = $icon;
        }

        if(!empty($post['buy_url']) && (int)$attr['product_type_id'] === 8) {
        	$attr['video'] = str_replace('https://youtu.be/', '', trim($post['buy_url']));
        }

        if ($validator->fails()) {
            $this->report->fail('Не удалось создать товар: '.implode(';', $validator->errors()->all()));
            return;
        }

        $product = Product::where(['slug' => $attr['slug'], 'category_id' => $attr['category_id']])->first();

        if($product) {
            $product = $this->productContract->update($product->id, $attr);
            event(new ProductSaved($product, $attr, false));
            $this->report->success('Товар '.$post['title'].' с ID '.$product->id.'. обновлен. Родитель '.$attr['category_id']);
        } else {
            try {
            	$attr['rate'] = !empty($post['rate']) ? $post['rate'] : 3;
            	$attr['rate_round'] = $attr['rate'];

                $product = $this->productContract->create($attr);

                event(new ProductSaved($product, $attr, false));

                $this->report->success('Обновлен товар', $product->id, $product->title);
            } catch(\Exception $e) {
                $this->report->fail('Не удалось создать товар '.$attr['title'].'. Ошибка: '.$e->getMessage());
                return;
            } 
        }
    }

    protected function getIconName($categoryId, $pack)
    {
        if(empty($this->iconPack[$categoryId][$pack])) {
            $this->iconPack[$categoryId][$pack] = $this->getIcons($categoryId, $pack);
        }

        if(!empty($this->iconPack[$categoryId][$pack])) {
            return array_shift($this->iconPack[$categoryId][$pack]);
        }

        return null;
    }

    protected function getIcons($categoryId, $pack)
    {
        if(isset($this->icons[$categoryId][$pack])) {
            return $this->icons[$categoryId][$pack];
        }

        $this->icons[$categoryId][$pack] = [];

        $path = public_path().'/images/icon/'.$categoryId.'/'.$pack;

        if(!file_exists($path)) {
            return $this->icons[$categoryId][$pack];
        }

        $icons = array_diff(scandir($path), array('..', '.'));

        if($icons) {
            foreach ($icons as $key => $value) {
                $this->icons[$categoryId][$pack][] = pathinfo($value, PATHINFO_FILENAME);
            }
        }

        return $this->icons[$categoryId][$pack];
    }

    protected function getCategoriesByPath($path)
    {
        $categories = [];

        $parentId = null;

        $url = '';

        foreach ($path as $key => $title) {
            $category = $this->findOrCreateCategory($title, str_slug($title), $parentId);

            $parentId = $category->id;

            $categories[] = $category->id;
        }

        return $categories;
    }

    protected function processCategories($post)
    {
        $parentId = null;

        $url = str_replace('yalpi.ru/', '', $post['product_url']);
        $url = trim($url);
        $url = trim($url, '/');
        $url = str_replace('#', '-', $url);
        $url = preg_replace('/\/+/', '/', $url);

        $urls = explode('/', $url);

        $parentPath = [];

        $ids = [];

        $i = 0;

        if(empty($post['path'])) {
            $this->report->fail('Не указана ни одна категория для '.$post['product_url']);
            return false;
        }

        foreach($post['path'] as $key => $value) {
            /*
            try {
                if($parentPath) {
                    $router = $this->findParent(implode('/', $parentPath));

                    if(!empty($router['id'])) {
                        $parentId = $router['id'];
                        
                        $this->report->success('Найдена родительская категория', $parentId);
                    }
                }
            } catch(\Exception $e) { }
            */

            try {
                $category = $this->findOrCreateCategory($value, $urls[$i], $parentId);
                $parentId = $category->id;
                $parentPath[] = $urls[$i];
                $ids[] = $category->id;

                $i++; 
            } catch(\Exception $e) {
                $this->report->fail('Ошибка при поиске\создании категории: '.$e->getMessage());
                return false;                
            }
        }

        return $ids;
    }

    protected function findOrCreateCategory($title, $slug, $parentId)
    {
        $post = compact('title', 'slug');
        $post['parent_id'] = $parentId;

        $validator = Validator::make($post, [
            'title' => 'required',
            'slug' => 'required'
        ]);

        if ($validator->fails()) {
            $this->report->fail('Не удалось создать категорию: '.$title.'/'.$slug.'/'.implode(';', $validator->errors()->all()));
            throw new \Exception('Не пройдена валидация');
        }

        $post['published'] = 1;
        $post['sitemap'] = 1;
        $post['show_filter'] = 1;

        $category = Category::where(['slug' => $post['slug'], 'parent_id' => $post['parent_id']])->first();

        if($category) {
            $category->update($post);
            $this->report->success('Категория '.$post['title'].' с ID '.$category->id.'. обновлена. Родитель '.$post['parent_id']);
        } else {
            try {
                $post['created_by_import'] = 1;
                $post['docs'] = [];
                $category = $this->categoryContract->create($post);
                $this->report->success('Создана категория', $category->id, $category->title);
            } catch(\Exception $e) {
                $this->report->fail('Не удалось создать категорию '.$post['title'].'. Ошибка: '.$e->getMessage());
                return;
            } 
        }

        if($parentId) {
            $parentMenu = MenuItem::where(['type' => 'category', 'item_id' => $parentId])->first();
            $parentMenuItemId = $parentMenu->id;
            $parentMenuId = $parentMenu->menu_id;
        } else {
            $parentMenuItemId = null;
            $parentMenuId = 5;
        }

        $this->report->info('Создание дочернего пункта меню категории. Родитель '.$parentId);

        $menuItem = MenuItem::where(['type' => 'category', 'item_id' => $category->id])->first();

        if(!$menuItem) {
            $menuItem = $this->addMenuItem($category->title, $category->slug, $category->id, 'category', $parentMenuItemId, $parentMenuId);
        } else {
            $menuItem->title = $category->title;
            $menuItem->save();

            $this->report->success('Пункт меню уже существует.', $menuItem->id, $menuItem->title);
        }

        return $category;
    }

    protected function findOrCreateProperties($filters)
    {
        $attr = [];

        foreach ($filters as $key => $value) {
            $title = str_replace('фильтр ', '', $key);

            $property = $this->findOrCreateProperty($title);

            $list = explode(',', $value);

            foreach ($list as $v) {
                $propertyValue = $property->values()->firstOrCreate(['title' => trim($v)]);

                $attr[$property->id][] = $propertyValue->id;
            }
        }

        return $attr;
    }

    protected function findOrCreateProperty($title)
    {
        $property = Property::where('title', $title)->first();

        if($property) {
            return $property;
        }

        return Property::create([
            'title' => $title,
            'filter' => 1,
            'type' => 'list',
            'multiple' => 1,
        ]);
    }

    private function addMenuItem($title, $slug, $itemId, $type, $parentId, $menuId)
    {
        $attributes = [
            'user_id' => auth()->id(),
            'title' => $title,
            'slug' => $slug,
            'type' => $type,
            'item_id' => $itemId,
            'parent_id' => $parentId,
            'published' => 1,
            'menu_id' => $menuId
        ];

        $search = [
            'type' => $type, 
            'item_id' => $itemId, 
            'parent_id' => $parentId,
            'menu_id' => $menuId
        ];

        if($type === 'header') {
            $search['title'] = $title;
        }

        $item = MenuItem::where($search)->first();

        if(!$item) {
            $attributes['created_by_import'] = 1;

            $item = MenuItem::create($attributes);

            $this->report->success('Создан пункт меню', $item->id, $item->title);

            return $item;
        }

        $item->update($attributes);

        $this->report->success('Обновлен пункт меню: '. $item->id.', '.$item->title);

        return $item;
    }

    private function findParent($url)
    {
        $parts = explode('/', $url);

        array_pop($parts);

        $url = implode('/', $parts);

        try {                    
            $query = $this->getUrlQuery($url);

            if($query['type'] === 'category' && !empty($query['id'])) {
                return $query;
            }

            $this->report->fail('Не найден родитель для '.$url);
        } catch (\Exception $e) {
            $this->report->fail('Не найден родитель для '.$url.'. Ошибка: '.$e->getMessage());
        }

        return null;
    }

    protected function mapColumns($row)
    {
        $mapped = [];

        $castAsArray = ['path', 'filters'];

        foreach ($row as $key => $value) {
            $header = $this->getHeader($key);
            $name = $this->getHeaderName($key);

            if(in_array($header, $castAsArray)) {  
                if(trim($value) !== '') {
                    $mapped[$header][$name] = $value;
                }

                continue;
            }

            $mapped[$header] = $value;
        }

        return $mapped;
    }

    private function getHeader($num)
    {
        return $this->headers[$num];
    }

    private function getHeaderName($num)
    {
        return $this->headerNames[$num];
    }

    private function setHeader($num, $name)
    {
        $name = trim(mb_strtolower($name, 'utf-8'));
        $this->headerNames[$num] = $name;

        $m = [];

        if(preg_match('/(категория|фильтр)/', $name, $m)) {
            $name = $m[1];
        }

        $header = isset($this->alias[$name]) ? $this->alias[$name] : $name;

        $this->headers[$num] = $header;
    }

    protected function upload()
    {
        $path = storage_path('app/import/meta/import');

        \File::cleanDirectory($path);

        \File::exists($path) or \File::makeDirectory($path, 0755, true);

        $file = time().'.csv';

        request()->file('file')->move($path, $file);

        return $path.'/'.$file;
    }
}
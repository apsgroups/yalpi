<?php

namespace App\Services\Import\Report;

use App\Models\Import\ImportReport;

class ReportDestroyer
{
    public static function destroy($id)
    {
        $report = ImportReport::find($id);

        $path = ReportFile::getFilepath($report);

        $report->delete();

        @unlink($path);
    }
}
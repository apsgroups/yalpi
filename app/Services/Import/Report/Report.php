<?php

namespace App\Services\Import\Report;

use App\Exceptions\ImportException;

class Report
{
    protected static $instance;

    protected static $operation;

    public static function getInstance($operation, $id = null)
    {
        if(!is_null(self::$instance)) {
            return self::$instance;
        }

        $class = self::getClassName($operation);

        if(!class_exists($class)) {
            throw new ImportException($class . ' not found!');
        }

        $instance = new $class($operation, $id);

        if(!$instance instanceof ReportInterface) {
            throw new ImportException($class . ' must implement \App\Services\Import\Report\ReportInterface!');
        }

        self::$instance = $instance;

        return $instance;
    }

    protected static function getClassName($operation)
    {
        $name = studly_case($operation);

        return '\App\Services\Import\Report\Extend\\'.$name.'Report';
    }
}
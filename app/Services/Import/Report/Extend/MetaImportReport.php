<?php

namespace App\Services\Import\Report\Extend;

use App\Services\Import\Report\BaseReport;

class MetaImportReport extends BaseReport
{
    protected $columnHeaders = [
        'URL',
        'STATUS',
        'MESSAGE',
        'MODEL',
        'ID',
        'TITLE',
    ];

    public function fail($url, $msg)
    {
        $this->add([
            $url,
            'FAIL',
            $msg,
        ]);
    }

    public function success($url, $action, $model)
    {
        $this->add([
            $url,
            'OK',
            $action,
            class_basename($model),
            $model->id,
            $model->title
        ]);
    }
}
<?php

namespace App\Services\Import\Report\Extend;

use App\Services\Import\Report\BaseReport;

class PriceImportReport extends BaseReport
{
    protected $columnHeaders = [
        'ID',
        'Наименование',
        'Артикул',
        'Розница',
        'Опт',
        'Скидка',
        'Новинка',
        'Распродажа',
        'Предзаказ',
        'Скоро',
    ];
}
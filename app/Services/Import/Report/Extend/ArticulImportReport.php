<?php

namespace App\Services\Import\Report\Extend;

use App\Services\Import\Report\BaseReport;

class ArticulImportReport extends BaseReport
{
    protected $columnHeaders = [
        'ID',
        'Наименование',
        'Артикул',
        'Обновлено',
    ];
}
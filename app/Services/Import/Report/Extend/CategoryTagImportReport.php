<?php

namespace App\Services\Import\Report\Extend;

use App\Services\Import\Report\BaseReport;

class CategoryTagImportReport extends BaseReport
{
    protected $columnHeaders = [
        'STATUS',
        'MESSAGE',
        'ID',
        'TITLE',
    ];

    public function fail($msg)
    {
        $this->add([
            'FAIL',
            $msg,
        ]);
    }

    public function info($msg)
    {
        $this->add([
            'INFO',
            $msg,
        ]);
    }

    public function success($id, $title, $tag)
    {
        $this->add([
            'OK',
            $id,
            $title,
            $tag
        ]);
    }
}
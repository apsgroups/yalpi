<?php

namespace App\Services\Import\Report\Extend;

use App\Services\Import\Report\BaseReport;

class TagImportReport extends BaseReport
{
    protected $columnHeaders = [
        'URL SRC',
        'URL',
        'TITLE',
        'STATUS',
        'MESSAGE',
        'ID',
        'TITLE',
    ];

    public function fail($src, $url, $title, $msg)
    {
        $this->add([
            $src,
            $url,
            $title,
            'FAIL',
            $msg,
        ]);
    }

    public function success($src, $url, $title, $model)
    {
        $this->add([
            $src,
            $url,
            $title,
            'OK',
            class_basename($model),
            $model->id,
            $model->title
        ]);
    }
}
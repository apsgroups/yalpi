<?php

namespace App\Services\Import\Report\Extend;

use App\Models\Property;
use App\Services\Import\Report\BaseReport;

class CharacteristicImportReport extends BaseReport
{
    protected $columnHeaders = [
        'ID',
        'Наименование',
        'Артикул',
    ];

    public function __construct($operation, $id = null)
    {
        $characteristics = config('import.characteristics', []);

        foreach($characteristics as $name => $id) {
            if($property = Property::find($id)) {
                $this->columnHeaders[] = $property->name;
            }
        }

        parent::__construct($operation, $id = null);
    }
}
<?php

namespace App\Services\Import\Report\Extend;

use App\Services\Import\Report\BaseReport;

class CategoryLinkImportReport extends BaseReport
{
    protected $columnHeaders = [
        'STATUS',
        'MESSAGE',
        'ID',
        'TITLE',
    ];

    public function fail($msg)
    {
        $this->add([
            'FAIL',
            $msg,
        ]);
    }

    public function info($msg)
    {
        $this->add([
            'INFO',
            $msg,
        ]);
    }

    public function success($category)
    {
        $this->add([
            'OK',
            $category->id,
            $category->title
        ]);
    }
}
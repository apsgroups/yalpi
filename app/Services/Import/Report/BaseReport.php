<?php

namespace App\Services\Import\Report;

use App\Models\Import\ImportReport;

class BaseReport implements ReportInterface
{
    protected $model;
    protected $operation;
    protected $delimiter = ';';
    protected $filepath;
    protected $items = [];
    protected $columnHeaders;

    public function __construct($operation, $id = null)
    {
        $this->operation = $operation;

        $this->model = $this->model($id);

        $this->filepath = $this->initFile();
    }

    protected function model($id)
    {
        if(!is_null($this->model)) {
            return $this->model;
        }

        if($id && $model = ImportReport::find($id)) {
            return $model;
        }

        return ImportReport::create([
            'operation' => $this->operation,
            'user_id' => auth()->id(),
        ]);
    }

    public function add(array $item)
    {
        $this->items[] = $item;
    }

    protected function columnHeaders()
    {
        if($this->columnHeaders) {
            $this->add($this->columnHeaders);
        };
    }

    public function save()
    {
        $fp = fopen($this->filepath, 'w');

        foreach($this->items as $item) {
            fputcsv($fp, $item, $this->delimiter);
        }

        fclose($fp);
    }

    protected function initFile()
    {
        $path = ReportFile::getFilepath($this->model);

        \File::exists(dirname($path)) or \File::makeDirectory(dirname($path), 0755, true);

        $fp = fopen($path, 'w');

        fprintf($fp, chr(0xEF).chr(0xBB).chr(0xBF));

        fclose($fp);

        if(!file_exists($path)) {
            $this->columnHeaders();
        }

        return $path;
    }
}
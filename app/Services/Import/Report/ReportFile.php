<?php

namespace App\Services\Import\Report;

use App\Models\Import\ImportReport;

class ReportFile
{
    public static function getFilepath(ImportReport $model)
    {
        $path = storage_path('app/import/reports/'.$model->operation);

        $file = $model->id.'.csv';

        return $path.'/'.$file;
    }
}
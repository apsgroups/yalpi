<?php

namespace App\Services\Import\Report;

interface ReportInterface
{
    public function add(array $data);
}
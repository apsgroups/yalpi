<?php

namespace App\Services\Import\ColorPriority;

use App\Models\Property;
use App\Models\PropertyValue;
use App\Services\Import\Report\Report;

class Import
{
    protected $delimiter = ';';

    protected $path;

    protected $report;

    protected $categoryContract;

    public function __construct()
    {
        $this->report = Report::getInstance('color-priority-import');
    }

    public function run()
    {
        $path = $this->upload();

        $this->colors($path);

        $this->report->save();
    }

    protected function colors($path)
    {
        $row = 0;

        $colors = [];

        if (($handle = fopen($path, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 0, $this->delimiter)) !== FALSE) {
                $row++;

                if($row === 1) {
                    continue;
                }

                $this->updateColor($data[0], $data[1]);
            }
        }

        fclose($handle);

        \File::cleanDirectory(dirname($path));

        return $colors;
    }

    protected function updateColor($title, $priority)
    {
        $propertyId = \Settings::get('body_color_id');

        if(!$propertyId) {
            $this->report->fail('Поле для цвета не найдено');
            return;
        }

        $propertyValue = PropertyValue::where([
            'title' => trim($title), 'property_id' => $propertyId
        ])->first();

        if(!$propertyValue) {
            $this->report->fail('Цвет не найден: '.$title);
            return;
        }

        $propertyValue->sort_priority = $priority;

        $propertyValue->save();

        $this->report->success($propertyValue);
    }

    protected function upload()
    {
        $path = storage_path('app/import/color-priority');

        \File::cleanDirectory($path);

        \File::exists($path) or \File::makeDirectory($path, 0755, true);

        $file = time().'.csv';

        request()->file('file')->move($path, $file);

        return $path.'/'.$file;
    }
}
<?php

namespace App\Services\Import\Tag;

use App\Models\Category;
use App\Models\PrettyUrl;
use App\Services\Import\Report\Report;

class Import
{
    protected $delimiter = ';';

    protected $path;

    protected $report;

    public function __construct()
    {
        $this->report = Report::getInstance('tag-import');
    }

    public function run()
    {
        $path = $this->upload();

        $tags = $this->extractTags($path);

        foreach($tags as $k => $v) {
            try {
                $model = $this->getModelByUrl($v['src']);
            } catch(\Exception $e) {
                $this->report->fail($v['src'], '', '', $e->getMessage());
                continue;
            }

            $model->tags()->sync([]);
            foreach($v['items'] as $ordering => $tag) {
                $ordering++;

                if(mb_strtolower(trim($tag['title']), 'utf-8') === 'все кухни') {
                    $ordering = 0;
                }

                try {
                    $tagModel = $this->findOrCreateTag($tag['title'], $tag['url']);

                    $model->tags()->save($tagModel, ['ordering' => $ordering]);

                    $this->report->success($v['src'], $tag['url'], $tag['title'], $model);
                } catch(\Exception $e) {
                    $this->report->fail($v['src'], $tag['url'], $tag['title'], $e->getMessage());
                }
            }
        }

        $this->report->save();
    }

    protected function getModelByUrl($url)
    {
        $routeService = new \App\Services\RouteParserService();

        $path = parse_url($url, PHP_URL_PATH);

        $segments = explode('/', trim($path, '/'));

        $segments = array_map('trim', $segments);

        $routeService->setSegments($segments);

        if(!$routeService->parse()) {
            throw new \Exception('Can\'t parse category');
        }

        if(strpos($routeService->getAction(), 'Category')) {
            throw new \Exception('Not a category');
        }

        $prettyUrl = PrettyUrl::where('pretty_url', $path)->first();

        if($prettyUrl) {
            return $prettyUrl;
        }

        $query = $routeService->getQuery();
 
        if(!isset($query['id'])) {
            throw new \Exception('Can\'t parse URL');
        }

        return Category::findOrFail($query['id']);
    }

    protected function findOrCreateTag($title, $url)
    {
        $model = $this->getModelByUrl($url);

        $tag = $model->tagLinks()->firstOrCreate(['title' => $title]);

        $tag->published = 1;

        $tag->save();

        return $tag;
    }

    protected function extractTags($path)
    {
        $row = 0;

        $tags = [];

        if (($handle = fopen($path, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 0, $this->delimiter)) !== FALSE) {
                $row++;

                if($row === 1) {
                    continue;
                }

                $tag = [];

                foreach($data as $k => $v) {
                    if($k === 0) {
                        $tags[$row]['src'] = $v;
                        continue;
                    }

                    $tagKey = ($k % 2 === 0) ? 'url' : 'title';

                    $tag[$tagKey] = $v;

                    if($tagKey == 'url') {
                        $tags[$row]['items'][] = $tag;

                        $tag = [];
                    }
                }
            }
        }

        fclose($handle);

        \File::cleanDirectory(dirname($path));

        return $tags;
    }

    protected function upload()
    {
        $path = storage_path('app/import/meta/import');

        \File::cleanDirectory($path);

        \File::exists($path) or \File::makeDirectory($path, 0755, true);

        $file = time().'.csv';

        request()->file('file')->move($path, $file);

        return $path.'/'.$file;
    }
}
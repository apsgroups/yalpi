<?php

namespace App\Services\Import\Formatters;

use App\Services\Import\Stock;

class Price
{
    protected $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function items()
    {
        return $this->data;
    }
}
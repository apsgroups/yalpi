<?php

namespace App\Services\Import\Formatters;

class Characteristic
{
    protected $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function items()
    {
        $list = [];

        $characteristics = config('import.characteristics', []);

        foreach($this->data as $item) {
            $data = [];

            $data['code'] = $item->UID;
            $data['name'] = $item->Name;

            foreach($characteristics as $name => $id) {
                $data[$id] = $item->$name;
            }

            $list[] = $data;
        }

        return $list;
    }
}
<?php

namespace App\Services\Import\CategoryLink;

use Validator;
use App\Models\Category;
use App\Services\Import\Report\Report;
use App\Repositories\Backend\Category\CategoryContract;

class Import
{
    protected $delimiter = ';';

    protected $path;

    protected $report;

    protected $headers = [];

    protected $alias = [
            'url' => 'url',
            'выводить заголовок' => 'use_sub_category_alt_title',
            'выводить категорию' => 'sub_categories_src',
            'шаблон дочерних' => 'child_template_src',
        ];

    protected $categoryContract;

    public function __construct()
    {
        $this->report = Report::getInstance('category-link-import');
        $this->categoryContract = app()->make('\App\Repositories\Backend\Category\CategoryContract');
    }

    public function run()
    {
        $path = $this->upload();

        $this->categories($path);

        $this->report->save();

        \Cache::flush();
    }

    protected function getUrlQuery($url)
    {
        $routeService = new \App\Services\RouteParserService();

        $path = parse_url($url, PHP_URL_PATH);

        $segments = explode('/', trim($path, '/'));

        $segments = array_map('trim', $segments);

        $routeService->setSegments($segments);

        if(!$routeService->parse()) {
            throw new \Exception('Can\'t parse URL '.$url);
        }

        $query = $routeService->getQuery();

        $query['type'] = $routeService->getType();

        return $query;
    }

    protected function categories($path)
    {
        $row = 0;

        $categories = [];

        $headers = [];

        if (($handle = fopen($path, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 0, $this->delimiter)) !== FALSE) {
                $row++;

                if($row === 1) {
                    foreach ($data as $key => $value) {
                        $this->setHeader($key, $value);

                        $this->report->info('Найден заголовок '.$this->getHeader($key));
                    }

                    continue;
                }

                $this->report->info('Обновление категорий');

                $this->saveCategory($data);
            }
        }

        fclose($handle);

        \File::cleanDirectory(dirname($path));

        return $categories;
    }

    protected function findByUrl($url) {     
        try {                    
            $query = $this->getUrlQuery($url);

            if($query['type'] !== 'category') {
                throw new \Exception('URL не является категорией '.$url);
            }

            if(empty($query['id'])) {
                throw new \Exception('Не найден ID категории для '.$url);
            }
        } catch (\Exception $e) {
            $this->report->fail('Не найдена категория для '.$url.'. Ошибка: '.$e->getMessage());
            return null;
        }

        return Category::find($query['id']);
    }

    protected function findTemplate($template) {
        $templates = config('backend.category.child_templates');

        foreach ($templates as $key => $value) {
            if(strtolower($value) === strtolower($template)) {
                return $key;
            }
        }

        return null;
    }

    protected function subCategories($subcategories) {
        $ids = [];

        if(empty($subcategories)) {
            return null;
        }

        foreach ($subcategories as $key => $value) {
            $category = $this->findByUrl($value);

            if($category) {
                $ids[] = $category->id;
            }
        }

        return $ids;
    }

    protected function saveCategory($data)
    {
        $post = $this->mapCategory($data);

        $post['child_template'] = $this->findTemplate($post['child_template_src']);
        $post['sub_categories'] = $this->subCategories($post['sub_categories_src']);

        $category = $this->findByUrl($post['url']);

        if(!$category) {
            $this->report->fail('Категория с URL '.$post['url'].' не найдена в БД.');
            return;
        }

        $category->update($post);
        $this->report->success($category);
    }

    protected function mapCategory($row)
    {
        $mapped = [];

        $castAsArray = ['sub_categories_src'];

        foreach ($row as $key => $value) {
            $header = $this->getHeader($key);

            if(in_array($header, $castAsArray)) {  
                if(trim($value) !== '') {
                    $mapped[$header][] = $value;
                }

                continue;
            }

            $mapped[$header] = $value;
        }

        return $mapped;
    }

    private function getHeader($num)
    {
        return $this->headers[$num];
    }

    private function setHeader($num, $name)
    {
        $name = trim(mb_strtolower($name, 'utf-8'));

        $m = [];

        if(preg_match('/(выводить категорию)/', $name, $m)) {
            $name = $m[1];
        }

        $header = isset($this->alias[$name]) ? $this->alias[$name] : $name;

        $this->headers[$num] = $header;
    }

    protected function upload()
    {
        $path = storage_path('app/import/category-links');

        \File::cleanDirectory($path);

        \File::exists($path) or \File::makeDirectory($path, 0755, true);

        $file = time().'.csv';

        request()->file('file')->move($path, $file);

        return $path.'/'.$file;
    }
}
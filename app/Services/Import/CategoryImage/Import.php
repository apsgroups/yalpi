<?php

namespace App\Services\Import\CategoryImage;

use App\Models\Category;
use App\Services\Import\Report\Report;
use App\Services\Image\ImageService;

class Import
{
    protected $delimiter = ';';

    protected $report;

    public function __construct()
    {
        $this->report = Report::getInstance('category-image-import');
    }

    public function run()
    {
        $path = $this->upload();

        $this->categories($path);

        $this->report->save();

        \Cache::flush();
    }

    protected function getUrlQuery($url)
    {
        $routeService = new \App\Services\RouteParserService();

        $path = parse_url($url, PHP_URL_PATH);

        $segments = explode('/', trim($path, '/'));

        $segments = array_map('trim', $segments);

        $routeService->setSegments($segments);

        if(!$routeService->parse()) {
            throw new \Exception('Can\'t parse URL '.$url);
        }

        $query = $routeService->getQuery();

        $query['type'] = $routeService->getType();

        return $query;
    }

    protected function categories($path)
    {
        $row = 0;

        $categories = [];

        $headers = [];

        if (($handle = fopen($path, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 0, $this->delimiter)) !== FALSE) {
                $row++;

                if($row === 1) {
                    foreach ($data as $key => $value) {
                        $this->report->info('Найден заголовок '.$value);
                    }

                    continue;
                }
                
                if(empty($data[0])) {
                    continue;
                }

                $tag = empty($data[1]) ? '' : $data[1];

                $this->image($data[0], $tag);
            }
        }

        fclose($handle);

        \File::cleanDirectory(dirname($path));

        return $categories;
    }

    protected function findByUrl($url)
    {
        try {                    
            $query = $this->getUrlQuery($url);

            if($query['type'] !== 'category' || empty($query['id'])) {
                throw new \Exception('URL не является категорией');
            }
        } catch (\Exception $e) {
            $this->report->fail('Не найдена категория с URL: '.$url.'. Ошибка: '.$e->getMessage());
            return null;
        }

        return Category::find($query['id']);
    }

    protected function image($url, $imageUrl)
    {
        if(!$category = $this->findByUrl($url)) {
            return;
        }

        $imageUrl = 'uploads/media/category-images-import/'.ltrim($imageUrl, '/');

        if(empty($imageUrl)) {
            $this->report->fail('Для категории '.$category->title.'('.$category->id.') не задана картинка');
            return null;
        }

        $path = public_path($imageUrl);

        if(!file_exists($path) || !is_file($path)) {
            $this->report->fail('Изображение для '.$category->title.'('.$category->id.') '.$imageUrl.' не найдено');
            return null;
        }

        $imageService = new ImageService('custom', $category);

        $image = $imageService->saveByUrl($path);

        $category->image_id = $image->id;
        $category->save();

        $this->report->success($category);
    }

    protected function upload()
    {
        $path = storage_path('app/import/category-image/import');

        \File::cleanDirectory($path);

        \File::exists($path) or \File::makeDirectory($path, 0755, true);

        $file = time().'.csv';

        request()->file('file')->move($path, $file);

        return $path.'/'.$file;
    }
}
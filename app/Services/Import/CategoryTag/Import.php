<?php

namespace App\Services\Import\CategoryTag;

use App\Models\Category;
use App\Services\Import\Report\Report;

class Import
{
    protected $delimiter = ';';

    protected $report;

    public function __construct()
    {
        $this->report = Report::getInstance('category-tag-import');
    }

    public function run()
    {
        $path = $this->upload();

        $this->categories($path);

        $this->report->save();

        \Cache::flush();
    }

    protected function getUrlQuery($url)
    {
        $routeService = new \App\Services\RouteParserService();

        $path = parse_url($url, PHP_URL_PATH);

        $segments = explode('/', trim($path, '/'));

        $segments = array_map('trim', $segments);

        $routeService->setSegments($segments);

        if(!$routeService->parse()) {
            throw new \Exception('Can\'t parse URL '.$url);
        }

        $query = $routeService->getQuery();

        $query['type'] = $routeService->getType();

        return $query;
    }

    protected function categories($path)
    {
        $row = 0;

        $categories = [];

        $headers = [];

        if (($handle = fopen($path, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 0, $this->delimiter)) !== FALSE) {
                $row++;

                if($row === 1) {
                    foreach ($data as $key => $value) {
                        $this->report->info('Найден заголовок '.$value);
                    }

                    continue;
                }
                
                if(empty($data[0])) {
                    continue;
                }

                $tag = empty($data[1]) ? '' : $data[1];

                $this->tag($data[0], $tag);
            }
        }

        fclose($handle);

        \File::cleanDirectory(dirname($path));

        return $categories;
    }

    protected function tag($url, $tag)
    {
        try {                    
            $query = $this->getUrlQuery($url);

            if($query['type'] !== 'category' || empty($query['id'])) {
                $this->report->fail('URL не является категорией: '.$url);
                return;
            }
        } catch (\Exception $e) {
            $this->report->fail('Не найдена категория с URL: '.$url.'. Ошибка: '.$e->getMessage());
            return;
        }

        $category = Category::find($query['id']);

        if(!$category) {
            $this->report->fail('Не найдена категория с ID: '.$query['id']);
            return;
        }

        $category->update(['tag' => $tag]);

        $this->report->success($category->id, $category->title, $category->tag);
    }

    protected function upload()
    {
        $path = storage_path('app/import/category-tag/import');

        \File::cleanDirectory($path);

        \File::exists($path) or \File::makeDirectory($path, 0755, true);

        $file = time().'.csv';

        request()->file('file')->move($path, $file);

        return $path.'/'.$file;
    }
}
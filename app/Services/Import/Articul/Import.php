<?php

namespace App\Services\Import\Articul;

use App\Models\Product;
use App\Services\Import\Report\Report;

class Import
{
    protected $delimiter = ';';
    protected $path;

    protected $report;

    public function __construct()
    {
        $this->report = Report::getInstance('articul-import');
    }

    public function run($saveEmpty)
    {
        $path = $this->upload();

        if (($handle = fopen($path, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, $this->delimiter)) !== FALSE) {
                $log = [
                    'id' => $data[0],
                    'title' => $data[1],
                    'articul' => $data[2],
                    'result' => 0,
                ];

                if(!$saveEmpty && trim($data[2]) == '') {
                    $this->report->add($log);
                    continue;
                }

                $log['result'] = $this->update($data[0], $data[2]);
                $this->report->add($log);
            }

            fclose($handle);
        }

        $this->report->save();

        \File::cleanDirectory(dirname($path));
    }

    protected function upload()
    {
        $path = storage_path('app\import\articuls\import');

        \File::cleanDirectory($path);

        \File::exists($path) or \File::makeDirectory($path, 0755, true);

        $file = time().'.csv';

        request()->file('file')->move($path, $file);

        return $path.'/'.$file;
    }

    protected function update($id, $articul)
    {
        $product = Product::find($id);

        if($product) {
            $product->update(['ext_id' => $articul]);
            return true;
        }

        return false;
    }
}
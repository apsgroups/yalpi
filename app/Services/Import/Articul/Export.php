<?php

namespace App\Services\Import\Articul;

use App\Models\Product;

class Export
{
    protected $path;

    public function run()
    {
        $products = $this->getProducts();

        $this->write($products);
    }

    protected function write($products)
    {
        $path = $this->getPath();

        $fp = fopen($path, 'w');

        fprintf($fp, chr(0xEF).chr(0xBB).chr(0xBF));

        foreach ($products as $product) {
            fputcsv($fp, [
                $product->id,
                $product->title,
                $product->ext_id,
            ], ';');
        }

        fclose($fp);
    }

    public function getPath()
    {
        if(!empty($this->path)) {
            return $this->path;
        }

        $this->path = storage_path('app/import/articuls/export/'.date('d-m-Y-H-i-s').'.csv');

        \File::cleanDirectory(dirname($this->path));

        \File::exists(dirname($this->path)) or \File::makeDirectory(dirname($this->path), 0755, true);

        return $this->path;
    }

    protected function getProducts()
    {
        return Product::all();
    }
}
<?php

namespace App\Services\Import\CategoryBreadcrumbs;

use App\Models\Category;
use App\Services\Import\Report\Report;
use App\Repositories\Backend\Category\CategoryContract;

class Import
{
    protected $delimiter = ';';

    protected $path;

    protected $report;

    protected $headers = [];

    protected $categoryContract;

    public function __construct()
    {
        $this->report = Report::getInstance('category-breadcrumbs-import');
    }

    public function run()
    {
        $path = $this->upload();

        $this->categories($path);

        $this->report->save();
    }

    protected function categories($path)
    {
        $row = 0;

        $categories = [];

        $headers = [];

        if (($handle = fopen($path, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 0, $this->delimiter)) !== FALSE) {
                $row++;

                if($row === 1) {
                    continue;
                }

                $this->updateCategory($data[0], $data[1]);
            }
        }

        fclose($handle);

        \File::cleanDirectory(dirname($path));

        return $categories;
    }

    protected function updateCategory($id, $title)
    {
        $category = Category::find($id);

        if(!$category) {
            $this->report->fail('Категория '.$id.' не найдена в БД. Заголовок: '.$title);
            return;
        }

        $category->breadcrumb_title = $title;

        $category->save();

        $this->report->success($category);
    }

    protected function upload()
    {
        $path = storage_path('app/import/category-breadcrumbs');

        \File::cleanDirectory($path);

        \File::exists($path) or \File::makeDirectory($path, 0755, true);

        $file = time().'.csv';

        request()->file('file')->move($path, $file);

        return $path.'/'.$file;
    }
}
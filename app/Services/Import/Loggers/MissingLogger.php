<?php

namespace App\Services\Import\Loggers;

use App\Models\Import\ImportMissing;
use App\Models\Import\Trash;

class MissingLogger
{
    public static function log($articul, $name, $sku = '')
    {
        $exists = Trash::where(['articul' => $articul])->exists();

        if($exists) {
            return;
        }

        $missing = ImportMissing::firstOrNew(['articul' => $articul]);
        $missing->name = $name;
        $missing->sku = $sku;
        $missing->save();
    }
}
<?php

namespace App\Services;

use App\Models\Property;
use App\Repositories\Frontend\Product\ProductContract;

class FilterService
{
    protected $category;

    protected $productRepository;

    protected $productsByCategory = null;

    public function __construct(ProductContract $products)
    {
        $this->productRepository = $products;
    }

    public function setCategory($id)
    {
        $this->category = $id;
    }

    public function getCategory()
    {
        return $this->category;
    }

    protected function getProductsByCategory()
    {
        if(!is_null($this->productsByCategory)) {
            return $this->productsByCategory;
        }

        $categoryId = $this->getCategory();
        if($categoryId) {
            $this->productRepository->setCategories($categoryId);
        }

        $this->productsByCategory = $this->productRepository->getIds();

        return $this->productsByCategory;
    }

    public function render(Property $property)
    {
        $method = 'get'.ucfirst($property->type).'Values';

        $values = null;
        if(method_exists($this, $method)) {
            $values = call_user_func([$this, $method], $property);
        }

        $view = 'frontend.filter.'.$property->type;

        if(view()->exists($view)) {
            return view($view, compact('property', 'values'))->render();
        }

        return null;
    }

    protected function getColorValues(Property $property)
    {
        return $this->getListValues($property);
    }

    protected function getListValues(Property $property)
    {
        $productValues = $property->values()->whereHas('productValues', function($query) {
            $query->whereIn('product_id', $this->getProductsByCategory());
        })->get();

        return $productValues->pluck('title', 'id');
    }

    protected function getDoubleValues(Property $property)
    {
        return [
            'min' => $property->productProperties->min('value_double'),
            'max' => $property->productProperties->max('value_double'),
        ];
    }
}
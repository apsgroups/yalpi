<?php
namespace App\Services;

use App\Models\Product;
use App\Models\Property;

class Properties
{
    protected $properties;
    protected $propertiesById;
    protected $propertiesBySlug;
    protected $values = [];

    protected $fields = [
        'list' => 'property_value_id',
        'color' => 'property_value_id',
        'double' => 'value_double',
        'string' => 'value_string',
    ];

    public function __construct(Property $model)
    {
        $this->model = $model;

        $this->indexing();
    }

    public function getField($id) {
        return $this->fields[$this->propertiesById[$id]->type];
    }

    public function all()
    {
        return $this->properties;
    }

    protected function indexing()
    {
        if(!is_null($this->properties))
            return;

        $this->properties = $this->model->with('values')->get();

        foreach($this->properties as $property) {
            $this->values = $this->values + $property->values->keyBy('id')->toArray();
            $this->propertiesById[$property->id] = $property;
            $this->propertiesBySlug[$property->slug] = $property;
        }
    }

    public function get($id)
    {
        return $this->propertiesById[$id];
    }

    public function slug($slug)
    {
        if(empty($this->propertiesBySlug[$slug])) {
            return null;
        }

        return $this->propertiesBySlug[$slug];
    }

    public function getValueTitle($id)
    {
        if(isset($this->values[$id]['title'])) {
            return $this->values[$id]['title'];
        }

        return null;
    }

    public function getValueProduct(Product $product, $slug)
    {
        $property = $this->slug($slug);

        $values = array_where($product->properties->toArray(), function ($key, $value) use($property) {
            return $value['property_id'] === $property->id;
        });

        if(empty($values)) {
            return null;
        }

        return $this->getValueTitle(collect($values)->first()['property_value_id']);
    }

    public function selectOptions($slug)
    {
        $options = ['' => 'выбрать', 'не указано'] + $this->slug($slug)->values->lists('title', 'id')->toArray();

        return $options;
    }
}
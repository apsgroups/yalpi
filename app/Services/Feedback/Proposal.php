<?php

namespace App\Services\Feedback;

class Proposal extends Base
{
    protected $toUser = false;

    public function send($attributes)
    {
    	$attributes['upload_token'] = str_random(10);
    	
    	if (request()->hasFile('files')) {
    		$this->upload($attributes, 'files');
	    }

        return parent::send($attributes);
    }
}
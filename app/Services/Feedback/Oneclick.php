<?php

namespace App\Services\Feedback;

use App\Models\ProductProperty;
use App\Services\OrderService;

class Oneclick extends Base
{
    protected $products;

    protected $toUser = false;

    public function __construct()
    {
        $this->products = app()->make('\App\Repositories\Frontend\Product\ProductContract');

        parent::__construct();
    }

    protected function save($attributes)
    {
        $order = $this->saveOrder($attributes);

        $attributes['order_id'] = $order->id;

        return parent::save($attributes);
    }

    protected function saveOrder($attributes)
    {
        $product = $this->products->findById($attributes['product_id']);

        $attributes['attribute_id'] = isset($attributes['attribute_id']) ? $attributes['attribute_id'] : null;

        $orderService = new OrderService();
        $orderService->setProduct($product, $attributes['attribute_id'], $attributes['properties'], 1);

        $orderService->setName($attributes['name']);
        $orderService->setPhone($attributes['phone']);

        $orderService->setOneclick(true);

        return $orderService->save();
    }

    public function getColors($properties)
    {
        $names = config('settings.colors');
        if(!$names || empty($properties)) {
            return [];
        }

        $colors = [];
        foreach($properties as $property) {
            if($color = $this->getColorValue($property['value'])) {
                $colors[$property['id']] = $color;
            }
        }

        if(empty($colors)) {
            return [];
        }

        $properties = [];
        foreach($names as $name) {
            $id = \Settings::get($name);
            if($id && !empty($colors[$id])) {
                $properties[$id] = $colors[$id];
            }
        }

        return $colors;
    }

    protected function getColorValue($productPropertyId)
    {
        $productProperty = ProductProperty::find($productPropertyId);

        if(!$productProperty) {
            return [];
        }

        return [
            'property_id' => $productProperty->property_id,
            'title' => $productProperty->property->single_title,
            'active' => $productPropertyId,
            'values' => [
                [
                    'id' => $productPropertyId,
                    'plaintext' => $productProperty->propertyValue->title,
                    'image' => $productProperty->propertyValue->image,
                ]
            ]
        ];
    }
}
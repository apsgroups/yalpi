<?php

namespace App\Services\Feedback\Config;

use App\Models\Setting;

class User
{
    protected $form;

    public function __construct($form)
    {
        $this->form = $form;
    }

    public function subject($attributes = [])
    {
        $primary = static::getConfigKey($this->form, 'subject');

        $subject = \Settings::getOr($primary, 'subject', config('app.name'));

        foreach($attributes as $key => $value) {
            if(is_array($value)) {
                continue;
            }

            $subject = str_replace(':'.$key, $value, $subject);
        }

        foreach(Setting::all() as $param) {
            $subject = str_replace(':s.'.$param->title, $param->content, $subject);
        }

        return $subject;
    }

    public static function getConfigKey($form, $name)
    {
        return 'feedback_'.$form.'_user_'.$name;
    }
}
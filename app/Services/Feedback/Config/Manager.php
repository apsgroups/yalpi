<?php

namespace App\Services\Feedback\Config;

class Manager extends User
{
    public function emails()
    {
        $primary = self::getConfigKey($this->form, 'email');

        $emails = \Settings::getOr($primary, 'email', config('mail.from.address'));

        $emails = explode(',', $emails);

        return array_map('trim', $emails);
    }

    public static function getConfigKey($form, $name)
    {
        return 'feedback_'.$form.'_manager_'.$name;
    }
}
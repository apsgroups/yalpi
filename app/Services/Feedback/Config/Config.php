<?php

namespace App\Services\Feedback\Config;

class Config
{
    protected $form;

    public function __construct($form)
    {
        $this->from = new From($form);
        $this->user = new User($form);
        $this->manager = new Manager($form);
    }
}
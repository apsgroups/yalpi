<?php

namespace App\Services\Feedback\Config;

class From
{
    protected $form;

    public function __construct($form)
    {
        $this->form = $form;
    }

    public function email()
    {
        $primary = self::getConfigKey($this->form, 'email');

        return \Settings::getOr($primary, 'email', config('mail.from.address'));
    }

    public function name()
    {
        $primary = self::getConfigKey($this->form, 'name');

        return \Settings::getOr($primary, 'title', config('mail.from.name'));
    }

    public static function getConfigKey($form, $name)
    {
        return 'feedback_'.$form.'_from_'.$name;
    }
}
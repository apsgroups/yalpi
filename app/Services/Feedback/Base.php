<?php
namespace App\Services\Feedback;

use App\Models\Feedback;
use App\Services\Feedback\Config\Config;
use Illuminate\Support\Facades\Mail;
use App\Models\Multiupload;
use App\Services\Image\ImageService;

abstract class Base
{
    protected $model;

    protected $name;

    protected $config;

    protected $toUser = true;

    protected $toManager = true;

    public function __construct()
    {
        $this->name = strtolower(class_basename($this));

        $this->config = new Config($this->name);
    }

    public function getToUser()
    {
        return $this->toUser;
    }

    public function getToManager()
    {
        return $this->toManager;
    }

    public function getName()
    {
        return $this->name;
    }

    protected function setModel($model)
    {
        $this->model = $model;
    }

    protected function getModel()
    {
        return $this->model;
    }

    public function send($attributes)
    {
        $feedback = $this->save($attributes);

        if($this->getToUser()) {
            $this->sendToUser($feedback);
        }

        if($this->getToManager()) {
            $this->sendToManager($feedback);
        }

        return $feedback;
    }

    protected function save($attributes)
    {
        $feedback = Feedback::create($attributes);

        $feedback->user_id = auth()->id();
        $feedback->feedback = $this->name;
        $feedback->save();

        $this->setModel($feedback);

        if(!empty($attributes['upload_token'])) {
            Multiupload::where('upload_token', $attributes['upload_token'])
                ->update([
                    'upload_token' => '',
                    'multiuploadable_id' => $feedback->id,
                    'multiuploadable_type' => get_class($feedback),
                ]);
        }

        return $feedback;
    }

    protected function sendToManager($feedback)
    {
        $emails = $this->config->manager->emails();

        foreach($emails as $email) {
            $subject = $this->config->manager->subject($feedback->toArray());

            $this->email('manager', $email, $subject, compact('feedback'));
        }
    }

    protected function sendToUser($feedback)
    {
        if(trim($feedback->email) === '') {
            return;
        }
        
        $subject = $this->config->user->subject($feedback->toArray());

        $this->email('user', $feedback->email, $subject, compact('feedback'));
    }

    protected function email($layout, $email, $subject, $params = [])
    {
        Mail::send($this->layout($layout), $params, function ($message) use($email, $subject) {
            $from = $this->config->from;

            $message->from($from->email(), $from->name());

            $message->to($email)
                ->subject($subject);

            $feedback = $this->getModel();

            if(is_null($feedback->multiupload)) {
                return;
            }

            foreach($feedback->multiupload->images as $image) {
                $original = \App\Services\Image\ImageService::getFullpath($image);
                $message->attach(public_path($original), ['as' => $image->original_name]);
            }
        });
    }

    protected function layout($to)
    {
        return 'emails.'.$this->name.'.'.$to;
    }

    public function upload($attributes, $name = 'qqfile')
    {
        $multiupload = Multiupload::firstOrCreate(['title' => $this->name, 'upload_token' => $attributes['upload_token']]);

        $image = (new ImageService($name, $multiupload))->upload();

        return $image;
    }
}
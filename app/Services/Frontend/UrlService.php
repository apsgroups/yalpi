<?php

namespace App\Services\Frontend;

class UrlService
{
    protected static $cleanedVars = null;

    protected $query = [];

    public function __construct(array $query = [])
    {
        $this->query = $query;
    }

    public static function buildQuery(array $query = [], $scope = null)
    {
        if(is_null(self::$cleanedVars)) {
            $urlService = new static(request()->all());
            $urlService->cleanParam('id');
            $urlService->cleanParam('menu_item_id');
            $urlService->cleanParam('page');

            if($scope != 'sorting') {
                $urlService->cleanSortingParams();
            }

            if(true) {
                $urlService->cleanLayoutParams();
            }

            self::$cleanedVars = $urlService->getQuery();
        }

        return array_merge(self::$cleanedVars, $query);
    }

    public static function url(array $query = [])
    {
        $query = self::buildQuery($query);
        return url(request()->fullUrl(), $query);
    }

    public static function currentUrl(array $query = [])
    {
        $query = self::buildQuery($query);
        return url(request()->fullUrl(), $query);
    }

    public function getQuery()
    {
        return $this->query;
    }

    public function cleanSortingParams()
    {
        $sortingService = new SortingService();

        if(isset($this->query[$sortingService->getKeyAttribute()])) {
            $this->query[$sortingService->getKeyAttribute()] = null;
        }

        if(isset($this->query[$sortingService->getKeyDirection()])) {
            $this->query[$sortingService->getKeyDirection()] = null;
        }
    }

    public function cleanLayoutParams()
    {
        $layoutService = new LayoutService();

        if(isset($this->query[$layoutService->getKeyName()])) {
            $this->query[$layoutService->getKeyName()] = null;
        }
    }

    public function cleanParam($name)
    {
        if(isset($this->query)) {
            $this->query[$name] = null;
        }
    }
}
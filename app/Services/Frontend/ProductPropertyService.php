<?php

namespace App\Services\Frontend;

use App\Models\Product;
use App\Models\ProductProperty;

class ProductPropertyService
{
    protected $product;

    protected $cacheTime;

    public function __construct()
    {
        $this->cacheTime = config('cache.time');
    }

    public function getProduct()
    {
        return $this->product;
    }

    public function setProduct(Product $product)
    {
        $this->product = $product;
    }

    public function getPropertyValue()
    {
        $product = $this->getProduct();
        
        $properties = $product->properties;

        $productProperties = [];

        foreach($properties as $value) {
            $property = [];

            $property['id'] = $value->id;
            $property['plaintext'] = $this->getPlainText($value);
            $property['image'] = $this->getImage($value);

            $productProperties[$value->property_id][$value->id] = $property;
        }

        return $productProperties;
    }

    protected function getPropertyType(ProductProperty $value)
    {
        return $value->property->type;
    }

    protected function getPlainText(ProductProperty $value)
    {
        $propertyType = $this->getPropertyType($value);

        if($this->isList($propertyType)) {
            return $this->getListValue($value);
        }

        return $value->getAttribute('value_'.$propertyType);
    }

    protected function getListValue(ProductProperty $value)
    {
        return $value->propertyValue->title;
    }

    protected function isList($type)
    {
        if(in_array($type, ['color', 'list'])) {
            return true;
        }

        return false;
    }

    protected function getImage(ProductProperty $value)
    {
        $propertyType = $this->getPropertyType($value);

        if(!$this->isList($propertyType)) {
            return null;
        }

        $cacheKey = 'product.property.image.'.$value->property_value_id;

        return $value->propertyValue->image;
    }
}
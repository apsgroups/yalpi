<?php

namespace App\Services\Frontend;

class LayoutService
{
    protected $all;
    protected $layout;
    protected $default;
    protected $keyName;

    public function __construct()
    {
        $this->all = config('site.layouts.all');
        $this->default = config('site.layouts.default');
        $this->keyName = config('site.layouts.key');
    }

    public function getAll()
    {
        return $this->all;
    }

    public function getKeyName()
    {
        return $this->keyName;
    }

    public function getDefault()
    {
        return $this->default;
    }

    public function isActive($layout)
    {
        return $layout === $this->getLayout();
    }

    public function getLayout()
    {
        if(empty($this->layout)) {
            $this->layout = $this->default;
        }

        return $this->layout;
    }

    public function setLayout($layout)
    {
        $this->layout = $layout;
    }

    public function getQuery($layout)
    {
        $query = [];

        if($layout !== $this->getDefault()) {
            $query[$this->getKeyName()] = $layout;
        }

        return $query;
    }
}
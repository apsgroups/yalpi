<?php

namespace App\Services\Frontend;

use App\Models\Product;
use App\Models\Property;
use App\Models\PropertyValue;
use Illuminate\Support\Facades\Cache;

class DimensionService
{
    protected $dimension;
    protected $config;
    protected $product;

    public function getDimension()
    {
        if(is_null($this->dimension)) {
            $name = config('properties.dimensions.name');
            $property = Property::slug($name)->first();

            if(!$property) {
                return null;
            }

            $product = $this->getProduct();

            $value = $product->properties->where('property_id', $property->id)->first();

            if(!$value) {
                // для кухонь без установленных габаритов устанавливаем по расположению
                if($product->product_type_id === config('properties.dimensions.kitchen.product_type_id')) {
                    $type = $this->getProduct()->properties->where('property_id', config('properties.dimensions.kitchen.placement.property_id'))->first();

                    if($type) {
                        $dimensionId = null;

                        if($type->property_value_id == config('properties.dimensions.kitchen.placement.corner')) {
                            $dimensionId = 'corner';
                        }

                        if($type->property_value_id == config('properties.dimensions.kitchen.placement.straight')) {
                            $dimensionId = 'straight';
                        }

                        if($dimensionId) {
                            $value = PropertyValue::find(config('properties.dimensions.kitchen.dimension.'.$dimensionId));
                            $this->setDimension($value);
                            return $this->dimension;
                        }
                    }
                }

                return null;
            }

            $this->setDimension($value->propertyValues);
        }

        return $this->dimension;
    }

    protected function setDimension($dimension)
    {
        $this->dimension = $dimension;
    }

    public function getWidth()
    {
        return config('properties.dimensions.properties.'.$this->getDimension()->slug.'.width');
    }

    public function getImage()
    {

        return $this->getDimension()->image;
    }

    public function getDimensions(Product $product)
    {
        $properties = $this->getProperties();

        if(empty($properties)) {
            return [];
        }

        $dimensions = [];
        foreach($properties as $name => $position) {
            $value = $product->propertyBySlug($name);

            if(empty($value)) {
                continue;
            }

            $dimensions[$name] = [
                'name' => $name,
                'title' => Property::slug($name)->first()->title,
                'top' => $position['top'],
                'left' => $position['left'],
                'value' => $value,
            ];
        }

        return $dimensions;
    }

    protected function getProperties()
    {
        return config('properties.dimensions.properties.'.$this->getDimension()->slug.'.elements');
    }

    public function getProduct()
    {
        return $this->product;
    }

    public function setProduct(Product $product)
    {
        $this->product = $product;
    }

    public function getText($tmpl = 'default', $callback = null)
    {
        $product = $this->getProduct();

        $template = config('properties.dimensions.templates.'.$tmpl);
        $separators = config('properties.dimensions.separators');

        $text = [];

        foreach($template as $sizes) {
            $sizeText = [];

            foreach($sizes as $size) {
                $value = $product->propertyBySlug($size);

                if($value) {
                    $sizeText[] = $callback instanceof \Closure ? $callback($value) : $value;
                }
            }

            $dimension = implode($separators['size'], $sizeText);

            if($dimension) {
                $text[] = $dimension;
            }
        }

        return implode($separators['dimension'], $text);
    }

    public function getInSm()
    {
        $text = $this->getText('sm', function($value) {
            $value = round($value / 10, 1);
            return number_format($value, 0, '', '');
        });

        return $text.($text ? ' см' : '');
    }

    public function getPreviewDimensions()
    {
        $product = $this->getProduct();

        if($product->product_type_id === 1) {
            return $this->getText('width_meters', function($value) {
                $value = round($value / 1000, 1);
                return number_format($value, 1, ',', '') . 'м';
            });
        }

        if(in_array($product->product_type_id, config('properties.dimensions.template_sm_types'))) {
            return $this->getText('width_sm', function($value) {
                $value = round($value / 10, 1);
                return number_format($value, 0, '', '') . ' см';
            });
        }

        return $this->getText(). ' мм';
    }
}
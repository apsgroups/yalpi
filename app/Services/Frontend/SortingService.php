<?php
namespace App\Services\Frontend;

class SortingService
{
    protected $directions;

    protected $defaultAttribute;

    protected $attribute;

    protected $direction;

    protected $keyAttribute;

    protected $keyDirection;

    protected $aliases;

    protected $reverse;

    public function __construct()
    {
        $this->directions = config('site.sorting.directions');
        $this->defaultAttribute = config('site.sorting.default');
        $this->keyAttribute = config('site.sorting.keys.attribute');
        $this->keyDirection = config('site.sorting.keys.direction');
        $this->aliases = config('site.sorting.aliases');
        $this->reverse = config('site.sorting.reverse');

        $sortAttribute = request()->input($this->getKeyAttribute());
        $sortDirection = request()->input($this->getKeyDirection());

        $this->setAttribute($sortAttribute);
        $this->setDirection($sortDirection);
    }

    public function getNames()
    {
        return array_keys($this->directions);
    }

    public function getAliases($scope)
    {
        return $this->aliases[$scope];
    }

    /**
     * @return mixed
     */
    public function getKeyDirection()
    {
        return $this->keyDirection;
    }

    /**
     * @param mixed $direction
     */
    public function setDirection($direction)
    {
        $this->direction = $direction;
    }

    /**
     * @param mixed $attribute
     */
    public function setAttribute($attribute)
    {
        $this->attribute = $attribute;
    }

    /**
     * @return mixed
     */
    public function getKeyAttribute()
    {
        return $this->keyAttribute;
    }

    public function getDefaultDirection($attribute)
    {
        return $this->directions[$attribute];
    }

    public function getDefaultAttribute()
    {
        return $this->defaultAttribute;
    }

    public function getAttribute()
    {
        if(empty($this->attribute)) {
            $this->attribute = $this->getDefaultAttribute();
        }

        return $this->attribute;
    }

    public function getDirection()
    {
        if(empty($this->direction)) {
            $attribute = $this->getAttribute();
            $dir = $this->getDefaultDirection($attribute);
            $this->direction = $dir;
        }

        return $this->direction;
    }

    public function getIconDirection($attribute)
    {
        if($attribute === $this->defaultAttribute) {
            return '<i class="uk-icon-child"></i> ';
        }

        $icon = $this->getDefaultDirection($attribute);

        if($this->isActive($attribute)) {
            $icon .= ' active';
        }

        return '<span class="sort-icon-holder"><i class="sort-icon '.$icon.'"></i></span> ';
    }

    public function getQuery($forAttribute) {
        if($forAttribute === $this->defaultAttribute) {
            return [];
        }

        $query = [];

        $defaultAttribute = $this->getDefaultAttribute();
        $defaultDir = $this->getDefaultDirection($forAttribute);

        if(isset($this->reverse[$forAttribute]) && !$this->reverse[$forAttribute]) {
            return [$this->getKeyAttribute() => $forAttribute];
        }

        $direction = $this->getDirection();
        $attribute = $this->getAttribute();

        if($defaultAttribute === $forAttribute && $direction !== $defaultDir) {
            return $query;
        }

        $query[$this->getKeyAttribute()] = $forAttribute;

        if($attribute === $forAttribute && $direction === $defaultDir) {
            $query[$this->getKeyDirection()] = ($defaultDir === 'asc') ? 'desc' : 'asc';
        }

        return $query;
    }

    public function isActive($attribute)
    {
        return ($this->getAttribute() === $attribute);
    }

    public function convertAttributeToField($attribute = null, $scope = 'products')
    {
        if(is_null($attribute)) {
            $attribute = $this->getAttribute();
        }

        $aliases = $this->getAliases($scope);

        return (isset($aliases[$attribute]) ? $aliases[$attribute] : null);
    }
}
<?php
namespace App\Services;

use App\Models\MenuItem;

class MenuService
{
    protected $parentList;

    public function getParentList($menuId)
    {
        if(is_null($this->parentList)) {
            $list = MenuItem::distinct()
                ->select('parent_id')
                ->where('parent_id', '>', 0)
                ->where('menu_id', $menuId)
                ->get()
                ->toArray();

            $this->setParentList(array_flatten($list));
        }

        return $this->parentList;
    }

    public function setParentList($list)
    {
        $this->parentList = $list;
    }

    public function getRoot($id)
    {
        return MenuItem::whereNull('parent_id')
            ->where('menu_id', $id)
            ->orderBy('ordering', 'asc')
            ->get();
    }

    public function getParentsForSelect($menuId, $items = null, $depth = 0)
    {
        if (is_null($items)) {
            $items = $this->getRoot($menuId);
        }

        $list = ['' => 'Выбрать'];

        $parentList = $this->getParentList($menuId);

        foreach ($items as $item) {
            $list[$item->id] = str_repeat('&mdash;', $depth) . ' ' . $item->title;
            if (in_array($item->id, $parentList) && $item->has('children')) {
                $list += $this->getParentsForSelect($menuId, $item->children, $depth + 1);
            }
        }

        return $list;
    }

    public function sortTree($items, $parent = null)
    {
        foreach ($items as $depth => $item) {
            $menuItem = MenuItem::findOrFail($item['id']);
            $menuItem->parent_id = $parent;
            $menuItem->ordering = $depth;
            $menuItem->save();

            if (isset($item['children']))
                $this->sortTree($item['children'], $item['id']);
        }
    }

    public function getBreadcrumbPath($itemId, $type)
    {
        $item = MenuItem::where(['item_id' => $itemId, 'type' => $type])->published()->first();

        if(empty($item)) {
            return null;
        }

        $crumbs[] = ['title' => $item->getBreadcrumbTitle(), 'url' => menu_url($item->id)];

        $skip = config('menu.breadcrumbs', []);

        while($item) {
            $item = $item->parent()->published()->first();

            if($item && in_array($item->type, $skip)) {
                $crumbs[] = ['title' => $item->getBreadcrumbTitle(), 'url' => menu_url($item->id)];
            }
        }

        return array_reverse($crumbs);
    }

    public function destroy($id)
    {
        $menuItem = MenuItem::findOrFail($id);

        if ($menuItem->children->count()) {
            throw new \LogicException('Удалите дочерние пункты!');
        }

        $menuItem->delete();
    }
}
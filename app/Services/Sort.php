<?php
namespace App\Services;

use Illuminate\Support\Facades\Input;

class Sort
{
    public function query($attribute='id', $dir='desc') {
        $currentAttribute = Input::get('sort');
        $currentDir = Input::get('dir', 'desc');

        if($attribute === $currentAttribute && $currentDir === 'desc') {
            $dir = 'asc';
        }

        return [
            'sort' => $attribute,
            'dir' => $dir,
            'filter' => Input::get('filter'),
        ];
    }

    public function isActive($attribute)
    {
        $current = Input::get('sort', 'id');

        return $current === $attribute;
    }

    public function route($route, $attribute, $title, $query=[])
    {
        $currentDir = Input::get('dir', 'desc');

        $icon = '<i style="font-size: 12px" class="fa fa-sort-amount-'.(($currentDir === 'desc') ? 'desc' : 'asc').'"></i>';

        $html = '';

        $params = [];
        if($this->isActive($attribute)) {
            $html .= $icon;
            $params = ['class' => 'active'];
        }

        $query += $this->query($attribute);

        $stringAttr = [];
        foreach($params as $k => $v) {
            $stringAttr[] = $k.'="'.$v.'"';
        }

        $html .= '<a href="'.route($route, $query).'" '.implode(' ', $stringAttr).'>'.$title.'</a>';

        return $html;
    }
}
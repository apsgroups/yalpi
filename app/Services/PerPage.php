<?php

namespace App\Services;

class PerPage
{
    public function render()
    {
        $options = \Settings::get('per-page');
        view()->make('includes.per-page', compact('options'))->render();
    }

    public function getPerPage()
    {

    }

}
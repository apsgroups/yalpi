<?php

namespace App\Services;

/**
 * Class Translit
 * @desc Trancliterate string by rules http://translit-online.ru/yandex.html
 * @package App\Services
 */
class Translit
{
    private static $primary = [
        'а' => 'a',
        'б' => 'b',
        'в' => 'v',
        'г' => 'g',
        'д' => 'd',
        'е' => 'e',
        'ё' => 'yo',
        'ж' => 'zh',
        'з' => 'z',
        'и' => 'i',
        'й' => 'j',
        'к' => 'k',
        'л' => 'l',
        'м' => 'm',
        'н' => 'n',
        'о' => 'o',
        'п' => 'p',
        'р' => 'r',
        'с' => 's',
        'т' => 't',
        'у' => 'u',
        'ф' => 'f',
        'ц' => 'c',
        'ч' => 'ch',
        'ш' => 'sh',
        'щ' => 'shch',
        'ъ' => '',
        'ы' => 'y',
        'ь' => '',
        'э' => 'eh',
        'ю' => 'yu',
        'я' => 'ya',
    ];

    // kh после букв c,s,e,h ; в остальных случаях - h
    private static $convert = [
        'cх' => 'ckh',
        'sх' => 'skh',
        'eх' => 'ekh',
        'hх' => 'hkh',
    ];

    public static function convert($string)
    {
        $string = str_replace(array_keys(self::$primary), self::$primary, $string);
        $string = str_replace(array_keys(self::$convert), self::$convert, $string);
        $string = str_replace('х', 'h', $string);

        return $string;
    }
}
<?php
namespace App\Services;

use App\Models\Product;
use App\Models\ProductProperty;
use App\Models\Property;

class PropertyService
{
    public function render(Product $product, Property $property)
    {
        $cacheKey = 'product.'.$product->id.'.property.'.$property->id;

        return \Cache::remember($cacheKey, 10, function() use($product, $property) {
            $productProperty = $product->properties;
            $productProperty = $productProperty->where('property_id', $property->id)->first();

            if(!$productProperty) {
                return null;
            }

            $method = 'getPlain'.ucfirst($property->type);
            if(method_exists($this, $method)) {
                return call_user_func([$this, $method], $productProperty);
            }

            return null;
        });
    }

    protected function getPlainDouble(ProductProperty $productProperty)
    {
        return $productProperty->value_double;
    }

    protected function getPlainString(ProductProperty $productProperty)
    {
        return $productProperty->value_string;
    }

    protected function getPlainColor(ProductProperty $productProperty)
    {
        return $this->getPlainList($productProperty);
    }

    protected function getPlainList(ProductProperty $productProperty)
    {
        return @$productProperty->propertyValues()->first()->title;
    }
}
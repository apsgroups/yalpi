<?php

namespace App\Console\Commands;

use App\Models\Category;
use App\Models\MenuItem;
use App\Models\Product;
use Illuminate\Console\Command;

class SeoCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'seo:fix';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
//        $this->setTitles();
//        $this->replaceLowLine();
//        $this->removeDuplicateCategories();
        //[4, 34, 361, 390]


        $this->switchOnSitemapItems(Category::find(4));
        $this->switchOnSitemapItems(Category::find(34));
        $this->switchOnSitemapItems(Category::find(390));
        $this->switchOnSitemapItems(Category::find(361));
    }

    protected function replaceLowLine()
    {
        $categories = Category::query()
            ->where('title', 'like', '%\_%')
            ->orWhere('alter_title', 'like', '%\_%')
            //->orWhere()
            ->get(['id', 'title'])
            ->all();

        /** @var Category $category */
        foreach ($categories as $category) {
            echo $category->title . PHP_EOL;
            echo $category->alter_title . PHP_EOL;
//            $category->title = str_replace('_', ' ', $category->title);
//            $category->update();
        }
    }

    protected function setTitles()
    {
        /** @var Category $category */
        $category = Category::query()->findOrFail(34);
        $this->setH1($category, "Онлайн обучение «{title}»", [36, 161, 177]);

        $category = Category::query()->findOrFail(36);
        $this->setH1($category, "Обучение разработке веб-сайтов «{title}»", []);

        $category = Category::query()->findOrFail(267);
        $this->setH1($category, "Онлайн обучение программированию на {title}", []);
        $category = Category::query()->findOrFail(269);
        $this->setH1($category, "Онлайн обучение программированию на {title}", []);

        $category = Category::query()->findOrFail(162);
        $this->setH1($category, "Онлайн обучение {title} программированию", []);

        $category = Category::query()->findOrFail(177);
        $this->setH1($category, "Онлайн обучение разработке {title|lower}", [298, 304, 179, 180, 181, 243, 261, 302, 312, 265, 300, 318]);

        $category = Category::query()->findOrFail(178);
        $this->setH1($category, "Онлайн обучение разработке баз данных {title}", []);

        $category = Category::query()->findOrFail(304);
        $this->setH1($category, "Онлайн обучение разработке приложений на платформе {title}", []);

        $category = Category::query()->findOrFail(300);
        $this->setH1($category, "Онлайн обучение разработке игр на платформе {title}", []);

        $category = Category::query()->findOrFail(318);
        $this->setH1($category, "Онлайн обучение разработке игр на языке {title}", []);

        $category = Category::query()->findOrFail(261);
        $this->setH1($category, "Онлайн обучение разработке сайтов на CMS {title}", []);

        $category = Category::query()->findOrFail(302);
        $this->setH1($category, "Онлайн обучение разработке сайтов на платформе {title}", []);

        $category = Category::query()->findOrFail(312);
        $this->setH1($category, "Онлайн обучение разработке сайтов на языке {title}", []);
    }

    protected function setH1(Category $category, $template, $skipCategories = [])
    {
        if (in_array($category->id, $skipCategories)) return;

        if (!$category->alter_title) {
            $masks = [
                '{title}' => trim($category->title),
                '{title|lower}' => mb_strtolower(trim($category->title)),
            ];

            $alterTitle = $template;
            foreach ($masks as $mask => $replace) {
                $alterTitle = str_replace("{$mask}", $replace, $alterTitle);
            }

            $category->alter_title = $alterTitle;
            $category->update();
        }

        echo "id: {$category->id} - title: {$category->title} - alter_title: {$category->alter_title} \n";

        foreach ($category->children as $child) {
            $this->setH1($child, $template, $skipCategories = []);
        }
    }

    protected function removeDuplicateCategories()
    {
        $handle = fopen(storage_path('app/duplicate-categories.csv'), "r");

        while ($parsedString = fgetcsv($handle, 1000, ",")) {
            $fromCategory = (int)$parsedString[0];
            $toCategory = (int)$parsedString[1];
            $additionalCategories = array_map(function ($categoryId) {
                return (int)$categoryId;
            }, explode(',', $parsedString[2]));

            echo $fromCategory . PHP_EOL;

            $products = Product::where('category_id', '=', $fromCategory)
                ->orWhereHas('categories', function ($query) use ($fromCategory) {
                    $query->where('category_id', '=', $fromCategory);
                })
                ->get()
                ->all();

            /** @var Product $product */
            foreach ($products as $product) {
                echo  $product->id . PHP_EOL;
                echo  $product->category_id . PHP_EOL;
                echo route('admin.product.edit', ['id' => $product->id]) . PHP_EOL;
                $product->category_id = $toCategory;
                $product->categories()->sync(array_merge($additionalCategories, [$toCategory]));
                $product->save();
            }

            MenuItem::query()
                ->where('item_id', '=', $fromCategory)
                ->delete();

            Category::query()->findOrFail($fromCategory)->delete();

        }
    }

    protected function switchOnSitemapItems(Category $category)
    {
        $category->sitemap = true;
        $category->sitemap_items = true;
        $category->update();

        foreach ($category->children as $child) {
            $this->switchOnSitemapItems($child);
        }
    }
}

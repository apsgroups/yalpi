<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class Video extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'video:preview';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Video previews';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $videos = \App\Models\Product::where('generate_video_preview', 1)->take(1)->get();

        if($videos->isEmpty()) {
            $this->info('Empty list');
            return;
        }

        foreach ($videos as $video) {
            $this->info('Run '.$video->id);

	        if(/*$video->image || */!$video->video) {
                $this->info('has img');

	        	$video->generate_video_preview = 0;
	        	$video->save();
	        	continue;
	        }

            try {
		        $videoPath = $this->upload($video->video);

		        if(!$videoPath || !file_exists($videoPath)) {
		        	$video->generate_video_preview = 2;
		        	$video->save();
                    $this->info('!videoPath');
		        	continue;
		        }

        		$preview = storage_path().'/videos/'.\Uuid::generate().'.jpg';

                $cmd = 'ffmpeg -i '.$videoPath.' -ss 00:00:45.000 -vframes 1 '.$preview;

                shell_exec($cmd);

                @unlink($videoPath);

                if(!file_exists($preview)) {
		        	$video->generate_video_preview = 2;
		        	$video->save();
		        	continue;
                }

                $image = new \App\Services\Image\ImageService('image_id', $video);
                
                $video->image_id = $image->saveByUrl($preview)->id;

                $video->image->save();
                
                $video->generate_video_preview = 0;
                $video->save();
                $this->info($video->link());
            } catch(\Exception $e) {
                $this->info($e->getMessage());
                $video->generate_video_preview = 3;
                $video->save();
            }
        }
    }

    protected function upload($video)
    {
		include_once app_path().'/YouTubeDownloader.class.php'; 
		$handler = new \YouTubeDownloader(); 
		 
		// Youtube video url 
		$youtubeURL = 'https://www.youtube.com/watch?v='.$video;

		$downloader = $handler->getDownloader($youtubeURL);
		$downloader->setUrl($youtubeURL); 

	    if(!$downloader->hasVideo()){
	    	return false;
	    }

	    $videoDownloadLink = $downloader->getVideoDownloadLink();

	    $downloadURL = $videoDownloadLink[0]['url'];

        if(empty($downloadURL)){
            return false;
        }

        $file = storage_path().'/videos/'.\Uuid::generate().'.mp4';

        \File::exists(dirname($file)) or \File::makeDirectory(dirname($file), 0755, true);

        file_put_contents($file, file_get_contents($downloadURL));

        return $file;
    }
}

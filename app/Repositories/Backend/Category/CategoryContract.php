<?php

namespace App\Repositories\Backend\Category;

use App\Models\Category;
use Illuminate\Http\Request;

interface CategoryContract
{
    public function getCategory();

    public function tree();

    public function getParentsForSelect();

    public function getVendors();

    public function findById($id);

    public function destroy($id);

    public function publish($id, $state);

    public function create($attributes);

    public function autoSale(Category $category);

    public function update($id, $attributes);
}
<?php

namespace App\Repositories\Backend\Category;

use App\Exceptions\GeneralException;
use App\Models\Category;
use App\Models\Popular;
use App\Repositories\Backend\Product\ProductContract;
use App\Services\Image\ImageDestroyer;
use App\Services\Image\ImageService;
use Illuminate\Support\Facades\Auth;

class EloquentCategoryRepository implements CategoryContract
{
    protected $categories;

    protected $parentList;

    protected $productContract;

    public function __construct(Category $categories, ProductContract $productContract)
    {
        $this->categories = $categories;
        $this->productContract = $productContract;
    }

    public function getCategory()
    {
        return $this->categories;
    }

    public function tree()
    {
        return $this->categories->root()->get();
    }

    public function findById($id)
    {
        return $this->categories->find($id);
    }

    public function getParentList()
    {
        if(is_null($this->parentList)) {
            $list = $this->categories
                ->distinct()
                ->select('parent_id')
                ->whereNotNull('parent_id')
                ->get()
                ->toArray();

            $this->setParentList(array_flatten($list));
        }

        return $this->parentList;
    }

    public function setParentList($list)
    {
        $this->parentList = $list;
    }

    public function getParentsForSelect($categories = null, $depth = 0)
    {
        if (is_null($categories))
            $categories = $this->categories->root()->get();

        $list = ['' => 'Выбрать'];

        $parentList = $this->getParentList();

        foreach ($categories as $category) {
            $list[$category->id] = str_repeat('&mdash;', $depth) . ' ' . $category->title.' - М ' . $category->model. ', id '.$category->id;
            if (in_array($category->id, $parentList) && $category->has('children')) {
                $list += $this->getParentsForSelect($category->children, $depth + 1);
            }
        }

        return $list;
    }

    public function sortTree($items, $parent = null)
    {
        foreach ($items as $depth => $item) {
            $category = $this->findById($item['id']);
            $category->parent_id = $parent;
            $category->ordering = $depth;
            $category->save();

            if (isset($item['children']))
                $this->sortTree($item['children'], $item['id']);
        }
    }

    public function destroy($id)
    {
        $category = $this->findById($id);

        if ($category->children->count()) {
            throw new GeneralException('Удалите дочерние категории!');
        }

        $primaryProductsCount = $category->primaryProducts->count();
        if ($primaryProductsCount) {
            throw new GeneralException('У категории есть '.$primaryProductsCount.' товар/ы! Удалите или открепите их');
        }

        $menuItemsCount = $category->menuItem->count();
        if ($menuItemsCount) {
            throw new GeneralException('У категории есть '.$menuItemsCount.' пункт/ы меню! Удалите для начала их');
        }

        $category = $this->findById($id);
        $category->image_id = null;
        $category->mobile_image_id = null;
        $category->save();

        foreach($category->images as $image) {
            try {
                ImageDestroyer::destroy($image->id);
            } catch(\InvalidArgumentException $e) {

            }
        }

        $category->delete();
    }

    public function publish($id, $published)
    {
        $category = $this->findById($id);
        $category->published = $published;
        $category->save();
    }

    public function create($attributes)
    {
        $attributes['user_id'] = Auth::id();

        $attributes = $this->beforeSave($attributes);

        $category = $this->categories->create($attributes);

        $this->afterSave($category, $attributes);

        return $category;
    }

    public function update($id, $attributes)
    {
        $category = $this->findById($id);

        $attributes = $this->beforeSave($attributes);

        if(isset($attributes['autosale']) && (int)$category->autosale !== (int)$attributes['autosale']) {
            $this->autoSale($category);
        }

        $category->update($attributes);

        $this->afterSave($category, $attributes);

        return $category;
    }

    protected function beforeSave(array $attributes)
    {
        $attributes['image_id'] = empty($attributes['image_id']) ? null : $attributes['image_id'];
        $attributes['image_id'] = empty($attributes['image_id']) ? null : $attributes['image_id'];
        $attributes['alt_image_id'] = empty($attributes['alt_image_id']) ? null : $attributes['alt_image_id'];
        $attributes['search_image_id'] = empty($attributes['search_image_id']) ? null : $attributes['search_image_id'];
        $attributes['mobile_image_id'] = empty($attributes['mobile_image_id']) ? null : $attributes['mobile_image_id'];
        $attributes['parent_id'] = empty($attributes['parent_id']) ? null : $attributes['parent_id'];
        $attributes['populars'] = empty($attributes['populars']) ? [] : $attributes['populars'];
        $attributes['tags'] = empty($attributes['tags']) ? [] : $attributes['tags'];
        $attributes['filters_activate'] = empty($attributes['filters_activate']) ? [] : $attributes['filters_activate'];
        $attributes['filters_deactivate'] = empty($attributes['filters_deactivate']) ? [] : $attributes['filters_deactivate'];
        
        if(!empty($attributes['automorph'])) {
            try {                    
                $attributes['morph'] = $this->morph($attributes['title']);
                $attributes['automorph'] = 0;
            } catch (\Exception $e) { }
        }

        return $attributes;
    }

    protected function morph($title)
    {
        $url = 'https://ws3.morpher.ru/russian/declension';

        $params['s'] = $title;

        $ch = curl_init();
        if ($params !== NULL && !empty($params)){
            $url .= '?';
            foreach($params as $key => $value) {
                $url .= $key . '=' . curl_escape($ch, $value) . '&';
            }
            $url = rtrim($url, '&');
        }

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch,
                    CURLOPT_HTTPHEADER,
                    array('Accept: application/json',
                          'Authorization: Basic '.base64_encode(config('site.morpher'))));
        $result = curl_exec($ch);
        if ($result === false) { $result = curl_error($ch); }
        $json = json_decode($result, true);
        curl_close($ch);

        $morph = [
            'R' => $json['Р'],
            'V' => $json['В'],
        ];

        return $morph;
    }

    protected function afterSave($category, array $attributes)
    {
        $this->savePopulars($category, $attributes['populars']);
        $this->saveImage($category);
        $this->saveAltImage($category);
        $this->saveAltImage($category, 'mobile');
        $this->saveAltImage($category, 'search');
        $this->saveDocs($category, $attributes['docs']);
        $this->saveTags($category, $attributes['tags']);
    }

    protected function saveTags(Category $category, $tags)
    {
        $items = [];

        foreach ($tags as $key => $value) {
            $items[$value] = ['ordering' => $key];
        }
        
        $category->tags()->sync($items);
    }

    public function autoSale(Category $category)
    {
        set_time_limit(210);

        if(!$category->primaryProducts()->count()) {
            return;
        }

        $useStock = \Settings::get('virtual_discount_stock') === '1';

        $category->primaryProducts()->chunk(50, function($products) use($category, $useStock) {
            foreach($products as $product) {
                $product = $this->productContract->recalcModulesCount($product);

                $inStock = !$useStock || $product->in_stock > 0;

                $product->sale = (($product->discount > 0 || $product->virtual_discount > 0) && $category->autosale == 1 && $inStock);

                $product = $this->productContract->syncLabelCategory($product, 'sale');
                
                $product->save();

                $product = \App\Models\Product::find($product->id);

                $product->updateIndex();
            }
        });
    }

    protected function saveAltImage($category, $key = 'alt')
    {
        $this->saveImage($category, $key.'_image', $key.'_image_id');
    }

    protected function saveImage($category, $key = 'image', $property = 'image_id')
    {
        if(!request()->hasFile($key)) {
            return;
        }

        $image = new ImageService($key, $category);

        $image = $image->upload();

        $category->{$property} = $image->id;
        $category->save();
    }

    protected function savePopulars(Category $category, array $categories = [])
    {
        if(empty($categories)) {
            $category->populars()->delete();
            return $category;
        }

        foreach($categories as $k => $id) {
            $category->populars()->updateOrCreate(['category_id' => $id], ['category_id' => $id, 'ordering' => $k]);
        }

        $category->populars()->whereNotIn('category_id', $categories)->delete();

        return $category;
    }

    protected function saveDocs(Category $category, $attributes)
    {
        $path = 'uploads/docs/categories/'.$category->id;
        \File::exists(public_path($path)) or \File::makeDirectory($path, 0755, true);

        $docs = [];

        foreach ($attributes as $key => $value) {
            if(!empty($value['file']) && $value['file']->isValid()) {

                $name = \Uuid::generate().'_'.str_slug($value['title']).'.'.$value['file']->getClientOriginalExtension();
                
                $value['file']->move(public_path($path), $name);

                $docs[] = ['url' => '/'.$path.'/'.$name, 'title' => $value['title']];
            } elseif(!empty($value['url'])) {
                $docs[] = ['url' => $value['url'], 'title' => $value['title']];
            }
        }

        $category->docs = $docs;

        $category->save();

        return $category;
    }

    public function getVendors()
    {
        return ['выбрать'] + Category::where('vendor', 1)->pluck('title', 'id')->toArray();
    }
}
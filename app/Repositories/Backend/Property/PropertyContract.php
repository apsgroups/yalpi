<?php

namespace App\Repositories\Backend\Property;

interface PropertyContract
{
    public function findById($id);

    public function destroy($id);

    public function paginate($perPage=10);

    public function update($id, array $attributes);

    public function create(array $attributes);
}
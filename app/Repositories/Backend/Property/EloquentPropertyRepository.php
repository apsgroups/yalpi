<?php

namespace App\Repositories\Backend\Property;

use App\Events\PropertySaved;
use App\Models\Property;
use Illuminate\Support\Facades\DB;

class EloquentPropertyRepository implements PropertyContract
{
    protected $properties;

    public function __construct(Property $properties)
    {
        $this->properties = $properties;
    }

    public function setSort($attribute, $dir)
    {
        $this->properties = $this->properties->orderBy($attribute, $dir);
        return $this;
    }

    public function findById($id)
    {
        return $this->properties->findOrFail($id);
    }

    public function paginate($perPage=10)
    {
        return $this->properties->paginate($perPage);
    }

    public function destroy($id)
    {
        $this->properties->destroy($id);
    }

    public function create(array $attributes)
    {
        $property = $this->properties->create($attributes);

        $property = $this->afterSave($property, $attributes);

        return $property;
    }

    public function update($id, array $attributes)
    {
        $property = $this->findById($id);

        $property->update($attributes);

        $property = $this->afterSave($property, $attributes);

        return $property;
    }

    public function afterSave($property, array $attributes)
    {
        $productTypes = isset($attributes['productTypes']) ? $attributes['productTypes'] : [];
        
        $property->productTypes()->sync($productTypes);

        return $property;
    }

    public function setProductTypesFilter(array $productTypes = [])
    {
        if(!empty($productTypes)) {
            // Get properties without product types.
            if (in_array('0', $productTypes)) {
                $this->properties = $this->properties->whereDoesntHave('productTypes');
                return $this;
            }

            $this->properties = $this->properties->whereHas('productTypes', function ($query) use ($productTypes) {
                $query->whereIn('product_type_id', $productTypes);
            });
        }

        return $this;
    }

    public function setPropertyTitleFilter($title = '')
    {
        if (!empty($title)) {
            $this->properties = $this->properties
                ->where('title', 'LIKE', "%$title%")
                ->orWhere('slug', 'LIKE', "%$title%");
        }

        return $this;
    }

    public function setPropertyValueFilter($value = '')
    {
        if (!empty($value)) {
            $this->properties->whereHas('values', function ($query) use ($value) {
                $query
                    ->where('title', 'LIKE', "%$value%")
                    ->orWhere('slug', 'LIKE', "%$value%");
            });
        }

        return $this;
    }

    public function setBooleanFilter(array $attributes = [])
    {
        foreach($attributes as $name => $value) {
            if($value !== '') {
                $this->properties = $this->properties->where($name, $value);
            }
        }

        return $this;
    }

    public function get()
    {
        return $this->properties->get();
    }
}
<?php

namespace App\Repositories\Backend\Widget;

use App\Models\Widgets\Widget;
use App\Models\Widgets\WidgetAssign;
use App\Services\Widget\Save\WidgetSave;

class EloquentWidgetRepository implements WidgetContract
{
    protected $widgets;

    public function __construct(Widget $widgets)
    {
        $this->widgets = $widgets;
    }

    public function findById($id)
    {
        return $this->widgets->with('assigns')->find($id);
    }

    public function create(array $attributes)
    {
        $attributes['user_id'] = auth()->id();

        $attributes = $this->beforeSave($attributes);

        $widget = $this->widgets->create($attributes);

        $this->afterSave($widget, $attributes);

        return $widget;
    }

    public function update($id, array $attributes)
    {
        $widget = $this->findById($id);

        $attributes = $this->beforeSave($attributes);

        $widget->update($attributes);

        $this->afterSave($widget, $attributes);

        return $widget;
    }

    protected function beforeSave(array $attributes)
    {
        $attributes['assign_items'] = empty($attributes['assign_items']) ? [] : $attributes['assign_items'];

        return $attributes;
    }

    protected function saveAssign(Widget $widget, array $items)
    {
        $current = $widget->assigns->pluck('menu_item_id')->toArray();

        if(!empty($current)) {
            $delete = array_diff($current, $items);
        }

        if(!empty($delete)) {
            WidgetAssign::whereIn('menu_item_id', $delete)->delete();
        }

        $insert = array_diff($items, $current);

        if(empty($insert)) {
            return;
        }

        $models = [];
        foreach($insert as $id) {
            $models[] = new WidgetAssign(['menu_item_id' => $id]);
        }

        $widget->assigns()->saveMany($models);
    }

    protected function afterSave(Widget $widget, array $attributes)
    {
        $this->saveAssign($widget, $attributes['assign_items']);
        $this->saveWidget($widget, $attributes);
        return $widget;
    }

    public function destroy($id)
    {
        \DB::transaction(function () use($id) {
            Widget::destroy($id);
        });
    }

    public function sort($ids)
    {
        foreach($ids as $k => $id) {
            $widget = $this->findById($id);
            $widget->ordering = $k;
            $widget->save();
        }
    }

    protected function saveWidget(Widget $widget, array $attributes)
    {
        (new WidgetSave($widget, $attributes))->save();
    }
}
<?php

namespace App\Repositories\Backend\Widget;

interface WidgetContract
{
    public function findById($id);

    public function create(array $attributes);

    public function update($id, array $attributes);

    public function destroy($id);
}
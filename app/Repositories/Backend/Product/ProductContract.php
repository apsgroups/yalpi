<?php

namespace App\Repositories\Backend\Product;

use App\Models\Product;

interface ProductContract
{
    public function findById($id);

    public function paginateWith($perPage=10, array $relations = []);

    public function create(array $attributes);

    public function update($id, array $attributes);

    public function setSearchWord($search);

    public function setBooleanFilter(array $attributes = []);

    public function setCategoryFilter($id, $sort=false);

    public function setPropertyFilter(array $attributes);

    public function setFilledOrEmpty($attribute, $cond);

    public function setSort($attribute, $dir);

    public function setSortByProperty($propertyId, $dir);

    public function saveSortByCategory($items, $categoryId);

    public function destroy($id);

    public function copy($id);

    public function byIds(array $ids);

    public function syncCategories(Product $product, array $categories = []);

    public function attachCategories(Product $product, array $categories = []);

    public function detachCategories(Product $product, array $categories = []);

    public function syncLabelCategory(Product $product, $name);

    public function batchUpdate(array $ids, array $attributes);
}
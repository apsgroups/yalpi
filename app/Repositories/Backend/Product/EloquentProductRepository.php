<?php

namespace App\Repositories\Backend\Product;

use App\Events\ProductSaved;
use App\Models\Category;
use App\Models\Image;
use App\Models\Popular;
use App\Models\Product;
use App\Models\Feature;
use App\Models\Cource;
use App\Models\ProductAttribute;
use App\Services\Image\ImageDestroyer;
use App\Services\Image\ImageService;

class EloquentProductRepository implements ProductContract
{
    protected $products;

    public function __construct(Product $products)
    {
        $this->products = $products;
    }

    public function findById($id)
    {
        return $this->products->with('properties')->findOrFail($id);
    }

    public function setSearchWord($search)
    {
        if(!empty($search)) {
            $this->products = $this->products
                ->where(function($query) use($search) {
                    $query->where('products.title', 'LIKE', '%'.$search.'%')
                        ->orWhere('products.id', $search)
                        ->orWhere('products.sku', $search)
                        ->orWhere('products.ext_id', $search)
                        ->orWhere('products.slug', $search);
                });
        }

        return $this;
    }

    public function setSort($attribute, $dir)
    {
        $this->products = $this->products->orderBy($attribute, $dir);
        return $this;
    } 

    public function setSortByProperty($propertyId, $dir)
    {
        $this->products = $this->products->select('products.*')
            ->join('product_properties', 'products.id', '=', 'product_properties.product_id')
            ->join('property_values', 'product_properties.property_value_id', '=', 'property_values.id')
            ->where('product_properties.property_id', $propertyId)
            ->orderBy('property_values.title', $dir)
            ->groupBy('products.id');

        return $this;
    }

    public function setFilledOrEmpty($attribute, $cond)
    {
        if(empty($cond)) {
            return $this;
        }

        $cond = (int)$cond === 2 ? '=' : '<>';

        $this->products = $this->products->where($attribute, $cond, '');

        return $this;
    }

    public function setBooleanFilter(array $attributes = [])
    {
        foreach($attributes as $name => $value) {
            if($value !== '') {
                $this->products = $this->products->where($name, $value);
            }
        }

        return $this;
    }

    public function setCategoryFilter($id, $sort=false)
    {
        if(empty($id)) {
            return $this;
        }

        $this->products = $this->products->select(['products.*', 'category_product.ordering as category_ordering'])
            ->join('category_product', 'products.id', '=', 'category_product.product_id')
            ->where('category_product.category_id', $id);

        if($sort) {
            $this->products = $this->products->orderBy('category_product.ordering', $sort);
        }

        return $this;
    }

    public function setPropertyFilter(array $attributes)
    {
        if(empty($attributes)) {
            return $this;
        }

        foreach($attributes as $propertyId => $valueId) {
            if($valueId === '') {
                continue;
            }

            $cond = ($valueId === '0') ? '<' : '>=';

            $this->products = $this->products->whereHas('properties', function($query) use($propertyId, $valueId) {
                $cond = ($valueId === '0') ? '>' : '=';
                $query->where('property_value_id', $cond, $valueId);
                $query->where('property_id', $propertyId);
            }, $cond, 1);
        }

        return $this;
    }

    public function paginateWith($perPage=10, array $relations = [])
    {
        if($relations) {
            $this->products = $this->products->with($relations);
        }

        return $this->products->paginate($perPage);
    }

    public function byIds(array $ids)
    {
        return $this->products->whereIn('id', $ids)->get();
    }

    public function syncCategories(Product $product, array $categories = [])
    {
        $categories = $this->filterEmpty($categories);

        $pivot = [];

        if($product->categories->count()) {
            foreach($product->categories as $category) {
                $pivot[$category->id] = ['ordering' => $category->pivot->ordering];
            }
        }

        $sync = [];

        foreach($categories as $k => $id) {
            $sync[$id] = isset($pivot[$id]) ? $pivot[$id] : ['ordering' => 0];
        }

        $product->categories()->sync($sync);

        return $product;
    }

    public function saveAuthors(Product $product, array $authors = [])
    {
        $sync = [];

        if($authors) {
            foreach($authors as $order => $id) {
                $sync[$id]['ordering'] = $order;
            }
        }

        $product->authors()->sync($sync);

        return $product;
    }

    public function saveRelateds(Product $product, array $relateds = [])
    {
        $sync = [];

        if($relateds) {
            foreach($relateds as $order => $id) {
                $sync[$id]['ordering'] = $order;
            }
        }

        $product->relateds()->sync($sync);

        return $product;
    }

    public function attachCategories(Product $product, array $categories = [])
    {
        $ids = [];

        $product->load('categories');

        if($product->categories->count()) {
            foreach ($product->categories as $category) {
                $ids[$category->id] = ['ordering' => $category->pivot->ordering];
            }
        }

        foreach($categories as $k => $v) {
            if(!$v) {
                continue;
            }

            $ids[$k] = $v;
        }

        $product->categories()->sync($ids);

        return $product;
    }

    public function detachCategories(Product $product, array $categories = [])
    {
        $categories = $this->filterEmpty($categories);

        $product->categories()->detach($categories);

        return $product;
    }

    public function syncLabelCategory(Product $product, $name)
    {
        $categoryId = \Settings::get($name.'_category_id');

        if(!$categoryId) {
            return $product;
        }

        $ordering = \DB::table('category_product')->select('ordering')
            ->where(['product_id' => $product->id, 'category_id' => $product->category_id])
            ->first(); 

        $ordering = ($ordering && $ordering->ordering > 0) ? $ordering->ordering : 1;

        if($name === 'sale') {
            $typeCategory = $product->type->saleCategory;

            if($typeCategory) {
                $ordering = ($typeCategory->ordering * 1000) + $ordering;
                $categories[$typeCategory->id]['ordering'] = $ordering;
            } else {
                $ordering += 50000;
            }
        }

        $categories[$categoryId]['ordering'] = $ordering;

        if($product->getAttribute($name)) {
            return $this->attachCategories($product, $categories);
        }

        return $this->detachCategories($product, array_keys($categories));
    }

    public function create(array $attributes)
    {
        $attributes['user_id'] = auth()->id();

        $attributes = $this->beforeSave($attributes);

        $product = $this->products->create($attributes);

        $this->afterSave($product, $attributes);

        return $product;
    }

    public function copy($id)
    {
        $origProduct = $this->findById($id);

        $attributes = $origProduct->toArray();
        $attributes['title'] .= '[copy]';
        $attributes['slug'] .= '-copy';
        $attributes['user_id'] = auth()->id();

        $attributes['property'] = [];
        if($origProduct->properties->count() > 0) {
            foreach($origProduct->properties as $propertyValue) {
                if(in_array($propertyValue->property->type, ['color', 'list'])) {
                    $property = $propertyValue->property_value_id;
                } else {
                    $property = $propertyValue->value_double;
                }

                if($propertyValue->property->multiple) {
                    $attributes['property'][$propertyValue->property_id][] = $property;
                    continue;
                }

                $attributes['property'][$propertyValue->property_id] = $property;
            }
        }

        $categories = $origProduct->categories->pluck('pivot')->toArray();
        $attributes['categories'] = [];

        if($categories > 0) {
            foreach($categories as $category) {
                $attributes['categories'][$category['category_id']] = $category['category_id'];
            }
        }

        $attributes['image_id'] = null;

        $attributes = $this->beforeSave($attributes);

        $product = $this->products->create($attributes);

        $this->afterSave($product, $attributes);

        event(new ProductSaved($product, $attributes));
    }

    public function update($id, array $attributes)
    {
        $product = $this->findById($id);

        $attributes = $this->beforeSave($attributes);

        $product->update($attributes);

        $product = $this->afterSave($product, $attributes);

        return $product;
    }

    protected function beforeSave(array $attributes)
    {
        $attributes['image_id'] = empty($attributes['image_id']) ? null : $attributes['image_id'];
        $attributes['categories'] = empty($attributes['categories']) ? [$attributes['category_id']] : $attributes['categories'];
        $attributes['populars'] = empty($attributes['populars']) ? [] : $attributes['populars'];
        $attributes['ext_preview'] = empty($attributes['ext_preview']) ? [] : $attributes['ext_preview'];
        $attributes['features'] = empty($attributes['features']) ? [] : $attributes['features'];
        $attributes['authors'] = empty($attributes['authors']) ? [] : $attributes['authors'];
        $attributes['relateds'] = empty($attributes['relateds']) ? [] : $attributes['relateds'];
        $attributes['rate'] = empty($attributes['rate']) ? 0 : $attributes['rate'];
        
        return $attributes;
    }

    protected function afterSave($product, array $attributes)
    {
        $product->sale = $product->discount > 0;

        $product = $this->syncCategories($product, $attributes['categories']);
        $product = $this->savePopulars($product, $attributes['populars']);
        $product = $this->syncLabelCategory($product, 'sale');
        $product = $this->syncLabelCategory($product, 'novelty');
        $product = $this->saveAuthors($product, $attributes['authors']);
        $product = $this->saveRelateds($product, $attributes['relateds']);
        $product = $this->saveRate($product, $attributes['rate']);
        $product = $this->saveImage($product);
        $product = $this->saveVideoThumbnail($product);

        $rootCategory = $product->categories()->whereNull('parent_id')->first();

        if($rootCategory) {
            $product->root_category_id = $rootCategory->id;
            $product->icon_bg = $rootCategory->img_bg;
        }

        $product->rate_round = !empty($attributes['rate_round']) ? $attributes['rate_round'] : floor($product->rate);

        $product->generate_video_preview = 1;
        
        $product->save();

        $this->saveFeatures($product, $attributes['features']);

        $this->saveCource($product, $attributes['free_cources'], 'free_cources', 1);
        $this->saveCource($product, $attributes['cource_program'], 'cource_program', 0);

        $this->updateExtPreviews($product, array_keys($attributes['ext_preview']));

        if(!empty($attributes['id'])) {
            try {
                $product->updateIndex();
            } catch(\Elasticsearch\Common\Exceptions\Missing404Exception $e) {
                $product->addToIndex();
            }
        } else {
            $product->addToIndex();
        }
        
        return $product;
    }

    protected function saveRate($product, $rate)
    {
        if($product->comments()->count()) {
            return $product;
        }

        $product->rate = $rate;
        $product->rate_round = $rate;

        return $product;
    }

    protected function saveVideoThumbnail($product)
    {
        if(!$product->video || $product->image_id) {
            return $product;
        }

        $code = str_replace('https://www.youtube.com/watch?v=', '', $product->video);

        try {
            $imageService = new \App\Services\Image\ImageService('', $product);
            $image = $imageService->saveByUrl('https://img.youtube.com/vi/'.$code.'/maxresdefault.jpg');

            $product->image_id = $image->id;
        } catch (\Exception $e) {
            try {
                $imageService = new \App\Services\Image\ImageService('', $product);
                $image = $imageService->saveByUrl('https://img.youtube.com/vi/'.$code.'/0.jpg');

                $product->image_id = $image->id;
            } catch (\Exception $e) { }
        }

        return $product;
    }

    protected function saveCource($product, $cources, $imgScope, $free)
    {
        $ids = [];

        $added = false;

        foreach ($cources as $key => $value) {
            $title = trim($value['title']);

            if($title === '') {
                continue;
            }

            if($value['id'] > 0) {
                $cource = Cource::findOrNew($value['id']);
            } else {
                $cource = new Cource();
            }

            $cource->product_id = $product->id;
            $cource->title = $title;
            $cource->desc = $value['desc'];
            $cource->ordering = $value['ordering'];
            $cource->video = $value['video'];
            $cource->duration = 3600;
            $cource->free = $free;
            $cource->save();

            $imgKey = $imgScope.'.'.$key.'.img';

            if(request()->hasFile($imgKey)) {
                $image = (new ImageService($imgKey, $cource))->upload();
                $cource->image_id = $image->id;
                $cource->save();
            }

            $ids[] = $cource->id;

            $added = true;
        }

        if($ids) {
            $product->cources()->free($free)->whereNotIn('id', $ids)->delete();
        } elseif(!$added) {
            $product->cources()->free($free)->delete();
        }
    }

    protected function saveFeatures($product, $features)
    {
        $ids = [];

        $added = false;

        foreach ($features as $key => $value) {
            $text = trim($value['text']);

            if($text === '') {
                continue;
            }

            if($value['id'] > 0) {
                $feature = Feature::findOrNew($value['id']);
            } else {
                $feature = new Feature();
            }

            $feature->product_id = $product->id;
            $feature->text = $text;
            $feature->icon = $value['icon'];
            $feature->ordering = $value['ordering'];
            $feature->save();

            $ids[] = $feature->id;

            $added = true;
        }

        if($ids) {
            $product->features()->whereNotIn('id', $ids)->delete();
        } elseif(!$added) {
            $product->features()->delete();
        }
    }

    protected function updateExtPreviews(Product $product, $images = [])
    {
        if(!$product->images()->count()) {
            return;
        }

        if($images) {
            $product->images()->whereIn('id', $images)->update(['ext_preview' => 1]);
        }

        $product->images()->whereNotIn('id', $images)->update(['ext_preview' => 0]);
    }

    protected function savePopulars(Product $product, array $categories = [])
    {
        $ids = $product->populars()->pluck('category_id')->toArray();

        if(!empty($categories)) {
            $key = array_search('', $categories);

            if($key !== false) {
                unset($categories[$key]);
            }
        }

        $delete = array_diff($ids, $categories);
        $insert = array_diff($categories, $ids);

        if($insert) {
            $rows = [];
            foreach($insert as $id) {
                $rows[] = new Popular(['category_id' => $id]);
            }

            $product->populars()->saveMany($rows);
        }

        if($delete) {
            $product->populars()->whereIn('category_id', $delete)->delete();
        }

        return $product;
    }

    protected function saveImage($product, $field = 'image')
    {
        $request = app()->make('request');

        if(!$request->hasFile($field)) {
            return $product;
        }

        $image = new ImageService($field, $product);

        $image = $image->upload();

        $product->image_id = $image->id;

        return $product;
    }

    public function saveSortByCategory($items, $categoryId)
    {
        foreach($items as $id => $ordering) {
            $prev = \DB::table('category_product')
                ->select('ordering')
                ->where('product_id', $id)
                ->where('category_id', $categoryId)
                ->first();

            if($prev && (int)$prev->ordering === (int)$ordering) {
                continue;
            }

            \DB::table('category_product')
                ->where('product_id', $id)
                ->where('category_id', $categoryId)
                ->update(['ordering' => $ordering]);

            $this->findById($id)->addToIndex();
        }
    }

    public function destroy($id)
    {
        \DB::transaction(function () use($id) {
            $product = $this->findById($id);
            $product->image_id = null;
            $product->save();

            foreach($product->images as $image) {
                try {
                    ImageDestroyer::destroy($image->id);
                } catch(\InvalidArgumentException $e) { }
            }

            try {
                $product->removeFromIndex();
            } catch(\Elasticsearch\Common\Exceptions\Missing404Exception $e) { }
            
            $product->delete();
        });
    }

    protected function filterEmpty(array $rows)
    {
        $key = array_search('', $rows);

        if($key !== false) {
            unset($rows[$key]);
        }

        return $rows;
    }

    public function batchUpdate(array $ids, array $attributes)
    {
        $this->products->whereIn('id', $ids)->update($attributes);
    }
}
<?php

namespace App\Repositories\Frontend\Compare;

use App\Models\Compare;
use App\Repositories\Frontend\Product\ProductContract;

class EloquentCompareRepository implements CompareContract
{
    protected $compare;

    protected $products;

    protected $user;

    public function __construct(Compare $compare, ProductContract $products)
    {
        $this->compare = $compare;
        $this->user = auth()->user();
        $this->products = $products;
    }

    public function all()
    {
        if(!$this->user) {
            return null;
        }

        return $this->user->compare->keyBy('product_id');
    }

    public function add($id)
    {
        return $this->compare->create([
            'product_id' => $id,
            'user_id' => $this->user->id,
        ]);
    }

    public function remove($id)
    {
        return $this->user->compare()->ofProduct($id)->delete();
    }

    public function clear()
    {
        return $this->user->compare()->delete();
    }
}
<?php

namespace App\Repositories\Frontend\Compare;

interface CompareContract
{
    public function all();

    public function add($id);

    public function remove($id);

    public function clear();
}
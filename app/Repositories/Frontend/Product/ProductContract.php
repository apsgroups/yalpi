<?php

namespace App\Repositories\Frontend\Product;

use App\Models\Product;
use App\Models\Category;
use App\Services\Frontend\SortingService;

interface ProductContract
{
    public function findById($id);

    public function setOrderByIds(array $ids);

    public function setOrderBy(SortingService $sortingService);

    public function setCategories($id);

    public function getIds();

    public function setLastSeen($id);

    public function getLastSeen($exclude=null);

    public function similarProducts(Product $product, $skip = 0, $take = 4);

    public function features(Product $product);

    public function experience(Product $product);
}
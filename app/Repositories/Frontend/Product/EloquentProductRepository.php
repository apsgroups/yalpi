<?php

namespace App\Repositories\Frontend\Product;

use App\Models\Category;
use App\Models\Product;
use App\Models\Property;
use App\Services\Frontend\SortingService;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class EloquentProductRepository implements ProductContract
{
    protected $products;

    protected $cacheTime;

    public function __construct(Product $products)
    {
        $this->products = $products;

        $this->cacheTime = config('cache.time');
    }

    public function findById($id)
    {
        return $this->products->findOrFail($id);
    }

    public function all()
    {
        return $this->products
            ->published()
            ->get();
    }

    public function getIds()
    {
        return $this->all()->pluck('id');
    }

    public function setCategories($id)
    {
        $this->products = $this->products->join('category_product', 'category_product.product_id', '=', 'products.id');

        if(is_array($id)) {
            $this->products = $this->products->whereIn('category_product.category_id', $id);
        } elseif(is_int($id)) {
            $this->products = $this->products->where('category_product.category_id', $id);
        }

        return $this;
    }

    public function setOrderBy(SortingService $sortingService)
    {
        $attribute = $sortingService->convertAttributeToField();
        $direction = $sortingService->getDirection();

        return $this->orderBy($attribute, $direction);
    }

    public function orderBy($attribute, $direction)
    {
        $this->products = $this->products->orderBy($attribute, $direction);

        return $this;
    }

    public function setOrderByIds(array $ids)
    {
        if(!empty($ids)) {
            $this->products = $this->products->orderByRaw(\DB::raw('FIELD(id, '.implode(',', $ids).')'));
        }

        return $this;
    }

    public function setLastSeen($id)
    {
        $lastSeen = $this->getLastSeenIds();

        if(!empty($lastSeen)) {
            $key = array_search($id, $lastSeen);

            if($key !== false) {
                unset($lastSeen[$key]);
            }
        }

        $lastSeen[] = $id;

        $lastSeen = array_slice($lastSeen, (config('site.last-seen') * -1));

        session()->put('products.lastSeen', $lastSeen);
    }

    public function getLastSeen($exclude = null)
    {
        $lastSeen = $this->getLastSeenIds();

        if(empty($lastSeen)) {
            return collect([]);
        }

        $products = [];

        foreach($lastSeen as $k => $id) {
            try {
                $product = $this->findById($id);
                $products[] = $product;
            } catch(ModelNotFoundException $e) {
                unset($lastSeen[$k]);
                session()->put('products.lastSeen', $lastSeen);
            }
        }

        return collect($products);
    }

    public function getLastSeenIds()
    {
        return session()->get('products.lastSeen');
    }

    public function similarProducts(Product $product, $skip = 0, $take = 4)
    {
    }

    public function features(Product $product)
    {
        return $product->features()->orderBy('ordering', 'asc')->get();
    }

    public function experience(Product $product)
    {
        return explode("\n", $product->experience);
    }
}
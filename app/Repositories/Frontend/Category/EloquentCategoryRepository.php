<?php

namespace App\Repositories\Frontend\Category;

use App\Models\Category;
use App\Models\MenuItem;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class EloquentCategoryRepository implements CategoryContract
{
    protected $categories;

    protected $cacheTime;

    public function __construct(Category $categories)
    {
        $this->categories = $categories;
        $this->cacheTime = config('cache.time');
    }

    public function clear()
    {
        $this->models = [];
    }

    public function findById($id)
    {
        return $this->categories->published(1)->findOrFail($id);
    }

    public function getChildren($id)
    {
        return \Cache::remember('category.children.'.$id, $this->cacheTime, function () use($id) {
            return $this->categories->children()
                ->published()
                ->orderBy('ordering', 'asc')
                ->get();
        });
    }

    public function getChildrenWithoutComposition($id, $relations = [])
    {
        $cacheKey = 'category.cwc.'.$id.'.'.md5(implode(',', $relations));

        return \Cache::remember($cacheKey, $this->cacheTime, function () use($id, $relations) {
            $category = $this->findById($id);

            if($category->sub_categories) {
                $category->sub_categories = array_filter($category->sub_categories, function($v, $k) {
                    return $v > 0;
                }, ARRAY_FILTER_USE_BOTH);

                if($category->sub_categories) {
                    return $this->categories->published()->whereIn('id', $category->sub_categories)
                        ->orderByRaw(\DB::raw('FIELD(id, '.implode(',', $category->sub_categories).')'))
                        ->get();
                }
            }

            return $category
                ->children()
                ->published()
                ->where('composition', 0)
                ->where('show_as_child', true)
                ->with($relations)
                ->orderBy('ordering', 'asc')
                ->get();
        });
    }

    public function getCompositions($id, $sortBy, $sortDir)
    {
        $cacheKey = 'category.compositions.'.$id.'.'.$sortBy.'.'.$sortDir;

        return \Cache::remember($cacheKey, $this->cacheTime, function () use($id, $sortBy, $sortDir) {
            return $this->categories
                ->select(
                    'categories.*',
                    \DB::raw('MIN(products.price) as min_price'),
                    \DB::raw('MIN(products.price_old) as min_price_old'),
                    \DB::raw('MAX(products.hits) as max_hits')
                )
                ->join('category_product', 'categories.id', '=', 'category_product.category_id')
                ->join('products', 'category_product.product_id', '=', 'products.id')
                ->where([
                    'composition' => 1,
                    'categories.published' => 1,
                    'products.published' => 1,
                    'show_as_child' => 1,
                    'parent_id' => $id,
                ])
                ->with('image')
                ->orderBy($sortBy, $sortDir)
                ->groupBy('categories.id')
                ->get();
        });
    }

    public function getByIds(array $ids)
    {
        return $this->categories
            ->published()
            ->whereIn('id', $ids)
            ->where('show_as_child', true)
            ->with('image')
            ->orderBy('ordering', 'asc')
            ->get();
    }

    public function getChildrenWith($id, $relations = [])
    {
        $cacheKey = 'category.with.'.$id.'.'.md5(implode(',', $relations));

        return \Cache::remember($cacheKey, $this->cacheTime, function () use($id, $relations) {
            return $this->findById($id)
                ->children()
                ->with($relations)
                ->get();
        });
    }

    public function getParent($id)
    {
        $child = $this->findById($id);

        if(!empty($child->parent)) {
            return null;
        }

        return $child->parent;
    }

    public function getCategoriesForSearch()
    {
        return \Cache::remember('category.for.search', $this->cacheTime, function () {
            $parents = $this->categories->whereNull('parent_id')
                ->published()
                ->where('search_form', 1)
                ->orderBy('ordering', 'asc')
                ->with('children')
                ->get();

            $list = [];

            foreach($parents as $category) {
                $list = $this->getChildrenRecursive($category, $list);
            }

            return $list;
        });
    }

    protected function getChildrenRecursive(Category $parent, array $list, $level = 0)
    {
        $childrens = $parent->children()
            ->published()
            ->where('search_form', 1)
            ->with('children')
            ->orderBy('ordering', 'asc')
            ->get();

        $list[$parent->id] = str_repeat('-', $level).' '.$parent->title;

        $level++;
        foreach($childrens as $child) {
            $list = $this->getChildrenRecursive($child, $list, $level);
        }

        return $list;
    }

    public function getBreadcrumbsPath($id)
    {
        return \Cache::remember('breadcrumbs.category.'.$id, config('cache.time'), function () use($id) {
            $path = $this->buildBreadcrumbsPath($id);
            return array_reverse($path);
        });
    }

    protected function buildBreadcrumbsPath($id, $path = [])
    {
        $menuPath = (new \App\Services\MenuService())->getBreadcrumbPath($id, 'category');

        if(!empty($menuPath)) {
            return array_merge($path, array_reverse($menuPath));
        }

        try {
            $category = $this->findById($id);
        } catch(ModelNotFoundException $e) {
            return [];
        }


        $path[] = ['title' => $category->getBreadcrumbTitle(), 'url' => category_url($category)];

        if($category->parent_id) {
            $path = $this->buildBreadcrumbsPath($category->parent_id, $path);
        }

        return $path;
    }
}
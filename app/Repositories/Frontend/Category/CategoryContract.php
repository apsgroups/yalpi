<?php

namespace App\Repositories\Frontend\Category;

interface CategoryContract
{
    public function findById($id);

    public function getChildren($id);

    public function getChildrenWithoutComposition($id, $relations = []);

    public function getCompositions($id, $sortBy, $sortDir);

    public function getByIds(array $ids);

    public function getParent($id);

    public function getCategoriesForSearch();

    public function getBreadcrumbsPath($id);
}
<?php

namespace App\Events;

use App\Events\Event;
use App\Http\Requests\Request;
use App\Models\Property;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class PropertySaved extends Event
{
    use SerializesModels;

    public $property;

    public $attributes;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Property $property, $attributes)
    {
        $this->property = $property;

        $this->attributes = $attributes;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}

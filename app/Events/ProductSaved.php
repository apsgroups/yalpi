<?php

namespace App\Events;

use App\Events\Event;
use App\Models\Product;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ProductSaved extends Event
{
    use SerializesModels;

    public $product;
    public $attriutes;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Product $product, array $attributes)
    {
        $this->product = $product;

        $this->attributes = $attributes;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}

<?php

namespace App\Events\Frontend\Feedback;

use App\Events\Event;
use App\Http\Requests\Request;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class FeedbackSend extends Event
{
    use SerializesModels;

    public $feedback;

    public $params;

    public $request;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($feedback, $params)
    {
        $this->request = $request;
        $this->feedback = $feedback;
        $this->params = $params;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}

<?php

namespace App\Vendor\laravel\framework\src\Illuminate\Routing;

use Illuminate\Contracts\Routing\UrlGenerator as UrlGeneratorContract;

class UrlGenerator extends \Illuminate\Routing\UrlGenerator implements UrlGeneratorContract
{
    /**
     * Get the URL for a given route instance.
     *
     * @param  \Illuminate\Routing\Route  $route
     * @param  mixed  $parameters
     * @param  bool   $absolute
     * @return string
     *
     * @throws \Illuminate\Routing\Exceptions\UrlGenerationException
     */
    protected function toRoute($route, $parameters, $absolute)
    {
        $url = parent::toRoute($route, $parameters, $absolute);

        return $this->appendSlash($url);
    }


    public function to($path, $extra = [], $secure = null)
    {
        $url = parent::to($path, $extra, $secure);

        return $this->appendSlash($url);
    }

    protected function appendSlash($url)
    {
        if(preg_match('/\.[a-zA-Z]+$/', $url)) {
            return $url;
        }

        if(ends_with($url, '/')) {
            return $url;
        }

        if(strpos($url, '#') !== false) {
            return $url;
        }

        if(strpos($url, '?') !== false && strpos($url, '/?') === false) {
            return str_replace('?', '/?', $url);
        }

        if(strpos($url, '?') !== false) {
            return $url;
        }

        return $url.'/';
    }

    protected function getScheme($secure)
    {
        // return config('app.scheme', 'https').'://';

        if(!empty($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] === 'on') {
            return 'https://';
        }

        return 'http://';
    }
}

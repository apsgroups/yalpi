<?php

namespace App\Widgets;

use App\Models\Category;
use App\Models\Menu;
use App\Models\MenuItem;
use Arrilot\Widgets\AbstractWidget;
use Illuminate\Support\Facades\Cache;
use Lavary\Menu\Facade as MenuFacade;

class MenuWidget extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [
        'menu_id' => null,
        'menu' => null,
        'layout' => null,
        'class' => '',
    ];

    protected $items = null;

    protected $model = null;

    protected $current = null;

    protected $targets = [];

    public function __construct(array $config = [])
    {
        $this->targets = config('menu.target');

        $config['only_showcase'] = isset($config['only_showcase']) ? $config['only_showcase'] : 0;

        $config['menu_html_id'] = isset($config['menu_html_id']) ? $config['menu_html_id'] : '';

        $config['limit'] = isset($config['limit']) ? $config['limit'] : 0;

        parent::__construct($config);
    }

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $this->current = request()->get('menu_item_id');

        $cacheUniq = '';

        if(in_array($this->config['layout'], ['catalog-children', 'sidebar-categories', 'catalog-children-mob'])) {
            $cacheUniq = request()->getPathInfo();
        }

        $view = \Cache::remember('menu.'.$this->config['menu'].'.'.$this->config['layout'].'.'.md5($cacheUniq).'.'.$this->config['limit'], config('cache.time'), function() {
            $model = $this->getModel($this->config);

            $menu = $this->items($model);

            return view()->make($this->getLayout(), [
                'config' => $this->config,
                'menu' => $menu,
                'model' => $model,
            ])->render();
        });

        if($this->config['layout'] === 'catalog-children') {
            return $view;
        }
        
        $view = $this->replaceActive($view);

        return $view;
    }

    protected function replaceActive($view)
    {
        $current = request()->getPathInfo();

        $path = explode('/', $current);

        $search = 'item-'.implode('---', $path).'----end';

        while($path && strpos($view, $search) === false) {
            array_pop($path);

            $search = 'item-'.implode('---', $path).'----end';
        }

        preg_match_all('/class="(.*'.$search.'.*?)"/', $view, $rows);

        if(isset($rows[1])) {
            foreach($rows[1] as $k => $m) {
                preg_match('/itemid-(.*?)-end/', $m, $match);
                preg_match('/layout-regex-(.*?)-end/', $m, $match2);

                $regexLayout = isset($match2[1]) ? $match2[1] : '';

                $view = $this->replaceRegex($current, $view, $regexLayout, $search);

                $ids = explode('_', $match[1]);

                $path = [];

                foreach ($ids as $key => $value) {
                    $path[] = $value;

                    $replace = 'active';

                    if($value === end($ids)) {
                        $replace .= ' selected';

                        if($m === end($rows[1])) {
                            $replace .= ' target';
                        }
                    }

                    $view = str_replace('itemid-'.implode('_', $path).'-end', $replace, $view);
                }
            }
        }

        $view = preg_replace('/layout-regex-.*?end/s', '', $view);
        $view = preg_replace('/item---.*?end/s', '', $view);
        $view = preg_replace('/itemid-.*?end/s', '', $view);
        $view = preg_replace('/class="\s*?"/s', '', $view);
        $view = str_replace('item-#-end', '', $view);

        if(request()->is('/')) {
            $view = str_replace(['<!-- noindex -->', '<!--/ noindex -->', 'nofollow'], ['', '', ''], $view);
        }

        return $view;
    }

    protected function replaceRegex($path, $view, $layout, $search) 
    {
        $path = str_replace('/', '\/', $path);

        if($layout === 'sidebar-sofa.index') {
            $view = preg_replace('/(<\!-- children (?!'.$search.').*?-->?(.*?)<\!-- end children.*?)>/s', '', $view);
        }

        if($layout === 'offcanvas.item') {
            return str_replace($search, 'active', $view);
        }

        if($layout === 'uikit.root') {
            return preg_replace('/<(a href="'.$path.'".*?)>(.*?)<\/a>/s', '<span class="menu-root-title">$2</span>', $view);
        }

        if($layout === 'uikit.type.default') {
            return preg_replace('/<(a href="'.$path.'".*?).*?>(.*?)<\/a>/s', '<span class="menu-item-title">$2</span>', $view);
        }

        if($layout === 'uikit-tab.root') {
            return preg_replace('/<(a href="'.$path.'".*?)(data-id="\d+?").*?>(.*?)<\/a>/s', '<span $2 class="tab-btn menu-item-title">$3</span>', $view);
        }

        return preg_replace('/<(a href="'.$path.'".*?)>(.*?)<\/a>/s', '<span>$2</span>', $view);
    }

    protected function getModel($config)
    {
        if(!empty($config['menu_id'])) {
            return Menu::published(1)->find($config['menu_id']);
        }

        return Menu::published(1)->slug($config['menu'])->first();
    }

    protected function getLayout()
    {
        $view = 'widgets.menu_widget';
        $custom = 'widgets.menu.'.$this->config['layout'].'.index';

        if(!empty($this->config['layout']) && view()->exists($custom)) {
            $view = $custom;
        }

        return $view;
    }

    protected function items($menu)
    {
        if(empty($menu)) {
            return [];
        }

        $items = $menu->items()->published(1)->orderBy('ordering', 'asc')->get();

        $nodes = [];

        foreach ($items as $item) {
            $nodes[(int)$item->parent_id][] = $item;
        }

        return $this->buildTree($nodes);
    }

    protected function buildTree($items, $parent = 0, $path = '')
    {
        if(empty($items[$parent])) {
            return [];
        }
        
        foreach ($items[$parent] as $key => $value) {
            if($this->config['only_showcase'] == 0 && $value->only_showcase) {
                continue;
            }

            $item = $value->toArray();

            $item['link'] = $value->link();

            if(!$item['link']) {
                $item['link'] = '#';
            }

            $item['target'] = $this->getTarget($value->target);
            $item['active'] = 0;
            $item['rel'] = $this->getRelString($value);
            $item['disabled'] = 0;
            $original = $value;

            if($item['type'] === 'menu_item') {
                $original = $this->findOriginal($item['item_id']);
            }

            if($original->type === 'category') {
                $category = \App\Models\Category::find($original->item_id);
                
                if($category) {
                    $item['category'] = $category->toArray();

                    if(!empty($this->config['product_count'])) {
                        $item['category_product_count'] = $category->products()->published(1)->count();
                    }
                }
            }

            $linkMarker = 'item-'.str_replace('/', '---', str_replace('https://', '/', $item['link'])).'-end';

            $pathChild = trim($path.'_'.$item['id'], '_');

            $pathMarker = 'itemid-'.$pathChild.'-end';

            $item['marker'] = $linkMarker.' '.$pathMarker;

            $items[$parent][$key] = $item;

            if(isset($items[$value->id])) {
                $items[$parent][$key]['children'] = $this->buildTree($items, $value->id, $pathChild);
            }
        }
        
        return $items[$parent];
    }

    protected function findOriginal($id)
    {
        $menuItem = MenuItem::find($id);

        if(!$menuItem) {
            return;
        }

        if($menuItem->type === 'menu_item') {
            return $this->findOriginal($menuItem->item_id);
        }

        return $menuItem;
    }

    protected function getTarget($target)
    {
        if($target && !empty($this->targets[$target])) {
            return $this->targets[$target];
        }

        return '';
    }

    protected function getRelString($item)
    {
        $rel = [];

        if($item->noindex) {
            $rel[] = 'nofollow';            
        }

        if($item->target) {
            $rel[] = 'noopener';            
        }

        return implode(' ', $rel);
    }
}
<?php

namespace App\Widgets\Category;

use App\Models\Category;
use Arrilot\Widgets\AbstractWidget;

class Homepage extends AbstractWidget
{
    public $cacheTime;

    protected $config = [];

    public function __construct(array $config = [])
    {
        $this->cacheTime = config('cache.time');

        parent::__construct($config);
    }

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $categories = Category::published()
            ->with('image')
            ->withCount(['products' => function ($query) {
                $query->published(1);
            }])
            ->where('at_home', 1)
            ->whereNull('parent_id')
            ->orderBy('ordering', 'asc')
            ->get();

        return view("widgets.category.homepage", [
            'config' => $this->config,
            'categories' => $categories,
        ]);
    }
}
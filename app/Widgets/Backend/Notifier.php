<?php

namespace App\Widgets\Backend;

use App\Models\Import\ImportMissing;
use App\Models\Product;
use Arrilot\Widgets\AbstractWidget;

class Notifier extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $items = [];

        return view("widgets.backend.notifier", compact('items'));
    }
}
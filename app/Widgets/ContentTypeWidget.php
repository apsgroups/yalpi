<?php

namespace App\Widgets;

use App\Models\ContentType;
use Arrilot\Widgets\AbstractWidget;

class ContentTypeWidget extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $contentType = ContentType::find($this->config['id']);

        if(!$contentType) {
            return '';
        }

        $contents = $contentType->contents()->published(1)->orderBy('id', 'desc')->get();

        $layout = isset($this->config['layout']) ? $this->config['layout'] : 'default';

        return view("widgets.content-type.".$layout, [
            'config' => $this->config,
            'contents' => $contents,
            'contentType' => $contentType,
        ]);
    }
}
<?php

namespace App\Widgets;

use App\Models\Content;
use Arrilot\Widgets\AbstractWidget;

class ContentWidget extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $content = Content::published(1)->find($this->config['id']);

        if(!$content) {
            return '';
        }

        return view("widgets.content_widget", [
            'config' => $this->config,
            'content' => $content,
        ]);
    }
}
<?php

namespace App\Widgets;

use App\Models\Content;
use App\Models\ContentType;
use Arrilot\Widgets\AbstractWidget;

class ArticlesWidget extends AbstractWidget
{
    public $cacheTime;

    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    public function __construct(array $config = [])
    {
        $this->cacheTime = config('cache.time');

        parent::__construct($config);
    }

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $articles = Content::with('image', 'type')
            ->ofType(config('site.articles.slug'))
            ->published()
            ->orderBy('id', 'desc')
            ->take(config('site.articles.homepage', 4))
            ->get();

        if($articles->isEmpty()) {
            return '';
        }

        $count = Content::ofType('articles')
            ->published()
            ->count();

        $contentType = ContentType::slug(config('site.articles.slug'))->first();

        return view("widgets.articles_widget", [
            'config' => $this->config,
            'articles' => $articles,
            'count' => $count,
            'contentType' => $contentType,
        ]);
    }
}
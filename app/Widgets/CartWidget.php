<?php

namespace App\Widgets;

use App\Facades\Cart;
use Arrilot\Widgets\AbstractWidget;

class CartWidget extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $amount = Cart::amount();
        $cost = Cart::cost();

        $layout = isset($this->config['layout']) ? $this->config['layout'] : 'default';

        return view("widgets.cart.".$layout, [
            'config' => $this->config,
            'cost' => $cost,
            'amount' => $amount,
        ]);
    }
}
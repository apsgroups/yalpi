<?php

namespace App\Widgets;

use App\Models\Content;
use App\Models\ContentType;
use Arrilot\Widgets\AbstractWidget;

class NewsWidget extends AbstractWidget
{
    public $cacheTime;

    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    public function __construct(array $config = [])
    {
        $this->cacheTime = config('cache.time');

        parent::__construct($config);
    }

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $limit = !empty($this->config['limit']) ? $this->config['limit'] : config('site.news.homepage', 2);
        
        $news = Content::with('image', 'type')
            ->ofType(config('site.news.slug'))
            ->published()
            ->orderBy('id', 'desc')
            ->take($limit)
            ->get();

        if($news->isEmpty()) {
            return '';
        }

        $count = Content::ofType('news')
            ->published()
            ->count();

        $contentType = ContentType::slug(config('site.news.slug'))->first();

        $layout = !empty($this->config['layout']) ? $this->config['layout'] : 'default';

        return view("widgets.news.".$layout, [
            'config' => $this->config,
            'news' => $news,
            'count' => $count,
            'contentType' => $contentType,
        ]);
    }
}
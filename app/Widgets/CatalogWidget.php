<?php

namespace App\Widgets;

use App\Models\Category;
use Arrilot\Widgets\AbstractWidget;

class CatalogWidget extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    public function __construct(array $config = [])
    {
        $this->cacheTime = config('cache.time');

        parent::__construct($config);
    }

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $positions = config('backend.category.positions');

        $categories = Category::with('image');

        if(empty($this->config['all'])) {
            $categories = $categories->where('type', $this->config['type'])->whereIn('position', $positions);
        }

        $categories = $categories->published(1)
            ->orderBy('ordering', 'asc')
            ->get();

        $prices = [];

        foreach($categories as $category) {
            $prices[$category->id] = $category->products()->published(1)->where('price', '>', 0)->min('price');
        }

        if(empty($this->config['all'])) {
            $categories = $categories->groupBy('position');
        } else {
            $categories = $categories->keyBy('id');
        }

        $layout = isset($this->config['layout']) ? $this->config['layout'] : '';

        return view("widgets.catalog.".$layout.$this->config['type'], [
            'config' => $this->config,
            'categories' => $categories,
            'prices' => $prices,
        ]);
    }
}
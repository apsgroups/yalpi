<?php

namespace App\Widgets\Product;

use Arrilot\Widgets\AbstractWidget;

class Similar extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $product = $this->config['product'];

        $limit = !empty($this->config['limit']) ? $this->config['limit'] : 10;

        $layout = !empty($this->config['layout']) ? $this->config['layout'] : 'default';

        $products = \Cache::remember('product.similar.'.$product->id, config('cache.time'), function () use($product, $limit) {
            return $product->category
                ->products()
                ->published(1)
                ->with('image', 'type')
                ->take($limit)
                ->where('product_id', '<>', $product->id)
                ->orderBy('pivot_ordering', 'asc')
                ->get();
        });

        return view("widgets.product.similar.".$layout, [
            'config' => $this->config,
            'products' => $products,
            'inRow' => 5,
        ]);
    }
}
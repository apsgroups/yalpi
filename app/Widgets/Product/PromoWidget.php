<?php

namespace App\Widgets\Product;

use App\Models\Category;
use App\Models\Product;
use Arrilot\Widgets\AbstractWidget;

class PromoWidget extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    public $cacheTime;

    public function __construct(array $config = [])
    {
        $this->cacheTime = config('cache.time');

        parent::__construct($config);
    }

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $categories = Category::where('promo_widget', 1)
            ->published(1)
            ->orderBy('ordering', 'asc')
            ->get();

        $categoriesIds = $categories->pluck('id')->toArray();

        $saleCategoryId = \Settings::get('sale_category_id');

        if(empty($saleCategoryId)) {
            return '';
        }

        $saleCategory = Category::published(1)->find($saleCategoryId);

        if(empty($saleCategory)) {
            return '';
        }

        $limit = !empty($this->config['limit']) ? $this->config['limit'] : config('site.product.promo_widget.limit', 10);
        $inRow = !empty($this->config['in_row']) ? $this->config['in_row'] : config('site.product.promo_widget.in_row', 5);

        $products = [];

        foreach($categories as $category) {
            $products[$category->id] = $category->products()
                ->with(['image', 'category'])
                ->published(1)
                ->whereHas('categories', function ($query) use($saleCategory, $category) {
                    $query->where('categories.id', $saleCategory->id);
                })
                ->orderBy('pivot_ordering', 'asc')
                ->take($limit)
                ->get();
        }

        $layout = !empty($this->config['layout']) ? $this->config['layout'] : 'default';

        return view("widgets.product.promo.".$layout, [
            'config' => $this->config,
            'products' => $products,
            'categories' => $categories,
            'inRow' => $inRow,
            'saleCategory' => $saleCategory,
        ]);
    }
}
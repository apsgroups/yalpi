<?php

namespace App\Widgets\Product;

use Arrilot\Widgets\AbstractWidget;

class PopularCategories extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    protected $model;

    protected $total;

    protected $title;

    protected $cachingTime;

    protected $categoryContract;

    protected $modelName;

    public function __construct(array $config = []) {
        $this->model = $config['model'];
        $this->total = config('site.popular.count');
        $this->in_row = config('site.popular.in_row');
        $this->cachingTime = config('cache.time');
        $this->categoryContract = app()->make('App\Repositories\Frontend\Category\CategoryContract');

        $this->modelName = class_basename($this->model);

        return parent::__construct($config);
    }

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $categories = $this->getPopulars();

        if(empty($categories)) {
            return '';
        }

        $layout = !empty($this->config['layout']) ? $this->config['layout'] : 'default';

        return view("widgets.product.popular_categories.".$layout, [
            'config' => $this->config,
            'categories' => $categories,
            'inRow' => 6,
        ]);
    }

    protected function cacheModel($model) {
        $cacheKey = $this->modelCacheKey();

        if(!\Cache::has($cacheKey)) {
            \Cache::put($cacheKey, $model);
        }
    }

    protected function getCachedModel()
    {
        $cacheKey = $this->modelCacheKey();

        if(\Cache::has($cacheKey)) {
            return \Cache::get($cacheKey);
        }

        return $this->getModel();
    }

    protected function modelCacheKey()
    {
        $model = $this->getModel();

        return 'popular.src.'.$this->modelName.'.'.$model->id;
    }

    protected function getModel()
    {
        return $this->model;
    }

    protected function getCategories($ids)
    {
        if(empty($ids)) {
            return null;
        }

        $populars =  [];

        foreach($ids as $id) {
            try {
                $populars[] = $this->categoryContract->findById($id);
            } catch (\Exception $e) {

            }
        }

        return $populars;
    }

    protected function getPopulars()
    {
        $model = $this->getCachedModel();

        if(!$model) {
            return null;
        }

        $categories = null;

        while(empty($categories) && $model) {
            $ids = $this->getCategoryIds($model);

            $categories = $this->getCategories($ids);

            if($categories) {
                $this->cacheModel($model);
                break;
            }

            if($model instanceof \App\Models\Product) {
                $model = $model->category;
            } else {
                $model = \Cache::remember('popular.model.parent.'.$model->id, $this->cachingTime, function () use($model) {
                    return $model->parent;
                });
            }
        }

        return $categories;
    }

    protected function getCategoryIds($model)
    {
        $cacheKey = 'popular.'.$this->modelName.'.'.$model->id;

        return \Cache::remember($cacheKey, $this->cachingTime, function () use($model) {
            return $model->populars()->orderBy('ordering', 'asc')->get()->pluck('category_id')->toArray();
        });
    }
}
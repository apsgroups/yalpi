<?php

namespace App\Widgets\Product;

use Arrilot\Widgets\AbstractWidget;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class Slider extends AbstractWidget
{
    /**
     * The configuration array. category_id, title
     *
     * @var array
     */
    protected $config = [];

    protected $categoryContract;

    public $cacheTime;

    public function __construct(array $config = [])
    {
        $this->categoryContract = app()->make('\App\Repositories\Frontend\Category\CategoryContract');

        $this->cacheTime = config('cache.time');

        parent::__construct($config);
    }

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $categoryId = $this->config['category_id'];

        if(empty($categoryId)) {
            return '';
        }

        try {
            $category = $this->categoryContract->findById($categoryId);
        } catch(ModelNotFoundException $e) {
            return '';
        }

        $products = $category->products()
            ->published()
            ->with('image', 'type')
            ->take(config('site.product.slider.limit'))
            ->orderBy('pivot_ordering', 'asc')
            ->get();

        if($products->count() == 0) {
            return '';
        }

        $more = category_url($category);

        return view("widgets.product.slider", [
            'config' => $this->config,
            'products' => $products,
            'button' => $this->config['button'],
            // 'count' => $category->products()->count(),
            'more' => $more,
            'title' => $this->config['title'],
            'inRow' => config('site.product.slider.in_row', 3)
        ]);
    }
}
<?php

/**
 * Global helpers file with misc functions
 *
 */

function highlight($haystack, $needle) {
    $color = "#daa732";
    return preg_replace("/($needle)/iu", sprintf('<span class="match">$1</span>', $color), $haystack);
}

function check_active_children($children)
{
    return array_filter($children, function($v, $k) {
        $active = boolval($v['active']);

        if(!$active && !empty($v['children'])) {
            $active = check_active_children($v['children']);
        }

        return $active;
    }, ARRAY_FILTER_USE_BOTH);
}

function category_mobile_description($text)
{
    $text = preg_replace('/(<img.*?)(style=".*?")/', '$1', $text);
    $text = preg_replace('/(<a.*?class="fancybox".*?>(.*?)?<\/a>)/s', '$2', $text);
    $text = preg_replace('/(<img.*?>)/', '<div class="row noMarg"><div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 img">$1</div></div>', $text);

    $text = preg_replace('/<\/div><\/div>\s*?<div class="row noMarg"><div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 img">/', '</div><div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 img">', $text);

    $text = str_replace('<img', '<img class="img-responsive noMarg"', $text);
    $text = preg_replace('/<li>(.*?)<\/li>/', '<li><p>$1</p></li>', $text);

    $text = str_replace('src="https://www.youtube.com/', 'class="video" src="https://www.youtube.com/', $text);

    return $text;
}

function resize_text_img($text)
{
    if(strpos($text, '?resize=') === false) {
        return $text;
    }

    preg_match_all('/(<img.*?)src="((.*?)\?resize=(\d+))".*?\/>?/', $text, $m);

    if(empty($m)) {
        return $text;
    }

    foreach($m[2] as $key => $value) {
        $text = str_replace($value, img_resize($m[3][$key], null, $m[4][$key], null, 'widen'), $text);
    }

    return $text;
}


function is_mobile()
{
    if(request()->get('mobile')) {
        session(['desktop' => false]);
        return true;
    }

    if(session('desktop')) {
        return false;
    }

    if(request()->get('desktop')) {
        session(['desktop' => true]);
        return false;
    }

    $agent = new \Jenssegers\Agent\Agent();

    return $agent->isMobile();
}

function mobile_prefix()
{
    return is_mobile() ? 'mobile' : 'frontend';
}

function mobile_characteristics($text)
{
    $text = preg_replace('/\t/s', '', $text);

    if(!preg_match_all('/<tbody.*?>(.*?)<\/tbody>?/s', $text, $match)) {
        return $text;
    }

    $specs = '';

    foreach($match[1] as $m) {
        $specs .= preg_replace('/<tr>\s*<td>(.*?)<\/td>\s*<td>(.*?)<\/td>\s*<\/tr>/i', '<tr><td><span class="title">$1</span></td><td><span class="value">$2</span></td></tr>', $m);
    }

    return $specs;
}
  
function img_youtube($code, $size = null)
{
    $custom = 'uploads/media/video/'.$code.'.jpg';
    $src = 'https://img.youtube.com/vi/'.$code.'/0.jpg';

    $dest = public_path($custom);

    if(!file_exists($dest)) {
        try {
            if(!copy('https://img.youtube.com/vi/'.$code.'/0.jpg', $dest)) {
                return $src;
            }
        } catch(\Exception $e) {
            \Log::info($code);
            \Log::warning($e);
        }            
    }

    if($size) {
        return img_resize($custom, $size);
    }

    return '/'.$custom;
}

function img_resize($original, $size, $w = null, $h = null, $method = 'fit')
{ 
    $path = pathinfo($original);

    $dir = public_path($path['dirname'].'/resize');

    if(empty($size)) {
        $size = 'custom_'.$w.'x'.$h;
    }

    try {
        \File::exists($dir) or \File::makeDirectory($dir, 0755, true);

        $thumbnail = $path['dirname'].'/resize/'.$path['filename'].'_'.$size.'.'.$path['extension'];

        $thumbnail = ltrim($thumbnail, '/');

        $dest = public_path($thumbnail);

        if(file_exists($dest)) {
            return '/'.$thumbnail;
        }

        $quality = 80;

        $config = config('thumbnail.sizes.custom.'.$size, [$w, $h]);

        $src = \Intervention\Image\Facades\Image::make(public_path($original));

        if(in_array($method, ['heighten', 'widen'])) {
            $k = $method === 'heighten' ? 1 : 0;
            $src->$method($config[$k], function ($constraint) {
                $constraint->upsize();
            });
        } else {                
            $src->$method($config[0], $config[1], function ($constraint) {
                $constraint->upsize();
            });
        }

        // $src->sharpen(10);

        $src->save($dest, $quality);
    } catch(\Exception $e) {
        \Log::info($original.', '.$size);
        \Log::warning($e);

        return $original;
    }

    return '/'.$thumbnail;
}

function mobile_description($text)
{
    if(strpos($text, '<table') !== false) {
        return mobile_characteristics($text);
    }

    if(!preg_match_all('/<p.*?>(.*)?<\/p>/', $text, $match)) {
        return $text;
    }

    $desc = '';

    $row = 0;

    foreach($match[0] as $m) {
        if(preg_match('/<strong>(.*)?<\/strong>/', $m)) {
            $row++;

            $desc[$row] = '<span class="title">'.$m.'</span><br>';
        } else {
            if(!isset($desc[$row])) {
                $desc[$row] = '';
            }

            $desc[$row] .= '<p>'.$m.'</p>';
        }
    }

    return '<tr><td>'.implode('</td></tr><tr><td>', $desc).'</td></tr>';
}

function convert_characteristics($spec)
{
    $crawler = new \Symfony\Component\DomCrawler\Crawler();

    $spec = str_replace('&nbsp;', ' ', $spec);
    $spec = str_replace(['<th', '</th'], ['<td', '</td'], $spec);
 
    $crawler->addHtmlContent($spec);

    if(stripos($spec, '<ul>') !== false) {
        $nodeLiValues = $crawler->filter('li')->each(function (\Symfony\Component\DomCrawler\Crawler $node, $i) {
            return strip_tags($node->text());
        });

        $nodeValues = [];

        foreach($nodeLiValues as $k => $value) {
            $nodeValues = array_merge($nodeValues, explode(' : ', $value));
        }
    } else {
        $crawler = $crawler->filter('tr td');

        $nodeValues = $crawler->filter('td')->each(function (\Symfony\Component\DomCrawler\Crawler $node, $i) {
            return $node->text();
        });
    }

    return collect($nodeValues);
}

function report_email($e)
{
    if(!\App::environment('production')) {
        return;
    }

    \Mail::send('emails.log.index', ['e' => $e], function ($m) {
        $m->from(config('mail.from.address'), config('mail.from.name'));

        $m->to(\Settings::get('email_dev'), 'Developer')->subject('Logs from '.config('app.name').'!');
    });
}

function total_products_morph($n)
{
    $words = ['урок', 'урока', 'уроков'];

    return $n%10==1&&$n%100!=11?$words[0]:($n%10>=2&&$n%10<=4&&($n%100<10||$n%100>=20)?$words[1]:$words[2]);
}

function total_results_morph($n)
{
    $words = ['результат', 'результата', 'результатов'];

    return $n%10==1&&$n%100!=11?$words[0]:($n%10>=2&&$n%10<=4&&($n%100<10||$n%100>=20)?$words[1]:$words[2]);
}

function word_morph($words, $n)
{
    return $n%10==1&&$n%100!=11?$words[0]:($n%10>=2&&$n%10<=4&&($n%100<10||$n%100>=20)?$words[1]:$words[2]);
}

function format_price($price, $currency = false) {
    $append = '';

    if($currency) {
        $append = ' <span class="currency">'.config('site.price.currency').'</span>';
    }

    $price = floatval($price);

    return number_format($price, 0, '.', ' ').$append;
}

function format_count($count) {
    return number_format($count, 0, '.', ' ');
}

function str_sef_slug($title, $separator = '-')
{
    $slug = \App\Services\Translit::convert($title);
    return str_slug($slug, $separator);
}

function img_src($image, $size='')
{
    if(!$image) {
        return false;
    }

    if(empty($size)) {
        return '/'.App\Services\Image\ImageService::getFullpath($image);
    }

    $service = new \App\Services\Image\ImageThumb($image, $size);

    try {                
        $thumb = $service->thumbnail();
    } catch(\Exception $e) {
        \Log::alert($e);

        return false;
    }

    return $thumb;
}

function shipping_date()
{
    $date =\Date::now();

    $weekDay = $date->format('N');
    $hour = $date->format('H');

    $add = 1;

    if($hour >= 12) {
        $add = 2;
    }

    if($weekDay == 5 && $hour < 12) {
        $add = 3;
    }

    if(($weekDay == 5 && $hour >= 12) || $weekDay > 5) {
        $add = 7 - $weekDay + 2;
    }

    $date->addDays($add);

    return $date;
}

function worktime_msg($opened = 'течение часа', $closed = 'рабочее время')
{
    $date = \Date::now();

    $weekDay = $date->format('N');
    $hour = $date->format('H');

    $openHour = 9;
    $closeHour = $weekDay > 5 ? 18 : 21;

    if($hour >= $openHour && $hour < $closeHour) {
        return $opened;
    }

    return $closed;
}

if (! function_exists('app_name')) {
    /**
     * Helper to grab the application name
     *
     * @return mixed
     */
    function app_name()
    {
        return config('app.name');
    }
}

if (! function_exists('access')) {
    /**
     * Access (lol) the Access:: facade as a simple function
     */
    function access()
    {
        return app('access');
    }
}

if (! function_exists('javascript')) {
    /**
     * Access the javascript helper
     */
    function javascript()
    {
        return app('JavaScript');
    }
}

if (! function_exists('gravatar')) {
    /**
     * Access the gravatar helper
     */
    function gravatar()
    {
        return app('gravatar');
    }
}

if (! function_exists('getFallbackLocale')) {
    /**
     * Get the fallback locale
     *
     * @return \Illuminate\Foundation\Application|mixed
     */
    function getFallbackLocale()
    {
        return config('app.fallback_locale');
    }
}

if (! function_exists('getLanguageBlock')) {

    /**
     * Get the language block with a fallback
     *
     * @param $view
     * @param array $data
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    function getLanguageBlock($view, $data = [])
    {
        $components = explode("lang", $view);
        $current  = $components[0]."lang.".app()->getLocale().".".$components[1];
        $fallback  = $components[0]."lang.".getFallbackLocale().".".$components[1];

        if (view()->exists($current)) {
            return view($current, $data);
        } else {
            return view($fallback, $data);
        }
    }
}

function is_wholesale()
{
    return session('wholesaler.on');
}
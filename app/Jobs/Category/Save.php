<?php

namespace App\Jobs\Category;

use App\Jobs\Job;
use App\Models\Category;
use App\Repositories\Backend\Category\CategoryContract;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class Save extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $attributes;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($id=null, $title, $parent_id, $published, $description, $description_bottom)
    {
        $this->attributes = [
            'id' => $id,
            'title' => $title,
            'parent_id' => $parent_id,
            'published' => $published,
            'description' => $description,
            'description_bottom' => $description_bottom,
        ];
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if(is_null($this->attributes['id'])) {
            $category = Category::create($this->attributes);
        } else {
            $category = Category::findOrFail($this->attributes['id']);
            $category->update($this->attributes);
        }

        return 12;//$category->id;
    }
}

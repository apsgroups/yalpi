<?php

namespace App\Exceptions;

use Exception;

/**
 * Class GeneralException
 * @package App\Exceptions
 */
class ImportException extends Exception {}
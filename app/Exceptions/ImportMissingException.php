<?php

namespace App\Exceptions;

use Exception;

/**
 * Class GeneralException
 * @package App\Exceptions
 */
class ImportMissingException extends Exception {
    protected $articul;

    protected $name;

    protected $sku;

    public function __construct($articul, $name, $sku = '')
    {
        $this->articul = $articul;

        $this->name = $name;

        $this->sku = $sku;
    }

    public function getArticul()
    {
        return $this->articul;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getSku()
    {
        return $this->sku;
    }
}
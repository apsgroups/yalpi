<?php

namespace App\Exceptions\Frontend;

use Exception;

/**
 * Class GeneralException
 * @package App\Exceptions
 */
class OrderException extends Exception {}
<?php namespace App;

use Landish\Pagination\UIKit;

class Pagination extends UIKit {

    protected function getAvailablePageWrapper($url, $page)
    {
        $parsedUrl = parse_url($url);
        parse_str($parsedUrl['query'], $params);

        if(isset($params['page']) && $params['page'] === '1') {
	        $url = str_replace(['&page=1', 'page=1'], ['', ''], $url);
        }

        if(ends_with($url, '?')) {
            $url = str_replace('?', '', $url);
        }
        
        return parent::getAvailablePageWrapper($url, $page);
    }
    
    protected $previousButtonText = 'Назад';

    protected $nextButtonText = 'Вперед';
}
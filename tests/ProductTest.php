<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ProductTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $user = factory(App\Models\Access\User\User::class)->create();

        $this->actingAs($user, 'admin');

        $response = $this->call('GET', route('admin.index'));

        $this->assertEquals(200, $response->status());
    }
}

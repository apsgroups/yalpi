(function ($) {
    $('body').on('click', '.addtocart', function(e) {
        e.preventDefault();

        var $productId = $(e.target).attr('data-product-id');
        var cartData = {'product_id': $productId, properties: [], attribute: null};

        $('.product-property.active').each(function() {
            var property = {
                id: $(this).parent().attr('data-property-id'),
                value: $(this).attr('data-property-value-id')
            };

            cartData.properties.push(property);
        });

        if($('#button-attribute').length) {
            cartData.attribute = $('#button-attribute').val();
        }

        var $desk = $('#selected-desk-id[data-product-id='+$productId+']');
        if($desk.length) {
            cartData.desk = $desk.val();
        }
        $.ajax({
            url: '/cart/add/',
            data: cartData,
            type: 'post',
            dataType: 'json',
            success: function(data) {
                updateMiniCart(data.amount, data.cost);

                $('#user-bar-fixed').show();

                setTimeout(function() {
                    $('#user-bar-fixed').fadeOut('slow');
                }, 4000);
            }
        });
    });

    $('#button-attribute').change(function() {
        var attrId = $(this).val();

        $('.attribute-price, .price-default, .attribute-characteristics, .attribute-characteristics-default').hide();
        $('#price-box-'+attrId).show();

        if($('#attribute-characteristics-'+attrId).length) {
            $('#attribute-characteristics-'+attrId).show();
        } else {
            $('#attribute-characteristics-default').show();
        }
    });


    var youtube = false;

    var timeoutWatch;

    var timeWatch = 0;

    var videoGoals = function (id) {
        clearInterval(timeoutWatch);

        var goal10sec = false;

        var goal30sec = false;

        var player = new YT.Player(id, {
            playerVars: {'autoplay': 1},
            events: {
                'onStateChange': function(event) {
                    if(event.data === 1) {
                        timeoutWatch = setInterval(function() {
                            timeWatch++;

                            if(timeWatch >= 10 && !goal10sec) {
                                goal10sec = true;
                                window.yaCounter37166085.reachGoal('video_10s');
                            }

                            if(timeWatch >= 30 && !goal30sec) {
                                goal30sec = true;
                                window.yaCounter37166085.reachGoal('video_30s');
                            }
                        }, 1000);
                    } else {
                        clearInterval(timeoutWatch);
                    }
                }
            }
        });

        return player;
    };

    if($('.fotorama').length) {            
        $('.fotorama')
            .on('fotorama:loadvideo', function (e, fotorama, extra) {
                var id = fotorama.activeFrame.video.id;

                fotorama.activeFrame.$video.find('iframe').attr('id', id);

                if(!youtube) {
                    var tag = document.createElement('script');
                    tag.src = "https://www.youtube.com/iframe_api";
                    var firstScriptTag = document.getElementsByTagName('script')[0];
                    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

                    youtube = true;

                    window.onYouTubePlayerAPIReady = function() {
                        videoGoals(id);
                    };

                    window.yaCounter37166085.reachGoal('open_video');
                } else {
                    videoGoals(id);
                }
            })
            .on('fotorama:showend', function (e, fotorama, extra) {
                clearInterval(timeoutWatch);

                if(typeof fotorama.activeFrame.video !== 'undefined' && fotorama.activeFrame.video.id && youtube) {
                    videoGoals(fotorama.activeFrame.video.id);
                }
            })
            .fotorama();
    }
}( jQuery ));
var prodInRow = 0;

var alignShortTags = function(resize) {
    var ww = parseInt($(window).width());
    var inRow = ww >= 768 ? 3 : 2;

    if(resize && prodInRow === inRow) {
        return;
    }

    if(resize) {
        $('.short-tag.imit').remove();
        $('.short-tag-checked').removeClass('.short-tag-checked');
    }

    prodInRow = inRow;

    var currRow = 1;
    var shortTagRows = [];

    $('.product:not(.short-tag-checked)', '#product-grid').each(function(i) {
        var num = i+1;
        
        currRow = Math.ceil(num / inRow);

        $(this).attr('data-row', currRow);
        $(this).addClass('shot-tag-checked');

        if($('.short-tag', this).length > 0) {
            if(!shortTagRows[currRow]) {
                shortTagRows[currRow] = 0;
            }

            shortTagRows[currRow]++;
        }
    });

    for (var key in shortTagRows) {
      if(shortTagRows[key] < currRow) {
        $('[data-row='+key+']').each(function() {
            if(!$('.short-tag', this).length) {
                $('<div />').addClass('short-tag imit').html('&nbsp;').insertAfter($('.img', this));
            }
        });
      }
    }
};

var productList = {
    addLoader: function() {
        this.removeLoader();

        $('<div class="loader" id="filter-loader" />').appendTo('#filter-results');
    },
    removeLoader: function() {
        $('#filter-loader').remove();
    },
    load: function(url, data, beforeLoad, afterLoad) {
        var obj = this;

        $.ajax({
            url: url,
            data: data,
            dataType: 'json',
            cache: false,
            beforeSend: function() {
                if(typeof beforeLoad == "function") {
                    beforeLoad();
                }

                obj.addLoader();
            },
            success: function(data) {
                if(typeof data.url !== 'undefined') {
                    history.replaceState(null, null, data.url);
                }

                if(typeof data.filterUrl !== 'undefined') {
                    $('#filter-form').attr('action', data.filterUrl);
                }

                $('#filter-results').empty().html(data.products);

                alignShortTags(false);

                if(typeof data.title !== 'undefined') {
                    $('title').empty().html(data.title);
                }

                if(typeof data.heading !== 'undefined') {
                    $('#page-heading').empty().html(data.heading);
                }

                $('.pagination-box').ajaxPagination({
                    onClick: function(btn) {
                        productList.load($(btn).attr('data-href'));
                    }
                });

                obj.removeLoader();

                if(typeof afterLoad == "function") {
                    afterLoad(data);
                }

                $('html, body').animate({
                    scrollTop: $("#filter-results").offset().top - 100
                }, 500);
            }
        });
    }
};

(function ($) {
    $('.pagination-box').ajaxPagination({
        onClick: function(btn) {
            productList.load($(btn).attr('data-href'));
        }
    });

    $('body').on('click', '#sort-links a', function(e) {
        e.preventDefault();

        productList.load($(this).attr('href'));
    });

    alignShortTags(true);

    $(window).resize(function() {
        alignShortTags(true);
    });
}( jQuery ));
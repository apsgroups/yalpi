var jivoCalled = false;

var initJivo = function(){ 
	var widget_id = 'widgetidhere';
	var d=document;
	var w=window;
	function l(){
		var s = document.createElement('script'); 
		s.type = 'text/javascript'; 
		s.async = true; 
		s.src = '//code.jivosite.com/script/widget/'+widget_id; 
		var ss = document.getElementsByTagName('script')[0]; 
		ss.parentNode.insertBefore(s, ss);
	}

	if(d.readyState=='complete'){
		l();
	} else {
		if(w.attachEvent){
			w.attachEvent('onload',l);
		} else {
			w.addEventListener('load',l,false);
		}
	}

	console.log('Trying to init jivosite');

	jivoCalled = true;
};

var jivo_onLoadCallback = function() {
	console.log('Chat inited');
	jivo_api.open();
}

jQuery(document).ready(function() {
	$('.chat-btn').click(function(e) {
		e.preventDefault();

		console.log('clicked chat.btn');

		if(jivoCalled) {
			jivo_api.open();
		} else {
			initJivo();
		}
	});
});
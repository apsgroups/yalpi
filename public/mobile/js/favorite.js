(function ($) {
    $('body').on('click', '.btn-favorite', function(e) {
        e.preventDefault();

        var $btn = $(this);

        var id = $btn.attr('data-product-id');

        var action = ($btn.hasClass('active')) ? 'remove' : 'add';

        $.ajax({
            url: '/favorite/'+action+'/'+id+'/',
            type: 'get',
            dataType: 'json',
            statusCode: {
                401: function(response) {

                }
            },
            success: function(data) {
                var $favTotal = $('#favorites-total');
                $favTotal.empty().text(data.total);

                if(data.total > 0) {
                    $('#favorites').removeClass('empty');
                } else {
                    $('#favorites').addClass('empty');
                }

                if($btn.hasClass('update')) {
                    productList.load('/favorite/');
                    return;
                }

                if(data.active) {
                    $('.btn-favorite[data-product-id='+id+']').addClass('active');
                } else {
                    $('.btn-favorite[data-product-id='+id+']').removeClass('active');
                }

            }
        });
    });
}( jQuery ));
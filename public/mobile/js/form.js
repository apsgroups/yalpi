$(function() {
    // Limit the number of files in a FileList.
    $.validator.addMethod( "maxfiles", function( value, element, param ) {
        if ( this.optional( element ) ) {
            return true;
        }

        if ( $( element ).attr( "type" ) === "file" ) {
            if ( element.files && element.files.length > param ) {
                return false;
            }
        }

        return true;
    }, $.validator.format("Выберите не более {0} файлов."));

    $.validator.addMethod("maxsize", function( value, element, param ) {
        if (this.optional(element) ) {
            return true;
        }

        param = 1024*1024*param;

        if ( $( element ).attr( "type" ) === "file" ) {
            if ( element.files && element.files.length ) {
                for ( var i = 0; i < element.files.length; i++ ) {
                    if ( element.files[ i ].size > param ) {
                        return false;
                    }
                }
            }
        }

        return true;
    }, $.validator.format("Максимальный размер файла {0} MB.") );


    $.validator.addClassRules("maxfiles-validate", {
         maxfiles: 3,
         maxsize: 4
    });

    $('.inputfile').change(function() {
        $('.inputfile-list').empty();

        if($(this).valid()) {                    
            var files = $(this).prop('files');

            for (var i = 0; i < files.length; i++) {
                $('<div>'+files[i].name+'</div>').appendTo('.inputfile-list');
            }    
        }     
    });

    var ajaxSubmit = function(form) {
        $.ajax({
            url: form.action,
            type: form.method,
            processData: false,
            contentType: false,
            data: new FormData(form),
            dataType: 'json',
            beforeSend: function() {
                $('button', form).button('loading');
            },
            error: function() {
                $('button', form).button('reset');
            },
            success: function(response) {
                $(form).empty().html(response.msg);

                $('html,body').animate({
                    scrollTop: $(form).offset().top - ((parseInt($(window).height()) - parseInt($(form).height())) / 2)
                }, 1000);
            }
        });
    };

    $('.validate-form').each(function() {
        $(this).validate({
            rules: {
                email: {
                    customemail: true
                }
            },
            errorClass: "error",
            submitHandler: function(form) {
                if($(form).hasClass('ajax-form')) {
                    ajaxSubmit(form);

                    return false;
                }

                return true;
            },
            errorPlacement: function(error, element) {

            },
            highlight: function(element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.form-group').removeClass('has-error');
            }
        });
    });

    $(".phone-mask").mask('+7(999) 999-99-99');

    $.validator.addMethod('customemail', function (emailaddr, element) {
        emailaddr = emailaddr.replace(/\s+/g, '');
        return this.optional(element) ||
            emailaddr.match(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/i);
    });
});
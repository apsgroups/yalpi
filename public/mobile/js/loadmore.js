(function ($) {
    $('body').on('click', '.loadmore', function(e) {
        e.preventDefault();

        var $btn = $(this);

        $.ajax({
            url: $btn.attr('data-href'),
            dataType: 'json',
            data: {loadmore: true},
            beforeSend: function() { 
                productList.addLoader();
                $('.loadmore').button('loading');
            },
            success: function(data)  {
                productList.removeLoader();

                if(typeof data.history === 'undefined' || data.history === 1) {
                    history.pushState(null, null, $btn.attr('data-href'));
                }

                $('.loadmore-hide').hide();

                $(data.products).appendTo('#product-grid');

                alignShortTags();

                $('.loadmore-block').empty().html(data.loadmore);

                if(typeof data.title !== 'undefined') {
                    $('title').empty().html(data.title);
                }

                $('.pagination-box').empty().html(data.pagination).ajaxPagination({
                    onClick: function(btn) {
                        productList.load($(btn).attr('data-href'));
                    }
                });
            }
        });
    });
}( jQuery ));
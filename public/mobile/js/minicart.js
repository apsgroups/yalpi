var updateMiniCart = function(amount, cost) {
    $('.minicart-amount').empty().text(amount);
    $('.minicart-cost').empty().text(cost);

    if(amount > 0) {
        $('.minicart').removeClass('empty');
    } else {
        $('.minicart').addClass('empty');
    }

    if($('.empty', '#user-bar').length < 2) {
    	$('#user-bar').addClass('visible');
    } else {
    	$('#user-bar').addClass('visible');
    }
};
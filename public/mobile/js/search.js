(function ($) {
    var setSearchCategory = function(id) {
        var $btn = $('[data-id='+id+']', '#search-categories');

        var text = 'Категории';
        var value = '';

        if($btn.length) {
            text = $btn.text();
            value = id;
        }

        $('#search-categories-lbl').empty().text(text);
        $('#search-category-input').val(value);
    };

    $('body').on('click', '#search-categories a', function(e) {
        e.preventDefault();

        setSearchCategory($(this).attr('data-id'));
    });

    setSearchCategory($('#search-category-input').val());
}( jQuery ));
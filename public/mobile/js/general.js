$(function() {
    // Sending goal to Yandex
    $('body').on('click', '[data-goal]', function(e) {
        var goal = $(this).attr('data-goal');

        if(goal) {
            global.yaCounter37166085.reachGoal(goal);
        }
    });
});
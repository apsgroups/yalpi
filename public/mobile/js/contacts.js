jQuery(document).ready(function($) {
    var addMap = function(id) {
        return new ymaps.Map(id, {
            center: [37.556160594996605, 55.77713522579245],
            zoom: 9,
            controls: []
        });
    };

    var addPlacemark = function(map, content, icon, lat, long) {
        var balloonContent = '<div style="width: 260px; line-height: 16px; font-size: 14px;">'+content+'</div>';

        var obj = new ymaps.Placemark([lat, long], {
            hintContent: balloonContent,
            balloonContent: balloonContent
        }, {
            iconLayout: 'default#image',
            iconImageHref: '/mobile/img/contact/map-icon-'+icon+'.png',
            iconImageSize: [46, 54],
            iconImageOffset: [-18, -52]
        });

        map.geoObjects.add(obj);

        return map;
    };

    $('.show-on-map').click(function(e) {
        e.preventDefault();

        if(!$('.show-on-map').hasClass('map-enabled')) {
            return;
        }

        var $btn = $(this);
        var map = $(this).attr('data-map');
        var content = $('#'+$(this).attr('data-content')).html();
        var lat = $(this).attr('data-lat');
        var long = $(this).attr('data-long');
        var icon = $(this).attr('data-map-icon');
        var $map = $('#'+map);

        if($map.hasClass('hidden')) {
            $map.height(0).removeClass('hidden').animate({
                'height': 440
            }, 'fast', function() {
                if(!$map.attr('data-loaded')) {
                    var contactMap = addMap(map);

                    $('.show-on-map').each(function(e) {
                        contactMap = addPlacemark(contactMap, content, icon, lat, long);
                    });

                    $map.attr('data-loaded', 1);
                }

                $btn.find('.txt:first').empty().text('Скрыть');
                $btn.find('.txt:last').empty().text('карту');
            });
        } else {
            $map.animate({
                'height': 0
            }, 'fast', function() {
                $(this).addClass('hidden');

                $btn.find('.txt:first').empty().text('Показать');
                $btn.find('.txt:last').empty().text('на карте');
            });
        }
    });


    var addressMap;

    var initAddressMap = function(){
        addressMap = addMap("map");

        var balloonContent = '<b>Фирменный магазин дверей и мебели в Медведково</b><br /><div>ул. Осташковская, д. 22.<br />Тел: <span class="ya-phone call_phone_2">+7 (495) 135-35-79</span></div>';

        addressMap = addPlacemark(addressMap, balloonContent, 1, 37.67600675210854, 55.886390238713226);

        var balloonContent2 = '<b>Магазин дверей, офис и склад в Кунцево</b><br /><div>ул. Молодогвардейская, д. 54, стр. 4.<br />Тел: <span class="ya-phone call_phone_2">+7 (495) 984-16-99</span></div>';

        addressMap = addPlacemark(addressMap, balloonContent2, 2, 37.397053944106226, 55.73559128281092);
    };

    var getDistance = function getDistance(position)
    {
        $('.get-distance').each(function() {
            var $elem = $(this);

            ymaps.route([
                {
                    type: 'wayPoint',
                    point: [position[0], position[1]]
                },
                {
                    type: 'viaPoint',
                    point: [$elem.attr('data-lat'), $elem.attr('data-long')]
                }
            ]).then(
                function (route) {
                    $elem.removeClass('hidden');
                    $elem.find('.distance-value').html(route.getHumanLength());
                },
                function (error) {
                    console.log("Возникла ошибка: " + error.message);
                }
            );
        });
    };

    var initLocationMap = function() {
        if(!$('.get-distance').length) {
            return;
        }

        if(navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                getDistance([position.coords.longitude, position.coords.latitude]);
            });
        }
    };

    var initMaps = function() {
        //initAddressMap();
        //initLocationMap();

        $('.show-on-map').addClass('map-enabled');
    };

    ymaps.ready(initMaps);
});
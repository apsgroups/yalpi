jQuery(document).ready(function(){
    $('.colorBut').on('click',function(){
        $('.colorBut').removeClass('active');
        $(this).addClass('active');
    });

    $('.servBlock .rating input').change(function(){
        var $radio=$(this);

        $('.servBlock .rating .selected').removeClass('selected');
        $radio.closest('label').addClass('selected');
    });

    $('.catalog-menu .collapse').on('show.bs.collapse', function() {
        $(this).closest('li').addClass('active');
    });

    $('.catalog-menu ul').on('hidden.bs.collapse', function() {
        if($(this).attr('aria-expanded') === 'false') {
            $(this).parent().removeClass('active');
        }
    });

    $('.catalog-menu li.active').each(function() {
        $(this).parents('li').addClass('active');
        $(this).parents('ul').addClass('in');
        $('.catalog-menu ul.in > li.active > span.toggle').removeClass('collapsed').attr('aria-expanded', 'true');
    });

    $('.catalog-menu .parent').click(function() {
        // hack for iOS
    });

    //left sidebar
    $("#offcanvas-box").mmenu({
        navbar: { title: null },
        dragOpen: { open: true, pageNode: $('body') },
        dragClose: { close: true },
        transitionDuration: 250
    });

    var scrollPos = 0;

    $('.modal')
        .on('show.bs.modal', function (){
            scrollPos = $('body').scrollTop();

            $('#allFilters .modal-dialog, #allFilters .modal-dialog .modal-content').css('min-height', window.innerHeight+'px');

            $('body').css({
                overflow: 'hidden',
                position: 'fixed',
                top : -scrollPos
            });
        })
        .on('hide.bs.modal', function (){
            $('body').css({
                overflow: '',
                position: '',
                top: ''
            }).scrollTop(scrollPos);
        });

    $('#drop-catalog-menu-btn').click(function() {
        if(!$('#drop-catalog-menu').hasClass('visible')) {             
            $('#drop-catalog-menu').stop().slideDown();                
            $('#drop-catalog-menu').addClass('visible');
        } else {                
            $('#drop-catalog-menu').stop().slideUp();         
            $('#drop-catalog-menu').removeClass('visible');
        }
    });

    if($('#category-overlay-boxes').length > 0) {
        $('#category-overlay-boxes').carousel();
        $('#category-overlay-boxes').bcSwipe({ threshold: 50 });
    }

    $('.offcanvas-menu .active > ul').addClass('in');
});
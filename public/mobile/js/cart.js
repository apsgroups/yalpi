(function ($) {
    $("#checkout-form").validate({
        submitHandler: function(form) {
            $('button', form).button('loading');
            return true;
        },
        highlight: function(element) {
            $(element).parent('.form-group').addClass('has-error');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parent('.form-group').removeClass('has-error');
        },
        errorPlacement: function(error, element) { },
        city: {
            required: function(element){
                return parseInt($("#shipping_id").val()) === 1;
            }
        },
        street: {
            required: function(element){
                return parseInt($("#shipping_id").val()) === 1;
            }
        },
        home: {
            required: function(element){
                return parseInt($("#shipping_id").val()) === 1;
            }
        },
        apartment: {
            required: function(element){
                return parseInt($("#shipping_id").val()) === 1;
            }
        }
    });

    var xhrSubmitCart;

    var submitCart = function() {
        var $form = $('#cart-form');

        if(xhrSubmitCart && xhrSubmitCart.readystate != 4){
            xhrSubmitCart.abort();
        }

        xhrSubmitCart = $.ajax({
            url: $form.attr('action'),
            data: $form.serialize(),
            type: 'post',
            dataType: 'json',
            success: function(data) {
                updateDetailCart(data);
            }
        });
    };

    var updateDetailCart = function(data) {
        updateMiniCart(data.amount, data.word);

        if(data.amount >= 1) {
            $('.cart-cost').html(data.cost);
        } else {
            $('.cart-page').hide();
            location.reload();
        }

        for (var key in data.products) {
            if (data.products.hasOwnProperty(key)) {
                $("[data-total-price='"+key+"']").empty().text(data.products[key].cost);
            }
        }
    };

    var removeFromCart = function(id) {
        $.ajax({
            url: '/cart/remove/',
            data: {id: id},
            type: 'post',
            dataType: 'json',
            success: function(data) {
                updateDetailCart(data);
            }
        });
    };

    //quantity of items in buy list (basket)
    $(document).on('click', '.number-spinner button', function () {
        var btn = $(this),
            oldValue = btn.closest('.number-spinner').find('input').val().trim(),
            newVal = 0;

        if (btn.attr('data-dir') == 'up') {
            newVal = parseInt(oldValue) + 1;
        } else {
            if (oldValue > 1) {
                newVal = parseInt(oldValue) - 1;
            } else {
                newVal = 1;
            }
        }

        btn.closest('.number-spinner').find('input').val(newVal);

        submitCart();
    });

    $('.quantity-control .amount', document).bind('change', function() {
        submitCart();
    });

    $('.cart-form', document).bind('submit', function(e) {
        e.preventDefault();
        submitCart();
    });

    $('.remove-cart', document).bind('click', function() {
        var id = $(this).attr('data-product');
        $("[data-cart-product='"+id+"']").remove();

        removeFromCart(id);
    });

    $('[data-input]').click(function(e) {
        e.preventDefault();

        var id = $(this).attr('data-input');
        var val = $(this).attr('data-value');

        $(id).val(val);

        $('[data-input="'+id+'"]').removeClass('active');

        $(this).addClass('active');

        if(id === '#shipping_id') {
            val === '1' ? $('#deliv').stop().slideDown() : $('#deliv').stop().slideUp();
        }
    });

    if($('#time-reserve').length > 0) {
        var remain = parseInt($('#time-reserve').attr('data-remein'));

        function parseTime(timestamp){
            if (timestamp <= 0) {
                timestamp = 0;
            }

            var hour = Math.floor(timestamp/60/60);
            var mins = Math.floor((timestamp - hour*60*60)/60);
            var secs = Math.floor(timestamp - hour*60*60 - mins*60);

            $('.minutes', '#time-reserve').text(mins);
            $('.sec', '#time-reserve').text(secs);
        }

        var intervalId = setInterval(function(){
            remain = remain - 1;
            parseTime(remain);
            if(remain <= 0){
                $('#time-reserve').remove();
                clearInterval(intervalId);
            }
        }, 1000);
    }
}(jQuery));
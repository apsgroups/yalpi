(function ($) {
    var excludeParams = function($form) {
        var exclude = [];

        var $disabled = $form.find('input:disabled');

        $disabled.prop('disabled', false);

        var data = $form.find('input').not(exclude.join(', ')).serialize();

        $disabled.prop('disabled', true);

        return data;
    };

    var applyFilter = function() {
        var $form = $('#filter-form');

        if($form.hasClass('loader')) {
            return;
        }

        var data = excludeParams($form);

        var beforeLoad = function() {
            $form.addClass('loader');
            $('#allFilters').modal('hide');
        };

        var afterLoad = function(data) {
            $form.removeClass('loader');
        };

        productList.load($form.attr('action'), data, beforeLoad, afterLoad);
    };

    var filterOptions = function() {
        var $form = $('#filter-form');

        if(!$form.length || $form.hasClass('no-options')) {
            return;
        }

        if($('#filter-term').length) {
            $('#filter-term').val($('#search-term').val());
        }

        $.ajax({
            url: '/filter/options/',
            dataType: 'json',
            data: $form.serialize(),
            success: function(data) {
                $('#filter-total-text').empty().text(data.total_text);

                $.each(data.options, function(name, opts) {
                    $.each(opts, function(title, total) {
                        var $input;

                        if(name == 'novelty' || name == 'sale' || name == 'in_stock') {
                            $input =  $('#'+name+'_check');
                        } else {
                            $input = $("input[name='filter["+name+"][]'][value='"+title+"']");
                        }

                        if(!$input.length) {
                            return;
                        }

                        if(total > 0) {
                            $input.prop('disabled', false);
                            $('label[for='+$input.attr('id')+']').removeClass('disabled');
                        } else {
                            $input.prop('disabled', true);
                            $('label[for='+$input.attr('id')+']').addClass('disabled');
                        }
                    });
                });
            }
        });
    };

    $('#filter-form').submit(function(e) {
        e.preventDefault();

        applyFilter();
    });

    $('input[type=checkbox]', '#filter-form').change(function() {
        filterOptions();
    });

    $('.color-checkbox input[type=checkbox]').change(function() {
        var $imgLbl = $(this).parent().find('.colorCheck');
        var $check = $(this);

        if($check.prop('disabled')) {
            return;
        }

        if($check.prop('checked')) {
            $imgLbl.addClass('active');
        } else {
            $imgLbl.removeClass('active');
        }
    });

    filterOptions();

    $('#clear-filter').click(function(e) {
        e.preventDefault();

        $('#filter-form').trigger('reset');

        $('.color-checkbox .colorCheck').removeClass('active');

        filterOptions();

        applyFilter();
    });
}(jQuery));
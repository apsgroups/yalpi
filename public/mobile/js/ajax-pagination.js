(function ($) {
    $.fn.ajaxPagination = function( options ) { 
        $('a', this).each(function() {
            var href = $(this).attr('href');
            console.log(href);
            $(this).attr('href', '#');
            $(this).attr('data-href', href);

            $(this).click(function(e) {
                e.preventDefault();

                options.onClick(this);
            });
        });
    };
}(jQuery));
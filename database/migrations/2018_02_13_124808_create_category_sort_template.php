<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategorySortTemplate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category_sort_template', function (Blueprint $table) {
            $table->unsignedInteger('sort_template_id')->foreign('sort_template_id')->references('sort_templates')->onDelete('CASCADE');
            $table->unsignedInteger('category_id')->foreign('category_id')->references('categories')->onDelete('CASCADE');
            $table->unsignedInteger('ordering')->default(0);
            $table->unsignedInteger('items')->default(0);

            $table->unique(['sort_template_id', 'category_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('category_sort_template', function (Blueprint $table) {
            //
        });
    }
}

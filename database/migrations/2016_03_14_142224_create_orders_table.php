<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->unsignedInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->string('phone');
            $table->string('email');
            $table->text('comment');
            $table->unsignedInteger('payment_id');
            $table->foreign('payment_id')->references('id')->on('payments');
            $table->unsignedInteger('shipping_id');
            $table->foreign('shipping_id')->references('id')->on('shippings');
            $table->unsignedSmallInteger('amount');
            $table->decimal('subtotal', 10, 2);
            $table->decimal('total', 10, 2);
            $table->decimal('payment_total', 10, 2);
            $table->decimal('shipping_total', 10, 2);
            $table->decimal('mounting_total', 10, 2);
            $table->boolean('mounting')->default(false);
            $table->boolean('oneclick')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('orders');
    }
}
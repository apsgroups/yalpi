<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertyValueParents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_value_parents', function (Blueprint $table) {
            $table->increments('id');
            
            $table->unsignedInteger('property_value_id')
                ->foreign('property_value_id')
                ->references('id')
                ->on('property_values')
                ->onDelete('CASCADE');

            $table->unsignedInteger('parent_id')
                ->foreign('parent_id')
                ->references('id')
                ->on('property_values')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('property_value_parents');
    }
}

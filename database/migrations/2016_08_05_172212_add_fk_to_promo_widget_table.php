<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFkToPromoWidgetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('promo_widgets', function (Blueprint $table) {
            $table->foreign('widget_id')->references('id')->on('widgets')->onDelete('cascade');
        });

        Schema::table('category_promo_widget', function (Blueprint $table) {
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');

            $table->foreign('promo_widget_id')->references('id')->on('promo_widgets')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('category_promo_widget', function (Blueprint $table) {
            //
        });
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiscussionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discussions', function (Blueprint $table) {
            $table->increments('id');

            $table->timestamps();

            $table->string('name');

            $table->text('message');

            $table->unsignedInteger('parent_id')
                ->nullable()
                ->foreign('parent_id')
                ->references('id')
                ->on('discussions')
                ->onDelete('cascade');

            $table->unsignedInteger('product_id')
                ->nullable()
                ->foreign('product_id')
                ->references('id')
                ->on('products')
                ->onDelete('cascade');

            $table->unsignedInteger('user_id')
                ->nullable()
                ->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('set null');

            $table->boolean('manager')->default(0);

            $table->boolean('published')->default(0);

            $table->boolean('answer')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('discussions');
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductAttributesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_attributes', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->unsignedInteger('product_id')
                ->foreign('product_id')
                ->references('id')
                ->on('products')
                ->onDelete('CASCADE');

            $table->unsignedInteger('attribute_id')
                ->foreign('attribute_id')
                ->references('id')
                ->on('attributes')
                ->onDelete('CASCADE');

            $table->string('value');
            $table->string('sku');
            $table->unsignedInteger('image_id')->nullable();
            $table->foreign('image_id')->references('id')->on('images')->onDelete('set null');
            $table->decimal('price', 10, 2)->index();
            $table->decimal('price_old', 10, 2);
            $table->decimal('price_wholesale', 10, 2);
            $table->decimal('price_old_wholesale', 10, 2)->index();
            $table->decimal('price_base', 10, 2)->index();
            $table->decimal('price_base_wholesale', 10, 2);
            $table->unsignedTinyInteger('discount');
            $table->unsignedSmallInteger('discount_from');
            $table->integer('ordering');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('product_attributes');
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilterSeosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('filter_seos', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('category_id')
                ->null()
                ->foreign('category_id')
                ->on('categories');

            //$table->

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('filter_seos');
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderProductPropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_product_properties', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->unsignedInteger('order_product_id');
            $table->foreign('order_product_id')->references('id')->on('order_products')->onDelete('cascade');

            $table->unsignedInteger('product_property_id')->nullable();
            $table->foreign('product_property_id')->references('id')->on('product_properties')->onDelete('set null');

            $table->string('title');
            $table->string('content');

            $table->decimal('price', 10, 2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('order_product_properties');
    }
}

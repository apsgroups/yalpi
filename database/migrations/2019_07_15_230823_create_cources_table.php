<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCourcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cources', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('title');
            $table->string('video');
            $table->unsignedInteger('duration')->default(0);
            $table->text('desc');
            $table->integer('ordering')->default(0);
            $table->unsignedInteger('product_id')->foreign('product_id')->references('id')->on('products')->onDelete('CASCADE');
            $table->unsignedInteger('image_id')->foreign('image_id')->references('id')->on('images')->onDelete('SET NULL');
            $table->boolean('free')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cources');
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrettyUrlsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pretty_urls', function (Blueprint $table) {
            $table->increments('id');
            $table->string('pretty_url');
            $table->text('description');
            $table->text('heading');
            $table->text('page_title');
            $table->text('metadesc');
            $table->text('metakeys');
            $table->unsignedInteger('category_id')
                ->nullable()
                ->foreign('category_id')
                ->references('id')
                ->on('categories')
                ->onDelete('SET NULL');
            $table->string('options');
            $table->decimal('price_min')->nullable();
            $table->decimal('price_max')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pretty_urls');
    }
}

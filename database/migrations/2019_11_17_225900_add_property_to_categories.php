<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPropertyToCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('categories', function (Blueprint $table) {
            $table->unsignedInteger('primary_property_id')
                ->nullable()
                ->foreign('primary_property_id')
                ->references('id')
                ->on('properties')
                ->onDelete('SET NULL');

            $table->unsignedInteger('secondary_property_id')
                ->nullable()
                ->foreign('secondary_property_id')
                ->references('id')
                ->on('properties')
                ->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('categories', function (Blueprint $table) {
            //
        });
    }
}

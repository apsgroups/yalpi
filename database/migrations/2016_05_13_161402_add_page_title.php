<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPageTitle extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $tables = [
            'categories',
            'contents',
            'menu_items',
            'products',
            'content_types'
        ];

        foreach($tables as $name) {
            Schema::table($name, function (Blueprint $table) {
                $table->string('page_title');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

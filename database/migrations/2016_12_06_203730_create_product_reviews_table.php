<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_reviews', function (Blueprint $table) {
            $table->increments('id');

            $table->timestamps();

            $table->string('name')->nullable();

            $table->unsignedInteger('product_id')
                ->foreign('product_id')
                ->references('id')
                ->on('products')
                ->onDelete('CASCADE');

            $table->boolean('published')->default(0);

            $table->unsignedInteger('likes')->nullable();

            $table->unsignedInteger('dislikes')->nullable();

            $table->text('dignity')->nullable();

            $table->text('limitations')->nullable();

            $table->text('comment')->nullable();

            $table->decimal('rate')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('product_reviews');
    }
}

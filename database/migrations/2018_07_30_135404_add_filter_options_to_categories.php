<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFilterOptionsToCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('categories', function (Blueprint $table) {
            $table->text('filter_options')->nullable();
            $table->boolean('autofiltering')->default(0);
            $table->unsignedInteger('autofiltering_category_id')
                ->nullable()
                ->foreign('autofiltering_category_id')
                ->references('autofiltering_category_id')
                ->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('categories', function (Blueprint $table) {
            //
        });
    }
}

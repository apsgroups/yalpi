<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuthorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('authors', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('name');
            $table->string('slug');
            $table->string('facebook')->nullable();
            $table->string('twitter')->nullable();
            $table->string('vk')->nullable();
            $table->string('youtube')->nullable();
            $table->string('instagram')->nullable();
            $table->text('about')->nullable();
            $table->text('slogan')->nullable();
            $table->unsignedInteger('image_id')->foreign('image_id')->references('id')->on('images')->onDelete('SET NULL');
            $table->unsignedInteger('user_id')->foreign('user_id')->references('id')->on('users')->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('authors');
    }
}

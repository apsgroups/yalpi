<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProductAttributeId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('module_product', function (Blueprint $table) {
            $table->unsignedInteger('product_attribute_id')->nullable();
            $table->foreign('product_attribute_id')->references('id')->on('product_attributes')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('module_product', function (Blueprint $table) {
            //
        });
    }
}

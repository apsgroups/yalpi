<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentRelated extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('content_related', function (Blueprint $table) {
            $table->unsignedInteger('content_id')
                ->foreign('content_id')
                ->references('id')
                ->on('contents')
                ->onDelete('cascade');

            $table->unsignedInteger('related_id')
                ->foreign('related_id')
                ->references('id')
                ->on('contents')
                ->onDelete('cascade');

            $table->smallInteger('ordering')->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('content_related');
    }
}

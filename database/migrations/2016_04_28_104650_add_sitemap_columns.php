<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSitemapColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $tables = [
            'products',
            'categories',
            'contents',
            'content_types',
        ];

        foreach($tables as $name) {
            Schema::table($name, function (Blueprint $table) use($name) {
                $table->unsignedTinyInteger('changefreq');
                $table->unsignedTinyInteger('yandex_changefreq');
                $table->unsignedTinyInteger('google_changefreq');

                $table->unsignedTinyInteger('priority');
                $table->unsignedTinyInteger('yandex_priority');
                $table->unsignedTinyInteger('google_priority');

                if(!Schema::hasColumn($name, 'yandex_sitemap')) {
                    $table->unsignedTinyInteger('yandex_sitemap')->index();
                }

                if(!Schema::hasColumn($name, 'google_sitemap')) {
                    $table->unsignedTinyInteger('google_sitemap')->index();
                }

                $table->unsignedTinyInteger('sitemap');
            });
        }

        foreach(['categories', 'content_types'] as $name) {
            Schema::table($name, function (Blueprint $table) {
                $table->unsignedTinyInteger('sitemap_items');
                $table->unsignedTinyInteger('yandex_sitemap_items');
                $table->unsignedTinyInteger('google_sitemap_items');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveSitemapColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $columns = [
            'google_sitemap',
            'yandex_sitemap',
            'google_priority',
            'yandex_priority',
            'google_changefreq',
            'yandex_changefreq',
        ];

        $ext = [
            'google_sitemap_items',
            'yandex_sitemap_items',
        ];

        Schema::table('categories', function ($table) use($columns, $ext) {
            $table->dropColumn(array_merge($columns, $ext));
        });

        Schema::table('content_types', function ($table) use($columns, $ext) {
            $table->dropColumn(array_merge($columns, $ext));
        });

        Schema::table('contents', function ($table) use($columns) {
            $table->dropColumn($columns);
        });

        Schema::table('products', function ($table) use($columns) {
            $table->dropColumn($columns);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentWidgetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('content_widgets', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('content_id');
            $table->foreign('content_id')->references('id')->on('contents')->onDelete('cascade');

            $table->unsignedInteger('widget_id');
            $table->foreign('widget_id')->references('id')->on('widgets')->onDelete('cascade');

            $table->string('layout');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('content_widgets');
    }
}

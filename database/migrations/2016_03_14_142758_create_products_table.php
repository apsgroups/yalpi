<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->index();
            $table->string('slug')->index();
            $table->string('sku')->index();
            $table->text('description');
            $table->unsignedInteger('image_id')->nullable();
            $table->foreign('image_id')->references('id')->on('images')->onDelete('set null');
            $table->decimal('price', 10, 2)->index();
            $table->decimal('price_old', 10, 2);
            $table->decimal('price_wholesale', 10, 2);
            $table->decimal('price_old_wholesale', 10, 2)->index();
            $table->decimal('price_base', 10, 2)->index();
            $table->decimal('price_base_wholesale', 10, 2);
            $table->unsignedTinyInteger('discount');
            $table->unsignedSmallInteger('discount_from');
            $table->boolean('published')->default(false)->index();
            $table->unsignedMediumInteger('hits');
            $table->boolean('novelty')->default(false)->index();
            $table->unsignedMediumInteger('in_stock');
            $table->boolean('bestseller')->default(false);
            $table->boolean('sale')->default(false);
            $table->boolean('preorder')->default(false);
            $table->string('ext_id')->index();
            $table->boolean('yandex_sitemap')->default(false)->index();
            $table->boolean('google_sitemap')->default(false)->index();
            $table->boolean('market')->default(false)->index();
            $table->boolean('import')->default(false)->index();
            $table->text('metadesc');
            $table->text('metakeys');
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->unsignedInteger('category_id');
            $table->foreign('category_id')->references('id')->on('categories');
            $table->unsignedInteger('product_type_id');
            $table->foreign('product_type_id')->references('id')->on('product_types');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            //
        });
    }
}
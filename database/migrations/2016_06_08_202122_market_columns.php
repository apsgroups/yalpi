<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MarketColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->string('market_title');

            $table->string('sales_notes');

            $table->string('market_category_id')
                ->nullable()
                ->foreign('market_category_id')
                ->references('id')
                ->on('market_categories')
                ->onDelete('set null');
        });

        Schema::table('categories', function (Blueprint $table) {
            $table->string('market_title');

            $table->string('sales_notes');

            $table->string('market_category_id')
                ->nullable()
                ->foreign('market_category_id')
                ->references('id')
                ->on('market_categories')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMultiuploadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('multiuploads', function (Blueprint $table) {
            $table->increments('id');
            $table->string('upload_token');
            $table->string('title');
            $table->unsignedInteger('multiuploadable_id');
            $table->string('multiuploadable_type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('multiuploads');
    }
}